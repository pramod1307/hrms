var Calendar = function() {
    return {
        init: function() {
            Calendar.initCalendar();
        },

        initCalendar: function() {
            if (!jQuery().fullCalendar) {
                return;
            }
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var h = {};

            if (Metronic.isRTL()) {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaWeek, month'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, prev,next'
                    };
                }
            } else {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'month,agendaWeek,agendaDay'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,month,agendaWeek,agendaDay'
                    };
                }
            }
            $('#calendar').fullCalendar('destroy');     // destroy the calendar
            $('#calendar').fullCalendar({               //re-initialize the calendar
                header: h,
                defaultView: 'month',                   // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                firstDay: 1,
                
                eventSources: {
                    //url: "ajax/getEventList.php?type=myNewEvents",
                    //url: "../../pages/Attendance/calling_page.php?type=myNewEvents&emp_code="+$('#emp_code').val(),
                    url: "../../pages/Attendance/employee-record-v1.php?auth=admin&fromType=calendar&emp_code="+$('#emp_code').val(),
                    type:'POST',
                    textColor: 'black'
                },
                eventRender: function (event, element) {

                    element.attr('href', 'javascript:void(0);');
                    element.click(function() {
                       
                        $('#birthdayRecords').html('');
                        $('#anniversaryRecords').html('');
                        $("#shiftName").html('');
                        $("#shiftInOutTime").html('');
                        $("#presetStatus").html('');
                        $("#firstpresetStatus").html('');
                        $("#secondpresetStatus").html('');
                        $("#startTime").html('');
                        $("#endTime").html('');
                        $("#leaveReason").html('');
                        var lateComingMessage = '';
                        if(typeof event.attendanceMessage !== 'undefined' && event.attendanceMessage != ''){
                            lateComingMessage +='<font style="color: #cc3300;">'+event.attendanceMessage+'</font>';
                        }
                        
                        if(event.hasOwnProperty('gatePassLate')){
                            console.log(event.gatePassLate);
                            var html1='<div><span><b>Gate Movement:</b></span></div>';
                                html1 +='<span style="font-size:.9em;">Total Outtime: '+event.gatePassLate.totalHours+' hrs.<br>';
                                html1 += (event.gatePassLate.reason != '') ? 'Reason: '+event.gatePassLate.reason : '';
                            html1 +='</span></div>';
                        }
                        $('#anniversaryRecords').html(html1);
                        
                        $("#shiftName").html(event.shiftName);
                        $("#shiftInOutTime").html(event.shiftInOutTime);

                        if(event.title !== 'POW' && event.title !== 'POH') {
                            $("#presetStatus").html(event.title);
                        }
                        $("#firstpresetStatus").html(event.firstpresetStatus);
                        if(event.startTime !== 'N/A' || event.endTime !== 'N/A'){
                            $("#startTime").html($.trim(event.startTime+" - "+event.endTime + ((lateComingMessage != '') ? "<br>"+lateComingMessage : '')) );
                        }
                        
                        $("#eventContent").dialog({ modal: true, title: event.formattedDate, width:230});
                    });
                },
                eventOverlap:true
            });
        }
    };
}();
function wordInString(s, word){
  return new RegExp( '\\b' + word + '\\b', 'i').test(s);
}