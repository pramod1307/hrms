<?php
/*ini_set("display_errors", "on");
error_reporting(E_ALL);*/
class global_class{
    function __construct(){
      
    }

    function checkEmployeeMarkAttendance_global($empCode, $currentDate = '') {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;

        $currentDate = (empty($currentDate))?date('Y-m-d'):$currentDate;

       $sql="SELECT * FROM AttendanceAll WHERE UpdateFlag IN('OUT', 'IN') AND Emp_Code='$empCode' AND CONVERT(date, PunchDate) = '".$currentDate."' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $data['data']['message'] = 'No';
        while($row=$fetch($result)){
            if(!empty($row['UpdateFlag']) && !empty($row['PunchDate'])){
                $data['data']['message'] = 'Yes';
                $data['data'][$row['UpdateFlag']] = date('H:i A', strtotime($row['PunchDate']));
            }
        }
        return json_encode($data);
    }

    function saveEmployeeMarkAttendance_global($empCode, $markType) {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        if(empty($markType)) return $markType;
        $markType = strtoupper($markType);
		$insertQueryForAttendanceAll = "INSERT INTO AttendanceAll (CARDNO, EMP_CODE, PunchDate, UpdateFlag) VALUES ('$empCode','$empCode',GETDATE(),'$markType')";
		$insertResultForAttendanceAll = query($query,$insertQueryForAttendanceAll,$pa,$opt,$ms_db);
		if($insertResultForAttendanceAll)
			return true;
		else
			return false;
    }

    /*
    * Check for existance leave in database table
    * return true or false
    * @param: leaveType = check which type of leaving you need to check
    */
    function checkExistanceLeave($fromDate = '05/11/2016', $toDate = '05/11/2016', $leaveType = 'odrequest', $argsData = array()){
        $fromDate   = trim($fromDate);
        $toDate     = trim($toDate);
        $leaveType  = strtolower(trim($leaveType));

        if(empty($fromDate) || empty($toDate) || empty($leaveType))
            return false;

        $fromDate   = date('Y-m-d',strtotime($fromDate));
        $toDate     = date('Y-m-d',strtotime($toDate));
        
        switch ($leaveType) {
            case 'leave':
                # code...
                break;
            
            case 'odrequest':
                $sqlq0="SELECT * FROM Leave WHERE '".$fromDate."' between cast(LvFrom as date)  and cast(LvTo as date) AND CreatedBy = '10910'";
                $resultq0=query($query,$sqlq0,$pa,$opt,$ms_db);
                if($num($resultq0)){
                   echo "done";
                }else{
                    echo "nooo";
                }
                exit;
                break;

            case 'markpastattendance':
                # code...
                break;

            default:
                # code...
                break;
        }
    }

    function getEmpAutoShiftByDate($empCode, $attnDate, $params = array()) {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $shiftReturnData = array();

        if(empty($empCode) || empty($attnDate)){
            return $shiftReturnData;
            exit();
        }

        $attnDate = date('Y-m-d', strtotime($attnDate));

        $getShiftQuery = "SELECT EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)  AS ShiftDate,CONVERT(VARCHAR(10),PunchDate,108)  AS ShiftTime, 
                        DBO.fnGetShift(EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)) AS Shift_Name, 
                        ShiftMast.Shift_From AS ShiftFrom,
                        ShiftMast.Shift_To AS Shiftto, ShiftMast.Shift_From AS ShiftFrom,
                        ShiftMast.LateAllow AS LateAllow,ShiftMast.LateAllowCycle AS LateAllowCycle,
                        ShiftMast.LateAllowGPrd AS LateAllowGPrd,ShiftMast.ErlyAllow AS ErlyAllow,
                        ShiftMast.ErlyAllowCycle AS ErlyAllowCycle,ShiftMast.ErlyAllowGPrd AS ErlyAllowGPrd,
                        ShiftMast.MHrsFul AS MHrsFul,ShiftMast.MHrsHalf AS MHrsHalf
                        FROM ATTENDANCEALL 
                        INNER JOIN ShiftMast ON ShiftMast.Shift_Code = DBO.fnGetShift(EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20))
                        WHERE EMP_CODE '".$empCode."' AND CONVERT(VARCHAR(10),PunchDate,20) = '".$attnDate."'";
        $getShiftResult = query($query,$getShiftQuery,$pa,$opt,$ms_db);
        
        if($num($getShiftResult) > 0){
            $serialNumber = 1;
            while($result = $fetch($getShiftResult)){
                $shiftReturnData['shiftName'] = $result['SHIFT'];
                $shiftReturnData['ShiftDate'] = $result['ShiftDate'];
                if($serialNumber == 1) $shiftReturnData['inTime'] = $result['ShiftTime'];
                if($serialNumber == 2) $shiftReturnData['outTime'] = $result['ShiftTime'];
                $shiftReturnData = array(
                    'empCode'           =>$empCode,
                    'attDate'           =>$attnDate,
                    'inTime'            =>$result['IN_TIME'],
                    'outTime'           =>$rowq00['OUT_TIME'],
                    'shiftName'         =>$rowq00['Shift_Name'],
                    'shiftFrom'         =>$rowq00['ShiftFrom'],
                    'shiftTo'           =>$rowq00['Shiftto'],
                    'shiftMFrom'        =>$rowq00['ShiftMFrom'],
                    'shiftMTo'          =>$rowq00['ShiftMTo'],
                    'lateAllow'         =>$rowq00['LateAllow'],
                    'lateAllowCycle'    =>$rowq00['LateAllowCycle'],
                    'lateAllowGPrd'     =>$rowq00['LateAllowGPrd'],
                    'erlyAllow'         =>$rowq00['ErlyAllow'],
                    'erlyAllowCycle'    =>$rowq00['ErlyAllowCycle'],
                    'erlyAllowGPrd'     =>$rowq00['ErlyAllowGPrd'],
                    'mHrsFul'           =>getMinutes($rowq00['MHrsFul'],'00:00:00'),
                    'mHrsHalf'          =>getMinutes($rowq00['MHrsHalf'],'00:00:00')
                    //'mHrsFul'         =>getMinutes(date_format($rowq00['MHrsFul'],'H:i:s'),'00:00:00'),
                    //'mHrsHalf'        =>getMinutes(date_format($rowq00['MHrsHalf'],'H:i:s'),'00:00:00')
                );
                $serialNumber++;
            }
        }
        return json_encode($shiftReturnData);
    }

    function getEmpShiftMasterIdByName($shiftName){
        $shiftName = trim($shiftName);
        if(empty($shiftName)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $getShiftMastQuery = "SELECT ShiftMastId FROM ShiftMast WHERE LOWER(Shift_Name) = LOWER('".$shiftName."')";
        $getShiftMastResult = query($query,$getShiftMastQuery,$pa,$opt,$ms_db);
        
        if($num($getShiftMastResult) > 0){
            $row = $fetch($getShiftMastResult);
            return $row['ShiftMastId'];
        }
        return '';
    }
    function getEmpShiftPatternIdByName($shiftPatternName){
        $shiftPatternName = trim($shiftPatternName);
        if(empty($shiftPatternName)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $getShiftPattQuery = "SELECT ShiftPatternMastid FROM ShiftPatternMast WHERE LOWER(ShiftPattern_Name) = LOWER('".$shiftPatternName."')";
        $getShiftPattResult = query($query,$getShiftPattQuery,$pa,$opt,$ms_db);
        
        if($num($getShiftPattResult) > 0){
            $row = $fetch($getShiftPattResult);
            return $row['ShiftPatternMastid'];
        }
        return '';
    }
    function checkRosterExist($rosterName, $startRostDate, $Emp_Code){
	return true; //remove this line if you want to apply validation
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $rosterName = trim($rosterName);
        if($rosterName == '') return false;

        $getAttRosterQuery          = "SELECT * FROM att_roster WHERE roster = '".$rosterName."'";
        $getRosterSchemaQuery       = "SELECT * FROM Roster_schema WHERE RosterName = '".$rosterName."'";
        $getRosterSchemaDateQuery   = "SELECT * FROM Roster_schema WHERE ('".$startRostDate."' BETWEEN start_rost AND end_rost) AND (Emp_Code = '".$Emp_Code."')";
        $getAttRosterResult = query($query,$getAttRosterQuery,$pa,$opt,$ms_db);
        $getRosterSchemaResult = query($query,$getRosterSchemaQuery,$pa,$opt,$ms_db);
        $getRosterSchemaDateResult = query($query,$getRosterSchemaDateQuery,$pa,$opt,$ms_db);
        
        if($num($getAttRosterResult) > 0 || $num($getRosterSchemaResult) > 0  || $num($getRosterSchemaDateResult) > 0){
            return false;
        }
        return true;
    }
    function checkUserExist($userName){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $userName = trim($userName);
        if($userName == '') return false;

        //$userName = md5($userName);

        $getUserQuery = "SELECT * FROM HrdMast WHERE Emp_Code = '".$userName."'";
        $getUserResult = query($query,$getUserQuery,$pa,$opt,$ms_db);
        if($num($getUserResult) == 0){
            return false;
        }
        return true;
    }

    /*
    * function for check auto shift and insert in roster
    */
    function uploadAutoshift($empCode, $month, $year, $params = array() ) {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        if(empty($empCode)) return false;

        $empCode = (!is_array($empCode)) ? trim($empCode) : $empCode;
        $date = trim($date);
        $userExistance = $this->checkUserExist($empCode);

        //check if we want to get current date auto shifts
        $isCurrentdate = ( isset($params["isCurrentdate"]) && !empty($params["isCurrentdate"]) ) ? true : false ;


        //validate if date and employee code are not valid
        if( !$isCurrentdate && (empty($month) || empty($year) || empty($empCode)) ) {
            return false;
        }

        if(is_array($empCode)){
            $Emp_Code = "'" . implode("','", $empCode) . "'";
            $empWhere = "EMP_CODE IN (".$Emp_Code.")";
        } else {
            if( !$userExistance ) return false;
            $empWhere = "EMP_CODE = '".$empCode."'";
        }

        
        //get all auto shifts of current user
        if($isCurrentdate == true){
            if(empty($params["isCurrentdate"]) || !isset($params["isCurrentdate"])) return false;

            $currentDate = $params["currentDate"];

            $getAllAutoShifts   =   "SELECT EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)  AS ShiftDate, DBO.fnGetShift(EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)) AS SHIFT FROM ATTENDANCEALL WHERE ".$empWhere." AND CAST(PunchDate AS DATE) = '".$currentDate."' GROUP BY EMP_CODE, CONVERT(VARCHAR(10),PunchDate,20)";
        }else{
            $getAllAutoShifts   =   "SELECT EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)  AS ShiftDate, DBO.fnGetShift(EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)) AS SHIFT FROM ATTENDANCEALL WHERE ".$empWhere." AND MONTH(PunchDate) = '".$month."' AND YEAR(PunchDate) = '".$year."' GROUP BY EMP_CODE, CONVERT(VARCHAR(10),PunchDate,20)";
        }

       $getAllAutoShiftsResult = query($query,$getAllAutoShifts,$pa,$opt,$ms_db);

        if($num($getAllAutoShiftsResult) > 0){

            while($autoshiftRow = $fetch($getAllAutoShiftsResult)){

                $cleanDate  = $autoshiftRow['ShiftDate'];
                $empCode    = $autoshiftRow['EMP_CODE'];

                if($this->checkRosterSchema($empCode, $cleanDate) == false){ 

                    //save into Roster Schema
                    $rosterName = $this->saveRosterSchema($empCode, $cleanDate);

                    //save att roster if roster shema inserted successfully
                    if(!empty($rosterName) && $rosterName  != false){
                        $shiftMasterId = $this->getShiftMastIdByShiftCode($autoshiftRow['SHIFT']);
                        $shiftPatternMasterId = $this->getShiftPatternPatternCode('AutoShiftP','01'); //Calliing by shift pattern name and shift_pattern_cde
                        $rosterName = $this->saveAttRoster($rosterName, $shiftMasterId, $shiftPatternMasterId, $cleanDate);
                    }
                }
            }
        }
         
    }

    function saveAttRoster($rosterName, $shiftMasterId, $shiftPatternMasterId, $rosterDate){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
       
        if(empty($rosterName) || empty($shiftMasterId) || empty($shiftPatternMasterId) || empty($rosterDate))
            return false;

        $rosterDate = date('Y-m-d', strtotime($rosterDate));

        $insertQuery = "INSERT INTO att_roster(roster,shiftMaster,shiftPattern,RosterDate,auto_period,created_on,updated_on)
                        VALUES('".$rosterName."','".$shiftMasterId."','".$shiftPatternMasterId."','".$rosterDate."','1',GETDATE(),GETDATE())";
        $response = query($query,$insertQuery,$pa,$opt,$ms_db);

        if($response)
            return true;

    }

    //Get shift pattern master id
    function getShiftPatternPatternCode($shiftPatternName, $shiftPatternCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $shiftPatternName = strtolower(trim($shiftPatternName));
        $shiftPatternCode = strtolower(trim($shiftPatternCode));

        if(empty($shiftPatternName) || empty($shiftPatternCode)) return '';

        $queryShiftPattern = "SELECT ShiftPatternMastid FROM ShiftPatternMast WHERE ShiftPattern_Name = '". $shiftPatternName."' AND ShiftPattern_Code = '".$shiftPatternCode."'";
        $queryShiftPatternResponse = query($query,$queryShiftPattern,$pa,$opt,$ms_db);
        $result = $fetch($queryShiftPatternResponse);

        return $result['ShiftPatternMastid'];
    }

    //Get shift master id by shift code
    function getShiftMastIdByShiftCode($shiftCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $shiftCode = trim($shiftCode);

        if(empty($shiftCode)) return '';

        $queryShift = "SELECT ShiftMastId FROM ShiftMast WHERE LOWER(Shift_Code) = '".$shiftCode."'";
        $responseResult = query($query,$queryShift,$pa,$opt,$ms_db);
        $result = $fetch($responseResult);

        return $result['ShiftMastId'];
    }

    //save roster schema
    function saveRosterSchema($empCode, $rosterStartDate, $rosterEndDate = ''){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $rosterName = "rosterAutoShift-".time().'-'.uniqid();

        if(empty($rosterStartDate) || empty($empCode))
            return false;

        $rosterStartDate = date('Y-m-d', strtotime($rosterStartDate));
        $rosterEndDate = (!empty($rosterEndDate))?date('Y-m-d', strtotime($rosterEndDate)):$rosterStartDate;

        $sqlRosterQuery = "INSERT INTO Roster_schema(rosterName,Emp_Code,created_on,updated_on,start_rost,end_rost,auto_period,auto_period_flag) VALUES('".$rosterName."','".$empCode."',GETDATE(),GETDATE(),'".$rosterStartDate."','".$rosterEndDate."','1','1')";
        $responseResult = query($query,$sqlRosterQuery,$pa,$opt,$ms_db);

        if($responseResult)
            return $rosterName;

    }

    //check user roster existance
    function checkRosterSchema($empCode, $rosterDate) {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        if(empty($empCode) || empty($rosterDate)){
            return false;
        }

        $checkRosterSql =   "SELECT top 1 * FROM Roster_schema WHERE Emp_Code = '".$empCode."' AND '".$rosterDate."' BETWEEN cast(start_rost AS DATE) AND  CAST(end_rost AS DATE) ORDER BY RostID DESC";
        $checkRosterResult = query($query,$checkRosterSql,$pa,$opt,$ms_db);
        if($num($checkRosterResult) > 0){
            return true;
        }
        return false;
    }

    //Generate CSV from Array
    function exportCSV($headings=false, $rows=false, $filename=false)
    {
        # Ensure that we have data to be able to export the CSV
        if ((!empty($headings)) AND (!empty($rows)))
        {
            # modify the name somewhat
            $name = ($filename !== false) ? $filename . ".csv" : "export.csv";

            # Set the headers we need for this to work
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=' . $name);

            # Start the ouput
            $output = fopen('php://output', 'w');

            # Create the headers
            fputcsv($output, $headings);

            # Then loop through the rows
            foreach($rows as $row)
            {
                # Add the rows to the body
                fputcsv($output, $row);
            }

            # Exit to close the stream off
            exit();
        }

        # Default to a failure
        return false;
    }

    /*function to get PY_OrgMast field value from paycycle table*/
    function getPayCycleOrgMastDt(){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $checkPayCycleOrgMastFieldSql =   "SELECT DISTINCT PY_OrgMast FROM PayCycle WHERE PY_OrgMastDt != ''";
        $checkPayCycleOrgMastFieldResult = query($query,$checkPayCycleOrgMastFieldSql,$pa,$opt,$ms_db);
        if($num($checkPayCycleOrgMastFieldResult) > 0){
            $result = $fetch($checkPayCycleOrgMastFieldResult);
            return $result['PY_OrgMast'];
        }
        return '';
    }

    function getEmployeePayrollCycle($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $empCode = (is_array($empCode)) ? $empCode : trim($empCode);
        $resultArray = array();

        if(is_array($empCode)){
            $empCode = "'" . implode("','", $empCode) . "'";
            $empCodeWhere = "HRDT.Emp_Code IN (".$empCode.")";
        } else {
            $empCodeWhere = "HRDT.Emp_Code = '".$empCode."'";
        }
        $orgMastValue = $this->getPayCycleOrgMastDt();
        //query for get record fro paycycle
        $payCycleQuery = "SELECT DISTINCT HRDT.Emp_Code,HRDT.Comp_Code,PAYC.PY_Start
        FROM HrdTran HRDT,PayCycle PAYC
        WHERE
        HRDT.COMP_CODE=PAYC.PY_COMP AND ".$orgMastValue."=PY_OrgMastDt AND PAYC.PY_TYPE='F' AND ".$empCodeWhere;

        $resultPayCycle = query($query, $payCycleQuery, $pa, $opt, $ms_db);
        if ($num($resultPayCycle)) {
            while($rowPayCycle = $fetch($resultPayCycle)){
                $PY_Start = $rowPayCycle['PY_Start'];
                $resultArray[$rowPayCycle['Emp_Code']] = array("startDate" => $PY_Start, "endDate" => $PY_Start - 1);
            }
        }

        return $resultArray;
    }

    /*get employee reasong and subreason of gatepass request*/
    function getEmployeeGatePassData($reason, $subreason){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $sqlreason="select LOV_Text from LOVMast  where LOV_Field='GateReason' and LOV_Value ='".$reason."'";        
        $resreason=query($query,$sqlreason,$pa,$opt,$ms_db);
        $resData=$fetch($resreason);
        $returnMessage[] = rtrim($resData['LOV_Text'],':');

        $subqry;
        if($reason == '1'){
            $subqry = 'GatePReason';
        }
        else if($reason == '2'){
            $subqry = 'GateOReason';
        }
        else if($reason == '3'){
            $subqry = 'GateHSReason';
        }
        else if($reason == '5' || $subreason == '5'){
            $ssubqry = $row['other_reason'];
        }
         
        $sqlotherreason="select LOV_Text from LOVMast  where 
        LOV_Field='$subqry' and LOV_Value ='".$subreason."'";
        
        $resother=query($query,$sqlotherreason,$pa,$opt,$ms_db);
        $resData1=$fetch($resother);
        $returnMessage[] = rtrim($resData1['LOV_Text'],':');
        if($reason == '5' || $subreason == '5'){
            $returnMessage[] = rtrim($ssubqry);
        }
        return $returnMessage;
    }

    function AddPlayTime($times) {
        $minutes =0;
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;

       
        return sprintf('%02d:%02d', $hours, $minutes);
    }
    
    function getEmployeeOUDetail($empCode, $date, $jsonData)
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;        

        $validateDateFormat = date_parse($date);

        if(empty($empCode) || empty($date) || empty($jsonData) || $validateDateFormat["error_count"] != 0)
            return false;
        
        $date = date('Y-m-d', strtotime($date));
        
        $sql = "SELECT TOP 1 EMP_CODE FROM HrdTran";
        
        $datalevel     = json_decode($jsonData, true);
        
        $sql .= " WHERE (";
        for ($i = 0; $i < count($datalevel); $i++) {
            if ($datalevel[$i]['text'] == "Company") {
                $condition = '';
                for ($b = 0; $b < count($datalevel[$i]['children']); $b++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$b]['id'], 1) . "', ";
                }
                $sql .= "Comp_code IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Business Unit") {
                $condition = '';
                for ($b = 0; $b < count($datalevel[$i]['children']); $b++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$b]['id'], 1) . "', ";
                }
                $sql .= "Busscode IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Location") {
                $condition = '';                   
                for ($l = 0; $l < count($datalevel[$i]['children']); $l++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$l]['id'], 1) . "', ";
                }
                $sql .= "loc_Code IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Working Location") {
                $condition = '';                 
                for ($wl = 0; $wl < count($datalevel[$i]['children']); $wl++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$wl]['id'], 1) . "', ";
                }
                $sql .= "WLOC_code IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Function") {
                $condition = '';              
                for ($f = 0; $f < count($datalevel[$i]['children']); $f++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$f]['id'], 1) . "', ";
                }
                $sql .= "FUNCT_code IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Sub Function") {
                $condition = '';
                for ($sf = 0; $sf < count($datalevel[$i]['children']); $sf++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$sf]['id'], 1) . "', ";
                }
                $sql .= "SFUNCT_code IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Grade") {  
                $condition = '';                  
                for ($g = 0; $g < count($datalevel[$i]['children']); $g++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$g]['id'], 1) . "', ";
                }
                $sql .= "GRD_code IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Employee Type") { 
                $condition = '';                   
                for ($et = 0; $et < count($datalevel[$i]['children']); $et++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$et]['id'], 1) . "', ";
                }
                $sql .= "TYPE_code IN (".RTRIM($condition,', ').") AND ";
            }

            if ($datalevel[$i]['text'] == "Process") {
                $condition = '';
                for ($pr = 0; $pr < count($datalevel[$i]['children']); $pr++) {
                    $condition .= "'" . substr($datalevel[$i]['children'][$pr]['id'], 1) . "', ";
                }
                $sql .= "PROC_code IN (".RTRIM($condition,', ').") AND ";
            }

        }

        $sql .= "Status_Code='01' )  AND Emp_Code = '".$empCode."' AND Trn_Date <= '".$date."' ORDER BY Trn_Date DESC ";
    
        $result = query($query, $sql, $pa, $opt, $ms_db);
        if ($num($result) > 0) {
            return true;
        }
        return false;
    }
}


