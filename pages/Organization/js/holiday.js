function getTables1(tablename,pre,divid,status,name){

        $.ajax({
            type : 'POST',
            data:{type:'init',status:status,table:tablename,pre:pre,name:name },
            url  : 'ajax/holiday_ajax.php',
            beforeSend: function(){
                loading();
            },
            success: function(responseText){
                unloading();
                $("#"+divid).html(responseText);

            }
        });
}

$('#holiDate').datepicker( {
            dateFormat : 'dd/mm/yy'
            
        // duration: 'fast'
        // }).focus(function() {
        //   $(".ui-datepicker-prev, .ui-datepicker-next").remove();
        
        });
      $(function() {
       // alert('1');
        getTables1('CompMast','COMP','company-select','1','a');
        getTables1('BussMast','buss','business-select','1','a');
        getTables1('subBussMast','subBuss','sub-business-select','1','a');
        //getTables('locMast','LOC','location-select','1','a');
        getTables1('WorkLocMast','WLoc','work-location-select','1','a');
        getTables1('FUNCTMast','FUNCT','function-select','1','a');
        getTables1('SubFunctMast','SubFunct','sub-function-select','1','a');
        getTables1('CostMast','cost','cost-master-select','1','a');
        getTables1('PROCMAST','PROC','process-select','1','a');
        getTables1('GrdMast','GRD','grade-select','1','a');
        getTables1('DsgMast','DSG','designation-select','1','a');

     // $('#my-default-text-select').searchableOptionList({
     //            showSelectAll: true,
     //             maxHeight: '250px',
     //              texts: {
     //                searchplaceholder: 'Select Employee'
     //            }
     //        });
     $('#loc').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,

        });
     $('#locdiv').find('.multiselect-selected-text').text('Select Location');
    
   });  
    
    // $('#').multiselect({
    //         includeSelectAllOption: true,
    //         enableFiltering: true,

    //     });
    
    mymultiselect();



$("#search").autocomplete({
        source: "holidaysearch.php", // name of controller followed by function
     }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        var inner_html = '<a>' +
            '<div class="list_item_container">' +
            '<span> ' + item.label + '</span></div></a>';

        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append(inner_html)
            .appendTo( ul );
    };



function showAdvanceFilter(sval,code)
{

    if(sval == '1'){
        $("#showorg").val('0');
         //   $("#emps").hide();
        $("#multisearch").show();
        $("#multisearch1").show();
        $("#multisearch2").show();
        $("#multisearch3").show();
        $('#company-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#business-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#sub-business-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
        //    $('#location-select').multiselect({
        //     includeSelectAllOption: true,
        //     enableFiltering: true
        // });
           $('#work-location-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#function-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#sub-function-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#cost-master-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#process-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#grade-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });
           $('#designation-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
        });

    }
    else{
            $("#showorg").val('1');
           // alert('hello');
           // $("#emps").show();
            $("#multisearch").hide();
            $("#multisearch1").hide();
            $("#multisearch2").hide();
            $("#multisearch3").hide();
    }
    var gadha= ['Select Company','Select Business','Select Sub-Business','Select Work Location','Select Function','Select Sub-Function','Select Cost Master','Select Process','Select Grade', 'Select Designation'];
    var gha = $(gadha).length;
    for(i=0;i<=gha;i++){
        $('.selectKunal').eq(i).find('.multiselect-selected-text').text(gadha[i]);
    }
    if(sval == '1'){
         getSelectedVal();
    }
   
}  

function submitholiday(action){

    var hDate = $("#holiDate").val();
    var loc = $("#loc").val();
    var hname = $("#search").val();
    var comp = $("#company-select").val();
    var bus = $("#business-select").val();
    var subbus = $("#sub-business-select").val();
    var wloc = $("#work-location-select").val();
    var func = $("#function-select").val();
    var subfunc = $("#sub-function-select").val();
    var cost = $("#cost-master-select").val();
    var proc = $("#process-select").val();
    var grd = $("#grade-select").val();
    var desg = $("#designation-select").val();
    var type="insert";
    var oldloc = [];
    var oldcomp = [];
    var oldbus = [];
    var oldsubbus = [];
    var oldwloc = [];
    var oldfunc = [];
    var oldsubfunc = [];
    var oldcost = [];
    var oldproc = [];
    var oldgrd = [];
    var olddesg = [];
    var count =0;
    var additionalholi;
    var holimand;
    if($('#additionalholiday').is(':checked')){
        additionalholi = '1';
    }
    else{
        additionalholi = '0';
    }
    holimand = $('input[name=holidaymandatory]:radio:checked').val();
   // alert(additionalholi);
    //alert(holimand);

    if(action != "add"){
        
        $('#loc').siblings().find('li.active input').each(function(){
            oldloc.push($(this).val());
        })
        if(oldloc[1]=='multiselect-all'){
        loc='';
        }
        else{
            loc=oldloc;
        }
        $('#company-select').siblings().find('li.active input').each(function(){
            oldcomp.push($(this).val());
        })
        if(oldcomp[1]=='multiselect-all'){
        comp='';
        }
        else{
            comp=oldcomp;
        }
        $('#business-select').siblings().find('li.active input').each(function(){
            oldbus.push($(this).val());
        })
        if(oldbus[1]=='multiselect-all'){
        bus='';
        }
        else{
            bus=oldbus;
        }
        $('#sub-business-select').siblings().find('li.active input').each(function(){
            oldsubbus.push($(this).val());
        })
        if(oldsubbus[1]=='multiselect-all'){
        subbus='';
        }
        else{
            subbus=oldsubbus;
        }
        $('#work-location-select').siblings().find('li.active input').each(function(){
            oldwloc.push($(this).val());
        })
        if(oldwloc[1]=='multiselect-all'){
        wloc='';
        }
        else{
            wloc=oldwloc;
        }
        $('#function-select').siblings().find('li.active input').each(function(){
            oldfunc.push($(this).val());
        })
        if(oldfunc[1]=='multiselect-all'){
        func='';
        }
        else{
            func=oldfunc;
        }
        $('#sub-function-select').siblings().find('li.active input').each(function(){
            oldsubfunc.push($(this).val());
        })
        if(oldsubfunc[1]=='multiselect-all'){
        subfunc='';
        }
        else{
            subfunc=oldsubfunc;
        }
        $('#cost-master-select').siblings().find('li.active input').each(function(){
            oldcost.push($(this).val());
        })
        if(oldcost[1]=='multiselect-all'){
        cost='';
        }
        else{
            cost=oldcost;
        }
        $('#process-select').siblings().find('li.active input').each(function(){
            oldproc.push($(this).val());
        })
        if(oldproc[1]=='multiselect-all'){
        proc='';
        }
        else{
            proc=oldproc;
        }
        $('#grade-select').siblings().find('li.active input').each(function(){
            oldgrd.push($(this).val());
        })
        if(oldgrd[1]=='multiselect-all'){
        grd='';
        }
        else{
            grd=oldgrd;
        }
        $('#designation-select').siblings().find('li.active input').each(function(){
            olddesg.push($(this).val());
        })
        if(olddesg[1]=='multiselect-all'){
        desg='';
        }
        else{
            desg=olddesg;
        }
}
else{
     var locval = $('#locdiv').find('.multiselect-selected-text').text();
    var locres = locval.substring(0, 3);
    if(locres=='All'){
       // alert (meraChecker1);
     loc = '';
    }
    var compval = $('#compdiv').find('.multiselect-selected-text').text();
    var compres = compval.substring(0, 3);
    if(compres=='All'){
       // alert (meraChecker1);
     comp = '';
    }

 var busval = $('#busdiv').find('.multiselect-selected-text').text();
    var busres = busval.substring(0, 3);
    if(busres=='All'){
       // alert (meraChecker1);
     bus = '';
    }
     var subbusval = $('#subbusdiv').find('.multiselect-selected-text').text();
    var subbusres = subbusval.substring(0, 3);
    if(subbusres=='All'){
       // alert (meraChecker1);
     subbus = '';
    }
     var wlocval = $('#wlocdiv').find('.multiselect-selected-text').text();
    var wlocres = wlocval.substring(0, 3);
    if(wlocres=='All'){
       // alert (meraChecker1);
     wloc = '';
    }
     var funcval = $('#funcdiv').find('.multiselect-selected-text').text();
    var funcres = funcval.substring(0, 3);
    if(funcres=='All'){
       // alert (meraChecker1);
     func = '';
    }
     var subfuncval = $('#subfuncdiv').find('.multiselect-selected-text').text();
    var subfuncres = subfuncval.substring(0, 3);
    if(subfuncres=='All'){
       // alert (meraChecker1);
     subfunc = '';
    }
     var costval = $('#costdiv').find('.multiselect-selected-text').text();
    var costres = costval.substring(0, 3);
    if(costres=='All'){
       // alert (meraChecker1);
     cost = '';
    }

var grdval = $('#grddiv').find('.multiselect-selected-text').text();
    var grdres = grdval.substring(0, 3);
    if(grdres=='All'){
       // alert (meraChecker1);
     grd = '';
    }
    var procval = $('#procdiv').find('.multiselect-selected-text').text();
    var procres = procval.substring(0, 3);
    if(procres=='All'){
       // alert (meraChecker1);
     proc = '';
    }
    var dsgval = $('#desgdiv').find('.multiselect-selected-text').text();
    var dsgres = dsgval.substring(0, 3);
    if(dsgres=='All'){
       // alert (meraChecker1);
     desg = '';
    }
}
   
    if(hDate == "")
    {
        toasterrormsg("Select To Holiday Date");
        return false;
    }
    if(hname == "")
    {
        toasterrormsg("Enter Holiday Description");
        return false;
    }
        $.ajax({
            type:"POST",
            url:"ajax/holiday_ajax.php",
            data:{type:type, hDate:hDate, loc:loc, hname:hname, comp:comp, bus:bus, subbus:subbus, wloc:wloc
            , func:func, subfunc:subfunc, cost:cost, proc:proc, grd:grd, desg:desg,action:action,additionalholi:additionalholi,holimand:holimand},
            success: function(result){
                if(result ==1){
                    toastmsg("Successfully Holiday inserted");
                    location.reload();
                }
                else if(result ==3){
                    toastmsg("Successfully Holiday updated");
                    location.reload();
                }
                else if(result ==4){
                    toasterrormsg("Error in updation");
                }
                else{
                     toasterrormsg("Error in insertion");
                }
            }
        });
   


}
function mymultiselect(){
$('a.multiselect-all').click(function(){
    var meraChecker = $(this).find('span').hasClass('checked')
if(meraChecker==true){
 $('.checker').children('span').addClass('checked'); 
}
else{
 $('.checker').children('span').removeClass('checked'); 
}    
    
});
}



/*--------- Start Add/Edit Holiday ----------*/
function holiday_action(id,action){

    if(action == 'edit'){

        var datastring = 'id=' + id + '&action=' + action;
        $.ajax({
            type:"POST",
            url:"ajax/holiday_ajax.php",
            data:{type:'checkstatus', id:id},
            success: function(result){
                if(result ==1){
                    $.ajax({
                        type: "POST",
                        url: "ajax/edit/edit_holiday_ajax.php",
                        data: datastring,
                        success: function (html) {

                            $('#modalbody').html(html);

                            $('#holidaypopup').modal({
                                show: true

                            });
                            // var loc_val = $('#hiddenloc').val();
                            // var split_loc = loc_val.split(",");
                            // for(i=0;i<split_loc.length;i++){
                            //     alert(split_loc[i]);
                            //     $('[value="split_loc['+i+']"]').prop('checked',true);
                            // }
                           getSelectedVal();

                            $('#close').click(function() {
                                location.reload();
                            });
                        }
                    });
                }
                else{
                     toasterrormsg("Please First Active Holiday");
                }
            }
        });
       

    }
    else{
        $.ajax({
            type: "POST",
            url: "ajax/add/add_holiday_ajax.php",
            data: datastring,
            success: function (data) {

                $('#modalbody').html(data);

                $('#holidaypopup').modal({
                    show: true

                });
                $('#close').click(function() {
                    location.reload();
                });
            }
        });


    }


}
/*--------- End Add/Edit Company ----------*/

function getSelectedVal(){

var y,kunal;
$('.hiddenloc').each(function(){
y = $(this).val();
kunal = y.split(",");
var faltuAray = [];
var xi = kunal.length;
for (i =0 ; i < xi; i++) {
 var kd = kunal[i];   
$(this).siblings('.multiselect-native-select').find('.multiselect-container [value="'+kd+'"]').prop('checked',true).addClass('bhondu'+i);
$(this).siblings('.multiselect-native-select').find('.multiselect-container [value="'+kd+'"]').parents('li').addClass('active');
var faltuObj = $('.bhondu'+i).parent().text(); 

faltuAray.push(faltuObj)    
}
var faltuLength = faltuAray.length;
$(this).siblings('.multiselect-native-select').find('button').attr('title',faltuAray);
$(this).siblings('.multiselect-native-select').find('button span').text(faltuLength+' selected');
//alert("aaa"+kunal[0]+"bbbb");
//console.log(kunal[0]);
//console.log(xi);
// if(kunal[0]==" "||kunal[0]==null||kunal[0]==undefined){
//     console.log('12121');
// }
if(xi==1 && (kunal[0]==" "||kunal[0]==null||kunal[0]==undefined) ){
    //console.log('monika');
$(this).siblings('.multiselect-native-select').find('.multiselect-container input').prop('checked',true).addClass('bhondu');
$(this).siblings('.multiselect-native-select').find('.multiselect-container input').parents('li').addClass('active');
$(this).siblings('.multiselect-native-select').find('.multiselect-container label').text();
$(this).siblings('.multiselect-native-select').find('button span').text('All selected');
}
y =""
kunal.length = 0
})

}

function changeStatus(status,id){

$.ajax({
            type:"POST",
            url:"ajax/holiday_ajax.php",
            data:{type:'statusUpdate', status:status, id:id},
            success: function(result){
                if(result ==1){
                    toastmsg("Successfully Updated");
                    location.reload();
                }
                else{
                     toasterrormsg("Error in updation");
                }
            }
        });

}

$(document).ready(function(){
    $('.k-sd1').find('.btn-group').children('button').addClass('kpd')
    $('.k-sd1').find('ul').addClass('ksd')
    var xp =[];
    $('.ksd').children('li').mouseup(function(){

       setTimeout(function(){ 
            var kd = $('.ksd').children('.active').text()
            xp.push(kd)
            xp1 = $('.ksd').children('.active').length
            xp2 = $('.ksd').children('li').length
            $('.kpd').attr('knlgpt',xp)
            $('.kpd').text(xp1 + 'Selected')
            console.log('sdsds' + xp2)
            if( xp1 === xp2){ $('.kpd').text('All Selected')} 
             xp.length = 0  
        }, 10); 
        
    })

});