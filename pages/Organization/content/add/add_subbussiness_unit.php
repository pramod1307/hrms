<form action="#" id="form_sample_1" class="form-horizontal">
                  <div class="form-body">
                      <div class="alert alert-danger display-hide">
                          <button class="close" data-close="alert"></button> 
                          You have some form errors. Please check below.
                      </div>
                      <div class="alert alert-success display-hide">
                          <button class="close" data-close="alert"></button>
                          Business Unit Created And Added successfully!
                      </div>
                     <div class="form-group">
                      <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="control-label">Sub Business Unit Code <span class="required">
                            * </span>
                            </label></br>
                            <input type="text" name="subbussCode" class="form-control"/>
                        </div>
                           <div class="col-md-4">                          
                            <label class="control-label">Sub Business Unit Name <span class="required">
                            * </span>
                            </label></br>
                              <input name="subbussName" type="text" class="form-control"/>
                           </div>
                           <div class="col-md-4">                          
                            <label class="control-label">Sub Business Unit Head Name<span class="required">
                            * </span>
                            </label></br>
                              <input name="subbussHname" type="text" class="form-control"/>
                           </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="control-label">Reporting Business Unit<span class="required">
                            * </span>
                            </label></br>
                            <input type="text" name="subbussReport" class="form-control"/>
                        </div>
                           <div class="col-md-4">                          
                            <label class="control-label">About Sub Business Unit<span class="not-required">
                             </span>
                            </label></br>
                              <input name="subbussAbt" type="text" class="form-control"/>
                           </div>
                           <div class="col-md-4">                          
                            <label class="control-label">Sub Business Address<span class="not-required">
                             </span>
                            </label></br>
                              <textarea name="subbussAddr" type="text" class="form-control" cols="10" rows="1"></textarea>
                           </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="control-label">Sub Business City <span class="not-required">
                             </span>
                            </label></br>
                            <input type="text" name="subbussCity" class="form-control"/>
                        </div>
                           <div class="col-md-4">                          
                            <label class="control-label">Sub Business Pincode No.<span class="required">
                             </span>
                            </label></br>
                              <input name="subbussPin" type="text" class="form-control" placeholder=""/>
                           </div>
                           <div class="col-md-4">                          
                            <label class="control-label">Sub Business State<span class="not-required">
                            </span>
                            </label></br>
                              <input name="subbussState" type="text" class="form-control"/>
                           </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="col-md-4">
                            <label class="control-label">Currency Of Payment<span class="required">
                            * </span>
                            </label></br>
                            <input type="text" name="bussCur" class="form-control"/>
                        </div>
                           
                      </div>
                    </div>
                </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-9 col-md-3">
                                                <button type="submit" class="btn blue">Submit</button>
                                                <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>