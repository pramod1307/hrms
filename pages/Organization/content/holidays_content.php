

<div class="tab-pane" id="tab_4">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Holidays
            </div>

        </div>
        <div class="portlet-body">
            <!-- BEGIN FORM-->
            <div class="table-toolbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">

                            <a href="#holidaypopup" data-toggle="modal" class="btn green" onclick="holiday_action(this.value,'add')">Add New <i class="fa fa-plus"></i></a>
                        </div>
                    </div>




                </div>
            </div>


            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead><tr class='odd gradeX'>
                    <th>Holiday Name </th>
                    <th>Holiday Date </th>
                    <th> Location </th>
                    <th>Status </th>
                    <th>Action</th></tr></thead><tbody>
                <?php
                $i = 1;
                $sqlq="select *,CONVERT(varchar(10), [HDATE], 105) as HDATE from Holidays WHERE DATEPART(YY,HDATE) IN (SELECT DATEPART(YY,FY_START) FROM FinYear WHERE FY_TYPE='L')   ORDER BY H_STATUS DESC  ";
                $resultq=query($query,$sqlq,$pa,$opt,$ms_db);
                if($num($resultq)) {

                    while ($rowq = $fetch($resultq)) {
                        ?>
                        <tr>
                            
                            <td><?php echo $rowq['HDESC'];?></td>
                            <td><?php echo $rowq['HDATE'];?></td>
                            <td><?php   
                                    if(empty($rowq['LOC_CODE']) || $rowq['LOC_CODE'] == NULL || $rowq['LOC_CODE']=='' || $rowq['LOC_CODE']== ' '){
                                        echo "All Locations";
                                    }else{

                                        //echo $rowq['LOC_CODE'];
                                        $sqlloc="select LOC_NAME from LOCMAST where LOCID IN (
                                            ".$rowq['LOC_CODE'].")";

                                        $resultloc=query($query,$sqlloc,$pa,$opt,$ms_db);
                                        while ($rowloc = $fetch($resultloc)){
                                            echo $rowloc['LOC_NAME'].",";
                                        }
                                    }
                                ?></td>
                            <td><input type="button" id="stid" class="btn btn-default"
                            value='<?php if($rowq['H_status']=="1"){ echo"Active"; }else{ echo"Inactive"; } ?>'
                            onclick="changeStatus('<?php echo $rowq['H_status'] ?>','<?php echo $rowq['HolidayID']; ?>')" /></td>
                             <td><button type="button" class="btn btn-block blue" id="edit_update<?php echo $rowq['HolidayID']?>" value="<?php echo $rowq['HolidayID']; ?>" onclick="holiday_action(this.value,'edit')">Edit</button>
                            </td>
                        </tr>

                        <?php
                        $i++;
                    }
                }

                ?>

                </tbody>
            </table>




            <!-- END FORM-->
        </div>


    </div>
</div>
<div class="modal fade bs-modal-lg" id="holidaypopup" data-backdrop="static" data-keyboard="false">
    <div  tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="modalbody">


            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

