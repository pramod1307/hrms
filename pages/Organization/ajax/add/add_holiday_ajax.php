<?php

//include "../../db_conn.php";
include ('../../../db_conn.php');
include ('../../../configdata.php');
?>
<style type="text/css">
    ul.multiselect-container.dropdown-menu li:nth-child(1) {
    padding: 0;
}
    ul.multiselect-container.dropdown-menu li {
    padding: 0 0 0 20px;
}
</style>
<form action="#" id="form_sample_1" method="post"  class="form-horizontal" enctype="multipart/form-data">
    <div class="modal-header portlet box blue">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title white-txt"><b>New Holiday</b> </h4>
    </div>
    <div class="modal-body">
        <div class="form-body">

              <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 control-label">Holiday Date</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control input-medium" name="holiDate" id="holiDate" placeholder="dd/mm/yy">
                            </div>
                        <label class="col-md-2 control-label">Location</label>
                            <div class="col-md-3 k-sd1" id="locdiv">
                                <select id="loc" name="multiselect[]" multiple="multiple" style="width: 300px">
                                       <?php 
                                            $sql1="select LOCID,LOC_NAME,LOC_CODE from LOCMAST order by LOC_NAME"; 
                                            $result1 = query($query,$sql1,$pa,$opt,$ms_db);
                                            while($row1 = $fetch($result1)){ ?> 
                                                <option value="<?php echo $row1['LOCID'] ?>">
                                                    <?php echo $row1['LOC_NAME']; ?>
                                                </option>
                                                <?php }?>
                                </select>
                            </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 control-label">Holiday Name</label>
                            <div class="col-md-3" >
                                <div class="Search">
                                    <input type="text" class="form-control" name="term" id="search">
                                </div>
                            </div>
                            <label class="col-md-2 control-label">Is Additional Holiday</label>
                                                <div class="col-md-3" >
                                                    <div>
                                                        <label ><input type="checkbox" class="icheck" name="additionalholiday" id="additionalholiday"  value="1" >
                                                        </label>
                                       
                                                    </div>
                                                </div>
                       
                    </div>
                                        <div class="row">
                                             <label class="col-md-2 control-label">Is Holiday </label>
                        <div class="col-md-3" >
                                <label >
                                <input type="radio" class="icheck" name="holidaymandatory" id="Mandatory" value="1" checked>Mandatory
                                </label>
                                <label>
                                <input type="radio" class="icheck" name="holidaymandatory" id="Optional"  value="0" >Optional
                                </label>
                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3" >
                                                <div class="col-md-1" style="margin-top: 25px;margin-left: 750px">
                                                        <button type="button" id="showorg" value="1" class="btn green" onclick="showAdvanceFilter(this.value,'<?php echo $code;?>');">Advanced Filter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="advancefilter">
                                <div class="form-group" id="multisearch" style="display: none">    
                                    <div class="col-md-12" >
                                        <label class="col-md-1 control-label" style="padding-top: 30px;">Company</label>
                                        <div class="col-md-3 selectKunal" id="compdiv" style="margin-top: 25px;">
                                            <select id="company-select" name="multiselect[]" multiple="multiple" style="width: 300px">
                                            </select>
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-top: 30px;">Business</label>
                                        <div class="col-md-3 selectKunal" id="busdiv" style="margin-top: 25px;">
                                            <select id="business-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-top: 30px;">Sub-business</label>
                                        <div class="col-md-3 selectKunal" id="subbusdiv" style="margin-top: 25px;">
                                            <select id="sub-business-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="form-group" id="multisearch1" style="display: none">    
                                    <div class="col-md-12" >
                                    <label class="col-md-1 control-label" style="padding-top: 30px;">Work Location</label>
                                        <div class="col-md-3 selectKunal" id="wlocdiv" style="margin-top: 25px;">
                                            <select id="work-location-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-top: 30px;">Function</label>
                                        <div class="col-md-3 selectKunal" id="funcdiv" style="margin-top: 25px;">
                                            <select id="function-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-top: 30px;">Sub-function</label>
                                        <div class="col-md-3 selectKunal" id="subfuncdiv" style="margin-top: 25px;">
                                            <select id="sub-function-select" name="multiselect[]" multiple="multiple" style="width: 300px">
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group" id="multisearch2" style="display: none">    
                                    <div class="col-md-12" >
                                    <label class="col-md-1 control-label" style="padding-top: 30px;">Cost</label>
                                        <div class="col-md-3 selectKunal" id="costdiv" style="margin-top: 25px;">
                                            <select id="cost-master-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-top: 30px;">Process</label>
                                        <div class="col-md-3 selectKunal" id="procdiv" style="margin-top: 25px;">
                                            <select id="process-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-top: 30px;">Grade</label>
                                        <div class="col-md-3 selectKunal" id="grddiv" style="margin-top: 25px;">
                                            <select id="grade-select" name="multiselect[]" multiple="multiple" style="width: 300px">
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group" id="multisearch3" style="display: none">    
                                    <div class="col-md-12" >
                                    <label class="col-md-1 control-label" style="padding-top: 30px;">Designation</label>
                                        <div class="col-md-3 selectKunal" id="desgdiv" style="margin-top: 25px;">
                                            <select id="designation-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                                                              
                                    </div>
                                </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn green" id="subholi" onclick="submitholiday('add');"><i class="fa fa-check"></i>Save
        </button>
    </div>
</form>
<script src="js/holiday.js"></script>