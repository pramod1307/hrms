<?php

/**
 * Pramod Kumar Sharma
 * calculate all events and save into database single table
 */
date_default_timezone_set('Asia/Kolkata');
require_once "global_class.php";
require_once "Attendance/Events/attendance-class-v2.php";

class AttendaceClass
{
    
    var $empCode;
    var $todayDateTime;
    var $todayDate;
    var $isMarkPastattendance;
    var $markPastattendanceStatus;
    var $saveRecords;
    var $actualShift;
    var $scheduleShift;
    var $shiftId;
    var $ShiftStart;
    var $ShiftEnd;
    var $tableName;
    var $tableFields;
    var $tableValues;
    var $globalClassObj;
    var $attendanceClassObj;
    var $otTime;
    var $status;
    var $statustitle;
    var $Lateflag;
    var $Earlyflag;
    var $remark;
    var $workingHours;
    var $Shift_MFrom;
    var $Shift_MTo;
    var $Shiftmendatryhrs;
    var $lateMinutes;
    var $earlyMinutes;
    var $overStayTime;
    var $shiftPatterns;
    var $tempCurrentDateYearForHoliday;
    var $tempCurrentDateYearForLeave;
    var $tempCurrentDateYearForOD;
    var $holidaysDates;
    var $stepofDays;
    var $dateFormat;
    var $dateTimeFormat;
    var $hoursFormat;
    var $setLeaveTransactions;
    var $leaveDates;
    var $setODTransactions;
    var $insertQuery;
    var $finalStatus;
    var $punchRecord;
    var $punchStatus;
    var $htmlclass;
    var $markpast_remark;
    var $isWeeklyOffOrHoliday;
    var $edlaPolicies;
    var $isSchedular;
    var $isAutoShiftEmployee;
    var $autoShiftConfiguration;
    var $previoudDayShiftDetail;
    var $isOtExist;
    var $OT_StartTime;
    var $OT_EndTime;
    var $OT_TimeArray;
    var $companyCode;
    var $compoOffWorkHours;
    var $compoOffTimeArray;
    var $setOptionsInTemp;
    var $setOptionsInTempStatus;
    var $_leaveName;
    var $_masterDetail;
    var $punchStatusPH;
    var $legendId;
    var $isInSeparationPeriod;
    var $payrollcycle;
    
    public static $counter = 0;
    
    function __construct()
    {
        $this->tableName            = 'attendancesch';
        $this->tableNameLegends     = 'attendanceLegendMasters';
        $this->tableNameLeaveMaster = 'leave_master';
        
        $this->tableFields = array(
            'Emp_Code',
            'date',
            'Schedule_Shift',
            'Actual_Shift',
            'Shift_start',
            'Shift_end',
            'Intime',
            'Outtime',
            'Latemin',
            'Earlymin',
            'Overstay',
            'Overtime',
            'Workinghrs',
            'Leavetype',
            'LeaveTypeText',
            'Leavestatus',
            'leaveNature',
            'Shiftmendatryhrs',
            'Lateflag',
            'Earlyflag',
            'status',
            'Lateremark',
            'statustitle',
            'is_mark_past',
            'mark_past_id',
            'shiftId',
            'markPastStatus',
            'markpast_remark',
            'isWeeklyOff',
            'isHoliday',
            'holidayTitle',
            'odStatus',
            'odStartTime',
            'odEndTime',
            'odReason',
            'odType',
            'final_status',
            'html_class',
            'update_time',
            'legendId'
        );
    }
    
    public function initializeObject($options)
    {
        $this->globalClassObj     = new global_class();
        $this->attendanceClassObj = new ProcessAttendance();
        
        $this->todayDate                 = '';
        $this->isMarkPastattendance      = '';
        $this->markPastattendanceStaus   = '';
        $this->markpast_remark           = '';
        $this->saveRecords               = '';
        $this->actualShift               = '';
        $this->scheduleShift             = '';
        $this->shiftId                   = '';
        $this->ShiftStart                = '';
        $this->ShiftEnd                  = '';
        $this->otTime                    = '';
        $this->Lateflag                  = '';
        $this->Earlyflag                 = '';
        $this->remark                    = '';
        $this->workingHours              = '';
        $this->Shift_MFrom               = '';
        $this->Shift_MTo                 = '';
        $this->Shiftmendatryhrs          = '';
        $this->lateMinutes               = '';
        $this->earlyMinutes              = '';
        $this->overStayTime              = '';
        $this->shiftPatterns             = '';
        $this->insertQuery               = '';
        $this->finalStatus               = '';
        $this->htmlclass                 = 'red circle-legend';
        $this->isWeeklyOffOrHoliday      = FALSE;
        $this->stepofDays                = '+1 day';
        $this->hoursFormat               = 'H:i';
        $this->dateFormat                = 'Y-m-d';
        $this->dateTimeFormat            = 'Y-m-d H:i';
        $this->edlaPolicies              = array();
        $this->empCode                   = $options['empCode'];
        $this->todayDateTime             = $options['todayDateTime'];
        $this->isSchedular               = (isset($options['isSchedular']) && $options['isSchedular'] == true) ? true : false;
        $this->saveRecords               = array();
        $this->isAutoShiftEmployee       = FALSE;
        $this->autoShiftConfiguration    = array();
        $this->status                    = "A";
        $this->statustitle               = "Absent";
        $this->leaveDates                = array();
        $this->setODTransactions         = array();
        $this->previoudDayShiftDetail    = array();
        $this->OT_StartTime              = array();
        $this->OT_EndTime                = array();
        $this->OT_TimeArray              = array();
        $this->companyCode               = (isset($options['companyCode'])) ? $options['companyCode'] : '';
        $this->compoOffWorkHours         = "00:00";
        $this->compoOffTimeArray         = array();
        $this->_leaveName["shortStatus"] = 'L';
        $this->_leaveName["fullStatus"]  = 'Leave';
        $this->_masterDetail             = array();
        $this->punchStatusPH             = '';
        $this->legendId                  = 0;
        $this->payrollcycle              = array();
        $this->isInSeparationPeriod      = FALSE;
        
        /* if (!empty($options['isInternalLoop']) && $options['isInternalLoop'] == 'scheuler') {
        $this->setOptionsInTemp = $options;
        $this->checkAndUpdatePreviousDatedPunch();
        } */
        
        $this->_setLegendDetailByMaster();
        $this->_setLeaveNameStatus();
        $this->isEmployeeInSeprationPeriod();
        
        if ($this->isInSeparationPeriod !== "SeprationPeriodOver") {
            if (!$this->isInSeparationPeriod) {
                $this->getEmployeeShiftDetail();
                $this->setEDLAPolicy();
                $this->isWeeklyOff();
                $this->checkHolidays();
                $this->getEmployeeAttendance();
                $this->getLeaves();
                $this->getODRequest();
                $this->checkMarkPastAttendance();
            }
            $this->saveResult();
        }
    }
    
    public function prd($record)
    {
        if (is_array($record)) {
            echo '<pre>';
            print_r($record);
            echo '</pre>';
        } else {
            echo '<pre>';
            var_dump($record);
            echo '</pre>';
        }
        exit();
    }
    
    // First day of the year.
    public function getFirstDateOfGivenYear()
    {
        //return date('Y', strtotime($this->todayDateTime)) . "-01-01";
        return $this->cleanDateFormat($this->todayDateTime);
    }
    
    // Last day of the year.
    public function lastDateOfGivenYear()
    {
        //return date('Y', strtotime($this->todayDateTime)) . "-12-31";
        return $this->cleanDateFormat($this->todayDateTime);
    }
    
    // First day of the month.
    public function getFirstDateOfGivenMonth()
    {
        return date('Y-m-01', strtotime($this->todayDateTime));
    }
    
    // Last day of the month.
    public function lastDateOfGivenMonth()
    {
        return date('Y-m-t', strtotime($this->todayDateTime));
    }
    
    public function convertDateTimeToTimeStamp($dateTime)
    {
        return (empty($dateTime)) ? '' : (!is_numeric($dateTime)) ? strtotime($dateTime) : $dateTime;
    }
    
    public function cleanDateFormat($dateTime)
    {
        return (!empty($dateTime)) ? date($this->dateFormat, $this->convertDateTimeToTimeStamp($dateTime)) : "";
    }
    
    public function cleanTimeFormat($time)
    {
        return (!empty($time)) ? date($this->hoursFormat, $this->convertDateTimeToTimeStamp($time)) : "";
    }
    
    public function cleanDateTimeFormat($dateTime)
    {
        return (!empty($dateTime)) ? date($this->dateTimeFormat, $this->convertDateTimeToTimeStamp($dateTime)) : "";
    }
    
    public function getMinutes($OutTime, $InTime)
    {
        return (abs(strtotime($OutTime) - strtotime($InTime)) / 60);
    }
    
    public function isDifferentYear()
    {
        $yearOfTodayDate = date('Y', strtotime($this->cleanDateFormat($this->todayDateTime)));
        return (!empty($this->tempCurrentDateYear) && $yearOfTodayDate == $this->tempCurrentDateYear) ? false : true;
    }
    
    public function getShiftMandatoryHours()
    {
        $dateDiff = intval((strtotime($this->Shift_MFrom) - strtotime($this->Shift_MTo)) / 60);
        $hours    = ((0 - intval($dateDiff / 60)) > 0) ? 0 - intval($dateDiff / 60) : 0;
        $minutes  = ((0 - $dateDiff % 60) > 0) ? 0 - $dateDiff % 60 : 0;
        return str_pad($hours, 2, '0', STR_PAD_LEFT) . ":" . str_pad($minutes, 2, '0', STR_PAD_LEFT);
    }
    
    public function getLateMinutes()
    {
        return $this->calculateLateEarly('lateComing');
    }
    
    public function getEarlyMinutes()
    {
        return $this->calculateLateEarly('earlyGoing');
    }
    
    public function getTimeDiff($firstTime, $lastTime)
    {
        $firstTime = strtotime($firstTime);
        $lastTime  = strtotime($lastTime);
        $timeDiff  = $lastTime - $firstTime;
        return $timeDiff;
    }
    
    public function setPucnhStatus($punchStatus)
    {
        $this->punchStatus[$this->cleanDateFormat($this->todayDateTime)] = $punchStatus;
    }
    
    public function isPunchStatusPresentHalfDay()
    {
        return (isset($this->punchStatus[$this->cleanDateFormat($this->todayDateTime)]) && $this->punchStatus[$this->cleanDateFormat($this->todayDateTime)] != 'Absent') ? true : false;
    }
    
    public function getPunchStatus()
    {
        return (isset($this->punchStatus[$this->cleanDateFormat($this->todayDateTime)])) ? $this->punchStatus[$this->cleanDateFormat($this->todayDateTime)] : false;
    }
    
    public function setPucnhRecords($punchStatus)
    {
        $this->punchRecord[$this->cleanDateFormat($this->todayDateTime)] = $punchStatus;
    }
    
    public function isPresentByPunch()
    {
        return isset($this->punchRecord[$this->cleanDateFormat($this->todayDateTime)]) ? true : false;
    }
    
    public function isPunchStatusPH()
    {
        return ($this->punchStatusPH != "Absent") ? TRUE : FALSE;
    }
    
    public function isMarkPastAttendance()
    {
        return $this->isMarkPastattendance;
    }
    
    public function getMarkPastAttendanceStatus()
    {
        return (isset($this->markPastattendanceStatus) && !empty($this->markPastattendanceStatus)) ? $this->markPastattendanceStatus : false;
    }
    
    public function isOutOnDuty()
    {
        return (isset($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)])) ? true : false;
    }
    
    public function getOutOnDutyStatus()
    {
        return (isset($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['status'])) ? ($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['status'] == 'Approved' || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['status'] == 'Approved Cancel - Request for cancel' || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['status'] == 'Approved Cancel Rejected') ? "Present" : "Absent" : false;
    }
    
    public function isOutOndutyPresentHF()
    {
        return ($this->getOutOnDutyStatus() == "Present") ? TRUE : FALSE;
    }
    
    public function isLeave()
    {
        return isset($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]) ? true : false;
    }
    
    public function getLeaveStatus()
    {
        return (isset($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['status'])) ? ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['status'] == 'Approved' || $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['status'] == 'Approved Cancel - Request for cancel' || $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['status'] == 'Approved Cancel Rejected') ? "Present" : "Absent" : false;
    }
    
    public function isLeavePresentHF()
    {
        return ($this->getLeaveStatus() == "Present") ? TRUE : FALSE;
    }
    
    public function isCompoOffApproved()
    {
        return ($this->compoOffWorkHours != "00:00" && count($this->compoOffTimeArray) > 0) ? TRUE : FALSE;
    }
    
    public function isEmployeeInSeprationPeriod()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        $this->isInSeparationPeriod = "SeprationPeriodOver";
        
        $getResignationTransactionDateQuery  = "SELECT TOP 1 CAST(Trn_Date AS DATE) Trn_Date FROM HRDTRAN WHERE Status_Code = '04' AND Emp_Code = '" . $this->empCode . "' ORDER BY Trn_Date";
        $getResignationTransactionDateResult = query($query, $getResignationTransactionDateQuery, $pa, $opt, $ms_db);
        
        if ($num($getResignationTransactionDateResult) > 0) {
            $getResignationTransactionDateRow = $fetch($getResignationTransactionDateResult);
            $resignationDate                  = (!empty($getResignationTransactionDateRow['Trn_Date'])) ? $this->cleanDateFormat($getResignationTransactionDateRow['Trn_Date']) : '';
            
            $this->getEmployeePayrollCycleDetail($resignationDate);
            if (!empty($this->payrollcycle)) {                
                if (!empty($resignationDate) && (strtotime($this->cleanDateFormat($this->todayDateTime)) <= strtotime($this->payrollcycle['endDate']))) {
                    $this->isInSeparationPeriod = (strtotime($this->cleanDateFormat($this->todayDateTime)) > strtotime($resignationDate) && strtotime($resignationDate) < strtotime($this->payrollcycle['endDate'])) ? TRUE : FALSE;
                }
            }
        }
    }
    
    public function getEmployeePayrollCycleDetail($resignationDate = '')
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $this->payrollcycle = array();
        
        $recordDate   = (empty($resignationDate)) ? $this->cleanDateFormat($this->todayDateTime) : $resignationDate;
        $orgMastValue = $this->globalClassObj->getPayCycleOrgMastDt();
        
        $actualGradeCode      = "SELECT TOP 1 TRN_DATE,DATEPART(DAY,Trn_Date) Trn_Date,Emp_Code,Grd_Code,PayCycle.PY_Start FROM HrdTran,PayCycle WHERE HrdTran." . $orgMastValue . "=PayCycle.PY_OrgMastDt AND HrdTran.Comp_Code=PayCycle.PY_Comp AND Emp_Code='" . $this->empCode . "' AND Trn_Date <= '" . $recordDate . "' ORDER BY Trn_Date DESC";
        $actualGradeCodeQuery = query($query, $actualGradeCode, $pa, $opt, $ms_db);
        if ($num($actualGradeCodeQuery)) {
            $actualGradeCodeResult           = $fetch($actualGradeCodeQuery);
            $PY_Start                        = $actualGradeCodeResult['PY_Start'];
            $this->payrollcycle['startDate'] = date('Y', strtotime($recordDate)) . '-' . date('m', strtotime($recordDate)) . '-' . $PY_Start;
            $this->payrollcycle['endDate']   = date("Y-m-d", strtotime("+1 month", strtotime($this->payrollcycle['startDate'])));
        }
    }
    
    public function _setLeaveNameStatus()
    {
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $checkLeaveNamesQuery  = "SELECT leave_id, leave_short_name, leave_full_name FROM " . $this->tableNameLeaveMaster . "";
        $checkLeaveNamesResult = query($query, $checkLeaveNamesQuery, $pa, $opt, $ms_db);
        if ($num($checkLeaveNamesResult) > 0) {
            while ($checkLeaveNamesRow = $fetch($checkLeaveNamesResult)) {
                $this->_leaveName[trim($checkLeaveNamesRow['leave_id'])] = array(
                    "shortStatus" => trim($checkLeaveNamesRow['leave_short_name']),
                    "fullStatus" => trim($checkLeaveNamesRow['leave_full_name'])
                );
            }
        }
    }
    
    public function _setLegendDetailByMaster()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $checkLeaveNamesResult = 0;
        
        $getMastersQuery  = "SELECT id,[master],report_legend,calendar_message,[description] FROM " . $this->tableNameLegends . " WHERE is_active = 1";
        $getMastersResult = query($query, $getMastersQuery, $pa, $opt, $ms_db);
        if ($num($getMastersResult) > 0) {
            while ($getMastersRow = $fetch($getMastersResult)) {
                $this->_masterDetail[trim($getMastersRow['master'])] = array(
                    "masterId" => trim($getMastersRow['id']),
                    "master" => trim($getMastersRow['master']),
                    "reportLegend" => trim($getMastersRow['report_legend']),
                    "calendarMessage" => trim($getMastersRow['calendar_message'])
                );
            }
        }
    }
    
    public function _getLegendDetailByMaster($masterCoode, $type)
    {
        if (!isset($this->_masterDetail) && count($this->_masterDetail) <= 0) {
            return FALSE;
        }
        
        switch ($type) {
            case "masterId":
                return (isset($this->_masterDetail[$masterCoode]) && !empty($this->_masterDetail[$masterCoode])) ? $this->_masterDetail[$masterCoode]["masterId"] : '';
                break;
            
            case "master":
                return (isset($this->_masterDetail[$masterCoode]) && !empty($this->_masterDetail[$masterCoode])) ? $this->_masterDetail[$masterCoode]["master"] : '';
                break;
            
            case "reportLegend":
                return (isset($this->_masterDetail[$masterCoode]) && !empty($this->_masterDetail[$masterCoode])) ? $this->_masterDetail[$masterCoode]["reportLegend"] : '';
                break;
            
            case "calendarMessage":
                return (isset($this->_masterDetail[$masterCoode]) && !empty($this->_masterDetail[$masterCoode])) ? $this->_masterDetail[$masterCoode]["calendarMessage"] : '';
                break;
        }
        return '';
    }
    
    public function calculateLateEarly($timeCaluclatefor)
    {
        $dat          = $this->cleanDateFormat($this->todayDateTime);
        $instart1     = $this->cleanTimeFormat($this->saveRecords['Intime']);
        $endtime1     = $this->cleanTimeFormat($this->saveRecords['Outtime']);
        $shiftintime  = $this->cleanTimeFormat($this->ShiftStart);
        $shiftouttime = $this->cleanTimeFormat($this->ShiftEnd);
        $timestamp    = '';
        if ($this->saveRecords['Intime'] == "" || $this->saveRecords['Outtime'] == "") {
            $tworkinghrs = 0;
            $latecomming = 0;
            $earlygoing  = 0;
        } else {
            $puchTimeDifference  = ($this->getTimeDiff($this->saveRecords['Intime'], $this->saveRecords['Outtime']) / 60) / 60;
            $shiftTimeDifference = ($this->getTimeDiff($shiftintime, $shiftouttime) / 60) / 60;
            
            if ($puchTimeDifference <= 0) {
                $endDate = date($this->dateFormat, strtotime($this->cleanDateFormat($this->todayDateTime) . "+1 days"));
            } else {
                $endDate = $this->cleanDateFormat($this->todayDateTime);
            }
            
            if ($shiftTimeDifference <= 0) {
                $endDate1 = date($this->dateFormat, strtotime($this->cleanDateFormat($this->todayDateTime) . "+1 days"));
            } else {
                $endDate1 = $this->cleanDateFormat($this->todayDateTime);
            }
            
            $timestamp   = new DateTime();
            $t1          = $this->cleanDateFormat($this->todayDateTime) . " " . $instart1;
            $t2          = $endDate . " " . $endtime1;
            $dat1        = new DateTime($t1);
            $date2       = new DateTime($t2);
            $interval    = $dat1->diff($date2);
            $tworkinghrs = $interval->h . ":" . $interval->i;
            $t3          = $this->cleanDateFormat($this->todayDateTime) . " " . $shiftintime;
            $dat2        = new DateTime($t3);
            $t4          = $this->cleanDateFormat($this->todayDateTime) . " " . $shiftouttime;
            $dat3        = new DateTime($t4);
            
            if ($dat2 >= $dat1) {
                $latecomming = 0;
            } else {
                $interval    = $dat1->diff($dat2);
                $latecomming = sprintf("%02d", $interval->h) . ":" . sprintf("%02d", $interval->i);
            }
            
            if ($date2 >= $dat3) {
                $earlygoing = 0;
            } else {
                $interval   = $date2->diff($dat3);
                $earlygoing = sprintf("%02d", $interval->h) . ":" . sprintf("%02d", $interval->i);
            }
        }
        
        if ($timeCaluclatefor == 'lateComing') {
            return $latecomming;
        } else if ($timeCaluclatefor == 'earlyGoing') {
            return $earlygoing;
        }
    }
    
    public function getSpentWorkingHours()
    {
        if (!empty($this->saveRecords['Intime']) && !empty($this->saveRecords['Outtime'])) {
            $dateDiff = intval((strtotime($this->saveRecords['Intime']) - strtotime($this->saveRecords['Outtime'])) / 60);
            $hours    = ((0 - intval($dateDiff / 60)) > 0) ? 0 - intval($dateDiff / 60) : 0;
            $minutes  = ((0 - $dateDiff % 60) > 0) ? 0 - $dateDiff % 60 : 0;
            return str_pad($hours, 2, '0', STR_PAD_LEFT) . ":" . str_pad($minutes, 2, '0', STR_PAD_LEFT);
        }
    }
    
    public function isWeeklyOffHoliday()
    {
        return ($this->isWeeklyOff() || $this->isHoliday()) ? TRUE : FALSE;
    }
    
    public function isWorkingOnWeeklyOffHoliday()
    {
        return (($this->isWeeklyOff() || $this->isHoliday()) && ($this->finalStatus == 'POH' || $this->finalStatus == 'POW')) ? TRUE : FALSE;
    }
    
    public function getOverStayTime()
    {
        if (!empty($this->saveRecords['Outtime']) && !empty($this->ShiftStart) && !empty($this->ShiftEnd)) {
            
            if (strtotime($this->saveRecords['Outtime']) > strtotime($this->ShiftEnd)) {
                
                $start_date = new DateTime($this->ShiftEnd);
                
                $since_start = $start_date->diff(new DateTime($this->saveRecords['Outtime']));
                /* echo $since_start->days.' days total<br>';
                echo $since_start->y.' years<br>';
                echo $since_start->m.' months<br>';
                echo $since_start->d.' days<br>';
                echo $since_start->h.' hours<br>';
                echo $since_start->i.' minutes<br>';
                echo $since_start->s.' seconds<br>'; */
                
                return str_pad($since_start->h, 2, "0", STR_PAD_LEFT) . ":" . str_pad($since_start->i, 2, "0", STR_PAD_LEFT);
            }
        }
        return 0;
    }
    
    public function checkAndUpdatePreviousDatedPunch()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $checkDifferentDates       = "SELECT DISTINCT CAST(PunchDate AS DATE) AS uniquePunchDate FROM AttendanceAll WHERE EMP_CODE = '" . $this->empCode . "' AND CAST(UPDATEDON AS DATE) = '" . $this->cleanDateFormat($this->todayDateTime) . "' AND CAST(PunchDate AS DATE) != '" . $this->cleanDateFormat($this->todayDateTime) . "'";
        $checkDifferentDatesResult = query($query, $checkDifferentDates, $pa, $opt, $ms_db);
        if ($num($checkDifferentDatesResult) > 0) {
            while ($checkDifferentDatesRow = $fetch($checkDifferentDatesResult)) {
                $options = array(
                    'empCode' => $this->empCode,
                    'todayDateTime' => $checkDifferentDatesRow['uniquePunchDate'],
                    'isSchedular' => FALSE,
                    'isInternalLoop' => "NoScheduler"
                );
                $this->initializeObject($options);
            }
        }
        
        $this->setOptionsInTemp['isInternalLoop'] = "NoScheduler";
        $this->initializeObject($this->setOptionsInTemp);
    }
    
    public function setEDLAPolicy()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $EDLAQuery                         = "SELECT LOV_Field,LOV_Value FROM LOVMast WHERE LOV_Field LIKE 'EDLA%'";
        $EDLAResult                        = query($query, $EDLAQuery, $pa, $opt, $ms_db);
        $this->edlaPolicies['fullDayTime'] = $this->edlaPolicies['halfDayTime'] = '';
        if ($num($EDLAResult) > 0) {
            while ($EDLARow = $fetch($EDLAResult)) {
                switch ($EDLARow['LOV_Field']) {
                    case 'EDLA_fullDayTime':
                        $this->edlaPolicies['fullDayTime'] = $EDLARow['LOV_Value'];
                        break;
                    
                    case 'EDLA_halfDayTime':
                        $this->edlaPolicies['halfDayTime'] = $EDLARow['LOV_Value'];
                        break;
                }
            }
        }
    }
    
    public function isEmployeeValidForRoster()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $isMappedShiftAllow = FALSE;
        
        $isFlagEmpValidForRosterQuery  = "SELECT LOV_Value FROM LOVMast WHERE LOV_Field = 'isAllowEmpGradesForMappedRoster'";
        $isFlagEmpValidForRosterResult = query($query, $isFlagEmpValidForRosterQuery, $pa, $opt, $ms_db);
        if ($num($isFlagEmpValidForRosterResult) > 0) {
            $isFlagEmpValidForRosterRow = $fetch($isFlagEmpValidForRosterResult);
            if ($isFlagEmpValidForRosterRow['LOV_Value'] == '1') {
                $empGradesForMappedRosterQuery  = "SELECT LOV_Value FROM LOVMast WHERE LOV_Field = 'empGradesForMappedRoster'";
                $empGradesForMappedRosterResult = query($query, $empGradesForMappedRosterQuery, $pa, $opt, $ms_db);
                if ($num($empGradesForMappedRosterResult) > 0) {
                    $empGradesForMappedRosterRow = $fetch($empGradesForMappedRosterResult);
                    $grdCodes                    = trim($empGradesForMappedRosterRow['LOV_Value']);
                    $grdCodes                    = rtrim($grdCodes, ',');
                }
                $gradeQuery       = "SELECT TOP 1 Grd_Code FROM HrdTran WHERE Emp_Code = '" . $this->empCode . "' AND Trn_Date <= '" . $this->cleanDateFormat($this->todayDateTime) . "' AND CAST(Grd_Code AS numeric(16,7)) IN (" . $grdCodes . ") ORDER BY Trn_Date DESC";
                $resultGradeQuery = query($query, $gradeQuery, $pa, $opt, $ms_db);
                if ($num($resultGradeQuery) > 0) {
                    $isMappedShiftAllow = TRUE;
                }
            }
        }
        
        return $isMappedShiftAllow;
    }
    
    public function checkGradeForEmployee()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $grade_flag = FALSE;
        
        $isAllowEDLAQuery  = "SELECT LOV_Value FROM LOVMast WHERE LOV_Field = 'isAllowEDLA'";
        $isAllowEDLAResult = query($query, $isAllowEDLAQuery, $pa, $opt, $ms_db);
        if ($num($isAllowEDLAResult) > 0) {
            $isAllowEDLARow = $fetch($isAllowEDLAResult);
            if ($isAllowEDLARow['LOV_Value'] == '1') {
                $EDLA_LoveMast    = "SELECT LOV_Value FROM LOVMast WHERE LOV_Field = 'EDLA_allow_ids'";
                $resultGradeQuery = query($query, $EDLA_LoveMast, $pa, $opt, $ms_db);
                if ($num($resultGradeQuery) > 0) {
                    $grd_data = $fetch($resultGradeQuery);
                    $grd_code = $grd_data['LOV_Value'];
                }
                $gradeQuery       = "SELECT TOP 1 Grd_Code FROM HrdTran WHERE Emp_Code = '" . $this->empCode . "' AND Trn_Date <= '" . $this->cleanDateFormat($this->todayDateTime) . "' AND CAST(Grd_Code AS numeric(16,7)) IN ($grd_code) ORDER BY Trn_Date DESC";
                $resultGradeQuery = query($query, $gradeQuery, $pa, $opt, $ms_db);
                if ($num($resultGradeQuery) > 0) {
                    $grade_flag = TRUE;
                }
            }
        }
        return $grade_flag;
    }
    
    public function getPreviousDayEmployeeShift()
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $prev_date = date($this->dateFormat, strtotime($this->cleanDateFormat($this->todayDateTime) . ' -1 day'));
        
        $employeeShiftDetailQuery  = "SELECT TOP 1 CAST(CAST('" . $prev_date . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) Shift_From_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $prev_date . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $prev_date . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
END as Shift_To_Date,

CAST(CAST('" . $prev_date . "' AS DATETIME)+ CAST(ShiftMast.Shift_MFrom AS DATETIME) AS DATETIME) Shift_MFrom_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_MFrom AS TIME)> CAST(ShiftMast.Shift_MTo AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $prev_date . "') AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $prev_date . "' AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
END as Shift_MTo_Date,
CAST( CAST(CAST(CAST('" . $prev_date . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftMast.ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [PunchStart],
CASE 
WHEN (CAST(Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $prev_date . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) + CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME) + CAST('" . $this->compoOffWorkHours . "' AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $prev_date . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME)+ CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME) + CAST('" . $this->compoOffWorkHours . "' AS DATETIME)  AS DATETIME)
END as [PunchEnd],
ShiftMast.*, ShiftPatternMast.* FROM Roster_schema, att_roster, ShiftMast, ShiftPatternMast
            WHERE " . $this->setShiftWhereCondition();
        $employeeShiftDetailResult = query($query, $employeeShiftDetailQuery, $pa, $opt, $ms_db);
        if ($num($employeeShiftDetailResult) > 0) {
            $employeeShiftDetailResultRow = $fetch($employeeShiftDetailResult);
            $this->previoudDayShiftDetail = $employeeShiftDetailResultRow;
        }
    }
    
    public function getEmployeeAttendance()
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        //check current employee grade are need to map roster shift
        if ($this->isEmployeeValidForRoster()) {
            
            $isWeeklyHolidayDate = FALSE;
            
            if ($this->isWeeklyOff() || $this->isHoliday()) {
                $isWeeklyHolidayDate = TRUE;
                $this->PunchStart    = $this->cleanDateFormat($this->todayDateTime) . ' 00:00';
                $this->PunchEnd      = $this->cleanDateFormat($this->todayDateTime) . ' 23:59';
                
                
                //get employee last day shift and replace punch end time if C shift found
                $this->getPreviousDayEmployeeShift();
                $currentDateUnixTime                   = strtotime($this->cleanDateFormat($this->todayDateTime));
                $previousDatePunchEndUnixTime          = strtotime($this->cleanDateFormat($this->previoudDayShiftDetail['PunchEnd']));
                $isCurrentAndPunchEndDateUnixTimeEquel = ($currentDateUnixTime == $previousDatePunchEndUnixTime) ? TRUE : FALSE;
                
                if ($isCurrentAndPunchEndDateUnixTimeEquel && isset($this->previoudDayShiftDetail) && count($this->previoudDayShiftDetail) > 0 && strtolower($this->previoudDayShiftDetail['Shift_Code']) == 'c') {
                    $this->PunchStart = date($this->dateTimeFormat, strtotime($this->previoudDayShiftDetail['PunchEnd'] . ' +1 minutes'));
                }
                
                if (($this->isOtExist && !empty($this->OT_StartTime) && !empty($this->OT_EndTime)) || ($this->isCompoOffApproved())) {
                    $this->PunchStart = $this->setMinPunchForOTCompo();
                    $this->PunchEnd   = $this->setMaxPunchForOTCompo();
                }
            }
            
            $employeeAttendanceQuery     = "SELECT TOP 1 CASE WHEN SUM((CASE CHARINDEX('1', LateComing) WHEN 1 THEN 1 ELSE 0 END )) >= 1 THEN 1 ELSE 0 END BUSLATE, MIN(PunchDate) as inTime, CASE WHEN (MIN(PunchDate) = MAX(PunchDate)) THEN NULL ELSE MAX(PunchDate) END outTime FROM AttendanceAll WHERE EMP_CODE = '" . $this->empCode . "' AND CAST(PunchDate AS DATETIME) BETWEEN '" . $this->PunchStart . "' AND '" . $this->PunchEnd . "'";
            $employeeAttendanceResult    = query($query, $employeeAttendanceQuery, $pa, $opt, $ms_db);
            $employeeAttendanceResultRow = $fetch($employeeAttendanceResult);
            
            if ((empty($this->ShiftStart) || !isset($this->ShiftStart)) && (!$this->isWeeklyOff() && !$this->isHoliday())) {
                unset($employeeAttendanceResultRow);
            }
            
            if (count($employeeAttendanceResultRow) > 0 && !empty($this->PunchStart) && !empty($this->PunchEnd)) {
                
                /* Check OT request and add into shiftEndtime+SwEnd_TIme */
                $actualShiftEnd          = $this->ShiftEnd;
                $actualShiftMendatoryEnd = $this->Shift_MTo;
                if (!empty($this->otTime)) {
                    $otTimeArray             = explode(":", $this->otTime);
                    $actualShiftend          = date($this->dateTimeFormat, strtotime('+' . $otTimeArray[0] . ' hour +' . $otTimeArray[1] . ' minutes', strtotime($this->ShiftEnd)));
                    $actualShiftMendatoryEnd = date($this->dateTimeFormat, strtotime('+' . $otTimeArray[0] . ' hour +' . $otTimeArray[1] . ' minutes', strtotime($this->Shift_MTo)));
                }
                
                $this->saveRecords['Intime']  = $this->cleanDateTimeFormat($employeeAttendanceResultRow['inTime']);
                $this->saveRecords['Outtime'] = $this->cleanDateTimeFormat($employeeAttendanceResultRow['outTime']);
                
                
                $attendanceResultData = '';
                if (!$this->checkGradeForEmployee()) {
                    
                    $this->attendanceClassObj->initializeObject(array(
                        'empCode' => $this->empCode,
                        'attDate' => $this->todayDateTime,
                        'inTime' => $this->cleanDateTimeFormat($employeeAttendanceResultRow['inTime']),
                        'outTime' => $this->cleanDateTimeFormat($employeeAttendanceResultRow['outTime']),
                        'shiftName' => $this->Shift_Name,
                        'actual_shiftFrom' => $this->ShiftStart,
                        'actual_shiftTo' => $this->ShiftEnd,
                        'shiftFrom' => $this->ShiftStart,
                        'shiftTo' => $actualShiftEnd,
                        'shiftMFrom' => $this->Shift_MFrom,
                        'shiftMTo' => $actualShiftMendatoryEnd,
                        'ShiftStartSwTime' => $this->ShiftStart_SwTime,
                        'ShiftEndSwTime' => $this->ShiftEnd_SwTime,
                        'lateAllow' => (!empty($this->LateAllow)) ? $this->LateAllow : 0,
                        'lateAllowCycle' => (!empty($this->LateAllowCycle)) ? $this->LateAllowCycle : 0,
                        'lateAllowGPrd' => (!empty($this->LateAllowGPrd)) ? $this->LateAllowGPrd : 0,
                        'erlyAllow' => (!empty($this->ErlyAllow)) ? $this->ErlyAllow : 0,
                        'erlyAllowCycle' => (!empty($this->ErlyAllowCycle)) ? $this->ErlyAllowCycle : 0,
                        'erlyAllowGPrd' => (!empty($this->ErlyAllowGPrd)) ? $this->ErlyAllowGPrd : 0,
                        'mHrsFul' => $this->getMinutes($this->MHrsFul, '00:00:00'),
                        'mHrsHalf' => $this->getMinutes($this->MHrsHalf, '00:00:00'),
                        "BusArrival" => (!empty($employeeAttendanceResultRow['BUSLATE']) && isset($employeeAttendanceResultRow['BUSLATE'])) ? $employeeAttendanceResultRow['BUSLATE'] : 0,
                        "isWeeklyHolidayDate" => $isWeeklyHolidayDate,
                        "isOT" => $this->isOtExist
                    ));
                    
                    $attendanceResultData = $this->attendanceClassObj->getResult();
                } else {
                    if (!empty($employeeAttendanceResultRow['inTime']) && !empty($employeeAttendanceResultRow['outTime'])) {
                        $actualHoursSpent         = strtotime($employeeAttendanceResultRow['outTime']) - strtotime($employeeAttendanceResultRow['inTime']);
                        $hours                    = intval($actualHoursSpent / 3600);
                        $min                      = intval(($actualHoursSpent - ($hours * 3600)) / 60);
                        $spendTimeInOfficeMinutes = $this->getMinutes($hours . ":" . $min . ":00", '00:00:00');
                        $mHrsFullMinutes          = $this->getMinutes($this->edlaPolicies['fullDayTime'], '00:00:00');
                        $mHrsHalfMinutes          = $this->getMinutes($this->edlaPolicies['halfDayTime'], '00:00:00');
                        if ($spendTimeInOfficeMinutes > $mHrsHalfMinutes && $spendTimeInOfficeMinutes >= $mHrsFullMinutes) {
                            //Full day
                            $this->punchStatusPH                       = 'Present';
                            $attendanceResultData['presetStatus']      = 'Present';
                            $attendanceResultData['presetShortStatus'] = 'P';
                        } else if ($spendTimeInOfficeMinutes >= $mHrsHalfMinutes && $spendTimeInOfficeMinutes < $mHrsFullMinutes) {
                            //Half Day
                            $this->punchStatusPH                       = 'Half Day';
                            $attendanceResultData['presetStatus']      = 'Half Day';
                            $attendanceResultData['presetShortStatus'] = 'H';
                        } else {
                            //Absent
                            $this->punchStatusPH                       = 'Absent';
                            $attendanceResultData['presetStatus']      = 'Absent';
                            $attendanceResultData['presetShortStatus'] = 'A';
                        }
                    }
                }
                $this->punchStatusPH                       = $this->statustitle = isset($attendanceResultData['presetStatus']) ? $attendanceResultData['presetStatus'] : '';
                $this->status                              = isset($attendanceResultData['presetShortStatus']) ? $attendanceResultData['presetShortStatus'] : '';
                $this->Lateflag                            = isset($attendanceResultData['isLateComing']) ? $attendanceResultData['isLateComing'] : 0;
                $attendanceResultData['isEarlyGoing']      = isset($attendanceResultData['isEarlyGoing']) ? $attendanceResultData['isEarlyGoing'] : 0;
                $attendanceResultData['attendanceMessage'] = isset($attendanceResultData['attendanceMessage']) ? $attendanceResultData['attendanceMessage'] : '';
                $this->Earlyflag                           = ($this->isOtExist && (strtotime($this->cleanDateTimeFormat($employeeAttendanceResultRow['outTime'])) >= (strtotime($actualShiftEnd) - $this->erlyAllowGPrd))) ? 0 : $attendanceResultData['isEarlyGoing'];
                $this->remark                              = ($this->isOtExist && (strtotime($this->cleanDateTimeFormat($employeeAttendanceResultRow['outTime'])) >= (strtotime($actualShiftEnd) - $this->erlyAllowGPrd)) && $attendanceResultData['isEarlyGoing']) ? '' : $attendanceResultData['attendanceMessage'];
                $this->workingHours                        = isset($attendanceResultData['timeSpanClean']) ? $attendanceResultData['timeSpanClean'] : '';
                $attendanceResultData['presetStatus']      = isset($attendanceResultData['presetStatus']) ? $attendanceResultData['presetStatus'] : '';
                $this->setPucnhRecords($attendanceResultData['presetStatus']);
                $this->setPucnhStatus($attendanceResultData['presetStatus']);
            }
        } else {
            
            $this->isAutoShiftEmployee = TRUE;
            /* Get first punch and last punch of current date because grade are matched with non-roster employees */
            $getFirstLastPunchQuery    = "SELECT EMP_CODE, CARDNO, MIN(PUNCHDATE) as Intime,MAX(PUNCHDATE) as Outtime FROM ATTENDANCEALL WHERE EMP_CODE = '" . $this->empCode . "' AND CAST(PunchDate AS DATE) = '" . $this->cleanDateFormat($this->todayDateTime) . "' GROUP BY CARDNO, EMP_CODE, cast(PUNCHDATE AS DATE)";
            $getFirstLastPunchResult   = query($query, $getFirstLastPunchQuery, $pa, $opt, $ms_db);
            
            if ($num($getFirstLastPunchResult) > 0) {
                $getFirstLastPunchRow         = $fetch($getFirstLastPunchResult);
                $this->saveRecords['Intime']  = (!empty($getFirstLastPunchRow["Intime"])) ? $getFirstLastPunchRow["Intime"] : '';
                $this->saveRecords['Outtime'] = (!empty($getFirstLastPunchRow["Outtime"])) ? $getFirstLastPunchRow["Outtime"] : '';
                
                if (strtotime($this->saveRecords['Intime']) == strtotime($this->saveRecords['Outtime'])) {
                    $this->saveRecords['Outtime'] = '';
                }
                
                //replace employee original shift to autoshift
                $this->getEmployeeShiftDetail();
                if (!empty($this->saveRecords['Intime']) && !empty($this->saveRecords['Outtime'])) {
                    $actualHoursSpent         = strtotime($this->saveRecords['Outtime']) - strtotime($this->saveRecords['Intime']);
                    $hours                    = intval($actualHoursSpent / 3600);
                    $min                      = intval(($actualHoursSpent - ($hours * 3600)) / 60);
                    $spendTimeInOfficeMinutes = $this->getMinutes($hours . ":" . $min . ":00", '00:00:00');
                    $mHrsFullMinutes          = $this->getMinutes($this->edlaPolicies['fullDayTime'], '00:00:00');
                    $mHrsHalfMinutes          = $this->getMinutes($this->edlaPolicies['halfDayTime'], '00:00:00');
                    if ($spendTimeInOfficeMinutes > $mHrsHalfMinutes && $spendTimeInOfficeMinutes >= $mHrsFullMinutes) {
                        //Full day
                        $this->punchStatusPH = $this->statustitle = 'Present';
                        $this->status        = 'P';
                    } else if ($spendTimeInOfficeMinutes >= $mHrsHalfMinutes && $spendTimeInOfficeMinutes < $mHrsFullMinutes) {
                        //Half Day
                        $this->punchStatusPH = $this->statustitle = 'Half Day';
                        $this->status        = 'H';
                    } else {
                        //Absent
                        $this->punchStatusPH = $this->statustitle = 'Absent';
                        $this->status        = 'A';
                    }
                }
                
                if (empty($this->saveRecords['Intime']) && empty($this->saveRecords['Outtime'])) {
                    //Absent
                    $this->punchStatusPH = $this->statustitle = 'Absent';
                    $this->status        = 'A';
                }
                
                $this->Lateflag  = 0;
                $this->Earlyflag = 0;
                $this->remark    = '';
            }
        }
        
        
        //set to blank all attendance major fields, if shift is not defined and some condition are wrong
        if ((empty($this->ShiftStart) || !isset($this->ShiftStart)) && (!$this->isWeeklyOff() && !$this->isHoliday())) {
            
            $this->saveRecords['Intime']  = '';
            $this->saveRecords['Outtime'] = '';
            $this->status                 = "A";
            $this->punchStatus            = $this->punchStatusPH = "Absent";
            $this->punchRecord            = '';
        }
        
        
        if (strtolower($this->companyCode) == 'mial') {
            $MIALOUCheckQuery  = "SELECT ForWhom FROM AttendanceSetup WHERE AttMethodCode = 1 AND AttMethodName = 'MIALAutoAttendance'";
            $MIALOUCheckResult = query($query, $MIALOUCheckQuery, $pa, $opt, $ms_db);
            if ($num($MIALOUCheckResult) > 0) {
                $MIALOUCheckRow = $fetch($MIALOUCheckResult);
                $OUResponse     = $this->globalClassObj->getEmployeeOUDetail($this->empCode, $this->cleanDateFormat($this->todayDateTime), $MIALOUCheckRow['ForWhom']);
                if ($OUResponse) {
                    $this->punchStatusPH = $this->statustitle = 'Present';
                    $this->status        = 'P';
                }
            }
        }
        
        
        if (strtolower($this->companyCode) == 'maxlife') {
            if ($this->saveRecords['Intime'] != '' || $this->saveRecords['Outtime'] != '') {
                $this->punchStatusPH = $this->statustitle = 'Present';
                $this->status        = 'P';
            }
        }
        
        return false;
    }
    
    public function checkEmployeeCompoOffRequest()
    {
        global $flag;
        global $flag1;
        global $lates;
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $compoOffQuery  = "SELECT /*wd_date, CreatedBy,action_status, actual_INtime, actual_OUTtime,DATEDIFF(HOUR, actual_INtime, actual_OUTtime) AS timeDifference,*/
CONVERT(DATETIME, CONVERT(CHAR(8), wd_date, 112) + ' ' + CONVERT(CHAR(8), actual_INtime, 108)) AS ActualCompoInTime,
CASE WHEN DATEDIFF(HOUR, actual_INtime, actual_OUTtime) < 0 THEN CONVERT(DATETIME, CONVERT(CHAR(8), DATEADD(day,1,wd_date), 112) + ' ' + CONVERT(CHAR(8), actual_OUTtime, 108)) ELSE CONVERT(DATETIME, CONVERT(CHAR(8), wd_date, 112) + ' ' + CONVERT(CHAR(8), actual_OUTtime, 108)) END AS
ActualCompoOutTime,CONVERT(varchar(5), DATEADD(minute, DATEDIFF(minute, actual_INtime, actual_OUTtime), 0), 114) workTime FROM compOff WHERE CreatedBy = '" . $this->empCode . "' AND CAST(wd_date AS DATE) = '" . $this->cleanDateFormat($this->todayDateTime) . "'  AND action_status IN (2,5)";
        $compoOffResult = query($query, $compoOffQuery, $pa, $opt, $ms_db);
        if ($num($compoOffResult) > 0) {
            $compoOffRow                              = $fetch($compoOffResult);
            $this->compoOffWorkHours                  = ($compoOffRow['workTime']) ? $compoOffRow['workTime'] : '00:00';
            $this->compoOffTimeArray['StartDateTime'] = ($compoOffRow['ActualCompoInTime']) ? $this->cleanDateTimeFormat($compoOffRow['ActualCompoInTime']) : '00:00';
            $this->compoOffTimeArray['EndDateTime']   = ($compoOffRow['ActualCompoOutTime']) ? $this->cleanDateTimeFormat($compoOffRow['ActualCompoOutTime']) : '00:00';
        }
    }
    
    public function checkEmployeeOTRequest()
    {
        global $flag;
        global $flag1;
        global $lates;
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        $this->isOtExist   = false;
        $combineTimesArray = array();
        
        $selectQuery = "SELECT overTimeHours,OT_Approver,appStatus, OT_Date, OT_StartTime, OT_EndTime,DATEDIFF(HOUR, OT_StartTime, OT_EndTime) AS timeDifference, 
CONVERT(DATETIME, CONVERT(CHAR(8), OT_Date, 112) + ' ' + CONVERT(CHAR(8), OT_StartTime, 108)) AS ActualOTStartTime,
CASE WHEN DATEDIFF(HOUR, OT_StartTime, OT_EndTime) < 0 THEN CONVERT(DATETIME, CONVERT(CHAR(8), DATEADD(day,1,OT_Date), 112) + ' ' + CONVERT(CHAR(8), OT_EndTime, 108)) ELSE CONVERT(DATETIME, CONVERT(CHAR(8), OT_Date, 112) + ' ' + CONVERT(CHAR(8), OT_EndTime, 108)) END AS
ActualOTEndTime FROM overtime_Balances WHERE Emp_Code = '" . $this->empCode . "' AND CAST(OT_Date AS DATE) = '" . $this->cleanDateFormat($this->todayDateTime) . "'";
        $resultQuery = query($query, $selectQuery, $pa, $opt, $ms_db);
        if ($num($resultQuery) > 0) {
            while ($resultArray = $fetch($resultQuery)) {
                $approverArray       = (!empty($resultArray['OT_Approver'])) ? explode(',', $resultArray['OT_Approver']) : '';
                $approverStatusArray = (!empty($resultArray['appStatus'])) ? explode(',', $resultArray['appStatus']) : '';
                if (end($approverStatusArray) == '2' && count($approverArray) == count($approverStatusArray)) {
                    $combineTimesArray[]                  = $resultArray['overTimeHours'];
                    $this->OT_TimeArray['OT_starttime'][] = strtotime($resultArray['ActualOTStartTime']);
                    $this->OT_TimeArray['OT_endtime'][]   = strtotime($resultArray['ActualOTEndTime']);
                }
            }
            if (!empty($combineTimesArray) && count($combineTimesArray) > 0) {
                $this->isOtExist = true;
                
                sort($this->OT_TimeArray['OT_starttime']);
                rsort($this->OT_TimeArray['OT_endtime']);
                
                //$this->OT_StartTime = date($this->dateTimeFormat, strtotime(date($this->dateTimeFormat, $this->OT_TimeArray['OT_starttime'][0]) . ' -1 hours'));
                //$this->OT_EndTime   = date($this->dateTimeFormat, strtotime(date($this->dateTimeFormat, $this->OT_TimeArray['OT_endtime'][0]) . ' +2 hours'));
                $this->OT_StartTime = $this->cleanDateTimeFormat($this->OT_TimeArray['OT_starttime'][0]);
                $this->OT_EndTime   = $this->cleanDateTimeFormat($this->OT_TimeArray['OT_endtime'][0]);
                
                return $this->globalClassObj->AddPlayTime($combineTimesArray);
            }
        }
        
        $this->OT_StartTime = $this->OT_EndTime = '';
        
        return 0;
    }
    
    public function setMinPunchForOTCompo()
    {
        $minPunchArray = array_filter(array(
            (!empty($this->OT_StartTime) && $this->OT_StartTime != "00:00") ? $this->convertDateTimeToTimeStamp($this->OT_StartTime . ' -1 hours') : 0,
            (!empty($this->compoOffTimeArray['StartDateTime']) && $this->compoOffTimeArray['StartDateTime'] != "00:00") ? $this->convertDateTimeToTimeStamp($this->compoOffTimeArray['StartDateTime'] . ' -1 hours') : 0
        ));
        
        sort($minPunchArray);
        
        return (!empty($minPunchArray)) ? $this->cleanDateTimeFormat($minPunchArray[0]) : '';
    }
    
    public function setMaxPunchForOTCompo()
    {
        $maxPunchArray = array_filter(array(
            (!empty($this->OT_EndTime) && $this->OT_EndTime != "00:00") ? $this->convertDateTimeToTimeStamp($this->OT_EndTime . ' +2 hours') : 0,
            (!empty($this->compoOffTimeArray['EndDateTime']) && $this->compoOffTimeArray['EndDateTime'] != "00:00") ? $this->convertDateTimeToTimeStamp($this->compoOffTimeArray['EndDateTime'] . ' +2 hours') : 0
        ));
        
        rsort($maxPunchArray);
        
        return (!empty($maxPunchArray)) ? $this->cleanDateTimeFormat($maxPunchArray[0]) : '';
    }
    
    public function getSetAutoShiftConfiguration()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $autoShiftConfigQuery                                  = "SELECT LOV_Field,LOV_Value FROM LOVMast WHERE LOV_Field = 'autoShiftPunchRadius' OR  LOV_Field = 'shiftCodeForAutoShift'";
        $autoShiftConfigResult                                 = query($query, $autoShiftConfigQuery, $pa, $opt, $ms_db);
        $this->autoShiftConfiguration['autoShiftPunchRadius']  = '00:30';
        $this->autoShiftConfiguration['shiftCodeForAutoShift'] = 'gen';
        if ($num($autoShiftConfigResult) > 0) {
            while ($autoShiftConfigRow = $fetch($autoShiftConfigResult)) {
                switch ($autoShiftConfigRow['LOV_Field']) {
                    case 'autoShiftPunchRadius':
                        $this->autoShiftConfiguration['autoShiftPunchRadius'] = $autoShiftConfigRow['LOV_Value'];
                        break;
                    
                    case 'shiftCodeForAutoShift':
                        $this->autoShiftConfiguration['shiftCodeForAutoShift'] = $autoShiftConfigRow['LOV_Value'];
                        break;
                }
            }
        }
    }
    
    public function setShiftWhereCondition()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $whereCondition           = '';
        //check employee auto period flag off record
        $autoPeriodOffQuery       = "SELECT TOP 1 * FROM Roster_schema WHERE Emp_Code = '" . $this->empCode . "' AND '" . $this->cleanDateFormat($this->todayDateTime) . "' BETWEEN CAST(start_rost AS DATE) AND CAST(end_rost AS DATE) AND auto_period = 0";
        $autoPeriodOffQueryResult = query($query, $autoPeriodOffQuery, $pa, $opt, $ms_db);
        
        if ($num($autoPeriodOffQueryResult) > 0) {
            $whereCondition = " Roster_schema.RosterName= att_roster.roster AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid AND att_roster.shiftMaster = shiftMast.ShiftMastId  AND Roster_schema.Emp_Code = '" . $this->empCode . "' AND '" . $this->cleanDateFormat($this->todayDateTime) . "' BETWEEN CAST(Roster_schema.start_rost AS DATE) AND CAST(Roster_schema.end_rost AS DATE) AND Roster_schema.auto_period = 0 ORDER BY Roster_schema.end_rost DESC";
        } else {
            $whereCondition = " Roster_schema.RosterName= att_roster.roster AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid AND att_roster.shiftMaster = shiftMast.ShiftMastId  AND Roster_schema.Emp_Code = '" . $this->empCode . "' AND DAY('" . $this->cleanDateFormat($this->todayDateTime) . "') BETWEEN DAY(Roster_schema.start_rost) AND DAY(Roster_schema.end_rost) AND Roster_schema.auto_period = 1 AND Roster_schema.end_rost <= '" . $this->cleanDateFormat($this->todayDateTime) . "' ORDER BY Roster_schema.end_rost DESC";
        }
        
        return $whereCondition;
    }
    
    public function getEmployeeShiftDetail()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $this->checkEmployeeCompoOffRequest();
        
        //check and get employee OT time
        $this->otTime = (!$this->checkEmployeeOTRequest()) ? '00:00' : $this->checkEmployeeOTRequest();
        
        if ($this->isAutoShiftEmployee) {
            
            $this->getSetAutoShiftConfiguration();
            
            if (empty($this->saveRecords['Intime']) && $this->saveRecords['Outtime'] == '00:00') {
                $inTime = '00:00';
            } else {
                $inTime = date('H:i', strtotime($this->saveRecords['Intime']));
            }
            
            $employeeShiftDetailQuery = "IF EXISTS ( SELECT ShiftMastId FROM ShiftMast WHERE
                                            Shift_From
                                            BETWEEN CAST( CAST( CAST('" . $inTime . "' AS DATETIME) AS DATETIME) - CAST('" . $this->autoShiftConfiguration['autoShiftPunchRadius'] . "' AS DATETIME) AS TIME )
                                            AND CAST( CAST( CAST('" . $inTime . "' AS DATETIME) AS DATETIME) + CAST('" . $this->autoShiftConfiguration['autoShiftPunchRadius'] . "' AS DATETIME) AS TIME )
                                            )
                                        BEGIN
                                            SELECT TOP 1
                                            CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) Shift_From_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
END as Shift_To_Date,

CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_MFrom AS DATETIME) AS DATETIME) Shift_MFrom_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_MFrom AS TIME)> CAST(ShiftMast.Shift_MTo AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
END as Shift_MTo_Date,
CAST( CAST(CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftMast.ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [PunchStart],
CASE 
WHEN (CAST(Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) + CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME) + CAST('" . $this->compoOffWorkHours . "' AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME)+ CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME) + CAST('" . $this->compoOffWorkHours . "' AS DATETIME)  AS DATETIME)
END as [PunchEnd],
* FROM ShiftMast WHERE
                                            Shift_From
                                            BETWEEN CAST( CAST( CAST('" . $inTime . "' AS DATETIME) AS DATETIME) - CAST('" . $this->autoShiftConfiguration['autoShiftPunchRadius'] . "' AS DATETIME) AS TIME )
                                            AND CAST( CAST( CAST('" . $inTime . "' AS DATETIME) AS DATETIME) + CAST('" . $this->autoShiftConfiguration['autoShiftPunchRadius'] . "' AS DATETIME) AS TIME )
                                        END
                                        ELSE
                                        BEGIN
                                            SELECT CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) Shift_From_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
END as Shift_To_Date,

CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_MFrom AS DATETIME) AS DATETIME) Shift_MFrom_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_MFrom AS TIME)> CAST(ShiftMast.Shift_MTo AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
END as Shift_MTo_Date,
CAST( CAST(CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftMast.ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [PunchStart],
CASE 
WHEN (CAST(Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) + CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME) + CAST('" . $this->compoOffWorkHours . "' AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME)+ CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME) + CAST('" . $this->compoOffWorkHours . "' AS DATETIME)  AS DATETIME)
END as [PunchEnd],
* FROM ShiftMast WHERE LOWER(Shift_Code) = '" . $this->autoShiftConfiguration['shiftCodeForAutoShift'] . "'
                                        END";
        } else {
            $employeeShiftDetailQuery = "SELECT TOP 1 CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) Shift_From_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
END as Shift_To_Date,

CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_MFrom AS DATETIME) AS DATETIME) Shift_MFrom_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_MFrom AS TIME)> CAST(ShiftMast.Shift_MTo AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
END as Shift_MTo_Date,
CAST( CAST(CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftMast.ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [PunchStart],
CASE 
WHEN (CAST(Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $this->cleanDateFormat($this->todayDateTime) . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) + CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME)  + CAST('" . $this->compoOffWorkHours . "' AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $this->cleanDateFormat($this->todayDateTime) . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME)+ CAST(ShiftMast.ShiftEnd_SwTime AS DATETIME) + CAST('" . $this->otTime . "' AS DATETIME) + CAST('" . $this->compoOffWorkHours . "' AS DATETIME)  AS DATETIME)
END as [PunchEnd],
ShiftMast.*, ShiftPatternMast.* FROM Roster_schema, att_roster, ShiftMast, ShiftPatternMast WHERE " . $this->setShiftWhereCondition();
        }
        $employeeShiftDetailResult = query($query, $employeeShiftDetailQuery, $pa, $opt, $ms_db);
        
        $this->shiftPatterns = array(
            7,
            7,
            7,
            7,
            7,
            7
        );
        
        if ($num($employeeShiftDetailResult) > 0) {
            $employeeShiftDetailResultRow = $fetch($employeeShiftDetailResult);
            $this->actualShift            = $employeeShiftDetailResultRow['Shift_Name'];
            $this->scheduleShift          = $employeeShiftDetailResultRow['Shift_Name'];
            $this->shiftId                = $employeeShiftDetailResultRow['ShiftMastId'];
            $this->ShiftStart             = $this->cleanDateTimeFormat($employeeShiftDetailResultRow['Shift_From_Date']);
            $this->ShiftEnd               = $this->cleanDateTimeFormat($employeeShiftDetailResultRow['Shift_To_Date']);
            $this->Shift_MFrom            = $this->cleanDateTimeFormat($employeeShiftDetailResultRow['Shift_MFrom_Date']);
            $this->Shift_MTo              = $this->cleanDateTimeFormat($employeeShiftDetailResultRow['Shift_MTo_Date']);
            $this->ShiftStart_SwTime      = $employeeShiftDetailResultRow['ShiftStart_SwTime'];
            $this->ShiftEnd_SwTime        = $employeeShiftDetailResultRow['ShiftEnd_SwTime'];
            $this->LateAllow              = $employeeShiftDetailResultRow['LateAllow'];
            $this->LateAllowCycle         = $employeeShiftDetailResultRow['LateAllowCycle'];
            $this->LateAllowGPrd          = $employeeShiftDetailResultRow['LateAllowGPrd'];
            $this->ErlyAllow              = $employeeShiftDetailResultRow['ErlyAllow'];
            $this->ErlyAllowCycle         = $employeeShiftDetailResultRow['ErlyAllowCycle'];
            $this->ErlyAllowGPrd          = $employeeShiftDetailResultRow['ErlyAllowGPrd'];
            $this->MHrsFul                = $employeeShiftDetailResultRow['MHrsFul'];
            $this->MHrsHalf               = $employeeShiftDetailResultRow['MHrsHalf'];
            $this->Shift_Name             = $employeeShiftDetailResultRow['Shift_Name'];
            $this->PunchStart             = $employeeShiftDetailResultRow['PunchStart'];
            $this->PunchEnd               = $employeeShiftDetailResultRow['PunchEnd'];
            $this->Shiftmendatryhrs       = $this->getShiftMandatoryHours();
            $this->shiftPatterns          = array(
                ($employeeShiftDetailResultRow['WeeklyOff1']) ? $employeeShiftDetailResultRow['WeeklyOff1'] : 7,
                ($employeeShiftDetailResultRow['WeeklyOff2']) ? $employeeShiftDetailResultRow['WeeklyOff2'] : 7,
                ($employeeShiftDetailResultRow['WeeklyOff3']) ? $employeeShiftDetailResultRow['WeeklyOff3'] : 7,
                ($employeeShiftDetailResultRow['WeeklyOff4']) ? $employeeShiftDetailResultRow['WeeklyOff4'] : 7,
                ($employeeShiftDetailResultRow['WeeklyOff5']) ? $employeeShiftDetailResultRow['WeeklyOff5'] : 7
            );
        }
        
        //chek actual shift and replace it
        $getActualShiftQuery  = "SELECT s.Shift_Name from rost_change r,ShiftMast s WHERE r.shiftMaster=s.ShiftMastId AND r.RosterDate='" . $this->cleanDateFormat($this->todayDateTime) . "' AND r.Emp_code='" . $this->empCode . "'";
        $getActualShiftResult = query($query, $getActualShiftQuery, $pa, $opt, $ms_db);
        
        if ($getActualShiftRow = $fetch($getActualShiftResult))
            $this->actualShift = $getActualShiftRow['Shift_Name'];
    }
    
    public function checkWeeklyOffs()
    {
        $start     = new DateTime($this->getFirstDateOfGivenMonth());
        $end       = new DateTime($this->lastDateOfGivenMonth());
        $interval  = new DateInterval('P1D');
        $dateRange = new DatePeriod($start, $interval, $end);
        
        $weekSet     = array();
        $weekNumber  = 0;
        $weeks       = array();
        $weekSetTemp = array();
        for ($date = $start; $date <= $end; $date->modify('+1 day')) {
            if (!in_array($date->format("W"), $weekSetTemp) && isset($this->shiftPatterns[$weekNumber])) {
                $weekSetTemp[] = $date->format("W");
                $weekSet       = explode(',', $this->shiftPatterns[$weekNumber]);
                $weekNumber++;
            }
            
            if (in_array($date->format('N'), $weekSet)) {
                //$weeks[$date->format("W")][] = $date->format('Y-m-d') . ' - ' . $date->format('N');
                $weeks[$date->format("W") . "_" . $date->format('N')] = $date->format($this->dateFormat);
            }
        }
        
        
        
        return $weeks;
    }
    
    public function isWeeklyOff()
    {
        return (in_array($this->cleanDateFormat($this->todayDateTime), $this->checkWeeklyOffs())) ? true : false;
    }
    
    public function checkHolidays()
    {
        /* $yearOfTodayDate = date('Y', strtotime($this->cleanDateFormat($this->todayDateTime)));
        
        if (!empty($this->tempCurrentDateYearForHoliday) && $yearOfTodayDate == $this->tempCurrentDateYearForHoliday)
        return false; */
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        self::$counter++;
        
        $this->tempCurrentDateYearForHoliday = date('Y', strtotime($this->getFirstDateOfGivenYear()));
        $startDate                           = $this->getFirstDateOfGivenYear();
        $endDate                             = $this->lastDateOfGivenYear();
        $empCode                             = $this->empCode;
        
        $where = $wherefor = $forAllHolidays = "";
        
        //get employees data
        $getEmployeeDataSql       = "SELECT LOC_CODE,COMP_CODE,BussCode AS BUS_CODE ,SUBBuss_CODE AS SUBBUS_CODE,WLOC_CODE,FUNCT_CODE AS FUNC_CODE,SFUNCT_CODE AS SUBFUNC_CODE,COST_CODE,PROC_CODE,GRD_CODE,DSG_CODE FROM HrdMastQry WHERE Emp_Code = '" . $empCode . "'";
        $getEmployeeDataSqlResult = query($query, $getEmployeeDataSql, $pa, $opt, $ms_db);
        if ($num($getEmployeeDataSqlResult) > 0) {
            $getEmployeeData = $fetch($getEmployeeDataSqlResult);
        }
        
        $this->holidaysDates = array();
        
        /*   Query For Holidays which are applicable for all Employees without any restriction */
        $queryForAll = "SELECT HDATE,HDESC,IsAdditionalHoliday FROM Holidays WHERE ( HDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ) AND H_status=1 
                        AND (LOC_CODE is null or LOC_CODE = '') 
                        AND (COMP_CODE is null or COMP_CODE = '')
                        AND (BUS_CODE is null or BUS_CODE = '')
                        AND (SUBBUS_CODE is null or SUBBUS_CODE = '')
                        AND (WLOC_CODE is null or WLOC_CODE = '')
                        AND (FUNC_CODE is null or FUNC_CODE = '')
                        AND (SUBFUNC_CODE is null or SUBFUNC_CODE = '')
                        AND (COST_CODE is null or COST_CODE = '')
                        AND (PROC_CODE is null or PROC_CODE = '')
                        AND (GRD_CODE is null or GRD_CODE = '')
                        AND (DSG_CODE is null or DSG_CODE = '')";
        
        $resultq = query($query, $queryForAll, $pa, $opt, $ms_db);
        
        if ($resultq) {
            $tempArray4 = $num($resultq);
        } else {
            $tempArray4 = -1;
        }
        if ($tempArray4 > 0) {
            while ($rowq = $fetch($resultq)) {
                $this->holidaysDates[$this->cleanDateFormat($rowq['HDATE'])] = array(
                    'IsAdditionalHoliday' => $rowq['IsAdditionalHoliday'],
                    'title' => $rowq['HDESC'],
                    'start' => $this->cleanDateFormat($rowq['HDATE'])
                );
                $forAllHolidays .= "'" . $rowq['HDATE'] . "',";
            }
        }
        $forAllHolidays = rtrim($forAllHolidays, ",");
        
        if ($forAllHolidays != '') {
            $wherefor = " AND HDATE NOT IN (" . $forAllHolidays . ")";
        }
        
        /*  END Process  */
        
        $queryNew = "SELECT * FROM Holidays WHERE ( HDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ) AND  H_status=1" . $wherefor . "";
        $resultq  = query($query, $queryNew, $pa, $opt, $ms_db);
        
        if ($resultq) {
            $tempArray4 = $num($resultq);
        } else {
            $tempArray4 = -1;
        }
        
        if ($tempArray4 > 0) {
            
            while ($rowy = $fetch($resultq)) {
                if (trim($rowy['LOC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['LOC_CODE'] == '')) ? "" : " AND ( ','+LOC_CODE+',' LIKE '%," . $getEmployeeData['LOC_CODE'] . ",%' )";
                }
                if (trim($rowy['COMP_CODE']) != '') {
                    $where .= (trim($getEmployeeData['COMP_CODE'] == '')) ? "" : " AND ( ','+COMP_CODE+',' LIKE '%," . $getEmployeeData['COMP_CODE'] . ",%' )";
                }
                if (trim($rowy['BUS_CODE']) != '') {
                    $where .= (trim($getEmployeeData['BUS_CODE'] == '')) ? "" : " AND ( ','+BUS_CODE+',' LIKE '%," . $getEmployeeData['BUS_CODE'] . ",%' )";
                }
                if (trim($rowy['SUBBUS_CODE']) != '') {
                    $where .= (trim($getEmployeeData['SUBBUS_CODE'] == '')) ? "" : " AND ( ','+SUBBUS_CODE+',' LIKE '%," . $getEmployeeData['SUBBUS_CODE'] . ",%' )";
                }
                if (trim($rowy['WLOC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['WLOC_CODE'] == '')) ? "" : " AND ( ','+WLOC_CODE+',' LIKE '%," . $getEmployeeData['WLOC_CODE'] . ",%' )";
                }
                if (trim($rowy['FUNC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['FUNC_CODE'] == '')) ? "" : " AND ( ','+FUNC_CODE+',' LIKE '%," . $getEmployeeData['FUNC_CODE'] . ",%' )";
                }
                if (trim($rowy['SUBFUNC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['SUBFUNC_CODE'] == '')) ? "" : " AND ( ','+SUBFUNC_CODE+',' LIKE '%," . $getEmployeeData['SUBFUNC_CODE'] . ",%' )";
                }
                if (trim($rowy['COST_CODE']) != '') {
                    $where .= (trim($getEmployeeData['COST_CODE'] == '')) ? "" : " AND ( ','+COST_CODE+',' LIKE '%," . $getEmployeeData['COST_CODE'] . ",%' )";
                }
                if (trim($rowy['PROC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['PROC_CODE'] == '')) ? "" : " AND ( ','+PROC_CODE+',' LIKE '%," . $getEmployeeData['PROC_CODE'] . ",%' )";
                }
                if (trim($rowy['GRD_CODE']) != '') {
                    $where .= (trim($getEmployeeData['GRD_CODE'] == '')) ? "" : " AND ( ','+GRD_CODE+',' LIKE '%," . $getEmployeeData['GRD_CODE'] . ",%' )";
                }
                if (trim($rowy['DSG_CODE']) != '') {
                    $where .= (trim($getEmployeeData['DSG_CODE'] == '')) ? "" : " AND ( ','+DSG_CODE+',' LIKE '%," . $getEmployeeData['DSG_CODE'] . ",%' )";
                }
                
                $sqlq = "SELECT HDATE,HDESC,IsAdditionalHoliday FROM Holidays WHERE HDATE='" . $rowy['HDATE'] . "' AND H_status=1 " . $where . " ";
                
                $resultp = query($query, $sqlq, $pa, $opt, $ms_db);
                
                if ($resultq) {
                    $tempArray4 = $num($resultp);
                } else {
                    $tempArray4 = -1;
                }
                
                if ($tempArray4 > 0) {
                    while ($rowq = $fetch($resultp)) {
                        $this->holidaysDates[$this->cleanDateFormat($rowq['HDATE'])] = array(
                            'IsAdditionalHoliday' => $rowq['IsAdditionalHoliday'],
                            'title' => $rowq['HDESC'],
                            'start' => $this->cleanDateFormat($rowq['HDATE'])
                        );
                    }
                }
                $where = "";
            }
        }
    }
    
    public function isHoliday()
    {
        $this->checkHolidays(); //get all holidays list
        return (array_key_exists($this->cleanDateFormat($this->todayDateTime), $this->holidaysDates)) ? true : false;
    }
    
    public function getLeaves()
    {
        /* $yearOfTodayDate = date('Y', strtotime($this->cleanDateFormat($this->todayDateTime)));
        
        if (!empty($this->tempCurrentDateYearForLeave) && $yearOfTodayDate == $this->tempCurrentDateYearForLeave) {
        return false;
        } */
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $this->tempCurrentDateYearForLeave = date('Y', strtotime($this->getFirstDateOfGivenYear()));
        $startDate                         = $this->getFirstDateOfGivenYear();
        $endDate                           = $this->lastDateOfGivenYear();
        $empCode                           = $this->empCode;
        $startDateTimeStamp                = strtotime($startDate);
        
        $leaveQuery = "SELECT cast(LvFrom as date) as startDate,cast(LvTo as date) as endDate,status,reason,LvType,FromHalf FROM leave WHERE leaveID IN (SELECT MAX(leaveID) FROM leave GROUP BY LevKey) AND ('" . $this->getFirstDateOfGivenYear() . "'  BETWEEN LvFROM AND LvTO ) AND status  NOT IN( '4' , '3')  and createdby='" . $this->empCode . "' ORDER BY LvFrom";
        
        $leaveResult = query($query, $leaveQuery, $pa, $opt, $ms_db);
        
        $leavesData = array();
        if ($num($leaveResult) > 0) {
            
            while ($leaveResultRow = $fetch($leaveResult)) {
                $status = $this->getEmployeeStatusFromCode($leaveResultRow['status']);
                
                $current = strtotime($this->cleanDateFormat($leaveResultRow['startDate']));
                $last    = strtotime($this->cleanDateFormat($leaveResultRow['endDate']));
                
                $leaveNature = 'Full Day';
                switch (trim($leaveResultRow['FromHalf'])) {
                    case '1FH':
                        $leaveNature = 'First Half';
                        break;
                    
                    case '2FH':
                        $leaveNature = 'Second Half';
                        break;
                }
                
                if ($current == $last) {
                    $leavesData = array(
                        'date' => trim($this->cleanDateFormat($leaveResultRow['startDate'])),
                        'status' => trim($status),
                        'reason' => trim($leaveResultRow['reason']),
                        'LvType' => trim($leaveResultRow['LvType']),
                        'leaveNature' => trim($leaveNature)
                    );
                    
                    $this->setLeaveTransactions($leavesData);
                } else {
                    while ($current <= $last) {
                        $leaveDate = date($this->dateFormat, $current);
                        $current   = strtotime($this->stepofDays, $current);
                        if ($startDateTimeStamp < $current) {
                            $leavesData = array(
                                'date' => trim($leaveDate),
                                'status' => trim($status),
                                'reason' => trim($leaveResultRow['reason']),
                                'LvType' => trim($leaveResultRow['LvType']),
                                'leaveNature' => trim($leaveNature)
                            );
                            $this->setLeaveTransactions($leavesData);
                        }
                    }
                }
            }
        }
    }
    
    public function getLeaveTypeText($leaveType)
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $leaveTypeTextQuery  = "SELECT LOV_Text FROM lovmast WHERE LOV_Field = 'leave' AND LOV_Value = '" . $leaveType . "'";
        $leaveTypeTextResult = query($query, $leaveTypeTextQuery, $pa, $opt, $ms_db);
        if ($num($leaveTypeTextResult) > 0) {
            while ($leaveResultRow = $fetch($leaveTypeTextResult)) {
                return (isset($leaveResultRow['LOV_Text']) && !empty($leaveResultRow['LOV_Text'])) ? $leaveResultRow['LOV_Text'] : $leaveType;
            }
        }
        return $leaveType;
    }
    
    public function setLeaveTransactions($leaveData)
    {
        if (empty($leaveData))
            return false;
        
        if (trim($leaveData['status']) == 'Approved' || trim($leaveData['status']) == 'Approved Cancel - Request for cancel' || trim($leaveData['status']) == 'Approved Cancel Rejected') {
            $this->leaveDates[$leaveData['date']] = array(
                'type' => 'leave',
                'title' => 'Leave',
                'reason' => $leaveData['reason'],
                'status' => $leaveData['status'],
                'leaveTempStatus' => 'Present',
                'start' => $this->cleanDateFormat($leaveData['date']),
                'className' => array(
                    "blue_over circle-legend"
                ),
                'presetShortStatus' => 'P',
                'lvType' => trim($leaveData['LvType']),
                'leaveNature' => trim($leaveData['leaveNature']),
                'leaveTypeText' => trim($this->getLeaveTypeText($leaveData['LvType']))
            );
            $this->status                         = 'P';
            $this->statustitle                    = 'Present';
            
            if ($leaveData['leaveNature'] == 'First Half' || $leaveData['leaveNature'] == 'Second Half') {
                $this->status      = 'H';
                $this->statustitle = 'Half Day';
            }
        } else if ($leaveData['status'] == 'Pending') {
            $this->leaveDates[$leaveData['date']] = array(
                'type' => 'leave',
                'title' => 'Absent',
                'reason' => $leaveData['reason'],
                'status' => $leaveData['status'],
                'leaveTempStatus' => 'Absent',
                'start' => $this->cleanDateFormat($leaveData['date']),
                'className' => array(
                    "red circle-legend"
                ),
                'presetShortStatus' => 'A',
                'lvType' => trim($leaveData['LvType']),
                'leaveNature' => trim($leaveData['leaveNature']),
                'leaveTypeText' => trim($this->getLeaveTypeText($leaveData['LvType']))
            );
        } else if ($leaveData['status'] == 'Approved Cancellation Request') {
            return array(
                'type' => 'leave',
                'title' => 'Leave',
                'reason' => $leaveData['reason'],
                'status' => $leaveData['status'],
                'start' => $leaveData['date'],
                'leaveTempStatus' => 'Absent',
                'className' => array(
                    "red circle-legend"
                ),
                'presetShortStatus' => 'A',
                'lvType' => trim($leaveData['LvType']),
                'leaveNature' => trim($leaveData['leaveNature']),
                'leaveTypeText' => trim($this->getLeaveTypeText($leaveData['LvType']))
            );
        } else {
            $this->leaveDates[$leaveData['date']] = array(
                'type' => 'leave',
                'title' => 'Leave',
                'reason' => $leaveData['reason'],
                'status' => $leaveData['status'],
                'leaveTempStatus' => 'Absent',
                'start' => $this->cleanDateFormat($leaveData['date']),
                'className' => array(
                    "red circle-legend"
                ),
                'presetShortStatus' => 'A',
                'lvType' => trim($leaveData['LvType']),
                'leaveTypeText' => trim($this->getLeaveTypeText($leaveData['LvType']))
            );
        }
    }
    
    public function getODRequest()
    {
        /* $yearOfTodayDate = date('Y', strtotime($this->cleanDateFormat($this->todayDateTime)));
        
        if (!empty($this->tempCurrentDateYearForOD) && $yearOfTodayDate == $this->tempCurrentDateYearForOD)
        return false;
        */
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        //$this->tempCurrentDateYearForOD = date('Y', strtotime($this->getFirstDateOfGivenYear()));
        
        $startDateTimeStamp = strtotime($this->getFirstDateOfGivenYear());
        $result             = array();
        $odQuery            = "SELECT cast(date_from as date) as date_from,cast(date_to as date) as date_to,intime,outtime,action_status,reason,natureOfWorkCause fROM outOnWorkRequest WHERE outWorkId IN (SELECT MAX(outWorkId) fROM outOnWorkRequest GROUP BY oDKey) AND ('" . $this->getFirstDateOfGivenYear() . "' BETWEEN date_FROM AND date_TO) and action_status NOT IN( '4' , '3') and createdby='" . $this->empCode . "' ORDER BY date_from";
        
        $odResult = query($query, $odQuery, $pa, $opt, $ms_db);
        
        $varArray = Array();
        if ($num($odResult) > 0) {
            while ($odResultRow = $fetch($odResult)) {
                
                $s = $odResultRow['date_from'];
                $e = $odResultRow['date_to'];
                
                $status = $this->getEmployeeStatusFromCode($odResultRow['action_status']);
                
                $current = strtotime($s);
                $last    = strtotime($e);
                $inTime  = trim($odResultRow['intime']);
                $outTime = trim($odResultRow['outtime']);
                
                if ($current == $last) {
                    $odData = array(
                        'date' => date('Y-m-d', $current),
                        'status' => $status,
                        'startTime' => $inTime,
                        'endTime' => $outTime,
                        'reason' => $odResultRow['reason'],
                        'natureOfWorkCause' => trim($odResultRow['natureOfWorkCause'])
                    );
                    
                    $this->setOutOnDutyTransactions($odData);
                } else {
                    while ($current <= $last) {
                        $syncDate = date($this->dateFormat, $current);
                        $current  = strtotime($this->stepofDays, $current);
                        if ($startDateTimeStamp < $current) {
                            $odData = array(
                                'date' => $syncDate,
                                'status' => trim($status),
                                'startTime' => $inTime,
                                'endTime' => $outTime,
                                'reason' => $odResultRow['reason'],
                                'natureOfWorkCause' => trim($odResultRow['natureOfWorkCause'])
                            );
                            
                            $this->setOutOnDutyTransactions($odData);
                        }
                    }
                }
            }
        }
    }
    
    public function setOutOnDutyTransactions($odData)
    {
        $odTempStatus          = '';
        $natureTypeCleanString = $odData['natureOfWorkCause'];
        if ($odData['natureOfWorkCause'] == 'full_day') {
            
            if (trim($odData['status']) == 'Approved' || trim($odData['status']) == 'Approved Cancel - Request for cancel') {
                $this->status = 'P';
                $odTempStatus = $this->statustitle = 'Present';
            }
            $natureTypeCleanString = 'Full Day';
        } else if ($odData['natureOfWorkCause'] == 'first_half' || $odData['natureOfWorkCause'] == 'second_half') {
            if (trim($odData['status']) == 'Approved' || trim($odData['status']) == 'Approved Cancel - Request for cancel') {
                $this->status = 'H';
                $odTempStatus = $this->statustitle = 'Half Day';
            }
            $natureTypeCleanString = trim(ucfirst(current(explode('_', $odData['natureOfWorkCause']))) . ' Half');
        }
        if ($odData['status'] == 'Approved' || $odData['status'] == 'Approved Cancel - Pending' || $odData['status'] == 'Approved Cancel Rejected') {
            $this->setODTransactions[$odData['date']] = array(
                'type' => 'odrequest',
                'title' => 'OD Request',
                'reason' => $odData['reason'],
                'status' => $odData['status'],
                'start' => $this->cleanDateFormat($odData['date']),
                'className' => array(
                    "pink_over circle-legend"
                ),
                'odTempStatus' => $odTempStatus,
                'presetShortStatus' => 'P',
                'startTime' => $this->cleanTimeFormat($odData['startTime']),
                'endTime' => $this->cleanTimeFormat($odData['endTime']),
                'odType' => $natureTypeCleanString
            );
        } else if ($odData['status'] == 'Pending') {
            $this->setODTransactions[$odData['date']] = array(
                'type' => 'odrequest',
                'title' => 'Absent',
                'reason' => $odData['reason'],
                'status' => $odData['status'],
                'odTempStatus' => 'Absent',
                'start' => $this->cleanDateFormat($odData['date']),
                'className' => array(
                    "red circle-legend"
                ),
                'presetShortStatus' => 'A',
                'startTime' => $this->cleanTimeFormat($odData['startTime']),
                'endTime' => $this->cleanTimeFormat($odData['endTime']),
                'odType' => $natureTypeCleanString
            );
        } else if ($odData['status'] == 'Approved Cancellation Request') {
            return array(
                'type' => 'odrequest',
                'title' => 'OD Request',
                'reason' => $odData['reason'],
                'status' => $odData['status'],
                'start' => $odData['date'],
                'odTempStatus' => 'Absent',
                'className' => array(
                    "red circle-legend"
                ),
                'presetShortStatus' => 'A',
                'startTime' => $odData['startTime'],
                'endTime' => $odData['endTime'],
                'natureOfWorkCause' => $natureTypeCleanString
            );
        } else {
            $this->setODTransactions[$odData['date']] = array(
                'type' => 'odrequest',
                'title' => 'OD Request',
                'reason' => $odData['reason'],
                'status' => $odData['status'],
                'odTempStatus' => 'Absent',
                'start' => $this->cleanDateFormat($odData['date']),
                'className' => array(
                    "red circle-legend"
                ),
                'presetShortStatus' => 'A',
                'startTime' => $this->cleanTimeFormat($odData['startTime']),
                'endTime' => $this->cleanTimeFormat($odData['endTime']),
                'odType' => $natureTypeCleanString
            );
        }
    }
    
    public function checkMarkPastAttendance()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        $markPastAttendanceQuery  = "SELECT inDate, outDate, markPastId,cast(date_from as date) as date_from,cast(date_to as date) as date_to,intime,outtime,action_status,notMarkingReason,remarks FROM markpastattendance WHERE markPastId IN (SELECT MAX(markPastId) fROM markpastattendance GROUP BY AttnKey) AND (date_FROM BETWEEN '" . $this->cleanDateFormat($this->todayDateTime) . "' AND '" . $this->cleanDateFormat($this->todayDateTime) . "' OR date_TO BETWEEN '" . $this->cleanDateFormat($this->todayDateTime) . "' AND '" . $this->cleanDateFormat($this->todayDateTime) . "') AND action_status  NOT IN( '4' , '3')  and createdby='" . $this->empCode . "' ORDER BY date_from";
        $markPastAttendanceResult = query($query, $markPastAttendanceQuery, $pa, $opt, $ms_db);
        
        $markPastAttendanceResultRow = $fetch($markPastAttendanceResult);
        
        $this->isMarkPastattendance = ($num($markPastAttendanceResult) > 0) ? 1 : 0;
        
        $this->saveRecords['is_mark_past'] = $this->isMarkPastattendance;
        if ($this->isMarkPastattendance) {
            $this->saveRecords['markPastId'] = $markPastAttendanceResultRow['markPastId'];
            if ($markPastAttendanceResultRow['action_status'] == 2 || $markPastAttendanceResultRow['action_status'] == 5 || $markPastAttendanceResultRow['action_status'] == 7) {
                $this->saveRecords['Intime'] = $this->cleanDateTimeFormat($this->cleanDateFormat($markPastAttendanceResultRow['inDate']) . ' ' . $markPastAttendanceResultRow['intime']);
                
                $this->saveRecords['Outtime'] = $this->cleanDateTimeFormat($this->cleanDateFormat($markPastAttendanceResultRow['outDate']) . ' ' . $markPastAttendanceResultRow['outtime']);
            }
            $this->saveRecords['markPastStatus'] = $this->getEmployeeStatusFromCode($markPastAttendanceResultRow['action_status']);
            
            $this->markPastattendanceStatus = 'Absent';
            $this->markpast_remark          = $markPastAttendanceResultRow['remarks'];
            if ($markPastAttendanceResultRow['action_status'] == 2 || $markPastAttendanceResultRow['action_status'] == 5 || $markPastAttendanceResultRow['action_status'] == 7) {
                $actualHoursSpent         = strtotime($this->saveRecords['Outtime']) - strtotime($this->saveRecords['Intime']);
                $hours                    = intval($actualHoursSpent / 3600);
                $min                      = intval(($actualHoursSpent - ($hours * 3600)) / 60);
                $spendTimeInOfficeMinutes = $this->getMinutes($hours . ":" . $min . ":00", '00:00:00');
                $mHrsFullMinutes          = $this->getMinutes((isset($this->MHrsFul) && !empty($this->MHrsFul)) ? $this->MHrsFul : $this->edlaPolicies['fullDayTime'], '00:00:00');
                $mHrsHalfMinutes          = $this->getMinutes((isset($this->MHrsHalf) && !empty($this->MHrsHalf)) ? $this->MHrsHalf : $this->edlaPolicies['halfDayTime'], '00:00:00');
                if ($spendTimeInOfficeMinutes > $mHrsHalfMinutes && $spendTimeInOfficeMinutes >= $mHrsFullMinutes) {
                    //Full day
                    $this->markPastattendanceStatus = 'Present';
                    $this->status                   = 'P';
                } else if ($spendTimeInOfficeMinutes >= $mHrsHalfMinutes && $spendTimeInOfficeMinutes < $mHrsFullMinutes) {
                    //Half Day
                    $this->markPastattendanceStatus = 'Half Day';
                    $this->status                   = 'H';
                } else {
                    //Absent
                    $this->markPastattendanceStatus = 'Absent';
                    $this->status                   = 'A';
                }
            }
        }
        return (($num($markPastAttendanceResult) > 0) && ($this->saveRecords['markPastStatus'] == 2 || $this->saveRecords['markPastStatus'] == 5)) ? true : false;
    }
    
    public function getEmployeeStatusFromCode($statusCode)
    {
        $statusCode = trim($statusCode);
        if (empty($statusCode))
            return false;
        
        switch ($statusCode) {
            case 1:
                $statusString = 'Pending';
                break;
            case 2:
                $statusString = 'Approved';
                break;
            case 3:
                $statusString = 'Rejected';
                break;
            case 4:
                $statusString = 'Cancel';
                break;
            case 5:
                $statusString = 'Approved Cancel - Request for cancel';
                break;
            case 6:
                $statusString = 'Approved Cancellation';
                break;
            case 7:
                $statusString = 'Approved Cancel Rejected';
                break;
            default:
                $statusString = '';
                break;
        }
        
        return $statusString;
    }
    
    public function isValidMultipleEventStatus($firstStatus, $secondStatus)
    {
        return (!empty($firstStatus) || !empty($secondStatus)) ? TRUE : FALSE;
    }
    
    public function getMultipleEventStatus($firstStatus, $secondStatus)
    {
        if (empty($firstStatus) || empty($secondStatus)) {
            return FALSE;
        }
        
        if ($firstStatus == 'Present' && $secondStatus == 'Present') {
            $this->htmlclass = $this->getHtmlClassName('P');
        } else if (($firstStatus == 'Present' && $secondStatus == 'Half Day') || ($firstStatus == 'Half Day' && $secondStatus == 'Present')) {
            $this->htmlclass = $this->getHtmlClassName('P');
        } else if (($firstStatus == 'Present' && $secondStatus == 'Absent') || ($firstStatus == 'Absent' && $secondStatus == 'Present')) {
            $this->htmlclass = $this->getHtmlClassName('PA');
        } else {
            $this->htmlclass = $this->getHtmlClassName('A');
        }
    }
    
    public function isMissPunch()
    {
        return (($this->saveRecords['Intime'] == '' && $this->saveRecords['Outtime'] != '') || ($this->saveRecords['Intime'] != '' && $this->saveRecords['Outtime'] == '')) ? TRUE : FALSE;
    }
    
    public function PunchHalffDayStatus()
    {
        
    }
    
    public function getFinalStatus()
    {
        $this->finalStatus = $this->_getLegendDetailByMaster("A", "calendarMessage");
        $this->status      = $this->_getLegendDetailByMaster("A", "reportLegend");
        $this->htmlclass   = $this->getHtmlClassName('A');
        $this->legendId    = $this->_getLegendDetailByMaster("A", "masterId");
        
        switch ($this->punchStatusPH) {
            case "Present":
                $this->finalStatus = $this->_getLegendDetailByMaster("P", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("P", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("P", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('P');
                break;
            case "Half Day":
                $this->finalStatus = $this->_getLegendDetailByMaster("HLP", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("HLP", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("HLP", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('H');
                break;
        }
        
        $isPunchExist       = (!empty($this->saveRecords['Intime']) || !empty($this->saveRecords['Outtime'])) ? TRUE : FALSE;
        $isBothPunchExist   = (!empty($this->saveRecords['Intime']) && !empty($this->saveRecords['Outtime'])) ? TRUE : FALSE;
        $isSinglePunchExist = $this->isMissPunch();
        
        if ($this->isWeeklyOff()) {
            $this->finalStatus          = $this->_getLegendDetailByMaster("WO", "calendarMessage");
            $this->statustitle          = 'weeklyoff';
            $this->status               = $this->_getLegendDetailByMaster("WO", "reportLegend");
            $this->isWeeklyOffOrHoliday = TRUE;
            $this->htmlclass            = $this->getHtmlClassName('WH');
            $this->legendId             = $this->_getLegendDetailByMaster("WO", "masterId");
        }
        if ($this->isHoliday()) {
            $this->finalStatus = $this->_getLegendDetailByMaster("HLD", "calendarMessage");
            $this->statustitle = 'pholiday';
            $this->status      = $this->_getLegendDetailByMaster("HLD", "reportLegend");
            ;
            $this->isWeeklyOffOrHoliday = TRUE;
            $this->htmlclass            = $this->getHtmlClassName('WH');
            $this->legendId             = $this->_getLegendDetailByMaster("HLD", "masterId");
        }
        if ($this->isWeeklyOff() && $isPunchExist) {
            $this->finalStatus          = $this->_getLegendDetailByMaster("POW", "calendarMessage");
            $this->status               = $this->_getLegendDetailByMaster("POW", "reportLegend");
            $this->statustitle          = 'weeklyoff';
            $this->isWeeklyOffOrHoliday = TRUE;
            $this->htmlclass            = $this->getHtmlClassName('PWH');
            $this->legendId             = $this->_getLegendDetailByMaster("POW", "masterId");
        }
        
        if ($this->isHoliday() && $isPunchExist) {
            $this->finalStatus          = $this->_getLegendDetailByMaster("POH", "calendarMessage");
            $this->status               = $this->_getLegendDetailByMaster("POH", "reportLegend");
            $this->statustitle          = 'pholiday';
            $this->isWeeklyOffOrHoliday = TRUE;
            $this->htmlclass            = $this->getHtmlClassName('PWH');
            $this->legendId             = $this->_getLegendDetailByMaster("POH", "masterId");
        }
        
        if ($this->isOtExist && $this->punchStatusPH == "Present") {
            $this->finalStatus = $this->_getLegendDetailByMaster("POT", "calendarMessage");
            $this->status      = $this->_getLegendDetailByMaster("POT", "reportLegend");
            $this->legendId    = $this->_getLegendDetailByMaster("POT", "masterId");
            $this->htmlclass   = $this->getHtmlClassName('P');
        }
        
        if ($this->Earlyflag && $this->punchStatusPH == "Present") {
            $this->finalStatus = $this->_getLegendDetailByMaster("EDP", "calendarMessage");
            $this->status      = $this->_getLegendDetailByMaster("EDP", "reportLegend");
            $this->legendId    = $this->_getLegendDetailByMaster("EDP", "masterId");
            $this->htmlclass   = $this->getHtmlClassName('P');
        }
        
        if ($this->Lateflag && $this->punchStatusPH == "Present") {
            $this->finalStatus = $this->_getLegendDetailByMaster("LAP", "calendarMessage");
            $this->status      = $this->_getLegendDetailByMaster("LAP", "reportLegend");
            $this->legendId    = $this->_getLegendDetailByMaster("LAP", "masterId");
            $this->htmlclass   = $this->getHtmlClassName('P');
        }
        
        if ($this->Lateflag && $this->Earlyflag && $this->punchStatusPH == "Present") {
            $this->finalStatus = $this->_getLegendDetailByMaster("EDLAP", "calendarMessage");
            $this->status      = $this->_getLegendDetailByMaster("EDLAP", "reportLegend");
            $this->legendId    = $this->_getLegendDetailByMaster("EDLAP", "masterId");
            $this->htmlclass   = $this->getHtmlClassName('P');
        }
        
        //don't need to check miss punch for maxlife company employees
        if (strtolower($this->companyCode) != 'maxlife') {
            if (!$this->isWeeklyOff() && !$this->isHoliday() && $isSinglePunchExist && !$this->isOutOndutyPresentHF() && !$this->isLeavePresentHF() && !$this->isMarkPastattendance()) {
                $this->finalStatus = $this->_getLegendDetailByMaster("MIS", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("MIS", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("MIS", "masterId");
                $this->statustitle = 'Absent';
                $this->htmlclass   = $this->getHtmlClassName('MissPunch');
            }
        }
        
        if ($isPunchExist && $this->isOutOndutyPresentHF()) {
            if ($this->punchStatusPH == 'Present' && $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Full Day") {
                $this->finalStatus = $this->_getLegendDetailByMaster("OD", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("OD", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("OD", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('P');
            }
            
            if ($this->punchStatusPH == 'Half Day' && $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "First Half" || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Second Half") {
                $this->finalStatus = $this->_getLegendDetailByMaster("HP_OD", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("HP_OD", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("HP_OD", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('H');
            }
            
            /* Need Discussion */
            /* if($this->getOutOnDutyStatus() == "Present" && ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "First Half" || $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Second Half" )){
            $this->finalStatus = $this->_getLegendDetailByMaster("H_OD", "calendarMessage");
            $this->status = $this->_getLegendDetailByMaster("H_OD", "reportLegend");
            } */
        }
        
        if ($this->isOutOndutyPresentHF() && $this->isLeavePresentHF()) {
            if ((($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "First Half" || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Second Half") || ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Second Half" || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "First Half"))) {
                $this->finalStatus = $this->_getLegendDetailByMaster("P", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("P", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("P", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('H');
            }
        }
        
        if ($this->isMarkPastattendance()) {
            switch ($this->markPastattendanceStatus) {
                case 'Present':
                    $this->finalStatus = $this->_getLegendDetailByMaster("AR", "calendarMessage");
                    $this->status      = $this->_getLegendDetailByMaster("AR", "reportLegend");
                    $this->legendId    = $this->_getLegendDetailByMaster("AR", "masterId");
                    $this->htmlclass   = $this->getHtmlClassName('P');
                    break;
                
                case 'Half Day':
                    $this->finalStatus = $this->_getLegendDetailByMaster("HAR", "calendarMessage");
                    $this->status      = $this->_getLegendDetailByMaster("HAR", "reportLegend");
                    $this->legendId    = $this->_getLegendDetailByMaster("HAR", "masterId");
                    $this->htmlclass   = $this->getHtmlClassName('H');
                    break;
            }
        }
        
        if ($this->isOutOndutyPresentHF()) {
            if ($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Full Day") {
                $this->finalStatus = $this->_getLegendDetailByMaster("OD", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("OD", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("OD", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('P');
            }
            
            if ($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "First Half" || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Second Half") {
                $this->finalStatus = $this->_getLegendDetailByMaster("H_OD", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("H_OD", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("H_OD", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('H');
            }
        }
        
        if ($this->isOutOndutyPresentHF() && $this->isHoliday()) {
            $this->finalStatus = $this->_getLegendDetailByMaster("H(OD)", "calendarMessage");
            $this->status      = $this->_getLegendDetailByMaster("H(OD)", "reportLegend");
            $this->legendId    = $this->_getLegendDetailByMaster("H(OD)", "masterId");
            $this->htmlclass   = $this->getHtmlClassName('PWH');
        }
        
        if ($this->isOutOndutyPresentHF() && $this->isWeeklyOff()) {
            $this->finalStatus = $this->_getLegendDetailByMaster("WO(OD)", "calendarMessage");
            $this->status      = $this->_getLegendDetailByMaster("WO(OD)", "reportLegend");
            $this->legendId    = $this->_getLegendDetailByMaster("WO(OD)", "masterId");
            $this->htmlclass   = $this->getHtmlClassName('PWH');
        }
        
        if ($this->isLeavePresentHF()) {
            $shortNameOfLeave = $this->_leaveName[trim($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'])]['shortStatus'];
            
            if ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Full Day") {
                $this->finalStatus = $this->_getLegendDetailByMaster($shortNameOfLeave, "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster($shortNameOfLeave, "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster($shortNameOfLeave, "masterId");
                $this->htmlclass   = $this->getHtmlClassName('L');
            }
            
            if ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "First Half" || $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Second Half") {
                $this->finalStatus = $this->_getLegendDetailByMaster("H_" . $shortNameOfLeave, "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("H_" . $shortNameOfLeave, "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("H_" . $shortNameOfLeave, "masterId");
                $this->htmlclass   = $this->getHtmlClassName('L');
            }
        }
        
        if ($isPunchExist && $this->isLeavePresentHF()) {
            if ($isPunchExist && $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Full Day") {
                $shortNameOfLeave  = $this->_leaveName[trim($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'])]['shortStatus'];
                $this->finalStatus = $this->_getLegendDetailByMaster($shortNameOfLeave . "(P)", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster($shortNameOfLeave . "(P)", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster($shortNameOfLeave . "(P)", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('L');
            }
            
            if ($isPunchExist && ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "First Half" || $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Second Half")) {
                $shortNameOfLeave  = $this->_leaveName[trim($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'])]['shortStatus'];
                $this->finalStatus = $this->_getLegendDetailByMaster("H_" . $shortNameOfLeave . "(P)", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("H_" . $shortNameOfLeave . "(P)", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("H_" . $shortNameOfLeave . "(P)", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('L');
            }
        }
        
        if ($this->isLeavePresentHF() && $this->isWeeklyOff()) {
            if ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Full Day") {
                $shortNameOfLeave  = $this->_leaveName[trim($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'])]['shortStatus'];
                $this->finalStatus = $this->_getLegendDetailByMaster($shortNameOfLeave . "_WO", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster($shortNameOfLeave . "_WO", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster($shortNameOfLeave . "_WO", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('PWH');
            }
            
            if ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "First Half" || $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Second Half") {
                $shortNameOfLeave  = $this->_leaveName[trim($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'])]['shortStatus'];
                $this->finalStatus = $this->_getLegendDetailByMaster("H" . $shortNameOfLeave . "_WO", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("H" . $shortNameOfLeave . "_WO", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("H" . $shortNameOfLeave . "_WO", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('PWH');
            }
        }
        
        if ($this->isLeavePresentHF() && $this->isHoliday()) {
            if ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Full Day") {
                $shortNameOfLeave  = $this->_leaveName[trim($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'])]['shortStatus'];
                $this->finalStatus = $this->_getLegendDetailByMaster($shortNameOfLeave . "_HO", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster($shortNameOfLeave . "_HO", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster($shortNameOfLeave . "_HO", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('PWH');
            }
            
            if ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "First Half" || $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Second Half") {
                $shortNameOfLeave  = $this->_leaveName[trim($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'])]['shortStatus'];
                $this->finalStatus = $this->_getLegendDetailByMaster("H" . $shortNameOfLeave . "_HO", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("H" . $shortNameOfLeave . "_HO", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("H" . $shortNameOfLeave . "_HO", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('PWH');
            }
        }
        
        if ($this->isMarkPastattendance() && $this->isOutOndutyPresentHF()) {
            if ($this->markPastattendanceStatus == "Present" && $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Full Day") {
                $this->finalStatus = $this->_getLegendDetailByMaster("P", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("P", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("P" . $shortNameOfLeave . "_HO", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('P');
            }
            
            if ($this->markPastattendanceStatus == "Half Day" && ($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "First Half" || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Second Half")) {
                $this->finalStatus = $this->_getLegendDetailByMaster("HLP", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("HLP", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("HLP", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('H');
            }
        }
        
        if ($this->isMarkPastattendance() && $this->isLeavePresentHF()) {
            if ($this->markPastattendanceStatus == "Present" && ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "Full Day")) {
                $this->finalStatus = $this->_getLegendDetailByMaster("HLP", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("HLP", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("HLP", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('L');
            }
            
            if ($this->markPastattendanceStatus == "Half Day" && ($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] == "First Half" || $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] == "Second Half")) {
                $this->finalStatus = $this->_getLegendDetailByMaster("HLP", "calendarMessage");
                $this->status      = $this->_getLegendDetailByMaster("HLP", "reportLegend");
                $this->legendId    = $this->_getLegendDetailByMaster("HLP", "masterId");
                $this->htmlclass   = $this->getHtmlClassName('L');
            }
        }
    }
    
    public function getHtmlClassName($type)
    {
        $class = '';
        $type  = ($this->isWeeklyOffOrHoliday) ? 'WH' : $type;
        switch ($type) {
            case "P": //Present
                $class = "green2 circle-legend";
                break;
            case "A": //Absent
                $class = "red circle-legend";
                break;
            case "H": //Half Day
                $class = "yellow circle-legend ";
                break;
            case "WH": //weekly off and holiday
                $class = "green ";
                break;
            case "PWH": //Present On Hoiday
                $class = "green2 circle-legend bgnone";
                break;
            case "L": //Leave Present
                $class = "blue_over circle-legend";
                break;
            case "PA": //Preenet and Absent
                $class = "presentAbsent circle-legend";
                break;
            case "MissPunch": //Preenet and Absent
                $class = "miss-punch circle-legend";
                break;
        }
        return $class;
    }
    
    public function setHTMLClass()
    {
        if ($this->finalStatus == 'Present') {
            $this->htmlclass = $this->getHtmlClassName('P');
        }
        if ($this->finalStatus == 'Half Day') {
            $this->htmlclass = $this->getHtmlClassName('H');
        }
        
        if ($this->isMissPunch() && (strtolower($this->companyCode) != 'maxlife')) {
            
            $this->htmlclass = $this->getHtmlClassName('MissPunch');
        }
        if ($this->isWeeklyOffHoliday()) {
            $this->htmlclass = $this->getHtmlClassName('WH');
        }
        if ($this->isWorkingOnWeeklyOffHoliday()) {
            $this->htmlclass = $this->getHtmlClassName('PWH');
        }
        if ($this->isOutOnDuty()) {
            switch ($this->getOutOnDutyStatus()) {
                case 'Present':
                    $this->htmlclass = $this->getHtmlClassName('P');
                    break;
                case 'Half Day':
                    $this->htmlclass = $this->getHtmlClassName('H');
                    break;
                case 'Absent':
                    $this->htmlclass = $this->getHtmlClassName('A');
                    break;
            }
        }
        if ($this->isLeave()) {
            switch (trim($this->getLeaveStatus())) {
                case 'Present':
                    $this->htmlclass = $this->getHtmlClassName('L');
                    break;
                case 'Half Day':
                    $this->htmlclass = $this->getHtmlClassName('H');
                    break;
                case 'Absent':
                    $this->htmlclass = $this->getHtmlClassName('A');
                    break;
            }
        }
        if ($this->isMarkPastattendance()) {
            switch ($this->getmarkPastAttendanceStatus()) {
                case 'Present':
                    $this->htmlclass = $this->getHtmlClassName('P');
                    break;
                case 'Absent':
                    $this->htmlclass = $this->getHtmlClassName('A');
                    break;
            }
        }
        if ($this->isLeave() && $this->isMarkPastattendance()) {
            if ($this->isValidMultipleEventStatus($this->getLeaveStatus(), $this->getmarkPastAttendanceStatus())) {
                $this->getMultipleEventStatus($this->getLeaveStatus(), $this->getmarkPastAttendanceStatus());
            }
        }
        if ($this->isLeave() && $this->isOutOnDuty()) {
            if ($this->isValidMultipleEventStatus($this->getLeaveStatus(), $this->getOutOnDutyStatus())) {
                $this->getMultipleEventStatus($this->getLeaveStatus(), $this->getOutOnDutyStatus());
            }
        }
        if ($this->isLeave() && $this->isPunchStatusPresentHalfDay()) {
            if ($this->isValidMultipleEventStatus($this->getLeaveStatus(), $this->getPunchStatus())) {
                $this->getMultipleEventStatus($this->getLeaveStatus(), $this->getPunchStatus());
            }
        }
        if ($this->isMarkPastattendance() && $this->isOutOnDuty()) {
            if ($this->isValidMultipleEventStatus($this->getMarkPastattendanceStatus(), $this->getOutOnDutyStatus())) {
                $this->getMultipleEventStatus($this->getMarkPastattendanceStatus(), $this->getOutOnDutyStatus());
            }
        }
        if ($this->isOutOnDuty() && $this->isPunchStatusPresentHalfDay()) {
            if ($this->isValidMultipleEventStatus($this->getOutOnDutyStatus(), $this->getPunchStatus())) {
                $this->getMultipleEventStatus($this->getOutOnDutyStatus(), $this->getPunchStatus());
            }
        }
    }
    
    public function saveResult()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        if ($this->isInSeparationPeriod) {
            $this->finalStatus = $this->_getLegendDetailByMaster("SEP", "calendarMessage");
            $this->status      = $this->_getLegendDetailByMaster("SEP", "reportLegend");
            $this->legendId    = $this->_getLegendDetailByMaster("SEP", "masterId");
        } else {
            $this->getFinalStatus();
        }
        
        $this->tableValues = array(
            $this->empCode,
            $this->cleanDateFormat($this->todayDateTime),
            $this->scheduleShift,
            $this->actualShift,
            $this->ShiftStart,
            $this->ShiftEnd,
            $this->saveRecords['Intime'],
            $this->saveRecords['Outtime'],
            $this->getLateMinutes(),
            $this->getEarlyMinutes(),
            $this->getOverStayTime(),
            $this->otTime,
            $this->getSpentWorkingHours(),
            isset($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType']) ? $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['lvType'] : '',
            isset($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveTypeText']) ? $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveTypeText'] : '',
            isset($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['status']) ? $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['status'] : '',
            isset($this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature']) ? $this->leaveDates[$this->cleanDateFormat($this->todayDateTime)]['leaveNature'] : '',
            $this->getShiftMandatoryHours(),
            $this->Lateflag,
            $this->Earlyflag,
            $this->status,
            $this->remark,
            $this->statustitle,
            $this->isMarkPastattendance,
            (isset($this->saveRecords['markPastId'])) ? $this->saveRecords['markPastId'] : 0,
            $this->shiftId,
            (isset($this->saveRecords['markPastStatus'])) ? $this->saveRecords['markPastStatus'] : '',
            (isset($this->markpast_remark)) ? $this->markpast_remark : '',
            $this->isWeeklyOff(),
            $this->isHoliday(),
            (isset($this->holidaysDates[$this->cleanDateFormat($this->todayDateTime)]['title'])) ? $this->holidaysDates[$this->cleanDateFormat($this->todayDateTime)]['title'] : '',
            (isset($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['status'])) ? $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['status'] : '',
            (isset($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['startTime'])) ? $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['startTime'] : '',
            (isset($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['endTime'])) ? $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['endTime'] : '',
            (isset($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['reason'])) ? $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['reason'] : '',
            (isset($this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'])) ? $this->setODTransactions[$this->cleanDateFormat($this->todayDateTime)]['odType'] : '',
            $this->finalStatus,
            $this->htmlclass,
            date('Y-m-d H:i:s'),
            $this->legendId
        );
        
        /* echo "<pre>";
        print_r($this->tableFields);
        print_r($this->tableValues);
        die; */
        
        
        $this->insertQuery = " ( ";
        $this->insertQuery .= "'" . implode("','", $this->tableValues) . "'";
        $this->insertQuery .= " ) ";
        
        //save records into database from here if current action is not schedular
        if (!$this->isSchedular) {
            $this->executeQuery();
        }
    }
    
    public function executeQuery()
    {
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $startDate = $this->cleanDateFormat($this->todayDateTime);
        
        $insertQuery = "INSERT INTO " . $this->tableName;
        $insertQuery .= " (" . implode(', ', $this->tableFields) . ") ";
        $insertQuery .= " VALUES ";
        
        $checkExistingRecordQuery = "SELECT Id FROM attendancesch  WITH (NOLOCK) WHERE Emp_code = '" . $this->empCode . "' AND CAST([date] AS DATE) = '" . $startDate . "' ";
        $isRecordExist            = query($query, $checkExistingRecordQuery, $pa, $opt, $ms_db);
        
        $updateColumns = $failureUpdateQuery = '';
        
        if ($num($isRecordExist) > 0) {
            $sleectResultRow   = $fetch($isRecordExist);
            $sleectResultRowId = $sleectResultRow['Id'];
            $updateRowsFound++;
            $updateQuery = "UPDATE " . $this->tableName . " SET ";
            foreach ($this->tableFields AS $columnKey => $ColumnName) {
                $updateColumns .= $ColumnName . " = '" . $this->tableValues[$columnKey] . "', ";
            }
            $updateQuery .= " " . rtrim($updateColumns, ', ') . " WHERE Id = '" . $sleectResultRowId . "';";
            $updateQueryResult = query($query, $updateQuery, $pa, $opt, $ms_db);
            if (!$updateQueryResult) {
                $failureUpdateQuery .= $updateQuery;
            }
        } else {
            $insertRowsFound++;
            $insertQuery .= $this->insertQuery . ',';
        }
        
        $resultUpdate = ($updateRowsFound > 0) ? true : '';
        $resultInsert = ($insertRowsFound > 0) ? query($query, rtrim($insertQuery, ' ,'), $pa, $opt, $ms_db) : '';
        if ($resultInsert || $resultUpdate) {
            $str .= "Attendance schedular result is " . $insertRowsFound . "-inserted and " . $updateRowsFound . "-updated at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code'];
        } else {
            $str .= "Error Getting while saved attendance at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code'] . "\n" . rtrim($insertQuery, ',') . "\nFailure Update Query:" . $failureUpdateQuery . "\n------------------\n";
        }
        fwrite($fd, $str . "\n");
        fclose($fd);
        
        return TRUE;
    }
    
    public function removeRecord($empCode, $actionDate)
    {
        $empCode    = trim($empCode);
        $actionDate = trim($actionDate);
        
        if (!empty($empCode) && !empty($actionDate)) {
            return false;
        }
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        $actionDate  = date('Y-m-d', strtotime($actionDate));
        $deleteQuery = "DELETE FROM attendancesch WHERE Emp_code = '" . $empCode . "' AND CAST([date] AS DATE) = '" . $actionDate . "' ";
        @query($query, rtrim($deleteQuery, ' ,'), $pa, $opt, $ms_db);
        return true;
    }
    
}

?>