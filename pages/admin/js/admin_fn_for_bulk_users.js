var userids={};

var Role = {
    init:function(status){
        $.ajax({
            type : 'POST',
            data:{type:'init',status:status},
            url  : 'ajax/role_ajax.php',
            success: function(responseText){
                $("#showrole").html(responseText);
            }
        });
    },

    isEmpty:function(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    },

    selectAllUsers:function(type){
        var i;
        if(type==='checked'){
            $('#btn-select-all').attr('onchange','Role.selectAllUsers("unchecked")');
            i=0;
            $('.select-users-id').each(function(){
                $(this).attr('checked',true);
                var code = $(this).val();
                userids[$(this).attr('id')]={'userId': $(this).val(),'mngrId':$(this).attr('myval'),'role':$('#role_'+code).val(),'email':$('#ue_'+code).val(),'name':$('#un_'+code).val()};
                i++;
            });
        }else{
            i=0;
            $('#btn-select-all').attr('onchange','Role.selectAllUsers("checked")');
            $('.select-users-id').each(function(){
                $(this).attr('checked',false);
                delete userids[$(this).attr('id')];
                i++;
            });
        }
        //console.log(userids);
    },

    ChangeRole:function(code,value){
      var key = 'cu_'+code;
      userids['cu_'+code];
      userids[key]['role']=value;
      console.log(userids);
    },

    CreateUser:function(code,name){
      if(Role.isEmpty(userids)){
        toasterrormsg('Please select user to assigned any role');
      }else{
        var key = 'cu_'+code;
        
        Object.keys(userids).forEach(function(itm){
          if(itm != key) delete userids[itm];
        });

        if (!(key in userids)){
            toasterrormsg('Please select user before proceed');
        }else{
            Role.assigningRole();
        }
      }
    },

    CreateMultipleUsers:function(){
        if(Role.isEmpty(userids)){
            toasterrormsg('Please select at least one user');
        }else{
            Role.assigningRole();
        }
        //Role.init();
    },

    createmodel:function(){
       $.ajax({
            type : 'POST',
            data:{type:'Add',header:'Create Role',Role_name:'',roledivid:'menu',
            datadivid:'datamenu',id:'' },
            url  : 'ajax/add_roleajax.php',
            success: function(responseText){
                $('#AddRole').html(responseText);
                $('#AddRole').modal('show');
                //$("#showrole").html(responseText);
            }
        });
    },

    assigningRole:function(){
        $('#fillUsers,#fillManagerStatus,#fillEmployeeStatus').html('');
        $.ajax({
            type:'POST',
            data:userids,
            url:'ajax/add_role_for_multiple_users.php?type=set_role',
            beforeSend:function(){
                loading();
            },
            success:function(data){
                var result = $.parseJSON(data);
                if(result.success==true){
                    //console.log(result.data);
                    //alert(result.statusOfMngr.length);
                    User.createmodel(result.emptyMngr,result.statusOfMngr,result.data);
                    userids={};
                    User.init();
                    unloading(); 
                }

            },
        });
    },
    
    //show role ///////////////////////////////////////////////////////////////
    showrole:function(id,role){
        $.ajax({
            type : 'POST',
            data:{type:'Show',header:'Show Role',Role_name:role,roledivid:'showmenu',datadivid:'showdatamenu',id:id },
            url  : 'ajax/add_roleajax.php',
            success: function(responseText){
                $('#ShowRole').html(responseText);       
                $('#ShowRole').modal('show');
              //$("#showrole").html(responseText);
            }
        });
    },
    
    deletefunc : function(dbid,tbid,status){
        $.ajax({
            type: "POST",
            url: "ajax/role_ajax.php",
            data: {type:'delete',id:dbid,status:status} ,
            cache: false,
            beforeSend: function(){
                loading();
            },
            success: function(result){
                unloading();        
                if(result=="Y"){
                    if(status=="Active"){
                        var status1="Inactive";
                    }else{
                        var  status1="Active";
                    }
                    $("#st"+tbid).html(status1);
                    Role.init();
                } else {
                    alert(result);
                }
            }
        });
    }

};


var User = {
    init: function(){
        $('select').select2(); 
        $.ajax({
            type : 'POST',
            url  : 'ajax/bulk_users_ajax.php?type=init',
            beforeSend:function(){
                loading();
            },
            success: function(responseText){   
                $("#showuser").html(responseText);
                unloading();
            },
            complete:function(){
                TableAdvanced.init9();
                $('.select2Me').select2();
            }
        });
    },

    createmodel:function(data,data1,data2){
        //data for empty manager
        //data1 for status of manager
        //data2 for user created user
        
        $("#AddRole").modal('show');   
        if(data.length==0){
            $('.hide-e-m-s').css('display','none');
        }else{
            var html =''; 
            for(var i = 0; i<data.length; i++){
                html +='<span class="label label-sm label-success mr-10 ml-10" style="margin-top:8px">'+data[i].user+'</span>';
            }
            $('#fillUsers').html(html);
        }

            
        if(data1.length==0){
            $('.hide-m-s').css('display','none');
        }else{
            var html1 =''; 
            for(var i = 0; i<data1.length; i++){
                html1 +='<div><span class="label label-sm label-success mr-10 ml-10" style="margin-top:8px">'+data1[i].manager+' For '+data1[i].user +' </span></div><br>';
            }
            $('#fillManagerStatus').html(html1);
        }
        
        if(data2.length==0){
            $('.hide-e-s').css('display','none');
        }else{
            var html1 =''; 
            console.log(data2);
            for(var i = 0; i<data2.length; i++){
                html1 +='<div><span class="label label-sm label-success mr-10 ml-10" style="margin-top:8px">'+data2[i][0].role+'</span></div><br>';
            }
            $('#fillEmployeeStatus').html(html1);
        }
        
                   
    
        
    },

    subrole:function(){
        alert('i am here ');
        var code  = $('#hidcode').val();
        var name  = $('#user_name').val();
        var email1= $('#user_email').val();
            email = email1.trim();
    	var role  = $('#rolecode').val();
        var error = $("#errorhid").val();
    	  
        if( name=="" || email.length==0 || !email){
            toasterrormsg("Fill all valid Fields.");
        } else {
            $.ajax({
                type: "POST",
                url: "ajax/userinfo_ajax.php?type=rolesub",
                data:{code:code,name:name,email:email,role:role},
                cache: false,
                beforeSend: function(){
                    loading();
                },
                success: function(result){
                    var result = $.parseJSON(result);
                    unloading();
                    if(result.success==true){
                        toastmsg("Successfully Uploaded.");
                        $("#Addrole").modal("hide");
                        User.init();
                    } else {
                        toasterrormsg(result.mail);
                    }
                }
            });
        }
    },

};

$(document).ready(function(){ 
    $(document).on('change', '.select-users-id', function() {
        var checked = (this.checked) ? true : false;
        if(checked){
          $(this).attr('checked','checked');
          var code = $(this).val();
          userids[$(this).attr('id')] = {'userId': $(this).val(),'mngrId':$(this).attr('myval'),'role':$('#role_'+code).val(),'email':$('#ue_'+code).val(),'name':$('#un_'+code).val()};
        }else{
          $(this).removeAttr('checked','checked');
          delete userids[$(this).attr('id')];
        }
    });

    $(document).on('click','.paginate_button',function(){
        $('.select2Me').select2();
    });
    //$(".paginate_button").on("click", "a", function() { $('.select2Me').select2(); });

});


function setRole_for_all(role_id){
    $('.select2Me option').removeAttr('selected');
    $('.select2Me option[value='+role_id+']').attr('selected','selected');
    $('.select2Me').select2('destroy').select2();
}
