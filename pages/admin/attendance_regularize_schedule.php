<?php
session_start();
include "../db_conn.php";
include "../configdata.php";

if((!isset($_SESSION['usercode']) || $_SESSION['usercode']=="")&& (!isset($_SESSION['usertype']) || $_SESSION['usertype']=="")){
	header('location: ../login/index.php');
}
include "../include/preFunction.php";
$code=$_SESSION['usercode'];
$type=$_SESSION['usertype'];
$c_val = 'end1234';

?>

<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
   <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN PAGE STYLES -->
    <link href="../../assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
    <link href="../../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/jstree/dist/themes/default/style.css"/>
    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="../../assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/layout2/css/kunal.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->

    <link href="../css/toastr.css"  rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link href="../Attendance/css/sol.css"  rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="../../assets/admin/layout2/css/bootstrap-multiselect.css" type="text/css"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white custom-layout">
<!-- BEGIN HEADER -->
<?php  include('../include/header.php'); ?>

<!-- END HEADER -->
    <div class="clearfix"></div>

        <div class="page-content-wrapper cus-dark-grey">
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper modified">

                    <div class="page-sidebar navbar-collapse collapse cus-dark-grey">

                        <?php include('../include/leftMenu.php') ;?>

                    </div>
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <?php include ('content/attendance_regularize_content.php');?>      
            </div>
                    <!-- BEGIN CONTENT -->
        </div>

   
	 <!-- END CONTENT -->
		
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="page-footer">
		<?php include('../include/footer.php') ?>
	</div>
	<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="../../assets/admin/pages/scripts/components-dropdowns.js"></script>
<script src="../Attendance/js/sol.js"  type="text/javascript"></script>
<script src="../js/toastr.js"  type="text/javascript"></script>
<script src="../js/common.js"  type="text/javascript"></script>
<script type="text/javascript" src="../../assets/admin/layout2/scripts/bootstrap-multiselect.js"></script>


<!-- END PAGE LEVEL SCRIPTS -->
<script>
 jQuery(document).ready(function() {       
    Metronic.init(); 
	Layout.init(); 
	Demo.init(); 
    ComponentsDropdowns.init();
  });
$("#select_schedular_type").change(function(){
    if($(this).val() == 1) {
        $("#display_month").css('display','none');
        $("#display_week").css('display','block');
    } else {
        $("#display_week").css('display','none');
        $("#display_month").css('display','block');
    }
});

$("#select_schedular_type1").change(function(){
    if($(this).val() == 1) {
        $("#display_month1").css('display','none');
        $("#display_week1").css('display','block');
    } else {
        $("#display_week1").css('display','none');
        $("#display_month1").css('display','block');
    }
});

$("#submit_schedular_record").click(function(){
    var week_day = $("#select_week").val();
    var month_day = $("#select_month").val();
    var schedular_type = $("#select_schedular_type").val();
    var running_month_day = 0;
    var running_week_day = 0;
    if($("#select_week").val().length != 0) {
        running_week_day = $("#select_week").val();
    } else if($("#select_month").val().length != 0) {
        running_month_day = $("#select_month").val();
    }

    $.ajax({
        type: "POST",
        url: "ajax/add_attendanceRegularizeSchedular.php",
        data: {type:'add',running_week_day:running_week_day,running_month_day:running_month_day,schedular_type:schedular_type},
        cache:false,
        success: function (data) {
            if(data) {
                alert('Success');
                location.reload();
            } else {
                alert('Fail');
            }
        }
    });    
});

$("#edit_schedular_record").click(function(){
    var week_day = $("#select_week").val();
    var month_day = $("#select_month").val();
    var schedular_type = $("#select_schedular_type1").val();
    var running_month_day = 0;
    var running_week_day = 0;
    if($("#select_week").val().length != 0 && schedular_type === '1') {
        running_week_day = $("#select_week").val();
    } else if($("#select_month").val().length != 0 && schedular_type === '2') {
        running_month_day = $("#select_month").val();
    }

    $.ajax({
        type: "POST",
        url: "ajax/add_attendanceRegularizeSchedular.php",
        data: {type:'edit',running_week_day:running_week_day,running_month_day:running_month_day,schedular_type:schedular_type},
        cache:false,
        success: function (data) {
            if(data) {
                alert('Success');
                location.reload();
            } else {
                alert('Fail');
            }
        }
    });    
});
</script>
<!-- END GOOGLE RECAPTCHA -->
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
