<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue z-depth-1">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-edit"></i>Manage User
				</div>	
			</div>
			<div class="portlet-body">
				<form class="form-horizontal">
					<div class="form-body">
						<div class="form-group">
							<div class="col-md-4 pull-right" style="margin :10px auto;">
								<?php
									$query2="SELECT Id,role_name FROM hrms_role WHERE status='Active'";
									$sql2= query($query,$query2,$pa,$opt,$ms_db);
									
									?>
								<select class="selectMe form-control" name="role_for_all" id="role_for_all" onchange="setRole_for_all(this.value);">
									<?php  $option = ''; 
								$option .="<option value=''>Select Role For All</option>";
									$j=0; while($roleRow = $fetch($sql2)){ 
										
										$option .="<option value='".$roleRow['Id']."'>".$roleRow['role_name']."</option>";
											
									$j++; } echo $option; ?>

								</select>							
							</div>
							

							<div class="col-md-12" id="showuser">
								
							</div>
							<div class="col-md-4 align-left" id="btn-for-all">
								<button type="button" class="btn btn-block blue" onclick="Role.CreateMultipleUsers();">
									Create All Selected Users<i class="fa fa-user"></i>
								</button>
							</div>
						</div>
					</div>
				
					<div class="modal fade" id="AddRole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
						<!--User Role Dialog Box START here add by surendra vimal-->
						<div class="modal-dialog">
						    <div class="modal-content">
						      	<!-- Fill Invalide Manager Status here -->
						      	<div class="modal-header modal-color white-color hide-e-m-s" style="padding:8px;">
						      		<h4 class="modal-title h5" id="myModalLabel">Following Users have not their Manager</h4>
						      	</div>
						      	<div class="modal-body hide-e-m-s">
						      		<div class="col-md-12" id="fillUsers">
									</div>	
								</div>
								<!-- Fill Manager Status here -->
								<div class="modal-header modal-color white-color hide-m-s" style="padding:8px;">
						        	<h4 class="modal-title h5" id="myModalLabel2">Manager Status</h4>
						      	</div>
								<div class="modal-body hide-m-s">
									<div class="col-md-12" id="fillManagerStatus">
									</div>
								</div>

								<!-- Fill Empoyee Status here -->
								<div class="modal-header modal-color white-color hide-e-s" style="padding:8px;">
						        	<h4 class="modal-title h5" id="myModalLabel3">Employee Status</h4>
						      	</div>
								<div class="modal-body hide-e-s">
									<div class="col-md-12" id="fillEmployeeStatus">
									</div>
								</div>
								

								<div class="modal-footer">
									<div class="col-md-12">
										<div class="col-md-offset-4 col-md-4">
											<button type="button" class="btn btn-block blue" data-dismiss="modal" aria-label="Close">Close</button>
										</div>
									</div>
								</div>
						    </div>
						</div>
						<!--User Role Dialog Box END here add by surendra vimal-->
					</div>	
				</form>			
			</div>
		</div>

	</div>
</div>