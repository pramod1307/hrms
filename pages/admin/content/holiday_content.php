<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Holidays
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form  enctype="multipart/form-data" id="form" name="reportform" class="form-horizontal form-row-seperated">
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 control-label">Holiday Date</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control input-medium" name="holiDate" id="holiDate" placeholder="dd/mm/yy">
                                            </div>
                                        <label class="col-md-2 control-label">Location</label>
                                            <div class="col-md-3" id="locdiv">
                                                <select id="loc" name="multiselect[]" multiple="multiple" style="width: 300px">
                                               
                                                <?php 
                                                $sql1="select LOC_NAME,LOC_CODE from LOCMAST"; 
                                                $result1 = query($query,$sql1,$pa,$opt,$ms_db);
                                                while($row1 = $fetch($result1)){ ?> 
                                                    <option value="<?php echo $row1['LOC_CODE'] ?>">
                                                    <?php echo $row1['LOC_NAME']; ?>
                                                    </option>
                                                <?php }?>
                                                </select>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-2 control-label">Holiday Name</label>
                                                <div class="col-md-3" >
                                                    <div class="Search">
                                                        <input type="text" class="form-control" name="term" id="search">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3" >
                                                <div class="col-md-1" style="margin-top: 25px;margin-left: 995px">
                                                        <button type="button" id="showorg" value="1" class="btn  bg-blue" onclick="showAdvanceFilter(this.value,'<?php echo $code;?>');">Advance</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="advancefilter">
                                <div class="form-group" id="multisearch" style="display: none">    
                                    <div class="col-md-12" >
                                        <div class="col-md-4 selectKunal" id="compdiv" style="margin-top: 25px;">
                                            <select id="company-select" name="multiselect[]" multiple="multiple" style="width: 300px">
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" id="busdiv" style="margin-top: 25px;">
                                            <select id="business-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" id="subbusdiv" style="margin-top: 25px;">
                                            <select id="sub-business-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="form-group" id="multisearch1" style="display: none">    
                                    <div class="col-md-12" >
                                        <div class="col-md-4 selectKunal" id="wlocdiv" style="margin-top: 25px;">
                                            <select id="work-location-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" id="funcdiv" style="margin-top: 25px;">
                                            <select id="function-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" id="subfuncdiv" style="margin-top: 25px;">
                                            <select id="sub-function-select" name="multiselect[]" multiple="multiple" style="width: 300px">
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group" id="multisearch2" style="display: none">    
                                    <div class="col-md-12" >
                                        <div class="col-md-4 selectKunal" id="costdiv" style="margin-top: 25px;">
                                            <select id="cost-master-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" id="procdiv" style="margin-top: 25px;">
                                            <select id="process-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" id="grddiv" style="margin-top: 25px;">
                                            <select id="grade-select" name="multiselect[]" multiple="multiple" style="width: 300px">
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group" id="multisearch3" style="display: none">    
                                    <div class="col-md-12" >
                                        <div class="col-md-4 selectKunal" id="desgdiv" style="margin-top: 25px;">
                                            <select id="designation-select" name="multiselect[]" multiple="multiple" style="width: 300px">

                                                  
                                            </select>
                                        </div>
                                                                              
                                    </div>
                                </div>
                                 <div class="row">
                                            <div class="col-md-3" >
                                                <div class="col-md-1" style="margin-top: 25px;margin-left: 995px">
                                                        <button type="button" id="subholi" value="1" class="btn  bg-blue" onclick="submitholiday();">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                               
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>