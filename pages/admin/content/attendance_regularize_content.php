<div class="page-content-wrapper" >
<div class="page-content cus-light-grey">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				Employee Regularize Schedular
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse">
				</a>
				<a href="javascript:;" class="reload">
				</a>
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
				<div class="form-body">
					<h4 class="form-section"></h4>
					<div class="form-group">
						<div class="row">
							<?php 
								$sql = "SELECT * FROM attendance_regularize_schedular";
								$stmtRows= query($query,$sql,$pa,$opt,$ms_db);
								$rowCount = $num($stmtRows);
							?>
							<?php if($rowCount == 0) { ?>
							<div class="col-md-6">
								<select class="form-control input-large" id="select_schedular_type" data-placeholder="Select Schedular Type">
									<option value="">Select Schedular Type</option>
									<option value="1"> Weekly </option>
									<option value="2"> Monthly </option>
								</select>
							</div>

							<div class="col-md-6" id="display_week">
								<select class="form-control input-large " id="select_week" 
								data-placeholder="Select Schedular Running Day">
									<option value="">Select Schedular Running Day...</option>
									<option value="1"> Sunday </option>
									<option value="2"> Monday </option>
									<option value="3"> Tuesday </option>
									<option value="4"> Wednusday </option>
									<option value="5"> Thursday </option>
									<option value="6"> Friday </option>
									<option value="7"> Saturday </option>																		
								</select>
							</div>

							<div class="col-md-6" style="display: none;" id="display_month">
								<select class="form-control input-large"  id="select_month" data-placeholder="Select Schedular Running Day...">
									<option value="">Select Schedular Running Date...</option>
									<?php
									for($i = 1; $i < 29; $i++) {
										echo "<option value='".$i."'>" .$i. "</option>";
									}
									?>																	
								</select>
							</div>	
						<?php } else {
							 $rowRecord = $fetch($stmtRows);
							 // print_r($rowRecord); die;
							 ?>
							<div class="col-md-6">
								<select class="form-control input-large" id="select_schedular_type1" data-placeholder="Select Schedular Type">
									<option value="">Select Schedular Type</option>
									<option value="1" <?php echo ($rowRecord['schedular_type'] == '1') ? 'selected' : ''; ?>> Weekly </option>
									<option value="2" <?php echo ($rowRecord['schedular_type'] == '2') ? 'selected' : ''; ?>> Monthly </option>
								</select>
							</div> 
							<?php 
								$week_array = array(1 =>'Sunday', 2 =>'Monday', 3 =>'Tuesday', 4 =>'Wednusday', 5 =>'Thursday', 6 =>'Friday', 7 =>'Saturday'); 
							 	$display = ($rowRecord['schedular_type'] == '1') ? 'block': 'none';
								?>
								<div class="col-md-6">								
									<div class="col-md-6"  style="display:<?php echo $display; ?>" id="display_week1" >
										<select class="form-control input-large"  id="select_week" data-placeholder="Select Schedular Running Day...">
											<option value="">Select Schedular Running Day...</option>
											<?php
												for($i = 1; $i < 8; $i++) {
													$select_val = ($rowRecord['running_week_day'] == $i) ? 'selected' : '';
													echo "<option value='".$i."' $select_val>". $week_array[$i]." </option>";
												}
											?>																	
										</select>
									</div>								
								</div>
								<?php 
								$display = ($rowRecord['schedular_type'] == '2') ? 'block': 'none';
								?>
								<div class="col-md-6" style="display:<?php echo $display; ?>" id="display_month1">
									<select class="form-control input-large"  id="select_month" data-placeholder="Select Schedular Running Date...">
										<option value=""> Select Schedular Running Date... </option>
										<?php
											for($i = 1; $i < 29; $i++) {
												$select_val = ($rowRecord['running_month_day'] == $i) ? 'selected' : '';
												echo "<option value='".$i."' $select_val>" .$i. "</option>";
											}
										?>																	
									</select>
								</div>	
						<?php } ?>
						</div>
					</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
						<?php if($rowCount == 0) { ?>
							<button type="button" id="submit_schedular_record" value="" class="btn purple"><i class="fa fa-check"></i> 
							Submit </button>
						<?php } else { ?>
							<button type="button" id="edit_schedular_record" value="" class="btn purple"><i class="fa fa-check"></i> 
							Update </button>
						<?php } ?>
						</div>
					</div>
				</div>
			
			<!-- END FORM-->
		</div>
	</div>
</div>
</div>