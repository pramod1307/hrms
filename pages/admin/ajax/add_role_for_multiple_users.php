<?php
include("../../db_conn.php");
include_once('../../configdata.php');
//ini_set("display_errors","on");   
//error_reporting(E_ALL);
//session_start();
$type=$_GET['type'];

if($type=="set_role"){
	$array = array();
	foreach ($_POST as $key => $value) {
		$temp = explode("_", $key); 
		$array[] = $temp[1];
	}

	$returnResult  		=array();			//users those are newly created
	$returnEmptyMngr 	=array();			//if manager code empty in column
	$returnCreatedMngr 	=array(); 			//if manager code existed and create them as manager 
	$statusOfMngr 		=array();

	foreach ($array as $key => $value) {

		$code  		= $_POST['cu_'.$value]['userId'];
		$name		= $_POST['cu_'.$value]['name'];
		$email 		= empty($_POST['cu_'.$value]['email'])?'':$_POST['cu_'.$value]['email'];
		$role 		= $_POST['cu_'.$value]['role'];
		$codecrypt 	= md5($code);
		$emailcrypt = empty($email)?'':md5($email);
		$date 		= date("Y-m-d H:i:s");
		$status 	= 1;
		$emailReturnResult  = '';
		$userCreated 		= '';
		$defaulPassword = getDefaultPassword($code);
		
		
		if(empty($_POST['cu_'.$value]['mngrId'])){
			$returnEmptyMngr[] = array('user'=>$_POST['cu_'.$value]['name']);
		}else{
			$mngrId = $_POST['cu_'.$value]['mngrId'];
			$sqlQuery = "select Emp_FName,OEMailID from hrdmast where emp_code ='".$mngrId."'";
			$sqlQExecute = query($query,$sqlQuery,$pa,$opt,$ms_db);
			$mngrRole='';
			if($num($sqlQExecute) >=1){
				while($row=$fetch($sqlQExecute)){
					$name1 = $row['Emp_FName'];
					$email1 =$row['OEMailID']; 
				}
				$mngrRole = createRole('2', $name1, $mngrId, $email1, $status, $user,$date);
				$statusOfMngr[] = array('user' =>$_POST['cu_'.$value]['name'],'manager'=>$mngrRole);
			}else{
				$statusOfMngr[] = array('user' =>$_POST['cu_'.$value]['name'],'manager'=>'Invalide Manager Code' );
			}

		}
		
			
		$returnResult[] = createUsers($code,$name,$email,$role,$codecrypt,$emailcrypt,$date,$status,$emailReturnResult,$userCreated,$defaulPassword);	
		
	}
	echo json_encode(array('success'=>TRUE,'data'=>$returnResult,'emptyMngr'=>$returnEmptyMngr,'statusOfMngr'=>$statusOfMngr));
}

//Begin set default password
function getDefaultPassword($emp_code){
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $fetch;

	$userPass = "";
	$passVar  = array();
    $passComb = "";
    $date_var = array("DOB","DOJ");
    $policySql= "Select default_password_status,default_password from PasswordPolicy";
    $policyRes= query($query,$policySql,$pa,$opt,$ms_db);
   
    $policyRow=$fetch($policyRes);
   
    if($policyRow['default_password_status'] == 1){
    	$passVar=explode(",", $policyRow['default_password']);
        $getPassSql = "SELECT ";
        for ($i = 0; $i < count($passVar); $i++){
            if($i>0) $getPassSql .=",";
            if(in_array($passVar[$i],$date_var)){
                $getPassSql .="Replace(convert(varchar(10),DOJ,105),'-','') as $passVar[$i]" ;
            } else {
                $getPassSql .="$passVar[$i]";
            }
        }
        $getPassSql .=" FROM HrdMastQry WHERE Emp_Code='$emp_code' ";
        $getPassRes = query($query,$getPassSql,$pa,$opt,$ms_db);

        while($getPassRow = $fetch($getPassRes)){
            for ($j = 0; $j < count($passVar); $j++){
                $passComb.=$getPassRow[$j];
            }
            $userPass = md5($passComb);
        }
        return $userPass;
    }
}
//End set default password



//Begin create users with user's role 
function createUsers($code,$name,$email,$role,$codecrypt,$emailcrypt,$date,$status,$emailReturnResult,$userCreated,$defaulPassword){
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $fetch;
	global $num;
	$sql="select * from users where userID='$codecrypt'";
	$sqlre= query($query,$sql,$pa,$opt,$ms_db);
	$returnResult1 = array();
	if($num($sqlre) >=1){
		$userCreated = "aleady existed !";
	}else{
		$usertable = query($query,"insert into users (userId, username, UserPWD , useremailid, islocked, userActive,UserType) values ('$codecrypt','$name','$defaulPassword','$emailcrypt','N','1','U') ",$pa,$opt,$ms_db);	
		$userCreated = "successfully";
		if(!empty($email)){
			$to=$email;
			$message=welcome_msg($code);
			$subject='Welcome message !';
			//$emailReturnResult = mymailer1('donotreply@sequelone.com',$subject,$message,$to);
		}
	}  
	
	$sqlc="select * from usersroles where userid='$code' and roleId='".$role."' ";
	$sqlrec= query($query,$sqlc,$pa,$opt,$ms_db);
	
	if($num($sqlrec) >=1){
		$userRoleMsg = "Role aleady assigned to ".$name;
		$returnResult1[] = array('mail'=>$emailReturnResult,'role'=>$userRoleMsg);
	}else{
		$result = query($query,"insert into usersroles (roleId, username, userid, menucode, actions, createdby, createdon) values ('$role','$name','$code','$email','$status',  '$user','$date') ",$pa,$opt,$ms_db);
		if($result) {
			$userRoleMsg = "Role assigned successfully to ".$name;
			$returnResult1[] =array('mail'=>$emailReturnResult,'role'=>$userRoleMsg,'userCreated'=>$userCreated) ;
		}else{
			$userRoleMsg = "Role not assigned to ".$name;
			$returnResult1[] = array('mail'=>$emailReturnResult,'role'=>$userRoleMsg,'userCreated'=>$userCreated);
		}
	}

	return $returnResult1;

}
//End create users with user's role

//Begin create Role for Manager if MngrCode is valide 
function createRole($roleId,$code,$name,$email,$status,$user,$date){
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $fetch;
	global $num;

	$sqlc="select * from usersroles where userid='$code' and roleId='$roleId' ";
	$sqlrec= query($query,$sqlc,$pa,$opt,$ms_db);
	$text='';
	if($num($sqlrec) >=1){
		$text = "Manager aleady created ".$name."(".$code.")";
	}else{
		$result = query($query,"insert into usersroles (roleId,username,userid,menucode, actions,createdby,createdon) values ('2','$name','$code','$email','$status','$user','$date') ",$pa,$opt,$ms_db);
		if($result) {
			$text = "Manager created ".$name."(".$code.")";
		}else{
			$text = "Manager not created ".$name."(".$code.")";
			
		}
	}
	return $text;
}
//End create Role for Manager if MngrCode is valide

 
?>

