<?php
session_start();
include ('../db_conn.php');
include ('../configdata.php');

global $emp_code;

	if($_SESSION['usertype']=='A' || Is_hr($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
		$emp_code = "HR";
	}else if($_SESSION['usertype']=='U' && Is_mngr($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
		$emp_code.="'".$_SESSION['usercode']."',";
		$sql_query = "SELECT Emp_Code FROM HrdTran WHERE MNGR_CODE='".$_SESSION['usercode']."'";
	
		$fetch_result=query($query,$sql_query,$pa,$opt,$ms_db);
        while($row = $fetch($fetch_result)){
            $emp_code	.=	$row['Emp_Code'].',';                                    
        }
        $emp_code = rtrim($emp_code,',');
	}else if($_SESSION['usertype']=='U' && Is_emp($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
		$emp_code = $_SESSION['usercode'];
	}
	
	$emp_codearray		=	explode(",",$emp_code);
	
$valueForFillExcelData =array();	
	//get search term
$searchTerm = $_GET['term'];
$data=	'';    
$creatNewFile	=	0;
if ($handle = opendir('ajax/upload/')) {
    while (false !== ($file = readdir($handle)))
    {
        if ($file != "." && $file != ".." && strtolower(substr($file, strrpos($file, '.') + 1)) == 'json')
        {
            $thelist[]= $file;
        }
    }
    closedir($handle);
}

if(isset($thelist) && !empty($thelist))
$file = basename($thelist[0], ".json");
else
$creatNewFile	=	1;
	

$getTimeAfterFiveHours = time() + 1*60*60;
$checkTime = time();

if($checkTime >$file){ 
$creatNewFile	=	1;
}

//get matched data from skills table
	 if($creatNewFile == 1){
		$sqlq="SELECT Status_Code,DOJ,EMP_NAME,Emp_Code,COMP_CODE,FUNCT_CODE,SFUNCT_CODE,GRD_CODE,LOC_CODE,MNGR_CODE,WLOC_CODE,DSG_CODE,BussCode,SUBBuss_Code,TYPE_CODE,PROC_CODE,COST_CODE,Divi_Code,
		DEPT_NAME,DSG_NAME,COMP_NAME,FUNCT_NAME,SUBFUNCT_NAME,GRD_NAME,LOC_NAME,MNGR_NAME,WLOC_NAME,BUSSNAME,SUBBUSSNAME,TYPE_NAME,PROC_NAME,COST_NAME,DIVI_NAME FROM HrdMastQry";
		//$sqlq="SELECT Top 20 EMP_NAME,Emp_Code FROM HrdMastQry WHERE (Emp_Code LIKE '%".$searchTerm."%') OR (EMP_NAME LIKE '%".$searchTerm."%') ORDER BY EMP_NAME ASC";
	 }else{
		 $sqlq="SELECT EMP_NAME,Emp_Code FROM HrdMastQry";
	 }
	$resultq=query($query,$sqlq,$pa,$opt,$ms_db);
    if($num($resultq)) {
		
        while ($rowq = $fetch($resultq)) {
			
			if(isset($emp_code) && $emp_code == 'HR'){
			$data	.= '<option value="'.$rowq['Emp_Code'].'">'.$rowq['EMP_NAME'].' ( '.$rowq['Emp_Code'].')</option>';
			}else{
				if(in_array($rowq['Emp_Code'],$emp_codearray)){
				$data	.= '<option value="'.$rowq['Emp_Code'].'">'.$rowq['EMP_NAME'].' ( '.$rowq['Emp_Code'].')</option>';
				}
			}
		   if($creatNewFile == 1){
				   
				$valueForFillExcelData[$rowq['Emp_Code']] = array(
																"Emp_Code" => $rowq['Emp_Code'], 
																"EMP_NAME" => $rowq['EMP_NAME'], 
																"FUNCT_CODE" => $rowq['FUNCT_CODE'], 
																"SFUNCT_CODE" => $rowq['SFUNCT_CODE'], 
																"Department" => "0", 
																"DSG_CODE" => $rowq['DSG_CODE'], 
																"LOC_CODE" => $rowq['LOC_CODE'], 
																"GRD_CODE" => $rowq['GRD_CODE'], 
																"WLOC_CODE" => $rowq['WLOC_CODE'], 
																"BussCode" => $rowq['BussCode'], 
																"SUBBuss_Code" => $rowq['SUBBuss_Code'], 
																"PROC_CODE" => $rowq['PROC_CODE'], 
																"COST_CODE" => $rowq['COST_CODE'], 
																"Divi_Code" => $rowq['Divi_Code'], 
																"COMP_CODE" => $rowq['COMP_CODE'], 
																"MNGR_CODE" => $rowq['MNGR_CODE'], 
																"TYPE_CODE" => $rowq['TYPE_CODE'], 
																"DEPT_NAME" => $rowq['DEPT_NAME'], 
																"DSG_NAME" => $rowq['DSG_NAME'], 
																"COMP_NAME" => $rowq['COMP_NAME'], 
																"FUNCT_NAME" => $rowq['FUNCT_NAME'], 
																"SUBFUNCT_NAME" => $rowq['SUBFUNCT_NAME'], 
																"GRD_NAME" => $rowq['GRD_NAME'], 
																"LOC_NAME" => $rowq['LOC_NAME'], 
																"MNGR_NAME" => $rowq['MNGR_NAME'], 
																"WLOC_NAME" => $rowq['WLOC_NAME'], 
																"BUSSNAME" => $rowq['BUSSNAME'], 
																"SUBBUSSNAME" => $rowq['SUBBUSSNAME'], 
																"TYPE_NAME" => $rowq['TYPE_NAME'], 
																"PROC_NAME" => $rowq['PROC_NAME'], 
																"DIVI_NAME" => $rowq['DIVI_NAME'], 
																"DOJ" => $rowq['DOJ'], 
																"Status_Code" => $rowq['Status_Code'], 
																"DSG_CODE" => $rowq['DSG_CODE'], 
																"COST_NAME" => $rowq['COST_NAME']
																); 
																
		   }
	   
	   }
	   
		//$fileName 	=	getDirectoryUrl().'ajax/upload/'; 
		if($creatNewFile == 1){
			unlink('ajax/upload/'.$file.'.json');
			$fp = fopen('ajax/upload/'.$getTimeAfterFiveHours.'.json', 'w')or die('Cannot open file:  results.json');
			//file_put_contents($fileName.'results.json', json_encode($p));
			fwrite($fp, json_encode($valueForFillExcelData));
			fclose($fp);	
		}
		
    }
//return json data
    echo $data;

function getDirectoryUrl(){
	
	$url = $_SERVER['REQUEST_URI']; //returns the current URL
	$parts = explode('/',$url);
	$dir = $_SERVER['SERVER_NAME'];
	for ($i = 0; $i < count($parts) - 1; $i++) {
	 $dir .= $parts[$i] . "/";
	}
	return $dir;
}	


?>
