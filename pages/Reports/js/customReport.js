jQuery(document).ready(function() {
	
	
	$(".filter_advance").live('click',function(){
		
		if ($(".firstTimeLoad")[0]){
			$.ajax({
					type:"POST",
					url: "ajax/custom_report_filter.php",
					success: function (data) {
						$(".advanceFilterOption").html(data);
						loadMultiSelect();
					}
			});
			 $(".advanceFilterOption").removeClass("firstTimeLoad");
		}
		
		$( ".advanceFilterOption" ).slideToggle( "slow", function() {
			if($('.changeimage').attr('color') == '1'){
				$('.changeimage').removeClass( "glyphicon-plus" ).addClass( "glyphicon-minus" );
				$('.changeimage').attr('color','2');
			}else{
				$('.changeimage').removeClass( "glyphicon-minus" ).addClass( "glyphicon-plus" )
				$('.changeimage').attr('color','1');
			}
			 
		});	
	});
		
	$("#customReport").live('change',function(){
		var customReprtType 	=	$("#customReport").val();
		//alert(customReprtType);
		$(".help-block-date1").css('display','none');
		$(".help-block-date").css('display','none');
		$(".help-block-month").css('display','none');
		$(".help-block-year").css('display','none');
		if(customReprtType	==	'2'){
			$(".yearMonth").css("display","");
			$(".dateSelection").css("display","none");
			$(".rtypeValue").css("display","none");
			$(".dailyReport").css("display","none");
			$(".monthFor").css("display","none");
			$(".shift").css("display","");
			$(".month").css("display","none");	
		}else if(customReprtType	==	'4' ){
			$(".rtypeValue").css("display","");
			$(".dailyReport").css("display","");
			$(".shift").css("display","");
			$(".dateSelection").css("display","none");
			$(".month").css("display","none");
			$(".yearMonth").css("display","none");
		}else if(customReprtType	==	'5' || customReprtType	==	'10' || customReprtType	==	'13'  || customReprtType	==	'21' ){
			$(".rtypeValue").css("display","none");
			$(".dailyReport").css("display","none");
			$(".dateSelection").css("display","none");
			$(".yearMonth").css("display","none");
			$(".shift").css("display","none");
			$(".year").css("display","");
			$(".month").css("display","");
		}else if(customReprtType	==	'11'){
			$(".rtypeValue").css("display","none");
			$(".dailyReport").css("display","");
			$(".shift").css("display","");
			$(".dateSelection").css("display","none");
			$(".month").css("display","none");
			$(".yearMonth").css("display","none");
		}else if(customReprtType	==	'16'){
			$(".rtypeValue").css("display","none");
			$(".dailyReport").css("display","");
			$(".dateSelection").css("display","none");
			$(".month").css("display","none");
			$(".yearMonth").css("display","none");
			$(".dateSelection").css("display","none");
			$(".dailyReport").css("display","block");
			$(".shift").css("display","none");
			$(".status").css("display","none");
			$(".employee").css("display","none");
			

		}else if(customReprtType	==	'17'){
		
			$(".rtypeValue").css("display","none");
			$(".dailyReport").css("display","none");
			$(".dateSelection").css("display","none");
			$(".month").css("display","none");
			$(".yearMonth").css("display","none");
			$(".dateSelection").css("display","block");
			$(".dailyReport").css("display","none");
			$(".shift").css("display","none");
			$(".status").css("display","none");
			$(".employee").css("display","none");
			

		}
		else if(customReprtType	==	'18'){
		
			$(".rtypeValue").css("display","none");
			$(".dailyReport").css("display","none");
			$(".dateSelection").css("display","none");
			$(".month").css("display","none");
			$(".yearMonth").css("display","none");
			$(".dateSelection").css("display","block");
			$(".dailyReport").css("display","none");
			$(".shift").css("display","none");
			$(".status").css("display","block");
			$(".employee").css("display","block");
			

		}
    	else if(customReprtType	==	'22'){
			$(".dateSelection").css("display","none");
			$(".shift").css("display","none");
			$(".status").css("display","none");
			$(".advancedFilter").css("display","none");
		}		
		else{
			$(".yearMonth").css("display","none");
			$(".dateSelection").css("display","");
			$(".rtypeValue").css("display","none");	
			$(".dailyReport").css("display","none");
			$(".shift").css("display","");
			$(".month").css("display","none");
		}
	});
	
	$(".submitCustomReport").live('click',function(){
		
			var customReport 		=	$("#customReport").val();	
			var CompanyName			=	$("#company").val();
			var functionName		=	$("#function").val();
			var sfunction			=	$("#sfunction").val();
			var grade				=	$("#grade").val();
			var location1			=	$("#location").val();
			var mname				=	$("#mname").val();
			var designation			=	$("#designation").val();
			var bu					=	$("#bu").val();
			var sbu					=	$("#sbu").val();
			var wlocation			=	$("#wlocation").val();
			var ccenter				=	$("#ccenter").val();
			var division			=	$("#division").val();
			var rcode				=	$("#rcode").val();
			var employeeType		=	$("#employeeType").val();
			var process				=	$("#process").val();
			var shift				=	$("#shift").val();
			var employee			=	$("#employee").val();
			var statusName			=	$("#status").val();
			var year				=	$("#year").val();
			var month				=	$("#month").val();
			var fromDate			=	$("#fromDate").val();
			var toDate				=	$("#toDate").val();
			var daily 				=	$("#daily").val();
			var monthFor			=	$("#monthFor").val();
			var error				=	0;
			var emp_code_list		= 	$("#emp_code_list").val();
			var customReprtType 	=	$("#customReport").val();
			
			$(".help-block-date1").css('display','none');
			$(".help-block-date").css('display','none');
			$(".help-block-month").css('display','none');
			$(".help-block-year").css('display','none');
			if(customReprtType != 0){
				
				if(customReprtType == '2'){
					
					if(month==	'null' || month == '' || month == null){
						error=	1;
						$(".help-block-month").css('display','');
					}
					if(year 	==	'0'){ 
						error=	1;
						$(".help-block-year").css('display','');
					
					}
				}else if(customReprtType == '10' || customReprtType == '13'){
					
					if(year 	==	'0'){ 
						error=	1;
						$(".help-block-year").css('display','');
					
					}
				
				}else if(customReprtType == '5'){
					
					if(year 	==	'0'){ 
						error=	1;
						$(".help-block-year").css('display','');
					
					}
				}else if(customReprtType == '11' || customReprtType == '4'){
					
					if(daily==	'null' || daily == '' || daily == null){
						error=	1;
						$(".help-block-date").css('display','');
					}
				}else if(customReprtType == '12' || customReprtType == '14' || customReprtType == '3' || customReprtType == '6'	|| customReprtType == '7'	|| customReprtType == '8'	|| customReprtType == '9'	|| customReprtType == '15'){
					if(fromDate==	'null' || fromDate == '' || fromDate == null){
						error=	1;
						$(".help-block-date1").css('display','');
					}
					if(toDate==	'null' || toDate == '' || toDate == null){
						error=	1;
						$(".help-block-date1").css('display','');
					}
				}	
				
				if(error == '0'){
					$(".loading").css("display","");
					$(".error_class").css('display','none');
					$(".error_class").css('display','none');
					
					$.ajax({
							type:"POST",
							url: "ajax/custom-report-generation.php",
							data: {type:customReport,monthFor:monthFor,fromDate :  fromDate,daily:daily,toDate :  toDate,CompanyName :  CompanyName, functionName: functionName, sfunction:sfunction,grade :  grade, location1: location1, mname:mname,designation :  designation
							, bu: bu, sbu:sbu,wlocation :  wlocation, ccenter: ccenter, division:division, rcode:rcode,employeeType :  employeeType, process: process, shift:shift
							, employee:employee,statusName :  statusName, year: year, month:month,emp_code_list:emp_code_list},
							complete:function (data) {
								$(".loading").css("display","none");
							},	
							success: function (data) {
								var data1 	=	JSON.parse(data);
								//alert(url+'ajax/'+data1.fileName);	
								if(data1.error	==	'Error'){
									alert("Something Missing");
								}else{
									document.location.href		=	url+'ajax/'+data1.fileName;
								}
							}

					});
				
				}
				
			}else{
				alert("Please Select Report")
			}
			
	
	
	
	});	
		
	
	
	$(window).load(function() {
		
			$('#designation').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#bu').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true    });			
			$('#sbu').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true    });			
			$('#wlocation').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#ccenter').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#division').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#rcode').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#employeeType').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
		//	$('#qualification').multiselect({ includeSelectAllOption: true,enableFiltering: true });			
			$('#process').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#shift').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#mname').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#location').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#grade').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#sfunction').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#function').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#company').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#status').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });	
			$('#month').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });	
			$('#rtype').multiselect({ includeSelectAllOption: true,enableFiltering: true , maxHeight: 450,enableCaseInsensitiveFiltering: true });	
			$(".rtypeValue").css("display","none");			
			$(".yearMonth").css("display","none");
			$(".dateSelection").css("display","");
					
			$.ajax({
				type:"POST",
				url: "search.php",
				success: function (data) {
					$("#employee").html(data);
					$('#employee').multiselect({
						includeSelectAllOption: true,
						enableFiltering: true,
						maxHeight: 450,
						enableCaseInsensitiveFiltering: true 
					});	
					$('#loading').hide();
					markPastAttandance();
				  }

			});
			
			
			/*$('.searchable').multiSelect({
				  selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='try \"12\"'>",
				  selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='try \"4\"'>",
				  afterInit: function(ms){
					var that = this,
						$selectableSearch = that.$selectableUl.prev(),
						$selectionSearch = that.$selectionUl.prev(),
						selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
						selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

					that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
					.on('keydown', function(e){
					  if (e.which === 40){
						that.$selectableUl.focus();
						return false;
					  }
					});

					that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
					.on('keydown', function(e){
					  if (e.which == 40){
						that.$selectionUl.focus();
						return false;
					  }
					});
				  },
				  afterSelect: function(){
					this.qs1.cache();
					this.qs2.cache();
				  },
				  afterDeselect: function(){
					this.qs1.cache();
					this.qs2.cache();
				  }
			});
			*/
		});


});	

function markPastAttandance(){
	
	$.ajax({
					type:"POST",
					url: "ajax/markPastAttendance.php",
					success: function (data) {
						
					}
	});
}

function loadMultiSelect(){
	
			$('#designation').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#bu').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true    });			
			$('#sbu').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true    });			
			$('#wlocation').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#ccenter').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#division').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#rcode').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#employeeType').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
		//	$('#qualification').multiselect({ includeSelectAllOption: true,enableFiltering: true });			
			$('#process').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#shift').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#mname').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#location').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#grade').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#sfunction').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#function').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#company').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });			
			$('#status').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });	
			$('#month').multiselect({ includeSelectAllOption: true,enableFiltering: true, maxHeight: 450,enableCaseInsensitiveFiltering: true   });
}