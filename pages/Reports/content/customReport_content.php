<?php 
	global $emp_code;

	if($_SESSION['usertype']=='A' || Is_hr($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
		$emp_code = "HR";
	}else if($_SESSION['usertype']=='U' && Is_mngr($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
		$emp_code.=$_SESSION['usercode'].",";
		$sql_query = "SELECT Distinct(Emp_Code) FROM HrdTran WHERE MNGR_CODE='".$_SESSION['usercode']."'";
	
		$fetch_result=query($query,$sql_query,$pa,$opt,$ms_db);
        while($row = $fetch($fetch_result)){
            $emp_code	.=	$row['Emp_Code'].',';                                    
        }
        $emp_code = rtrim($emp_code,',');
	}else if($_SESSION['usertype']=='U' && Is_emp($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
		$emp_code = $_SESSION['usercode'];
	}
	//$emp_code $this variabl contain employee code//	
	
?>

<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN CONDENSED TABLE PORTLET-->
                <div class="portlet box blue">

                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Custom Report Panel
                        </div>
                      
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
								
                                <div style="margin-bottom:25px;" class="col-md-12">

                                    <form role="form">
										<div class="row">
											<input type="hidden" name="emp_code_list" id="emp_code_list" value="<?php echo $emp_code; ?>" />
											<div class="form-body">
												<div class="col-md-6">	
													<div class="form-group">
														<label>Report Name</label>
														
														<select name="customReport" id="customReport" class="form-control">
															<option value="0">Please Select Report</option>
															<?php $result = $custom_class_obj->getReportsName();
															foreach($result as $key=>$value){
															?>
															<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php } ?>
														</select>
													</div>
												</div>
											</div>
											<div style="display:none" class="form-body dateSelection">	
												<div class="col-md-3">
													<div class="form-body">
														<div class="form-group">
															<label>From</label>
															<input type="text" class="form-control date2me" name="fromDate" id="fromDate" placeholder="dd/mm/yyyy" >
														</div>
													</div>
												</div>
											</div>
											<div style="display:none" class="form-body dateSelection">
												<div class="col-md-3">
													<div class="form-body">
														<div class="form-group">
															<label>To</label>
															<input type="text" class="form-control date2me" name="toDate" id="toDate" placeholder="dd/mm/yyyy"  >
														</div>
													</div>
												</div>
											</div>
											<div style="display:none" class="form-body dailyReport">
												<div class="col-md-3">
													<div class="form-body">
														<div class="form-group">
															<label>Select Date</label>
															<input type="text" class="form-control" name="daily" id="daily" placeholder="dd/mm/yyyy"  >
														</div>
													</div>
												</div>
											</div>	
											<div class="form-body yearMonth year">	
												<div class="col-md-3">
													<div class="form-body">
														<div class="form-group">
															<label>Year</label>
															<select class="form-control input-lg" name="year" id="year">
																<option value="0">Please Select Year</option>
																<?php
																$result		=	$custom_class_obj->yearFunction();
																foreach($result as $key=>$value){
																?>
																	<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																<?php
																}
																?>
															</select>
															<span style="display:none;color:#e73d4a" class="help-block-year error_class"> Please Select Year</span>
														</div>
													</div>
												</div>
											</div>
											<div style="display:none;" class="form-body yearMonth month">
												<div class="col-md-3">
													<div class="form-body">
														<div class="form-group">
															<label>Month</label>
															<select class="form-control input-lg" name="monthFor" id="monthFor">
																
																<?php
																$monthName		=	array("1"=>"JAN","2"=>"FEB","3"=>"MAR","4"=>"APR","5"=>"MAY","6"=>"JUN","7"=>"JUL","8"=>"AUG","9"=>"SEP","10"=>"OCT","11"=>"NOV","12"=>"DEC");
																foreach($monthName as $key=>$vale){
																?>
																<option value="<?php echo $key; ?>"><?php echo $vale; ?></option>
																<?php
																}
																?>
															</select>
															<div style="clear:both;"></div>
															<span style="display:none;color:#e73d4a;" class="help-block-month error_class"> Please Select Month</span>
														</div>
													</div>
												</div>
											</div>
											<div class="form-body yearMonth">
												<div class="col-md-3">
													<div class="form-body">
														<div class="form-group">
															<label>Month</label>
															<select name="month" id="month" multiple="multiple">
																
																<?php $monthName=array("1"=>"JAN","2"=>"FEB","3"=>"MAR","4"=>"APR","5"=>"MAY","6"=>"JUN","7"=>"JUL","8"=>"AUG","9"=>"SEP","10"=>"OCT","11"=>"NOV","12"=>"DEC");
																foreach($monthName as $key=>$vale){
																?>
																<option value="<?php echo $key; ?>"><?php echo $vale; ?></option>
																<?php } ?>
															</select>
															<div style="clear:both;"></div>
															<span style="display:none;color:#e73d4a;" class="help-block-month error_class"> Please Select Month
															</span>
														</div>
													</div>
												</div>
											</div>	

										</div>
									</form>
									
                                    <div style="clear:both"></div>
									<div class="form-body shift">
										<div class="col-md-3">	
											<div class="form-group">
												<label>Shift</label>
												<select name="shift" id="shift" class="form-control input-lg designation_"  multiple="multiple">
													
												<?php $result=$custom_class_obj->getShiftName();
												foreach($result as $key=>$value){ ?>
												<option value="<?php echo $key; ?>">
												<?php echo $value; ?>
													
												</option>
												<?php } ?>
												</select>
											</div>
										</div>
									</div>
									<div class="form-body status">
										<div class="col-md-3">	
											<div class="form-group">
												<label>Status</label>
												<select name="status" id="status" class="form-control input-lg designation_"  multiple="multiple">
													
												<?php $result=$custom_class_obj->getStatus();
												foreach($result as $key=>$value){ ?>
												<option value="<?php echo $key; ?>">
												<?php echo $value; ?>
												</option>
												<?php } ?>
												</select>
											</div>
										</div>
									</div>	

									<div class="form-body employee">
										<div class="col-md-3">	
											<div class="form-group">
												<label>Employee</label>
												<div class="input-group">
													<select name="employee" id="employee" class="form-control select2" multiple="multiple">
														
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-body rtypeValue">
										<div class="col-md-3">	
											<div class="form-group">
												<label>Report Type</label>
												<div class="input-group">
													<select name="rtype" id="rtype" class="form-control select2" multiple="multiple">
														<option value="1">Absent</option>
														<option value="2">Present</option>
														<option value="3">Misspunch</option>
														<option value="4">OD</option>
														<option value="5">Leave</option>
													</select>
												</div>
											</div>
										</div>
									</div>	
										
                                </div>
								<div style="clear:both"></div>
								<span class="loading" style="color: red; margin-bottom: 24px; float: left; font-size: 15px;display:none;">Please Wait ......</span>
								<div style="border:none !important" class="col-md-12 portlet light portlet-fit portlet-form bordered filter">
									
								<?php include ('content/customReportFilter_content.php');?>
									
								</div>	
                                </div>
								<div class="form-actions">
                                    <div class="row">
                                        <div style="margin-left:1%;" class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn green submitCustomReport">Generate Report</button>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
						
                    </div>
                </div>
                <!-- END CONDENSED TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>

