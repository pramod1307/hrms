<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Manage Reports
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                          
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist" style="display: none;">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                              
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                               
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Step 4">
                            <span class="round-tab">
                               
                            </span>
                        </a>
                    </li>
                      <li role="presentation" class="disabled">
                        <a href="#step5" data-toggle="tab" aria-controls="step5" role="tab" title="Step 5">
                            <span class="round-tab">
                               
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step6" data-toggle="tab" aria-controls="step6" role="tab" title="Step 6">
                            <span class="round-tab">
                               
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step7" data-toggle="tab" aria-controls="step7" role="tab" title="Step 7">
                            <span class="round-tab">
                               
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                               
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="form-group">
                                <div class="col-md-12">
                                     <div class="col-md-3" style="margin-top: 25px;">
                                        Select Repot Category
                                    </div>
                                    <div class="col-md-3">
                                       
                                            <select class="form-control" onchange="report_cate('cateselect')" style="margin-top: 25px;" id="cateselect">
                                            <option value="">Select</option>
                                            <?php
                                                $sql_special="select LOV_Value,LOV_Text from LOVMast where LOV_Field='Report'";
                                                                
                                                           $result_special = query($query,$sql_special,$pa,$opt,$ms_db);
                                                           while($row_special = $fetch($result_special))
                                                           {
                                            ?>
                                                       <option  value="<?php echo $row_special['LOV_Value'] ?>">
                                                           <?php echo $row_special['LOV_Text'] ?>
                                                       </option>
                                            <?php } ?>
                                             
                                            </select>
                                       <div id="refdiv" style="display: none;"></div>
                                    </div>
                                  
                                
                                </div>
                                <div class="col-md-12" id="showdiv" style="display: none;">
                                     <div class="col-md-3" style="margin-top: 25px;">
                                        Select Sub Category
                                    </div>
                                    <div class="col-md-3">
                                       
                                            <select class="form-control" onchange="report_cate1('cateselect','subcateselect')" style="margin-top: 25px;" id="subcateselect" >
                                        
                                               
                                            </select>
                                       
                                    </div>
                                  
                                
                                </div>
                                
                            </div>
                        <ul class="list-inline" style="text-align: center;">
                              <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary next-step">Next</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                            <div style="width: 30%;float: left; margin-left: 5%">
                            <h2>Move Items From</h2>

                                <select id="sbOne" multiple="multiple" class="selectpicker" style="width: 70%;height: 250px;">
                                   
                                </select>
                             </div>
                              <div style="width: 15%;float: left;margin: 15% 0 75px 0;">
                                <input type="button" value="<<" onclick="moveAll('sbTwo','sbOne');"/>
                                <input type="button" value="<" onclick="moveSelected('sbTwo','sbOne');"/>
                                <input type="button" value=">" onclick="moveSelected('sbOne','sbTwo');"/>
                                
                                <input type="button" value=">>" onclick="moveAll('sbOne','sbTwo');"/>
                            </div>
                             <div style="width: 30%;float: left; margin-left: 5%">
                             <h2>Selected Items</h2>
                                <select id="sbTwo" name="sbTwo" multiple="multiple" class="selectpicker" style="width: 70%;height: 250px;" >
                                   
                                </select>
                                <div style="display: none">
                                 <select id="sb2" name="sb2" multiple="multiple" class="selectpicker" style="width: 70%;height: 250px;" >
                                   
                                </select>
                                </div>
                                <input type="hidden" name="" id="selectedfielddiv">
                                <input type="hidden" name="" id="selectedfielddiv1">
                             </div>
                              <div style="width: 15%;float: left;margin: 15% 0 75px 0;">
                              <button type="button" class="btn btn-default btn-sm" onclick="moveUp('sbTwo')">
                                  <span class="glyphicon glyphicon-chevron-up"></span> Up
                                </button>
                                <button type="button" class="btn btn-default btn-sm"  onclick="moveDown('sbTwo')">
                                  <span class="glyphicon glyphicon-chevron-down"></span> Down
                                </button>
                                 <div style="margin-top: 2%">
                                <label>Column Name</label>
                                <input type="text" class="" id="selectedbox" style="margin-top: 5%;width: 100%;">
                                </div>
                                 <div style="margin-top: 2%;" id="4rd">
                                 <div id="titleerror" style="color: red"></div>
                                 </div>
                                 <div style="margin-top: 2%;" id="4rd">
                                 <input type="button" id="" onclick="changeTitle();" value="Apply">
                                 </div>
                                <div id="error_code" style="color: red"></div>
   
                            </div>
                              </br>
                            
                        <ul class="list-inline" style="text-align: center;">
                             <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary prev-step">Previous</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary next-step" onclick="moveAll1('sbTwo','sbthree');">Next</button></li>
                            <!--<li style="margin-top: 50px;"><button type="button" class="btn btn-primary">Save</button></li>-->
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                     <div class="page-title" style="padding: 1px 0 0 0;margin: 0px 0px 5px 0px;">
                    <h3>Report Format</h3>
                    </div>
                    <p>Specify a title for the report and add formatting information to the report.</p>
                        <div style="width: 25%;float: left; margin-left: 5%">
                            <h2>Available Fields</h2>

                            <select id="sbthree" size="12" class="selectpicker" style="width:70%;height: 250px;"onchange="removeselected('sbthree');">
                               
                            </select>
                             <select id="hiddenlist" size="12" class="selectpicker" style="width: 70%;height: 250px;display: none;">
                               
                            </select>
                        </div>
                        <div style="width: 15%;float: left;margin: 15% 0 75px 0;">
                            <input type="button" value="<" onclick="moveSelected('sbfour','sbthree');"/>
                            <input type="button" value=">" onclick="moveSelected('sbthree','sbfour');"/>
                        </div>
                        <div style="width: 25%;float: left; margin-left: 5%">
                            <h2>Group By</h2>
                            <select id="sbfour" size="12" class="selectpicker" style="width: 70%;height: 250px;" ></select>
                            <!-- <input type="hidden" name="" id="selectedfieldgroupdiv">
                            <input type="hidden" name="" id="selectedfieldgroupdiv1">-->
                            <input type="hidden" name="" id="selectedgroupbyfields">
                            <input type="hidden" name="" id="countselectedgroupbyfields">
                        </div>
                           <div style="width: 15%;float: left;margin: 15% 0 75px 0;">
                              <button type="button" class="btn btn-default btn-sm" onclick="moveUp('sbfour')">
                                  <span class="glyphicon glyphicon-chevron-up"></span> Up
                                </button>
                                <button type="button" class="btn btn-default btn-sm"  onclick="moveDown('sbfour')">
                                  <span class="glyphicon glyphicon-chevron-down"></span> Down
                                </button>
   
                            </div>
                              
                            <div style="float: left;width: 100%;"">
                          <ul class="list-inline" style="text-align: center;">
                              <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary prev-step">Previous</button></li>
                          
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary btn-info-full next-step" onclick="moveAllgroup('sbTwo','sbnine','sbfour');">Next</button></li>
                             <!--<li style="margin-top: 50px;"><button type="button" class="btn btn-primary">Save</button></li>-->
                        </ul>
                        </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step4">
                     <div class="page-title" style="padding: 1px 0 0 0;margin: 0px 0px 5px 0px;">
                    <h3>Sorting</h3>
                    </div>
                    <p>Specify a title for the report and add formatting information to the report.</p>
                       <div style="width: 25%;float: left; margin-left: 5%">
                            <h2>Available Fields</h2>

                                <select id="sbnine" size="12" class="selectpicker" style="width: 70%;height: 250px;"onchange="removeselected('sbthree');">
                                   
                                </select>
                                
                             </div>
                              <div style="width: 15%;float: left;margin: 15% 0 75px 0;">
                                  
                                 <input type="button" value="<" onclick="moveSelected('sbten','sbnine');"/>
                                <input type="button" value=">" onclick="moveSelected('sbnine','sbten');"/>
                             
                            </div>
                             <div style="width: 25%;float: left; margin-left: 5%">
                             <h2>Sorted By</h2>
                                <select id="sbten" size="12" class="selectpicker" style="width: 70%;height: 250px;" >
                                   
                                </select>
                                 <input type="hidden" name="" id="selectedorderbyfield">

                                </div>
                            <div style="width: 20%;float:left;margin: 5% 0 75px 0;">
                            
                                <div style="margin-top: 2%">
                               <label>Sort Order *</label>
                               <div id="orderbymsg" style="color: red;text-align: center;"></div>
                                <select id="sortorder" name="sortorder" style="width: 150px">
                                        <option value="">Select</option>
                                        <option value="Asc">Asc</option>
                                        <option value="Desc">Desc</option>
                                </select>
                                </div>
                                  <div style="margin-top: 2%;" id="4rd">
                                 <input type="button" id="" onclick="orderbyfield();" value="Apply">
                                 </div>
                               
                            </div>
                            <div style="float: left;width: 100%;"">
                          <ul class="list-inline" style="text-align: center;">
                              <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary prev-step">Previous</button></li>
                          
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary btn-info-full next-step"onclick="moveAll2('sbTwo','sbfive','sbfour');">Next</button></li>
                             <!--<li style="margin-top: 50px;"><button type="button" class="btn btn-primary">Save</button></li>-->
                        </ul>
                        </div>
                    </div>


                    <div class="tab-pane" role="tabpanel" id="step5">
                     <div class="page-title" style="padding: 1px 0 0 0;margin: 0px 0px 5px 0px;">
                    <h3>Report Format</h3>
                    </div>
                    <p>Specify a title for the report and add formatting information to the report.</p>
                    <div id="summerr" style="color:red"></div>
                       <div style="width: 25%;float: left; margin-left: 5%">
                            <h2>Available Fields</h2>

                                <select id="sbfive" size="12" class="selectpicker" style="width: 70%;height: 250px;">
                                   
                                </select>
                             </div>
                              <div style="width: 15%;float: left;margin: 15% 0 75px 0;">
                                  
                                 <input type="button" value="<" onclick="moveSelected('sbsix','sbfive');"/>
                                <input type="button" value=">" onclick="moveSelected_check('sbfive','sbsix');"/>
                             
                            </div>
                              <div style="width: 25%;float: left; margin-left: 5%">
                             <h2>Summarized Fields</h2>
                             
                                <select id="sbsix" size="12" class="selectpicker" style="width: 70%;height: 250px;" >
                                   
                                </select>
                                   <input type="hidden" name="" id="selectedsumfield">
                             </div>
                            <div style="width: 20%;float:left;margin: 5% 0 75px 0;">
                            
                                <div style="margin-top: 2%">
                              <label>Filter Category *</label></br>
                              <div id="erro_sum" style="color: red"></div>
                                <select id="summaried_by" name="summaried" style="width: 150px" onchange="showhidedivforsummariedcoulmn();">
                                        
                                       
                                </select>
                                </div>
                                <div style="margin-top: 2%;display: none;" id="columnnamediv">
                                <label>Column Name</label>
                                <input type="text" class="" id="coulumnname" style="margin-top: 5%;width: 150px;">
                                </div>
                                  <div style="margin-top: 2%;" id="4rd">
                                 <input type="button" id="" onclick="summaried_byfun();" value="Apply">
                                 </div>
                               
                            </div>
                            <div style="float: left;width: 100%;"">
                         <ul class="list-inline" style="text-align: center;">
                              <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary prev-step">Previous</button></li>
                          
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary btn-info-full"  onclick="moveAll5('sbTwo','sbseven','sbsix');">Next</button></li>
                           
                        </ul>
                        </div>
                    </div>

                    <div class="tab-pane" role="tabpanel" id="step6">
                     <div class="page-title" style="padding: 1px 0 0 0;margin: 0px 0px 5px 0px;">
                    <h3>Filter Fields</h3>
                    </div>
                    <p>Specify a title for the report and add formatting information to the report.</p>
                      <div style="width: 25%;float: left; margin-left: 5%">
                            <h2>Available Fields</h2>

                                <select id="sbseven" size="12" class="selectpicker" style="width: 70%;height: 250px;">
                                   
                                </select>
                             </div>
                              <div style="width: 15%;float: left;margin: 15% 0 75px 0;">
                                  
                                 <input type="button" value="<" onclick="moveSelected('sbeight','sbseven');"/>
                                <input type="button" value=">" onclick="moveSelected('sbseven','sbeight');"/>
                             
                            </div>
                             <div style="width: 25%;float: left; margin-left: 5%">
                             <h2>Fields</h2>
                             <div id="erro_sum1" style="color: red"></div>
                                <select id="sbeight" size="12" class="selectpicker" style="width: 70%;height: 250px;" >
                                   
                                </select>
                                  
                             </div>

                             <div style="width: 20%;float:left;margin: 5% 0 75px 0;">
                            <div style="color: red"  id="error_msg"></div>
                                <div style="margin-top: 2%">
                               <label>Filter Category *</label>
                                <select id="filter_by" name="filter_by" onchange="filtercate();" >
                                      <option value="">Select</option>
                                      <option value="1">Is equal to</option>
                                      <option value="2">Is not equal to</option>
                                      <option value="3">Is greater than</option>
                                      <option value="4">Is greater than or equal to</option>
                                      <option value="5">Is less than</option>
                                      <option value="6">Is less than or equal to</option>
                                      <option value="7">Begins with</option>
                                      <option value="8">Doesn't begin with</option>
                                      <option value="9">Ends width</option>
                                      <option value="10">Doesn't end with</option>
                                      <option value="11">Contains</option>
                                      <option value="12">Does Not Contain</option>
                                      <option value="13">Is between</option>
                                      <option value="14">Is not between</option>
                                </select>
                                </div>
                                 <div style="margin-top: 2%;display: none;" id="1st" >
                                 <select name="commanfilter1" id="commanfilter1" width="100%" class="form-control">
                               
                                 </select>
                                 </div>
                                  <div style="margin-top: 2%;display: none;" id="2nd" >
                                 <select name="commanfilter2" id="commanfilter2" width="100%" class="form-control">
                               
                                 </select>
                                 </div>
                                  <div style="margin-top: 2%;display: none;" id="3rd" >
                                 <select name="commanfilter3" id="commanfilter3" width="100%" class="form-control" multiple="multiple">
                               
                                 </select>
                                 </div>
                                 <div style="margin-top: 2%;display: none;" id="4th">
                                    <input type="text" name="customtext" class="form-control" id="customtext" >
                                 </div>
                                 <input type="hidden" name="" id="filtercondition" style="width: 500px;">
                                <!--<div style="margin-top: 2%;display: none;" id="2nd">
                                 <input type="text" id="datepicker1">
                                 </div>
                                  <div style="margin-top: 2%;display: none;" id="3rd">
                                 <input type="text" id="datepicker2">
                                 </div>-->
 				<div style="margin-top: 2%;" id="4rd">
                                 <input type="button" id="" onclick="text1stbox();" value="Apply">
                                 </div>

                           
                            </div>
                            <div style="float: left;width: 100%;"">
                        <ul class="list-inline" style="text-align: center;">
                              <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary prev-step">Previous</button></li>
                          
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary btn-info-full next-step"  onclick="filteredstep();">Next</button></li>
                            <!-- <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" >Save</button></li>-->
                        </ul>
                        </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step7">
                    <div class="page-title" style="padding: 1px 0 0 0;margin: 0px 0px 5px 0px;">
                    <h3>Report Format</h3>
                    </div>
                    <p>Specify a title for the report and add formatting information to the report.</p>
                      <div class="form-group">

                      
                                <div class="col-md-12">
                                <div id="createerr" style="color: red"></div>
                                     <div class="col-md-3" style="margin-top: 15px;">
                                        Title
                                    </div>
                                    <div class="col-md-3" style="margin-top: 15px;">
                                       
                                          <input type="text" name="title" id="title" class="form-control" style="width: 450px;">
                                    </div>

                                  
                                
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="col-md-3" style="margin-top:15px;">Description
                                    </div>
                                    <div class="col-md-3" style="margin-top: 15px;">
                                        <input type="text" name="" id="Description" class="form-control" style="width: 450px;">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3" style="margin-top:15px;">Select View Options
                                    </div>
                                    <div class="col-md-9" style="margin-top: 15px; display:inline-flex;">
                                        <input type="checkbox" name="view_emp"  id="view_emp" class="form-control"> For Employee
                                        <input type="checkbox" name="view_mngr" id="view_mngr" class="form-control"> For Manager
                                    </div>
                                </div>

                                 
                            </div>
                              
                            
                               <ul class="list-inline" style="text-align: center;">
                              <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                            <li style="margin-top: 50px;"><button type="button" class="btn btn-primary prev-step">Previous</button></li>
                          
                             <li style="margin-top: 50px;"><button type="button" class="btn btn-primary btn-info-full next-step" onclick="finalstepsave();">Save</button></li>
                        </ul>
                    </div>
                    
                    <div class="tab-pane" role="tabpanel" id="complete">
                    <div class="page-title" style="padding: 1px 0 0 0;margin: 0px 0px 5px 0px;">
                    
                    </div>
                    
                      <div class="form-group">

                      
                                <div class="col-md-12">
                                    <div id="mssgdiv"></div>
                                
                                </div>
                               
                             
                            </div>
                              
                            
                               <ul class="list-inline" style="text-align: center;">
                              <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="reloadphppage();">Cancel</button></li>
                           
                             <li style="margin-top: 50px;"><button type="button" class="btn btn-primary" onclick="getselectedExcel('title');">Download In Excel</button></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div id="textare" style="display: none;"><textarea rows="5" cols="100" id="txtarea"></textarea></div>
                    <input type="hidden" id="selectedfields" value="">
                    <input type="hidden" id="wherecondition" value="">
                    <input type="hidden" id="groupbycondition" value="">
                    <input type="hidden" id="orderbycondition" value="">
                    <input type="hidden" id="filterfields" value="">
                </div>
            </form>
        </div>
  
 </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>