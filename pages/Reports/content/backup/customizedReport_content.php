<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Customized Reports
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                      <form  enctype="multipart/form-data" id="form" name="reportform" class="form-horizontal form-row-seperated">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label>
                                                From Date              
                                            </label>
                                             <input name="startDate" id="startDate" class="date-picker form-control" />
                                            
                                       
                                    </div>
                                     <div class="col-md-3" id="fromDate_div">
                                            <label>
                                                Business Unit            
                                            </label>
                                             <select name="divi_select" id="divi_select" width="100%" class="form-control" data-placeholder="Select...">
                                             <option value="">Choose Options</option>
                                              <?php 
                                              $sql="select Buss_Code,BussName,BUSSID from BussMast";
                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                            While($row=$fetch($res))
                                            {
                                            ?>
                                                <option value="<?php echo $row['BUSSID'];?>"><?php echo $row['BussName'];?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                    </div>

                                     <div class="col-md-3" id="fromDate_div">
                                            <label>
                                                Grade Name            
                                            </label>
                                             <select name="grd_select" id="grd_select" width="100%" class="form-control" data-placeholder="Select...">
                                             <option value="">Choose Options</option>
                                              <?php 
                                              $sql=" Select GRD_CODE,GRD_NAME,GRDID from GrdMast";
                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                            While($row=$fetch($res))
                                            {
                                            ?>
                                                <option value="<?php echo $row['GRDID'];?>"><?php echo $row['GRD_NAME'];?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                    </div>
                                    
                                   

                                </div>
                                 <div class="col-md-12" id="fromDate_div" style="margin-top: 22px;">
                                            
                                            <input type="button" name="" value="Generate Report" style="float: right;" onclick="getcustomizedReport1();">
                                    </div>
                            </div>
                            <div id="reportdiv">
                            <div style="float: right;padding: 2px;">
                            <button id="btnExport" onclick="fnExcelReport('sample_2');"> EXPORT </button>
                            </div>
                            <iframe id="txtArea1" style="display:none"></iframe>
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead >
                            <tr bgcolor="#3390FF">
                                <th>Date</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th>29</th><th>30</th><th>31</th><th>TOTAL DIRECT HOURS</th>
                            </tr>
                            </thead>
                            <tbody id="getdata">
                            
                            </tbody>
                            </table>
                        </div>
                        </form>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>