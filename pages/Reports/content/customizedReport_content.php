<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Customized Reports
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                      <form  enctype="multipart/form-data" id="form" name="reportform" class="form-horizontal form-row-seperated">  <input type="hidden" name="" value="<?php echo $code;?>" id="emp_id">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                 <div class="col-md-3">
                                        IBW Report Category
                                              <ul class="ulclass">
                                                   <li style="margin-left: -40px;"><input type="checkbox" name="chk1" id="chk1" value="1">Head Count</li>
                                                   <li style="margin-left: -40px;"><input type="checkbox" name="chk1" id="chk1" value="2">Attendance Hours</li>
                                                   <li style="margin-left: -40px;"><input type="checkbox" name="chk1" id="chk1" value="3">Summary Attendance Hours</li>
                                                   <li style="margin-left: -40px;"><input type="checkbox" name="chk1" id="chk4" value="4">Summary</li>
                                            </ul>

                                       
                                    </div>
                                    <div class="col-md-3">
                                        <label>
                                                From Date              
                                            </label>
                                             <input name="startDate" id="startDate" class="date-picker form-control" style="width: 84%;" />
                                            
                                       
                                    </div>
                                     <div class="col-md-3" id="fromDate_div">
                                            <label>
                                                Business Name            
                                            </label>
                                                <select name="business_select" id="business_select" width="100%" class="form-control" data-placeholder="Select..." multiple="multiple">
                                             <?php 
                                              $sql=" Select BUSSID,BussName from BussMast where status='1'";
                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                            While($row=$fetch($res))
                                            {
                                            ?>
                                                <option value="<?php echo $row['BUSSID'];?>"><?php echo $row['BussName'];?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                    </div>
                                     <div class="col-md-3" id="fromDate_div">
                                            <label>
                                                Sub Business Unit            
                                             <select name="sbusiness_select" id="sbusiness_select" width="100%" class="form-control" data-placeholder="Select..." multiple="multiple">
                                             
                                              <?php 
                                              $sql="select subBussName,subBussid from subBussMast where status='1'";
                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                            While($row=$fetch($res))
                                            {
                                            ?>
                                                <option value="<?php echo $row['subBussid'];?>"><?php echo $row['subBussName'];?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                    </div>
                                     <div class="col-md-3" id="fromDate_div" style="margin-top: 20px;">
                                            <label>
                                                Process Unit            
                                             <select name="process_select" id="process_select" width="100%" class="form-control" data-placeholder="Select..." multiple="multiple">
                                             
                                              <?php 
                                              $sql="select PROCID,PROC_NAME from PROCMAST where proc_status='1'";
                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                            While($row=$fetch($res))
                                            {
                                            ?>
                                                <option value="<?php echo $row['PROCID'];?>"><?php echo $row['PROC_NAME'];?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                    </div>
                                     <div class="col-md-3" id="fromDate_div" style="margin-top: 20px;">
                                            <label>
                                                Division Unit            
                                             <select name="divi_select" id="divi_select" width="100%" class="form-control" data-placeholder="Select..." multiple="multiple">
                                             
                                              <?php 
                                              $sql="select DIVIID,DIVI_NAME from DiviMast where Divi_status='1'";
                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                            While($row=$fetch($res))
                                            {
                                            ?>
                                                <option value="<?php echo $row['DIVIID'];?>"><?php echo $row['DIVI_NAME'];?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                    </div>
                                     <div class="col-md-3" id="fromDate_div"  style="margin-top: 20px;">
                                            <label>
                                                Grade Unit            
                                             <select name="grd_select" id="grd_select" width="100%" class="form-control" data-placeholder="Select..." multiple="multiple">
                                             
                                              <?php 
                                              $sql="select GRD_NAME,GRDID from GrdMast";
                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                            While($row=$fetch($res))
                                            {
                                            ?>
                                                <option value="<?php echo $row['GRDID'];?>"><?php echo $row['GRD_NAME'];?></option>
                                            <?php
                                            }
                                            ?>
                                            </select>
                                    </div>
                                    </div>

                                 <div class="col-md-12" id="fromDate_div" style="margin-top: 22px;">
                                            
                                            <input type="button" name="" value="Generate Report" style="float: right;" onclick="getcustomizedReport1();">
                                    </div>
                            </div>
                            <div id="reportdiv">
                           
                        </div>
                        </form>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>