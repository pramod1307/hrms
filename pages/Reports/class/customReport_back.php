<?php

class customReport_class
{
    
    var $designation = array();
    var $functionName = array();
    var $getSubFunctionName = array();
    var $getLocation = array();
    var $getManager = array();
    var $getBusinessUnit = array();
    var $getSubBusinessUnit = array();
    var $wlocationName = array();
    var $costMasterName = array();
    var $getDivisionName = array();
    var $getProcessName = array();
    var $getRegonCodeName = array();
    var $getEmpTypeName = array();
    var $getqualificationName = array();
    var $getStatusName = array();
    var $getLeaveType = array();
    var $yearList = array();
    var $getReportName = array();
    var $getAttendanceAllowance = array();
    var $getshiftAllowance = array();
    var $getShiftName = array();
    
    function __construct()
    {
        
    }
    
    
    function yearFunction()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        $sql = "select FY from FinYear";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->yearList[] = $rowel['FY'];
        }
        
        return $this->yearList;
        
    }
    
    function getDesignationName()
    {
        
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        $sql = "select DSG_NAME,DSGID from DsgMast where Dsg_status='1' order by DSG_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->designation[trim($rowel['DSGID'])] = trim($rowel['DSG_NAME']);
        }
        
        return $this->designation;
    }
    
    function getFunctionName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select FUNCT_NAME,FunctID from FUNCTMast where status='1' order by FUNCT_NAME";
        
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->functionName[trim($rowel['FunctID'])] = trim($rowel['FUNCT_NAME']);
        }
        
        //print_r($this->functionName);die;
        
        return $this->functionName;
    }
    
    function getSubFunctionName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select SubFunct_NAME,SubFunctID from SubFunctMast where status='1' order by SubFunct_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getSubFunctionName[trim($rowel['SubFunctID'])] = trim($rowel['SubFunct_NAME']);
        }
        
        return $this->getSubFunctionName;
    }
    
    function getGradeName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select GRDID,GRD_NAME from GrdMast where Grd_status='1' order by GRD_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getGradeName[trim($rowel['GRDID'])] = trim($rowel['GRD_NAME']);
        }
        
        return $this->getGradeName;
    }
    
    function getLocationName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select LOC_NAME,LOCID from locmast where LOC_STATUS='1' order by LOC_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getLocation[trim($rowel['LOCID'])] = trim($rowel['LOC_NAME']);
        }
        
        return $this->getLocation;
    }
    
    function getManagerName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select MNGR_CODE2,MNGR_NAME from HrdMastQry where MNGR_NAME != '' group by MNGR_NAME,MNGR_CODE2 order by MNGR_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getManager[trim($rowel['MNGR_CODE2'])] = trim($rowel['MNGR_NAME']);
        }
        
        return $this->getManager;
    }
    
    function getDepartmentName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select MNGR_CODE2,MNGR_NAME from HrdMastQry where MNGR_NAME != '' group by MNGR_NAME,MNGR_CODE2 order by MNGR_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getManager[trim($rowel['MNGR_CODE2'])] = trim($rowel['MNGR_NAME']);
        }
        
        return $this->getManager;
    }
    
    function getShiftName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select ShiftMastId,Shift_Name from ShiftMast order by Shift_Name";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getShiftName[trim($rowel['ShiftMastId'])] = trim($rowel['Shift_Name']);
        }
        
        return $this->getShiftName;
    }
    function businessUnit()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select BUSSID,BussName from bussmast where status='1' order by BussName";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getBusinessUnit[trim($rowel['BUSSID'])] = trim($rowel['BussName']);
        }
        
        return $this->getBusinessUnit;
        
    }
    function subBusinessUnit()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select subBussid,subBussName from subBussMast where status='1'  order by subBussName";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getSubBusinessUnit[trim($rowel['subBussid'])] = trim($rowel['subBussName']);
        }
        
        return $this->getSubBusinessUnit;
        
    }
    function wlocation()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select WLOC_CODE,WLOC_NAME from WorkLocMast where Status='1' order by WLOC_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->wlocationName[trim($rowel['WLOC_CODE'])] = trim($rowel['WLOC_NAME']);
        }
        
        return $this->wlocationName;
        
    }
    function costMaster()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select COST_CODE,COST_NAME,CostID from CostMast where Cost_status='1' order by COST_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->costMasterName[trim($rowel['CostID'])] = trim($rowel['COST_NAME']);
        }
        
        return $this->costMasterName;
        
    }
    function division()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select DIVI_NAME,DIVIID from DiviMast order by DIVI_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getDivisionName[trim($rowel['DIVIID'])] = trim($rowel['DIVI_NAME']);
        }
        
        return $this->getDivisionName;
        
    }
    function getProcess()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select PROCID,PROC_NAME from PROCMAST where proc_status='1' order by PROC_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getProcessName[trim($rowel['PROCID'])] = trim($rowel['PROC_NAME']);
        }
        
        return $this->getProcessName;
        
    }
    function getRegonCode()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select REGN_CODE,REGN_NAME from RegnMast order by REGN_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getRegonCodeName[trim($rowel['REGN_CODE'])] = trim($rowel['REGN_NAME']);
        }
        
        return $this->getRegonCodeName;
        
    }
    
    function getEmpType()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select TYPEID,TYPE_NAME from TypeMast where status='1' order by TYPE_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getEmpTypeName[trim($rowel['TYPEID'])] = trim($rowel['TYPE_NAME']);
        }
        
        return $this->getEmpTypeName;
        
    }
    
    function getQualification()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select QualID,Qual_Name from QualMast where Qual_status='1' order by Qual_Name";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getqualificationName[trim($rowel['QualID'])] = trim($rowel['Qual_Name']);
        }
        
        return $this->getqualificationName;
        
    }
    
    function getStatus()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select Status_Code,Status_Name from StatusMast order by Status_Name";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getStatusName[trim($rowel['Status_Code'])] = trim($rowel['Status_Name']);
        }
        
        return $this->getStatusName;
        
    }
    
    function getReportsName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select RepCode,RepName from reports order by RepCode";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getReportName[trim($rowel['RepCode'])] = trim($rowel['RepName']);
        }
        
        return $this->getReportName;
        
    }
    
    function getCompany()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select COMPID,COMP_NAME from CompMast";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getCompanyName[trim($rowel['COMPID'])] = trim($rowel['COMP_NAME']);
        }
        
        return $this->getCompanyName;
        
    }
    function LeaveListType()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $queryForGetTotalLeave = "select LOV_Value,LOV_Text from lovmast where LOV_Field='leave' and LOV_Active='A' order by cast(LOV_Value as Numeric(10,0)) asc";
        
        $result = query($query, $queryForGetTotalLeave, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getLeaveType[$rowel['LOV_Value']] = $rowel['LOV_Text'];
        }
        
        return $this->getLeaveType;
        
    }
    function shiftAllowance()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $queryForGetTotalLeave = "SELECT Shifts,Rates FROM ShiftAllowance WHERE RuleStatus='1'";
        $result                = query($query, $queryForGetTotalLeave, $pa, $opt, $ms_db);
        while ($rowel = $fetch($result)) {
            $this->getshiftAllowance[$rowel['Shifts']] = $rowel['Rates'];
        }
        return $this->getshiftAllowance;
        
    }
    function AttendanceAllowance()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $queryForGetTotalLeave = "SELECT NoOfDays,Rates,MinNoOfDays FROM AttendanceAllowance WHERE RuleStatus='1'";
        $result                = query($query, $queryForGetTotalLeave, $pa, $opt, $ms_db);
        while ($rowel = $fetch($result)) {
            $this->getAttendanceAllowance[] = $rowel['NoOfDays'] . '---' . $rowel['Rates'] . '---' . $rowel['MinNoOfDays'];
        }
        return $this->getAttendanceAllowance;
        
    }
    function generateCustomReport($objPHPExcel)
    {
        //var_dump($_POST['emp_code_list']);
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $colorCode             = '#ABEBC6';
        $colorCodeForHeader    = '#b8d8f5';
        $headingSetIndex       = 'E1';
        $resultStatus          = array();
        $columnName            = $this->columnOfExcel("3");
        $resultStatus['error'] = '';
        
        if ($_POST['type'] == '2') {
            
            $monthName            = array(
                "1" => "JAN",
                "2" => "FEB",
                "3" => "MAR",
                "4" => "APR",
                "5" => "MAY",
                "6" => "JUN",
                "7" => "JUL",
                "8" => "AUG",
                "9" => "SEP",
                "10" => "OCT",
                "11" => "NOV",
                "12" => "DEC"
            );
            $getPreviousIdOfLeave = '';
            $where                = '';
            $HorizontalColumnName = array(); // This is use for print Horijontal Column
            $HorizontalMonthName  = array(); // User selected Month  store in this 
            
            if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] == 'HR') {
                $where .= " lovmast.LOV_Field='leave' and leave.status in ('2') and";
            } else {
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                
                $where .= " lovmast.LOV_Field='leave' and leave.status in ('2') and ";
                $where .= (!empty($_POST['emp_code_list'])) ? " hrdmastqry.Emp_Code in (" . $empArr . ") AND" : "";
            }
            
            // leave.status = 2 (Approved)
            
            if (isset($_POST['month']) && isset($_POST['year'])) {
                
                if (!empty($_POST['month'])) {
                    
                    $HorizontalColumnName[0] = 'Leave Type';
                    $HorizontalMonthName     = $_POST['month'];
                    
                    foreach ($_POST['month'] as $key => $value) {
                        
                        $HorizontalColumnName[] = $monthName[$value];
                        
                    }
                    
                    
                } else {
                    
                    $HorizontalColumnName = array(
                        "0" => "Leave Type",
                        "1" => "JAN",
                        "2" => "FEB",
                        "3" => "MAR",
                        "4" => "APR",
                        "5" => "MAY",
                        "6" => "JUN",
                        "7" => "JUL",
                        "8" => "AUG",
                        "9" => "SEP",
                        "10" => "OCT",
                        "11" => "NOV",
                        "12" => "DEC"
                    );
                    $HorizontalMonthName  = array(
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8",
                        "9",
                        "10",
                        "11",
                        "12"
                    );
                    
                }
                
                if (isset($_POST['year']) && $_POST['year'] != '') {
                    $yearForSelection = $_POST['year'];
                    $where .= " YEAR(leave.LvFrom)=" . $_POST['year'] . " and";
                } else {
                    
                    $yearForSelection = date("Y");
                    $where .= " YEAR(leave.LvFrom)=" . $yearForSelection . " and";
                }
                
            } else {
                $error = "Somthing Missing !";
                exit;
            }
            
            $where .= (!empty($_POST['statusName'])) ? " hrdmastqry.Status_Code in (" . implode(",", $_POST['statusName']) . ") and" : ""; // Check User Status Active or not
            
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Leave Transaction Report');
            $objPHPExcel->getActiveSheet()->SetCellValue('F1', $yearForSelection);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            
            $advanceFilterConditions = $this->advanceSearchOption();
            
            $counterForExcel          = 2;
            $countForHorizonatalExcel = 0;
            
            /*												*****************  Print Horizontal Column  (Start Code) **************************** 					*/
            
            foreach ($HorizontalColumnName as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '', $value);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            
            /*												*****************  Print Vertical Column  (Start Code) **************************** 					*/
            
            $counterForExcel = 3;
            
            foreach ($this->LeaveListType() as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $counterForExcel, $value);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $counterForExcel)->getFont()->setBold(true);
                $counterForExcel++;
            }
            
            $counterForExcel = 3;
            
            $where .= $advanceFilterConditions;
            if ($advanceFilterConditions == '')
                $where = rtrim($where, "and");
            
            
            /*												*****************  First Loop fetch record for particular Leave  (Start Code) **************************** 					*/
            
            foreach ($this->getLeaveType as $key1 => $value1) {
                
                $countForHorizonatalExcel = 1;
                
                /*												*****************  Second Loop fetch record for particular Month Like Casual leave for January Month  (Start Code) **************************** 					*/
                
                foreach ($HorizontalMonthName as $key => $value) {
                    
                    $totalNoOFLeave   = 0;
                    $daysinMonth      = date("t", strtotime($yearForSelection . '-' . $value . '-01'));
                    $endDateCondition = $yearForSelection . '-' . $value . '-' . $daysinMonth;
                    
                    
                    //	$sql1 =	"select  distinct(leave.leaveID),lovmast.LOV_Text,leave.FromHalf,leave.LvFrom,leave.LvTo,leave.LvDays,leave.Levkey from leave left join lovmast on leave.LvType=lovmast.LOV_Value left join hrdmastqry on leave.CreatedBy=hrdmastqry.Emp_Code  left join Roster_schema on Roster_schema.Emp_Code=leave.CreatedBy left join att_roster on att_roster.roster=Roster_schema.RosterName where  $where and MONTH(leave.LvFrom)='".$value."' and leave.LvType='".$key1."' order by leave.leaveID asc";
                    $sql1 = "select  distinct(leave.leaveID),lovmast.LOV_Text,hrdmastqry.Emp_Code,leave.CreatedBy,leave.FromHalf,leave.LvFrom,leave.LvTo,leave.LvDays,leave.Levkey from leave left join lovmast on leave.LvType=lovmast.LOV_Value left join hrdmastqry on leave.CreatedBy=hrdmastqry.Emp_Code  where  $where and MONTH(leave.LvFrom)='" . $value . "' and leave.LvType='" . $key1 . "' order by leave.leaveID asc";
                    
                    $result12 = query($query, $sql1, $pa, $opt, $ms_db);
                    
                    while ($rowel12 = $fetch($result12)) {
                        
                        /*  This #Condition will work when user select any shift for searching  */
                        
                        if (!empty($_POST['shift'])) {
                            
                            $sqlone = "select top 1 att_roster.shiftMaster from Roster_schema left join att_roster on Roster_schema.RosterName=att_roster.roster where Roster_schema.start_rost='" . $rowel12['LvFrom'] . "' and Roster_schema.Emp_Code='" . $rowel12['CreatedBy'] . "'   order by Roster_schema.RostID desc";
                            
                            $result123 = query($query, $sqlone, $pa, $opt, $ms_db);
                            
                            while ($rowel123 = $fetch($result123)) {
                                
                                if (in_array($rowel123['shiftMaster'], $_POST['shift'])) {
                                    
                                    if ($rowel12['Levkey'] != $getPreviousIdOfLeave) {
                                        
                                        if ($rowel12['LvTo'] != '' && $rowel12['LvFrom'] != '') {
                                            
                                            
                                            
                                            if (strtotime($rowel12['LvTo']) > strtotime($endDateCondition)) {
                                                
                                                
                                                $toDate         = strtotime($endDateCondition); // or your date as well
                                                $fromDate       = strtotime($rowel12['LvFrom']);
                                                $datediff       = $toDate - $fromDate;
                                                $datediff       = floor($datediff / (60 * 60 * 24));
                                                $totalNoOFLeave = $totalNoOFLeave + $datediff + 1;
                                                if ($rowel12['FromHalf'] != '') {
                                                    if (trim($rowel12['FromHalf']) == '2FH' || trim($rowel12['FromHalf']) == '1FH') {
                                                        $totalNoOFLeave = $totalNoOFLeave - 0.50;
                                                    }
                                                }
                                                
                                            } else {
                                                
                                                $totalNoOFLeave = $totalNoOFLeave + $rowel12['LvDays'];
                                                
                                            }
                                            
                                        }
                                        $getPreviousIdOfLeave = $rowel12['Levkey'];
                                    }
                                }
                            }
                            
                        } else {
                            
                            if ($rowel12['Levkey'] != $getPreviousIdOfLeave) {
                                
                                if ($rowel12['LvTo'] != '' && $rowel12['LvFrom'] != '') {
                                    
                                    
                                    
                                    if (strtotime($rowel12['LvTo']) > strtotime($endDateCondition)) {
                                        
                                        //echo $endDateCondition.' end ## ';
                                        $toDate         = strtotime($endDateCondition); // or your date as well
                                        $fromDate       = strtotime($rowel12['LvFrom']);
                                        $datediff       = $toDate - $fromDate;
                                        $datediff       = floor($datediff / (60 * 60 * 24));
                                        $totalNoOFLeave = $totalNoOFLeave + $datediff + 1;
                                        if ($rowel12['FromHalf'] != '') {
                                            if (trim($rowel12['FromHalf']) == '2FH' || trim($rowel12['FromHalf']) == '1FH') {
                                                $totalNoOFLeave = $totalNoOFLeave - 0.50;
                                            }
                                        }
                                        
                                    } else {
                                        
                                        $totalNoOFLeave = $totalNoOFLeave + $rowel12['LvDays'];
                                        
                                    }
                                    
                                }
                                $getPreviousIdOfLeave = $rowel12['Levkey'];
                            }
                            
                            
                        }
                        
                        /*  This #Condition End 		*/
                        
                        
                    }
                    
                    
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalNoOFLeave);
                    
                    $countForHorizonatalExcel++;
                    
                }
                
                /*												*****************  Second Loop (End Code) **************************** 					*/
                
                $counterForExcel++;
            }
            
            /*												*****************  First Loop (End Code) **************************** 					*/
            
            $my_excel_filename = "upload/LeaveTransaction.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/LeaveTransaction.xls');
            
            
        } else if ($_POST['type'] == '3') {
            
            $employeeLeave            = "";
            $sumOfLeave               = 0;
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $where                    = '';
            $valueForFillExcelData    = array();
            $HorizontalColumnName     = array(
                "S.No",
                "Employee Code",
                "Employee Name",
                "Function",
                "SUB Function",
                "Department",
                "Designation",
                "Location",
                "Manager Name",
                "Month Days",
                "Present Day",
                "Weekly off",
                "Holidays",
                "Days Worked"
            );
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            
            $this->getShiftName();
            $this->AttendanceAllowance();
            $this->shiftAllowance();
            
            foreach ($this->LeaveListType() as $key => $value) {
                $HorizontalColumnName[] = $value;
            }
            
            $HorizontalColumnName[] = 'Total Leave';
            $HorizontalColumnName[] = 'Day Absent';
            $HorizontalColumnName[] = 'Payroll Days';
            $HorizontalColumnName[] = 'No. of Late/Early';
            $HorizontalColumnName[] = 'Un authorized Shift Change ';
            $HorizontalColumnName[] = 'Attendance Allowance';
            $HorizontalColumnName[] = 'Shift Allowance';
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            
            
            $fromDatearr         = explode("/", $_POST['fromDate']);
            $dateforDailyRecord  = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $toDatearr           = explode("/", $_POST['toDate']);
            $toDate              = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $dateforDailyRecord1 = date('d-M-Y', strtotime($dateforDailyRecord));
            $toDate1             = date('d-M-Y', strtotime($toDate));
            
            /*												*****************  Print Horizontal Column  (Start Code) **************************** 					*/
            
            
            $headingInfo = 'Payroll Report for ' . $dateforDailyRecord1 . ' To ' . $toDate1 . '';
            $objPHPExcel = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $counterForExcel++;
            $countForHorizonatalExcel = 0;
            
            
            $i                         = 1;
            $dayDifferencesBetweenDate = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dayDifferencesBetweenDate = $dayDifferencesBetweenDate + 1;
            $advanceFilterConditions   = $this->advanceSearchOptionForJson($getFields, '2');
            
            foreach ($advanceFilterConditions as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dayDifferencesBetweenDate);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "SELECT SUM(CASE WHEN status ='P' THEN 1 ELSE 0 END) AS present
															,SUM(CASE WHEN status ='A' THEN 1 ELSE 0 END) AS Absent
															,SUM(CASE WHEN status ='L' THEN 1 ELSE 0 END) AS Leave
															,SUM(CASE WHEN status ='H' THEN 1 ELSE 0 END) AS halfDay
															,SUM(CASE WHEN status ='W' THEN 1 ELSE 0 END) AS woff
															,SUM(CASE WHEN status ='F' THEN 1 ELSE 0 END) AS holiday
															,SUM(CASE WHEN Lateflag != '' OR Earlyflag != '' THEN 1 ELSE 0 END) AS lateEarly
															FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "'";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                = $fetch($resultForGetRecord);
                $present            = (isset($row['present'])) ? $row['present'] : '0';
                $Absent             = (isset($row['Absent'])) ? $row['Absent'] : '0';
                $Leave              = (isset($row['Leave'])) ? $row['Leave'] : '0';
                $halfDay            = (isset($row['halfDay'])) ? $row['halfDay'] : '0';
                $woff               = (isset($row['woff'])) ? $row['woff'] : '0';
                $holiday            = (isset($row['holiday'])) ? $row['holiday'] : '0';
                $lateEarly          = (isset($row['lateEarly'])) ? $row['lateEarly'] : '0';
                $halfDayCalculation = $halfDay / 2;
                
                $dayDifferencesBetweenDate = $dayDifferencesBetweenDate - ($woff + $holiDay);
                $Absent                    = $Absent + $halfDayCalculation; // This is for When Somone will take halfDay then other hlafDAy part will be Present Part.
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dayDifferencesBetweenDate);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $woff);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $holiday);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dayDifferencesBetweenDate);
                $countForHorizonatalExcel++;
                $sumOfLeave      = 0;
                $employeeLeave   = $this->leaveBaseOFDate($dateforDailyRecord, $toDate, $key);
				
                $leaveWithoutPay = 0;
                foreach ($this->getLeaveType as $key1 => $value1) {
                    
                    if (isset($employeeLeave[$key1])) {
                        $sumOfLeave = $sumOfLeave + $employeeLeave[$key1];
                        if ($key1 == '4')
                            $leaveWithoutPay = $employeeLeave[$key1];
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $employeeLeave[$key1]);
                        $countForHorizonatalExcel++;
                    } else {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                        $countForHorizonatalExcel++;
                    }
                }
                $present1 = $dayDifferencesBetweenDate - ($leaveWithoutPay + $Absent);
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $sumOfLeave);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present1);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $lateEarly);
                $countForHorizonatalExcel++;
                $changeShift            = array();
                $queryForGetDailyResult = "SELECT date as date1,Emp_code,Schedule_Shift FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "' AND Actual_Shift!=Schedule_Shift";
               
				$resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $unAuthorigeShift       = 0;
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $changeShift[$row['date1']] = $row['Emp_code'];
                    }
                } else {
                    $changeShift = array();
                }
                
                if (!empty($changeShift)) {
                    foreach ($changeShift as $key1 => $value1) {
                        $queryForGetDailyResult = "SELECT * FROM rost_change WHERE Emp_code='" . $value1 . "' AND RosterDate='" . $key1 . "' AND type_name='approve'";
                        $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        if ($num($resultForGetRecord) <= 0) {
                            $unAuthorigeShift++;
                        }
                    }
                }
				
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $unAuthorigeShift);
                $countForHorizonatalExcel++;
                $shiftAllowance         = 0;
                $attendenceAllowance    = 0;
                $queryForGetDailyResult = "SELECT COUNT(Schedule_Shift) as shiftCount,Schedule_Shift FROM attendancesch  Where Schedule_Shift!='' AND date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "' AND Schedule_Shift!='' AND status in ('P','L') AND Actual_Shift=Schedule_Shift group by Schedule_Shift";
				$resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        if (in_array($row['Schedule_Shift'], $this->getShiftName)) {
                            if (array_key_exists($row['Schedule_Shift'], $this->getshiftAllowance)) {
                                //echo $key.'->'.$row['shiftCount'].'  '.$this->getshiftAllowance[$this->getShiftName[$row['Schedule_Shift']]].'  '.$shiftAllowance; die;
                                $shiftAllowance = $row['shiftCount'] * $this->getshiftAllowance[$row['Schedule_Shift']] + $shiftAllowance;
                            }
                        }
                    }
                }
                //$this->getAttendanceAllowance[] = $rowel['NoOfDays'].'---'.$rowel['Rates'].'---'.$rowel['MinNoOfDays'];
                foreach ($this->getAttendanceAllowance as $key3 => $value3) {
                    $attAllowArr = explode("---", $value3);
                    if ($attAllowArr[2] <= $sumOfLeave && $attAllowArr[0] >= $sumOfLeave) {
                        $attendenceAllowance = $sumOfLeave * $attAllowArr['1'];
                        break;
                    }
                }
                
                $queryForGetDailyResult = "SELECT COUNT(Actual_Shift) as shiftCount,Actual_Shift FROM attendancesch  Where Actual_Shift!=Schedule_Shift AND date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "'  AND status in ('P','L')  group by Actual_Shift";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        if (in_array($row['Actual_Shift'], $this->getShiftName)) {
                            if (array_key_exists($row['Actual_Shift'], $this->getshiftAllowance)) {
                                //	echo $key.'->'.$row['shiftCount'].'  '.$this->getshiftAllowance[$this->getShiftName[$row['Actual_Shift']]].'  '.$shiftAllowance; die;
                                $shiftAllowance = $row['shiftCount'] * $this->getshiftAllowance[$row['Actual_Shift']] + $shiftAllowance;
                            }
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $attendenceAllowance);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftAllowance);
                $countForHorizonatalExcel++;
                
                
                $i++;
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/payRollReport.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/payRollReport.xls');
            
        } else if ($_POST['type'] == '4') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Scheduled Shift",
                "Scheduled Shift Time",
                "Actual Shift",
                "Time In",
                "Time Out",
                "Status",
                "Late Arrival",
                "Early Departure",
                "Hrs Works",
                "Less Hrs Work",
                "Over Stay"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            $this->getShiftName();
            //print_r($_POST); die;
            $resultStatus['error'] = $this->checkDat($_POST['daily'], '', '');
            //print_r($resultStatus); die;
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            $dailyDate                = explode("/", $_POST['daily']);
            $dateForResultCalculation = $dailyDate[2] . "-" . $dailyDate[1] . "-" . $dailyDate[0];
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            $headingInfo             = 'Daily Multiple Report for ' . $dateForResultCalculation;
            $objPHPExcel             = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
            foreach ($advanceFilterConditions as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "SELECT Schedule_Shift,Actual_Shift,Shift_start,Shift_end,Intime,Outtime,status,Latemin,Earlymin,Workinghrs,Overstay FROM attendancesch WHERE date='" . $dateForResultCalculation . "' AND status!='' AND Schedule_Shift!='' AND Emp_code='" . $key . "' " . $where . " order by date";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    
                    while ($row = $fetch($resultForGetRecord)) {
                        $totalLessHoursWork = "0:0";
                        if (trim($row['Shift_start']) != '' && trim($row['Shift_end']) != '' && trim($row['Intime']) != '' && trim($row['Outtime']) != '') {
                            $time_in_24_hour_format_start = date("H:i", strtotime($row['Shift_start']));
                            $time_in_24_hour_format_end   = date("H:i", strtotime($row['Shift_end']));
                            $shiftTiming                  = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                            
                            $time_in_24_hour_format_punch_start = date("H:i", strtotime($row['Intime']));
                            $time_in_24_hour_format_punch_end   = date("H:i", strtotime($row['Outtime']));
                            $userShiftTiming                    = $this->getTimeDifference($time_in_24_hour_format_punch_start, $time_in_24_hour_format_punch_end);
                            
                            if ($shiftTiming > $userShiftTiming) {
                                $timeDiff           = $this->getTimeDifference($userShiftTiming, $shiftTiming);
                                $time_in_24_hour    = date("H:i", strtotime($timeDiff));
                                $totalLessHoursWork = $this->addHourMinutes($time_in_24_hour, $totalLessHoursWork);
                            }
                        } else {
                            if (trim($row['Shift_start']) != '' && trim($row['Shift_end']) != '') {
                                $time_in_24_hour_format_start = date("H:i", strtotime($row['Shift_start']));
                                $time_in_24_hour_format_end   = date("H:i", strtotime($row['Shift_end']));
                                $shiftTiming                  = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                                $time_in_24_hour              = date("H:i", strtotime($shiftTiming));
                                $totalLessHoursWork           = $time_in_24_hour;
                            }
                        }
                        $scheduledShift = $row['Schedule_Shift'];
                        $actualShift    = $row['Actual_Shift'];
						
						if(strtotime($row['Intime']) >0)
						$inTime		=	Date('d-m-Y h:i A',strtotime($row['Intime']));
						else
						$inTime		=	'';

						if(strtotime($row['Outtime'])>0)
						$Outtime		=	Date('d-m-Y h:i A',strtotime($row['Outtime']));
						else
						$Outtime		=	'';
					
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $scheduledShift);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('h:i A',strtotime($row['Shift_start'])) . '-' . Date('h:i A',strtotime($row['Shift_end'])));
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $inTime);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Outtime);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel,$row['status']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Latemin']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Earlymin']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Workinghrs']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalLessHoursWork);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Overstay']);
                        $countForHorizonatalExcel++;
                        
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                }
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/dailyMultipleReport.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/dailyMultipleReport.xls');
            
            
        } else if ($_POST['type'] == '5') {
            
            $HorizontalColumnName     = array(
                "Employee Code",
                "Name",
                "Department",
                "Category",
                "Designation",
                "DOJ",
                "Details"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "DSG_NAME",
                "DOJ"
            );
            $Details                  = array(
                "status" => "status",
                "Schedule_Shift" => "shift schedule",
                "Actual_Shift" => "Shift Actual",
                "Earlymin" => "Early Departure",
                "Latemin" => "Late Arrival",
                "Overtime" => "Over Time"
            );
            $this->getShiftName();
            
            if (!isset($_POST['monthFor']) || !isset($_POST['year']) || $_POST['monthFor'] == '') {
                
                echo "Error Please Select Month";
                die;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields);
            
            if ($_POST['monthFor'] != '' && $_POST['year'] != '') {
                
                $monthDay  = date('t', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $monthName = date('F', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Day Status Report for the ' . $monthName . ' ' . $_POST['year'] . '');
                $objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
                $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E1')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F1')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G1')->setAutoSize(true);
            }
            
            for ($counter = 1; $counter <= $monthDay; $counter++) {
                $HorizontalColumnName[] = $counter;
            }
            
            
            foreach ($HorizontalColumnName as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '', $value);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            
            $counterForExcel++;
            $countForHorizonatalExcel = 0;
            $j                        = 1;
            //echo '<pre>'; print_r($advanceFilterConditions); echo '</pre>'; die;
            foreach ($advanceFilterConditions as $key => $value) {
                $i = 1;
                foreach ($Details as $key1 => $value1) {
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                    $countForHorizonatalExcel++;
                    foreach ($value as $key2 => $value2) {
                        
                        if ($countForHorizonatalExcel == '1')
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                        if ($countForHorizonatalExcel == '2')
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                        if ($countForHorizonatalExcel == '3')
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                        if ($countForHorizonatalExcel == '4')
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DSG_NAME']);
                        if ($countForHorizonatalExcel == '5') {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('d-M-Y',strtotime($value['DOJ'])));
                            $countForHorizonatalExcel++;
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value1);
                        }
                        
                        $countForHorizonatalExcel++;
                        
                    }
                    
                    $getRecordStatus         = array();
                    $getRecordSchedule_Shift = array();
                    $Actual_Shift            = array();
                    $Earlymin                = array();
                    $Latemin                 = array();
                    $Overtime                = array();
                    
                    $queryForGetDailyResult = "SELECT date,status,Schedule_Shift,Actual_Shift,Earlymin,Latemin,Overtime FROM attendancesch WHERE Emp_code='" . $key . "' AND MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "'";
                    $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                    if ($num($resultForGetRecord) > 0) {
                        while ($row = $fetch($resultForGetRecord)) {
                            $getRecordStatus[trim($row['date'])]         = trim($row['status']);
                            $getRecordSchedule_Shift[trim($row['date'])] = $row['Schedule_Shift'];
                            $Actual_Shift[trim($row['date'])]            = $row['Actual_Shift'];
                            $Earlymin[trim($row['date'])]                = trim($row['Earlymin']);
                            $Latemin[trim($row['date'])]                 = trim($row['Latemin']);
                            $Overtime[trim($row['date'])]                = trim($row['Overtime']);
                        }
                    }
                    
                    if ($i == '1') {
                        
                        for ($counter = 1; $counter <= $monthDay; $counter++) {
                            
                            $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                            //if($key == '10154'){ echo $dateforDailyRecord.'-'.$getRecordStatus[$dateforDailyRecord].'  '; }
                            $recordData         = (isset($getRecordStatus[$dateforDailyRecord]) && trim($getRecordStatus[$dateforDailyRecord]) == '') ? trim($getRecordStatus[$dateforDailyRecord]) : '0';
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                            $countForHorizonatalExcel++;
                        }
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    } else if ($i == '2') {
                        
                        for ($counter = 1; $counter <= $monthDay; $counter++) {
                            
                            //if(trim($key) == '10154') echo $getRecordSchedule_Shift[$dateforDailyRecord].' && ';
                            $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                            $recordData         = (isset($getRecordSchedule_Shift[$dateforDailyRecord]) && trim($getRecordSchedule_Shift[$dateforDailyRecord]) != '') ? trim($getRecordSchedule_Shift[$dateforDailyRecord]) : '0';
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                            $countForHorizonatalExcel++;
                        }
                        //if($key == '10154'){ echo $i; die; }
                        
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    } else if ($i == '3') {
                        
                        for ($counter = 1; $counter <= $monthDay; $counter++) {
                            
                            $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                            $recordData         = (isset($Actual_Shift[$dateforDailyRecord]) && trim($Actual_Shift[$dateforDailyRecord]) != '') ? trim($Actual_Shift[$dateforDailyRecord]) : '0';
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                            $countForHorizonatalExcel++;
                        }
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    } else if ($i == '4') {
                        for ($counter = 1; $counter <= $monthDay; $counter++) {
                            
                            $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                            $recordData         = (isset($Earlymin[$dateforDailyRecord]) && trim($Earlymin[$dateforDailyRecord]) != '') ? trim($Earlymin[$dateforDailyRecord]) : '0';
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                            $countForHorizonatalExcel++;
                        }
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    } else if ($i == '5') {
                        for ($counter = 1; $counter <= $monthDay; $counter++) {
                            
                            $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                            $recordData         = (isset($Latemin[$dateforDailyRecord]) && trim($Latemin[$dateforDailyRecord]) != '') ? trim($Latemin[$dateforDailyRecord]) : '0';
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                            $countForHorizonatalExcel++;
                        }
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    } else if ($i == '6') {
                        for ($counter = 1; $counter <= $monthDay; $counter++) {
                            
                            $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                            $recordData         = (isset($Overtime[$dateforDailyRecord]) && trim($Overtime[$dateforDailyRecord]) != '') ? trim($Overtime[$dateforDailyRecord]) : '0';
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                            $countForHorizonatalExcel++;
                        }
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    }
                    $i++;
                }
                
            }
            
            $my_excel_filename = "upload/daily-status-monthly-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/daily-status-monthly-report.xls');
            
        } else if ($_POST['type'] == '6') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $HorizontalColumnName     = array(
                "Date",
                "Strength",
                "Present",
                "Absent",
                "Leave"
            );
            $valueForExcelStatus      = array(
                'Present' => 'P',
                'Absent' => 'A',
                'Leave' => 'L'
            );
            $getFields                = array(
                "Emp_Code"
            );
            $valueForStatus           = array();
            $where                    = '';
            $totalStrength            = $totalPresent = $totalAbsent = $totalLeave = 0;
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where .= " AND shiftId in ('" . $validShiftID . "')";
            }
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            
            if (!empty($advanceFilterConditions)) {
                $validEmpID = implode("','", $advanceFilterConditions);
                $validEmpID = str_replace(",", "','", $validEmpID);
                $where .= " AND Emp_Code in('" . $validEmpID . "')";
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            
            $toDatearr = explode("/", $_POST['toDate']);
            $toDate    = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Employee Strength for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E1')->setAutoSize(true);
            
            foreach ($HorizontalColumnName as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '', $value);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            
            $counterForExcel++;
            
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            
            for ($counter = 1; $counter <= $dateDifference; $counter++) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateforDailyRecord);
                $countForHorizonatalExcel++;
                
                $queryForGetTotalStrength = "SELECT count(status) as status FROM attendancesch WHERE date='" . $dateforDailyRecord . "' AND Schedule_Shift !='' " . $where . "";
                $resultForRecord          = query($query, $queryForGetTotalStrength, $pa, $opt, $ms_db);
                
                if ($num($resultForRecord) > 0) {
                    
                    $row1 = $fetch($resultForRecord);
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row1['status']);
                    $countForHorizonatalExcel++;
                    $totalStrength = $totalStrength + $row1['status'];
                    foreach ($valueForExcelStatus as $key => $value) {
                        
                        $queryForGetDailyResult = "SELECT count(status) as status FROM attendancesch WHERE date='" . $dateforDailyRecord . "' AND status='" . $value . "' AND Schedule_Shift !='' " . $where . "";
                        
                        $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        
                        if ($num($resultForGetRecord) > 0) {
                            
                            while ($row = $fetch($resultForGetRecord)) {
                                if ($value == 'P') {
                                    $totalPresent      = $totalPresent + $row['status'];
                                    $valueForStatus[0] = $totalPresent;
                                } elseif ($value == 'A') {
                                    $totalAbsent       = $totalAbsent + $row['status'];
                                    $valueForStatus[1] = $totalAbsent;
                                } elseif ($value == 'L') {
                                    $totalLeave        = $totalLeave + $row['status'];
                                    $valueForStatus[2] = $totalLeave;
                                }
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['status']);
                                $countForHorizonatalExcel++;
                                
                            }
                            
                        } else {
                            
                            break;
                        }
                        
                    }
                }
                
                $dateforDailyRecord       = date('Y-m-d', strtotime(' +1 day', strtotime($dateforDailyRecord)));
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, "Total");
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
            $countForHorizonatalExcel++;
            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalStrength);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
            $countForHorizonatalExcel++;
            
            for ($i = 0; $i <= 2; $i++) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $valueForStatus[$i]);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            $my_excel_filename = "upload/daily-employee-strength-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/daily-employee-strength-report.xls');
            
        } else if ($_POST['type'] == '7') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $HorizontalColumnName     = array(
                "Shift",
                "Scheduled Shift",
                "Actual",
                "Shift Change",
                "Absent",
                "Leave"
            );
            $valueForExcelStatus      = array(
                'Absent' => 'A',
                'Leave' => 'L'
            );
            $getFields                = array(
                "Emp_Code"
            );
            $where                    = '';
            $advanceFilterConditions  = $this->advanceSearchOptionForJson($getFields, '1');
            $this->getShiftName();
            
            if (!empty($advanceFilterConditions)) {
                $validEmpID = implode("','", $advanceFilterConditions);
                $where .= " AND Emp_code IN ('" . $validEmpID . "')";
            }
			
			if ($_POST['emp_code_list'] != 'HR') {
                
               $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                
                
                $where .= (!empty($_POST['emp_code_list'])) ? " AND Emp_code IN (" . $empArr . ") " : "";
            }
           // echo $where; die("fdfdfd");
            
            
            if (!empty($_POST['shift'])) {
                foreach ($this->getShiftName as $key1 => $value1) {
                    if (!in_array($key1, $_POST['shift'])) {
                        unset($this->getShiftName[$key1]);
                    }
                }
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            $toDatearr   = explode("/", $_POST['toDate']);
            $toDate      = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $headingInfo = 'Employee Strength for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
            
            foreach ($this->getShiftName as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "select COUNT(Id) as id from attendancesch where date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Schedule_Shift='" . $value . "' " . $where . "";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $scheduledShift         = (isset($row['id']) && $row['id'] != '') ? $row['id'] : '0';
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $scheduledShift);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "select count(Id) as actual from attendancesch where date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Actual_Shift='" . $value . "' AND status='P' AND Id not in (SELECT Id FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Schedule_Shift='" . $key . "') " . $where . "";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $actualShift            = (isset($row['actual']) && $row['actual'] != '') ? $row['actual'] : '0';
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "SELECT COUNT(ID) as shiftChange FROM rost_change WHERE RosterDate>='" . $dateforDailyRecord . "' AND RosterDate<='" . $toDate . "' AND type_name='approve' AND shiftMaster='" . $key . "' " . $where . "";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $shiftChange            = (isset($row['shiftChange']) && $row['shiftChange'] != '') ? $row['shiftChange'] : '0';
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftChange);
                $countForHorizonatalExcel++;
                
                foreach ($valueForExcelStatus as $key1 => $value1) {
                    
                    $queryForGetDailyResult = "SELECT count(status) as status FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Schedule_Shift='" . $value . "' AND status='" . $value1 . "' " . $where . "";
                    $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                    $row                    = $fetch($resultForGetRecord);
                    $status                 = (isset($row['status']) && $row['status'] != '') ? $row['status'] : '0';
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $status);
                    $countForHorizonatalExcel++;
                    
                }
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
               
            }
            
            $my_excel_filename = "upload/shift-change-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/shift-change-report.xls');
            
        } else if ($_POST['type'] == '8') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $this->getShiftName();
            $HorizontalColumnName = array(
                "Employee Code",
                "Employee Name",
                "Department",
                "Category",
                "Working Days",
                "Present",
                "Leave",
                "Holiday/Weekly-Off"
            );
            $getFields            = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME"
            );
            $where                = '';
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            $resultStatus['error']   = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            $toDatearr   = explode("/", $_POST['toDate']);
            $toDate      = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $headingInfo = 'Employee Strength for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            
            if (!empty($_POST['shift'])) {
                foreach ($this->getShiftName as $key1 => $value1) {
                    if (!in_array($key1, $_POST['shift'])) {
                        unset($this->getShiftName[$key1]);
                    }
                }
            }
            foreach ($this->getShiftName as $key => $value) {
                $HorizontalColumnName[] = $value;
            }
            
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
           // print_r($advanceFilterConditions); die;
            foreach ($advanceFilterConditions as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                
                $halfDay                = $present = $holiday = $woff = $leave = $absent = '0';
                $queryForGetDailyResult = "SELECT status, COUNT(*) as statusCount FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' GROUP BY status";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        
                        if ($row['status'] == 'P')
                            $present = $row['statusCount'];
                        if ($row['status'] == 'H')
                            $halfDay = $row['statusCount'];
                        if ($row['status'] == 'W')
                            $woff = $row['statusCount'];
                        if ($row['status'] == 'F')
                            $holiDay = $row['statusCount'];
                        if ($row['status'] == 'L')
                            $leave = $row['statusCount'];
                        if ($row['status'] == 'A')
                            $absent = $row['statusCount'];
                    }
                } else {
                    $holiDay = $halfDay = $present = $woff = $leave = $absent = '0';
                }
                $totalPresent = $halfDay + $present;
                $totalHoliDay = $woff + $holiDay;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $leave);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalHoliDay);
                $countForHorizonatalExcel++;
                $shiftCount = array();
                
                $queryForGetDailyResult = "SELECT Schedule_Shift, COUNT(*) as shiftCount FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' AND Schedule_Shift != '' GROUP BY Schedule_Shift order by Schedule_Shift";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        if (in_array($row['Schedule_Shift'], $this->getShiftName)) {
                            $shiftCount[$row['Schedule_Shift']] = $row['shiftCount'];
                        }
                    }
                } else {
                    foreach ($this->getShiftName as $key4 => $value4) {
                        $shiftCount[$key4] = '0';
                    }
                }
              // print_r($shiftCount); die;
                foreach ($this->getShiftName as $key4 => $value4) {
                    if (isset($shiftCount[$value4])) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftCount[$value4]);
                        $countForHorizonatalExcel++;
                    } else {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '0');
                        $countForHorizonatalExcel++;
                    }
                }
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/shift-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/shift-report.xls');
            
            
        } else if ($_POST['type'] == '9') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            
            $HorizontalColumnName = array(
                "Employee Code",
                "Employee Name",
                "Department",
                "Category",
                "Manager",
                "Location",
                "Present",
                "Leave",
                "Miss-Punch",
                "Hlf Days",
                "Absent",
                "Holiday/Weekly-Off",
                "Working Days",
                "Early",
                "Late Arrival",
                "Shift Change",
                "OD"
            );
            $getFields            = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME"
            );
            $where                = '';
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            $fromDatearr             = explode("/", $_POST['fromDate']);
            $dateforDailyRecord      = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            $toDatearr   = explode("/", $_POST['toDate']);
            $toDate      = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $headingInfo = 'Employee Summary for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
            
            //echo '<pre>'; print_r($advanceFilterConditions); die;
            foreach ($advanceFilterConditions as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $queryForGetDailyResult = "SELECT SUM(CASE WHEN Earlymin !='0' THEN 1 ELSE 0 END) AS Early_count
															,SUM(CASE WHEN Latemin !='0' THEN 1 ELSE 0 END) AS Late_count
															,SUM(CASE WHEN status ='P' THEN 1 ELSE 0 END) AS present
															,SUM(CASE WHEN status ='A' THEN 1 ELSE 0 END) AS Absent
															,SUM(CASE WHEN status ='L' THEN 1 ELSE 0 END) AS Leave
															,SUM(CASE WHEN status ='H' THEN 1 ELSE 0 END) AS halfDay
															,SUM(CASE WHEN status ='W' THEN 1 ELSE 0 END) AS woff
															,SUM(CASE WHEN status ='F' THEN 1 ELSE 0 END) AS holiday
															FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' " . $where . "";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                = $fetch($resultForGetRecord);
                $Early_count        = (isset($row['Early_count'])) ? $row['Early_count'] : '0';
                $Late_count         = (isset($row['Late_count'])) ? $row['Late_count'] : '0';
                $present            = (isset($row['present'])) ? $row['present'] : '0';
                $absent             = (isset($row['Absent'])) ? $row['Absent'] : '0';
                $Leave              = (isset($row['Leave'])) ? $row['Leave'] : '0';
                $halfDay            = (isset($row['halfDay'])) ? $row['halfDay'] : '0';
                $woff               = (isset($row['woff'])) ? $row['woff'] : '0';
                $holiday            = (isset($row['holiday'])) ? $row['holiday'] : '0';
                
                $totalPresent = $halfDay + $present;
                $totalHoliDay = $woff + $holiDay;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $leave);
                $countForHorizonatalExcel++;
                $queryForGetDailyResult = "SELECT COUNT(Id) as totalMissPunch FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' AND Schedule_Shift!='' AND ((Intime = '' AND Outtime != '') OR (Intime != '' AND Outtime = ''))";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $totalMissPunch = $row['totalMissPunch'];
                    }
                } else {
                    $totalMissPunch = '0';
                }
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalMissPunch);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $halfDay);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $holiDay);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Early_count);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Late_count);
                $countForHorizonatalExcel++;
                $queryForGetDailyResult = "SELECT COUNT(ID) as shiftChange FROM rost_change WHERE RosterDate>='" . $dateforDailyRecord . "' AND RosterDate<='" . $toDate . "' AND type_name='approve' AND shiftMaster='" . $key . "'";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $shiftChange            = (isset($row['shiftChange']) && $row['shiftChange'] != '') ? $row['shiftChange'] : '0';
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftChange);
                $countForHorizonatalExcel++;
                
                
                /*$total		=	0;
                $dateforDailyRecord1	=	$dateforDailyRecord;
                for($counter =1;$counter<=$dateDifference;$counter++){
                $queryForGetDailyResult			=	"select COUNT(outWorkId) as id from outOnWorkRequest where CreatedBy='".$key."' AND action_status IN ('2','5') AND '".$dateforDailyRecord1."' BETWEEN date_from AND date_to";
                $resultForGetRecord 			=	query($query,$queryForGetDailyResult,$pa,$opt,$ms_db);
                if($num($resultForGetRecord) > 0){
                while ($row = $fetch($resultForGetRecord)){
                $total 		=	$total+$row['id'];
                }
                }else{
                $total 		=	$total+0;
                }
                $dateforDailyRecord1			=	date('Y-m-d', strtotime(' +1 day', strtotime($dateforDailyRecord1)));
                }	
                $objPHPExcel->getActiveSheet()->SetCellValue(''.$columnName[$countForHorizonatalExcel].$counterForExcel,$total);
                $countForHorizonatalExcel++;*/
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
                
            }
            
            $my_excel_filename = "upload/employee-summary-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/employee-summary-report.xls');
            
        } else if ($_POST['type'] == '10') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            if (!isset($_POST['monthFor']) || !isset($_POST['year']) || $_POST['monthFor'] == '') {
                $resultStatus['error'] = "Error";
                json_encode($resultStatus);
                die;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            
            if ($_POST['monthFor'] != '' && $_POST['year'] != '') {
                $monthDay  = date('t', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $monthName = date('F', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $firstDate = date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
            }
            
            for ($counter = 1; $counter <= $monthDay; $counter++) {
                $HorizontalColumnName[] = $counter;
            }
            foreach ($this->getShiftName() as $key => $value) {
                $HorizontalColumnName[] = $value;
            }
            $HorizontalColumnName[] = 'Shift Change';
            $HorizontalColumnName[] = 'Unauthorized Change';
            $HorizontalColumnName[] = 'Authorized change';
            
            $headingInfo              = 'Monthly Shift Wise Performance for ' . $monthName . ' ' . $_POST['year'] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            
            foreach ($advanceFilterConditions as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $dateArray              = array();
                $actualShift            = array();
                $firstDate1             = $firstDate;
                $queryForGetDailyResult = "SELECT status,date,Actual_Shift,Schedule_Shift FROM attendancesch WHERE MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' AND status!='' AND Schedule_Shift!='' " . $where . " AND Emp_code='" . $key . "' order by date";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $dateArray[$row['date']] = $row['status'];
                        if (trim($row['Actual_Shift']) != trim($row['Schedule_Shift']))
                            $actualShift[$row['date']] = trim($row['Actual_Shift']);
                    }
                } else {
                    $dateArray = array();
                }
                
                if (!empty($dateArray)) {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        if (isset($dateArray[$firstDate1])) {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateArray[$firstDate1]);
                            $countForHorizonatalExcel++;
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                            $countForHorizonatalExcel++;
                        }
                        $firstDate1 = date('Y-m-d', strtotime(' +1 day', strtotime($firstDate1)));
                    }
                } else {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                        $countForHorizonatalExcel++;
                    }
                }
                
                $shiftCount = array();
                
                $queryForGetDailyResult = "SELECT Schedule_Shift, COUNT(*) as shiftCount FROM attendancesch WHERE MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' AND Emp_code='" . $key . "' AND Schedule_Shift != '' GROUP BY Schedule_Shift order by Schedule_Shift";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        if (in_array($row['Schedule_Shift'], $this->getShiftName)) {
                            $shiftCount[$row['Schedule_Shift']] = $row['shiftCount'];
                        }
                    }
                } else {
                    foreach ($this->getShiftName as $key4 => $value4) {
                        $shiftCount[$key4] = '0';
                    }
                }
                
                foreach ($this->getShiftName as $key4 => $value4) {
                    if (isset($shiftCount[$value4])) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftCount[$value4]);
                        $countForHorizonatalExcel++;
                    } else {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '0');
                        $countForHorizonatalExcel++;
                    }
                }
                if (!empty($actualShift)) {
                    $totalChangedShift = count($actualShift);
                } else {
                    $totalChangedShift = 0;
                }
                $unAuthorigeShift = 0;
                $AuthorigeShift   = 0;
                $count            = count($actualShift);
                
                if (!empty($actualShift)) {
                    
                    $queryForGetDailyResult = "SELECT RosterDate,shiftMaster FROM rost_change WHERE MONTH(RosterDate)='" . $_POST['monthFor'] . "' AND YEAR(RosterDate)='" . $_POST['year'] . "' AND Emp_code='" . $key . "' AND type_name='approve' ";
                    $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                    if ($num($resultForGetRecord) > 0) {
                        while ($row = $fetch($resultForGetRecord)) {
                            
                            if (array_key_exists($row['RosterDate'], $actualShift)) {
                                if ($row['shiftMaster'] == $actualShift[$row['RosterDate']])
                                    $AuthorigeShift = $AuthorigeShift + 1;
                                else
                                    $unAuthorigeShift = $unAuthorigeShift + 1;
                            } else {
                                $unAuthorigeShift = $unAuthorigeShift + 1;
                            }
                            $count = $count - 1;
                        }
                        if ($count != '0' && $count > '0')
                            $unAuthorigeShift = $unAuthorigeShift + $count;
                    } else {
                        $unAuthorigeShift = count($actualShift);
                    }
                    
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalChangedShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $unAuthorigeShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $AuthorigeShift);
                    $countForHorizonatalExcel++;
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalChangedShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $unAuthorigeShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $AuthorigeShift);
                    $countForHorizonatalExcel++;
                }
                $i++;
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/monthly-shift-wise-performance.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/monthly-shift-wise-performance.xls');
            
        } else if ($_POST['type'] == '11') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Date",
                "IN",
                "OUT",
                "Regularize",
                "Not Regularize",
                "Rejected",
                "Regularize after days",
                "Approve"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where .= " AND shiftId in ('" . $validShiftID . "')";
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['daily'], '', '2');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            if (isset($_POST['daily']) && trim($_POST['daily']) != '') {
                $dailyDate                = explode("/", $_POST['daily']);
                $dateForResultCalculation = $dailyDate[2] . "-" . $dailyDate[1] . "-" . $dailyDate[0];
            } else {
                $resultStatus['error'] = "Error";
                return json_encode($resultStatus);
                die;
            }
            
            $headingInfo              = 'Daily Miss Punch Report ' . $dailyDate[0] . "-" . $dailyDate[1] . "-" . $dailyDate[2];
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            
            
            $dateArray              = array();
            $queryForGetDailyResult = "SELECT date,Intime,Outtime,Emp_code FROM attendancesch WHERE ((Intime='' AND Outtime!='') OR (Intime!='' AND Outtime='')) AND date='" . $dateForResultCalculation . "' " . $where . "";
            $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            if ($num($resultForGetRecord) > 0) {
                while ($row = $fetch($resultForGetRecord)) {
                    $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '4', $row['Emp_code']);
                    foreach ($advanceFilterConditions as $key => $value) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                        $countForHorizonatalExcel++;
                        break;
                    }
					
					if(strtotime($row['Intime'])>0)
					$inTime		=	Date('d-m-Y h:i A',strtotime($row['Intime']));
					else
					$inTime		=	'';

					if(strtotime($row['Outtime'])>0)
					$Outtime		=	Date('d-m-Y h:i A',strtotime($row['Outtime']));
					else
					$Outtime		=	'';
				
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dailyDate[0] . "-" . $dailyDate[1] . "-" . $dailyDate[2]);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel,$inTime);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel,$Outtime);
                    $countForHorizonatalExcel++;
                    $regularize      = $notRegularige = $rejected = $approve = '0';
                    $dateAverageDiff = $counteOfDate = $dateDifference1 = '0';
                    
                    $queryForGetDailyResult1 = "SELECT markPastId,action_status,CreatedOn FROM markPastAttendance WHERE CreatedBy='" . $key . "' AND '" . $row['date'] . "' between date_from AND date_to AND action_status NOT IN ('1','4')";
                    $resultForGetRecord1     = query($query, $queryForGetDailyResult1, $pa, $opt, $ms_db);
                    if ($num($resultForGetRecord1) > 0) {
                        $regularize++;
                        while ($row1 = $fetch($resultForGetRecord1)) {
                            if ($row1['CreatedOn'] != '') {
                                $dateDifference1 = $this->dayDifferencesBetweenDate($row['date'], $row1['CreatedOn']);
                                $dateDifference1 = $dateDifference1 + 1;
                            }
                            if ($row1['action_status'] == '2' || $row1['action_status'] == '5' || $row1['action_status'] == '7') {
                                $approve++;
                            } elseif ($row1['action_status'] == '3' || $row1['action_status'] == '6') {
                                $rejected++;
                            }
                        }
                    } else {
                        $notRegularige++;
                    }
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $regularize);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $notRegularige);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $rejected);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference1);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $approve);
                    $countForHorizonatalExcel++;
                    $i++;
                    $countForHorizonatalExcel = 0;
                    $counterForExcel++;
                }
            }
            
            
            
            
            
            $my_excel_filename = "upload/mis-punch-daily-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/mis-punch-daily-report.xls');
            
        } else if ($_POST['type'] == '12') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Day",
                "Regularize",
                "Not Regularize",
                "Rejected",
                "Regularize after days",
                "Approve"
            );
            $columnName               = array(
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            $resultStatus['error']    = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = " AND shiftId in ('" . $validShiftID . "')";
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $toDatearr          = explode("/", $_POST['toDate']);
            $toDate             = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            
            
            $headingInfo              = 'Monthly Miss Punch Report For ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            $dateDifference         = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference         = $dateDifference + 1;
            $counter                = 0;
            $dateArray              = array();
            $queryForGetDailyResult = "SELECT date,Emp_code FROM attendancesch WHERE ((Intime='' AND Outtime!='') OR (Intime!='' AND Outtime='')) AND (date >= '" . $dateforDailyRecord . "' AND date<='" . $toDate . "')" . $where . "";
            $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            if ($num($resultForGetRecord) > 0) {
                while ($row = $fetch($resultForGetRecord)) {
                    $counter++;
                    $dateArray[] = $row['date'] . '--' . $row['Emp_code'];
                    
                }
            } else {
                $counter = 0;
            }
            
            
            if (!empty($dateArray)) {
                foreach ($dateArray as $key2 => $value2) {
                    $arr     = explode("--", $value2);
                    $date    = $arr[0];
                    $empCode = $arr[1];
                    
                    $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '4', $empCode);
                    
                    foreach ($advanceFilterConditions as $key => $value) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('d-M-Y', strtotime($date)));
                        $countForHorizonatalExcel++;
                        
                        /*
                        status	1	Pending
                        status	2	Approved
                        status	3	Rejected
                        status	4	Cancel
                        status	5	Approved Cancel - Request for cancel
                        status	6	Approved Cancellation
                        status	7	Approved Cancel Rejected
                        */
                        $regularize      = $notRegularige = $rejected = $approve = 0;
                        $dateAverageDiff = $counteOfDate = $dateDifference1 = '0';
                        
                        $queryForGetDailyResult = "SELECT markPastId,action_status,CreatedOn FROM markPastAttendance WHERE CreatedBy='" . $empCode . "' AND '" . $date . "' between date_from AND date_to AND action_status NOT IN ('1','4')";
                        $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        if ($num($resultForGetRecord) > 0) {
                            while ($row = $fetch($resultForGetRecord)) {
                                $regularize++;
                                if ($row['CreatedOn'] != '') {
                                    $dateDifference1  = $this->dayDifferencesBetweenDate($date, $row['CreatedOn']);
                                    $dateDifference1  = $dateDifference1 + 1;
                                    $checkDiffbetDate = 0;
                                }
                                $counteOfDate++;
                                
                                if ($row['action_status'] == '2' || $row['action_status'] == '5' || $row['action_status'] == '7') {
                                    $approve++;
                                } elseif ($row['action_status'] == '3' || $row['action_status'] == '6') {
                                    $rejected++;
                                }
                            }
                        } else {
                            $notRegularige++;
                        }
                        
                        if (isset($checkDiffbetDate))
                            $dateAverageDiff = $dateDifference1 / $counteOfDate;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $regularize);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $notRegularige);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $rejected);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateAverageDiff);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $approve);
                        $countForHorizonatalExcel++;
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                        $i++;
                    }
                }
            }
            $my_excel_filename = "upload/mis-punch-Monthly-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/mis-punch-Monthly-report.xls');
            
        } else if ($_POST['type'] == '13') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            $advanceFilterConditions  = $this->advanceSearchOptionForJson($getFields, '2');
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            if (trim($_POST['monthFor']) != '' && trim($_POST['year']) != '0') {
                $monthDay      = date('t', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $monthName     = date('F', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $firstDate     = date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $dateForQuery  = date('d-M-Y', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $dateForQuery1 = date('d-M-Y', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $monthDay));
            } else {
                echo $error = "Somthing Missing";
                exit;
            }
            
            for ($counter = 1; $counter <= $monthDay; $counter++) {
                $HorizontalColumnName[] = $counter;
            }
            $HorizontalColumnName[] = 'Present Days';
            $HorizontalColumnName[] = 'Weekly Off';
            $HorizontalColumnName[] = 'Holidays';
            $HorizontalColumnName[] = 'Days Worked';
            foreach ($this->LeaveListType() as $key => $value) {
                $HorizontalColumnName[] = $value;
            }
            $HorizontalColumnName[] = ' LWP Days';
            $HorizontalColumnName[] = 'Total Leave';
            $HorizontalColumnName[] = 'Days Absent';
            $HorizontalColumnName[] = 'Total Present Days';
            $HorizontalColumnName[] = 'Un authorized Shift Change';
            $HorizontalColumnName[] = ' Less hrs worked';
            $HorizontalColumnName[] = 'Over Stay';
            $HorizontalColumnName[] = 'Total Hrs worked';
            
            $headingInfo              = 'Monthly Employee Performancefor ' . $monthName . ' ' . $_POST['year'] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            $dateDifference = $this->dayDifferencesBetweenDate($dateForQuery, $dateForQuery1);
            $dateDifference = $dateDifference + 1;
            
            foreach ($advanceFilterConditions as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                
                $dateArray  = array();
                $firstDate1 = $firstDate;
                $Workinghrs = $Earlymin = $Latemin = $Overstay = $actualShift = '0';
                
                $queryForGetDailyResult = "SELECT Schedule_Shift,status,date,Actual_Shift,Overstay,Workinghrs,Latemin,Earlymin FROM attendancesch WHERE MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' AND status!='' AND Schedule_Shift!='' " . $where . " AND Emp_code='" . $key . "' order by date";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $dateArray[$row['date']] = $row['status'];
                        if (trim($row['Overstay']) != '')
                            $Overstay = $Overstay + $row['Overstay'];
                        if (trim($row['Workinghrs']) != '')
                            $Workinghrs = $Workinghrs + $row['Workinghrs'];
                        if (trim($row['Latemin']) != '')
                            $Latemin = $Latemin + $row['Latemin'];
                        if (trim($row['Earlymin']) != '')
                            $Earlymin = $Earlymin + $row['Earlymin'];
                        if (trim($row['Actual_Shift']) != trim($row['Schedule_Shift']))
                            $actualShift++;
                    }
                } else {
                    $dateArray = array();
                }
                
                if (!empty($dateArray)) {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        if (isset($dateArray[$firstDate1])) {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateArray[$firstDate1]);
                            $countForHorizonatalExcel++;
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                            $countForHorizonatalExcel++;
                        }
                        $firstDate1 = date('Y-m-d', strtotime(' +1 day', strtotime($firstDate1)));
                    }
                } else {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '-');
                        $countForHorizonatalExcel++;
                    }
                }
                
                $queryForGetDailyResult = "SELECT SUM(CASE WHEN status ='P' THEN 1 ELSE 0 END) AS present
															,SUM(CASE WHEN status ='A' THEN 1 ELSE 0 END) AS Absent
															,SUM(CASE WHEN status ='L' THEN 1 ELSE 0 END) AS Leave
															,SUM(CASE WHEN status ='H' THEN 1 ELSE 0 END) AS halfDay
															,SUM(CASE WHEN status ='W' THEN 1 ELSE 0 END) AS woff
															,SUM(CASE WHEN status ='F' THEN 1 ELSE 0 END) AS holiday
															FROM attendancesch WHERE MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' " . $where . " AND Emp_Code='" . $key . "'";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                = $fetch($resultForGetRecord);
                $present            = (isset($row['present'])) ? $row['present'] : '0';
                $Absent             = (isset($row['Absent'])) ? $row['Absent'] : '0';
                $Leave              = (isset($row['Leave'])) ? $row['Leave'] : '0';
                $halfDay            = (isset($row['halfDay'])) ? $row['halfDay'] : '0';
                $woff               = (isset($row['woff'])) ? $row['woff'] : '0';
                $holiday            = (isset($row['holiday'])) ? $row['holiday'] : '0';
                $halfDayCalculation = $halfDay / 2;
                $present1           = $present + $halfDayCalculation;
                $dateDifference     = $dateDifference - ($woff + $holiDay);
                $Absent             = $Absent + $halfDayCalculation; // This is for When Somone will take halfDay then other hlafDAy part will be Present Part.
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $woff);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $holiday);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                $totalLeave             = 0;
                $queryForGetDailyResult = "select COUNT(*) as leave,LvType  from leave,master..spt_values 
																	where CreatedBy='" . $key . "' AND type = 'P' AND DATEADD(DAY,number,LvFrom) <= LVTO
																	AND DATEADD(DAY,number,LvFrom) IN (select DATEADD(DAY,number,'" . $dateForQuery . "') [LDate] FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number,'" . $dateForQuery . "') <= '" . $dateForQuery1 . "') GROUP BY LvType HAVING ( COUNT(leaveID) > 1 )";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    foreach ($this->getLeaveType as $key5 => $value5) {
                        $countOFLeave = '-a';
                        while ($row = $fetch($resultForGetRecord)) {
                            if ($key5 == $row['LvType'])
                                $countOFLeave = $row['leave'];
                            break;
                        }
                        if ($countOFLeave == '-a') {
                            $totalLeave = $totalLeave + 0;
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '0');
                            $countForHorizonatalExcel++;
                        } else {
                            $totalLeave = $totalLeave + $countOFLeave;
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $countOFLeave);
                            $countForHorizonatalExcel++;
                        }
                    }
                } else {
                    foreach ($this->getLeaveType as $key5 => $value5) {
                        $totalLeave = $totalLeave + 0;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '0');
                        $countForHorizonatalExcel++;
                    }
                }
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalLeave);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present1);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Earlymin + $Latemin);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Overstay);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Workinghrs);
                $countForHorizonatalExcel++;
                
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
                $i++;
            }
            $my_excel_filename = "upload/monthly-employee-performance.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/monthly-employee-performance.xls');
            
        } else if ($_POST['type'] == '14') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            $advanceFilterConditions  = $this->advanceSearchOptionForJson($getFields, '2');
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr         = explode("/", $_POST['fromDate']);
            $dateforDailyRecord  = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $dateforDailyRecord1 = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $toDatearr           = explode("/", $_POST['toDate']);
            $toDate              = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $firstDate1          = date('d M', strtotime($dateforDailyRecord1));
            $dateDifference      = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference      = $dateDifference + 1;
            
            for ($counter = 1; $counter <= $dateDifference; $counter++) {
                $HorizontalColumnName[] = $firstDate1;
                $firstDate1             = date('d M', strtotime(' +1 day', strtotime($firstDate1)));
            }
            
            $headingInfo              = 'Monthly Attendance Summary For ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            
            foreach ($advanceFilterConditions as $key => $value) {
                $dateforDailyRecord2 = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $queryForGetDailyResult = "SELECT status,date,final_status,LeaveTypeText,odStatus,markPastStatus,statustitle FROM attendancesch WHERE date>='" . $dateforDailyRecord1 . "' AND date<='" . $toDate . "' AND status!='' AND Schedule_Shift!='' AND Emp_code='" . $key . "' " . $where . " order by date";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                   while ($row = $fetch($resultForGetRecord)) {
						if(($row['final_status'] == 'Leave')  && ($row['statustitle'] != 'Half Day')){
							if($row['LeaveTypeText'] != ''){
								$parsed = $this->get_string_between($row['LeaveTypeText'], '(', ')');
								if(trim($parsed) == ''){
									$parsed		=	$row['LeaveTypeText'];
								}
							}	
							$dateArray[$row['date']] = $parsed;
						}else if($row['final_status'] == 'Miss Punch'){
							$dateArray[$row['date']] = 'MIS';
						}else if($row['final_status'] == 'Weekly Off'){
							$dateArray[$row['date']] = 'WO';
						}else if(($row['final_status'] == 'Attendance Regularised') && ($row['markPastStatus'] == 'Approved')){
							$dateArray[$row['date']] = 'C-P';
						}else if($row['final_status'] == 'Holiday'){
							$dateArray[$row['date']] = 'HLD';
						}else if(($row['final_status'] == 'OD Request') && ($row['odStatus'] == 'Approved')){
							$dateArray[$row['date']] = 'OD';
						}else if($row['final_status'] == 'POH'){
							$dateArray[$row['date']] = 'POH';
						}else if($row['final_status'] == 'POW'){
							$dateArray[$row['date']] = 'POW';
						}else if(($row['final_status'] == 'Leave') && ($row['statustitle'] == 'Half Day')){
							if($row['LeaveTypeText'] != ''){
								$parsed = $this->get_string_between($row['LeaveTypeText'], '(', ')');
								if(trim($parsed) == ''){
									$parsed		=	$row['LeaveTypeText'];
								}
							}	
							$dateArray[$row['date']] = 'H_'.$parsed;
						}else{
							$dateArray[$row['date']] = $row['status'];
						}	
                        
                    }                } else {
                    $dateArray = array();
                }
                
                if (!empty($dateArray)) {
                    for ($counter = 1; $counter <= $dateDifference; $counter++) {
                        //if($key == '10545'){ echo $dateDifference." - ".$dateforDailyRecord2.$dateArray[$dateforDailyRecord2].'  ';  }
                        if (isset($dateArray[$dateforDailyRecord2])) {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateArray[$dateforDailyRecord2]);
                            $countForHorizonatalExcel++;
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                            $countForHorizonatalExcel++;
                        }
                        $dateforDailyRecord2 = date('Y-m-d', strtotime(' +1 day', strtotime($dateforDailyRecord2)));
                    }
                } else {
                    for ($counter = 1; $counter <= $dateDifference; $counter++) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                        $countForHorizonatalExcel++;
                    }
                }
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
                $i++;
            }
            
            $my_excel_filename = "upload/monthly-attendance-Summary.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/monthly-attendance-Summary.xls');
            
        } else if ($_POST['type'] == '15') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Scheduled Shift",
                "Scheduled Shift Time",
                "Actual Shift",
                "Time in",
                "Time Out",
                "Status",
                "Late Arrival",
                "Reason",
                "Early Departure",
                "Reason",
                "Hrs Work",
                "Less W. Hrs",
                "Over Stay",
                "Shift Change Self/Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "DEPT_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            //$advanceFilterConditions 				=	$this->advanceSearchOptionForJson($getFields,'4',$empCode);
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            $this->getShiftName();
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $toDatearr          = explode("/", $_POST['toDate']);
            $toDate             = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            
            
            $headingInfo              = 'Shift Change Report For ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            $dateArray              = array();
            $queryForGetDailyResult = "SELECT status,date,Emp_code FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND status!='' AND Schedule_Shift!=Actual_Shift  " . $where . " order by date";
           
			$resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            if ($num($resultForGetRecord) > 0) {
                while ($row = $fetch($resultForGetRecord)) {
                    $dateArray[] = $row['date'] . '--' . $row['Emp_code'];
                }
            } else {
                $dateArray = array();
            }
            
            if (!empty($dateArray)) {
                foreach ($dateArray as $key2 => $value2) {
                    $arr     = explode("--", $value2);
                    $date    = $arr[0];
                    $empCode = $arr[1];
                    
                    $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '4', $empCode);
                    
                    foreach ($advanceFilterConditions as $key => $value) {
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DEPT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                        $countForHorizonatalExcel++;
                        
                        $queryForGetDailyResult = "SELECT Schedule_Shift,Intime,Outtime,Actual_Shift,Shift_start,Shift_end,status,Latemin,Earlymin,Workinghrs,Overstay FROM attendancesch WHERE date='" . $date . "' AND Emp_code='" . $empCode . "'  " . $where . " order by date";
                        $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        if ($num($resultForGetRecord) > 0) {
                            while ($row = $fetch($resultForGetRecord)) {
                                $scheduledShift = $row['Schedule_Shift'];
                                $actualShift    = $row['Actual_Shift'];
								
								if(strtotime($row['Intime'])>0)
								$inTime		=	Date('d-m-Y h:i A',strtotime($row['Intime']));
								else
								$inTime		=	'';

								if(strtotime($row['Outtime'])>0)
								$Outtime		=	Date('d-m-Y h:i A',strtotime($row['Outtime']));
								else
								$Outtime		=	'';
								
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $scheduledShift);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('h:i A',strtotime($row['Shift_start'])) . '-' .Date('h:i A',strtotime($row['Shift_end'])));
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel,$inTime);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Outtime);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['status']);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Latemin']);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Earlymin']);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                                $countForHorizonatalExcel++;
								if(trim($row['Intime']) != '' && trim($row['Outtime']) != '' && !empty($row['Intime']) && !empty($row['Outtime']) && (strtotime($row['Outtime'])>0 && strtotime($row['Intime'])>0) ){
                                $time_in_24_hour_format_start    = date("H:i", strtotime($row['Shift_start']));
                                $time_in_24_hour_format_end      = date("H:i", strtotime($row['Shift_end']));
                                $shiftTiming                     = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                                $time_in_24_hour_format_user     = date("H:i", strtotime($row['Intime']));
                                $time_in_24_hour_format_user_end = date("H:i", strtotime($row['Outtime']));
                                $shiftTiming1                    = $this->getTimeDifference($time_in_24_hour_format_user, $time_in_24_hour_format_user_end);
                                if ($shiftTiming > $shiftTiming1) {
                                    $timeDiff        = $this->getTimeDifference($shiftTiming1, $shiftTiming);
                                    $time_in_24_hour = date("H:i", strtotime($timeDiff));
                                }
								}else{
									$shiftTiming1	=	'';
								}
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftTiming1);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $time_in_24_hour);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Overstay']);
                                $countForHorizonatalExcel++;
                                $queryForGetDailyResult = "SELECT Emp_code,CreatedBy FROM rost_change Where Emp_code='" . $empCode . "' AND type_name='approve' AND RosterDate='" . $date . "'";
                                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                                if ($num($resultForGetRecord) > 0) {
                                    while ($row = $fetch($resultForGetRecord)) {
                                        if ($row['CreatedBy'] == $row['Emp_code'] || trim($row['CreatedBy']) == '') {
                                            $shiftChangeRequest = "Self";
                                        } else {
                                            $shiftChangeRequest = "Manager";
                                        }
                                    }
                                } else {
                                    $shiftChangeRequest = "Self";
                                }
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftChangeRequest);
                                $countForHorizonatalExcel++;
                                
                            }
                        }
                        $i++;
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    }
                }
            }
            
            $my_excel_filename = "upload/shift-change-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/shift-change-report.xls');
        }
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        return json_encode($resultStatus);
        $excelOutput = ob_get_clean();
        
    }
    
    function dayDifferencesBetweenDate($firstDate, $secondDate)
    {
        //print_r(date_parse($firstDate));
        //print_r(date_parse($secondDate)); 
        $firstDate  = strtotime($firstDate);
        $secondDate = strtotime($secondDate);
        $datediff   = $secondDate - $firstDate;
        
        return floor($datediff / (60 * 60 * 24));
        
    }
    
    
    function advanceSearchOption()
    {
        
        $whereCondition = "";
        
        
        $whereCondition .= (!empty($_POST['CompanyName'])) ? " hrdmastqry.COMP_CODE in (" . implode(",", $_POST['CompanyName']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['functionName'])) ? " hrdmastqry.FUNCT_CODE in (" . implode(",", $_POST['functionName']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['sfunction'])) ? " hrdmastqry.SFUNCT_CODE in (" . implode(",", $_POST['sfunction']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['grade'])) ? " hrdmastqry.GRD_CODE in (" . implode(",", $_POST['grade']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['location1'])) ? " hrdmastqry.LOC_CODE in (" . implode(",", $_POST['location1']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['mname'])) ? " hrdmastqry.MNGR_CODE in (" . implode(",", $_POST['mname']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['wlocation'])) ? " hrdmastqry.WLOC_CODE in (" . implode(",", $_POST['wlocation']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['designation'])) ? " hrdmastqry.DSG_CODE in (" . implode(",", $_POST['designation']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['bu'])) ? " hrdmastqry.BussCode in (" . implode(",", $_POST['bu']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['sbu'])) ? " hrdmastqry.SUBBuss_Code in (" . implode(",", $_POST['sbu']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['employeeType'])) ? " hrdmastqry.TYPE_CODE in (" . implode(",", $_POST['employeeType']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['process'])) ? " hrdmastqry.PROC_CODE in (" . implode(",", $_POST['process']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['ccenter'])) ? " hrdmastqry.COST_CODE in (" . implode(",", $_POST['ccenter']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['division'])) ? " hrdmastqry.Divi_Code in (" . implode(",", $_POST['division']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['employee'])) ? " hrdmastqry.Emp_Code in (" . implode(",", $_POST['employee']) . ") AND" : "";
        
        return rtrim($whereCondition, "AND");
        
        
    }
    
    function advanceSearchOptionForJson($getFieldsWhichAreUsedInExcel = null, $type = '', $empID = '')
    {
        
        $whereCondition = "";
        $my_file        = 'upload/' . $this->getFileName();
        $handle         = fopen($my_file, 'r');
        $data           = fread($handle, filesize($my_file));
        $data           = json_decode($data);
        
        $validEmpID            = array();
        $getResult             = array();
        $withoutAnyFilteration = array();
        //echo '<pre>'; print_r($data); echo '</pre>'; die;
        //echo '<pre>'; print_r($getFieldsWhichAreUsedInExcel); echo '</pre>'; die;
        
        
        
        
        foreach ($data as $key) {
            
            $EmpKey     = '';
            $error      = 0;
            $checkEmpty = 0;
            
            if ($_POST['emp_code_list'] != 'HR') {
                
                $empArr = explode(",", $_POST['emp_code_list']);
                //print_r($empArr); die;
                if (!in_array($key->Emp_Code, $empArr) && !empty($key->Emp_Code)) {
                    $key->Emp_Code = '-465248HKL2@';
                } else {
                    //echo 'Start '.$key->Emp_Code.'   ';
                    if ($key->Emp_Code == '60275')
                        $key->Emp_Code = '-465248HKL2@';
                }
            }
            
			if(trim($key->Emp_Code) != '' && !empty($key->Emp_Code)){
				
				if (!empty($_POST['CompanyName'])) {
					$checkEmpty = 1;
					if (in_array($key->COMP_CODE, $_POST['CompanyName'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COMP_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['COMP_NAME'] = $key->COMP_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COMP_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['COMP_NAME'] = $key->COMP_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COMP_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['COMP_NAME']             = $key->COMP_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['COMP_NAME'] = $key->COMP_NAME;
					}
				}
				
				if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DEPT_NAME", $getFieldsWhichAreUsedInExcel)) {
					$getResult[$key->Emp_Code]['DEPT_NAME']             = $key->DEPT_NAME;
					$withoutAnyFilteration[$key->Emp_Code]['DEPT_NAME'] = $key->DEPT_NAME;
				}
				if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DOJ", $getFieldsWhichAreUsedInExcel)) {
					$getResult[$key->Emp_Code]['DOJ']             = $key->DOJ;
					$withoutAnyFilteration[$key->Emp_Code]['DOJ'] = $key->DOJ;
				}
				
				if (!empty($_POST['statusName'])) {
					$checkEmpty = 1;
					if (in_array($key->Status_Code, $_POST['statusName'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("Status_Code", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['Status_Code'] = $key->Status_Code;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("Status_Code", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['Status_Code'] = $key->Status_Code;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("Status_Code", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['Status_Code']             = $key->Status_Code;
						$withoutAnyFilteration[$key->Emp_Code]['Status_Code'] = $key->Status_Code;
					}
				}
				
				if (!empty($_POST['functionName'])) {
					$checkEmpty = 1;
					if (in_array($key->FUNCT_CODE, $_POST['functionName'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("FUNCT_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['FUNCT_NAME'] = $key->FUNCT_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("FUNCT_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['FUNCT_NAME'] = $key->FUNCT_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("FUNCT_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['FUNCT_NAME']             = $key->FUNCT_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['FUNCT_NAME'] = $key->FUNCT_NAME;
					}
				}
				
				if (!empty($_POST['sfunction'])) {
					$checkEmpty = 1;
					if (in_array($key->SFUNCT_CODE, $_POST['sfunction'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBFUNCT_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['SUBFUNCT_NAME'] = $key->SUBFUNCT_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBFUNCT_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['SUBFUNCT_NAME'] = $key->SUBFUNCT_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBFUNCT_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['SUBFUNCT_NAME']             = $key->SUBFUNCT_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['SUBFUNCT_NAME'] = $key->SUBFUNCT_NAME;
					}
				}
				
				if (!empty($_POST['grade'])) {
					$checkEmpty = 1;
					if (in_array($key->GRD_CODE, $_POST['grade'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("GRD_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['GRD_NAME'] = $key->GRD_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("GRD_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['GRD_NAME'] = $key->GRD_NAME;
					}
				} else {
					
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("GRD_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['GRD_NAME']             = $key->GRD_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['GRD_NAME'] = $key->GRD_NAME;
					}
				}
				
				if (!empty($_POST['location1'])) {
					$checkEmpty = 1;
					if (in_array($key->LOC_CODE, $_POST['location1'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("LOC_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['LOC_NAME'] = $key->LOC_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("LOC_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['LOC_NAME'] = $key->LOC_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("LOC_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['LOC_NAME']             = $key->LOC_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['LOC_NAME'] = $key->LOC_NAME;
					}
					
				}
				
				if (!empty($_POST['wlocation'])) {
					$checkEmpty = 1;
					if (in_array($key->WLOC_CODE, $_POST['wlocation'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("WLOC_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['WLOC_NAME'] = $key->WLOC_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("WLOC_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['WLOC_NAME'] = $key->WLOC_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("WLOC_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['WLOC_NAME']             = $key->WLOC_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['WLOC_NAME'] = $key->WLOC_NAME;
					}
				}
				
				if (!empty($_POST['mname'])) {
					$checkEmpty = 1; 
					
					if (in_array($key->MNGR_CODE, $_POST['mname'])) {
					   //echo $EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("MNGR_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['MNGR_NAME'] = $key->MNGR_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("MNGR_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['MNGR_NAME'] = $key->MNGR_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("MNGR_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['MNGR_NAME']             = $key->MNGR_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['MNGR_NAME'] = $key->MNGR_NAME;
					}
				}
				
				if (!empty($_POST['designation'])) {
					$checkEmpty = 1;
					if (in_array($key->DSG_CODE, $_POST['designation'])) {
						
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DSG_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['DSG_NAME'] = $key->DSG_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DSG_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['DSG_NAME'] = $key->DSG_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DSG_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['DSG_NAME']             = $key->DSG_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['DSG_NAME'] = $key->DSG_NAME;
					}
				}
				
				if (!empty($_POST['bu'])) {
					$checkEmpty = 1;
					if (in_array($key->BussCode, $_POST['bu'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("BUSSNAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['BUSSNAME'] = $key->BUSSNAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("BUSSNAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['BUSSNAME'] = $key->BUSSNAME;
					}
				} else {
					
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("BUSSNAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['BUSSNAME']             = $key->BUSSNAME;
						$withoutAnyFilteration[$key->Emp_Code]['BUSSNAME'] = $key->BUSSNAME;
					}
				}
				
				if (!empty($_POST['sbu'])) {
					$checkEmpty = 1;
					if (in_array($key->SUBBuss_Code, $_POST['sbu'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBBUSSNAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['SUBBUSSNAME'] = $key->SUBBUSSNAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBBUSSNAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['SUBBUSSNAME'] = $key->SUBBUSSNAME;
					}
				} else {
					
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBBUSSNAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['SUBBUSSNAME']             = $key->SUBBUSSNAME;
						$withoutAnyFilteration[$key->Emp_Code]['SUBBUSSNAME'] = $key->SUBBUSSNAME;
					}
				}
				
				if (!empty($_POST['employeeType'])) {
					$checkEmpty = 1;
					if (in_array($key->TYPE_CODE, $_POST['employeeType'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("TYPE_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['TYPE_NAME'] = $key->TYPE_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("TYPE_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['TYPE_NAME'] = $key->TYPE_NAME;
					}
				} else {
					
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("TYPE_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['TYPE_NAME']             = $key->TYPE_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['TYPE_NAME'] = $key->TYPE_NAME;
					}
				}
				
				if (!empty($_POST['process'])) {
					$checkEmpty = 1;
					if (in_array($key->PROC_CODE, $_POST['process'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("PROC_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['PROC_NAME'] = $key->PROC_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("PROC_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['PROC_NAME'] = $key->PROC_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("PROC_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['PROC_NAME']             = $key->PROC_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['PROC_NAME'] = $key->PROC_NAME;
					}
				}
				
				if (!empty($_POST['ccenter'])) {
					$checkEmpty = 1;
					if (in_array($key->COST_CODE, $_POST['ccenter'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COST_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['COST_NAME'] = $key->COST_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COST_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['COST_NAME'] = $key->COST_NAME;
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COST_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['COST_NAME']             = $key->COST_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['COST_NAME'] = $key->COST_NAME;
					}
				}
				
				if (!empty($_POST['division'])) {
					$checkEmpty = 1;
					if (in_array($key->Divi_Code, $_POST['division'])) {
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DIVI_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['DIVI_NAME'] = $key->DIVI_NAME;
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DIVI_NAME", $getFieldsWhichAreUsedInExcel))
							$getResult[$key->Emp_Code]['DIVI_NAME'] = $key->DIVI_NAME;
					}
				} else {
					
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DIVI_NAME", $getFieldsWhichAreUsedInExcel)) {
						$getResult[$key->Emp_Code]['DIVI_NAME']             = $key->DIVI_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['DIVI_NAME'] = $key->DIVI_NAME;
					}
				}
				
				if (!empty($_POST['employee'])) {
					$checkEmpty = 1;
					if (in_array($key->Emp_Code, $_POST['employee'])) {
						
						$EmpKey = $key->Emp_Code;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("EMP_NAME", $getFieldsWhichAreUsedInExcel)) {
							$getResult[$key->Emp_Code]['EMP_NAME'] = $key->EMP_NAME;
						}
					} else {
						$EmpKey = '';
						$error  = 1;
						if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("EMP_NAME", $getFieldsWhichAreUsedInExcel)) {
							$getResult[$key->Emp_Code]['EMP_NAME'] = $key->EMP_NAME;
						}
					}
				} else {
					if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("EMP_NAME", $getFieldsWhichAreUsedInExcel)) {
						$EmpKey                                            = $key->Emp_Code;
						$getResult[$key->Emp_Code]['EMP_NAME']             = $key->EMP_NAME;
						$withoutAnyFilteration[$key->Emp_Code]['EMP_NAME'] = $key->EMP_NAME;
					}
				}
            }
			
         //   echo '  ';
            if ($EmpKey != '' && $error == 0) {
                if ($EmpKey != '-465248HKL2@')
                    $validEmpID[] = $EmpKey;
            } else {
                if (isset($getResult[$key->Emp_Code]))
                    unset($getResult[$key->Emp_Code]);
            }
            
            if ($type == '4' && $empID != '') {
                //echo "P";
                if ($empID != $key->Emp_Code) {
                    unset($getResult);
                } else {
                    //print_r($getResult);
                    break;
                }
            }
            
        }
        
        // $validEmpID 	=	implode("','",$validEmpID);
        //echo '<pre>'; print_r($validEmpID); echo '</pre> test'; 
        if (isset($withoutAnyFilteration['-465248HKL2@']))
            unset($withoutAnyFilteration['-465248HKL2@']);
        if (isset($getResult['-465248HKL2@']))
            unset($getResult['-465248HKL2@']);
        //echo '<pre>'; print_r($withoutAnyFilteration); echo '</pre>'; die;
        if ($type != '1') {
            
            if ($checkEmpty == '1' || ($type == '4' && $empID != '')) {
                unset($withoutAnyFilteration);
                return $getResult;
            } else {
                return $withoutAnyFilteration;
            }
        } else {
            return $validEmpID;
        }
        
    }
    
    
    function getHolidays($startDate, $endDate, $empCode = '')
    {
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $where                    = "";
        //get employees data
        $getEmployeeDataSql       = "SELECT LOC_CODE,COMP_CODE,BussCode AS BUS_CODE ,SUBBuss_CODE AS SUBBUS_CODE,WLOC_CODE,FUNCT_CODE AS FUNC_CODE,SFUNCT_CODE AS SUBFUNC_CODE,COST_CODE,PROC_CODE,GRD_CODE,DSG_CODE FROM HrdMastQry WHERE Emp_Code = '" . $empCode . "'";
        $getEmployeeDataSqlResult = query($query, $getEmployeeDataSql, $pa, $opt, $ms_db);
        if ($num($getEmployeeDataSqlResult) > 0) {
            $getEmployeeData = $fetch($getEmployeeDataSqlResult);
            
        }
        
        $result1 = array();
        
        /*   Query For Holidays which are applicable for all Employees without any restriction */
        
        $queryForAll = "SELECT HDATE,HDESC FROM Holidays WHERE ( HDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ) AND H_status=1 
							AND (LOC_CODE is null or LOC_CODE = '') 
							AND (COMP_CODE is null or COMP_CODE = '')
							AND (BUS_CODE is null or BUS_CODE = '')
							AND (SUBBUS_CODE is null or SUBBUS_CODE = '')
							AND (WLOC_CODE is null or WLOC_CODE = '')
							AND (FUNC_CODE is null or FUNC_CODE = '')
							AND (SUBFUNC_CODE is null or SUBFUNC_CODE = '')
							AND (COST_CODE is null or COST_CODE = '')
							AND (PROC_CODE is null or PROC_CODE = '')
							AND (GRD_CODE is null or GRD_CODE = '')
							AND (DSG_CODE is null or DSG_CODE = '')";
        
        $resultq = query($query, $queryForAll, $pa, $opt, $ms_db);
        
        if ($resultq) {
            $tempArray4 = $num($resultq);
        } else {
            $tempArray4 = -1;
        }
        if ($tempArray4 > 0) {
            while ($rowq = $fetch($resultq)) {
                $result1['Weekly Off'][] = array(
                    'title' => $rowq['HDESC'],
                    'start' => date('Y-m-d', strtotime($rowq['HDATE']))
                );
                $forAllHolidays .= "'" . $rowq['HDATE'] . "',";
            }
            
        }
        $forAllHolidays = rtrim($forAllHolidays, ",");
        
        if ($forAllHolidays != '') {
            $wherefor = " AND HDATE NOT IN (" . $forAllHolidays . ")";
            
        }
        
        /*  END Process  */
        
        $queryNew = "SELECT * FROM Holidays WHERE ( HDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ) AND  H_status=1" . $wherefor . "";
        $resultq  = query($query, $queryNew, $pa, $opt, $ms_db);
        
        if ($resultq) {
            $tempArray4 = $num($resultq);
        } else {
            $tempArray4 = -1;
        }
        
        if ($tempArray4 > 0) {
            
            while ($rowy = $fetch($resultq)) {
                
                if (trim($rowy['LOC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['LOC_CODE'] == '')) ? "" : " AND ( ','+LOC_CODE+',' LIKE '%," . $getEmployeeData['LOC_CODE'] . ",%' )";
                }
                if (trim($rowy['COMP_CODE']) != '') {
                    $where .= (trim($getEmployeeData['COMP_CODE'] == '')) ? "" : " AND ( ','+COMP_CODE+',' LIKE '%," . $getEmployeeData['COMP_CODE'] . ",%' )";
                }
                if (trim($rowy['BUS_CODE']) != '') {
                    $where .= (trim($getEmployeeData['BUS_CODE'] == '')) ? "" : " AND ( ','+BUS_CODE+',' LIKE '%," . $getEmployeeData['BUS_CODE'] . ",%' )";
                }
                if (trim($rowy['SUBBUS_CODE']) != '') {
                    $where .= (trim($getEmployeeData['SUBBUS_CODE'] == '')) ? "" : " AND ( ','+SUBBUS_CODE+',' LIKE '%," . $getEmployeeData['SUBBUS_CODE'] . ",%' )";
                }
                if (trim($rowy['WLOC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['WLOC_CODE'] == '')) ? "" : " AND ( ','+WLOC_CODE+',' LIKE '%," . $getEmployeeData['WLOC_CODE'] . ",%' )";
                }
                if (trim($rowy['FUNC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['FUNC_CODE'] == '')) ? "" : " AND ( ','+FUNC_CODE+',' LIKE '%," . $getEmployeeData['FUNC_CODE'] . ",%' )";
                }
                if (trim($rowy['SUBFUNC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['SUBFUNC_CODE'] == '')) ? "" : " AND ( ','+SUBFUNC_CODE+',' LIKE '%," . $getEmployeeData['SUBFUNC_CODE'] . ",%' )";
                }
                if (trim($rowy['COST_CODE']) != '') {
                    $where .= (trim($getEmployeeData['COST_CODE'] == '')) ? "" : " AND ( ','+COST_CODE+',' LIKE '%," . $getEmployeeData['COST_CODE'] . ",%' )";
                }
                if (trim($rowy['PROC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['PROC_CODE'] == '')) ? "" : " AND ( ','+PROC_CODE+',' LIKE '%," . $getEmployeeData['PROC_CODE'] . ",%' )";
                }
                if (trim($rowy['GRD_CODE']) != '') {
                    $where .= (trim($getEmployeeData['GRD_CODE'] == '')) ? "" : " AND ( ','+GRD_CODE+',' LIKE '%," . $getEmployeeData['GRD_CODE'] . ",%' )";
                }
                if (trim($rowy['DSG_CODE']) != '') {
                    $where .= (trim($getEmployeeData['DSG_CODE'] == '')) ? "" : " AND ( ','+DSG_CODE+',' LIKE '%," . $getEmployeeData['DSG_CODE'] . ",%' )";
                }
                
                $sqlq = "SELECT HDATE,HDESC FROM Holidays WHERE HDATE='" . $rowy['HDATE'] . "' AND H_status=1 " . $where . " ";
                
                $resultp = query($query, $sqlq, $pa, $opt, $ms_db);
                
                if ($resultq) {
                    $tempArray4 = $num($resultp);
                } else {
                    $tempArray4 = -1;
                }
                
                if ($tempArray4 > 0) {
                    while ($rowq = $fetch($resultp)) {
                        $result1['Holidays'][] = array(
                            'title' => $rowq['HDESC'],
                            'start' => date('Y-m-d', strtotime($rowq['HDATE']))
                        );
                    }
                    
                }
                $where = "";
            }
            
        }
        
        
        return $result1;
        
    }
    
    function leaveBaseOFDate($from, $to, $empCode, $leaveType)
    {
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $totalNoOFLeave       = 0;
        $getPreviousIdOfLeave = '';
        $leaveArray           = array();
        
        
        $getLeaveData = "select SUM(CASE WHEN LvTo >'" . $to . "' THEN DATEDIFF(day,LvFrom,'" . $to . "')+1 ELSE DATEDIFF(day,LvFrom,LvTo)+1 END) AS leave,LvType,SUM(CASE WHEN FromHalf ='1FH' OR toHalf ='2FH' THEN 1 ELSE 0 END) AS halfDay from leave 
						where LvFrom>='" . $from . "' AND LvFrom<='" . $to . "' AND CreatedBy='" . $empCode . "' AND leave.status in ('2') group by LvType";
       
        $getEmployeeDataSqlResult = query($query, $getLeaveData, $pa, $opt, $ms_db);
        $n                        = 0;
        if ($num($getEmployeeDataSqlResult) > 0) {
            while ($rowel12 = $fetch($getEmployeeDataSqlResult)) {
                $n = 0;
                if (trim($rowel12['halfDay']) != '0') {
                    $n = trim($rowel12['halfDay']) / 2;
                }
                $totalNoOFLeave                       = $n + trim($rowel12['leave']);
                $leaveArray[trim($rowel12['LvType'])] = $totalNoOFLeave;
            }
        }
        
        return $leaveArray;
        
    }
    
    function getFileName()
    {
        
        if ($handle = opendir('upload/')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && strtolower(substr($file, strrpos($file, '.') + 1)) == 'json') {
                    $thelist[] = $file;
                }
            }
            closedir($handle);
        }
        
        if (isset($thelist) && !empty($thelist))
            return $thelist[0];
    }
    
    function createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName)
    {
        
        
        $objPHPExcel->getActiveSheet()->SetCellValue($headingSetIndex, $headingInfo);
        $objPHPExcel->getActiveSheet()->getStyle($headingSetIndex)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
        $objPHPExcel->getActiveSheet()->getStyle($headingSetIndex)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension($headingSetIndex)->setAutoSize(true);
        
        foreach ($HorizontalColumnName as $key => $value) {
            
            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel, $value);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->setAutoSize(true);
            $countForHorizonatalExcel++;
        }
        
        
        return $objPHPExcel;
    }
    
    function columnOfExcel($counter)
    {
        
        $columnName1 = array(
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        );
        $columnName  = $columnName1;
        for ($i = 0; $i <= $counter; $i++) {
            foreach ($columnName1 as $key => $value) {
                $columnName[] = $columnName1[$i] . $value;
            }
        }
        return $columnName;
    }
    
	function getTimeDifference($from, $to)
    {
		//echo $from.'  '.$to;
		//$one	=	"11:45";
		//$two	=	"09:14";
		$test 	=	explode(":",$to);
		$test1 	=	explode(":",$from);

		if($test[1] < $test1[1]){
			$hour		=	$test[0] - 1;
			$minutes 	=	$test[1]+60;
			
			$minutes =	sprintf("%02d", $minutes-$test1[1]);
			$hour	=	sprintf("%02d", $hour-$test1[0]);	
			
		}else{
			
			$minutes =	$test[1]-$test1[1];
			$hour	=	$test[0]-$test1[0];	
			$minutes =	sprintf("%02d", $minutes);
			$hour	=	sprintf("%02d", $hour);	
		}

		
			
        
        return $hour.":".$minutes;
    }
	
    /*function getTimeDifference($from, $to)
    {
        $to_time   = strtotime($from);
        $from_time = strtotime($to);
        return round(abs($to_time - $from_time) / 60 / 60, 2);
    }
    */
    function addHourMinutes($one, $two)
    {
        $test  = explode(":", $one);
        $test1 = explode(":", $two);
        
        $minutes = $test[1] + $test1[1];
        $hour    = $test[0] + $test1[0];
        if ($minutes >= 60) {
            $minutes = sprintf("%02d", $minutes - 60);
            $hour    = sprintf("%02d", $hour + 1);
        }
        return $hour . ":" . $minutes;
    }
    
    function checkDat($startDate, $endDate = '', $type = '')
    {
        
        if ($type == 1) {
            if (isset($startDate) && trim($endDate) == '' && trim($endDate) == '') {
                return "Error";
            }
        } else {
            if (isset($startDate) && trim($startDate) == '')
                return "Error";
        }
    }
    
    function generateHeadCountReport($objPHPExcel)
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $error  = array();
        $dateEx = date('d-M-Y', strtotime(str_replace('/', '-', $_POST['daily']))); //12/04/2017
        
        $getQuery = "SELECT LOV_Text FROM LOVMast where LOV_Field='HeadCount' and LOV_Active ='Y'";
        $result   = query($query, $getQuery, $pa, $opt, $ms_db);
        $value    = $fetch($result);
        $getQuery = $value[0];
        $sql      = str_replace('{DATE}', $dateEx, $getQuery);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'SNO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Description');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'HeadCount');
        
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C1')->setAutoSize(true);
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        $i = 2;
        while ($value = $fetch($result)) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value['SNo']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value['Description']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value['HeadCount']);
            $i++;
        }
        
        $my_excel_filename = "upload/HeadCountReport.xls";
        if (file_exists($my_excel_filename))
            unlink('upload/shift-change-report.xls');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        $resultStatus['error']    = "";
        
        return json_encode($resultStatus);
        //$excelOutput = ob_get_clean();		
        
    }
	function generateAttendancepunchReport($objPHPExcel)
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $error  = array();
        $fromdate = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['fromDate']))); //12/04/2017
         $emp=$_POST['employee'];
         $mystring = implode(',',$emp);
        // echo $mystring;die;
         $todate = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['toDate'])));
       // echo "from date".$fromdate."todate=".$todate.$_POST['emp_code_list'];
         //echo $_POST['emp_code_list'];
          if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] == 'HR') {
                $flag1='1';
            } else {
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                
                $where .= " lovmast.LOV_Field='leave' and leave.status in ('2') and ";
                $where .= (!empty($_POST['emp_code_list'])) ? " hrdmastqry.Emp_Code in (" . $empArr . ") AND" : "";
            }
            //echo "bbbb".$mystring."aaaa";
        if($flag1=='1' && !isset($mystring))
        {
             $getQuery = "select CONVERT(VARCHAR(17),PunchDate,109) as punchtime,CONVERT(VARCHAR(11),PunchDate,6) as punchdate,Emp_code,MachinID from ATTENDANCEALL where cast(PunchDate as date) between '$fromdate' and '$todate' order by EMP_CODE";
        }
        else if($flag1=='1' && isset($emp))
        {

            $getQuery = "select CONVERT(VARCHAR(17),PunchDate,109) as punchtime,CONVERT(VARCHAR(11),PunchDate,6) as punchdate,Emp_code,MachinID from ATTENDANCEALL where cast(PunchDate as date) between '$fromdate' and '$todate' and EMP_CODE IN('".str_replace(",", "','", $mystring)."') order by EMP_CODE";
        }
        else
        {
        $getQuery = "select CONVERT(VARCHAR(19),PunchDate,109) as punchtime,CONVERT(VARCHAR(11),PunchDate,6) as punchdate,Emp_code,MachinID from ATTENDANCEALL where cast(PunchDate as date) between '$fromdate' and '$todate' and EMP_CODE IN($empArr) order by EMP_CODE";
        }
        $result   = query($query, $getQuery, $pa, $opt, $ms_db);
        //$value    = $fetch($result);
       // $getQuery = $value[0];
        //$sql      = str_replace('{DATE}', $dateEx, $getQuery);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Emp_Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'MachinID');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Punch Time');
        
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C1')->setAutoSize(true);
         $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D1')->setAutoSize(true);
        
        //$result = query($query, $sql, $pa, $opt, $ms_db);
        
        $i = 2;
        while ($value = $fetch($result)) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value['Emp_code']);
             $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value['punchdate']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value['MachinID']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $value['punchtime']);
            $i++;
        }
        
        $my_excel_filename = "upload/AttendanceReport.xls";
        if (file_exists($my_excel_filename))
            unlink('upload/shift-change-report.xls');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        $resultStatus['error']    = "";
        
        return json_encode($resultStatus);
        //$excelOutput = ob_get_clean();		
        
    }
    
    function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}
    
    
}

?>