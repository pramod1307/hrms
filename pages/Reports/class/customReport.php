<?php

//ini_set("display_errors","on");   
//error_reporting(E_ALL);

class customReport_class
{
    
    var $designation = array();
    var $functionName = array();
    var $getSubFunctionName = array();
    var $getLocation = array();
    var $getManager = array();
    var $getBusinessUnit = array();
    var $getSubBusinessUnit = array();
    var $wlocationName = array();
    var $costMasterName = array();
    var $getDivisionName = array();
    var $getProcessName = array();
    var $getRegonCodeName = array();
    var $getEmpTypeName = array();
    var $getqualificationName = array();
    var $getStatusName = array();
    var $getLeaveType = array();
    var $yearList = array();
    var $getReportName = array();
    var $getAttendanceAllowance = array();
    var $getshiftAllowance = array();
    var $getShiftName = array();
    
    static $mainkey = 0;
    static $i = 2;
    static $j = 5;
    var $total_late_count = 0;
    
    function __construct()
    {
        //$this->insidefunction($var);  // note the "$this->" to access inside functions
        //getWeeklyOff($var,$var2,$var3); // no "$this->" - function is outside class
    }
    
    
    function yearFunction()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        $sql = "select FY from FinYear";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->yearList[] = $rowel['FY'];
        }
        
        return $this->yearList;
        
    }
    
    function getDesignationName()
    {
        
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        $sql = "select DSG_NAME,DSGID from DsgMast where Dsg_status='1' order by DSG_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->designation[trim($rowel['DSGID'])] = trim($rowel['DSG_NAME']);
        }
        
        return $this->designation;
    }
    
    function getFunctionName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select FUNCT_NAME,FunctID from FUNCTMast where status='1' order by FUNCT_NAME";
        
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->functionName[trim($rowel['FunctID'])] = trim($rowel['FUNCT_NAME']);
        }
        
        //print_r($this->functionName);die;
        
        return $this->functionName;
    }
    
    function getSubFunctionName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select SubFunct_NAME,SubFunctID from SubFunctMast where status='1' order by SubFunct_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getSubFunctionName[trim($rowel['SubFunctID'])] = trim($rowel['SubFunct_NAME']);
        }
        
        return $this->getSubFunctionName;
    }
    
    function getGradeName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select GRDID,GRD_NAME from GrdMast where Grd_status='1' order by GRD_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getGradeName[trim($rowel['GRDID'])] = trim($rowel['GRD_NAME']);
        }
        
        return $this->getGradeName;
    }
    
    function getLocationName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select LOC_NAME,LOCID from locmast where LOC_STATUS='1' order by LOC_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getLocation[trim($rowel['LOCID'])] = trim($rowel['LOC_NAME']);
        }
        
        return $this->getLocation;
    }
    
    function getManagerName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select MNGR_CODE2,MNGR_NAME from HrdMastQry where MNGR_NAME != '' group by MNGR_NAME,MNGR_CODE2 order by MNGR_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getManager[trim($rowel['MNGR_CODE2'])] = trim($rowel['MNGR_NAME']);
        }
        
        return $this->getManager;
    }
    
    function getDepartmentName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select MNGR_CODE2,MNGR_NAME from HrdMastQry where MNGR_NAME != '' group by MNGR_NAME,MNGR_CODE2 order by MNGR_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getManager[trim($rowel['MNGR_CODE2'])] = trim($rowel['MNGR_NAME']);
        }
        
        return $this->getManager;
    }
    
    function getShiftName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select ShiftMastId,Shift_Name from ShiftMast order by Shift_Name";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getShiftName[trim($rowel['ShiftMastId'])] = trim($rowel['Shift_Name']);
        }
        
        return $this->getShiftName;
    }
    function businessUnit()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        
        
        $sql = "select BUSSID,BussName from bussmast where status='1' order by BussName";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getBusinessUnit[trim($rowel['BUSSID'])] = trim($rowel['BussName']);
        }
        
        return $this->getBusinessUnit;
        
    }
    function subBusinessUnit()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select subBussid,subBussName from subBussMast where status='1'  order by subBussName";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getSubBusinessUnit[trim($rowel['subBussid'])] = trim($rowel['subBussName']);
        }
        
        return $this->getSubBusinessUnit;
        
    }
    function wlocation()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select WLOC_CODE,WLOC_NAME from WorkLocMast where Status='1' order by WLOC_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->wlocationName[trim($rowel['WLOC_CODE'])] = trim($rowel['WLOC_NAME']);
        }
        
        return $this->wlocationName;
        
    }
    function costMaster()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select COST_CODE,COST_NAME,CostID from CostMast where Cost_status='1' order by COST_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->costMasterName[trim($rowel['CostID'])] = trim($rowel['COST_NAME']);
        }
        
        return $this->costMasterName;
        
    }
    function division()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select DIVI_NAME,DIVIID from DiviMast order by DIVI_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getDivisionName[trim($rowel['DIVIID'])] = trim($rowel['DIVI_NAME']);
        }
        
        return $this->getDivisionName;
        
    }
    function getProcess()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select PROCID,PROC_NAME from PROCMAST where proc_status='1' order by PROC_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getProcessName[trim($rowel['PROCID'])] = trim($rowel['PROC_NAME']);
        }
        
        return $this->getProcessName;
        
    }
    function getRegonCode()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select REGN_CODE,REGN_NAME from RegnMast order by REGN_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getRegonCodeName[trim($rowel['REGN_CODE'])] = trim($rowel['REGN_NAME']);
        }
        
        return $this->getRegonCodeName;
        
    }
    
    function getEmpType()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select TYPEID,TYPE_NAME from TypeMast where status='1' order by TYPE_NAME";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getEmpTypeName[trim($rowel['TYPEID'])] = trim($rowel['TYPE_NAME']);
        }
        
        return $this->getEmpTypeName;
        
    }
    
    function getQualification()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select QualID,Qual_Name from QualMast where Qual_status='1' order by Qual_Name";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getqualificationName[trim($rowel['QualID'])] = trim($rowel['Qual_Name']);
        }
        
        return $this->getqualificationName;
        
    }
    
    function getStatus()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select Status_Code,Status_Name from StatusMast order by Status_Name";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getStatusName[trim($rowel['Status_Code'])] = trim($rowel['Status_Name']);
        }
        
        return $this->getStatusName;
        
    }
    
    function getReportsName()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select RepCode,RepName from reports order by RepCode";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getReportName[trim($rowel['RepCode'])] = trim($rowel['RepName']);
        }
        
        return $this->getReportName;
        
    }
    
    function getCompany()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $sql = "select COMPID,COMP_NAME from CompMast";
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getCompanyName[trim($rowel['COMPID'])] = trim($rowel['COMP_NAME']);
        }
        
        return $this->getCompanyName;
        
    }
    function LeaveListType()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $queryForGetTotalLeave = "select LOV_Value,LOV_Text from lovmast where LOV_Field='leave' and LOV_Active='A' order by cast(LOV_Value as Numeric(10,0)) asc";
        
        $result = query($query, $queryForGetTotalLeave, $pa, $opt, $ms_db);
        
        while ($rowel = $fetch($result)) {
            $this->getLeaveType[$rowel['LOV_Value']] = $rowel['LOV_Text'];
        }
        
        return $this->getLeaveType;
        
    }
    function shiftAllowance()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $queryForGetTotalLeave = "SELECT Shifts,Rates FROM ShiftAllowance WHERE RuleStatus='1'";
        $result                = query($query, $queryForGetTotalLeave, $pa, $opt, $ms_db);
        while ($rowel = $fetch($result)) {
            $this->getshiftAllowance[$rowel['Shifts']] = $rowel['Rates'];
        }
        return $this->getshiftAllowance;
        
    }
    function AttendanceAllowance()
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $queryForGetTotalLeave = "SELECT NoOfDays,Rates,MinNoOfDays FROM AttendanceAllowance WHERE RuleStatus='1'";
        $result                = query($query, $queryForGetTotalLeave, $pa, $opt, $ms_db);
        while ($rowel = $fetch($result)) {
            $this->getAttendanceAllowance[] = $rowel['NoOfDays'] . '---' . $rowel['Rates'] . '---' . $rowel['MinNoOfDays'];
        }
        return $this->getAttendanceAllowance;
        
    }
    
    function generateCustomReport($objPHPExcel)
    {
        //var_dump($_POST['emp_code_list']);
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $colorCode          = '#ABEBC6';
        $colorCodeForHeader = '#b8d8f5';
        $headingSetIndex    = 'E1';
        $resultStatus       = array();
        $columnName         = $this->columnOfExcel("5");
        
        $resultStatus['error'] = '';
        
        if ($_POST['type'] == '2') {
            
            $monthName            = array(
                "1" => "JAN",
                "2" => "FEB",
                "3" => "MAR",
                "4" => "APR",
                "5" => "MAY",
                "6" => "JUN",
                "7" => "JUL",
                "8" => "AUG",
                "9" => "SEP",
                "10" => "OCT",
                "11" => "NOV",
                "12" => "DEC"
            );
            $getPreviousIdOfLeave = '';
            $where                = '';
            $HorizontalColumnName = array(); // This is use for print Horijontal Column
            $HorizontalMonthName  = array(); // User selected Month  store in this 
            
            if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] == 'HR') {
                $where .= " lovmast.LOV_Field='leave' and leave.status in ('2') and";
            } else {
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                
                $where .= " lovmast.LOV_Field='leave' and leave.status in ('2','5','7') and ";
                $where .= (!empty($_POST['emp_code_list'])) ? " hrdmastqry.Emp_Code in (" . $empArr . ") AND" : "";
            }
            
            // leave.status = 2 (Approved)
            
            if (isset($_POST['month']) && isset($_POST['year'])) {
                
                if (!empty($_POST['month'])) {
                    
                    $HorizontalColumnName[0] = 'Leave Type';
                    $HorizontalMonthName     = $_POST['month'];
                    
                    foreach ($_POST['month'] as $key => $value) {
                        
                        $HorizontalColumnName[] = $monthName[$value];
                        
                    }
                    
                    
                } else {
                    
                    $HorizontalColumnName = array(
                        "0" => "Leave Type",
                        "1" => "JAN",
                        "2" => "FEB",
                        "3" => "MAR",
                        "4" => "APR",
                        "5" => "MAY",
                        "6" => "JUN",
                        "7" => "JUL",
                        "8" => "AUG",
                        "9" => "SEP",
                        "10" => "OCT",
                        "11" => "NOV",
                        "12" => "DEC"
                    );
                    $HorizontalMonthName  = array(
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8",
                        "9",
                        "10",
                        "11",
                        "12"
                    );
                    
                }
                
                if (isset($_POST['year']) && $_POST['year'] != '') {
                    $yearForSelection = $_POST['year'];
                    $where .= " YEAR(leave.LvFrom)=" . $_POST['year'] . " and";
                } else {
                    $yearForSelection = date("Y");
                    $where .= " YEAR(leave.LvFrom)=" . $yearForSelection . " and";
                }
                
            } else {
                $error = "Somthing Missing !";
                exit;
            }
            
            $where .= (!empty($_POST['statusName'])) ? " hrdmastqry.Status_Code in (" . implode(",", $_POST['statusName']) . ") and" : ""; // Check User Status Active or not
            
            $objPHPExcel->createSheet();
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Leave Transaction Report');
            $objPHPExcel->getActiveSheet()->SetCellValue('F1', $yearForSelection);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            
            $advanceFilterConditions = $this->advanceSearchOption();
            
            $counterForExcel          = 2;
            $countForHorizonatalExcel = 0;
            
            /*												*****************  Print Horizontal Column  (Start Code) **************************** 					*/
            
            foreach ($HorizontalColumnName as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '', $value);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            
            /*												*****************  Print Vertical Column  (Start Code) **************************** 					*/
            
            $counterForExcel = 3;
            
            foreach ($this->LeaveListType() as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $counterForExcel, $value);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $counterForExcel)->getFont()->setBold(true);
                $counterForExcel++;
            }
            
            $counterForExcel = 3;
            
            $where .= $advanceFilterConditions;
            if ($advanceFilterConditions == '')
                $where = rtrim($where, "and");
            
            
            /*												*****************  First Loop fetch record for particular Leave  (Start Code) **************************** 					*/
            
            foreach ($this->getLeaveType as $key1 => $value1) {
                
                $countForHorizonatalExcel = 1;
                
                /*												*****************  Second Loop fetch record for particular Month Like Casual leave for January Month  (Start Code) **************************** 					*/
                
                foreach ($HorizontalMonthName as $key => $value) {
                    
                    $totalNoOFLeave   = 0;
                    $daysinMonth      = date("t", strtotime($yearForSelection . '-' . $value . '-01'));
                    $endDateCondition = $yearForSelection . '-' . $value . '-' . $daysinMonth;
                    
                    
                    //	$sql1 =	"select  distinct(leave.leaveID),lovmast.LOV_Text,leave.FromHalf,leave.LvFrom,leave.LvTo,leave.LvDays,leave.Levkey from leave left join lovmast on leave.LvType=lovmast.LOV_Value left join hrdmastqry on leave.CreatedBy=hrdmastqry.Emp_Code  left join Roster_schema on Roster_schema.Emp_Code=leave.CreatedBy left join att_roster on att_roster.roster=Roster_schema.RosterName where  $where and MONTH(leave.LvFrom)='".$value."' and leave.LvType='".$key1."' order by leave.leaveID asc";
                    $sql1 = "select  distinct(leave.leaveID),lovmast.LOV_Text,hrdmastqry.Emp_Code,leave.CreatedBy,leave.FromHalf,leave.LvFrom,leave.LvTo,leave.LvDays,leave.Levkey from leave left join lovmast on leave.LvType=lovmast.LOV_Value left join hrdmastqry on leave.CreatedBy=hrdmastqry.Emp_Code  where  $where and MONTH(leave.LvFrom)='" . $value . "' and leave.LvType='" . $key1 . "' order by leave.leaveID asc";
                    
                    $result12 = query($query, $sql1, $pa, $opt, $ms_db);
                    
                    while ($rowel12 = $fetch($result12)) {
                        
                        /*  This #Condition will work when user select any shift for searching  */
                        
                        if (!empty($_POST['shift'])) {
                            
                            $sqlone = "select top 1 att_roster.shiftMaster from Roster_schema left join att_roster on Roster_schema.RosterName=att_roster.roster where Roster_schema.start_rost='" . $rowel12['LvFrom'] . "' and Roster_schema.Emp_Code='" . $rowel12['CreatedBy'] . "'   order by Roster_schema.RostID desc";
                            
                            $result123 = query($query, $sqlone, $pa, $opt, $ms_db);
                            
                            while ($rowel123 = $fetch($result123)) {
                                
                                if (in_array($rowel123['shiftMaster'], $_POST['shift'])) {
                                    
                                    if ($rowel12['Levkey'] != $getPreviousIdOfLeave) {
                                        
                                        if ($rowel12['LvTo'] != '' && $rowel12['LvFrom'] != '') {
                                            
                                            
                                            
                                            if (strtotime($rowel12['LvTo']) > strtotime($endDateCondition)) {
                                                
                                                
                                                $toDate         = strtotime($endDateCondition); // or your date as well
                                                $fromDate       = strtotime($rowel12['LvFrom']);
                                                $datediff       = $toDate - $fromDate;
                                                $datediff       = floor($datediff / (60 * 60 * 24));
                                                $totalNoOFLeave = $totalNoOFLeave + $datediff + 1;
                                                if ($rowel12['FromHalf'] != '') {
                                                    if (trim($rowel12['FromHalf']) == '2FH' || trim($rowel12['FromHalf']) == '1FH') {
                                                        $totalNoOFLeave = $totalNoOFLeave - 0.50;
                                                    }
                                                }
                                                
                                            } else {
                                                
                                                $totalNoOFLeave = $totalNoOFLeave + $rowel12['LvDays'];
                                                
                                            }
                                            
                                        }
                                        $getPreviousIdOfLeave = $rowel12['Levkey'];
                                    }
                                }
                            }
                            
                        } else {
                            
                            if ($rowel12['Levkey'] != $getPreviousIdOfLeave) {
                                
                                if ($rowel12['LvTo'] != '' && $rowel12['LvFrom'] != '') {
                                    
                                    
                                    
                                    if (strtotime($rowel12['LvTo']) > strtotime($endDateCondition)) {
                                        
                                        //echo $endDateCondition.' end ## ';
                                        $toDate         = strtotime($endDateCondition); // or your date as well
                                        $fromDate       = strtotime($rowel12['LvFrom']);
                                        $datediff       = $toDate - $fromDate;
                                        $datediff       = floor($datediff / (60 * 60 * 24));
                                        $totalNoOFLeave = $totalNoOFLeave + $datediff + 1;
                                        if ($rowel12['FromHalf'] != '') {
                                            if (trim($rowel12['FromHalf']) == '2FH' || trim($rowel12['FromHalf']) == '1FH') {
                                                $totalNoOFLeave = $totalNoOFLeave - 0.50;
                                            }
                                        }
                                        
                                    } else {
                                        
                                        $totalNoOFLeave = $totalNoOFLeave + $rowel12['LvDays'];
                                        
                                    }
                                    
                                }
                                $getPreviousIdOfLeave = $rowel12['Levkey'];
                            }
                            
                            
                        }
                        
                        /*  This #Condition End 		*/
                        
                        
                    }
                    
                    
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalNoOFLeave);
                    
                    $countForHorizonatalExcel++;
                    
                }
                
                /*												*****************  Second Loop (End Code) **************************** 					*/
                
                $counterForExcel++;
            }
            
            /*												*****************  First Loop (End Code) **************************** 					*/
            
            $my_excel_filename = "upload/LeaveTransaction.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/LeaveTransaction.xls');
            
            
        } else if ($_POST['type'] == '3') {
            
            $employeeLeave            = "";
            $sumOfLeave               = 0;
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $where                    = '';
            $valueForFillExcelData    = array();
            $HorizontalColumnName     = array(
                "S.No",
                "Employee Code",
                "Employee Name",
                "Function",
                "SUB Function",
                "Department",
                "Designation",
                "Location",
                "Manager Name",
                "Month Days",
                "Present Day",
                "Weekly off",
                "Holidays",
                "Days Worked"
            );
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            
            $this->getShiftName();
            $this->AttendanceAllowance();
            $this->shiftAllowance();
            
            foreach ($this->LeaveListType() as $key => $value) {
                $HorizontalColumnName[] = $value;
            }
            
            $HorizontalColumnName[] = 'Total Leave';
            $HorizontalColumnName[] = 'Day Absent';
            $HorizontalColumnName[] = 'Payroll Days';
            $HorizontalColumnName[] = 'No. of Late/Early';
            $HorizontalColumnName[] = 'Un authorized Shift Change ';
            $HorizontalColumnName[] = 'Attendance Allowance';
            $HorizontalColumnName[] = 'Shift Allowance';
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            
            
            $fromDatearr         = explode("/", $_POST['fromDate']);
            $dateforDailyRecord  = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $toDatearr           = explode("/", $_POST['toDate']);
            $toDate              = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $dateforDailyRecord1 = date('d-M-Y', strtotime($dateforDailyRecord));
            $toDate1             = date('d-M-Y', strtotime($toDate));
            
            /*												*****************  Print Horizontal Column  (Start Code) **************************** 					*/
            
            
            $headingInfo = 'Payroll Report for ' . $dateforDailyRecord1 . ' To ' . $toDate1 . '';
            $objPHPExcel = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $counterForExcel++;
            $countForHorizonatalExcel = 0;
            
            
            $i                         = 1;
            $dayDifferencesBetweenDate = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dayDifferencesBetweenDate = $dayDifferencesBetweenDate + 1;
            $advanceFilterConditions   = $this->advanceSearchOptionForJson($getFields, '2');
            
            foreach ($advanceFilterConditions as $key => $value) {
                $dayDifferencesBetweenDate = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
                $dayDifferencesBetweenDate = $dayDifferencesBetweenDate + 1;
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dayDifferencesBetweenDate);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "SELECT SUM(CASE WHEN status ='P' THEN 1 ELSE 0 END) AS present
															,SUM(CASE WHEN status ='A' THEN 1 ELSE 0 END) AS Absent
															,SUM(CASE WHEN status ='L' THEN 1 ELSE 0 END) AS Leave
															,SUM(CASE WHEN status ='H' THEN 1 ELSE 0 END) AS halfDay
															,SUM(CASE WHEN status ='W' THEN 1 ELSE 0 END) AS woff
															,SUM(CASE WHEN status ='F' THEN 1 ELSE 0 END) AS holiday
															,SUM(CASE WHEN Lateflag != '' OR Earlyflag != '' THEN 1 ELSE 0 END) AS lateEarly
															FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "'";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                = $fetch($resultForGetRecord);
                $present            = (isset($row['present'])) ? $row['present'] : '0';
                $Absent             = (isset($row['Absent'])) ? $row['Absent'] : '0';
                $Leave              = (isset($row['Leave'])) ? $row['Leave'] : '0';
                $halfDay            = (isset($row['halfDay'])) ? $row['halfDay'] : '0';
                $woff               = (isset($row['woff'])) ? $row['woff'] : '0';
                $holiday            = (isset($row['holiday'])) ? $row['holiday'] : '0';
                $lateEarly          = (isset($row['lateEarly'])) ? $row['lateEarly'] : '0';
                $halfDayCalculation = $halfDay / 2;
                
                $dayDifferencesBetweenDate1 = $dayDifferencesBetweenDate - ($woff + $holiDay);
                $Absent                     = $Absent + $halfDayCalculation; // This is for When Somone will take halfDay then other hlafDAy part will be Present Part.
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dayDifferencesBetweenDate1);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $woff);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $holiday);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dayDifferencesBetweenDate1);
                $countForHorizonatalExcel++;
                $sumOfLeave    = 0;
                $employeeLeave = $this->leaveBaseOFDate($dateforDailyRecord, $toDate, $key);
                
                $leaveWithoutPay = 0;
                foreach ($this->getLeaveType as $key1 => $value1) {
                    
                    if (isset($employeeLeave[$key1])) {
                        $sumOfLeave = $sumOfLeave + $employeeLeave[$key1];
                        if ($key1 == '4')
                            $leaveWithoutPay = $employeeLeave[$key1];
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $employeeLeave[$key1]);
                        $countForHorizonatalExcel++;
                    } else {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                        $countForHorizonatalExcel++;
                    }
                }
                $present1 = $dayDifferencesBetweenDate1 - ($leaveWithoutPay + $Absent);
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $sumOfLeave);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present1);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $lateEarly);
                $countForHorizonatalExcel++;
                $changeShift            = array();
                $queryForGetDailyResult = "SELECT date as date1,Emp_code,Schedule_Shift FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "' AND Actual_Shift!=Schedule_Shift";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $unAuthorigeShift   = 0;
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $changeShift[$row['date1']] = $row['Emp_code'];
                    }
                } else {
                    $changeShift = array();
                }
                
                if (!empty($changeShift)) {
                    foreach ($changeShift as $key1 => $value1) {
                        $queryForGetDailyResult = "SELECT * FROM rost_change WHERE Emp_code='" . $value1 . "' AND RosterDate='" . $key1 . "' AND type_name='approve'";
                        $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        if ($num($resultForGetRecord) <= 0) {
                            $unAuthorigeShift++;
                        }
                    }
                }
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $unAuthorigeShift);
                $countForHorizonatalExcel++;
                $shiftAllowance         = 0;
                $attendenceAllowance    = 0;
                $queryForGetDailyResult = "SELECT COUNT(Schedule_Shift) as shiftCount,Schedule_Shift FROM attendancesch  Where Schedule_Shift!='' AND date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "' AND Schedule_Shift!='' AND status in ('P','L') AND Actual_Shift=Schedule_Shift group by Schedule_Shift";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        if (in_array($row['Schedule_Shift'], $this->getShiftName)) {
                            if (array_key_exists($row['Schedule_Shift'], $this->getshiftAllowance)) {
                                //echo $key.'->'.$row['shiftCount'].'  '.$this->getshiftAllowance[$this->getShiftName[$row['Schedule_Shift']]].'  '.$shiftAllowance; die;
                                $shiftAllowance = $row['shiftCount'] * $this->getshiftAllowance[$row['Schedule_Shift']] + $shiftAllowance;
                            }
                        }
                    }
                }
                //$this->getAttendanceAllowance[] = $rowel['NoOfDays'].'---'.$rowel['Rates'].'---'.$rowel['MinNoOfDays'];
                foreach ($this->getAttendanceAllowance as $key3 => $value3) {
                    $attAllowArr = explode("---", $value3);
                    if ($attAllowArr[2] <= $sumOfLeave && $attAllowArr[0] >= $sumOfLeave) {
                        $attendenceAllowance = $sumOfLeave * $attAllowArr['1'];
                        break;
                    }
                }
                
                $queryForGetDailyResult = "SELECT COUNT(Actual_Shift) as shiftCount,Actual_Shift FROM attendancesch  Where Actual_Shift!=Schedule_Shift AND date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' " . $where . " AND Emp_Code='" . $key . "'  AND status in ('P','L')  group by Actual_Shift";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        if (in_array($row['Actual_Shift'], $this->getShiftName)) {
                            if (array_key_exists($row['Actual_Shift'], $this->getshiftAllowance)) {
                                //	echo $key.'->'.$row['shiftCount'].'  '.$this->getshiftAllowance[$this->getShiftName[$row['Actual_Shift']]].'  '.$shiftAllowance; die;
                                $shiftAllowance = $row['shiftCount'] * $this->getshiftAllowance[$row['Actual_Shift']] + $shiftAllowance;
                            }
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $attendenceAllowance);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftAllowance);
                $countForHorizonatalExcel++;
                
                
                $i++;
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/payRollReport.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/payRollReport.xls');
            
        } else if ($_POST['type'] == '4') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Scheduled Shift",
                "Scheduled Shift Time",
                "Actual Shift",
                "Time In",
                "Time Out",
                "Status",
                "Late Arrival",
                "Early Departure",
                "Hrs Works",
                "Less Hrs Work",
                "Over Stay"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            $this->getShiftName();
            //print_r($_POST); die;
            $resultStatus['error'] = $this->checkDat($_POST['daily'], '', '');
            //print_r($resultStatus); die;
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            $dailyDate                = explode("/", $_POST['daily']);
            $dateForResultCalculation = $dailyDate[2] . "-" . $dailyDate[1] . "-" . $dailyDate[0];
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            $headingInfo             = 'Daily Multiple Report for ' . $dateForResultCalculation;
            $objPHPExcel             = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
            foreach ($advanceFilterConditions as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "SELECT Leavestatus,Emp_code,final_status,LeaveTypeText,odStatus,markPastStatus,statustitle,Lateflag, Earlyflag,date,Schedule_Shift,Actual_Shift,Shift_start,Shift_end,Intime,Outtime,status,Latemin,Earlymin,Workinghrs,Overstay FROM attendancesch WHERE date='" . $dateForResultCalculation . "' AND status!='' AND Schedule_Shift!='' AND Emp_code='" . $key . "' " . $where . " order by date";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    
                    while ($row = $fetch($resultForGetRecord)) {
                        $status             = $this->getDayFinalStatus($row);
                        $totalLessHoursWork = "0:0";
                        if (trim($row['Shift_start']) != '' && trim($row['Shift_end']) != '' && trim($row['Intime']) != '' && trim($row['Outtime']) != '') {
                            $time_in_24_hour_format_start = date("H:i", strtotime($row['Shift_start']));
                            $time_in_24_hour_format_end   = date("H:i", strtotime($row['Shift_end']));
                            $shiftTiming                  = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                            
                            $time_in_24_hour_format_punch_start = date("H:i", strtotime($row['Intime']));
                            $time_in_24_hour_format_punch_end   = date("H:i", strtotime($row['Outtime']));
                            $userShiftTiming                    = $this->getTimeDifference($time_in_24_hour_format_punch_start, $time_in_24_hour_format_punch_end);
                            
                            if ($shiftTiming > $userShiftTiming) {
                                $timeDiff           = $this->getTimeDifference($userShiftTiming, $shiftTiming);
                                $time_in_24_hour    = date("H:i", strtotime($timeDiff));
                                $totalLessHoursWork = $this->addHourMinutes($time_in_24_hour, $totalLessHoursWork);
                            }
                        } else {
                            if (trim($row['Shift_start']) != '' && trim($row['Shift_end']) != '') {
                                $time_in_24_hour_format_start = date("H:i", strtotime($row['Shift_start']));
                                $time_in_24_hour_format_end   = date("H:i", strtotime($row['Shift_end']));
                                $shiftTiming                  = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                                $time_in_24_hour              = date("H:i", strtotime($shiftTiming));
                                $totalLessHoursWork           = $time_in_24_hour;
                            }
                        }
                        $scheduledShift = $row['Schedule_Shift'];
                        if (trim($scheduledShift) != '' && !empty($scheduledShift)) {
                            $scheduledShiftArr = explode(" ", $row['Schedule_Shift']);
                            $scheduledShift    = $scheduledShiftArr[0];
                            if ($status == 'POH' || $status == 'POW' || $status == 'HLD' || $status == 'WO') {
                                $scheduledShift = '';
                            }
                        }
                        
                        $actualShift = $row['Actual_Shift'];
                        if (trim($actualShift) != '' && !empty($actualShift)) {
                            $actualShiftArr = explode(" ", $row['Actual_Shift']);
                            
                            $actualShift = $actualShiftArr[0];
                            if ($status == 'A' || $status == 'POH' || $status == 'POW' || $status == 'HLD' || $status == 'WO') {
                                $actualShift = '';
                            }
                        }
                        
                        if (strtotime($row['Intime']) > 0)
                            $inTime = Date('d-m-Y h:i A', strtotime($row['Intime']));
                        else
                            $inTime = '';
                        
                        if (strtotime($row['Outtime']) > 0)
                            $Outtime = Date('d-m-Y h:i A', strtotime($row['Outtime']));
                        else
                            $Outtime = '';
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $scheduledShift);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('h:i A', strtotime($row['Shift_start'])) . '-' . Date('h:i A', strtotime($row['Shift_end'])));
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $inTime);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Outtime);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $status);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Latemin']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Earlymin']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Workinghrs']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalLessHoursWork);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Overstay']);
                        $countForHorizonatalExcel++;
                        
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                    $countForHorizonatalExcel++;
                }
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/dailyMultipleReport.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/dailyMultipleReport.xls');
            
            
        } else if ($_POST['type'] == '5') {
            
            $HorizontalColumnName     = array(
                "Employee Code",
                "Name",
                "Department",
                "Category",
                "Designation",
                "DOJ",
                "Details"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "EMP_NAME",
                "DOJ"
            );
            $Details                  = array(
                "status" => "status",
                "Schedule_Shift" => "shift schedule",
                "Actual_Shift" => "Shift Actual",
                "Earlymin" => "Early Departure",
                "Latemin" => "Late Arrival",
                "Overtime" => "Over Stay"
            );
            $this->getShiftName();
            
            if (!isset($_POST['monthFor']) || !isset($_POST['year']) || $_POST['monthFor'] == '') {
                
                echo "Error Please Select Month";
                die;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            $where                   = '';
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            if ($_POST['monthFor'] != '' && $_POST['year'] != '') {
                
                $monthDay  = date('t', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $monthName = date('F', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Day Status Report for the ' . $monthName . ' ' . $_POST['year'] . '');
                $objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
                $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E1')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F1')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G1')->setAutoSize(true);
            }
            
            for ($counter = 1; $counter <= $monthDay; $counter++) {
                $HorizontalColumnName[] = $counter;
            }
            
            
            foreach ($HorizontalColumnName as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '', $value);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            
            $counterForExcel++;
            $countForHorizonatalExcel = 0;
            $j                        = 1;
            // echo '<pre>'; print_r($advanceFilterConditions); echo '</pre>'; die;
            
            foreach ($advanceFilterConditions as $key => $value) {
                $this->total_late_count  = '0';
                $i                       = 1;
                $getRecordStatus         = array();
                $getRecordSchedule_Shift = array();
                $Actual_Shift            = array();
                $Earlymin                = array();
                $Latemin                 = array();
                $Overtime                = array();
                $queryForGetDailyResult  = "SELECT Leavestatus,Emp_code,final_status,LeaveTypeText,odStatus,markPastStatus,statustitle,Lateflag, Earlyflag,date,status,Schedule_Shift,Actual_Shift,Earlymin,Latemin,Overstay,Intime,Outtime FROM attendancesch WHERE Emp_code='" . $key . "' AND MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' " . $where . "";
                $resultForGetRecord      = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $finalStatus                                 = $this->getDayFinalStatus($row);
                        $leaveDay                                    = $this->checkLeave($row);
                        $getRecordStatus[trim($row['date'])]         = trim($finalStatus);
                        $getRecordSchedule_Shift[trim($row['date'])] = $row['Schedule_Shift'] . ' @@@' . $finalStatus;
                        $Actual_Shift[trim($row['date'])]            = $row['Actual_Shift'] . ' @@@' . $finalStatus . ' @@@ ' . $leaveDay;
                        $Earlymin[trim($row['date'])]                = $row['Earlymin'] . ' @@@' . $finalStatus;
                        $Latemin[trim($row['date'])]                 = $row['Latemin'] . ' @@@' . $finalStatus;
                        $Overtime[trim($row['date'])]                = $row['Overstay'] . ' @@@' . $finalStatus;
                        /*	if($row['Overstay'] == '0'){
                        $Overtime[trim($row['date'])]            =	'';
                        }else{
                        $overStayArr 							 =	explode(":",$row['Overstay']);
                        if($overStayArr[0]>=10)
                        $Overtime[trim($row['date'])]            = 	$row['Overstay'];
                        else
                        $Overtime[trim($row['date'])]			 =	'';	
                        }
                        */
                    }
                }
                foreach ($Details as $key1 => $value1) {
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['DSG_NAME']);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('d-M-Y', strtotime($value['DOJ'])));
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value1);
                    $countForHorizonatalExcel++;
                    
                    switch ($key1) {
                        case "status":
                            
                            for ($counter = 1; $counter <= $monthDay; $counter++) {
                                $setDoj = 0;
                                
                                $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                                if (isset($value['DOJ']) && strtotime($value['DOJ']) > 0) {
                                    if (strtotime($value['DOJ']) > strtotime($dateforDailyRecord2)) {
                                        $setDoj = 1;
                                    }
                                }
                                
                                if ($setDoj == 1) {
                                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                                    $countForHorizonatalExcel++;
                                } else {
                                    
                                    if (isset($getRecordStatus[$dateforDailyRecord])) {
                                        $data_val = $getRecordStatus[$dateforDailyRecord];
                                    } else {
                                        $getRecordStatus[$dateforDailyRecord] = '-';
                                        $data_val                             = '-';
                                    }
                                    
                                    $getRecordStatus[$dateforDailyRecord] = (strpos($data_val, '~~1') > 0) ? str_replace('~~1', '', $data_val) : $data_val;
                                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $getRecordStatus[$dateforDailyRecord]);
                                    $styleArray = array(
                                        'fill' => array(
                                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'color' => array(
                                                'rgb' => 'FF0000'
                                            )
                                        )
                                    );
                                    if (strpos($data_val, '~~1') > 0 && (($getRecordStatus[$dateforDailyRecord] != 'HLD') && ($getRecordStatus[$dateforDailyRecord] != 'WO'))) {
                                        //  $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . $counterForExcel)->applyFromArray($styleArray);
                                    }
                                    $countForHorizonatalExcel++;
                                    
                                }
                                $setDoj = 0;
                            }
                            $countForHorizonatalExcel = 0;
                            $counterForExcel++;
                            break;
                        
                        case "Schedule_Shift":
                            for ($counter = 1; $counter <= $monthDay; $counter++) {
                                
                                //if(trim($key) == '10154') echo $getRecordSchedule_Shift[$dateforDailyRecord].' && ';
                                $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                                $recordData         = (isset($getRecordSchedule_Shift[$dateforDailyRecord]) && trim($getRecordSchedule_Shift[$dateforDailyRecord]) != '') ? trim($getRecordSchedule_Shift[$dateforDailyRecord]) : '';
                                
                                if (trim($recordData) != '' && !empty($recordData)) {
                                    
                                    $recordDataArr = explode(" ", trim($recordData));
                                    $recordData5   = $recordDataArr[0];
                                    if (trim($recordData5) != '') {
                                        $recordDataVal = explode("@@@", $recordData);
                                        
                                        if (isset($recordDataVal[0]) && trim($recordDataVal[0]) == '') {
                                            $recordData5     = '';
                                            $statusForAbsent = '';
                                        } else {
                                            $statusForAbsent = $recordDataVal[1];
                                        }
                                        
                                        if ($statusForAbsent == 'HLD' || $statusForAbsent == 'WO' || $statusForAbsent == 'POH' || $statusForAbsent == 'POW') {
                                            $recordData5 = '';
                                        }
                                    }
                                    if ($recordData5 == '0')
                                        $recordData5 = '';
                                } else {
                                    $recordData5 = '';
                                }
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData5);
                                $countForHorizonatalExcel++;
                            }
                            $countForHorizonatalExcel = 0;
                            $counterForExcel++;
                            break;
                        
                        
                        case "Actual_Shift":
                            for ($counter = 1; $counter <= $monthDay; $counter++) {
                                $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                                $recordData         = (isset($Actual_Shift[$dateforDailyRecord]) && trim($Actual_Shift[$dateforDailyRecord]) != '') ? trim($Actual_Shift[$dateforDailyRecord]) : '';
                                if (trim($recordData) != '' && !empty($recordData)) {
                                    
                                    $recordDataArr = explode(" ", trim($recordData));
                                    $recordData1   = $recordDataArr[0];
                                    if (trim($recordData1) != '') {
                                        $recordDataVal = explode("@@@", $recordData);
                                        
                                        
                                        
                                        if (isset($recordDataVal[0]) && trim($recordDataVal[0]) == '') {
                                            $recordData1     = '';
                                            $statusForAbsent = '';
                                        } else {
                                            $statusForAbsent = $recordDataVal[1];
                                        }
                                        
                                        if (trim($recordDataVal[1]) == 'HLD' || trim($recordDataVal[1]) == 'WO' || trim($recordDataVal[1]) == 'A' || trim($recordDataVal[1]) == 'POH' || trim($recordDataVal[1]) == 'POW') {
                                            $recordData1 = '';
                                        }
                                        if (isset($recordDataVal[2]) && trim($recordDataVal[2]) == 'L') {
                                            $recordData1 = '';
                                        }
                                    }
                                } else {
                                    $recordData1 = '';
                                }
                                
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData1);
                                $countForHorizonatalExcel++;
                            }
                            $countForHorizonatalExcel = 0;
                            $counterForExcel++;
                            break;
                        
                        case 'Latemin':
                            for ($counter = 1; $counter <= $monthDay; $counter++) {
                                $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                                $recordData         = (isset($Latemin[$dateforDailyRecord]) && trim($Latemin[$dateforDailyRecord]) != '') ? trim($Latemin[$dateforDailyRecord]) : '0';
                                if (trim($recordData) != '' && !empty($recordData)) {
                                    $recordDataArr = explode("@@@", trim($recordData));
                                    $recordData1   = $recordDataArr[0];
                                    if (isset($recordDataArr[1]) && ($recordDataArr[1] == 'POH' || $recordDataArr[1] == 'POW' || $recordDataArr[1] == 'HLD' || $recordDataArr[1] == 'WO')) {
                                        $recordData1 = '0';
                                    }
                                } else {
                                    $recordData1 = '0';
                                }
                                
                                $recordData = (isset($Latemin[$dateforDailyRecord]) && trim($Latemin[$dateforDailyRecord]) != '') ? trim($recordData1) : '0';
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                                $countForHorizonatalExcel++;
                            }
                            $countForHorizonatalExcel = 0;
                            $counterForExcel++;
                            break;
                        
                        case 'Earlymin':
                            for ($counter = 1; $counter <= $monthDay; $counter++) {
                                $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                                $recordData         = (isset($Earlymin[$dateforDailyRecord]) && trim($Earlymin[$dateforDailyRecord]) != '') ? trim($Earlymin[$dateforDailyRecord]) : '0';
                                if (trim($recordData) != '' && !empty($recordData)) {
                                    $recordDataArr = explode("@@@", trim($recordData));
                                    $recordData1   = $recordDataArr[0];
                                    if (isset($recordDataArr[1]) && ($recordDataArr[1] == 'POH' || $recordDataArr[1] == 'POW' || $recordDataArr[1] == 'HLD' || $recordDataArr[1] == 'WO')) {
                                        $recordData1 = '0';
                                    }
                                } else {
                                    $recordData1 = '0';
                                }
                                $recordData = (isset($Earlymin[$dateforDailyRecord]) && trim($Earlymin[$dateforDailyRecord]) != '') ? trim($recordData1) : '0';
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                                $countForHorizonatalExcel++;
                            }
                            $countForHorizonatalExcel = 0;
                            $counterForExcel++;
                            break;
                        
                        case 'Overtime':
                            for ($counter = 1; $counter <= $monthDay; $counter++) {
                                $dateforDailyRecord = Date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $counter));
                                $recordData         = (isset($Overtime[$dateforDailyRecord]) && trim($Overtime[$dateforDailyRecord]) != '') ? trim($Overtime[$dateforDailyRecord]) : '0';
                                if (trim($recordData) != '' && !empty($recordData)) {
                                    $recordDataArr = explode("@@@", trim($recordData));
                                    $recordData1   = $recordDataArr[0];
                                    if (isset($recordDataArr[1]) && ($recordDataArr[1] == 'POH' || $recordDataArr[1] == 'POW' || $recordDataArr[1] == 'HLD' || $recordDataArr[1] == 'WO')) {
                                        $recordData1 = '0';
                                    }
                                } else {
                                    $recordData1 = '0';
                                }
                                $recordData = (isset($Overtime[$dateforDailyRecord]) && trim($Overtime[$dateforDailyRecord]) != '') ? trim($recordData1) : '0';
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $recordData);
                                $countForHorizonatalExcel++;
                            }
                            $countForHorizonatalExcel = 0;
                            $counterForExcel++;
                            break;
                    }
                    
                    $i++;
                    //if($i == '10000'){  break; }
                    
                }
                //if($i == '10000'){ echo $j; break; }	
            }
            
            $my_excel_filename = "upload/daily-status-monthly-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/daily-status-monthly-report.xls');
            
        } else if ($_POST['type'] == '6') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $HorizontalColumnName     = array(
                "Date",
                "Strength",
                "Present",
                "Absent",
                "Leave"
            );
            
            $valueForExcelStatus = array(
                'P' => " AND (statustitle='Present') AND final_status!='Leave' AND Leavestatus!='Approved' AND Leavestatus!='Approved Cancel - Request for cancel' OR final_status='POH' OR final_status='POW' AND final_status!='Weekly Off' AND final_status!='Holiday'",
                'A' => " AND final_status!='Weekly Off' AND final_status!='Holiday' AND statustitle='Absent' AND final_status!='POH' AND final_status!='POW' ",
                'L' => " AND final_status!='Weekly Off' AND final_status!='Holiday' AND Leavestatus='Approved' AND Leavestatus='Approved Cancel - Request for cancel' AND final_status!='POH' AND final_status!='POW'"
            );
            
            
            $getFields      = array(
                "Emp_Code"
            );
            $valueForStatus = array();
            $where          = '';
            $totalStrength  = $totalPresent = $totalAbsent = $totalLeave = 0;
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where .= " AND shiftId in ('" . $validShiftID . "')";
            }
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            
            if (!empty($advanceFilterConditions)) {
                $validEmpID = implode("','", $advanceFilterConditions);
                $validEmpID = str_replace(",", "','", $validEmpID);
                $where .= " AND Emp_Code in('" . $validEmpID . "')";
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            
            $toDatearr = explode("/", $_POST['toDate']);
            $toDate    = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Employee Strength for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '');
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E1')->setAutoSize(true);
            
            foreach ($HorizontalColumnName as $key => $value) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '', $value);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            
            $counterForExcel++;
            
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            
            for ($counter = 1; $counter <= $dateDifference; $counter++) {
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateforDailyRecord);
                $countForHorizonatalExcel++;
                $queryForGetTotalStrength = "SELECT count(status) as status FROM attendancesch WHERE date='" . $dateforDailyRecord . "' AND Schedule_Shift !='' AND final_status!='Weekly Off' AND final_status!='Holiday' " . $where . "";
                $resultForRecord          = query($query, $queryForGetTotalStrength, $pa, $opt, $ms_db);
                
                if ($num($resultForRecord) > 0) {
                    
                    $row1         = $fetch($resultForRecord);
                    $halfDay      = 0;
                    $halfDayData  = 0;
                    $halfDay1     = 0;
                    $halfDayData1 = 0;
                    
                    $queryForHalfDay        = "SELECT count(status) as status FROM attendancesch WHERE date='" . $dateforDailyRecord . "' AND Schedule_Shift !='' " . $where . " AND statustitle!='weeklyoff' AND statustitle='Half Day' AND final_status!='Holiday' AND final_status!='Weekly Off' AND final_status!='Leave'";
                    $resultForGetHalfRecord = query($query, $queryForHalfDay, $pa, $opt, $ms_db);
                    if ($num($resultForGetHalfRecord) > 0) {
                        while ($rowData = $fetch($resultForGetHalfRecord)) {
                            $halfDay = $rowData['status'];
                        }
                    }
                    if ($halfDay != 0) {
                        $halfDayData = $halfDay / 2;
                    }
                    
                    $queryForHalfDay1        = "SELECT count(status) as status FROM attendancesch WHERE date='" . $dateforDailyRecord . "' AND Schedule_Shift !='' " . $where . " AND statustitle!='weeklyoff' AND statustitle='Half Day' AND final_status!='Holiday' AND final_status!='Weekly Off' AND final_status='Leave' AND Leavestatus='Approved' AND Leavestatus='Approved Cancel - Request for cancel'";
                    $resultForGetHalfRecord1 = query($query, $queryForHalfDay1, $pa, $opt, $ms_db);
                    if ($num($resultForGetHalfRecord1) > 0) {
                        while ($rowData1 = $fetch($resultForGetHalfRecord1)) {
                            $halfDay1 = $rowData1['status'];
                        }
                    }
                    if ($halfDay1 != 0) {
                        $halfDayData1 = $halfDay1 / 2;
                    }
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row1['status']);
                    $countForHorizonatalExcel++;
                    $totalStrength = $totalStrength + $row1['status'];
                    foreach ($valueForExcelStatus as $key => $value) {
                        
                        $queryForGetDailyResult = "SELECT count(status) as status FROM attendancesch WHERE date='" . $dateforDailyRecord . "' AND Schedule_Shift !='' " . $where . " " . $value . "";
                        $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        if ($num($resultForGetRecord) > 0) {
                            while ($row = $fetch($resultForGetRecord)) {
                                if ($key == 'P') {
                                    /*if($dateforDailyRecord == '2017-04-21'){
                                    echo $queryForGetDailyResult; die;
                                    } */
                                    $valueForStatus[0] = $valueForStatus[0] + $row['status'] + $halfDayData + $halfDayData1;
                                    $totalData         = $row['status'] + $halfDayData + $halfDayData1;
                                } elseif ($key == 'A') {
                                    $valueForStatus[1] = $valueForStatus[1] + $row['status'] + $halfDayData;
                                    $totalData         = $row['status'] + $halfDayData;
                                    
                                } elseif ($key == 'L') {
                                    $valueForStatus[2] = $valueForStatus[2] + $row['status'] + $halfDayData1;
                                    $totalData         = $row['status'] + $halfDayData1;
                                    
                                }
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalData);
                                $countForHorizonatalExcel++;
                                
                            }
                            
                        } else {
                            
                            break;
                        }
                        
                    }
                }
                
                $dateforDailyRecord       = date('Y-m-d', strtotime(' +1 day', strtotime($dateforDailyRecord)));
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, "Total");
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
            $countForHorizonatalExcel++;
            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalStrength);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
            $countForHorizonatalExcel++;
            
            for ($i = 0; $i <= 2; $i++) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $valueForStatus[$i]);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
                $countForHorizonatalExcel++;
            }
            $my_excel_filename = "upload/daily-employee-strength-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/daily-employee-strength-report.xls');
            
        } else if ($_POST['type'] == '7') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $HorizontalColumnName     = array(
                "Shift",
                "Scheduled Shift",
                "Actual",
                "Shift Change",
                "Absent",
                "Leave"
            );
            $valueForExcelStatus      = array(
                'Absent' => " final_status!='Weekly Off' AND final_status!='Holiday' AND statustitle='Absent' AND final_status!='POH' AND final_status!='POW' ",
                'Leave' => " AND final_status!='Weekly Off' AND final_status!='Holiday' AND Leavestatus='Approved' AND Leavestatus='Approved Cancel - Request for cancel' AND final_status!='POH' AND final_status!='POW'"
            );
            $getFields                = array(
                "Emp_Code"
            );
            $where                    = '';
            $advanceFilterConditions  = $this->advanceSearchOptionForJson($getFields, '1');
            $this->getShiftName();
            
            if (!empty($advanceFilterConditions)) {
                $validEmpID = implode("','", $advanceFilterConditions);
                $where .= " AND Emp_code IN ('" . $validEmpID . "')";
            }
            
            if ($_POST['emp_code_list'] != 'HR') {
                
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                
                
                $where .= (!empty($_POST['emp_code_list'])) ? " AND Emp_code IN (" . $empArr . ") " : "";
            }
            // echo $where; die("fdfdfd");
            
            
            if (!empty($_POST['shift'])) {
                foreach ($this->getShiftName as $key1 => $value1) {
                    if (!in_array($key1, $_POST['shift'])) {
                        unset($this->getShiftName[$key1]);
                    }
                }
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            $toDatearr   = explode("/", $_POST['toDate']);
            $toDate      = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $headingInfo = 'Employee Strength for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
            
            foreach ($this->getShiftName as $key => $value) {
                if (!empty($value) && trim($value) != '') {
                    $shiftNameArr = explode(" ", $value);
                    $shiftName    = $shiftNameArr[0];
                } else {
                    $shiftName = $value;
                }
                
                $rosterChangeShift = 0;
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftName);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "select COUNT(Id) as id from attendancesch where date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Schedule_Shift='" . $value . "' AND final_status!='Weekly Off' AND final_status!='POH' AND final_status!='POW' AND final_status!='Holiday'  AND status!='F'  " . $where . "";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $scheduledShift         = (isset($row['id']) && $row['id'] != '') ? $row['id'] : '0';
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $scheduledShift);
                $countForHorizonatalExcel++;
                
                $queryForGetDailyResult = "select count(Id) as actual from attendancesch where date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Actual_Shift='" . $value . "' AND final_status!='Weekly Off' AND final_status!='POH' AND final_status!='POW' AND final_status!='Holiday'  AND final_status!='F'  " . $where . "";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $actualShift            = (isset($row['actual']) && $row['actual'] != '') ? $row['actual'] : '0';
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                $countForHorizonatalExcel++;
                
                $queryForGetResult   = "select count(Id) as rosterChange from attendancesch where date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Actual_Shift!=Schedule_Shift AND Schedule_Shift!= '' AND final_status!='Weekly Off' AND final_status!='POH' AND final_status!='POW' AND final_status!='Holiday'  AND final_status!='F'  " . $where . "";
                $resultForGetRecords = query($query, $queryForGetResult, $pa, $opt, $ms_db);
                $row                 = $fetch($resultForGetRecords);
                $rosterChangeShift   = (isset($row['rosterChange']) && $row['rosterChange'] != '') ? $row['rosterChange'] : '0';
                
                
                $queryForGetDailyResult = "SELECT COUNT(ID) as shiftChange FROM rost_change WHERE RosterDate>='" . $dateforDailyRecord . "' AND RosterDate<='" . $toDate . "' AND type_name='approve' AND shiftMaster='" . $key . "' " . $where . "";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $shiftChange            = (isset($row['shiftChange']) && $row['shiftChange'] != '') ? $row['shiftChange'] : '0';
                $shiftChange            = $rosterChangeShift + $shiftChange;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftChange);
                $countForHorizonatalExcel++;
                
                foreach ($valueForExcelStatus as $key1 => $value1) {
                    
                    $queryForGetDailyResult = "SELECT count(status) as status FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Schedule_Shift='" . $value . "' AND " . $value1 . " " . $where . "";
                    $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                    $row                    = $fetch($resultForGetRecord);
                    $status                 = (isset($row['status']) && $row['status'] != '') ? $row['status'] : '0';
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $status);
                    $countForHorizonatalExcel++;
                    
                }
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
                
            }
            
            $my_excel_filename = "upload/shift-change-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/shift-change-report.xls');
            
        } else if ($_POST['type'] == '8') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $this->getShiftName();
            $HorizontalColumnName = array(
                "Employee Code",
                "Employee Name",
                "Department",
                "Category",
                "Working Days",
                "Present",
                "Leave",
                "Holiday/Weekly-Off"
            );
            $getFields            = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME"
            );
            $where                = '';
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            $resultStatus['error']   = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            $toDatearr   = explode("/", $_POST['toDate']);
            $toDate      = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $headingInfo = 'Employee Strength for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            
            if (!empty($_POST['shift'])) {
                foreach ($this->getShiftName as $key1 => $value1) {
                    if (!in_array($key1, $_POST['shift'])) {
                        unset($this->getShiftName[$key1]);
                    }
                }
            }
            foreach ($this->getShiftName as $key => $value) {
                $shiftArr               = explode(" ", $value);
                $HorizontalColumnName[] = $shiftArr[0];
            }
            
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
            // print_r($advanceFilterConditions); die;
            foreach ($advanceFilterConditions as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                
                $halfDay                = $present = $holiday = $woff = $leave = $absent = '0';
                $queryForGetDailyResult = "SELECT status, COUNT(*) as statusCount FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' GROUP BY status";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        
                        if ($row['status'] == 'P')
                            $present = $row['statusCount'];
                        if ($row['status'] == 'H')
                            $halfDay = $row['statusCount'];
                        if ($row['status'] == 'W')
                            $woff = $row['statusCount'];
                        if ($row['status'] == 'F')
                            $holiDay = $row['statusCount'];
                        if ($row['status'] == 'L')
                            $leave = $row['statusCount'];
                        if ($row['status'] == 'A')
                            $absent = $row['statusCount'];
                    }
                } else {
                    $holiDay = $halfDay = $present = $woff = $leave = $absent = '0';
                }
                $totalPresent = $halfDay + $present;
                $totalHoliDay = $woff + $holiDay;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $leave);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalHoliDay);
                $countForHorizonatalExcel++;
                $shiftCount = array();
                
                $queryForGetDailyResult = "SELECT Schedule_Shift, COUNT(*) as shiftCount FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' AND Schedule_Shift != '' GROUP BY Schedule_Shift order by Schedule_Shift";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        if (in_array($row['Schedule_Shift'], $this->getShiftName)) {
                            $shiftCount[$row['Schedule_Shift']] = $row['shiftCount'];
                        }
                    }
                } else {
                    foreach ($this->getShiftName as $key4 => $value4) {
                        $shiftCount[$key4] = '0';
                    }
                }
                // print_r($shiftCount); die;
                foreach ($this->getShiftName as $key4 => $value4) {
                    if (isset($shiftCount[$value4])) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftCount[$value4]);
                        $countForHorizonatalExcel++;
                    } else {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '0');
                        $countForHorizonatalExcel++;
                    }
                }
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/shift-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/shift-report.xls');
            
            
        } else if ($_POST['type'] == '9') {
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            
            $HorizontalColumnName = array(
                "Employee Code",
                "Employee Name",
                "Department",
                "Category",
                "Manager",
                "Location",
                "Present",
                "Leave",
                "Miss-Punch",
                "Hlf Days",
                "Absent",
                "Holiday/Weekly-Off",
                "Working Days",
                "Early",
                "Late Arrival",
                "Shift Change",
                "OD"
            );
            $getFields            = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME"
            );
            $where                = '';
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            $fromDatearr             = explode("/", $_POST['fromDate']);
            $dateforDailyRecord      = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            $toDatearr   = explode("/", $_POST['toDate']);
            $toDate      = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            $headingInfo = 'Employee Summary for ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $dateDifference           = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference           = $dateDifference + 1;
            $countForHorizonatalExcel = 0;
            $counterForExcel++;
            
            //echo '<pre>'; print_r($advanceFilterConditions); die;
            foreach ($advanceFilterConditions as $key => $value) {
                $odType = $Early_count = $Late_count = $present = $absent = $Leave = $halfDay = $woff = $holiday = 0;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $queryForGetDailyResult = "SELECT Leavestatus,odType,Emp_code,final_status,LeaveTypeText,odStatus,markPastStatus,statustitle,Lateflag, Earlyflag,date,status,Schedule_Shift,Actual_Shift,Earlymin,Latemin,Overstay,Workinghrs,Intime,Outtime FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' " . $where . "";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $finalStatus = $this->getDayFinalStatus($row);
                        
                        if ($finalStatus == 'P' || $finalStatus == 'C-P' || $finalStatus == 'POH' || $finalStatus == 'POW' || $finalStatus == 'OD') {
                            $present++;
                        } else if ($finalStatus == 'WO' || $finalStatus == 'HLD') {
                            $woff++;
                        } else if ($finalStatus == 'A' || $finalStatus == 'MIS') {
                            $absent++;
                        } else if ($finalStatus == 'H') {
                            $present = $present + 0.5;
                            $absent  = $absent + 0.5;
                        } else {
                            $leaveStatus = $this->checkLeave($row);
                            if (trim($leaveStatus) != '' && $leaveStatus == 'L') {
                                $Leave++;
                            } else if (trim($leaveStatus) != '' && $leaveStatus == 'H') {
                                $Leave   = $Leave + 0.5;
                                $present = $present + 0.5;
                            } else if (trim($leaveStatus) != '' && $leaveStatus == 'PH') {
                                $present = $present + 0.5;
                                $Leave   = $Leave + 0.5;
                            }
                        }
                        if (trim($row['Lateflag']) != '' && $row['Lateflag'] != '0') {
                            $Late_count++;
                        }
                        if (trim($row['Earlyflag']) != '' && $row['Earlyflag'] != '0') {
                            $Early_count++;
                        }
                        if (trim($row['odStatus']) != '' && $row['odStatus'] == 'Approved') {
                            $odType++;
                        }
                    }
                }
                
                // $totalPresent = $halfDay + $present;
                // $totalHoliDay = $woff + $holiDay;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Leave);
                $countForHorizonatalExcel++;
                $queryForGetDailyResult = "SELECT COUNT(Id) as totalMissPunch FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' AND Schedule_Shift!='' AND ((Intime = '' AND Outtime != '') OR (Intime != '' AND Outtime = ''))";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $totalMissPunch = $row['totalMissPunch'];
                    }
                } else {
                    $totalMissPunch = '0';
                }
                $queryForGetshiftChange  = "SELECT COUNT(Id) as shiftChange FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND Emp_Code='" . $key . "' AND Schedule_Shift!='' AND Schedule_Shift!=Actual_Shift";
                $resultForShiftGetRecord = query($query, $queryForGetshiftChange, $pa, $opt, $ms_db);
                if ($num($resultForShiftGetRecord) > 0) {
                    while ($row23 = $fetch($resultForShiftGetRecord)) {
                        $totalShift = $row23['shiftChange'];
                    }
                } else {
                    $totalShift = '0';
                }
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalMissPunch);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $halfDay);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $woff);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Early_count);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Late_count);
                $countForHorizonatalExcel++;
                $queryForGetDailyResult = "SELECT COUNT(ID) as shiftChange FROM rost_change WHERE RosterDate>='" . $dateforDailyRecord . "' AND RosterDate<='" . $toDate . "' AND type_name='approve' AND shiftMaster='" . $key . "'";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                    = $fetch($resultForGetRecord);
                $shiftChange            = (isset($row['shiftChange']) && $row['shiftChange'] != '') ? $row['shiftChange'] : '0';
                $shiftChange            = $totalShift + $shiftChange;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftChange);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $odType);
                $countForHorizonatalExcel++;
                
                
                /*$total		=	0;
                $dateforDailyRecord1	=	$dateforDailyRecord;
                for($counter =1;$counter<=$dateDifference;$counter++){
                $queryForGetDailyResult			=	"select COUNT(outWorkId) as id from outOnWorkRequest where CreatedBy='".$key."' AND action_status IN ('2','5') AND '".$dateforDailyRecord1."' BETWEEN date_from AND date_to";
                $resultForGetRecord 			=	query($query,$queryForGetDailyResult,$pa,$opt,$ms_db);
                if($num($resultForGetRecord) > 0){
                while ($row = $fetch($resultForGetRecord)){
                $total 		=	$total+$row['id'];
                }
                }else{
                $total 		=	$total+0;
                }
                $dateforDailyRecord1			=	date('Y-m-d', strtotime(' +1 day', strtotime($dateforDailyRecord1)));
                }	
                $objPHPExcel->getActiveSheet()->SetCellValue(''.$columnName[$countForHorizonatalExcel].$counterForExcel,$total);
                $countForHorizonatalExcel++;*/
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
                
            }
            
            $my_excel_filename = "upload/employee-summary-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/employee-summary-report.xls');
            
        } else if ($_POST['type'] == '10') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            if (!isset($_POST['monthFor']) || !isset($_POST['year']) || $_POST['monthFor'] == '') {
                $resultStatus['error'] = "Error";
                json_encode($resultStatus);
                die;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            
            if ($_POST['monthFor'] != '' && $_POST['year'] != '') {
                $monthDay  = date('t', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $monthName = date('F', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $firstDate = date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
            }
            
            for ($counter = 1; $counter <= $monthDay; $counter++) {
                $HorizontalColumnName[] = $counter;
            }
            
            foreach ($this->getShiftName() as $key => $value) {
                $shiftArr               = explode(" ", $value);
                $HorizontalColumnName[] = $shiftArr[0];
            }
            
            
            $HorizontalColumnName[] = 'Shift Change';
            $HorizontalColumnName[] = 'Unauthorized Change';
            $HorizontalColumnName[] = 'Authorized change';
            
            $headingInfo              = 'Monthly Shift Wise Performance for ' . $monthName . ' ' . $_POST['year'] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            
            foreach ($advanceFilterConditions as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $dateArray   = array();
                $actualShift = array();
                $firstDate1  = $firstDate;
                $shiftArray  = array();
                
                $queryForGetDailyResult1 = "SELECT Leavestatus,Emp_code,final_status,LeaveTypeText,odStatus,markPastStatus,statustitle,Lateflag, Earlyflag,date,status,Schedule_Shift,Actual_Shift,Earlymin,Latemin,Overstay,Intime,Outtime FROM attendancesch WHERE MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' AND [status]!='' " . $where . " AND Emp_code='" . $key . "' order by date";
                $resultForGetRecord1     = query($query, $queryForGetDailyResult1, $pa, $opt, $ms_db);
                
                if ($num($resultForGetRecord1) > 0) {
                    while ($row = $fetch($resultForGetRecord1)) {
                        $leaveType = $this->checkLeave($row);
                        if (trim($leaveType) != '') {
                            $dateArray[$row['date']] = '';
                        } else if (($row['status'] == 'F') || ($row['final_status'] == 'Weekly Off') || ($row['final_status'] == 'POH') || ($row['final_status'] == 'POW') || ($row['status'] == 'W') || ($row['final_status'] == 'Holiday')) {
                            $dateArray[$row['date']] = 'OFF';
                        } else {
                            
                            if (!empty($row['Schedule_Shift']) && trim($row['Schedule_Shift']) != '') {
                                $scheduledShiftArr       = explode(" ", $row['Schedule_Shift']);
                                $dateArray[$row['date']] = $scheduledShiftArr['0'];
                                if (trim($row['Actual_Shift']) == trim($row['Schedule_Shift'])) {
                                    $shiftArray[$row['Schedule_Shift']][] = '-1';
                                }
                            }
                            if (trim($row['Actual_Shift']) != trim($row['Schedule_Shift'])) {
                                if (!empty($row['Actual_Shift']) && trim($row['Actual_Shift']) != '') {
                                    $actualShiftArr                     = explode(" ", $row['Actual_Shift']);
                                    $dateArray[$row['date']]            = $actualShiftArr['0'];
                                    $actualShift[$row['date']]          = $row['Actual_Shift'];
                                    $shiftArray[$actualShiftArr['0']][] = '-1';
                                }
                            }
                            
                        }
                        
                        
                    }
                } else {
                    $dateArray = array();
                }
                
                if (!empty($dateArray)) {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        if (isset($dateArray[$firstDate1])) {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateArray[$firstDate1]);
                            $countForHorizonatalExcel++;
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                            $countForHorizonatalExcel++;
                        }
                        $firstDate1 = date('Y-m-d', strtotime(' +1 day', strtotime($firstDate1)));
                    }
                } else {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                        $countForHorizonatalExcel++;
                    }
                }
                
                $shiftCount = array();
                
                foreach ($this->getShiftName as $key4 => $value4) {
                    if (isset($shiftArray[$value4])) {
                        $totalValue        = count($shiftArray[$value4]);
                        $totalValue        = $totalValue--;
                        $shiftCount[$key4] = $totalValue;
                    } else {
                        $shiftCount[$key4] = '0';
                    }
                }
                
                foreach ($this->getShiftName as $key4 => $value4) {
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftCount[$key4]);
                    $countForHorizonatalExcel++;
                }
                
                $unAuthorigeShift = 0;
                $AuthorigeShift   = 0;
                if (!empty($actualShift)) {
                    $totalChangedShift = count($actualShift);
                } else {
                    $totalChangedShift = 0;
                }
                $unAuthorigeShift = $totalChangedShift;
                
                if (!empty($actualShift)) {
                    
                    $queryForGetDailyResult = "SELECT RosterDate,shiftMaster,type_name FROM rost_change WHERE MONTH(RosterDate)='" . $_POST['monthFor'] . "' AND YEAR(RosterDate)='" . $_POST['year'] . "' AND Emp_code='" . $key . "' AND type_name='approve' ";
                    $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                    if ($num($resultForGetRecord) > 0) {
                        while ($row = $fetch($resultForGetRecord)) {
                            $totalChangedShift++;
                            if ($row['type_name'] == 'approve') {
                                $AuthorigeShift = $AuthorigeShift + 1;
                            } else {
                                $unAuthorigeShift = $unAuthorigeShift + 1;
                            }
                            
                        }
                    }
                    
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalChangedShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $unAuthorigeShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $AuthorigeShift);
                    $countForHorizonatalExcel++;
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalChangedShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $unAuthorigeShift);
                    $countForHorizonatalExcel++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $AuthorigeShift);
                    $countForHorizonatalExcel++;
                }
                $i++;
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
            }
            
            $my_excel_filename = "upload/monthly-shift-wise-performance.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/monthly-shift-wise-performance.xls');
            
        } else if ($_POST['type'] == '11') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Date",
                "IN",
                "OUT",
                "Regularize",
                "Not Regularize",
                "Rejected",
                "Regularize after days",
                "Approve"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where .= " AND shiftId in ('" . $validShiftID . "')";
            }
            
            $resultStatus['error'] = $this->checkDat($_POST['daily'], '', '2');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            if (isset($_POST['daily']) && trim($_POST['daily']) != '') {
                $dailyDate                = explode("/", $_POST['daily']);
                $dateForResultCalculation = $dailyDate[2] . "-" . $dailyDate[1] . "-" . $dailyDate[0];
            } else {
                $resultStatus['error'] = "Error";
                return json_encode($resultStatus);
                die;
            }
            
            $headingInfo              = 'Daily Miss Punch Report ' . $dailyDate[0] . "-" . $dailyDate[1] . "-" . $dailyDate[2];
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            
            $dateArray = array();
            
            if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] != 'HR') {
                
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = array_filter($empArr);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                $where .= (!empty($_POST['emp_code_list'])) ? " AND attendancesch.Emp_code in (" . $empArr . ") " : "";
                
            }
            
            $queryForGetDailyResult = "SELECT date,Intime,Outtime,Emp_code FROM attendancesch WHERE (((Intime='1900-01-01 00:00:00.000' AND Outtime!='') OR (Intime!='' AND Outtime='1900-01-01 00:00:00.000')) OR (mark_past_id!='0')) AND date='" . $dateForResultCalculation . "' " . $where . "";
            
            $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            if ($num($resultForGetRecord) > 0) {
                while ($row = $fetch($resultForGetRecord)) {
                    $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '4', $row['Emp_code']);
                    foreach ($advanceFilterConditions as $key => $value) {
                        if (strtotime($row['Intime']) > 0) {
                            $checkTime++;
                        }
                        if (strtotime($row['Outtime']) > 0) {
                            $checkTime++;
                        }
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                        $countForHorizonatalExcel++;
                        
                        
                        if (strtotime($row['Intime']) > 0)
                            $inTime = Date('d-m-Y h:i A', strtotime($row['Intime']));
                        else
                            $inTime = '';
                        
                        if (strtotime($row['Outtime']) > 0)
                            $Outtime = Date('d-m-Y h:i A', strtotime($row['Outtime']));
                        else
                            $Outtime = '';
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dailyDate[0] . "-" . $dailyDate[1] . "-" . $dailyDate[2]);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $inTime);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Outtime);
                        $countForHorizonatalExcel++;
                        $regularize      = $notRegularige = $rejected = $approve = '0';
                        $dateAverageDiff = $counteOfDate = $dateDifference1 = '0';
                        
                        $queryForGetDailyResult1 = "SELECT markPastId,action_status,CreatedOn FROM markPastAttendance WHERE CreatedBy='" . $key . "' AND '" . $row['date'] . "' between date_from AND date_to AND action_status NOT IN ('1','4')";
                        $resultForGetRecord1     = query($query, $queryForGetDailyResult1, $pa, $opt, $ms_db);
                        if ($num($resultForGetRecord1) > 0) {
                            $regularize++;
                            while ($row1 = $fetch($resultForGetRecord1)) {
                                if ($row1['CreatedOn'] != '') {
                                    $dateDifference1 = $this->dayDifferencesBetweenDate($row['date'], $row1['CreatedOn']);
                                    $dateDifference1 = $dateDifference1 + 1;
                                }
                                if ($row1['action_status'] == '2' || $row1['action_status'] == '5' || $row1['action_status'] == '7') {
                                    $approve++;
                                } elseif ($row1['action_status'] == '3' || $row1['action_status'] == '6') {
                                    $rejected++;
                                }
                            }
                        } else {
                            $notRegularige++;
                        }
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $regularize);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $notRegularige);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $rejected);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference1);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $approve);
                        $countForHorizonatalExcel++;
                        $i++;
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    }
                }
            }
            
            
            
            
            
            $my_excel_filename = "upload/mis-punch-daily-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/mis-punch-daily-report.xls');
            
        } else if ($_POST['type'] == '12') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Day",
                "Regularize",
                "Not Regularize",
                "Rejected",
                "Regularize after days",
                "Approve"
            );
            $columnName               = array(
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            $resultStatus['error']    = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = " AND shiftId in ('" . $validShiftID . "')";
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $toDatearr          = explode("/", $_POST['toDate']);
            $toDate             = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            
            
            $headingInfo              = 'Monthly Miss Punch Report For ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            $dateDifference = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference = $dateDifference + 1;
            $counter        = 0;
            $dateArray      = array();
            $empCodeArr     = array();
            
            if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] != 'HR') {
                
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = array_filter($empArr);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                $where .= (!empty($_POST['emp_code_list'])) ? " AND attendancesch.Emp_code in (" . $empArr . ") " : "";
                
            }
            
            $queryForGetDailyResult = "SELECT date,Emp_code FROM attendancesch WHERE (((Intime='1900-01-01 00:00:00.000' AND Outtime!='') OR (Intime!='' AND Outtime='1900-01-01 00:00:00.000')) OR (mark_past_id!='0')) AND (date >= '" . $dateforDailyRecord . "' AND date<='" . $toDate . "') " . $where . " order by date";
            $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            if ($num($resultForGetRecord) > 0) {
                while ($row1 = $fetch($resultForGetRecord)) {
                    $counter++;
                    $dateArray[]  = $row1['date'] . '--' . $row1['Emp_code'];
                    $empCodeArr[] = $row1['Emp_code'];
                }
            } else {
                $counter = 0;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '5', $empCode, $empCodeArr);
            
            //echo '<pre>'; print_r($advanceFilterConditions); echo '</pre>'; die;
            if (!empty($dateArray)) {
                foreach ($dateArray as $key2 => $value2) {
                    $arr     = explode("--", $value2);
                    $date    = $arr[0];
                    $empCode = $arr[1];
                    
                    //  $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '4', $empCode);
                    if (isset($advanceFilterConditions[$empCode])) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $empCode);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $advanceFilterConditions[$empCode]['EMP_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $advanceFilterConditions[$empCode]['FUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $advanceFilterConditions[$empCode]['SUBFUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $advanceFilterConditions[$empCode]['COST_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $advanceFilterConditions[$empCode]['GRD_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $advanceFilterConditions[$empCode]['LOC_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $advanceFilterConditions[$empCode]['MNGR_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('d-M-Y', strtotime($date)));
                        $countForHorizonatalExcel++;
                        
                        /*
                        status	1	Pending
                        status	2	Approved
                        status	3	Rejected
                        status	4	Cancel
                        status	5	Approved Cancel - Request for cancel
                        status	6	Approved Cancellation
                        status	7	Approved Cancel Rejected
                        */
                        $regularize      = $notRegularige = $rejected = $approve = 0;
                        $dateAverageDiff = $counteOfDate = $dateDifference1 = '0';
                        $row             = array();
                        $row             = $this->markPastAttandanceData($empCode, $date);
                        //	$queryForGetDailyResult = "SELECT markPastId,action_status,CreatedOn FROM markPastAttendance WHERE CreatedBy='" . $empCode . "' AND '" . $date . "' between date_from AND date_to AND action_status NOT IN ('1','4')";
                        //   $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        if (!empty($row)) {
                            if ($row['action_status'] != '1' && $row['action_status'] != '4') {
                                $regularize++;
                                if ($row['CreatedOn'] != '') {
                                    $dateDifference1  = $this->dayDifferencesBetweenDate($date, $row['CreatedOn']);
                                    $dateDifference1  = $dateDifference1 + 1;
                                    $checkDiffbetDate = 0;
                                }
                                $counteOfDate++;
                                
                                if ($row['action_status'] == '2' || $row['action_status'] == '5' || $row['action_status'] == '7') {
                                    $approve++;
                                } elseif ($row['action_status'] == '3' || $row['action_status'] == '6') {
                                    $rejected++;
                                }
                            } else {
                                $notRegularige++;
                            }
                            
                        } else {
                            $notRegularige++;
                        }
                        
                        if (isset($checkDiffbetDate) && $counteOfDate != '0' && $dateDifference1 != '0')
                            $dateAverageDiff = $dateDifference1 / $counteOfDate;
                        else
                            $dateAverageDiff;
                        
                        if ($dateAverageDiff == 'FALSE')
                            $dateAverageDiff = '0';
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $regularize);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $notRegularige);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $rejected);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateAverageDiff);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $approve);
                        $countForHorizonatalExcel++;
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                        $i++;
                    }
                }
            }
            $my_excel_filename = "upload/mis-punch-Monthly-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/mis-punch-Monthly-report.xls');
            
        } else if ($_POST['type'] == '13') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            $advanceFilterConditions  = $this->advanceSearchOptionForJson($getFields, '2');
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            if (trim($_POST['monthFor']) != '' && trim($_POST['year']) != '0') {
                $monthDay      = date('t', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $monthName     = date('F', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $firstDate     = date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $dateForQuery  = date('d-M-Y', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $dateForQuery1 = date('d-M-Y', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-' . $monthDay));
            } else {
                echo $error = "Somthing Missing";
                exit;
            }
            
            for ($counter = 1; $counter <= $monthDay; $counter++) {
                $HorizontalColumnName[] = $counter;
            }
            $HorizontalColumnName[] = 'Present Days';
            $HorizontalColumnName[] = 'Weekly Off';
            $HorizontalColumnName[] = 'Holidays';
            $HorizontalColumnName[] = 'Days Worked';
            foreach ($this->LeaveListType() as $key => $value) {
                $HorizontalColumnName[] = $value;
            }
            $HorizontalColumnName[] = ' LWP Days';
            $HorizontalColumnName[] = 'Total Leave';
            $HorizontalColumnName[] = 'Days Absent';
            $HorizontalColumnName[] = 'Total Present Days';
            $HorizontalColumnName[] = 'Un authorized Shift Change';
            $HorizontalColumnName[] = ' Less hrs worked';
            $HorizontalColumnName[] = 'Over Stay';
            $HorizontalColumnName[] = 'Total Hrs worked';
            
            $headingInfo              = 'Monthly Employee Performancefor ' . $monthName . ' ' . $_POST['year'] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            
            
            foreach ($advanceFilterConditions as $key => $value) {
                $this->total_late_count = '0';
                $present                = $Absent = $Leave = $halfDay = $woff = $holiday = '0';
                
                $dateDifference = $this->dayDifferencesBetweenDate($dateForQuery, $dateForQuery1);
                $dateDifference = $dateDifference + 1;
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                
                $dateArray  = array();
                $firstDate1 = $firstDate;
                $Workinghrs = $Earlymin = $Latemin = $Overstay = $actualShift = '0';
                
                $queryForGetDailyResult = "SELECT Leavestatus,Emp_code,final_status,LeaveTypeText,odStatus,markPastStatus,statustitle,Lateflag, Earlyflag,date,status,Schedule_Shift,Actual_Shift,Earlymin,Latemin,Overstay,Workinghrs,Intime,Outtime FROM attendancesch WHERE MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' " . $where . " AND Emp_code='" . $key . "' order by date";
                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $finalStatus = $this->getDayFinalStatus($row);
                        if ($finalStatus == 'P' || $finalStatus == 'C-P' || $finalStatus == 'POH' || $finalStatus == 'POW') {
                            $present++;
                        } else if ($finalStatus == 'WO') {
                            $woff++;
                        } else if ($finalStatus == 'HLD') {
                            $holiday++;
                        } else if ($finalStatus == 'A' || $finalStatus == 'MIS') {
                            $Absent++;
                        } else if ($finalStatus == 'H') {
                            $halfDay++;
                        } else {
                            $leaveStatus = $this->checkLeave($row);
                            if (trim($leaveStatus) != '' && $leaveStatus == 'L') {
                                $Leave++;
                            } else if (trim($leaveStatus) != '' && $leaveStatus == 'H') {
                                $Leave = $Leave + 0.5;
                            } else if (trim($leaveStatus) != '' && $leaveStatus == 'PH') {
                                $present = $present + 0.5;
                                $Leave   = $Leave + 0.5;
                            }
                        }
                        
                        $dateArray[$row['date']] = $finalStatus;
                        if (trim($row['Overstay']) != '')
                            $Overstay = $this->addTimeCalculation($Overstay, $row['Overstay']);
                        if (trim($row['Workinghrs']) != '')
                            $Workinghrs = $this->addTimeCalculation($Workinghrs, $row['Workinghrs']);
                        if (trim($row['Latemin']) != '')
                            $Latemin = $this->addTimeCalculation($Latemin, $row['Latemin']);
                        if (trim($row['Earlymin']) != '')
                            $Earlymin = $this->addTimeCalculation($Earlymin, $row['Earlymin']);
                        if (trim($row['Actual_Shift']) != trim($row['Schedule_Shift']))
                            $actualShift++;
                    }
                } else {
                    $dateArray = array();
                }
                
                if (!empty($dateArray)) {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        if (isset($dateArray[$firstDate1])) {
                            $data_val               = $dateArray[$firstDate1];
                            $dateArray[$firstDate1] = (strpos($data_val, '~~1') > 0) ? str_replace('~~1', '', $data_val) : $data_val;
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateArray[$firstDate1]);
                            $styleArray = array(
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array(
                                        'rgb' => 'FF0000'
                                    )
                                )
                            );
                            if (strpos($data_val, '~~1') > 0 && (($getRecordStatus[$dateforDailyRecord] != 'HLD') && ($getRecordStatus[$dateforDailyRecord] != 'WO'))) {
                                // $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . $counterForExcel)->applyFromArray($styleArray);
                            }
                            //$objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateArray[$firstDate1]);
                            $countForHorizonatalExcel++;
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '-');
                            $countForHorizonatalExcel++;
                        }
                        $firstDate1 = date('Y-m-d', strtotime(' +1 day', strtotime($firstDate1)));
                    }
                } else {
                    for ($counter = 1; $counter <= $monthDay; $counter++) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '-');
                        $countForHorizonatalExcel++;
                    }
                }
                
                /*$queryForGetDailyResult = "SELECT SUM(CASE WHEN status ='P' THEN 1 ELSE 0 END) AS present
                ,SUM(CASE WHEN status ='A' THEN 1 ELSE 0 END) AS Absent
                ,SUM(CASE WHEN status ='L' THEN 1 ELSE 0 END) AS Leave
                ,SUM(CASE WHEN status ='H' THEN 1 ELSE 0 END) AS halfDay
                ,SUM(CASE WHEN status ='W' THEN 1 ELSE 0 END) AS woff
                ,SUM(CASE WHEN status ='F' THEN 1 ELSE 0 END) AS holiday
                FROM attendancesch WHERE MONTH(date)='" . $_POST['monthFor'] . "' AND YEAR(date)='" . $_POST['year'] . "' " . $where . " AND Emp_Code='" . $key . "'";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                $row                = $fetch($resultForGetRecord);
                $present            = (isset($row['present'])) ? $row['present'] : '0';
                $Absent             = (isset($row['Absent'])) ? $row['Absent'] : '0';
                $Leave              = (isset($row['Leave'])) ? $row['Leave'] : '0';
                $halfDay            = (isset($row['halfDay'])) ? $row['halfDay'] : '0';
                $woff               = (isset($row['woff'])) ? $row['woff'] : '0';
                $holiday            = (isset($row['holiday'])) ? $row['holiday'] : '0'; */
                $halfDayCalculation                = $halfDay / 2;
                $present1                          = $present + $halfDayCalculation;
                $totalCalulationOfWeeklyAndHoliday = $woff + $holiday;
                $dateDifference                    = $dateDifference - $totalCalulationOfWeeklyAndHoliday;
                $Absent                            = $Absent + $halfDayCalculation; // This is for When Somone will take halfDay then other hlafDAy part will be Present Part.
                
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $woff);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $holiday);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateDifference);
                $countForHorizonatalExcel++;
                $totalLeave             = 0;
                $queryForGetDailyResult = "select COUNT(*) as leave,LvType  from leave,master..spt_values 
																	where CreatedBy='" . $key . "' AND type = 'P' AND DATEADD(DAY,number,LvFrom) <= LVTO
																	AND DATEADD(DAY,number,LvFrom) IN (select DATEADD(DAY,number,'" . $dateForQuery . "') [LDate] FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number,'" . $dateForQuery . "') <= '" . $dateForQuery1 . "') GROUP BY LvType HAVING ( COUNT(leaveID) > 1 )";
                
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                if ($num($resultForGetRecord) > 0) {
                    foreach ($this->getLeaveType as $key5 => $value5) {
                        $countOFLeave = '-a';
                        while ($row = $fetch($resultForGetRecord)) {
                            if ($key5 == $row['LvType'])
                                $countOFLeave = $row['leave'];
                            break;
                        }
                        if ($countOFLeave == '-a') {
                            $totalLeave = $totalLeave + 0;
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '0');
                            $countForHorizonatalExcel++;
                        } else {
                            $totalLeave = $totalLeave + $countOFLeave;
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $countOFLeave);
                            $countForHorizonatalExcel++;
                        }
                    }
                } else {
                    foreach ($this->getLeaveType as $key5 => $value5) {
                        $totalLeave = $totalLeave + 0;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '0');
                        $countForHorizonatalExcel++;
                    }
                }
                $totalLessTime = $this->addTimeCalculation($Earlymin, $Latemin);
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalLeave);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Absent);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $present1);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalLessTime);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Overstay);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Workinghrs);
                $countForHorizonatalExcel++;
                
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
                $i++;
            }
            $my_excel_filename = "upload/monthly-employee-performance.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/monthly-employee-performance.xls');
            
        } else if ($_POST['type'] == '14') {
            
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME",
                "Status_Code",
                "DOJ"
            );
            $where                    = '';
            $advanceFilterConditions  = $this->advanceSearchOptionForJson($getFields, '2');
            
            // echo '<pre>'; print_r($advanceFilterConditions); echo '</pre>'; die;
            
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die("Date Format Invalid.");
            }
            
            $fromDatearr         = explode("/", $_POST['fromDate']);
            $dateforDailyRecord  = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $dateforDailyRecord1 = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            
            $toDatearr = explode("/", $_POST['toDate']);
            $toDate    = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            
            $firstDate1     = date('d M', strtotime($dateforDailyRecord1));
            $dateDifference = $this->dayDifferencesBetweenDate($dateforDailyRecord, $toDate);
            $dateDifference = $dateDifference + 1;
            
            for ($counter = 1; $counter <= $dateDifference; $counter++) {
                $HorizontalColumnName[] = $firstDate1;
                $firstDate1             = date('d M', strtotime(' +1 day', strtotime($firstDate1)));
            }
            
            $headingInfo              = 'Monthly Attendance Summary For ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            $dateforDailyRecord1 = Date('Y-m-d', strtotime($dateforDailyRecord1));
            $dateforBackDated    = Date('Y-m-d', strtotime('2017-04-12'));
            //echo strtotime($dateforDailyRecord1)."  ".strtotime($dateforBackDated);
            
            foreach ($advanceFilterConditions as $key => $value) {
                
                $dateforDailyRecord2 = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                $countForHorizonatalExcel++;
                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                $countForHorizonatalExcel++;
                $total_late_allow = 2;
                $total_late_count = 0;
                $dateArray        = array();
                
                //$setDateLimit =   30;
                
                if (strtotime($dateforDailyRecord1) <= strtotime($dateforBackDated)) {
                    $queryForDailyResult = "SELECT att_status,att_date FROM attendance_backup WHERE emp_code='" . $key . "' AND att_date>='" . trim($dateforDailyRecord1) . "' AND att_date<='2017-04-12' order by att_date";
                    $resultForGetRecord1 = query($query, $queryForDailyResult, $pa, $opt, $ms_db);
                    if ($num($resultForGetRecord1) > 0) {
                        while ($row1 = $fetch($resultForGetRecord1)) {
                            if (trim($row1['att_status']) != '' && !empty($row1['att_status'])) {
                                $dataarr      = explode("~", $row1['att_status']);
                                $result       = COUNT($dataarr);
                                $result       = $result - 1;
                                $final_result = $dataarr[$result];
                                if (trim($final_result) == 'W') {
                                    $final_result = 'WO';
                                }
                                if (trim($final_result) == 'L') {
                                    $final_result = 'Leave';
                                }
                                $dateArray[$row1['att_date']] = trim($final_result);
                            } else {
                                $dateArray[$row1['att_date']] = "A";
                            }
                            
                        }
                    }
                    
                    $counterIsset = '1';
                }
                
                if (isset($counterIsset)) {
                    $queryForGetDailyResult = "SELECT date, status, Lateflag, Earlyflag FROM attendancesch WHERE date>='2017-04-13' AND date<='" . trim($toDate) . "' AND Emp_code='" . trim($key) . "' " . $where . " order by date";
                } else {
                    $queryForGetDailyResult = "SELECT date, status, Lateflag, Earlyflag FROM attendancesch WHERE date>='" . trim($dateforDailyRecord1) . "' AND date<='" . trim($toDate) . "' AND Emp_code='" . trim($key) . "' " . $where . " order by date";
                }
                $resultForGetRecord = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                
                $parsed         = '';
                $penaltyWarning = array();
                $penaltyApplied = array();
                if ($num($resultForGetRecord) > 0) {
                    while ($row = $fetch($resultForGetRecord)) {
                        $employeeDOJ = strtotime(date('Y-m-d', strtotime(trim($value['DOJ']))));
                        if ($employeeDOJ <= strtotime($row['date'])) {
                            $dateArray[$row['date']] = (isset($row['status']) && !empty($row['status'])) ? $row['status'] : "A";
                            
                            if (($row['Lateflag'] || $row['Earlyflag']) && count($penaltyWarning) == 2) {
                                $penaltyApplied[$row['date']] = $row['date'];
                            } else {
                                if (($row['Lateflag'] || $row['Earlyflag']) && count($penaltyWarning) < 2) {
                                    $penaltyWarning[$row['date']] = $row['date'];
                                }
                                
                                if ($row['Lateflag'] && $row['Earlyflag'] && count($penaltyWarning) < 2) {
                                    $penaltyWarning[$row['date']]           = $row['date'];
                                    $penaltyWarning[$row['date'] . "-temp"] = $row['date'];
                                }
                            }
                        }
                    }
                    $halfDayLegend        = 'H';
                    $fullDayLegend        = 'P';
                    $legendSetForRedColor = '';
                    if (count($penaltyApplied) > 0 || count($penaltyWarning) > 0) {
                        $getLegendForPresentHalfDayQuery  = "SELECT master, report_legend FROM attendanceLegendMasters WHERE master IN ('HLP','P') AND is_active = 1";
                        $getLegendForPresentHalfDayResult = query($query, $getLegendForPresentHalfDayQuery, $pa, $opt, $ms_db);
                        
                        if ($num($getLegendForPresentHalfDayResult) > 0) {
                            while ($getLegendForPresentHalfDayRow = $fetch($getLegendForPresentHalfDayResult)) {
                                if (trim($getLegendForPresentHalfDayRow['master']) == 'HLP')
                                    $halfDayLegend = trim($getLegendForPresentHalfDayRow['report_legend']);
                                if (trim($getLegendForPresentHalfDayRow['master']) == 'P')
                                    $fullDayLegend = trim($getLegendForPresentHalfDayRow['report_legend']);
                            }
                        }
                    }
                }
                $setDoj = 0;
                
                if (!empty($dateArray)) {
                    for ($counter = 1; $counter <= $dateDifference; $counter++) {
                        if (isset($dateArray[$dateforDailyRecord2])) {
                            
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $dateArray[$dateforDailyRecord2]);
                            
                            // for penalty
                            
                            if (isset($penaltyWarning[$dateforDailyRecord2]) || isset($penaltyApplied[$dateforDailyRecord2])) {
                                $styleArray = array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array(
                                            'rgb' => 'FF0000'
                                        )
                                    )
                                );
                                
                                if (isset($penaltyWarning[$dateforDailyRecord2])) {
                                    $legendSetForRedColor = $fullDayLegend;
                                } else if (isset($penaltyApplied[$dateforDailyRecord2])) {
                                    $legendSetForRedColor = $halfDayLegend;
                                }
                                
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $legendSetForRedColor);
                                $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . $counterForExcel)->applyFromArray($styleArray);
                            }
                            
                            $countForHorizonatalExcel++;
                        } else {
                            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                            $countForHorizonatalExcel++;
                        }
                        
                        $dateforDailyRecord2 = date('Y-m-d', strtotime(' +1 day', strtotime($dateforDailyRecord2)));
                        
                    }
                } else {
                    for ($counter = 1; $counter <= $dateDifference; $counter++) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                        $countForHorizonatalExcel++;
                        $dateforDailyRecord2 = date('Y-m-d', strtotime(' +1 day', strtotime($dateforDailyRecord2)));
                        
                    }
                    
                }
                
                $countForHorizonatalExcel = 0;
                $counterForExcel++;
                $i++;
            }
            
            $my_excel_filename = "upload/monthly-attendance-Summary.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/monthly-attendance-Summary.xls');
            
        } else if ($_POST['type'] == '15') {
            
            $this->getShiftName();
            $HorizontalColumnName     = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Function",
                "Sub Function",
                "Department",
                "Category",
                "Location",
                "Manager",
                "Scheduled Shift",
                "Scheduled Shift Time",
                "Actual Shift",
                "Time in",
                "Time Out",
                "Status",
                "Late Arrival",
                "Reason",
                "Early Departure",
                "Reason",
                "Hrs Work",
                "Less W. Hrs",
                "Over Stay",
                "Shift Change Self/Manager"
            );
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME",
                "GRD_NAME",
                "MNGR_NAME",
                "LOC_NAME",
                "FUNCT_NAME",
                "SUBFUNCT_NAME"
            );
            $where                    = '';
            //$advanceFilterConditions 				=	$this->advanceSearchOptionForJson($getFields,'4',$empCode);
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            $this->getShiftName();
            $resultStatus['error'] = $this->checkDat($_POST['fromDate'], $_POST['toDate'], '1');
            if ($resultStatus['error'] == 'Error') {
                return json_encode($resultStatus);
                die;
            }
            
            $fromDatearr        = explode("/", $_POST['fromDate']);
            $dateforDailyRecord = $fromDatearr[2] . "-" . $fromDatearr[1] . "-" . $fromDatearr[0];
            $toDatearr          = explode("/", $_POST['toDate']);
            $toDate             = $toDatearr[2] . "-" . $toDatearr[1] . "-" . $toDatearr[0];
            
            
            $headingInfo              = 'Shift Change Report For ' . $fromDatearr[0] . "-" . $fromDatearr[1] . "-" . $fromDatearr[2] . ' To ' . $toDatearr[0] . "-" . $toDatearr[1] . "-" . $toDatearr[2] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            $dateArray = array();
            
            if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] != 'HR') {
                
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = array_filter($empArr);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                $where .= (!empty($_POST['emp_code_list'])) ? " AND rost_change.Emp_code in (" . $empArr . ") " : "";
                
            }
            
            // $queryForGetDailyResult = "SELECT status,date,Emp_code FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND status!='' AND Schedule_Shift!=Actual_Shift AND Schedule_Shift!='' AND Actual_Shift!='' AND final_status!='Weekly Off' AND final_status!='POH'  AND final_status!='POW'  AND final_status!='Holiday' " . $where . " order by date";
            $queryForGetDailyResult = "SELECT Emp_code,RosterDate FROM rost_change WHERE RosterDate>='" . $dateforDailyRecord . "' AND RosterDate<='" . $toDate . "'";
            $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            if ($num($resultForGetRecord) > 0) {
                while ($row = $fetch($resultForGetRecord)) {
                    $dateArray[] = $row['RosterDate'] . '--' . $row['Emp_code'] . '--' . $row['oldShiftMaster'];
                }
            } else {
                $dateArray = array();
            }
            
            if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] != 'HR') {
                
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = array_filter($empArr);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                $where .= (!empty($_POST['emp_code_list'])) ? " AND attendancesch.Emp_code in (" . $empArr . ") " : "";
                
            }
            
            $queryForGetDailyResult = "SELECT status,date,Emp_code,Schedule_Shift,Actual_Shift FROM attendancesch WHERE date>='" . $dateforDailyRecord . "' AND date<='" . $toDate . "' AND status!='' AND Schedule_Shift!=Actual_Shift AND Schedule_Shift!='' AND Actual_Shift!='' AND final_status!='Weekly Off' AND final_status!='POH'  AND final_status!='POW'  AND final_status!='Holiday' " . $where . " order by date";
            $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            if ($num($resultForGetRecord) > 0) {
                while ($row = $fetch($resultForGetRecord)) {
                    $dateArray[] = $row['date'] . '--' . $row['Emp_code'] . '--K5245';
                }
            }
            
            if (!empty($dateArray)) {
                foreach ($dateArray as $key2 => $value2) {
                    $arr                     = explode("--", $value2);
                    $date                    = $arr[0];
                    $empCode                 = $arr[1];
                    $oldShift                = $arr[2];
                    $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '4', $empCode);
                    
                    foreach ($advanceFilterConditions as $key => $value) {
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['FUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['SUBFUNCT_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['GRD_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['LOC_NAME']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['MNGR_NAME']);
                        $countForHorizonatalExcel++;
                        
                        $queryForGetDailyResult = "SELECT Leavestatus,Emp_code,final_status,LeaveTypeText,odStatus,markPastStatus,statustitle,Lateflag, Earlyflag,date,Schedule_Shift,Intime,Outtime,Actual_Shift,Shift_start,Shift_end,status,Latemin,Earlymin,Workinghrs,Overstay FROM attendancesch WHERE date='" . $date . "' AND Emp_code='" . $empCode . "' AND final_status!='Weekly Off' AND final_status!='POH'  AND final_status!='POW'  AND final_status!='Holiday'  " . $where . " order by date";
                        $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                        if ($num($resultForGetRecord) > 0) {
                            while ($row = $fetch($resultForGetRecord)) {
                                $status = $this->getDayFinalStatus($row);
                                /*if($empCode == '60721'){
                                echo $status.'  END'; die;
                                }*/
                                if ($oldShift == 'K5245') {
                                    
                                    $actualShift = $row['Actual_Shift'];
                                    if (!empty($actualShift) && trim($actualShift) != '') {
                                        $actualShift = explode(" ", $actualShift);
                                        $actualShift = $actualShift[0];
                                        if ($status == 'POH' || $status == 'POW' || $status == 'HLD' || $status == 'WO') {
                                            $actualShift = '';
                                        }
                                    } else {
                                        $actualShift = '';
                                    }
                                    $scheduledShift = $row['Schedule_Shift'];
                                    if (!empty($scheduledShift) && trim($scheduledShift) != '') {
                                        
                                        $scheduledShiftArr = explode(" ", $scheduledShift);
                                        $scheduledShift    = $scheduledShiftArr[0];
                                        
                                        if ($status == 'POH' || $status == 'POW' || $status == 'A' || $status == 'HLD' || $status == 'WO') {
                                            $scheduledShift = '';
                                        }
                                        
                                    } else {
                                        $scheduledShift = '';
                                    }
                                    
                                } else {
                                    
                                    $actualShift = $row['Schedule_Shift'];
                                    if (!empty($actualShift) && trim($actualShift) != '') {
                                        $actualShift = explode(" ", $actualShift);
                                        $actualShift = $actualShift[0];
                                        if ($status == 'POH' || $status == 'POW' || $status == 'HLD' || $status == 'WO') {
                                            $actualShift = '';
                                        }
                                    } else {
                                        $actualShift = '';
                                    }
                                    
                                    if (isset($this->getShiftName[$oldShift])) {
                                        $scheduledShift = $this->getShiftName[$oldShift];
                                    } else {
                                        $scheduledShift = '';
                                    }
                                    
                                    if (!empty($scheduledShift) && trim($scheduledShift) != '') {
                                        $scheduledShiftArr = explode(" ", $scheduledShift);
                                        $scheduledShift    = $scheduledShiftArr[0];
                                        if ($status == 'POH' || $status == 'POW' || $status == 'A' || $status == 'HLD' || $status == 'WO') {
                                            $scheduledShift = '';
                                        }
                                    }
                                }
                                
                                if (strtotime($row['Intime']) > 0)
                                    $inTime = Date('d-m-Y h:i A', strtotime($row['Intime']));
                                else
                                    $inTime = '';
                                
                                if (strtotime($row['Outtime']) > 0)
                                    $Outtime = Date('d-m-Y h:i A', strtotime($row['Outtime']));
                                else
                                    $Outtime = '';
                                
                                
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $scheduledShift);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('h:i A', strtotime($row['Shift_start'])) . '-' . Date('h:i A', strtotime($row['Shift_end'])));
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $actualShift);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $inTime);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $Outtime);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $status);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Latemin']);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Earlymin']);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, '');
                                $countForHorizonatalExcel++;
                                if (trim($row['Intime']) != '' && trim($row['Outtime']) != '' && !empty($row['Intime']) && !empty($row['Outtime']) && (strtotime($row['Outtime']) > 0 && strtotime($row['Intime']) > 0)) {
                                    $time_in_24_hour_format_start    = date("H:i", strtotime($row['Shift_start']));
                                    $time_in_24_hour_format_end      = date("H:i", strtotime($row['Shift_end']));
                                    $shiftTiming                     = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                                    $time_in_24_hour_format_user     = date("H:i", strtotime($row['Intime']));
                                    $time_in_24_hour_format_user_end = date("H:i", strtotime($row['Outtime']));
                                    $shiftTiming1                    = $this->getTimeDifference($time_in_24_hour_format_user, $time_in_24_hour_format_user_end);
                                    if ($shiftTiming > $shiftTiming1) {
                                        $timeDiff        = $this->getTimeDifference($shiftTiming1, $shiftTiming);
                                        $time_in_24_hour = date("H:i", strtotime($timeDiff));
                                    }
                                } else {
                                    $shiftTiming1 = '';
                                }
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftTiming1);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $time_in_24_hour);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['Overstay']);
                                $countForHorizonatalExcel++;
                                $queryForGetDailyResult = "SELECT Emp_code,CreatedBy FROM rost_change Where Emp_code='" . $empCode . "' AND type_name='approve' AND RosterDate='" . $date . "'";
                                $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
                                if ($num($resultForGetRecord) > 0) {
                                    while ($row = $fetch($resultForGetRecord)) {
                                        if ($row['CreatedBy'] == $row['Emp_code'] || trim($row['CreatedBy']) == '') {
                                            $shiftChangeRequest = "Self";
                                        } else {
                                            $shiftChangeRequest = "Manager";
                                        }
                                    }
                                } else {
                                    $shiftChangeRequest = "Self";
                                }
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftChangeRequest);
                                $countForHorizonatalExcel++;
                                
                            }
                        }
                        $i++;
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    }
                }
            }
            
            $my_excel_filename = "upload/shift-change-report.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/shift-change-report.xls');
            
        } else if ($_POST['type'] == '21') {
            
            $HorizontalColumnName = array(
                "S.NO",
                "Employee Code",
                "Name",
                "Department",
                "Date Of OverTime",
                "In Time",
                "Out Time",
                "Actual start Time",
                "Actual End Time",
                "Total Hrs",
                "Planned Hrs.",
                "Approved Hrs",
                "Finalization Hrs",
                "Additional Hrs.",
                "Finalization Date & Time"
                
            );
            
            $countForHorizonatalExcel = 0;
            $counterForExcel          = 4;
            $getFields                = array(
                "Emp_Code",
                "EMP_NAME",
                "COST_NAME"
            );
            
            $where = '';
            
            if (!empty($_POST['shift'])) {
                $validShiftID = implode("','", $_POST['shift']);
                $where        = "AND shiftId in ('" . $validShiftID . "')";
            }
            
            if (!isset($_POST['monthFor']) || !isset($_POST['year']) || $_POST['monthFor'] == '') {
                $resultStatus['error'] = "Error";
                json_encode($resultStatus);
                die;
            }
            
            $advanceFilterConditions = $this->advanceSearchOptionForJson($getFields, '2');
            
            if ($_POST['monthFor'] != '' && $_POST['year'] != '') {
                $monthDay  = date('t', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $monthName = date('F', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
                $firstDate = date('Y-m-d', strtotime($_POST['year'] . '-' . $_POST['monthFor'] . '-1'));
            }
            
            $headingInfo              = 'OT Detail for ' . $monthName . ' ' . $_POST['year'] . '';
            $objPHPExcel              = $this->createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName);
            $countForHorizonatalExcel = 0;
            $i                        = 1;
            $counterForExcel++;
            $finalizationArr = array();
            
            if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] != 'HR') {
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = array_filter($empArr);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                $where .= (!empty($_POST['emp_code_list'])) ? " AND overtime_balances.Emp_Code in (" . $empArr . ") " : "";
            }
            
            $queryForGetDailyResult = "SELECT * FROM overtime_balances WHERE MONTH(OT_Date)='" . $_POST['monthFor'] . "' AND YEAR(OT_Date)='" . $_POST['year'] . "' order by OT_DATE,Emp_Code";
            $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
            $counterForHeader       = 0;
            if ($num($resultForGetRecord) > 0) {
                while ($row = $fetch($resultForGetRecord)) {
                    $shiftTime = $this->getShiftTime($row['OT_Date'], $row['Emp_Code'], $where);
                    if ((!empty($shiftTime)) && (strtotime($shiftTime['inPunch']) > '0')) {
                        
                        // && (strtotime($shiftTime['inPunch']) > '0')
                        foreach ($advanceFilterConditions as $key => $value) {
                            if ($row['Emp_Code'] == $key) {
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $i);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $key);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['EMP_NAME']);
                                $countForHorizonatalExcel++;
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $value['COST_NAME']);
                                $countForHorizonatalExcel++;
                                break;
                            }
                        }
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, Date('d-M-Y', strtotime($row['OT_Date'])));
                        $countForHorizonatalExcel++;
                        $shiftInPunch  = (strtotime($shiftTime['inPunch']) > 0) ? Date('d-M-Y h:i A', strtotime($shiftTime['inPunch'])) : '';
                        $shiftOutPunch = (strtotime($shiftTime['Outtime']) > 0) ? Date('d-M-Y h:i A', strtotime($shiftTime['Outtime'])) : '';
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftInPunch);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $shiftOutPunch);
                        $countForHorizonatalExcel++;
                        $otStartDate = (strtotime($shiftTime['inPunch']) > 0) ? Date('h:i A', strtotime($row['OT_StartTime'])) : '';
                        $otEndDate   = (strtotime($shiftTime['Outtime']) > 0) ? Date('h:i A', strtotime($row['OT_EndTime'])) : '';
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $otStartDate);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $otEndDate);
                        $countForHorizonatalExcel++;
                        if ($shiftInPunch != '' && $shiftOutPunch != '') {
                            
                            $time_in_24_hour_format_start = date("H:i", strtotime($shiftTime['inPunch']));
                            $time_in_24_hour_format_end   = date("H:i", strtotime($shiftTime['Outtime']));
                            $totalHours                   = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                        } else {
                            $totalHours = '';
                        }
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalHours);
                        $countForHorizonatalExcel++;
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['overTimeHours']);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $row['overTimeHours']);
                        $countForHorizonatalExcel++;
                        $finalOtHour                  = (trim($row['finalOtHour']) == '' || empty($row['finalOtHour']) || strtotime($row['finalOtHour']) < '0') ? '0:0' : $row['finalOtHour'];
                        $time_in_24_hour_format_start = date("H:i", strtotime($finalOtHour));
                        $time_in_24_hour_format_end   = date("H:i", strtotime($row['overTimeHours']));
                        $totalAdditionalHours         = $this->getTimeDifference($time_in_24_hour_format_start, $time_in_24_hour_format_end);
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $finalOtHour);
                        $countForHorizonatalExcel++;
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $totalAdditionalHours);
                        $countForHorizonatalExcel++;
                        $final_Approved = (isset($row['final_Approved']) && strtotime($row['final_Approved']) > '0' && trim($row['final_Approved']) != '') ? Date('d-M-Y h:i A', strtotime($row['final_Approved'])) : $row['final_Approved'];
                        $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $final_Approved);
                        $countForHorizonatalExcel++;
                        $otApproverName = array();
                        $otApproverStat = array();
                        
                        if (!empty($row['OT_Approver']) && trim($row['OT_Approver']) != '') {
                            $otApproverName = explode(",", $row['OT_Approver']);
                            $otApproverStat = explode(",", $row['appStatus']);
                            $flagSet        = 0;
                            
                            foreach ($otApproverName as $keysss => $valuess) {
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $valuess);
                                $countForHorizonatalExcel++;
                                $statusOFOT = (isset($otApproverStat[$flagSet])) ? $otApproverStat[$flagSet] : '';
                                if (!empty($statusOFOT) && trim($statusOFOT) != '') {
                                    switch ($statusOFOT) {
                                        case "1":
                                            $statusOFOT = "Pending";
                                            break;
                                        case "2":
                                            $statusOFOT = "Approved";
                                            break;
                                        case "3":
                                            $statusOFOT = "Rejected";
                                            break;
                                        case "4":
                                            $statusOFOT = "Cancel";
                                            break;
                                        case "5":
                                            $statusOFOT = "Approved Cancel";
                                            break;
                                        case "6":
                                            $statusOFOT = "Approved Cancellation";
                                            break;
                                        case "7":
                                            $statusOFOT = "Approved Cancel Rejected";
                                            break;
                                        default:
                                            $statusOFOT = "Pending";
                                    }
                                }
                                
                                $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . $counterForExcel, $statusOFOT);
                                $countForHorizonatalExcel++;
                                
                                $flagSet++;
                                if ($counterForHeader < $flagSet)
                                    $counterForHeader++;
                            }
                        }
                        $FinalstatusOFOT = ($row['status'] != '') ? $row['status'] : 'Pending';
                        if (!empty($FinalstatusOFOT) && trim($FinalstatusOFOT) != '') {
                            switch ($FinalstatusOFOT) {
                                case "1":
                                    $FinalstatusOFOT = "Pending";
                                    break;
                                case "2":
                                    $FinalstatusOFOT = "Finalized";
                                    break;
                                case "3":
                                    $FinalstatusOFOT = "Rejected";
                                    break;
                                case "4":
                                    $FinalstatusOFOT = "Cancel";
                                    break;
                                case "5":
                                    $FinalstatusOFOT = "Approved Cancel";
                                    break;
                                case "6":
                                    $FinalstatusOFOT = "Approved Cancellation";
                                    break;
                                case "7":
                                    $FinalstatusOFOT = "Approved Cancel Rejected";
                                    break;
                                default:
                                    $FinalstatusOFOT = "Pending";
                            }
                        }
                        $finalizationArr[$counterForExcel] = $FinalstatusOFOT;
                        
                        $i++;
                        $countForHorizonatalExcel = 0;
                        $counterForExcel++;
                    }
                    
                    
                }
                //die("fgsdfg");
            }
            $lastColumnIndex = '';
            if ($counterForHeader != '0') {
                $dynamicHeaderArray = array(
                    "P",
                    "Q",
                    "R",
                    "S",
                    "T",
                    "U",
                    "V",
                    "W",
                    "X",
                    "Y",
                    "Z",
                    "AA",
                    "AB",
                    "AC",
                    "AD",
                    "AE",
                    "AF",
                    "AG"
                );
                $i                  = 1;
                for ($j = 1; $j <= $counterForHeader; $j++) {
                    //echo $dynamicHeaderArray[$i-1].'4'."   ".$dynamicHeaderArray[$i].'4';		
                    $objPHPExcel->getActiveSheet()->SetCellValue($dynamicHeaderArray[$i - 1] . '4', 'Approver ' . $j);
                    $objPHPExcel->getActiveSheet()->getStyle($dynamicHeaderArray[$i - 1] . '4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#b8d8f5');
                    $objPHPExcel->getActiveSheet()->getStyle($dynamicHeaderArray[$i - 1] . '4')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($dynamicHeaderArray[$i - 1] . '4')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->SetCellValue($dynamicHeaderArray[$i] . '4', 'Status');
                    $objPHPExcel->getActiveSheet()->getStyle($dynamicHeaderArray[$i] . '4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#b8d8f5');
                    $objPHPExcel->getActiveSheet()->getStyle($dynamicHeaderArray[$i] . '4')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($dynamicHeaderArray[$i] . '4')->setAutoSize(true);
                    $i = $i + 2;
                }
                $objPHPExcel->getActiveSheet()->SetCellValue($dynamicHeaderArray[$i - 1] . '4', 'Finalaization Status');
                $lastColumnIndex = $dynamicHeaderArray[$i - 1];
                $objPHPExcel->getActiveSheet()->getStyle($dynamicHeaderArray[$i - 1] . '4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#b8d8f5');
                $objPHPExcel->getActiveSheet()->getStyle($dynamicHeaderArray[$i - 1] . '4')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension($dynamicHeaderArray[$i - 1] . '4')->setAutoSize(true);
                
            }
            //echo '<pre>'; print_r($finalizationArr); die;
            foreach ($finalizationArr as $key => $value) {
                if ($lastColumnIndex != '') {
                    $objPHPExcel->getActiveSheet()->SetCellValue($lastColumnIndex . $key, $value);
                }
            }
            //$colorCode             = '#ABEBC6';
            //$colorCodeForHeader    = '#b8d8f5';
            $my_excel_filename = "upload/OT-REPORT.xls";
            if (file_exists($my_excel_filename))
                unlink('upload/OT-REPORT.xls');
            
        }
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        return json_encode($resultStatus);
        $excelOutput = ob_get_clean();
        
    }
    
    function dayDifferencesBetweenDate($firstDate, $secondDate)
    {
        //print_r(date_parse($firstDate));
        //print_r(date_parse($secondDate)); 
        $firstDate  = strtotime($firstDate);
        $secondDate = strtotime($secondDate);
        $datediff   = $secondDate - $firstDate;
        
        return floor($datediff / (60 * 60 * 24));
        
    }
    
    
    function advanceSearchOption()
    {
        
        $whereCondition = "";
        
        
        $whereCondition .= (!empty($_POST['CompanyName'])) ? " hrdmastqry.COMP_CODE in (" . implode(",", $_POST['CompanyName']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['functionName'])) ? " hrdmastqry.FUNCT_CODE in (" . implode(",", $_POST['functionName']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['sfunction'])) ? " hrdmastqry.SFUNCT_CODE in (" . implode(",", $_POST['sfunction']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['grade'])) ? " hrdmastqry.GRD_CODE in (" . implode(",", $_POST['grade']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['location1'])) ? " hrdmastqry.LOC_CODE in (" . implode(",", $_POST['location1']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['mname'])) ? " hrdmastqry.MNGR_CODE in (" . implode(",", $_POST['mname']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['wlocation'])) ? " hrdmastqry.WLOC_CODE in (" . implode(",", $_POST['wlocation']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['designation'])) ? " hrdmastqry.DSG_CODE in (" . implode(",", $_POST['designation']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['bu'])) ? " hrdmastqry.BussCode in (" . implode(",", $_POST['bu']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['sbu'])) ? " hrdmastqry.SUBBuss_Code in (" . implode(",", $_POST['sbu']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['employeeType'])) ? " hrdmastqry.TYPE_CODE in (" . implode(",", $_POST['employeeType']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['process'])) ? " hrdmastqry.PROC_CODE in (" . implode(",", $_POST['process']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['ccenter'])) ? " hrdmastqry.COST_CODE in (" . implode(",", $_POST['ccenter']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['division'])) ? " hrdmastqry.Divi_Code in (" . implode(",", $_POST['division']) . ") AND" : "";
        $whereCondition .= (!empty($_POST['employee'])) ? " hrdmastqry.Emp_Code in (" . implode(",", $_POST['employee']) . ") AND" : "";
        
        return rtrim($whereCondition, "AND");
        
        
    }
    
    function markPastAttandanceData($empCode, $date)
    {
        
        $my_file     = 'upload/markPastAttendance.json';
        $handle      = fopen($my_file, 'r');
        $data        = fread($handle, filesize($my_file));
        $data        = json_decode($data);
        $resultArray = array();
        
        foreach ($data as $key) {
            if ((trim($key->CreatedBy) == trim($empCode)) && (strtotime($key->date_to) <= (strtotime($date))) && (strtotime($key->date_from) >= (strtotime($date)))) {
                $resultArray['CreatedBy']     = $key->CreatedBy;
                $resultArray['CreatedOn']     = $key->CreatedOn;
                $resultArray['date_from']     = $key->date_from;
                $resultArray['date_to']       = $key->date_to;
                $resultArray['markPastId']    = $key->markPastId;
                $resultArray['action_status'] = $key->action_status;
            }
        }
        return $resultArray;
    }
    
    function advanceSearchOptionForJson($getFieldsWhichAreUsedInExcel = null, $type = '', $empID = '', $empCodeArr = array())
    {
        $whereCondition = "";
        $my_file        = 'upload/' . $this->getFileName();
        $handle         = fopen($my_file, 'r');
        $data           = fread($handle, filesize($my_file));
        $data           = json_decode($data);
        
        $validEmpID            = array();
        $getResult             = array();
        $withoutAnyFilteration = array();
        // echo '<pre>'; print_r($data); echo '</pre>'; die;
        //echo '<pre>'; print_r($getFieldsWhichAreUsedInExcel); echo '</pre>'; die;
        
        
        foreach ($data as $key) {
            
            $EmpKey     = '';
            $error      = 0;
            $checkEmpty = 0;
            
            if ($_POST['emp_code_list'] != 'HR') {
                
                $empArr = explode(",", $_POST['emp_code_list']);
                //print_r($empArr); die;
                if (!in_array($key->Emp_Code, $empArr) && !empty($key->Emp_Code)) {
                    $key->Emp_Code = '-465248HKL2@';
                } else {
                    //echo 'Start '.$key->Emp_Code.'   ';
                    if ($key->Emp_Code == '60275')
                        $key->Emp_Code = '-465248HKL2@';
                }
            }
            $statusAvailable = true;
            $empAvaliable    = true;
            //echo $key->Emp_Code." ".$key->Status_Code."              ";
            
            if (isset($_POST['statusName']) && !empty($_POST['statusName'])) {
                
                if (in_array($key->Status_Code, $_POST['statusName'])) {
                    $statusAvailable = true;
                } else {
                    $statusAvailable = false;
                }
            } else {
                $statusAvailable = true;
            }
            
            
            if (trim($key->Emp_Code) != '' && !empty($key->Emp_Code) && ($statusAvailable)) {
                
                if (!empty($_POST['CompanyName']) && ($statusAvailable)) {
                    $checkEmpty = 1;
                    if (in_array($key->COMP_CODE, $_POST['CompanyName'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COMP_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['COMP_NAME'] = $key->COMP_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COMP_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['COMP_NAME'] = $key->COMP_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COMP_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['COMP_NAME']             = $key->COMP_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['COMP_NAME'] = $key->COMP_NAME;
                    }
                }
                
                if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DEPT_NAME", $getFieldsWhichAreUsedInExcel)) {
                    $getResult[$key->Emp_Code]['DEPT_NAME']             = $key->DEPT_NAME;
                    $withoutAnyFilteration[$key->Emp_Code]['DEPT_NAME'] = $key->DEPT_NAME;
                }
                if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DOJ", $getFieldsWhichAreUsedInExcel)) {
                    $getResult[$key->Emp_Code]['DOJ']             = $key->DOJ;
                    $withoutAnyFilteration[$key->Emp_Code]['DOJ'] = $key->DOJ;
                }
                
                if (!empty($_POST['statusName'])) {
                    $checkEmpty = 1;
                    if (in_array($key->Status_Code, $_POST['statusName'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("Status_Code", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['Status_Code'] = $key->Status_Code;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("Status_Code", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['Status_Code'] = $key->Status_Code;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("Status_Code", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['Status_Code']             = $key->Status_Code;
                        $withoutAnyFilteration[$key->Emp_Code]['Status_Code'] = $key->Status_Code;
                    }
                }
                
                if (!empty($_POST['functionName'])) {
                    $checkEmpty = 1;
                    if (in_array($key->FUNCT_CODE, $_POST['functionName'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("FUNCT_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['FUNCT_NAME'] = $key->FUNCT_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("FUNCT_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['FUNCT_NAME'] = $key->FUNCT_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("FUNCT_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['FUNCT_NAME']             = $key->FUNCT_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['FUNCT_NAME'] = $key->FUNCT_NAME;
                    }
                }
                
                if (!empty($_POST['sfunction'])) {
                    $checkEmpty = 1;
                    if (in_array($key->SFUNCT_CODE, $_POST['sfunction'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBFUNCT_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['SUBFUNCT_NAME'] = $key->SUBFUNCT_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBFUNCT_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['SUBFUNCT_NAME'] = $key->SUBFUNCT_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBFUNCT_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['SUBFUNCT_NAME']             = $key->SUBFUNCT_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['SUBFUNCT_NAME'] = $key->SUBFUNCT_NAME;
                    }
                }
                
                if (!empty($_POST['grade'])) {
                    $checkEmpty = 1;
                    if (in_array($key->GRD_CODE, $_POST['grade'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("GRD_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['GRD_NAME'] = $key->GRD_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("GRD_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['GRD_NAME'] = $key->GRD_NAME;
                    }
                } else {
                    
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("GRD_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['GRD_NAME']             = $key->GRD_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['GRD_NAME'] = $key->GRD_NAME;
                    }
                }
                
                if (!empty($_POST['location1'])) {
                    $checkEmpty = 1;
                    if (in_array($key->LOC_CODE, $_POST['location1'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("LOC_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['LOC_NAME'] = $key->LOC_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("LOC_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['LOC_NAME'] = $key->LOC_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("LOC_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['LOC_NAME']             = $key->LOC_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['LOC_NAME'] = $key->LOC_NAME;
                    }
                    
                }
                
                if (!empty($_POST['wlocation'])) {
                    $checkEmpty = 1;
                    if (in_array($key->WLOC_CODE, $_POST['wlocation'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("WLOC_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['WLOC_NAME'] = $key->WLOC_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("WLOC_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['WLOC_NAME'] = $key->WLOC_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("WLOC_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['WLOC_NAME']             = $key->WLOC_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['WLOC_NAME'] = $key->WLOC_NAME;
                    }
                }
                
                if (!empty($_POST['mname'])) {
                    $checkEmpty = 1;
                    
                    if (in_array($key->MNGR_CODE, $_POST['mname'])) {
                        //echo $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("MNGR_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['MNGR_NAME'] = $key->MNGR_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("MNGR_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['MNGR_NAME'] = $key->MNGR_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("MNGR_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['MNGR_NAME']             = $key->MNGR_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['MNGR_NAME'] = $key->MNGR_NAME;
                    }
                }
                
                if (!empty($_POST['designation'])) {
                    $checkEmpty = 1;
                    if (in_array($key->DSG_CODE, $_POST['designation'])) {
                        
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DSG_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['DSG_NAME'] = $key->DSG_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DSG_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['DSG_NAME'] = $key->DSG_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DSG_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['DSG_NAME']             = $key->DSG_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['DSG_NAME'] = $key->DSG_NAME;
                    }
                }
                
                if (!empty($_POST['bu'])) {
                    $checkEmpty = 1;
                    if (in_array($key->BussCode, $_POST['bu'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("BUSSNAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['BUSSNAME'] = $key->BUSSNAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("BUSSNAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['BUSSNAME'] = $key->BUSSNAME;
                    }
                } else {
                    
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("BUSSNAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['BUSSNAME']             = $key->BUSSNAME;
                        $withoutAnyFilteration[$key->Emp_Code]['BUSSNAME'] = $key->BUSSNAME;
                    }
                }
                
                if (!empty($_POST['sbu'])) {
                    $checkEmpty = 1;
                    if (in_array($key->SUBBuss_Code, $_POST['sbu'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBBUSSNAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['SUBBUSSNAME'] = $key->SUBBUSSNAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBBUSSNAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['SUBBUSSNAME'] = $key->SUBBUSSNAME;
                    }
                } else {
                    
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("SUBBUSSNAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['SUBBUSSNAME']             = $key->SUBBUSSNAME;
                        $withoutAnyFilteration[$key->Emp_Code]['SUBBUSSNAME'] = $key->SUBBUSSNAME;
                    }
                }
                
                if (!empty($_POST['employeeType'])) {
                    $checkEmpty = 1;
                    if (in_array($key->TYPE_CODE, $_POST['employeeType'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("TYPE_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['TYPE_NAME'] = $key->TYPE_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("TYPE_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['TYPE_NAME'] = $key->TYPE_NAME;
                    }
                } else {
                    
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("TYPE_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['TYPE_NAME']             = $key->TYPE_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['TYPE_NAME'] = $key->TYPE_NAME;
                    }
                }
                
                if (!empty($_POST['process'])) {
                    $checkEmpty = 1;
                    if (in_array($key->PROC_CODE, $_POST['process'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("PROC_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['PROC_NAME'] = $key->PROC_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("PROC_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['PROC_NAME'] = $key->PROC_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("PROC_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['PROC_NAME']             = $key->PROC_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['PROC_NAME'] = $key->PROC_NAME;
                    }
                }
                
                if (!empty($_POST['ccenter'])) {
                    $checkEmpty = 1;
                    if (in_array($key->COST_CODE, $_POST['ccenter'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COST_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['COST_NAME'] = $key->COST_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COST_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['COST_NAME'] = $key->COST_NAME;
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("COST_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['COST_NAME']             = $key->COST_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['COST_NAME'] = $key->COST_NAME;
                    }
                }
                
                if (!empty($_POST['division'])) {
                    $checkEmpty = 1;
                    if (in_array($key->Divi_Code, $_POST['division'])) {
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DIVI_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['DIVI_NAME'] = $key->DIVI_NAME;
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DIVI_NAME", $getFieldsWhichAreUsedInExcel))
                            $getResult[$key->Emp_Code]['DIVI_NAME'] = $key->DIVI_NAME;
                    }
                } else {
                    
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("DIVI_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $getResult[$key->Emp_Code]['DIVI_NAME']             = $key->DIVI_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['DIVI_NAME'] = $key->DIVI_NAME;
                    }
                }
                
                if (!empty($_POST['employee'])) {
                    $checkEmpty = 1;
                    if (in_array($key->Emp_Code, $_POST['employee'])) {
                        
                        $EmpKey = $key->Emp_Code;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("EMP_NAME", $getFieldsWhichAreUsedInExcel)) {
                            $getResult[$key->Emp_Code]['EMP_NAME'] = $key->EMP_NAME;
                        }
                    } else {
                        $EmpKey = '';
                        $error  = 1;
                        if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("EMP_NAME", $getFieldsWhichAreUsedInExcel)) {
                            $getResult[$key->Emp_Code]['EMP_NAME'] = $key->EMP_NAME;
                        }
                    }
                } else {
                    if (isset($getFieldsWhichAreUsedInExcel) && !empty($getFieldsWhichAreUsedInExcel) && in_array("EMP_NAME", $getFieldsWhichAreUsedInExcel)) {
                        $EmpKey                                            = $key->Emp_Code;
                        $getResult[$key->Emp_Code]['EMP_NAME']             = $key->EMP_NAME;
                        $withoutAnyFilteration[$key->Emp_Code]['EMP_NAME'] = $key->EMP_NAME;
                    }
                }
            }
            
            //   echo '  ';
            if ($EmpKey != '' && $error == 0) {
                if ($EmpKey != '-465248HKL2@')
                    $validEmpID[] = $EmpKey;
            } else {
                if (isset($getResult[$key->Emp_Code]))
                    unset($getResult[$key->Emp_Code]);
            }
            
            if ($type == '4' && $empID != '') {
                //echo "P";
                if ($empID != $key->Emp_Code) {
                    unset($getResult);
                } else {
                    //print_r($getResult);
                    break;
                }
            } else if ($type == '5' && !empty($empCodeArr)) {
                
                if (!in_array($key->Emp_Code, $empCodeArr)) {
                    unset($getResult[$key->Emp_Code]);
                }
            }
            
        }
        
        // $validEmpID 	=	implode("','",$validEmpID);
        //echo '<pre>'; print_r($validEmpID); echo '</pre> test'; 
        // echo '<pre>'; print_r($getResult); echo '</pre> test';  die;
        if (isset($withoutAnyFilteration['-465248HKL2@']))
            unset($withoutAnyFilteration['-465248HKL2@']);
        if (isset($getResult['-465248HKL2@']))
            unset($getResult['-465248HKL2@']);
        //echo '<pre>'; print_r($withoutAnyFilteration); echo '</pre>'; die;
        if ($type == '2') {
            return $getResult;
            die;
        }
        if ($type == '5') {
            return $getResult;
        } else if ($type != '1') {
            
            if ($checkEmpty == '1' || ($type == '4' && $empID != '')) {
                unset($withoutAnyFilteration);
                return $getResult;
            } else {
                return $withoutAnyFilteration;
            }
        } else {
            return $validEmpID;
        }
        
    }
    
    
    function getHolidays($startDate, $endDate, $empCode = '')
    {
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $where                    = "";
        //get employees data
        $getEmployeeDataSql       = "SELECT LOC_CODE,COMP_CODE,BussCode AS BUS_CODE ,SUBBuss_CODE AS SUBBUS_CODE,WLOC_CODE,FUNCT_CODE AS FUNC_CODE,SFUNCT_CODE AS SUBFUNC_CODE,COST_CODE,PROC_CODE,GRD_CODE,DSG_CODE FROM HrdMastQry WHERE Emp_Code = '" . $empCode . "'";
        $getEmployeeDataSqlResult = query($query, $getEmployeeDataSql, $pa, $opt, $ms_db);
        if ($num($getEmployeeDataSqlResult) > 0) {
            $getEmployeeData = $fetch($getEmployeeDataSqlResult);
            
        }
        
        $result1 = array();
        
        /*   Query For Holidays which are applicable for all Employees without any restriction */
        
        $queryForAll = "SELECT HDATE,HDESC FROM Holidays WHERE ( HDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ) AND H_status=1 
							AND (LOC_CODE is null or LOC_CODE = '') 
							AND (COMP_CODE is null or COMP_CODE = '')
							AND (BUS_CODE is null or BUS_CODE = '')
							AND (SUBBUS_CODE is null or SUBBUS_CODE = '')
							AND (WLOC_CODE is null or WLOC_CODE = '')
							AND (FUNC_CODE is null or FUNC_CODE = '')
							AND (SUBFUNC_CODE is null or SUBFUNC_CODE = '')
							AND (COST_CODE is null or COST_CODE = '')
							AND (PROC_CODE is null or PROC_CODE = '')
							AND (GRD_CODE is null or GRD_CODE = '')
							AND (DSG_CODE is null or DSG_CODE = '')";
        
        $resultq = query($query, $queryForAll, $pa, $opt, $ms_db);
        
        if ($resultq) {
            $tempArray4 = $num($resultq);
        } else {
            $tempArray4 = -1;
        }
        if ($tempArray4 > 0) {
            while ($rowq = $fetch($resultq)) {
                $result1['Weekly Off'][] = array(
                    'title' => $rowq['HDESC'],
                    'start' => date('Y-m-d', strtotime($rowq['HDATE']))
                );
                $forAllHolidays .= "'" . $rowq['HDATE'] . "',";
            }
            
        }
        $forAllHolidays = rtrim($forAllHolidays, ",");
        
        if ($forAllHolidays != '') {
            $wherefor = " AND HDATE NOT IN (" . $forAllHolidays . ")";
            
        }
        
        /*  END Process  */
        
        $queryNew = "SELECT * FROM Holidays WHERE ( HDATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ) AND  H_status=1" . $wherefor . "";
        $resultq  = query($query, $queryNew, $pa, $opt, $ms_db);
        
        if ($resultq) {
            $tempArray4 = $num($resultq);
        } else {
            $tempArray4 = -1;
        }
        
        if ($tempArray4 > 0) {
            
            while ($rowy = $fetch($resultq)) {
                
                if (trim($rowy['LOC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['LOC_CODE'] == '')) ? "" : " AND ( ','+LOC_CODE+',' LIKE '%," . $getEmployeeData['LOC_CODE'] . ",%' )";
                }
                if (trim($rowy['COMP_CODE']) != '') {
                    $where .= (trim($getEmployeeData['COMP_CODE'] == '')) ? "" : " AND ( ','+COMP_CODE+',' LIKE '%," . $getEmployeeData['COMP_CODE'] . ",%' )";
                }
                if (trim($rowy['BUS_CODE']) != '') {
                    $where .= (trim($getEmployeeData['BUS_CODE'] == '')) ? "" : " AND ( ','+BUS_CODE+',' LIKE '%," . $getEmployeeData['BUS_CODE'] . ",%' )";
                }
                if (trim($rowy['SUBBUS_CODE']) != '') {
                    $where .= (trim($getEmployeeData['SUBBUS_CODE'] == '')) ? "" : " AND ( ','+SUBBUS_CODE+',' LIKE '%," . $getEmployeeData['SUBBUS_CODE'] . ",%' )";
                }
                if (trim($rowy['WLOC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['WLOC_CODE'] == '')) ? "" : " AND ( ','+WLOC_CODE+',' LIKE '%," . $getEmployeeData['WLOC_CODE'] . ",%' )";
                }
                if (trim($rowy['FUNC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['FUNC_CODE'] == '')) ? "" : " AND ( ','+FUNC_CODE+',' LIKE '%," . $getEmployeeData['FUNC_CODE'] . ",%' )";
                }
                if (trim($rowy['SUBFUNC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['SUBFUNC_CODE'] == '')) ? "" : " AND ( ','+SUBFUNC_CODE+',' LIKE '%," . $getEmployeeData['SUBFUNC_CODE'] . ",%' )";
                }
                if (trim($rowy['COST_CODE']) != '') {
                    $where .= (trim($getEmployeeData['COST_CODE'] == '')) ? "" : " AND ( ','+COST_CODE+',' LIKE '%," . $getEmployeeData['COST_CODE'] . ",%' )";
                }
                if (trim($rowy['PROC_CODE']) != '') {
                    $where .= (trim($getEmployeeData['PROC_CODE'] == '')) ? "" : " AND ( ','+PROC_CODE+',' LIKE '%," . $getEmployeeData['PROC_CODE'] . ",%' )";
                }
                if (trim($rowy['GRD_CODE']) != '') {
                    $where .= (trim($getEmployeeData['GRD_CODE'] == '')) ? "" : " AND ( ','+GRD_CODE+',' LIKE '%," . $getEmployeeData['GRD_CODE'] . ",%' )";
                }
                if (trim($rowy['DSG_CODE']) != '') {
                    $where .= (trim($getEmployeeData['DSG_CODE'] == '')) ? "" : " AND ( ','+DSG_CODE+',' LIKE '%," . $getEmployeeData['DSG_CODE'] . ",%' )";
                }
                
                $sqlq = "SELECT HDATE,HDESC FROM Holidays WHERE HDATE='" . $rowy['HDATE'] . "' AND H_status=1 " . $where . " ";
                
                $resultp = query($query, $sqlq, $pa, $opt, $ms_db);
                
                if ($resultq) {
                    $tempArray4 = $num($resultp);
                } else {
                    $tempArray4 = -1;
                }
                
                if ($tempArray4 > 0) {
                    while ($rowq = $fetch($resultp)) {
                        $result1['Holidays'][] = array(
                            'title' => $rowq['HDESC'],
                            'start' => date('Y-m-d', strtotime($rowq['HDATE']))
                        );
                    }
                    
                }
                $where = "";
            }
            
        }
        
        
        return $result1;
        
    }
    
    function leaveBaseOFDate($from, $to, $empCode, $leaveType)
    {
        
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        
        $totalNoOFLeave       = 0;
        $getPreviousIdOfLeave = '';
        $leaveArray           = array();
        
        
        $getLeaveData = "select SUM(CASE WHEN LvTo >'" . $to . "' THEN DATEDIFF(day,LvFrom,'" . $to . "')+1 ELSE DATEDIFF(day,LvFrom,LvTo)+1 END) AS leave,LvType,SUM(CASE WHEN FromHalf ='1FH' OR toHalf ='2FH' THEN 1 ELSE 0 END) AS halfDay from leave 
						where LvFrom>='" . $from . "' AND LvFrom<='" . $to . "' AND CreatedBy='" . $empCode . "' AND leave.status in ('2') group by LvType";
        
        $getEmployeeDataSqlResult = query($query, $getLeaveData, $pa, $opt, $ms_db);
        $n                        = 0;
        if ($num($getEmployeeDataSqlResult) > 0) {
            while ($rowel12 = $fetch($getEmployeeDataSqlResult)) {
                $n = 0;
                if (trim($rowel12['halfDay']) != '0') {
                    $n = trim($rowel12['halfDay']) / 2;
                }
                $totalNoOFLeave                       = $n + trim($rowel12['leave']);
                $leaveArray[trim($rowel12['LvType'])] = $totalNoOFLeave;
            }
        }
        
        return $leaveArray;
        
    }
    
    function getFileName()
    {
        
        if ($handle = opendir('upload/')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && strtolower(substr($file, strrpos($file, '.') + 1)) == 'json') {
                    $thelist[] = $file;
                }
            }
            closedir($handle);
        }
        
        if (isset($thelist) && !empty($thelist))
            return $thelist[0];
    }
    
    function createHeaderOfExcel($objPHPExcel, $headingSetIndex, $headingInfo, $colorCode, $colorCodeForHeader, $columnName, $countForHorizonatalExcel, $counterForExcel, $HorizontalColumnName)
    {
        
        
        $objPHPExcel->getActiveSheet()->SetCellValue($headingSetIndex, $headingInfo);
        $objPHPExcel->getActiveSheet()->getStyle($headingSetIndex)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCode);
        $objPHPExcel->getActiveSheet()->getStyle($headingSetIndex)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension($headingSetIndex)->setAutoSize(true);
        
        foreach ($HorizontalColumnName as $key => $value) {
            
            $objPHPExcel->getActiveSheet()->SetCellValue('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel, $value);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($colorCodeForHeader);
            $objPHPExcel->getActiveSheet()->getStyle('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('' . $columnName[$countForHorizonatalExcel] . '' . $counterForExcel . '')->setAutoSize(true);
            $countForHorizonatalExcel++;
        }
        
        
        return $objPHPExcel;
    }
    
    function columnOfExcel($counter)
    {
        
        $columnName1 = array(
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        );
        $columnName  = $columnName1;
        for ($i = 0; $i <= $counter; $i++) {
            foreach ($columnName1 as $key => $value) {
                $columnName[] = $columnName1[$i] . $value;
            }
        }
        return $columnName;
    }
    
    function getTimeDifference($from, $to)
    {
        //echo $from.'  '.$to;
        //$one	=	"11:45";
        //$two	=	"09:14";
        $test  = explode(":", $to);
        $test1 = explode(":", $from);
        
        if ($test[1] < $test1[1]) {
            $hour    = $test[0] - 1;
            $minutes = $test[1] + 60;
            
            $minutes = sprintf("%02d", $minutes - $test1[1]);
            $hour    = sprintf("%02d", $hour - $test1[0]);
            
        } else {
            
            $minutes = $test[1] - $test1[1];
            $hour    = $test[0] - $test1[0];
            $minutes = sprintf("%02d", $minutes);
            $hour    = sprintf("%02d", $hour);
        }
        
        
        
        
        return $hour . ":" . $minutes;
    }
    
    /*function getTimeDifference($from, $to)
    {
    $to_time   = strtotime($from);
    $from_time = strtotime($to);
    return round(abs($to_time - $from_time) / 60 / 60, 2);
    }
    
    */
    function addHourMinutes($one, $two)
    {
        $test    = explode(":", $one);
        $test1   = explode(":", $two);
        $minutes = $test[1] + $test1[1];
        $hour    = $test[0] + $test1[0];
        
        if ($minutes >= 60) {
            $minutes = sprintf("%02d", $minutes - 60);
            $hour    = sprintf("%02d", $hour + 1);
        }
        return $hour . ":" . $minutes;
    }
    
    function checkDat($startDate, $endDate = '', $type = '')
    {
        if ($type == 1) {
            if (isset($startDate) && trim($endDate) == '' && trim($endDate) == '') {
                return "Error";
            }
        } else {
            if (isset($startDate) && trim($startDate) == '')
                return "Error";
        }
    }
    
    function generateHeadCountReport($objPHPExcel)
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $error  = array();
        $dateEx = date('d-M-Y', strtotime(str_replace('/', '-', $_POST['daily']))); //12/04/2017
        
        $getQuery = "SELECT LOV_Text FROM LOVMast where LOV_Field='HeadCount' and LOV_Active ='Y'";
        $result   = query($query, $getQuery, $pa, $opt, $ms_db);
        $value    = $fetch($result);
        $getQuery = $value[0];
        $sql      = str_replace('{DATE}', $dateEx, $getQuery);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'SNO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Description');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'HeadCount');
        
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C1')->setAutoSize(true);
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        $i = 2;
        while ($value = $fetch($result)) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value['SNo']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value['Description']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value['HeadCount']);
            $i++;
        }
        
        $my_excel_filename = "upload/HeadCountReport.xls";
        if (file_exists($my_excel_filename))
            unlink('upload/shift-change-report.xls');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        $resultStatus['error']    = "";
        
        return json_encode($resultStatus);
        //$excelOutput = ob_get_clean();		
    }
    
    
    function generateViewRosterReport($objPHPExcel)
    {
        //ini_set("display_errors","on");   
        //error_reporting(E_ALL);
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $error = array();
        
        $newDateS = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['fromDate'])));
        $newDateE = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['toDate'])));
        
        
        
        $begin = new DateTime($newDateS);
        $end   = new DateTime($newDateE);
        $end   = $end->modify('+1 day');
        
        
        $interval = new DateInterval('P1D');
        $period   = new DatePeriod($begin, $interval, $end);
        
        $headCol             = $this->columnOfExcel("12");
        $horizontalStaticCol = array(
            "S.No.",
            "Emp Code",
            "Emp Name",
            "Manager",
            "Department"
        );
        $setdatewithdb       = array();
        foreach ($period as $dt) {
            $horizontalStaticCol[] = $dt->format("d M");
            $setdatewithdb[]       = $dt->format("Y-m-d");
            //echo $dt->format( "d M y")."\n";
        }
        
        foreach ($horizontalStaticCol as $key => $value) {
            $objPHPExcel->getActiveSheet()->SetCellValue($headCol[$key] . '1', $value);
            $objPHPExcel->getActiveSheet()->getStyle($headCol[$key] . '1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension($headCol[$key] . '1')->setAutoSize(true);
            
            $objPHPExcel->getActiveSheet()->getStyle($headCol[$key] . '1')->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'F28A8C'
                )
            ));
            
        }
        
        if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] == "HR") {
            $empFilter  = " ";
            $empFilter2 = " ";
            
        } else {
            $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
            $empArr                 = explode(",", $_POST['emp_code_list']);
            $empArr                 = implode("','", $empArr);
            $empArr                 = "'" . $empArr . "'";
            
            $empFilter  = " AND Roster_schema.Emp_Code IN( " . $empArr . " ) ";
            $empFilter2 = " AND RS.Emp_Code IN( " . $empArr . " ) ";
            
        }
        
        //$empFilter = " AND Roster_schema.Emp_Code IN( '16268' ) ";
        //$empFilter2 = " AND RS.Emp_Code IN( '16268' ) ";        
        
        $getQuery = "SELECT RS.RostID, RS.EMP_CODE,HM.EMP_NAME,HM.MNGR_NAME,HM.DEPT_NAME, RS.RosterName,CAST(RS.start_rost AS DATE) AS RosterStart,CAST(RS.end_rost AS DATE) AS [RosterEnd],RC.[type_name],
CASE WHEN RS.start_rost=RC.RosterDate AND RC.[type_name]='approve' THEN 
RC.Shift_Code ELSE SM.Shift_Code END Shift_Code, RS.auto_period FROM ROSTER_SCHEMA RS
LEFT OUTER JOIN ShiftChangeqry RC ON RS.start_rost=RC.RosterDate AND RC.Emp_code=RS.Emp_Code 
INNER JOIN ATT_ROSTER AR ON RS.RosterName=AR.roster AND RS.start_rost=AR.RosterDate
INNER JOIN ShiftMast SM ON  AR.shiftMaster=SM.ShiftMastId 
INNER JOIN ShiftPatternMast SP ON AR.shiftPattern=SP.ShiftPatternMastid
INNER JOIN HrdMastqry HM ON HM.Emp_Code = RS.EMP_CODE  
WHERE start_rost IN ( SELECT MAX(start_rost) fROM Roster_schema 
      WHERE CAST(end_rost AS DATE) <= '" . $newDateE . "'  AND CAST(start_rost AS DATE) >= '" . $newDateS . "' " . $empFilter . " AND   ROSTER_SCHEMA.auto_period=1 AND Roster_schema.RosterName LIKE 'rost-upload%'
      GROUP BY DAY(start_rost),EMP_CODE ) " . $empFilter2 . " AND HM.GRD_CODE IN (1,3) AND RS.RosterName LIKE 'rost-upload%' ORDER BY RS.EMP_CODE,RS.start_rost";
        
        $result = query($query, $getQuery, $pa, $opt, $ms_db);
        
        
        $empNamesArray = array();
        $mywholedata   = array();
        while ($value = $fetch($result)) {
            
            array_push($mywholedata, $value['RosterStart']);
            
            if (!in_array($value['EMP_CODE'], $empNamesArray)) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . self::$i, self::$i - 1);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . self::$i, $value['EMP_CODE']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . self::$i, $value['EMP_NAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . self::$i, $value['MNGR_NAME']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . self::$i, $value['DEPT_NAME']);
                self::$i++;
                $empNamesArray[] = $value['EMP_CODE'];
            }
            
        }
        self::$i = 3;
        foreach ($empNamesArray as $key => $x) {
            self::$j = 5;
            $this->fillExcel($empNamesArray, $setdatewithdb, $objPHPExcel, $mywholedata, $headCol);
            self::$i++;
        }
        
        $my_excel_filename = "upload/ViewRosterReport.xls";
        if (file_exists($my_excel_filename))
            unlink('upload/ViewRosterReport.xls');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        $resultStatus['error']    = "";
        return json_encode($resultStatus);
        
    }
    
    
    function fillExcel($empNamesArray, $setdatewithdb, $objPHPExcel, $mywholedata, $headCol)
    {
        
        foreach ($setdatewithdb as $key => $value4) {
            if ($value4 == $mywholedata[self::$mainkey]) {
                $k = self::$i - 1;
                $objPHPExcel->getActiveSheet()->SetCellValue($headCol[self::$j] . $k, "F");
                /*$objPHPExcel->getActiveSheet()->getStyle($headCol[self::$j] . $k)->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                'rgb' => 'F2F2F2'
                )
                ));*/
                self::$j++;
                self::$mainkey++;
            } else {
                $k = self::$i - 1;
                $objPHPExcel->getActiveSheet()->SetCellValue($headCol[self::$j] . $k, "B");
                $objPHPExcel->getActiveSheet()->getStyle($headCol[self::$j] . $k)->getFill()->applyFromArray(array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array(
                        'rgb' => 'F28A8C'
                    )
                ));
                self::$j++;
            }
        }
    }
    
    function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini    = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
    
    function getShiftTime($date, $emp_code, $where)
    {
        
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $result                 = array();
        $queryForGetDailyResult = "SELECT date,Intime,Outtime FROM attendancesch WHERE Schedule_Shift!='' AND Emp_code='" . $emp_code . "' AND date='" . $date . "' " . $where . "";
        $resultForGetRecord     = query($query, $queryForGetDailyResult, $pa, $opt, $ms_db);
        
        if ($num($resultForGetRecord) > 0) {
            while ($row = $fetch($resultForGetRecord)) {
                $result['inPunch'] = $row['Intime'];
                $result['Outtime'] = $row['Outtime'];
            }
        }
        
        return $result;
    }
    function generateAttendancepunchReport($objPHPExcel)
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $error    = array();
        $fromdate = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['fromDate']))); //12/04/2017
        $emp      = $_POST['employee'];
        $mystring = implode(',', $emp);
        // echo $mystring;die;
        $todate   = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['toDate'])));
        $where    = '';
        
        if (isset($_POST['emp_code_list']) && $_POST['emp_code_list'] != 'HR') {
            
            if (!empty($_POST['emp_code_list']) && trim($_POST['emp_code_list']) != '') {
                $_POST['emp_code_list'] = $_POST['emp_code_list'] . ',';
                $empArr                 = explode(",", $_POST['emp_code_list']);
                $empArr                 = array_filter($empArr);
                $empArr                 = implode("','", $empArr);
                $empArr                 = "'" . $empArr . "'";
                $where .= (!empty($_POST['emp_code_list'])) ? " AND ATTENDANCEALL.EMP_CODE in (" . $empArr . ") " : "";
            } else {
                if (isset($_SESSION['usercode']) && !empty($_SESSION['usercode'])) {
                    $where .= " AND ATTENDANCEALL.EMP_CODE in ('" . $_SESSION['usercode'] . "')";
                }
                
            }
        }
        //echo "bbbb".$mystring."aaaa";
        
        $getQuery = "select CONVERT(VARCHAR(19),PunchDate,109) as punchtime,CONVERT(VARCHAR(11),PunchDate,6) as punchdate,Emp_code,MachinID,CONVERT(VARCHAR(19),UPDATEDON,109) as UPDATEDON1 from ATTENDANCEALL where cast(PunchDate as date) between '$fromdate' and '$todate' " . $where . " order by EMP_CODE,punchdate";
        
        $result = query($query, $getQuery, $pa, $opt, $ms_db);
        //$value    = $fetch($result);
        // $getQuery = $value[0];
        //$sql      = str_replace('{DATE}', $dateEx, $getQuery);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Emp_Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'MachinID');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Punch Time');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Timestamp');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C1')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D1')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E1')->setAutoSize(true);
        
        
        //$result = query($query, $sql, $pa, $opt, $ms_db);
        
        $i = 2;
        while ($value = $fetch($result)) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value['Emp_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value['punchdate']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value['MachinID']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $value['punchtime']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $value['UPDATEDON1']);
            $i++;
        }
        
        $my_excel_filename = "upload/AttendanceReport.xls";
        if (file_exists($my_excel_filename))
            unlink('upload/AttendanceReport.xls');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        $resultStatus['error']    = "";
        // echo $resultStatus;die;
        return json_encode($resultStatus);
        //$excelOutput = ob_get_clean();		
        
    }
    
    function generatenonActiveUserReport($objPHPExcel)
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $error = array();
        // echo $employee = "'".str_replace(",", "','", implode(",", $_POST['employee']))."'"; die;
        $whereCondition .= (!empty($_POST['employee'])) ? " AND Emp_Code in (" . "'" . str_replace(",", "','", implode(",", $_POST['employee'])) . "'" . ") " : "";
        $sql = "with cte as ( select Emp_Code,(Emp_FName + ' '+ Emp_MName+ ' '+ Emp_LName) as user_name,OEMailID,GRD_NAME from HrdMastqry where LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', emp_code),2)) not in (select distinct  userid from Users ) and Status_code='01' $whereCondition) 
        select * from cte";
        
        $sql1 = "with cte as ( select Emp_Code,(Emp_FName + ' '+ Emp_MName+ ' ' + Emp_LName) as user_name,GRD_NAME from HrdMastqry where LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', emp_code),2)) in (select distinct userid from Users where Users.IsLocked = 'Y' and Users.UserActive != 1) $whereCondition)
        select *,(select top 1  UserActive from Users u where u.UserID = LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', cte.emp_code),2)))[UserActive],(select top 1 IsLocked from Users u where u.UserID = LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', cte.emp_code),2)))[IsLocked] from cte";
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'User Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Grade');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'IS Locked');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Active');
        
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E1')->setAutoSize(true);
        
        $result = query($query, $sql, $pa, $opt, $ms_db);
        
        $result1 = query($query, $sql1, $pa, $opt, $ms_db);
        
        
        
        $i = 2;
        while ($value = $fetch($result)) {
            $status = "";
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value['Emp_Code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value['user_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value['GRD_NAME']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, "");
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $status);
            $i++;
        }
        
        while ($value1 = $fetch($result1)) {
            $status = ($value1['UserActive'] == 1) ? "Active" : "InActive";
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value1['Emp_Code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value1['user_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value1['GRD_NAME']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $value1['IsLocked']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $status);
            $i++;
        }
        
        $my_excel_filename = "upload/NonActiveUserRecords.xls";
        if (file_exists($my_excel_filename))
            unlink('upload/NonActiveUserRecords.xls');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        $resultStatus['error']    = "";
        
        return json_encode($resultStatus);
        $excelOutput = ob_get_clean();
    }
    
    function generateActiveUserReport($objPHPExcel)
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $error = array();
        // echo $employee = "'".str_replace(",", "','", implode(",", $_POST['employee']))."'"; die;
        $whereCondition .= (!empty($_POST['employee'])) ? " AND Emp_Code in (" . "'" . str_replace(",", "','", implode(",", $_POST['employee'])) . "'" . ") " : "";
        /*$sql = "with cte as ( select Emp_Code,(Emp_FName + ' '+ Emp_MName+ ' '+ Emp_LName) as user_name,OEMailID,GRD_NAME from HrdMastqry where LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', emp_code),2)) not in (select distinct  userid from Users ) and Status_code='01' $whereCondition) 
        select * from cte";*/
        
        $sql1 = "with cte as ( select Emp_Code,(Emp_FName + ' '+ Emp_MName+ ' ' + Emp_LName) as user_name,GRD_NAME from HrdMastqry where LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', emp_code),2)) in (select distinct userid from Users where Users.IsLocked = 'N' and Users.UserActive = '1') $whereCondition)
        select *,(select top 1  UserActive from Users u where u.UserID = LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', cte.emp_code),2)))[UserActive],(select top 1 IsLocked from Users u where u.UserID = LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', cte.emp_code),2)))[IsLocked] from cte";
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'User Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'User Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Grade');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'IS Locked');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Active');
        
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D1')->setAutoSize(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E1')->setAutoSize(true);
        
        //$result = query($query, $sql, $pa, $opt, $ms_db);
        
        $result1 = query($query, $sql1, $pa, $opt, $ms_db);
        
        
        
        $i = 2;
        /* while ($value = $fetch($result)) {
        $status = "";
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value['Emp_Code']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value['user_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value['GRD_NAME']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, "");
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $status);
        $i++;
        }*/
        
        while ($value1 = $fetch($result1)) {
            $status = ($value1['UserActive'] == 1) ? "Active" : "InActive";
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $i, $value1['Emp_Code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, $value1['user_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, $value1['GRD_NAME']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, $value1['IsLocked']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, $status);
            $i++;
        }
        
        $my_excel_filename = "upload/ActiveUserRecords.xls";
        if (file_exists($my_excel_filename))
            unlink('upload/ActiveUserRecords.xls');
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $my_excel_filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save(str_replace(__FILE__, $my_excel_filename, __FILE__));
        $resultStatus['fileName'] = $my_excel_filename;
        $resultStatus['error']    = "";
        
        return json_encode($resultStatus);
        $excelOutput = ob_get_clean();
    }
    
    function getDayFinalStatus($row = array())
    {
        
        $total_late_allow = 2;
        $lastStatus       = '';
        
        if (!empty($row)) {
            $misPunch = False;
            
            if (isset($row['Intime']) && isset($row['Outtime'])) {
                if (((strtotime($row['Intime']) <= 0) && (strtotime($row['Outtime']) > 0)) || ((strtotime($row['Intime']) > 0) && (strtotime($row['Outtime']) <= 0))) {
                    $misPunch = true;
                    /*if($row['Emp_code'] == '60721'){
                    echo "   Miss";
                    }*/
                } else {
                    $misPunch = False;
                    /*if($row['Emp_code'] == '60721'){
                    echo "   NOT Miss";
                    }*/
                }
            } else {
                /*if($row['Emp_code'] == '60721'){
                echo "   NOT Miss";
                }	*/
                $misPunch = False;
            }
            /*if($row['Emp_code'] == '60721'){
            echo strtotime($row['Intime'])."    END    ";
            echo strtotime($row['Outtime']);
            die;
            }  */
            if ((trim($row['final_status']) == 'Leave') && (trim($row['statustitle']) != 'Half Day') && (trim($row['status']) != 'F') && (trim($row['status']) != 'W') && ((trim($row['Leavestatus']) == 'Approved') || (trim($row['Leavestatus']) == 'Approved Cancel - Request for cancel'))) {
                if ($row['LeaveTypeText'] != '') {
                    $parsed = $this->get_string_between($row['LeaveTypeText'], '(', ')');
                    if (trim($parsed) == '') {
                        $parsed = $row['LeaveTypeText'];
                    }
                }
                $lastStatus = $parsed;
            } else if (trim($row['final_status']) == 'Weekly Off') {
                $lastStatus = 'WO';
            } else if (trim($row['final_status']) == 'POH') {
                $lastStatus = 'POH';
            } else if (trim($row['final_status']) == 'POW') {
                $lastStatus = 'POW';
            } else if (trim($row['final_status']) == 'Holiday' || trim($row['status']) == 'F') {
                $lastStatus = 'HLD';
            } else if (trim($row['final_status']) == 'Miss Punch' && ($misPunch)) {
                $lastStatus = 'MIS';
            } else if ((trim($row['final_status']) == 'Attendance Regularised') && (trim($row['markPastStatus']) == 'Approved')) {
                $lastStatus = 'C-P';
            } else if ((trim($row['final_status']) == 'OD Request') && (trim($row['odStatus']) == 'Approved')) {
                $lastStatus = 'OD';
            } else if ((trim($row['final_status']) == 'Leave') && (trim($row['statustitle']) == 'Half Day')) {
                $parsed = $row['LeaveTypeText'];
                if ($row['LeaveTypeText'] != '') {
                    $parsed = $this->get_string_between(trim($row['LeaveTypeText']), '(', ')');
                    if (trim($parsed) == '') {
                        $parsed = $row['LeaveTypeText'];
                    }
                }
                //echo	'H_'.$parsed; die;
                
                $lastStatus = 'H_' . $parsed;
                if (($row['Lateflag'] == 1 || $row['Earlyflag'] == 1)) {
                    $this->total_late_count++;
                    if ($this->total_late_count > $total_late_allow) {
                        $lastStatus = 'H~~1';
                    } else {
                        $lastStatus = 'H_' . $parsed . '~~1';
                    }
                }
            } else if (trim($row['status']) == '') {
                $lastStatus = 'A';
            } else {
                if (trim($row['status']) == 'W') {
                    $lastStatus = "WO";
                } else {
                    $lastStatus = $row['status'];
                }
                /*if(($row['Lateflag'] == 1 || $row['Earlyflag'] == 1)) {
                $this->total_late_count++;
                if($this->total_late_count > $total_late_allow) {
                $lastStatus  =  "H~~1";
                } else {
                $lastStatus = $row['status']."~~1";
                }
                }    */
            }
        }
        
        return $lastStatus;
    }
    
    function checkLeave($row)
    {
        $leaveStatus = '';
        if (($row['final_status'] == 'Leave') && ($row['statustitle'] != 'Half Day') && ($row['status'] != 'F') && ($row['final_status'] != 'Weekly Off') && ($row['final_status'] != 'POH') && ($row['final_status'] != 'POW') && ($row['status'] != 'W') && ($row['final_status'] != 'Holiday' || $row['status'] != 'F') && (($row['Leavestatus'] == 'Approved') || ($row['Leavestatus'] == 'Approved Cancel - Request for cancel'))) {
            if ($row['LeaveTypeText'] != '') {
                $parsed      = $this->get_string_between($row['LeaveTypeText'], '(', ')');
                $leaveStatus = 'L';
            }
            $leaveStatus = 'L';
        } else if (($row['final_status'] == 'Leave') && ($row['statustitle'] == 'Half Day') && (($row['Leavestatus'] == 'Approved') || ($row['Leavestatus'] == 'Approved Cancel - Request for cancel')) && ($row['status'] != 'F') && ($row['final_status'] != 'Weekly Off') && ($row['final_status'] != 'POH') && ($row['final_status'] != 'POW') && ($row['status'] != 'W') && ($row['final_status'] != 'Holiday' || $row['status'] != 'F')) {
            $leaveStatus = 'H';
        } else if ($row['final_status'] == 'Leave & Punch' && (($row['Leavestatus'] == 'Approved') || ($row['Leavestatus'] == 'Approved Cancel - Request for cancel')) && ($row['status'] != 'F') && ($row['final_status'] != 'Weekly Off') && ($row['final_status'] != 'POH') && ($row['final_status'] != 'POW') && ($row['status'] != 'W') && ($row['final_status'] != 'Holiday' || $row['status'] != 'F')) {
            $leaveStatus = 'PH';
        }
        return $leaveStatus;
    }
    function addTimeCalculation($firstTime, $secondTime)
    {
        if (trim($secondTime) == '0') {
            $secondTime = "0:0";
        }
        if (trim($firstTime) == '0') {
            $firstTime = "0:0";
        }
        $test  = explode(":", $firstTime);
        $test1 = explode(":", $secondTime);
        
        $minutes = $test[1] + $test1[1];
        $hour    = $test[0] + $test1[0];
        if ($minutes >= 60) {
            $minutes = sprintf("%02d", $minutes - 60);
            $hour    = sprintf("%02d", $hour + 1);
        }
        
        return $hour . ":" . $minutes;
    }
    
}
?>