<?php
include '../../db_conn.php';

include ('../../configdata.php');
include('../class/customReport.php');
require_once '../../PHPExcel/Classes/PHPExcel.php';
require_once '../../PHPExcel/Classes/PHPExcel/IOFactory.php';

$custom_class_obj=new customReport_class();
$objPHPExcel = new PHPExcel();
ini_set('max_execution_time',0);
 ini_set("output_buffering", "On");
ini_set('memory_limit', '-1');
set_time_limit(0);
if(isset($_POST['type']) && !empty($_POST['type'])){
	if($_POST['type']=='16'){
		echo $whereConditionField	=	$custom_class_obj->generateHeadCountReport($objPHPExcel);  
	}
	else if($_POST['type']=='18'){ 
		echo $whereConditionField	=	$custom_class_obj->generateAttendancepunchReport($objPHPExcel);  
	}
	else if($_POST['type']=='17'){
		echo $whereConditionField	=	$custom_class_obj->generateViewRosterReport($objPHPExcel);  
	} elseif($_POST['type']=='22') {
		echo $whereConditionField	=	$custom_class_obj->generateActiveUserReport($objPHPExcel);  
	} 
	elseif($_POST['type']=='23') {
		echo $whereConditionField	=	$custom_class_obj->generatenonActiveUserReport($objPHPExcel);  
	}
	else{
		echo $whereConditionField	=	$custom_class_obj->generateCustomReport($objPHPExcel);  

	}	
	//This is use for LEave Transaction Report
}

?>