<?php
session_start();
//ini_set('MAX_EXECUTION_TIME', -1);
//ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);
 //ini_set('max_execution_time', 3000);
 ini_set("memory_limit", "-1");
 //ini_set('memory_limit','96550M');
 ini_set("output_buffering", "On");
ini_set("upload_max_filesize", "20M");
ini_set("post_max_size", "21M");

include ('../../db_conn.php');
include ('../../configdata.php');
require_once '../../PHPExcel/Classes/PHPExcel.php';
require_once '../../PHPExcel/Classes/PHPExcel/IOFactory.php';
$sql="Select * from iwbsch where flag='0'";
$result=query($query,$sql,$pa,$opt,$ms_db);
while ($row=$fetch($result))
{
  $Emp_Code=$row['Emp_Code'];
  $id=$row['Id'];
  $Emp_email=$row['Emp_email'];
  $type=$row['type'];
  $business=$row['business'];
  $sbusiness=$row['sbusiness'];
  $process=$row['process'];
  $divi=$row['divi'];
  $grd=$row['grd'];
  $startdate=$row['startdate'];
  $enddate=$row['enddate'];
  if($type==1)
  {
    getIWBreportHeadCount($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id);
  }
  elseif ($type==2) 
  {
      getIWBreportAttendanceHours($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id);  
  }
  elseif ($type==3) 
  {
      getIWBreportSummaryAttendanceHours($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id);  
  }
  elseif ($type==4) 
  {
      getIWBreportSummary($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id);  
  }

}


/*********************** IWB Report Function For Head Count *************************/

function getIWBreportHeadCount($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id)
{
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $objPHPExcel = new PHPExcel();
      $start = strtotime($startdate);
    $end = strtotime($enddate);
    $days_between = ceil(abs($end - $start) / 86400);
    $getPresentHourscount=0;
    $getSlHourscount=0;
    $getLeaveHourscont=0;
    $getabsentHourscount=0;
    $getSLperHourscount=0;
    $getleaveperHourscount=0;
    $getabsentperHourscount=0;
    $data = array();
    $sac1=array();
    $sac2=array();
    $sac3=array();
    $tem=array();
    $count1=0;
    $sql="select convert(varchar(10), DutyDate, 126) as DutyDate,PrsHrs,LEV,lvtype,DSTATUS,GRD_CODE,DIVI_CODE,BUSS_CODE,SUBBuss_Code,PROC_CODE from DayStatusQry where convert(varchar(10), DutyDate, 126) BETWEEN '$startdate' AND '$enddate' and GRD_CODE='1'";
    $result = query($query,$sql,$pa,$opt,$ms_db);
    $i=1;
    $PrsHrs=array();
    $ptotal=0;
    $DutyDate=array();
    $LEV=array();
    $lvtype=array();
    $DSTATUS=array();
    $dataPoints=array();
    while ($row=$fetch($result)) 
    {
        $dataPoints[] = array("DutyDate"=>$row ['DutyDate'],"PrsHrs"=>$row['PrsHrs'],"LEV"=>$row ['LEV'],"lvtype"=>$row ['lvtype'],"DSTATUS"=>$row ['DSTATUS'],"GRD_CODE"=>$row ['GRD_CODE'],"DIVI_CODE"=>$row ['DIVI_CODE'],"BUSS_CODE"=>$row ['BUSS_CODE'],"SUBBUSS_CODE"=>$row ['SUBBuss_Code'],"PROC_CODE"=>$row ['PROC_CODE']);
       
    }
    $arrsize=sizeof($dataPoints);
    $sbusinessarr = explode(",",$sbusiness);
    $businessarr = explode(",",$business);
    $processarr = explode(",",$process);
    $diviarr = explode(",",$divi);
    $grdarr = explode(",",$grd);
    $countsbusiness=sizeof($sbusinessarr);
    $countbusiness=sizeof($businessarr);
    $countgrd=sizeof($grdarr);
    $countdiviarr=sizeof($diviarr);
    $countprocessarr=sizeof($processarr);
    //$objPHPExcel->createSheet();
    $styleArray = array(
    'font'  => array(
        'color' => array('rgb' => 'FFFFFF'),
    ));
    //print_r($sbusinessarr);
 
    for ($i=0; $i < sizeof($sbusinessarr); $i++) 
    { 
         $c=1;
        $sql_sbuss="Select subBussName from subBussMast where subBussid='$sbusinessarr[$i]'";
        $result_sbuss=query($query,$sql_sbuss,$pa,$opt,$ms_db);
        $row_sbuss=$fetch($result_sbuss);
        $sbussname=$row_sbuss['subBussName'];
        //$objPHPExcel->setActiveSheetIndex(1);
        if($i!=0)
        {
        $objPHPExcel->createSheet();
        }
        $objPHPExcel->setActiveSheetIndex($i);
        //$objPHPExcel->setActiveSheetIndex(3);
        $objPHPExcel->getActiveSheet()->setTitle('Att Data For '.$sbussname);
        for ($j=0; $j < sizeof($businessarr); $j++) 
        {
           // $temp=$countdiviarr+$countdiviarr*($countgrd+1)+5;
             // $c=1;
          $c=$c+$j;
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$c.':AG'.$c);
          //$objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$c)->setValue($sbussname.' Attendance Head Count-'.$bussname);

            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('F28A8C');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $c+=1;
            $c=$c+1;
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#008000');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getColumnDimension(A)->setWidth(15);
          
            $sql_buss="Select BussName from BussMast where BUSSID='$businessarr[$j]'";
            $result_buss=query($query,$sql_buss,$pa,$opt,$ms_db);
            $row_buss=$fetch($result_buss);
            $bussname=$row_buss['BussName'];
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$c, 'Date');
            $e=1;
             for($m=0;$m<=$days_between;$m++)
              {
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($e, $c, $e);
                  $e++;
              }
            /*$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $c, '1');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $c, '2');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $c, '3');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $c, '4');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $c, '5');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $c, '6');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $c, '7');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $c, '8');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $c, '9');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $c, '10');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $c, '11');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $c, '12');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $c, '13');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $c, '14');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $c, '15');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $c, '16');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $c, '17');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $c, '18');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $c, '19');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $c, '20');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $c, '21');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $c, '22');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $c, '23');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24, $c, '24');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25, $c, '25');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26, $c, '26');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27, $c, '27');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28, $c, '28');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29, $c, '29');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(30, $c, '30');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31, $c, '31');*/
            for ($k=0; $k < sizeof($processarr); $k++) 
              {
                  $c=$c+1;
                    $sql_pro="Select PROC_NAME from PROCMAST where PROCID='$processarr[$k]'";
                  $result_pro=query($query,$sql_pro,$pa,$opt,$ms_db);
                  $row_pro=$fetch($result_pro);
                  $proname=$row_pro['PROC_NAME'];
                  
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$proname);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('483D8B');
                 for ($q=0; $q < sizeof($diviarr); $q++) 
                  {
                   	
                  $sql_divi="Select DIVI_NAME from DiviMast where DIVIID='$diviarr[$q]' and PID='$processarr[$k]'";
                  $result_divi=query($query,$sql_divi,$pa,$opt,$ms_db);
                  $divicount=$num($result_divi);
                  if($divicount > 0)
                  {
                    $c=$c+1;
                  $row_divi=$fetch($result_divi);
                  $diviname=$row_divi['DIVI_NAME'];
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$diviname);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#008000');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->applyFromArray($styleArray);
                    for ($l=0; $l < sizeof($grdarr); $l++) 
                    {
                    $c=$c+1;
                    $sql_grd="Select GRD_NAME from GrdMast where GRDID='$grdarr[$l]'";
                    $result_grd=query($query,$sql_grd,$pa,$opt,$ms_db);
                    $row_grd=$fetch($result_grd);
                    $grdname=$row_grd['GRD_NAME'];
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$grdname);
                   // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  for($m=0;$m<=$days_between;$m++)
                     {
                  $day1=date("Y-m-d", strtotime($startdate . ' + ' . $m . 'day'));
                  $getheadcountpresent=getheadcountpresent($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$diviarr[$q],$grdarr[$l],$processarr[$k]);
                  if($m==0){$mm=$m+1;}else{$mm=$m+1;}
                  
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($mm,$c,$getheadcountpresent);
                 
                  //$count1=$count1+1;
                   }
                    }
                  }
                  }
                  $c=$c+1;
                   $mmm=1;
                   $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($K,$c,'Total');
                   $getheadcountpresentprocess=0;
                  for($m=0;$m<=$days_between;$m++)
                     {
                        $day1=date("Y-m-d", strtotime($startdate . ' + ' . $m . 'day'));
                        $getheadcountpresentprocess=getheadcountpresentprocess($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$processarr[$k],$diviarr,$grdarr);
                        if($m==0){$mmm=$m+1;}else{$mmm=$m+1;}
                  
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($mmm,$c,$getheadcountpresentprocess);
                        
                  //$count1=$count1+1;
                      }
                      $c=$c+1;
              }
              $c=$c+3;
        }
    }
$my_excel_filename = "upload/".md5(session_id().microtime(TRUE)).".xls";
sendemail($my_excel_filename,$Emp_Code,$Emp_email,$id);  
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="upload/name_of_file.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace(__FILE__,$my_excel_filename,__FILE__));
$objWriter->save('php://upload');
$excelOutput = ob_get_clean();
 exit();
}

function getheadcountpresent($arr,$p,$arrsize,$sbusiness,$business,$divi,$grd,$proces)
{
		global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DutyDate"]==$p && $arr[$row]["DSTATUS"]=='A' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["DIVI_CODE"]==$divi && $arr[$row]["GRD_CODE"]==$grd && $arr[$row]["PROC_CODE"]==$proces)
            {
                $val+=1;
            }
        }
        return $val;
}

function getheadcountpresentprocess($arr,$p,$arrsize,$sbusiness,$business,$proces,$diviarr,$grdarr)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DutyDate"]==$p && $arr[$row]["DSTATUS"]=='A' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["PROC_CODE"]==$proces && in_array($arr[$row]["GRD_CODE"], $grdarr) && in_array($arr[$row]["DIVI_CODE"], $diviarr))
            {
                $val+=1;
            }
        }
        return $val;
}
/*********************** End IWB Report Function For Head Count *************************/

/*********************** IWB Report Function For Head Count *************************/

function getIWBreportAttendanceHours($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id)
{
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $objPHPExcel = new PHPExcel();
      $start = strtotime($startdate);
    $end = strtotime($enddate);
    $days_between = ceil(abs($end - $start) / 86400);
    $getPresentHourscount=0;
    $getSlHourscount=0;
    $getLeaveHourscont=0;
    $getabsentHourscount=0;
    $getSLperHourscount=0;
    $getleaveperHourscount=0;
    $getabsentperHourscount=0;
    $data = array();
    $sac1=array();
    $sac2=array();
    $sac3=array();
    $tem=array();
    $count1=0;
    $sql="select convert(varchar(10), DutyDate, 126) as DutyDate,PrsHrs,LEV,lvtype,DSTATUS,GRD_CODE,DIVI_CODE,BUSS_CODE,SUBBuss_Code,PROC_CODE from DayStatusQry where convert(varchar(10), DutyDate, 126) BETWEEN '$startdate' AND '$enddate' and GRD_CODE IN($grd)";
    $result = query($query,$sql,$pa,$opt,$ms_db);
    $i=1;
    $PrsHrs=array();
    $ptotal=0;
    $DutyDate=array();
    $LEV=array();
    $lvtype=array();
    $DSTATUS=array();
    $dataPoints=array();
    while ($row=$fetch($result)) 
    {
        $dataPoints[] = array("DutyDate"=>$row ['DutyDate'],"PrsHrs"=>$row['PrsHrs'],"LEV"=>$row ['LEV'],"lvtype"=>$row ['lvtype'],"DSTATUS"=>$row ['DSTATUS'],"GRD_CODE"=>$row ['GRD_CODE'],"DIVI_CODE"=>$row ['DIVI_CODE'],"BUSS_CODE"=>$row ['BUSS_CODE'],"SUBBUSS_CODE"=>$row ['SUBBuss_Code'],"PROC_CODE"=>$row ['PROC_CODE']);
       
    }
    $arrsize=sizeof($dataPoints);
    $sbusinessarr = explode(",",$sbusiness);
    $businessarr = explode(",",$business);
    $processarr = explode(",",$process);
    $diviarr = explode(",",$divi);
    $grdarr = explode(",",$grd);
    $countsbusiness=sizeof($sbusinessarr);
    $countbusiness=sizeof($businessarr);
    $countgrd=sizeof($grdarr);
    $countdiviarr=sizeof($diviarr);
    $countprocessarr=sizeof($processarr);
    //$objPHPExcel->createSheet();
    $styleArray = array(
    'font'  => array(
        'color' => array('rgb' => 'FFFFFF'),
    ));
    //print_r($sbusinessarr);
 
    for ($i=0; $i < sizeof($sbusinessarr); $i++) 
    { 
         $c=1;
        $sql_sbuss="Select subBussName from subBussMast where subBussid='$sbusinessarr[$i]'";
        $result_sbuss=query($query,$sql_sbuss,$pa,$opt,$ms_db);
        $row_sbuss=$fetch($result_sbuss);
        $sbussname=$row_sbuss['subBussName'];
        //$objPHPExcel->setActiveSheetIndex(1);
        if($i!=0)
        {
        $objPHPExcel->createSheet();
        }
        $objPHPExcel->setActiveSheetIndex($i);
        //$objPHPExcel->setActiveSheetIndex(3);
        $objPHPExcel->getActiveSheet()->setTitle('Att Data For '.$sbussname);
        for ($j=0; $j < sizeof($businessarr); $j++) 
        {
           // $temp=$countdiviarr+$countdiviarr*($countgrd+1)+5;
             // $c=1;
          $c=$c+$j;
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$c.':AG'.$c);
          //$objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$c)->setValue($sbussname.' Attendance Head Count-'.$bussname);

            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('F28A8C');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $c+=1;
            $c=$c+1;
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#008000');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getColumnDimension(A)->setWidth(15);
          
            $sql_buss="Select BussName from BussMast where BUSSID='$businessarr[$j]'";
            $result_buss=query($query,$sql_buss,$pa,$opt,$ms_db);
            $row_buss=$fetch($result_buss);
            $bussname=$row_buss['BussName'];
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$c, 'Date');
             $e=1;
             for($m=0;$m<=$days_between;$m++)
              {
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($e, $c, $e);
                  $e++;
              }
            /*$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $c, '1');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $c, '2');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $c, '3');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $c, '4');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $c, '5');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $c, '6');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $c, '7');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $c, '8');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $c, '9');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $c, '10');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $c, '11');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $c, '12');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $c, '13');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $c, '14');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $c, '15');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $c, '16');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $c, '17');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $c, '18');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $c, '19');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $c, '20');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $c, '21');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $c, '22');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $c, '23');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24, $c, '24');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25, $c, '25');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26, $c, '26');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27, $c, '27');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28, $c, '28');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29, $c, '29');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(30, $c, '30');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31, $c, '31');*/
            for ($k=0; $k < sizeof($processarr); $k++) 
              {
                  $c=$c+1;
                    $sql_pro="Select PROC_NAME from PROCMAST where PROCID='$processarr[$k]'";
                  $result_pro=query($query,$sql_pro,$pa,$opt,$ms_db);
                  $row_pro=$fetch($result_pro);
                  $proname=$row_pro['PROC_NAME'];
                  
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$proname);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('483D8B');
                 for ($q=0; $q < sizeof($diviarr); $q++) 
                  {
                    
                  $sql_divi="Select DIVI_NAME from DiviMast where DIVIID='$diviarr[$q]' and PID='$processarr[$k]'";
                  $result_divi=query($query,$sql_divi,$pa,$opt,$ms_db);
                  $divicount=$num($result_divi);
                  if($divicount > 0)
                  {
                    $c=$c+1;
                  $row_divi=$fetch($result_divi);
                  $diviname=$row_divi['DIVI_NAME'];
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$diviname);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#008000');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->applyFromArray($styleArray);
                    for ($l=0; $l < sizeof($grdarr); $l++) 
                    {
                    $c=$c+1;
                    $sql_grd="Select GRD_NAME from GrdMast where GRDID='$grdarr[$l]'";
                    $result_grd=query($query,$sql_grd,$pa,$opt,$ms_db);
                    $row_grd=$fetch($result_grd);
                    $grdname=$row_grd['GRD_NAME'];
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$grdname);
                   // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  for($m=0;$m<=$days_between;$m++)
                     {
                  $day1=date("Y-m-d", strtotime($startdate . ' + ' . $m . 'day'));
                  $getheadcountpresent=getheadcountpresenthours($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$diviarr[$q],$grdarr[$l],$processarr[$k]);
                  if($m==0){$mm=$m+1;}else{$mm=$m+1;}
                  
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($mm,$c,$getheadcountpresent);
                 
                  //$count1=$count1+1;
                   }
                    }
                  }
                  }
                  $c=$c+1;
                   $mmm=1;
                   $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($K,$c,'Total');
                   $getheadcountpresentprocess=0;
                  for($m=0;$m<=$days_between;$m++)
                     {
                        $day1=date("Y-m-d", strtotime($startdate . ' + ' . $m . 'day'));
                        $getheadcountpresentprocess=getheadcountpresentprocesshours($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$processarr[$k],$diviarr,$grdarr);
                        if($m==0){$mmm=$m+1;}else{$mmm=$m+1;}
                  
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($mmm,$c,$getheadcountpresentprocess);
                        
                  //$count1=$count1+1;
                      }
                      $c=$c+1;
              }
              $c=$c+3;
        }
    }
$my_excel_filename = "upload/".md5(session_id().microtime(TRUE)).".xls";
sendemail($my_excel_filename,$Emp_Code,$Emp_email,$id);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="upload/name_of_file.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace(__FILE__,$my_excel_filename,__FILE__));
$objWriter->save('php://upload');
$excelOutput = ob_get_clean();
exit();
}

function getheadcountpresenthours($arr,$p,$arrsize,$sbusiness,$business,$divi,$grd,$proces)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DutyDate"]==$p && $arr[$row]["DSTATUS"]=='A' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["DIVI_CODE"]==$divi && $arr[$row]["GRD_CODE"]==$grd && $arr[$row]["PROC_CODE"]==$proces)
            {
                  $val+=$arr[$row]["PrsHrs"];
            }
        }
        return floor($val/60);
}

function getheadcountpresentprocesshours($arr,$p,$arrsize,$sbusiness,$business,$proces,$diviarr,$grdarr)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DutyDate"]==$p && $arr[$row]["DSTATUS"]=='A' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["PROC_CODE"]==$proces && in_array($arr[$row]["GRD_CODE"], $grdarr) && in_array($arr[$row]["DIVI_CODE"], $diviarr))
            {
                 $val+=$arr[$row]["PrsHrs"];
            }
        }
        return floor($val/60);
}
/*********************** End IWB Report Function For Head Count *************************/
/*********************** IWB Report Summary Attendance Hours *************************/

function getIWBreportSummaryAttendanceHours($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id)
{
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $objPHPExcel = new PHPExcel();
      $start = strtotime($startdate);
    $end = strtotime($enddate);
    $days_between = ceil(abs($end - $start) / 86400);
    $getPresentHourscount=0;
    $getSlHourscount=0;
    $getLeaveHourscont=0;
    $getabsentHourscount=0;
    $getSLperHourscount=0;
    $getleaveperHourscount=0;
    $getabsentperHourscount=0;
    $data = array();
   
    $count1=0;
    $sql="select convert(varchar(10), DutyDate, 126) as DutyDate,PrsHrs,LEV,lvtype,DSTATUS,GRD_CODE,DIVI_CODE,BUSS_CODE,SUBBuss_Code,PROC_CODE from DayStatusQry where convert(varchar(10), DutyDate, 126) BETWEEN '$startdate' AND '$enddate' and GRD_CODE IN($grd)";
    $result = query($query,$sql,$pa,$opt,$ms_db);
    $i=1;
    $PrsHrs=array();
    $ptotal=0;
    $DutyDate=array();
    $LEV=array();
    $lvtype=array();
    $DSTATUS=array();
    $dataPoints=array();
    while ($row=$fetch($result)) 
    {
        $dataPoints[] = array("DutyDate"=>$row ['DutyDate'],"PrsHrs"=>$row['PrsHrs'],"LEV"=>$row ['LEV'],"lvtype"=>$row ['lvtype'],"DSTATUS"=>$row ['DSTATUS'],"GRD_CODE"=>$row ['GRD_CODE'],"DIVI_CODE"=>$row ['DIVI_CODE'],"BUSS_CODE"=>$row ['BUSS_CODE'],"SUBBUSS_CODE"=>$row ['SUBBuss_Code'],"PROC_CODE"=>$row ['PROC_CODE']);
       
    }
    $arrsize=sizeof($dataPoints);
    $sbusinessarr = explode(",",$sbusiness);
    $businessarr = explode(",",$business);
    $processarr = explode(",",$process);
    $diviarr = explode(",",$divi);
    $grdarr = explode(",",$grd);
    $countsbusiness=sizeof($sbusinessarr);
    $countbusiness=sizeof($businessarr);
    $countgrd=sizeof($grdarr);
    $countdiviarr=sizeof($diviarr);
    $countprocessarr=sizeof($processarr);
    //$objPHPExcel->createSheet();
    $styleArray = array(
    'font'  => array(
        'color' => array('rgb' => 'FFFFFF'),
    ));
    //print_r($sbusinessarr);
 
    for ($i=0; $i < sizeof($sbusinessarr); $i++) 
    { 
         $c=1;
        $sql_sbuss="Select subBussName from subBussMast where subBussid='$sbusinessarr[$i]'";
        $result_sbuss=query($query,$sql_sbuss,$pa,$opt,$ms_db);
        $row_sbuss=$fetch($result_sbuss);
        $sbussname=$row_sbuss['subBussName'];
        //$objPHPExcel->setActiveSheetIndex(1);
        if($i!=0)
        {
        $objPHPExcel->createSheet();
        }
        $objPHPExcel->setActiveSheetIndex($i);
        //$objPHPExcel->setActiveSheetIndex(3);
        $objPHPExcel->getActiveSheet()->setTitle('Att Data For '.$sbussname);
        for ($j=0; $j < sizeof($businessarr); $j++) 
        {
           // $temp=$countdiviarr+$countdiviarr*($countgrd+1)+5;
             // $c=1;
          $c=$c+$j;
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$c.':AG'.$c);
          //$objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$c)->setValue($sbussname.' Attendance Head Count-'.$bussname);

            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('F28A8C');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $c+=1;
            //$c=$c+1;
            
            $objPHPExcel->getActiveSheet()->getColumnDimension(A)->setWidth(15);
          
            $sql_buss="Select BussName from BussMast where BUSSID='$businessarr[$j]'";
            $result_buss=query($query,$sql_buss,$pa,$opt,$ms_db);
            $row_buss=$fetch($result_buss);
            $bussname=$row_buss['BussName'];
            
            
            for ($k=0; $k < sizeof($processarr); $k++) 
              {
                    $sql_pro="Select PROC_NAME from PROCMAST where PROCID='$processarr[$k]'";
                  $result_pro=query($query,$sql_pro,$pa,$opt,$ms_db);
                  $row_pro=$fetch($result_pro);
                  $proname=$row_pro['PROC_NAME'];
                  
                  // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$proname);
                  // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('483D8B');
                  for ($l=0; $l < sizeof($grdarr); $l++) 
                    {
                    $c=$c+1;
                    $sql_grd="Select GRD_NAME from GrdMast where GRDID='$grdarr[$l]'";
                    $result_grd=query($query,$sql_grd,$pa,$opt,$ms_db);
                    $row_grd=$fetch($result_grd);
                    $grdname=$row_grd['GRD_NAME'];
                     $objPHPExcel->getActiveSheet()->mergeCells('A'.$c.':AG'.$c);
                    //$objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getCell('A'.$c)->setValue($proname.' Attendance Head Count-'.$grdname);
                   // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('F28A8C');
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$grdname);
                    $c=$c+1;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$c, 'Date');
                     $e=1;
                   for($m=0;$m<=$days_between;$m++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($e, $c, $e);
                        $e++;
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($e, $c, 'Total');
                    /*$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $c, '1');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $c, '2');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $c, '3');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $c, '4');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $c, '5');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $c, '6');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $c, '7');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $c, '8');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $c, '9');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $c, '10');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $c, '11');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $c, '12');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $c, '13');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $c, '14');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $c, '15');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $c, '16');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $c, '17');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $c, '18');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $c, '19');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $c, '20');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $c, '21');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $c, '22');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $c, '23');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24, $c, '24');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25, $c, '25');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26, $c, '26');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27, $c, '27');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28, $c, '28');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29, $c, '29');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(30, $c, '30');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31, $c, '31');*/
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#008000');
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->applyFromArray($styleArray);
                    $tem=0;
                    unset($sac1);
                    unset($sac2);
                    unset($sac3);
                    unset($sac4);
                    unset($sac5);
                    unset($sac6);
                    unset($sac7);
                    $sac1=array();
                    $sac2=array();
                    $sac3=array();
                    $sac4=array();
                    $sac5=array();
                    $sac6=array();
                    $sac7=array();
                    $tem=array();
                    $count1=0;
                    $getPresentHourscount=0;
                    $getSlHourscount=0;
                    $getLeaveHourscont=0;
                    $getabsentHourscount=0;
                    $getSLperHourscount=0;
                    $getleaveperHourscount=0;
                    $getabsentperHourscount=0;
                    for($q=0;$q <= $days_between;$q++)
                     {
                          $getPresentHours=0;
                          $getSlHours=0;
                          $getLeaveHours=0;
                          $day1=date("Y-m-d", strtotime($startdate . ' + ' . $q . 'day'));
                          $getPresentHours=getPresentHours($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$grdarr[$l],$processarr[$k]);
                          $getPresentHourscount+=$getPresentHours; 
                          $getSlHours=getSlHours($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$grdarr[$l],$processarr[$k]);
                          $getSlHourscount+=$getSlHours;
                          $getLeaveHours=getLeaveHours($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$grdarr[$l],$processarr[$k]);
                          $getLeaveHourscont+=$getLeaveHours;
                          $getabsentHours=getabsentHours($getSlHours,$getLeaveHours);
                          $getabsentHourscount+=$getabsentHours;
                          $getSLperHours=getSLperHours($getSlHours,$getPresentHours,$getLeaveHours);
                          $getSLperHourscount+=$getSLperHours;
                          $getleaveperHours=getSLperHours($getLeaveHours,$getPresentHours,$getSlHours);
                          $getleaveperHourscount+=$getleaveperHours;
                          $getabsentperHours=gettotalperHours($getabsentHours,$getPresentHours,$getSlHours,$getLeaveHours);
                          $getabsentperHourscount+=$getabsentperHours;
                          $sac1[$q]=$getPresentHours;
                          $sac2[$q]=$getSlHours;
                          $sac3[$q]=$getLeaveHours;
                          $sac4[$q]=$getabsentHours;
                          $sac5[$q]=$getSLperHours."%";
                          $sac6[$q]=$getleaveperHours."%";
                          $sac7[$q]=$getabsentperHours."%";
                          $count1=$count1+1;
                     }
                    $tem=$count1+1;
                    $sac1[]=$getPresentHourscount;
                    $sac2[]=$getSlHourscount;
                    $sac3[]=$getLeaveHourscont;
                    $sac4[]=$getabsentHourscount;
                    $sac5[]="";
                    $sac6[]="";
                    $sac7[]="";
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'Present Hrs'.$tem."qq".$count1);
                    $ii=1;
                    for($h=0;$h <$tem;$h++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac1[$h]);
                        $ii++;                 
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'SL Hrs');
                     $ii=1;
                    for($h=0;$h <$tem;$h++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac2[$h]);
                        $ii++;                 
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'CL/EL/Absent Hrs');
                    $ii=1;
                    for($h=0;$h <$tem;$h++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac3[$h]);
                        $ii++;                 
                    }
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'Total Absent');
                     $ii=1;
                    for($h=0;$h <$tem;$h++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac4[$h]);
                        $ii++;                 
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'SL Absent %');
                    $ii=1;
                    for($h=0;$h <$tem;$h++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac5[$h]);
                        $ii++;                 
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'CL/EL Absent');
                    $ii=1;
                    for($h=0;$h <$tem;$h++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac6[$h]);
                        $ii++;                 
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'Total Absent');
                    $ii=1;
                    for($h=0;$h <$tem;$h++)
                    {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac7[$h]);
                        $ii++;                 
                    }
                   // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  /*for($m=0;$m<=$days_between;$m++)
                     {
                  $day1=date("Y-m-d", strtotime($startdate . ' + ' . $m . 'day'));
                  $getheadcountpresent=getheadcountpresenthours($dataPoints,$day1,$arrsize,$sbusinessarr[$i],$businessarr[$j],$diviarr[$q],$grdarr[$l],$processarr[$k]);
                  if($m==0){$mm=$m+1;}else{$mm=$m+1;}
                  
                  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($mm,$c,$getheadcountpresent);
                 
                  //$count1=$count1+1;
                   }*/
                    }
                  
              }
              $c=$c+3;
        }
    }
$my_excel_filename = "upload/".md5(session_id().microtime(TRUE)).".xls";
sendemail($my_excel_filename,$Emp_Code,$Emp_email,$id);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="upload/name_of_file.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace(__FILE__,$my_excel_filename,__FILE__));
$objWriter->save('php://upload');
$excelOutput = ob_get_clean();
exit();
}

function getPresentHours($arr,$p,$arrsize,$sbusiness,$business,$grd,$proces)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DutyDate"]==$p && $arr[$row]["DSTATUS"]=='P' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["GRD_CODE"]==$grd && $arr[$row]["PROC_CODE"]==$proces)
            {
                  $val+=$arr[$row]["PrsHrs"];
            }
        }
        return floor($val/60);
}
function getSlHours($arr,$p,$arrsize,$sbusiness,$business,$grd,$proces)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DutyDate"]==$p && $arr[$row]["lvtype"]=='3' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["GRD_CODE"]==$grd && $arr[$row]["PROC_CODE"]==$proces)
            {
                  $val+=$arr[$row]["LEV"];
            }
        }
        return floor($val/60);
}
function getLeaveHours($arr,$p,$arrsize,$sbusiness,$business,$grd,$proces)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DutyDate"]==$p && $arr[$row]["lvtype"]!='3' && $arr[$row]["DSTATUS"]=='A' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["GRD_CODE"]==$grd && $arr[$row]["PROC_CODE"]==$proces)
            {
                  $val+=$arr[$row]["PrsHrs"];
            }
        }
        return floor($val/60);
}
 function getabsentHours($arr1,$arr2)
    {
       $temp=0;
       
        $temp=$arr1+$arr2;


        return $temp;

   // echo $sql;
    }
 function getSLperHours($sl,$p,$l)
    {
       $temp=0;
       
        $temp=$sl/($p+$sl+$l)*100;


        return round($temp,2);

   // echo $sql;
    }
     function gettotalperHours($total,$p,$sl,$l)
    {
       $temp=0;
       
        $temp=$total/($p+$sl+$l)*100;


        return round($temp,2);

   // echo $sql;
    }
/*********************** End IWB Summary Attendance Hours *************************/

/*********************** IWB Summary Attendance Hours *************************/

function getIWBreportSummary($Emp_Code,$Emp_email,$type,$business,$sbusiness,$process,$divi,$grd,$startdate,$enddate,$id)
{
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $objPHPExcel = new PHPExcel();
      $start = strtotime($startdate);
    $end = strtotime($enddate);
    $days_between = ceil(abs($end - $start) / 86400);
    $getPresentHourscount=0;
    $getSlHourscount=0;
    $getLeaveHourscont=0;
    $getabsentHourscount=0;
    $getSLperHourscount=0;
    $getleaveperHourscount=0;
    $getabsentperHourscount=0;
    $data = array();
   
    $count1=0;
    $sql="select convert(varchar(10), DutyDate, 126) as DutyDate,PrsHrs,LEV,lvtype,DSTATUS,GRD_CODE,DIVI_CODE,BUSS_CODE,SUBBuss_Code,PROC_CODE from DayStatusQry where convert(varchar(10), DutyDate, 126) BETWEEN '$startdate' AND '$enddate' and GRD_CODE IN($grd)";
    $result = query($query,$sql,$pa,$opt,$ms_db);
    $i=1;
    $PrsHrs=array();
    $ptotal=0;
    $DutyDate=array();
    $LEV=array();
    $lvtype=array();
    $DSTATUS=array();
    $dataPoints=array();
    while ($row=$fetch($result)) 
    {
        $dataPoints[] = array("DutyDate"=>$row ['DutyDate'],"PrsHrs"=>$row['PrsHrs'],"LEV"=>$row ['LEV'],"lvtype"=>$row ['lvtype'],"DSTATUS"=>$row ['DSTATUS'],"GRD_CODE"=>$row ['GRD_CODE'],"DIVI_CODE"=>$row ['DIVI_CODE'],"BUSS_CODE"=>$row ['BUSS_CODE'],"SUBBUSS_CODE"=>$row ['SUBBuss_Code'],"PROC_CODE"=>$row ['PROC_CODE']);
       
    }
    $arrsize=sizeof($dataPoints);
    $sbusinessarr = explode(",",$sbusiness);
    $businessarr = explode(",",$business);
    $processarr = explode(",",$process);
    $diviarr = explode(",",$divi);
    $grdarr = explode(",",$grd);
    $countsbusiness=sizeof($sbusinessarr);
    $countbusiness=sizeof($businessarr);
    $countgrd=sizeof($grdarr);
    $countdiviarr=sizeof($diviarr);
    $countprocessarr=sizeof($processarr);
    //$objPHPExcel->createSheet();
    $styleArray = array(
    'font'  => array(
        'color' => array('rgb' => 'FFFFFF'),
    ));
    //print_r($sbusinessarr);
 
   
        $objPHPExcel->createSheet();
        $c=1;
        for ($j=0; $j < sizeof($businessarr); $j++) 
        {
          
           // $temp=$countdiviarr+$countdiviarr*($countgrd+1)+5;
             // $c=1;
          $c=$c+$j;
          $sql_buss="Select BussName from BussMast where BUSSID='$businessarr[$j]'";
            $result_buss=query($query,$sql_buss,$pa,$opt,$ms_db);
            $row_buss=$fetch($result_buss);
            $bussname=$row_buss['BussName'];
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$c.':G'.$c);
          //$objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getCell('A'.$c)->setValue($sbussname.' Attendance Head Count-'.$bussname);

            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('F28A8C');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $c+=1;
            //$c=$c+1;
            
            $objPHPExcel->getActiveSheet()->getColumnDimension(A)->setWidth(20);
            for ($k=0; $k < sizeof($processarr); $k++) 
              {
                    $sql_pro="Select PROC_NAME from PROCMAST where PROCID='$processarr[$k]'";
                  $result_pro=query($query,$sql_pro,$pa,$opt,$ms_db);
                  $row_pro=$fetch($result_pro);
                  $proname=$row_pro['PROC_NAME'];
                  $c=$c+1;
                  $objPHPExcel->getActiveSheet()->mergeCells('A'.$c.':G'.$c);
                    //$objPHPExcel->getActiveSheet()->getStyle('A'.$c.':AG'.$c)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getCell('A'.$c)->setValue($proname);
                   // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('F28A8C');
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                  // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $c,$proname);
                  // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFont()->setBold(true);
                  // $objPHPExcel->getActiveSheet()->getStyle('A'.$c)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('483D8B');
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),$proname);
                    $ii=1;
                    $iii="B";
                    unset($sac1);
                    unset($sac2);
                    unset($sac3);
                    unset($sac4);
                    unset($sac5);
                    unset($sac6);
                    unset($sac7);
                    $sac1=array();
                    $sac2=array();
                    $sac3=array();
                    $sac4=array();
                    $sac5=array();
                    $sac6=array();
                    $sac7=array();
                     for ($i=0; $i < sizeof($sbusinessarr); $i++) 
                      { 
                        
                          $sql_sbuss="Select subBussName from subBussMast where subBussid='$sbusinessarr[$i]'";
                          $result_sbuss=query($query,$sql_sbuss,$pa,$opt,$ms_db);
                          $row_sbuss=$fetch($result_sbuss);
                          $sbussname=$row_sbuss['subBussName'];
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sbussname);
                          $objPHPExcel->getActiveSheet()->getColumnDimension($iii)->setWidth(15);
                          $ii++;
                          $iii++;
                          $getPresentHours1=getPresentHours1($dataPoints,$arrsize,$sbusinessarr[$i],$businessarr[$j],$processarr[$k]);
                          $sac1[$i]=$getPresentHours1;
                           $getSlHours1=getSlHours1($dataPoints,$arrsize,$sbusinessarr[$i],$businessarr[$j],$processarr[$k]);
                           $sac2[$i]=$getSlHours1;
                           $getLeaveHours1=getLeaveHours1($dataPoints,$arrsize,$sbusinessarr[$i],$businessarr[$j],$processarr[$k]);
                          $sac3[$i]=$getLeaveHours1;
                          $getabsentHours1=getabsentHours1($getSlHours1,$getLeaveHours1);
                          $sac4[$i]=$getabsentHours1;
                          $getSLperHours1=getSLperHours1($getSlHours1,$getPresentHours1,$getLeaveHours1);
                          $sac5[$i]=$getSLperHours1."%";
                          $getleaveperHours1=getSLperHours1($getLeaveHours1,$getPresentHours1,$getSlHours1);
                          $sac6[$i]=$getleaveperHours1."%";
                          $getabsentperHours1=gettotalperHours1($getabsentHours1,$getPresentHours1,$getSlHours1,$getLeaveHours1);
                          $sac7[$i]=$getabsentperHours1."%";
                      }

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'Present Hrs');
                    $ii=1;
                     for ($m=0; $m < sizeof($sac1); $m++) 
                      { 
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac1[$m]);
                         $ii++;
                      }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'SL HOURS');
                     $ii=1;
                     for ($m=0; $m < sizeof($sac2); $m++) 
                      { 
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac2[$m]);
                         $ii++;
                      }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'CL/EL/Absent/Coff Hrs');
                    $ii=1;
                     for ($m=0; $m < sizeof($sac3); $m++) 
                      { 
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac3[$m]);
                         $ii++;
                      }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'TOTAL ABSENT Hrs');
                    $ii=1;
                     for ($m=0; $m < sizeof($sac4); $m++) 
                      { 
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac4[$m]);
                         $ii++;
                      }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'SL ABSENT %');
                    $ii=1;
                     for ($m=0; $m < sizeof($sac5); $m++) 
                      { 
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac5[$m]);
                         $ii++;
                      }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'CL/EL ABSENT %');
                    $ii=1;
                     for ($m=0; $m < sizeof($sac6); $m++) 
                      { 
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac6[$m]);
                         $ii++;
                      }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,(++$c),'TOTAL ABSENT %');
                    $ii=1;
                     for ($m=0; $m < sizeof($sac7); $m++) 
                      { 
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ii, $c,$sac7[$m]);
                         $ii++;
                      }
                  $c=$c+1;
              }
              $c=$c+3;
        }

$my_excel_filename = "upload/".md5(session_id().microtime(TRUE)).".xls";
sendemail($my_excel_filename,$Emp_Code,$Emp_email,$id);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="upload/name_of_file.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace(__FILE__,$my_excel_filename,__FILE__));
$objWriter->save('php://upload');
$excelOutput = ob_get_clean();
exit();
}

function getPresentHours1($arr,$arrsize,$sbusiness,$business,$proces)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["DSTATUS"]=='P' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["PROC_CODE"]==$proces)
            {
                  $val+=$arr[$row]["PrsHrs"];
            }
        }
        return floor($val/60);
}
function getSlHours1($arr,$arrsize,$sbusiness,$business,$proces)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["lvtype"]=='3' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness && $arr[$row]["PROC_CODE"]==$proces)
            {
                  $val+=$arr[$row]["LEV"];
            }
        }
        return floor($val/60);
}
function getLeaveHours1($arr,$arrsize,$sbusiness,$business,$proces)
{
    global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $val=0;
       for($row=1;$row <= $arrsize;$row++)
        {
           
            if($arr[$row]["lvtype"]!='3' && $arr[$row]["DSTATUS"]=='A' && $arr[$row]["BUSS_CODE"]==$business && $arr[$row]["SUBBUSS_CODE"]==$sbusiness &&  $arr[$row]["PROC_CODE"]==$proces)
            {
                  $val+=$arr[$row]["PrsHrs"];
            }
        }
        return floor($val/60);
}
 function getabsentHours1($arr1,$arr2)
    {
       $temp=0;
       
        $temp=$arr1+$arr2;


        return $temp;

   // echo $sql;
    }
 function getSLperHours1($sl,$p,$l)
    {
       $temp=0;
       
        $temp=$sl/($p+$sl+$l)*100;


        return round($temp,2);

   // echo $sql;
    }
     function gettotalperHours1($total,$p,$sl,$l)
    {
       $temp=0;
       
        $temp=$total/($p+$sl+$l)*100;


        return round($temp,2);

   // echo $sql;
    }
/*********************** End IWB Summary Attendance Hours *************************/

/*******************Comman Email ********************************/
function sendemail($filename,$Emp_Code,$Emp_email,$id)
{
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        $queMail= "update iwbsch set url='$filename',flag='1' where Id='$id'";
        $resMail= query($query,$queMail,$pa,$opt,$ms_db);
        $to=trim($Emp_email);
        $subject= "IWB Report";
        $message="Dear ".$Emp_Code.",<br><br>";
        $message.="Your report file http://10.240.168.105/empkonnect/pages/Reports/ajax/".$filename."<br>";
        $message.="<br><br>";
        $message.="Regards <br>";
        $message.="";
        $mail1=mymailer('donotreply@sequelone.com',$subject,$message,$to);
}


?>