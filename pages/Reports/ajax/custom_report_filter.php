<?php
include '../../db_conn.php';
ini_set('MAX_EXECUTION_TIME', -1);
 ini_set('memory_limit', '-1');
include ('../../configdata.php');
include('../class/customReport.php');
$custom_class_obj=new customReport_class();

?>
<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Company Name</label>
														<select name="company" id="company" class="form-control input-lg" multiple="multiple" data-allow-clear="true" data-close-on-select="false">
															
															<?php
															$result		=	$custom_class_obj->getCompany();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Function</label>
														<select name="function" id="function" class="form-control input-lg" multiple="multiple" data-allow-clear="true" data-close-on-select="false">
															
														<?php $result	= $custom_class_obj->getFunctionName();
														foreach($result as $key=>$value){
															?>
														<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
														<?php } ?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Sub Function</label>
														<select name="sfunction" id="sfunction" class="form-control input-lg" multiple="multiple"  data-allow-clear="true" data-close-on-select="false">
															
															<?php
															$result		=	$custom_class_obj->getSubFunctionName();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Grade</label>
														<select name="grade" id="grade" class="form-control input-lg" multiple="multiple">
															
															<?php 
															$result		=	$custom_class_obj->getGradeName();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Location</label>
														<select name="location" id="location" class="form-control input-lg" multiple="multiple">
															
															<?php 
															$result		=	$custom_class_obj->getLocationName();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Manager Name</label>
														<select name="mname" id="mname" class="form-control input-lg" multiple="multiple">
															
															<?php 
															$result		=	$custom_class_obj->getManagerName();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Designation</label>
														<select name="designation" id="designation" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->getDesignationName();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Business Unit</label>
														<select name="bu" id="bu" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->businessUnit();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Sub Business Unit</label>
														<select name="sbu" id="sbu" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->subBusinessUnit();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Work Location</label>
														<select name="wlocation" id="wlocation" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->wlocation();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Cost Master</label>
														<select name="ccenter" id="ccenter" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->costMaster();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Division</label>
														<select name="division" id="division" class="form-control input-lg designation_"  multiple="multiple">
														
															<?php
															$result		=	$custom_class_obj->division();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Region Code</label>
														<select name="rcode" id="rcode" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->getRegonCode();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Employee Type</label>
														<select name="employeeType" id="employeeType" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->getEmpType();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
										<div class="form-body">
												<div class="col-md-3">	
													<div class="form-group">
														<label>Process</label>
														<select name="process" id="process" class="form-control input-lg designation_"  multiple="multiple">
															
															<?php
															$result		=	$custom_class_obj->getProcess();
															foreach($result as $key=>$value){
															?>
																<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
										</div>
<style>
.multiselect-container {
     height: 200px !important;
    overflow: scroll !important;
	width:100% !important;
}

</style>										
										