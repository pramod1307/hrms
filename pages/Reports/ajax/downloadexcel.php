<?php
ini_set('max_execution_time', 0);
ini_set("memory_limit", "-1");
ini_set("output_buffering", "On");
ini_set("upload_max_filesize", "20M");
ini_set("post_max_size", "21M");
include ('../../db_conn.php');
include ('../../configdata.php');

$title = $_REQUEST['val'];
$code = $_REQUEST['code'];
$type = $_REQUEST['type'];
//echo $title;
$sql="select * from report_query where title='$title'";
$result=query($query,$sql,$pa,$opt,$ms_db);
$res=$fetch($result);
$fields =explode(",",$res['fields']);
$tables =$res['tables'];
$filter =$res['filter'];
$groupby =$res['groupby'];
$sortedby =$res['sortedby'];
$joindiv =$res['joindiv'];
$functionfield =$res['functionfield'];
$functionfield1 =explode(",",$res['functionfield']);
$sc_ID = $res['sc_ID']; // to compare with LOV_value
$temp="";
//print_r($fields);
//echo "aaa".$groupby."aa".$sortedby;

$sql2 = "select * from LOVMast where LOV_Field ='Report category' and LOV_Value='".$sc_ID."'";

$result2 = query($query,$sql2,$pa,$opt,$ms_db);
$res2 = $fetch($result2);
$lov_text = $res2['LOV_Text'];

//If type 3 for employee, 2 for manager, 1 for admin/hr

$where ="";

switch($lov_text){
  case "Employee Details":
      if($type==2){
          $where = " HrdMastQry.MNGR_CODE = '".$code."'";
      }else if($type==3){
          $where = " HrdMastQry.Emp_Code = '".$code."'";
      }

  break;
  case "Leave":
      if($type==2){
          $where = " Leave.ApprovedBy = '".$code."'";
      }else if($type==3){
          $where = " Leave.CreatedBy = '".$code."'";
      }
  break;
  case "Comp Off":
      if($type==2){
          $where = " compOff.approvedBy = '".$code."'";
      }else if($type==3){
          $where = " compOff.CreatedBy = '".$code."'";
      }
  break;
  case "Attendance Details":
      
      /*if($type==2){
          $where = " AND compOff.approvedBy = '".$code."'";
      }else if($type==3){
          $where = " AND compOff.CreatedBy = '".$code."'";
      }*/
      $where = "";

  break;
  case "Mark Past Attendance":
      if($type==2){
          $where = " markPastAttendance.approvedBy = '".$code."'";
      }else if($type==3){
          $where = " markPastAttendance.CreatedBy = '".$code."'";
      }
  break;
  case "OD Request":
      if($type==2){
          $where = " outOnWorkRequest.approvedBy = '".$code."'";
      }else if($type==3){
          $where = " outOnWorkRequest.CreatedBy = '".$code."'";
      }
  break;
  case "Employee History":
      if($type==2){
          $where = " HrdMastQry.MNGR_CODE = '".$code."'";
      }else if($type==3){
          $where = " HrdMastQry.Emp_Code = '".$code."'";
      } 
  break;

  default:
      $where = ""; 
}



$columnname=array();
if($groupby=="" || $groupby==" "){
    $f=0;
    for($i=0;$i< sizeof($fields); $i++){
        //echo $fields[$i];
        //$new = substr($fields[$i], strpos($fields[$i], ".") + 1);
        $b = explode(" as", $fields[$i], 2);
        preg_match_all("/\[[^\]]*\]/", $fields[$i], $matches);
        if(count($matches[0])==1){
            $temp.="CONVERT(VARCHAR(100),".$b[0].",20) ".$matches[0][0].",";
            $columnname[]=$matches[0][0];
        } else {
            $temp.="CONVERT(VARCHAR(100),".$b[0].",20) ".$matches[0][1].",";
            $columnname[]=$matches[0][1];
        }
    }
} else {
    $f=1;
    for($i=0;$i< sizeof($functionfield1); $i++){
        // echo $functionfield1[$i];
        //$new = substr($fields[$i], strpos($fields[$i], ".") + 1);
        $b = explode(" as", $functionfield1[$i], 2);
        preg_match_all("/\[[^\]]*\]/", $functionfield1[$i], $matches);
        if(count($matches[0])==1){
            $temp.="CONVERT(VARCHAR(100),".$b[0].",20) ".$matches[0][0].",";
            $columnname[]=$matches[0][0];
        } else {
            $temp.="CONVERT(VARCHAR(100),".$b[0].",20) ".$matches[0][1].",";
            $columnname[]=$matches[0][1];
        }
    }
}

$temp=rtrim($temp,',');

$sql1="Select ".$temp." from ".$tables;
if($joindiv!="" && $joindiv!=" "){
    $sql1.=" Where ".$joindiv;
    if($filter!="" && $filter!=" "){
        //$filter=str_replace(" where ","",$filter);
        $sql1.= " and ".$filter;
    }
}

if($filter!="" && $joindiv==""){
    $sql1.=" Where ".$filter;
}

if(!empty($where)){
    if($filter=="" && $joindiv==""){
        $sql1.=" Where ".$where;
    }else{

        $sql1.=" AND ".$where;
    }  
}


if($groupby!="" && $groupby!=" "){
    $sql1.=" group by ".$groupby;
}

if($sortedby!="" && $sortedby!=" "){
    $sql1.=" order by ".$sortedby;
}

//echo $sql1;die;
$result1=query($query,$sql1,$pa,$opt,$ms_db);
$file="demo.xls";
$test="<table border='2px'>";
$test.="<tr bgcolor='#87AFC6'>";
for($k=0;$k< sizeof($columnname); $k++){
    $test.="<th>".$columnname[$k]."</th>";
}

$test.="</tr>";
while ($data = $fetch($result1)){
    $test.="<tr>";
    $e=0;
    for($j=0;$j< sizeof($columnname);$j++){
        if (DateTime::createFromFormat('Y-m-d G:i:s', $data[$j]) !== FALSE){  
            $r=date('Y-m-d G:i:s', strtotime($data[$j]));
        } else {
            $r=$data[$j];
        }
        $test.="<td>".$r."</td>";
    }
    $test.="</tr>";
}

$test.= "</table>";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
echo $test;


?>