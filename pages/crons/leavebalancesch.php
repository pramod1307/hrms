<?php
//ini_set("display_errors","on"); error_reporting(E_ALL);
date_default_timezone_set('America/New_York');
ini_set('max_execution_time', 0);
ini_set("memory_limit", "-1");
ini_set("output_buffering", "On");
ini_set("upload_max_filesize", "20M");
ini_set("post_max_size", "21M");
include ('/var/www/html/empkonnect/pages/db_conn.php');
include ('/var/www/html/empkonnect/pages/configdata.php');
include ('/var/www/html/empkonnect/pages/leave/ajax/leave_class.php');
include('/var/www/html/empkonnect/pages/leave/leaveCal.php');
$leave_class_obj=new leave_class();

$special_leave=$leave_class_obj->special_leave($emp_code);
//print_r($special_leave);
$currentyear_num=$special_leave[0];
$special_leave_val=$special_leave[1];
$LvYear_leave=$special_leave[2];

$sql111="select Emp_Code,EMP_NAME,GRD_NAME from HrdMastQry where Status_Code='01'";
$result111=query($query,$sql111,$pa,$opt,$ms_db);
$rowcount=$num($result111);
//echo $rowcount;
$newdate=date('Y-m-d');
$u=1;
while($res111=$fetch($result111))
{
	//echo "aaa".$u;

	$emp_code=$res111['Emp_Code'];
$employeetype=strtolower($res111['GRD_NAME']);


$json = file_get_contents('../leave/js/leave.json'); 

  $data = json_decode($json,true);

if (!key_exists($employeetype,$data)){
  	$employeetype='salaried';
	//echo'exist';
  }
  
  
// print_r($data['apprentice'][0]);
//echo"<table><tr><td>Leave type</td><td>Leave</td></tr>";
foreach ($data[$employeetype][0] as $key => $value) {
	# code...
	//$avail=3;
	 $sql4="Select REPLACE(CONVERT(VARCHAR(10),a.DOJ,102),'.','-') AS DOJ,REPLACE(CONVERT(VARCHAR(10),a.DOJ_WEF,102),'.','-') AS DOJ_WEF,REPLACE(CONVERT(VARCHAR(10),a.DOB,102),'.','-') AS DOB,a.MStatus,REPLACE(CONVERT(VARCHAR(10),a.DOM,102),'.','-') AS DOM,a.SEX from hrdmastqry a where emp_code='$emp_code'";
	 $result4=query($query, $sql4, $pa, $opt, $ms_db);
	 if($num($result4)==0){
		
		$DoL='';
		$DoJ='';
		$DOJ_WEF='';
		$status=1;

	}
	else{
    $row2=$fetch($result4);
    $status=$row2['MStatus'];
    if($status==2){
    	$DoL=$row2['DOM'];
    }
    else{
    	$DoL=$row2['DOB'];
    }
    $DoJ=$row2['DOJ'];
    $DOJ_WEF=$row2['DOJ_WEF'];

	}
if($key=='EL'){
		$keyval=1;
	}
	if($key=='CL'){
		$keyval=2;
	}
	if($key=='SL'){
		$keyval=3;
	}
	if($key=='SEL'){
		$keyval=8;
	}
	if($key=='ML'){
		$keyval=7;
	}
	if($key=='AL'){
		$keyval=9;
	}
	if($key=='SPL'){
		$keyval=10;
	}
	if($key=='OL'){
		$keyval=11;
	}
	if($key=='BTL'){
		if($status==2){
		$keyval=6;
	} else { $keyval=5; }
	}
	$sql2="select sum(a.LvDays) as LvDays from (SELECT LvDays ,Createdby,Levkey FROM leave where Createdby='$emp_code' and status IN('2','7') and LvType='$keyval' and YEAR(Trn_Date) = YEAR(getDate()) group by Levkey,Createdby,LvDays) a";
	
		$result2=query($query, $sql2, $pa, $opt, $ms_db);

		//$num($result4);
	
	if($num($result2)==0){
		$avail=0;

	}
	else{
		$row4=$fetch($result2);
    $avail=$row4['LvDays'];

	}

	if($value['rules']['accumulation']==0){

	$leave_year=date("Y-m-d");
	$lastyear = strtotime("-1 year", strtotime($leave_year));
	$lastyear=date("Y", $lastyear);
	$lastyear='2016';
	$sql_year="select Opening from LeavMast where Emp_Code='$emp_code' and LvType='$key' and LevYear='$lastyear'";
	//echo $sql_year;
    $res_year=query($query,$sql_year,$pa,$opt,$ms_db);
   // / echo $num($res_year);
    if($num($res_year)>0)
    {
    $val_year=$fetch($res_year);
   	$last_year_val=$val_year['Opening'];
   		if($last_year_val > $value['rules']['accumulation'])
   		{
   			$last_year_credit=$last_year_val;
   			$last_year_credit1=$value['rules']['accumulation'];
   		}
   		else
   		{
   			$last_year_credit=$last_year_val;
   			$last_year_credit1=$last_year_val;
   		}
   	}
   	else
   	{
   		$last_year_credit=0;
   			$last_year_credit1=0;
		// /$opening=0;
   	}
	}
	else{
		$leave_year=date("Y-m-d");
	$lastyear = strtotime("-1 year", strtotime($leave_year));
	$lastyear=date("Y", $lastyear);
	$lastyear='2016';
	$sql_year="select Opening from LeavMast where Emp_Code='$emp_code' and LvType='$key' and LevYear='$lastyear'";
	//echo $sql_year;
    $res_year=query($query,$sql_year,$pa,$opt,$ms_db);
     if($num($res_year)>0)
    {
    $val_year=$fetch($res_year);
   	$last_year_val=$val_year['Opening'];
   		if($last_year_val >$value['rules']['accumulation'])
   		{
   			$last_year_credit=$value['rules']['accumulation'];
   			$last_year_credit1=$value['rules']['accumulation'];
   		}
   		else
   		{
   			$last_year_credit=$last_year_val;
   			$last_year_credit1=$last_year_val;
   		}
   	}
   	else
   	{
   		$last_year_credit=0;
   			$last_year_credit1=0;
		// /$opening=0;
   	}
		
	}
	if($value['name']=='BTL'){
		if($status==2){
    	$Leave_Name='Birthday/Anniversary Leave';
    }
    else{
    	$Leave_Name='Birthday/Anniversary Leave';
    }
	}
	else{
		$Leave_Name=$value['name'];
	}
	if($value['name']=='Comp-off'){

		$sql_sum="select count(*) as sum1 from compOff where action_statusIN('2','7') and CreatedBy='$emp_code'";
		$result_sum1=query($query, $sql_sum, $pa, $opt, $ms_db);
	    $row_sum1=$fetch($result_sum1);
		$accur= $row_sum1['sum1'];
	
  $sql_comp1="select sum(LvDays) as ndays from leave where CreatedBy='$emp_code' AND status IN('2','7') and LvType='13' ";
//echo $sql_comp1;
	 $result_comp1=query($query, $sql_comp1, $pa, $opt, $ms_db);
	    $row_comp1=$fetch($result_comp1);
	$compOff_qury1= $row_comp1['ndays'];

	$compOff_total= $accur-$compOff_qury1;
	
		$sql_sel="select Id from leavebalancesch where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		$result_sel=query($query, $sql_sel, $pa, $opt, $ms_db);
		$num_sel=$num($result_sel);
		if($num_sel > 0)
		{
			$sql_ini="update leavebalancesch set Closing_balance='$last_year_credit',Credit_balance='$accur',Open_balance='$opening_bal',Availed='$compOff_qury1',Net_balance='$compOff_total' where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		}
		else
		{
			$sql_ini="insert into leavebalancesch(Emp_code,date,Type,Closing_balance,Credit_balance,Open_balance,Availed,Net_balance)values('$emp_code','$newdate','$Leave_Name','$last_year_credit','$accur','$opening_bal','$compOff_qury1','$compOff_total')";
		}
		$result_ini=query($query, $sql_ini, $pa, $opt, $ms_db);
	}
	else
	{
	
		$comman_leave=$leave_class_obj->comman_leave_balance($emp_code,$keyval);
		//print_r($special_leave);
		$commanyear_num=$comman_leave[0];
		$comman_leave_val=$comman_leave[1];
		$commanLvYear_leave=$comman_leave[2];
		
$totall_day=getCL_SL($key,$value['day_limit'],$last_year_credit1,$avail,$value['rules']['accumulation'],$DoL,$DoJ,$DOJ_WEF);

		$accur=($totall_day+$avail)-$last_year_credit1;
	$accur=$comman_leave_val+$accur;

	$totall_day= $accur-$totalleve;

	$opening_bal=$last_year_credit1+$accur;
	$totall_day=$opening_bal-$avail;
//echo $last_year_credit."aa".$last_year_credit1;
		$sql_sel1="select Id from leavebalancesch where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		$result_sel1=query($query, $sql_sel1, $pa, $opt, $ms_db);
		$num_sel1=$num($result_sel1);
		if($num_sel1 > 0)
		{
			$sql_ini1="update leavebalancesch set Closing_balance='$last_year_credit',Credit_balance='$accur',Open_balance='$opening_bal',Availed='$avail',Net_balance='$totall_day' where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		}
		else
		{
			$sql_ini1="insert into leavebalancesch(Emp_code,date,Type,Closing_balance,Credit_balance,Open_balance,Availed,Net_balance)values('$emp_code','$newdate','$Leave_Name','$last_year_credit','$accur','$opening_bal','$avail','$totall_day')";
		}
		$result_ini1=query($query, $sql_ini1, $pa, $opt, $ms_db);
	}
}
$currentdate=date("Y-m-d");
$sql_shut="select sum(LvDays) as ndays from LeavTran where Emp_code='$emp_code' and LvType='8' and EffictiveDate <='$currentdate'";
  $result_shut=query($query, $sql_shut, $pa, $opt, $ms_db);

	$row_shut=$fetch($result_shut);
		$accur= $row_shut['ndays'];
	if($accur != "")
	{
		
		 $sql11="Select LOV_Text from lovmast where LOV_Field='Leave' and LOV_Value='8'";
         // echo $sql;
        $result11=query($query,$sql11,$pa,$opt,$ms_db);
          $row11=$fetch($result11);
        $Leave_Name=$row11['LOV_Text'];
        $sql_sh="select sum(LvDays) as ndays from leave where CreatedBy='$emp_code' AND status IN('2','7') and LvType='8' ";
//echo $sql_comp1;
	 $result_shu=query($query, $sql_sh, $pa, $opt, $ms_db);
	    $row_shu=$fetch($result_shu);
	$shut_down= $row_shu['ndays'];

	$shutdown_total= $accur-$shut_down;
$opening_bal=0;
		
		$sql_sel2="select Id from leavebalancesch where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		$result_sel2=query($query, $sql_sel2, $pa, $opt, $ms_db);
		$num_sel2=$num($result_sel);
		if($num_sel2 > 0)
		{
			$sql_ini2="update leavebalancesch set Closing_balance='$last_year_credit',Credit_balance='$accur',Open_balance='$opening_bal',Availed='$shut_down',Net_balance='$shutdown_total' where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		}
		else
		{
			$sql_ini2="insert into leavebalancesch(Emp_code,date,Type,Closing_balance,Credit_balance,Open_balance,Availed,Net_balance)values('$emp_code','$newdate','$Leave_Name','$last_year_credit','$accur','$opening_bal','$shut_down','$shutdown_total')";
		}
		$result_ini2=query($query, $sql_ini2, $pa, $opt, $ms_db);
	}
	$sql_special="select sum(LvDays) as ndays from LeavTran where Emp_code='$emp_code' and LvType='10' and EffictiveDate <='$currentdate'";
	//echo $sql_special;
  	$result_speial=query($query, $sql_special, $pa, $opt, $ms_db);

	//$noofspecial=$num($result_speial);
	$row_special=$fetch($result_speial);
		$accur= $row_special['ndays'];
	//echo "aaa".$accur."bbb";
	if($accur != "")
	{
		
	$sql11="Select LOV_Text from lovmast where LOV_Field='Leave' and LOV_Value='10'";
         // echo $sql;
    $result11=query($query,$sql11,$pa,$opt,$ms_db);
    $row11=$fetch($result11);
    $Leave_Name=$row11['LOV_Text'];
    $sql_spe="select sum(LvDays) as ndays from leave where CreatedBy='$emp_code' AND status IN('2','7') and LvType='10' ";
//echo $sql_comp1;
	$result_spe=query($query, $sql_spe, $pa, $opt, $ms_db);
	$row_spe=$fetch($result_spe);
	$shut_special= $row_spe['ndays'];

	$special_total= $accur-$shut_special;
	$opening_bal=0;
		$sql_sel3="select Id from leavebalancesch where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		$result_sel3=query($query, $sql_sel3, $pa, $opt, $ms_db);
		$num_sel3=$num($result_sel3);
		if($num_sel3 > 0)
		{
			$sql_ini3="update leavebalancesch set Closing_balance='$last_year_credit',Credit_balance='$accur',Open_balance='$opening_bal',Availed='$shut_special',Net_balance='$special_total' where date='$newdate' and Emp_code='$emp_code' and Type='$Leave_Name'";
		}
		else
		{
		$sql_ini3="insert into leavebalancesch(Emp_code,date,Type,Closing_balance,Credit_balance,Open_balance,Availed,Net_balance)values('$emp_code','$newdate','$Leave_Name','$last_year_credit','$accur','$opening_bal','$shut_special','$special_total')";
		}
		$result_ini3=query($query, $sql_ini3, $pa, $opt, $ms_db);	
	}
  //print_r($data['Salaried']);
$u++;
//exit;
}


?>