<?php

/**
 * Developer: Pramod Kumar sharma
 * Date: 15 MARCH 2017
 */
date_default_timezone_set('Asia/Kolkata');
$filename = "logs-employee-attendance-schedular.log";
$msg = $str = "";

set_include_path($_SERVER['DOCUMENT_ROOT'] . "/pages/");

//db connection
require_once "db_conn.php";
require_once "configdata.php";
require_once "global_class.php";
require_once "Attendance/Events/attendance-class-v2.php";
require_once "attendanceClass.php";

//ini_set("display_error", "on");
//error_reporting(E_ALL);

//create a log file
$fd = fopen($filename, "a");
$str = "\n\n***********Last updated at: [" . date("Y/m/d H:i:s") . "]***********";
fwrite($fd, $str . "\n");

ini_set('max_execution_time', 0); //300 seconds = 5 minutes
set_time_limit(0);
$empcode = isset($_REQUEST['empcode']) ? $_REQUEST['empcode'] : '';
$whereClause = (!empty($empcode)) ? "AND EMP_CODE = '".$empcode."'" : '';


//get all employees and execute his/her loop
//$queryForEmployees = "SELECT * FROM hrdmast where Status_Code = '01'";
$queryForEmployees = "SELECT * FROM hrdmast WHERE Status_Code = '01' ".$whereClause;
$resultForEmployees = query($query, $queryForEmployees, $pa, $opt, $ms_db);

$startDate = $tempStartDate = (isset($_REQUEST['startdate'])) ? date('Y-m-d', strtotime($_REQUEST['startdate'])) : date('Y-m-d h:i:s', strtotime("-1 days")); //date('Y-m-d h:i:s', strtotime('-4 months', time()));
$endDate = $tempEndDate = (isset($_REQUEST['enddate'])) ? date('Y-m-d', strtotime($_REQUEST['enddate'])) : date('Y-m-d h:i:s', strtotime("-1 days"));
$step = '+1 day';

if ($num($resultForEmployees) > 0) {
    
    while ($getEmployeeRecordsRow = $fetch($resultForEmployees)) {
        $schedularAttendaceClassObj = new AttendaceClass();
        $insertQuery = '';
        $schedularAttendaceClassObj->insertQuery;
        $startDate = strtotime($tempStartDate);
        $endDate = strtotime($tempEndDate);

        $insertQuery = "INSERT INTO " . $schedularAttendaceClassObj->tableName;
        $insertQuery .= " (" . implode(', ', $schedularAttendaceClassObj->tableFields) . ") ";
        $insertQuery .= " VALUES ";

       $insertRowsFound = 0;

        while ($startDate <= $endDate) {
            $str = $updateQuery = "";
            $updateRowsFound = 0;
            $rowDate = date('Y-m-d', $startDate);
            $startDate = strtotime($step, $startDate);
            $checkExistingRecordQuery = "SELECT Id FROM attendancesch  WITH (NOLOCK) WHERE Emp_code = '" . $getEmployeeRecordsRow['Emp_Code'] . "' AND CAST([date] AS DATE) = '" . $rowDate . "' ";
            $isRecordExist = query($query, $checkExistingRecordQuery, $pa, $opt, $ms_db);

            $options = array(
                'empCode' => $getEmployeeRecordsRow['Emp_Code'],
                'todayDateTime' => $rowDate,
                'isSchedular' => true
            );

            $schedularAttendaceClassObj->initializeObject($options);

            $updateColumns = $failureUpdateQuery = '';

            if ($num($isRecordExist) > 0) {
                $sleectResultRow = $fetch($isRecordExist);
                $sleectResultRowId = $sleectResultRow['Id'];
                $updateRowsFound++;
                $updateQuery = "UPDATE " . $schedularAttendaceClassObj->tableName . " SET ";
                foreach ($schedularAttendaceClassObj->tableFields AS $columnKey => $ColumnName) {
                    $updateColumns .= $ColumnName . " = '" . $schedularAttendaceClassObj->tableValues[$columnKey] . "', ";
                }
                $updateQuery .= " " . rtrim($updateColumns, ', ') . " WHERE Id = '" . $sleectResultRowId . "';";
                $updateQueryResult = query($query, $updateQuery, $pa, $opt, $ms_db);
                if(!$updateQueryResult){
                    $failureUpdateQuery .= $updateQuery; 
                }
            } else {
                $insertRowsFound++;
                $insertQuery .= $schedularAttendaceClassObj->insertQuery . ',';
            }
        } 
        $resultUpdate = ($updateRowsFound > 0 ) ? true : '';
        $resultInsert = ($insertRowsFound > 0) ? query($query, rtrim($insertQuery,' ,'), $pa, $opt, $ms_db) : '';

        if ($resultInsert || $resultUpdate) {
            $str .= "Attendance schedular result is " . $insertRowsFound . "-inserted and " . $updateRowsFound . "-updated at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code'];
        } else {
            $str .= "Error Getting while saved attendance at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code']."<br>".rtrim($insertQuery, ',')."<br>Failure Update Query:".$failureUpdateQuery."<br>------------------<br>";
        }
        fwrite($fd, $str . "\n");
    }
}
fclose($fd);

die('Done');
