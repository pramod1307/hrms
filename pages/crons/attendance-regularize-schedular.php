<?php
/**
 * Developer: Piyush sharma
 * Date: 19 May 2017
 */
date_default_timezone_set('Asia/Kolkata');
set_include_path($_SERVER['DOCUMENT_ROOT'] . "/empkonnect/pages/");

//db connection
require_once "/var/www/html/empkonnect/pages/db_conn.php";
require_once "/var/www/html/empkonnect/pages/configdata.php";
//create a log file
$filename = "attendance-regularize-schedular.log";
$fd = fopen($filename, "a");
$str = "\n\n***********Last updated at: [" . date("Y/m/d H:i:s") . "]***********";
fwrite($fd, $str . "\n");

ini_set('max_execution_time', 0); //300 seconds = 5 minutes
set_time_limit(0);

function sendMailData($schedular_start_date = NULL, $schedular_end_date = NULL, $emp_code = NULL) {
    global $query;
    global $pa;
    global $opt;
    global $ms_db;
    global $fetch;
    global $num;

    $mail_status = 0;
    $insert_mail_query = "insert into attendance_regularize_mail (Emp_Code, status, created_on, updated_on, start_date, end_date) values ('".$emp_code."', '".$mail_status."', GETDATE(), GETDATE(), '".$schedular_start_date."', ' ".$schedular_end_date ."')";
    $mail_query_result = query($query,$insert_mail_query,$pa,$opt,$ms_db); 
 }
/*Get schedular date and time*/
$sql = "SELECT TOP 1 * FROM attendance_regularize_schedular";
$result = query($query,$sql,$pa,$opt,$ms_db);
if($num($result) > 0) {
    while ($row = $fetch($result)) {
        if($row['schedular_type'] == 1) {
            $dayofweek = (date('w', time()));
            if(trim($dayofweek) == (trim($row['running_week_day']) - 1)) {
                $schedular_end_date = date('Y-M-d', strtotime('-1 days')) ;
                $schedular_start_date = date('Y-M-d', strtotime('-7 days'));
            } else {
                exit;
            }
        } else if($row['schedular_type'] == 2) {
            $dayofmonth = date('d');
            if(trim($dayofmonth) == trim($row['running_month_day'])) {
                $schedular_end_date = date('Y-M-d', strtotime('-1 days')) ;
                $schedular_start_date = date('Y-M-d', strtotime('-30 days'));
            } else {
                exit;
            }
        } else {
            exit;
        }
    }  

    $emp_query = "select Emp_Code,OEMailID,EMP_NAME from HrdMastQry where OEMailID != '' and Emp_Code='64119'";
    $emp_result = query($query,$emp_query,$pa,$opt,$ms_db);
    if($num($emp_result) > 0) { 
        while($emp_row = $fetch($emp_result)) { 

	    $date_start = date('d-M-Y', strtotime($schedular_start_date));
            $subject = "Attendance Status Report for '".$emp_row['EMP_NAME']."' (Emp Code= '".$emp_row['Emp_Code']."')";
            $tablestr = "Please Find attendance status details from '".$schedular_start_date."' to '".($schedular_end_date)."'<br/> <br/>";
            $tablestr .= "<table border='1'> <tbody>";
            $tablestr .= "<tr> <td colspan='8'><strong>". $subject ."</strong></td></tr>";

            // $tablestr .= "<thead > <th colspan='4'> ".$subject." </th></thead>";
            $tablestr .= "<tr> <td style='background-color:#2d78bb'> <p> <strong> # </strong> </p></td> <td style='background-color:#2d78bb'> <p> <strong> Date </strong> </p></td> <td style='background-color:#2d78bb'> <p> <strong> IN Time </strong> </p></td> <td style='background-color:#2d78bb'> <p> <strong> Date </strong> </p></td><td style='background-color:#2d78bb'> <p> <strong> OUT Time </strong> </p> </td> <td style='background-color:#2d78bb'> <p> <strong> Status </strong> </p> </td> <td style='background-color:#2d78bb'> <p> <strong> Shift </strong> </p> </td> <td style='background-color:#2d78bb'> <p> <strong> EDLA </strong> </p> </td></tr>";

            $att_query = "select * from attendance_regularize_mail WHERE Emp_Code = '".$emp_row['Emp_Code']."' and start_date = '$schedular_start_date' AND end_date = '$schedular_end_date' and status = 0";
            $att_result = query($query,$att_query,$pa,$opt,$ms_db);
            if($num($att_result) > 0) {
                $att_row = $fetch($att_result);
                $attsch_query = "select * from attendancesch WHERE Emp_Code = '".$emp_row['Emp_Code']."' and date BETWEEN '$schedular_start_date' AND '$schedular_end_date' order by Id desc";
                $attsch_result = query($query,$attsch_query,$pa,$opt,$ms_db);
                $i = 1;
                while($att_row = $fetch($attsch_result)) {
                    $style = ($i % 2 == 0) ? 'background-color: #dddddd;' : 'border: 1px solid #dddddd;';
                    $tablestr .=  "<tr> <td style='background-color:#eff3fb'>" .$i. "</td>";
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .date('d/m/Y',strtotime($att_row['date'])). "</td>";
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .date('h:i:s',strtotime($att_row['Intime'])). "</td>";
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .date('h:i:s',strtotime($att_row['Outtime'])). "</td>";
                    $final_status = getAttnAbbriviation($att_row);
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .$final_status. "</td> </tr>";

                    $shift  = explode(" ", $att_row['Schedule_Shift']);
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .$shift[0]. "</td>";
                
                    if($att_row['Lateflag']==1 && $att_row['EarlyFlag']==1){
                        $tablestr .= "<td style='background-color:#eff3fb'>Late Arrival,Early Depature</td>";
                    }else if($att_row['EarlyFlag']==1 && $att_row['Lateflag']==0){
                        $tablestr .= "<td style='background-color:#eff3fb'>Early Depature</td>";
                    }else if($att_row['EarlyFlag']==0 && $att_row['Lateflag']==1){
                        $tablestr .= "<td style='background-color:#eff3fb'>Late Arrival</td>";
                    }else{
                        $tablestr .= "<td style='background-color:#eff3fb'></td>";
                    }

                    $i++;
                }
                // echo $tablestr; die;


                $tablestr .= "<tr> <td colspan='8'> <p>Legend: Missed Punch = MIS; Weekly Off = WO; Holiday = HLD   Any holiday + additional Holiday; Present = P; Absent = A;Leave = Leave type; Leave without Pay = LWP; </p> </td> </tr>";
                $tablestr .= "<tr> <td colspan='8'><p>Legend: Anniversary = WL; Birthday = BTH;   Half Day leave  = Leave type    (H_CL, H_SL, etc); Paternity Leave  = PL; Maternity Leave = ML; OD = OD; </p> </td> </tr>";
                $tablestr .= "<tr> <td colspan='8'> <p>Legend: Short Leave = SRT;  Regularized Missed Punch = C-P; Present on Holiday = POH; Present on Weekly Off = POW; Half OD  = H_OD; Special Leave = SPL;   </p> </td> </tr>";
                // $tablestr .= "<tr style='background-color: white'> <td colspan='4'> For any assistance, contact Dhananjay Tiwari, Ext. 6508 /Mobile 9350860914 / mailid : Dhananjay.Tiwari@cnhind.com</td> </tr>";
                $tablestr .= "<tr> <td colspan='8'> <p>Login to <a href='#'> Attendance </a> application to view your attendance. Login to <a href='#'> e-HR </a> to regulaize your attendance. </p> </td> </tr>";
                $tablestr .= "<tr> <td colspan='8'> <p>This is system generated mail, do not reply to this email. </p> </td> </tr>";
                $tablestr .= "</tbody></table>";
                //$to = $emp_row['OEMailID'];
		          $to='sachin@sequelone.com';
                $mail_status = attendanceMail('donotreply@sequelone.com',$subject,$tablestr,$to);  
                if($mail_status) {
                    $insert_mail_query = "update attendance_regularize_mail set status = $mail_status, updated_on = GETDATE() where id = '".$att_row['id']."'";
                    $mail_query_result = query($query,$insert_mail_query,$pa,$opt,$ms_db);
                    $str = "Mail has been successfully sent to ".$emp_row['Emp_Code']." for date b/w ".$schedular_start_date." and ".$schedular_end_date." At " .date('d-m-Y h:i:s')." and flag has been updated in attendance_regularize_mail";
                } else {
                    $str = "Again Mail did not send to ".$emp_row['Emp_Code']." for date b/w ".$schedular_start_date." and ".$schedular_end_date." At " .date('d-m-Y h:i:s');
                } 
                die('Done');         
            } else { 
                $i = 1;
                $attsch_query = "select * from attendancesch WHERE Emp_Code = '".$emp_row['Emp_Code']."' and date BETWEEN '$schedular_start_date' AND '$schedular_end_date' order by Id desc";
                $attsch_result = query($query,$attsch_query,$pa,$opt,$ms_db);
                while($att_row = $fetch($attsch_result)) {
                    $style = ($i % 2 == 0) ? 'background-color: #dddddd;' : 'border: 1px solid #dddddd;';
                    $tablestr .=  "<tr> <td style='background-color:#eff3fb'>" .$i. "</td>";
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .date('d/m/Y',strtotime($att_row['date'])). "</td>";
$intime = (strtotime($att_row['Intime']) <= 0) ? "" : date('h:i A', strtotime($att_row['Intime']));
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .$intime. "</td>";

                    $tablestr .=  "<td style='background-color:#eff3fb'>" .date('d/m/Y',strtotime($att_row['date'])). "</td>";

$outtime = (strtotime($att_row['Outtime']) <= 0) ? "" : date('h:i A', strtotime($att_row['Outtime']));
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .$outtime. "</td>";
                    $final_status = getAttnAbbriviation($att_row);
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .$final_status. "</td> </tr>";

                    $shift  = explode(" ", $att_row['Schedule_Shift']);
                    $tablestr .=  "<td style='background-color:#eff3fb'>" .$shift[0]. "</td>";
                
                    if($att_row['Lateflag']==1 && $att_row['EarlyFlag']==1){
                        $tablestr .= "<td style='background-color:#eff3fb'>Late Arrival,Early Depature</td>";
                    }else if($att_row['EarlyFlag']==1 && $att_row['Lateflag']==0){
                        $tablestr .= "<td style='background-color:#eff3fb'>Early Depature</td>";
                    }else if($att_row['EarlyFlag']==0 && $att_row['Lateflag']==1){
                        $tablestr .= "<td style='background-color:#eff3fb'>Late Arrival</td>";
                    }else{
                        $tablestr .= "<td style='background-color:#eff3fb'></td>";
                    }

                    $i++;
                }
                // echo $tablestr; die;


                $tablestr .= "<tr> <td colspan='8'> <p>Legend: MIS = Missed Punch; WO = Weekly Off; HLD = Holiday; P = Present; A = Absent; LWP = Leave without Pay; </p> </td> </tr>";
                $tablestr .= "<tr> <td colspan='8'><p>Legend: WL = Anniversary; BTH = Birthday;   Half Day leave  = Leave type    (H_CL, H_SL, etc); PL = Paternity Leave; ML = Maternity Leave; OD = OD; </p> </td> </tr>";
                $tablestr .= "<tr> <td colspan='8'> <p>Legend: SRT = Short Leave;  C-P = Regularized Missed Punch; POH = Present on Holiday; POW = Present on Weekly Off; H_OD = Half OD; SPL = Special Leave;   </p> </td> </tr>";
                // $tablestr .= "<tr style='background-color: white'> <td colspan='4'> For any assistance, contact Dhananjay Tiwari, Ext. 6508 /Mobile 9350860914 / mailid : Dhananjay.Tiwari@cnhind.com</td> </tr>";
                // $tablestr .= "<tr> <td colspan='8'> <p>Login to <a href='#'> Attendance </a> application to view your attendance. Login to <a href='#'> e-HR </a> to regulaize your attendance. </p> </td> </tr>";
                $tablestr .= "<tr> <td colspan='8'> <p>This is system generated mail, do not reply to this email. </p> </td> </tr>";
                $tablestr .= "</tbody></table>";
                //$to = $emp_row['OEMailID'];
		        $to='sachin@sequelone.com';

                $mail_status = attendanceMail('donotreply@sequelone.com',$subject,$tablestr,$to);  
                if(!$mail_status) {
                    sendMailData($schedular_start_date, $schedular_end_date, $emp_row['Emp_Code']);
                    $str = "Mail did not send to ".$emp_row['Emp_Code']." for date b/w ".$schedular_start_date." and ".$schedular_end_date." At " .date('d-m-Y h:i:s');
                } else {
                    $str = "Mail successfully sent to ".$emp_row['Emp_Code']." for date b/w ".$schedular_start_date." and ".$schedular_end_date." At " .date('d-m-Y h:i:s');
                }
                die('Done');
            }
            fwrite($fd, $str . "\n");
            fclose($fd);
        }
    } else {
        exit;
    }
}


