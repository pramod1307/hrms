<?php

/**
 * Developer: Pramod Kumar sharma
 * Date: 07 APRIL 2017
 */
date_default_timezone_set('Asia/Kolkata');
$filename = "logs-holiday-schedular.log";
$msg = $str = "";

set_include_path($_SERVER['DOCUMENT_ROOT'] . "/empkonnect/pages/");

//db connection
require_once "/var/www/html/empkonnect/pages/db_conn.php";
require_once "/var/www/html/empkonnect/pages/configdata.php";
require_once "/var/www/html/empkonnect/pages/global_class.php";
require_once "/var/www/html/empkonnect/pages/Attendance/Events/attendance-class-v2.php";
require_once "/var/www/html/empkonnect/pages/attendanceClass.php";

//create a log file
$fd = fopen($filename, "a");
$str = "\n\n***********Last updated at: [" . date("Y/m/d H:i:s") . "]***********";
fwrite($fd, $str . "\n");

ini_set('max_execution_time', 0); //300 seconds = 5 minutes
set_time_limit(0);

$queryForMPA = "SELECT markPastId, date_from, date_to, CreatedBy FROM markPastAttendance WHERE isScheduled = '1'";
$resultForMPA = query($query, $queryForMPA, $pa, $opt, $ms_db);

if ($num($resultForMPA) > 0) {

    while ($getEmployeeRecordsRowMPA = $fetch($resultForMPA)) {
        $MPAId = $getEmployeeRecordsRowMPA['markPastId'];
        $MPAEmp = $getEmployeeRecordsRowMPA['CreatedBy'];
        $MPADateFrom = date('Y-m-d', strtotime($getEmployeeRecordsRowMPA['date_from']));
        $MPADateTo = date('Y-m-d', strtotime($getEmployeeRecordsRowMPA['date_to']));
        $current = strtotime($MPADateFrom);
        $last = strtotime($MPADateTo);


        $schedularHolidaysClassObj = new AttendaceClass();

        $schedularHolidaysClassObj->insertQuery;


        $i = 1;
        while ($current <= $last) { //echo $i++; echo "<br>";
            $insertQuery = '';
            $insertQuery = "INSERT INTO " . $schedularHolidaysClassObj->tableName;
            $insertQuery .= " (" . implode(', ', $schedularHolidaysClassObj->tableFields) . ") ";
            $insertQuery .= " VALUES ";
            $syncDate = date('Y-m-d', $current);
            $current = strtotime('+1 days', $current);
            $options = array(
                'empCode' => $MPAEmp,
                'todayDateTime' => $syncDate,
                'isSchedular' => true
            );
            $schedularHolidaysClassObj->initializeObject($options);
            $insertRowsFound = 0;

            $str = $updateQuery = "";
            $updateRowsFound = 0;
            $checkExistingRecordQuery = "SELECT Id FROM attendancesch  WITH (NOLOCK) WHERE Emp_code = '" . $MPAEmp . "' AND CAST([date] AS DATE) = '" . $syncDate . "' ";
            $isRecordExist = query($query, $checkExistingRecordQuery, $pa, $opt, $ms_db);

            $updateColumns = $failureUpdateQuery = '';

            if ($num($isRecordExist) > 0) {
                $sleectResultRow = $fetch($isRecordExist);
                $sleectResultRowId = $sleectResultRow['Id'];
                $updateRowsFound++;
                $updateQuery = "UPDATE " . $schedularHolidaysClassObj->tableName . " SET ";
                foreach ($schedularHolidaysClassObj->tableFields AS $columnKey => $ColumnName) {
                    $updateColumns .= $ColumnName . " = '" . $schedularHolidaysClassObj->tableValues[$columnKey] . "', ";
                }
                $updateQuery .= " " . rtrim($updateColumns, ', ') . " WHERE Id = '" . $sleectResultRowId . "';";
                //echo $updateQuery;
                $updateQueryResult = query($query, $updateQuery, $pa, $opt, $ms_db);
                if (!$updateQueryResult) {
                    $failureUpdateQuery .= $updateQuery;
                }
            } else {
                $insertRowsFound++;
                $insertQuery .= $schedularHolidaysClassObj->insertQuery . ',';
                $resultInsert = ($insertRowsFound > 0) ? query($query, rtrim($insertQuery, ' ,'), $pa, $opt, $ms_db) : '';
            }
        }

        $updateODFlag = "UPDATE markPastAttendance SET isScheduled = '0' WHERE markPastId = '" . $MPAId . "'";
        @query($query, $updateODFlag, $pa, $opt, $ms_db);

        if ($resultInsert || $resultUpdate) {
            $str .= "MarkPastAttendance schedular result is " . $insertRowsFound . "-inserted and " . $updateRowsFound . "-updated at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code'];
        } else {
            $str .= "Error Getting while saved MarkPastAttendance at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code'] . "<br>" . rtrim($insertQuery, ',') . "<br>Failure Update Query:" . $failureUpdateQuery . "<br>------------------<br>";
        }
        fwrite($fd, $str . "\n");
    }
}
die("Done MarkPastAttendance upload");
?>