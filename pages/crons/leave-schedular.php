<?php

/**
 * Developer: Pramod Kumar sharma
 * Date: 07 APRIL 2017
 */
date_default_timezone_set('Asia/Kolkata');
$filename = "logs-holiday-schedular.log";
$msg = $str = "";

set_include_path($_SERVER['DOCUMENT_ROOT'] . "/empkonnect/pages/");

//db connection
require_once "/var/www/html/empkonnect/pages/db_conn.php";
require_once "/var/www/html/empkonnect/pages/configdata.php";
require_once "/var/www/html/empkonnect/pages/global_class.php";
require_once "/var/www/html/empkonnect/pages/Attendance/Events/attendance-class-v2.php";
require_once "/var/www/html/empkonnect/pages/attendanceClass.php";

//create a log file
$fd = fopen($filename, "a");
$str = "\n\n***********Last updated at: [" . date("Y/m/d H:i:s") . "]***********";
fwrite($fd, $str . "\n");

ini_set('max_execution_time', 0); //300 seconds = 5 minutes
set_time_limit(0);

$queryForleave = "SELECT leaveID, LvFrom as date_from,LvTo as date_to, CreatedBy FROM leave WHERE isScheduled = '1'";
$resultForleave = query($query, $queryForleave, $pa, $opt, $ms_db);

if ($num($resultForleave) > 0) {

    while ($getEmployeeRecordsRowleave = $fetch($resultForleave)) {
        $leaveId = $getEmployeeRecordsRowleave['markPastId'];
        $leaveEmp = $getEmployeeRecordsRowleave['CreatedBy'];
        $leaveDateFrom = date('Y-m-d', strtotime($getEmployeeRecordsRowleave['date_from']));
        $leaveDateTo = date('Y-m-d', strtotime($getEmployeeRecordsRowleave['date_to']));
        $current = strtotime($leaveDateFrom);
        $last = strtotime($leaveDateTo);


        $schedularHolidaysClassObj = new AttendaceClass();

        $schedularHolidaysClassObj->insertQuery;


        $i = 1;
        while ($current <= $last) { //echo $i++; echo "<br>";
            $insertQuery = '';
            $insertQuery = "INSERT INTO " . $schedularHolidaysClassObj->tableName;
            $insertQuery .= " (" . implode(', ', $schedularHolidaysClassObj->tableFields) . ") ";
            $insertQuery .= " VALUES ";
            $syncDate = date('Y-m-d', $current);
            $current = strtotime('+1 days', $current);
            $options = array(
                'empCode' => $leaveEmp,
                'todayDateTime' => $syncDate,
                'isSchedular' => true
            );
            $schedularHolidaysClassObj->initializeObject($options);
            $insertRowsFound = 0;

            $str = $updateQuery = "";
            $updateRowsFound = 0;
            $checkExistingRecordQuery = "SELECT Id FROM attendancesch  WITH (NOLOCK) WHERE Emp_code = '" . $leaveEmp . "' AND CAST([date] AS DATE) = '" . $syncDate . "' ";
            $isRecordExist = query($query, $checkExistingRecordQuery, $pa, $opt, $ms_db);

            $updateColumns = $failureUpdateQuery = '';

            if ($num($isRecordExist) > 0) {
                $sleectResultRow = $fetch($isRecordExist);
                $sleectResultRowId = $sleectResultRow['Id'];
                $updateRowsFound++;
                $updateQuery = "UPDATE " . $schedularHolidaysClassObj->tableName . " SET ";
                foreach ($schedularHolidaysClassObj->tableFields AS $columnKey => $ColumnName) {
                    $updateColumns .= $ColumnName . " = '" . $schedularHolidaysClassObj->tableValues[$columnKey] . "', ";
                }
                $updateQuery .= " " . rtrim($updateColumns, ', ') . " WHERE Id = '" . $sleectResultRowId . "';";
                //echo $updateQuery;
                $updateQueryResult = query($query, $updateQuery, $pa, $opt, $ms_db);
                if (!$updateQueryResult) {
                    $failureUpdateQuery .= $updateQuery;
                }
            } else {
                $insertRowsFound++;
                $insertQuery .= $schedularHolidaysClassObj->insertQuery . ',';
                $resultInsert = ($insertRowsFound > 0) ? query($query, rtrim($insertQuery, ' ,'), $pa, $opt, $ms_db) : '';
            }
        }

        $updateODFlag = "UPDATE leave SET isScheduled = '0' WHERE leaveID = '" . $leaveId . "'";
        @query($query, $updateODFlag, $pa, $opt, $ms_db);

        if ($resultInsert || $resultUpdate) {
            $str .= "Leave schedular result is " . $insertRowsFound . "-inserted and " . $updateRowsFound . "-updated at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code'];
        } else {
            $str .= "Error Getting while saved Leave at: [" . date("Y/m/d H:i:s") . "] of Employee: " . $getEmployeeRecordsRow['Emp_Code'] . "<br>" . rtrim($insertQuery, ',') . "<br>Failure Update Query:" . $failureUpdateQuery . "<br>------------------<br>";
        }
        fwrite($fd, $str . "\n");
    }
}
die("Done MarkPastAttendance upload");
?>