<?php
/*
* Created By: Pramod Shrama
* Date: 07 Oct 2016
*/


/*
* Change date format
* @params $dateTime string
* Return null if @datetime is null otherwise converted custom format
*/
if (!function_exists('dateFormat')) {

	function dateFormat($dateTime = null)
	{ 
		#Remove white spaces from string
		$currentformatDateTime = trim($dateTime);

		#Return null if date time not found
		if(empty($currentformatDateTime)){ return ''; exit(); }

		#Return converted date format
		return date("d M Y", strtotime($dateTime));
	}

}

/*
* Change time format
* @params $dateTime string
* Return null if @datetime is null otherwise converted custom format
*/
if (!function_exists('timeFormat')) {

	function timeFormat($dateTime = null)
	{ 
		#Remove white spaces from string
		$currentformatDateTime = trim($dateTime);

		#Return null if date time not found
		if(empty($currentformatDateTime)){ return ''; exit(); }

		#Return converted date format
		return date('h:i A', strtotime($dateTime));
	}

}

/*
* Change time format
* @params $dateTime string
* Return null if @datetime is null otherwise converted custom format
*/
if (!function_exists('dateTimeFormat')) {

	function dateTimeFormat($dateTime = null)
	{ 
		#Remove white spaces from string
		$currentformatDateTime = trim($dateTime);

		#Return null if date time not found
		if(empty($currentformatDateTime)){ return ''; exit(); }

		#Return converted date format
		return date('jS F Y h:i A', strtotime($dateTime));
	}

}


/*
* stringTextFormat get a string from user and return a proper formatted string
*/
function stringTextFormat($string = null){
	$s = trim($string);

	if(empty($s))
		return '';
	return str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($s))));
	exit();
}

function getCurrentEmpCodeName(){
$username = (!empty($_SESSION["username"]))?$_SESSION["username"].'-':'';
	return '<br>-('.$username.''.$_SESSION["usercode"].')';
}


function prd($record){
	if(is_array($record)){
		echo '<pre>'; print_r($record); echo '</pre>';
	}else{
		echo '<pre>'; var_dump($record); echo '</pre>';
	}
	exit();
}

function Is_hr($user_id,$query,$pa,$opt,$ms_db) {
    $fetch=$_SESSION['phpsql_fetch'];
    $sql = "select role_name from hrms_role where Id in (select RoleId from UsersRoles where UserID = '".$user_id."')";
    $result = query($query,$sql,$pa,$opt,$ms_db);
    $is_hr = false;

    while ($row = $fetch($result)) {
//       echo "<pre>"; print_r($row); die;
        if ((strpos(strtolower($row['role_name']), strtolower('HR')) !== false) && (strpos(strtolower($_SESSION['Allrole'][$_SESSION['selectedRole']]['name']), strtolower('HR')) !== false) )  {
            $is_hr = true;
            break;
        } else {
            continue;
        }
    }

    return $is_hr;
}
function Is_mngr($user_id,$query,$pa,$opt,$ms_db) {
    $fetch=$_SESSION['phpsql_fetch'];
    $sql = "select role_name from hrms_role where Id in (select RoleId from UsersRoles where UserID = '".$user_id."')";
    $result = query($query,$sql,$pa,$opt,$ms_db);
    $is_mngr = false;
    while ($row = $fetch($result)) {
		if ((strpos(strtolower($row['role_name']), strtolower('Ma')) !== false) && (strpos(strtolower($_SESSION['Allrole'][$_SESSION['selectedRole']]['name']), strtolower('Ma')) !== false) )  {
            $is_mngr = true;
            break;
        } else {
            continue;
        }
    }
    return $is_mngr;
}


function Is_emp($user_id,$query,$pa,$opt,$ms_db) {
    $fetch=$_SESSION['phpsql_fetch'];
    $sql = "select role_name from hrms_role where Id in (select RoleId from UsersRoles where UserID = '".$user_id."')";
    $result = query($query,$sql,$pa,$opt,$ms_db);
    $is_emp = false;
    while ($row = $fetch($result)) {
		if ((strpos(strtolower($row['role_name']), strtolower('Em')) !== false) && (strpos(strtolower($_SESSION['Allrole'][$_SESSION['selectedRole']]['name']), strtolower('Em')) !== false) )  {
            $is_emp = true;
            break;
        } else {
            continue;
        }
    }
    return $is_emp;
}

function Is_Your_Ero ($approver_id,$user_id,$query,$pa,$opt,$ms_db) {
    $ero_flag = false;
    $fetch=$_SESSION['phpsql_fetch'];
    $eroQuery = "Select * from HrdTran where Emp_Code='".$user_id."'";
    $eroRes=query($query,$eroQuery,$pa,$opt,$ms_db);
    while($ero_row = $fetch($eroRes)) {
	    if($ero_row['ERO'] == $approver_id) {
	        $ero_flag = true; break;
	    }
	}
    return $ero_flag;
}

function get_ero_emp_ids($ero_id,$query,$pa,$opt,$ms_db) {
    $fetch=$_SESSION['phpsql_fetch'];
    $eroQuery = "Select Emp_Code,ERO from HrdMastQry where ERO = '".$ero_id."' group by ERO,Emp_Code";
    $eroRes=query($query,$eroQuery,$pa,$opt,$ms_db);
    $id_arr = array(); 
    while($ero_row = $fetch($eroRes)) {
    	$id_arr[] = "'".$ero_row['Emp_Code']."'"; 
	}
	return $id_arr;
 }

function getWorkFlow_Ero_Flag($WFFor = '',$query,$pa,$opt,$ms_db) {
	$fetch=$_SESSION['phpsql_fetch'];
    $workflow_sql = "select top 1 IS_SELECT_ERO from WorkFlow where IS_SELECT_ERO = 1 and WFFor = '$WFFor' order by WorkFlowID desc";
    $eroRes=query($query,$workflow_sql,$pa,$opt,$ms_db);
    $IS_SELECT_ERO = 0;
    while($ero_row = $fetch($eroRes)) {
    	$IS_SELECT_ERO = $ero_row['IS_SELECT_ERO']; 
	}
	return $IS_SELECT_ERO;
}

function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

function getAttnAbbriviation($row) {

    if(($row['final_status'] == 'Leave')  && ($row['statustitle'] != 'Half Day') && ($row['status'] != 'F')  && ($row['status'] != 'W')){
        if($row['LeaveTypeText'] != ''){
            if(trim($row['LeaveTypeText']) == 'BirthDay Leave (BL)'){
                    $parsed                =        "BTH";
            }else if(trim($row['LeaveTypeText']) == 'Anniversary Leave (AdL)'){
                    $parsed                =        "WL";
            }else{        
                    $parsed = $this->get_string_between($row['LeaveTypeText'], '(', ')');
                    if(trim($parsed) == ''){
                            $parsed                =        $row['LeaveTypeText'];
                    }
            }
        }        
        $val = $parsed;
    }else if(($row['final_status'] == 'OD Request') && ($row['odStatus'] == 'Approved')){
            $val = 'OD';
    }else if(($row['final_status'] == 'OD Request') && ($row['odStatus'] == 'Approved') && ($row['status'] == 'H')){
            $val = 'H_OD';
    }else if($row['final_status'] == 'Weekly Off'){
            $val = 'WO';
    }else if($row['final_status'] == 'POH'){
            $val = 'POH';
    }else if($row['final_status'] == 'POW'){
            $val = 'POW';
    }else if($row['final_status'] == 'Miss Punch'){
            $val = 'MIS';
    }else if(($row['final_status'] == 'Attendance Regularised') && ($row['markPastStatus'] == 'Approved')){
            $val = 'C-P';
    }else if($row['final_status'] == 'Holiday'  || $row['status'] == 'F'){
            $val = 'HLD';
    }else if(($row['final_status'] == 'Leave') && ($row['statustitle'] == 'Half Day')){
            $parsed                =        $row['LeaveTypeText'];
            if($row['LeaveTypeText'] == 'BirthDay Leave (BL)'){
                                    $parsed                =        "BTH";
            }else if($row['LeaveTypeText'] == 'Anniversary Leave (AdL)'){
                                    $parsed                =        "WL";
            }else{
                    $parsed = $this->get_string_between($row['LeaveTypeText'], '(', ')');
                    if(trim($parsed) == ''){
                            $parsed                =        $row['LeaveTypeText'];
                    }
            }
            $val = 'H_'.$parsed;
    }else if(trim($row['status']) == ''){
            $val = 'A';
    }else{
            $val = $row['status'];
    }
    return $val;                                                
}
?>