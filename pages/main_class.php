<?php
//include ('configdata.php');
//include ('../../define_email_data.php');
//$define_email_data_obj=new define_email_data();

class main_class{
    function __construct(){
      
    }
 
    

   function getemployee_name_main($code){

      
       global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;

        $sql="select Emp_FName,Emp_LName from hrdmastqry WHERE Emp_Code='$code'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
          $row=$fetch($result);
        $emp_name=$row['Emp_FName']." ".$row['Emp_LName'];
       // echo $emp_name;
        return $emp_name;
    }
    function getemployee_email_main($code){

      
       global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;

        $sql="Select OEMailID,EMP_NAME from HrdMastQry where Emp_Code='".$code."'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
          $row=$fetch($result);
        $emp_email=$row['OEMailID'];
       // echo $emp_name;
        return $emp_email;
    }
     
      function getemployee_leave_type_main($type)
      {
          global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
          $sql="Select LOV_Text from lovmast where LOV_Field='Leave' and LOV_Value='".$type."'";
         // echo $sql;
        $result=query($query,$sql,$pa,$opt,$ms_db);
          $row=$fetch($result);
        $keyval=$row['LOV_Text'];
        return $keyval;
    }
      
    /* pramod */
    function getEmployeeShiftByDate($empCode, $date){
         $date = trim($date);
        if(empty($empCode) || empty($date)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        
        $todayDate = date('Y-m-d', strtotime(str_replace(array('/'),'-',$date)));
        
        $whereCondition = '';
        //check employee auto period flag off record
        $autoPeriodOffQuery = "SELECT TOP 1 * FROM Roster_schema WHERE Emp_Code = '" . $empCode . "' AND '" . $todayDate . "' BETWEEN CAST(start_rost AS DATE) AND CAST(end_rost AS DATE) AND auto_period = 0";
        $autoPeriodOffQueryResult = query($query, $autoPeriodOffQuery, $pa, $opt, $ms_db);
        
        if($num($autoPeriodOffQueryResult) > 0){
            $whereCondition = " Roster_schema.RosterName= att_roster.roster AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid AND att_roster.shiftMaster = shiftMast.ShiftMastId  AND Roster_schema.Emp_Code = '" . $empCode . "' AND '" . $todayDate . "' BETWEEN CAST(Roster_schema.start_rost AS DATE) AND CAST(Roster_schema.end_rost AS DATE) AND Roster_schema.auto_period = 0 ORDER BY Roster_schema.end_rost DESC";
        }else{
            $whereCondition = " Roster_schema.RosterName= att_roster.roster AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid AND att_roster.shiftMaster = shiftMast.ShiftMastId  AND Roster_schema.Emp_Code = '" . $empCode . "' AND DAY('" . $todayDate . "') BETWEEN DAY(Roster_schema.start_rost) AND DAY(Roster_schema.end_rost) AND Roster_schema.auto_period = 1 AND Roster_schema.end_rost <= '" . $todayDate . "' ORDER BY Roster_schema.end_rost DESC";
        }
        
        $employeeShiftDetailQuery  = "SELECT TOP 1 ShiftMast.* FROM Roster_schema, att_roster, ShiftMast, ShiftPatternMast WHERE ".$whereCondition;
        
        $employeeShiftDetailResult = query($query, $employeeShiftDetailQuery, $pa, $opt, $ms_db);
        
        $row = $fetch($employeeShiftDetailResult);

        return array('Shift_From'=>(isset($row['Shift_From']))?$row['Shift_Name'].' ('.timeFormat($row['Shift_From']):'N/A','Shift_To'=>(isset($row['Shift_To']))?timeFormat($row['Shift_To']).')':'N/A','allData'=>$row);
    }

   function getemployee_category_main($code){

      
       global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;

        $sql="SELECT GRD_NAME FROM hrdmastqry WHERE Emp_Code='$code'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $row=$fetch($result);
        $GRD_NAME=$row['GRD_NAME'];
        return $GRD_NAME;
    }

function check_any_qury($from,$to,$flag1,$flag2,$code,$type)
    {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
         global $num;
         $temp=0;
        $from = str_replace('/', '-', $from);
        $from=date("Y-m-d", strtotime($from));
        //$from='2016-10-27';
        $to = str_replace('/', '-', $to);
        $to=date("Y-m-d", strtotime($to));
        //$to='2016-10-27';
        //echo $from.$to.$flag1.$flag2.$code.$type;
        if($type=='leave')
        {
           // echo $flag1."aaaa".$flag2;
            if($flag1=='1FH' || $flag2=='1FH')
            {
              $flag1='first_half';
              $flag2='first_half';
            }
            else if($flag1=='2FH' || $flag2=='2FH')
            {
              $flag1='second_half';
              $flag2='second_half';
            }
            else if($flag1=='2FD' || $flag2=='2FD')
            {
              $flag1='full_day';
              $flag2='full_day';
            }
            else
            {
              $flag1='';
              $flag2='';
            }
            $sql="select * from outOnWorkRequest,master..spt_values where CreatedBy='$code' and action_status NOT IN('3','4') AND type = 'P' AND DATEADD(DAY,number,date_from) <= date_to AND DATEADD(DAY,number,date_from) IN (select DATEADD(DAY,number,'$from') [LDate] FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number,'$from') < '$to')";
           //echo $sql;
             $temp=0;
           $result=query($query,$sql,$pa,$opt,$ms_db);
           $row_count = $num($result);
           if($row_count > 0)
           {
            $temp=1;
           }
           if($flag1=='full_day')
           {
           $sql2="select * from outOnWorkRequest where (CONVERT (VARCHAR(10),date_from,121)='$from' and natureOfWorkCause='$flag1' ) or (CONVERT (VARCHAR(10),date_to,121)='$from' and natureOfWorkCause IN('first_half','second_half')) and CreatedBy='$code' and action_status NOT IN('3','4')";
         }
         elseif ($flag2=='full_day') {

             $sql2="select * from outOnWorkRequest where (CONVERT (VARCHAR(10),date_from,121)='$from' and natureOfWorkCause='$flag1' ) or (CONVERT (VARCHAR(10),date_to,121)='$from' and natureOfWorkCause IN('first_half','second_half')) and CreatedBy='$code' and action_status NOT IN('3','4')";
         }
         else
         {
            $sql2="select * from outOnWorkRequest where (CONVERT (VARCHAR(10),date_from,121)='$from' and natureOfWorkCause='$flag1' ) or (CONVERT (VARCHAR(10),date_to,121)='$from' and natureOfWorkCause='$flag2') and CreatedBy='$code' and action_status NOT IN('3','4')";
         }
            //echo $sql2;
           $result2=query($query,$sql2,$pa,$opt,$ms_db);
           $row_count2 = $num($result2);
         // echo $row_count2;
           if($row_count2 > 0)
           {
            $temp=2;
           }
           $start = strtotime($from);
            $end = strtotime($to);
            $days_between = ceil(abs($end - $start) / 86400);
           // echo "aaaa".$days_between."ssssss";
            for($i=0;$i<=$days_between;$i++)
            {
            $p=date("Y-m-d", strtotime($from . ' + ' . $i . 'day'));
            $sa=$this->getMarkPastAttendanceStatus($code, $p);
            if($i==0 && $sa==$flag1)
            {
                $temp=1;
            }
            elseif ($i==$days_between && $sa==$flag2) {
               $temp=1;
            }
        }
            //$days_between=$days_between-1;
            for($i=1;$i<$days_between;$i++)
            {
            $p=date("Y-m-d", strtotime($from . ' + ' . $i . 'day'));
            $sa=$this->getMarkPastAttendanceStatus($code, $p);
             $temp=2;
           
            }

           // echo "www".$temp."rrrr";
            /* $sql3="select * from markPastAttendance,master..spt_values where CreatedBy='$code' and action_status IN(1,2) AND type = 'P' AND DATEADD(DAY,number,date_from) <= date_to AND DATEADD(DAY,number,date_from) IN (select DATEADD(DAY,number,'$from') [LDate] FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number,'$from') <= '$to')";
           //echo $sql3;
            
           $result3=query($query,$sql3,$pa,$opt,$ms_db);
           $row_count3 = $num($result3);
           if($row_count3 > 0)
           {
            $temp=3;
           }*/
          
           
        }
        elseif($type=='Markpast')
         {
              $temp=0;
$flag1=trim($flag1);

            if($flag1=='first_half')
            {
              $flag3='1FH';
             }
            else if($flag1=='second_half')
            {
              $flag3='2FH';
              }
            else if($flag1=="full_day")
            {
              $flag3='2FD';
               }
            else
            {
		$flag3='';
              //$flag2='';
            }
	if($flag1=="full_day")
            {
             $sql2="select leaveID from leave,master..spt_values where type = 'P' AND DATEADD(DAY,number,LvFrom) <= LVTO
            AND DATEADD(DAY,number,LvFrom) IN (select DATEADD(DAY,number,'$from') [LDate] FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number,'$from') <= '$to') and CreatedBy='$code' and leave.status NOT IN('3','4')";
            //echo $sql2;
            $result2=query($query,$sql2,$pa,$opt,$ms_db);
           $row_count2 = $num($result2);
               if($row_count2 > 0)
               {
                $temp=1;
               }
          }
          else
          {
            $sql1="select leaveID from leave where (fromhalf IN('$flag3') AND LvFrom = '$from') OR (fromhalf IN('$flag3') AND LvTo = '$from') and CreatedBy='$code' and status NOT IN('3','4') ";
           // echo $sql1;
            $result1=query($query,$sql1,$pa,$opt,$ms_db);
           $row_count1 = $num($result1);
               if($row_count1 > 0)
               {
                $temp=1;
               }
           $sq31="select leaveID from leave where (fromhalf IN('$flag3') AND LvFrom = '$to') OR (fromhalf IN('$flag3') AND LvTo = '$to') and CreatedBy='$code' and status NOT IN('3','4') ";
            $result3=query($query,$sql3,$pa,$opt,$ms_db);
           $row_count3 = $num($result3);
               if($row_count3 > 0)
               {
                $temp=1;
               }
            }
              
            $sql="select * from outOnWorkRequest,master..spt_values where CreatedBy='$code' and action_status NOT IN('3','4') AND type = 'P' AND DATEADD(DAY,number,date_from) <= date_to AND DATEADD(DAY,number,date_from) IN (select DATEADD(DAY,number,'$from') [LDate] FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number,'$from') < '$to')";
           //echo $sql;
           
           $result=query($query,$sql,$pa,$opt,$ms_db);
           $row_count = $num($result);
           if($row_count > 0)
           {
            $temp=1;
           }
           if($flag1=='full_day')
           {
           $sql2="select * from outOnWorkRequest where CreatedBy='$code' and action_status NOT IN('3','4') and ((CONVERT (VARCHAR(10),date_from,121)='$from' and natureOfWorkCause='$flag1' ) or (CONVERT (VARCHAR(10),date_to,121)='$from' and natureOfWorkCause IN('first_half','second_half'))) ";
         }
         elseif ($flag2=='full_day') {

             $sql2="select * from outOnWorkRequest where CreatedBy='$code' and action_status NOT IN('3','4') and ((CONVERT (VARCHAR(10),date_from,121)='$from' and natureOfWorkCause='$flag1' ) or (CONVERT (VARCHAR(10),date_to,121)='$from' and natureOfWorkCause IN('first_half','second_half')))";
         }
         else
         {
            $sql2="select * from outOnWorkRequest where CreatedBy='$code' and action_status NOT IN('3','4') and ((CONVERT (VARCHAR(10),date_from,121)='$from' and natureOfWorkCause='$flag1' ) or (CONVERT (VARCHAR(10),date_to,121)='$from' and natureOfWorkCause='$flag2'))";
         }
            //echo $sql2;
           $result2=query($query,$sql2,$pa,$opt,$ms_db);
           $row_count2 = $num($result2);
         // echo $row_count2;
           if($row_count2 > 0)
           {
            $temp=2;
           }
        }
         elseif($type=='ODRequest')
         {
           // echo "ssssss";
             if($flag1=='first_half')
            {
              $flag3='1FH';
             }
            else if($flag1=='second_half')
            {
              $flag3='2FH';
              }
            else if($flag1=='full_day')
            {
              $flag3='2FD';
               }
            else
            {
              $flag3='';
              //$flag2='';
            }
            if($flag1=='full_day')
           {
            $sql2="select leaveID from leave where '$from' between LvFrom and LvTo and CreatedBy='$code' and status NOT IN('3','4')";
          // echo $sql2;
            $result2=query($query,$sql2,$pa,$opt,$ms_db);
           $row_count2 = $num($result2);
           //echo $row_count2;
               if($row_count2 > 0)
               {
                $temp=1;
               }
        
            }
             if($flag1=='second_half' || $flag1=='first_half')
           {
            $sql2="select leaveID from leave where CreatedBy='$code' and status NOT IN('3','4') and ((fromhalf IN('$flag3','2FD') AND LvFrom = '$from') or (toHalf IN('$flag3') AND LvTo = '$to'))";
          //echo $sql2;
            $result2=query($query,$sql2,$pa,$opt,$ms_db);
           $row_count2 = $num($result2);
           //echo $row_count2;
               if($row_count2 > 0)
               {
                $temp=5;
               }
        
            }
            $sql1="select leaveID from leave where CreatedBy='$code' and status NOT IN('3','4') and ((fromhalf IN('$flag3') AND LvFrom = '$from') or (toHalf IN('$flag3') AND LvTo = '$to'))";
           //echo $sql1;
            $result1=query($query,$sql1,$pa,$opt,$ms_db);
           $row_count1 = $num($result1);
               if($row_count1 > 0)
               {
                $temp=1;
               }
           $sq13="select leaveID from leave where '$from' > LvFrom and '$to' < LvTo and CreatedBy='$code' and status NOT IN('3','4')";
          //echo $sq13;
            $result3=query($query,$sq13,$pa,$opt,$ms_db);
           $row_count3 = $num($result3);
               if($row_count3 > 0)
               {
                $temp=1;
               }
            $sa=$this->getMarkPastAttendanceStatus($code, $from);
            //echo $sa."   ".$flag1;
            if($sa==$flag1)
            {
                $temp=3;
            }
        }  
     
       echo json_encode($temp);
    }
    function getMarkPastAttendanceStatus($empCode, $requestDate) {
        //define variables
        $punchStatus = '';

        //remove blank spaces from left and right
        $empCode = trim($empCode);
        $requestDate = trim($requestDate);

        //check validation and return
        $dateValidaton = date_parse($requestDate);
        if($dateValidaton['error_count'] > 0) return 'invalid date';
        if(empty($empCode)) return 'invalid employee';

        
        $markPastAttendanceData = $this->getMarkPAstAttendace($empCode, $requestDate);
        
        //set mark past attendance in/out time
        $markInTime = $markPastAttendanceData['intime'];
        $markOutTime = $markPastAttendanceData['outtime'];

        if(empty($markInTime) || empty($markOutTime)) return $punchStatus;
        // Mark past attendance time
        $punchInTime = date("H:i", strtotime($markInTime));
        $punchOutTime = date("H:i", strtotime($markOutTime));
        //get empoyee shift of this date
         $newDate = date("d/m/Y", strtotime($requestDate));
        $shiftData = $this->getEmployeeShiftByDate($empCode, $newDate);
//print_r($shiftData);
        //Shift start or end time
        $shiftStartTime = date("H:i", strtotime($shiftData['allData']['Shift_From']));
        $shiftEndTime = date("H:i", strtotime($shiftData['allData']['Shift_To']));

        //mendatory hours for complete full/half day
        $mHrsHalf = $shiftData['allData']['MHrsHalf'];
        $mHrsFull = $shiftData['allData']['MHrsFul'];

        $fulldayParse = date_parse($mHrsFull); 
        $fullDayHrs = $fulldayParse['hour'].'.'.$fulldayParse['minute'];

        $halfDayTime = strtotime(+$mHrsHalf." hours", strtotime($shiftStartTime));

        $punchTimeDifference = strtotime($punchOutTime)/3600 - strtotime($punchInTime)/3600;
//echo $punchTimeDifference .'>='. $fullDayHrs .'||'. strtotime($punchInTime) .'<'. $halfDayTime .'&&'. strtotime($punchOutTime) .'>'. $halfDayTime;
        //calculate status between timing
        if($punchTimeDifference >= $fullDayHrs && (strtotime($punchInTime) < $halfDayTime && strtotime($punchOutTime) > $halfDayTime) ){
            $punchStatus = "full_day";
        } else if($halfDayTime >= strtotime($punchOutTime) && strtotime($punchInTime) >= strtotime($shiftStartTime)){
            $punchStatus = "first_half";
        } else if($halfDayTime <= strtotime($punchOutTime) && strtotime($shiftEndTime) >= strtotime($punchOutTime)) {
            $punchStatus = "second_half";
        }

        return $punchStatus;
    }

    function getMarkPAstAttendace($empCode, $requestDate) {
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        if(empty($requestDate) || empty($empCode)) return false;

        $requestDate = date('Y-m-d', strtotime($requestDate));

        $sql = "SELECT intime, outtime FROM markpastattendance WHERE CreatedBy = '".$empCode."' AND '".$requestDate."' BETWEEN date_from and date_to";
        $result = query($query,$sql,$pa,$opt,$ms_db);
        if ($num($result) > 0) {
            $row = $fetch($result);
            return $row;
        } else {
            return '';
        }
    }

}

/*$var=array(

          "0"=> array(
                        "109100a"=>array(
                        "id"=>"10910",
                        "email"=>"himanshu@sequelone.com",
                        "subject"=>"",
                        "massege"=>""
                          ),
                        "109100b"=>array(
                        "id"=>"10910",
                        "email"=>"himanshu@sequelone.com",
                        "subject"=>"",
                        "massege"=>""
                          )
            ),
          "1"=> array(
                        "109100a"=>array(
                        "id"=>"10910",
                        "email"=>"himanshu@sequelone.com",
                        "subject"=>"",
                        "massege"=>""
                          ),
                        "109100b"=>array(
                        "id"=>"10910",
                        "email"=>"himanshu@sequelone.com",
                        "subject"=>"",
                        "massege"=>""
                          )
            )
  )*/
