<?php
include ('../../configdata.php');


class OTRequest_class{
    function __construct(){
      
    }


    function getForm(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc,Field_Len,DefaultValue,Nullable,MinLength from TablesFields where Table_Name='overTime' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_Desc[$i]=$row['Field_Desc'];
            $Field_Len[$i]=$row['Field_Len'];
            $DefaultValue[$i]=$row['DefaultValue'];
            $Nullable[$i]=$row['Nullable'];
            $MinLength[$i]=$row['MinLength'];
            $i++;
        }

        return array($Field_Desc,$Field_Len,$DefaultValue,$Nullable,$MinLength,$i);
    }

    function getTable(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc,Field_Len,DefaultValue,Nullable,MinLength from TablesFields where Table_Name='OverTime_Table' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_TDesc[$i]=$row['Field_Desc'];
            $Field_TLen[$i]=$row['Field_Len'];
            $DefaultTValue[$i]=$row['DefaultValue'];
            $TNullable[$i]=$row['Nullable'];
            $TMinLength[$i]=$row['MinLength'];
            $i++;
        }

        return array($Field_TDesc,$Field_TLen,$DefaultTValue,$TNullable,$TMinLength,$i);

    }


    function getCategory(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTCategory' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);
       
    }


    function getShift(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="SELECT Shift_Code+'- ('+Shift_Name+')' as shift,Shift_Code FROM SHIFTMAST";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$shift[$i]=$row['shift'];
        	$Shift_Code[$i]=$row['Shift_Code'];
        	$i++;
        }

        return array($shift,$Shift_Code,$i);
    }


    function getMyTeam($mngrcode){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="Select Emp_Code, Emp_Name+' - ('+EMP_CODE+')' as Emp_Name FROM HrdMastQry where MNGR_CODE='10545' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$Emp_Code[$i]=$row['Emp_Code'];
        	$Emp_Name[$i]=$row['Emp_Name'];
        	$i++;
        }

        return array($Emp_Code,$Emp_Name,$i);

    }

    function getPurpose(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTPurpose' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);
    }

    function getNewReqOpt(){
    	
    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTNewReqQues' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);
    }

    function getEmployeeShiftByDate($empCode, $date){
        $date = trim($date);
        if(empty($empCode) || empty($date)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];


         $SqlQuery =  "SELECT rost.EMP_CODE, 
        convert(varchar(10),RosterStart,103) as RosterStart,
        convert(varchar(10),RosterEnd,103) as RosterEnd,
        convert(char(5), rost.shift_from,108) AS shift_from, 
        convert(char(5),rost.SHIFT_TO) AS SHIFT_TO, rost.shift_name, 
        convert(char(5),c.IN_TIME,108) AS IN_TIME, 
        convert(char(5),c.OUT_TIME,108) AS OUT_TIME
        FROM rosterQry rost
        LEFT JOIN Attendanceqry C ON C.ATTDATE='$convertDate' AND c.emp_code=rost.emp_code  
        WHERE '$convertDate' BETWEEN RosterStart AND RosterEnd AND rost.EMP_CODE in ($empCode)
        ";


        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
           $i=0;
        while ($row = $fetch($result)) {

            $Shift_From[$i]=$row['shift_from'];
            $Shift_To[$i]=$row['SHIFT_TO'];
            $Shift_Name[$i]=$row['shift_name'];
            $in_time[$i]=$row['IN_TIME'];
            $out_time[$i]=$row['OUT_TIME'];

            $i++;
        }

        return array(
            $Shift_From,
            $Shift_To,
            $Shift_Name,
            $in_time,
            $out_time,
            $i
        );
    }

    function getEmployeeName($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select Emp_Code,Emp_Name from EmpNameQry where EMP_CODE in($empCode)";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Emp_Code[$i]=$row['Emp_Code'];
            $Emp_Name[$i]=$row['Emp_Name'];
            $i++;
        }

        return array($Emp_Code,$Emp_Name,$i);
    }

    function overTimeGlobal(){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Id,overTimeName,overTimeHours from overtime_global ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $ot_id[$i]=$row['Id'];
            $ot_name[$i]=$row['overTimeName'];
            $ot_hours[$i]=$row['overTimeHours'];
            $i++;
        }

        return array($ot_id,$ot_name,$ot_hours,$i);
    }

    function checkOTRequest($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

         $sql="select Emp_Code,overTimehours,OT_Date from overtime_Balances where Emp_Code in ($empCode) AND OT_Date='$convertDate'";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
            $i=0;
            while ($row=$fetch($result)) {
                $reqHours[$i]=$row['overTimehours'];
                $i++;
            }
            return array($reqHours,$i);
         }


         
    }

   
}

?>