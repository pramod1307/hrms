function reason_show(){

    
    var reason = $('input[name="reason"]:checked').val();
    //alert(reason);
    if(reason==1){
        $("#Preason").show();
        $("#Oreason").hide();
        $("#HSreason").hide();
        $("#otherreason").hide();
    }
    else if(reason==2){
        $("#Oreason").show();
        $("#Preason").hide();
        $("#HSreason").hide();
        $("#otherreason").hide();
    }
    else if(reason==3){
        $("#HSreason").show();
        $("#Preason").hide();
        $("#Oreason").hide();
        $("#otherreason").hide();

    }
    else if(reason==4){
        $("#Preason").hide();
        $("#HSreason").hide();
        $("#Oreason").hide();
        $("#otherreason").hide();
    }
    else{
        $("#Preason").hide();
        $("#HSreason").hide();
        $("#Oreason").hide();
        $("#otherreason").show();


    }


}

function showother1(){
    var pselect = $("#selpreason").val();
    if(pselect ==5){
        $("#otherreason").show();
    }
    else{
        $("#otherreason").hide();
    }
}
function showother2(){
    var oselect = $("#seloreason").val();
    if(oselect ==5){
        $("#otherreason").show();
    }
    else{
        $("#otherreason").hide();
    }
}


function showhrs(){
        var inHour = parseInt($("#inHour").val());
        var inMinute = $("#inMinute").val();
        var inap = $("input[name=inap]:checked").val();

        var outHour = parseInt($("#outHour").val());
        var outMinute = $("#outMinute").val();
        var outap =$("input[name=outap]:checked").val();

        


       if(inap == outap){
        if(inap =='PM'){
            inHour = inHour+12;
        }
        if(outap == 'PM'){
            outHour = outHour+12;
        }
        var intime = inHour * 60 + parseInt(inMinute);
        var outtime = outHour * 60 + parseInt(outMinute);

        var minutes = (intime) - (outtime);

        var thours = Math.floor(minutes/60);          
        var tminutes = minutes % 60;

        var temp = thours+"."+tminutes;
        $("#totalhrs").val(temp);
       }
       else{
        if(outap == 'AM' && inap =='PM'){
            if(inHour != 12){
                inHour = inHour+12;
            }
            
        var intime = inHour * 60 + parseInt(inMinute);
        var outtime = outHour * 60 + parseInt(outMinute);

        var minutes = (intime) - (outtime);

        var thours = Math.floor(minutes/60);          
        var tminutes = minutes % 60;

        var temp = thours+"."+tminutes;
        $("#totalhrs").val(temp);

        }
        else if(outap == 'PM' && inap =='AM'){
            if(outHour != 12){
                outHour = outHour+12;
            }
        var intime = inHour * 60 + parseInt(inMinute);
        var outtime = outHour * 60 + parseInt(outMinute);

        var minutes = (intime) - (outtime);

        var thours = Math.floor(minutes/60);          
        var tminutes = minutes % 60;

        var temp = thours+"."+tminutes;
        $("#totalhrs").val(temp);
        }
       }

       
}

function submitGatePass(userid, mngrcode) {
        var empname = $("#empname").val();
        var empcode = $("#empcode").val();

        var dept = $("#dept").val();
        var empshift = $("#empshift").val();

        var outHour = $("#outHour").val();
        var outMinute = $("#outMinute").val();
        var outap =$("input[name=outap]:checked").val();

        var inHour = $("#inHour").val();
        var inMinute = $("#inMinute").val();
        var inap = $("input[name=inap]:checked").val();

        
        
        var totalhrs = $("#totalhrs").val();
//alert(outHour);
//alert(outMinute);
        var reason = $('input[name="reason"]:checked').val();
        var subreason,otherreason,empreason;
        if(reason=='1'){
           subreason = $('#selpreason').val();
        }
        else if(reason=='2'){
            subreason = $('#seloreason').val();
        }
        else if(reason=='3'){
            empreason = $('#selempreason').val();
            subreason = $('#selhsreason').val();
        }
        else{
            otherreason = $("#other").val();
        }
        if(subreason=='5'){
            otherreason = $("#other").val();
        }

        var gateexit = $('input[name="gateexit"]:checked').val();
        var empremark = $("#empremark").val();


        var approverId=$("#approverId").val();

        var strlevel=$("#levellist").val();
        var level1=strlevel.split(";");
        var levelarr=level1[0].split(",");
        var level=levelarr.toString();
        var type="gatepass";

//alert(reason);alert(subreason);
        if(outHour == "00" ||  outap == undefined){
            if(outHour == "00" ){
                toasterrormsg("Select Out time");
                $("#outHour").css('border-color', 'red');
                return false;
            }
            else{
                toasterrormsg("Select AM/PM of out time");
                $("input[name=outap]").css('border-color', 'red');
                return false;
            }
        }
        if(inHour == "00" ||  inap == undefined){
            if(inHour == "00"){
                toasterrormsg("Select In time");
                $("#inHour").css('border-color', 'red');
                return false
            }
            else{
                toasterrormsg("Select AM/PM of in time");
                $("input[name=inap]").css('border-color', 'red');
                return false;
            }
        }
        if(reason == "" || reason == undefined){
            toasterrormsg("Select Reason for Gate Pass");
            $('input[name="reason"]').css('border-color', 'red');
            return false;
        }

        if(reason == '4' || reason == '5'){
            
        }
        else{
            if(reason != "" || reason != undefined){
                if(subreason == "" || subreason == undefined){
                toasterrormsg("Select sub Reason 1 for Gate Pass");
                return false;
            }
            }
        }
        if(reason == '3'){
            if(subreason == "" || subreason == undefined){
                toasterrormsg("Select sub Reason 2 for Gate Pass");
                return false;
            }
            if(empreason == "" || empreason == undefined){
                toasterrormsg("Select Employee");
                return false;
            }
        }
        if(reason == '5'|| subreason == '5'){
            if(otherreason == "" || otherreason == undefined){
                toasterrormsg("Enter Reason for Gate Pass");
                return false;
            }
        }
        if(gateexit == "" || gateexit == undefined){
            toasterrormsg("Select Exit Gate");
            return false;
        }
        if(inap == outap){

            if(outHour>inHour){
                toasterrormsg("In Time should be greater than Out time");
                $("#inHour").css('border-color', 'red');
                return false;
            }
            else if(outHour==inHour && outMinute>inMinute){
                toasterrormsg("In Time should be greater than Out time");
                $("#inMinute").css('border-color', 'red');
                return false;
            }
        }
        else if (outap == 'AM' || inap == 'PM'){
            if(inHour != 12){
                if(outHour<inHour){
                toasterrormsg("In Time should be greater than Out time");
                $("#inHour").css('border-color', 'red');
                return false;
                }
                else if(outHour==inHour && outMinute<inMinute){
                toasterrormsg("In Time should be greater than Out time");
                $("#inMinute").css('border-color', 'red');
                return false;
                }
            }
                
        }
        else if (outap == 'PM' || inap == 'AM'){
            if(outHour != 12){
                if(outHour<inHour){
                toasterrormsg("In Time should be greater than Out time");
                $("#inHour").css('border-color', 'red');
                return false;
                }
                else if(outHour==inHour && outMinute<inMinute){
                toasterrormsg("In Time should be greater than Out time");
                $("#inMinute").css('border-color', 'red');
                return false;
                }
            }
                
        }


//alert(outHour);
        var formdata = {
            empname: empname,
            empcode: empcode,
            dept: dept,
            empshift: empshift,
            inHour: inHour,
            inMinute: inMinute,
            inap: inap,
            outHour: outHour,
            outMinute: outMinute,
            outap: outap,
            totalhrs: totalhrs,
            reason: reason,
            subreason: subreason,
            otherreason: otherreason,
            empreason:empreason,
            gateexit:gateexit,
            empremark:empremark,
            type: type,
            approverId:approverId,
            level:level,
            userid: userid
        };
//console.log(formdata);
        $.ajax({
            type: "POST",
            url: "ajax/gatePass_ajax.php",
            data: formdata,
            cache:false,
            sbeforeSend: function(){
                      loading();
                   },
            success: function(result){
                  unloading();
                if(result == 1) {
                    toastmsg("Gate Pass submitted successfully");
                     
                      //location.reload();
                }else if(result == 2) {
                    toasterrormsg("GatePass Already Applied");
                     //$('#outWorkForm')[0].reset();
                }else if(result == 3) {
                    toasterrormsg("Already Two Employee Applied for this reason.");
                     //$('#outWorkForm')[0].reset();
                }else {
                    toasterrormsg("Failed in Addition");
                }

            }
        });
    }