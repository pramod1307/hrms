<?php 
	include('../../db_conn.php');
	include('../../configdata.php');
	$id=$_POST['id'];
	$status=$_POST['status'];
	$code=$_POST['code'];
	$sql="select *,CONVERT (VARCHAR(19),gatepass_date) as gatepass_date,CONVERT (VARCHAR(19),UpdatedOn) as UpdatedOn,CONVERT (VARCHAR(19),user_updated) as user_updated  from gatePass WHERE Id='$id'";
    $res=query($query,$sql,$pa,$opt,$ms_db);
    $row=$fetch($res);

	$sql1="select * from hrdmastqry WHERE Emp_Code='$code'";
	$res1=query($query,$sql1,$pa,$opt,$ms_db);
	$data2=$fetch($res1);

	$actioncode=$row['Emp_Code'];
	$createdBy=$row['Emp_Code'];

	$sql2="select DSG_NAME, MailingAddress,EmpImage from hrdmastqry WHERE Emp_Code='$actioncode'";
	$res2=query($query,$sql2,$pa,$opt,$ms_db);
	$data3=$fetch($res2);

	//include ('../../main_class.php');
	//$main_class_obj=new main_class();
?>
									
<div class="portlet light bordered">

	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<form class="form-horizontal" role="form">
			<div class="form-body">
				<!-- <h3 class="form-section">Address</h3> -->
				<div class="row">
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6 ">Applied Date:</label>
							<div class="col-md-6">
								<p class="form-control-static">
							<?php echo $row['gatepass_date']; ?>
							</p>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6">Requested By:</label>
							<div class="col-md-6">
								<p class="form-control-static">
							<?php
							$mngrcode=$row['Emp_Code'];
                            $sql1="select EMP_NAME,Emp_Code from HrdMastQry WHERE Emp_Code='$mngrcode'";
                            $res1=query($query,$sql1,$pa,$opt,$ms_db);
                            $data1=$fetch($res1);
                            echo $data1['EMP_NAME']  .' ('.$data1['Emp_Code'].')';
                            ?>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6">Out Time:</label>
							<div class="col-md-6">
								<p class="form-control-static">
									<?php echo $row['outtime']; ?>									
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6">In Time:</label>
							<div class="col-md-6">
								<p class="form-control-static">
									<?php echo $row['intime']; ?>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">	
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6">Total Hrs:</label>
							<div class="col-md-6">
								<p class="form-control-static">
									<?php  echo $row['totalHours'];?>
								</p>
							</div>
						</div>
					</div>
				</div>

				<div class="row">	
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6">Reason</label>
							<div class="col-md-6">
								<p class="form-control-static">
								<?php 
									$sqlreason="select LOV_Text from LOVMast  where LOV_Field='GateReason' and LOV_Value ='".$row['reason']."'";
									
							        $resreason=query($query,$sqlreason,$pa,$opt,$ms_db);
							        $resData=$fetch($resreason);
									echo rtrim($resData['LOV_Text'],':');
									echo"-";
									$subqry;
							        if($row['reason'] == '1'){
							            $subqry = 'GatePReason';
							        }
							        else if($row['reason'] == '2'){
							            $subqry = 'GateOReason';
							        }
							        else if($row['reason'] == '3'){
							            $subqry = 'GateHSReason';
							        }
							        else if($row['reason'] == '5' || $row['subreason'] == '5'){
							        	$ssubqry = $row['other_reason'];
							        }
							         
							        $sqlotherreason="select LOV_Text from LOVMast  where 
							        LOV_Field='$subqry' and LOV_Value ='".$row['subreason']."'";
									
							        $resother=query($query,$sqlotherreason,$pa,$opt,$ms_db);
							        $resData1=$fetch($resother);
							        echo rtrim($resData1['LOV_Text'],':');
							        if($row['reason'] == '5' || $row['subreason'] == '5'){
							        	echo rtrim($ssubqry);
							        }
							        
									?>
							        

									 
								</p>
							</div>
						</div>
					</div>
				</div>
				
				
<?php if($row['action_status'] == "2" || $row['action_status'] == "3" || $row['action_status'] == "5" || $row['action_status'] == "6" || $row['action_status'] == "7"){?>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6">Approved On:</label>
							<div class="col-md-6">
								<p class="form-control-static">
							<?php
							  echo $row['UpdatedOn'];
                          
                            ?>
								</p>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<label class="col-md-6">Approver Remarks:</label>
						<div class="col-md-6">
							<p class="form-control-static">
								<?php  
									echo $row['action_remark'];
								?>
							</p>
						</div>
					</div>
				</div>
				<?php } 
				
				 if($row['action_status'] == "4" || $row['action_status'] == "5"  || $row['action_status'] == "6" || $row['action_status'] == "7"){
				 	?>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group m-0">
							<label class="col-md-6">Employee Updation On:</label>
							<div class="col-md-6">
								<p class="form-control-static">
							<?php
							 echo $row['user_updated'];
                          
                            ?>
								</p>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<label class="col-md-6">Employee Remarks:</label>
						<div class="col-md-6">
							<p class="form-control-static">
								<?php  
									echo $row['user_remarks'];
								?>
							</p>
						</div>
					</div>
				</div>
				<?php }
				  ?>

					
				

			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group m-0">
						<div class="col-md-6"><?php
                        		$status_val = $emp_code_status[$k];
                        $sqlL="select * from LOVMast WHERE LOV_Field='status' and LOV_Value='$status_val'";
						$resL=query($query,$sqlL,$pa,$opt,$ms_db);
						$dataL=$fetch($resL);
						if($status_val == "1" || $status_val == "" || $status_val == "5"){

						 echo "<span style='color:blue;'>";ECHO $dataL['LOV_Text'];echo"</span>";
						}
						else if($status_val == "2" || $status_val == "6"){
							echo "<span style='color:green;'>";ECHO $dataL['LOV_Text'];echo"</span>";
						}
						else {
							echo "<span style='color:red;'>";ECHO $dataL['LOV_Text'];echo"</span>";
						}
                        ?>
                        </div>
			            <?php if($row['action_status'] == "4"){?>
			            	<div class="col-md-6">
			            	<span class="appMan blue-bg">
				             <?php if($data3['EmpImage'] == ""){?>
				             <span class="appManImg">
				             <img class="img-circle img50" src="../Profile/upload_images/change_img.png" >
				             </span>
				             <?php }else{?>
				             <span class="appManImg">
								<img class="img-circle img50" src="../Profile/upload_images/<?php echo $data3['EmpImage'];?>" >
							 </span>
				             <?php	} ?>
				             <span  class="appManName" data-des="<?php echo $data3['DSG_NAME'];?>">
				             <?php echo $data1['EMP_NAME'];?>
				             </span>
				             </span>
			            	</div>
			            <?php } else{?>	
			            <div class="col-md-6">
			            <span class="appMan blue-bg">
	 					<span class="appManImg">
				             <?php if($data3['EmpImage'] == ""){?>
				             <img class="img-circle img50" src="../Profile/upload_images/change_img.png" >
				             <?php }else{?>
							<img class="img-circle img50" src="../Profile/upload_images/<?php echo $data2['EmpImage'];?>" >
				             <?php	} ?>
			             </span>
			             <span class="appManName" data-des="<?php echo $data2['DSG_NAME'];?>">
			             	<?php echo $data2['EMP_NAME'];?>
			             </span>
			             </span>
			            </div>
			            <?php } ?>
            		</div>
            	</div>
            </div>	
            <div class="clearfix"></div>
            <?php if($status == "1" || $status == "5"){ ?>	
			<div class="form-actions" style="margin-top:10px;">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
							<textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
							</div>

							<div class="col-md-3" style="margin-top:10px;">		
							<button type="button" onclick="submitAppRequest('<?php echo $row['Id'];?>','2','<?php echo $row['Emp_Code'];?>','<?php echo $code;?>');" class="btn default green">Approved</button>
							</div>

							<div class="col-md-3" style="margin-top:10px;">		
							<button type="button" onclick="submitAppRequest('<?php echo $row['Id'];?>','3','<?php echo $row['Emp_Code'];?>','<?php echo $code;?>');" class="btn default red">Rejected
							</button>
							</div>
								
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
			<?php } ?> 
		</form>
		<!-- END FORM-->
	</div>
</div>

							
