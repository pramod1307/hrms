<?php 
	$getForm=$GPRequest_class->getForm();	
	//print_r($getForm);
	$Field_Desc=$getForm[0];
    $Field_Name=$getForm[1];
	$Field_Len=$getForm[2];
	$DefaultValue=$getForm[3];
	$Nullable=$getForm[4];
	$MinLength=$getForm[5];
	$num=$getForm[6];	

	// $getTable=$GPRequest_class->getTable();
	// //print_r($getTable);

	// $Field_TDesc=$getTable[0];
	// $Field_TLen=$getTable[1];
	// $DefaultTValue=$getTable[2];
	// $TNullable=$getTable[3];
	// $TMinLength=$getTable[4];
	// $Tnum=$getTable[5];	


?>
<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Gate Pass Request
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                      <form  enctype="multipart/form-data" id="form" name="GatePassform" class="form-horizontal form-row-seperated">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3" >
                                        <label class="control-label" style="padding-left: 33px"><?php echo $Field_Desc[0];?></label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control input-small" name="empname" id="empname" maxlength='<?php echo $Field_Len[0];?>' readonly value='<?php echo $data['EMP_NAME'];?>'>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label"><?php echo $Field_Desc[1];?></label>
                                    </div>    
                                    <div class="col-md-3">
                                        <input type="text" class="form-control input-small" name="empcode" id="empcode" maxlength='<?php echo $Field_Len[1];?>' value='<?php echo $code;?>' readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label" style="padding-left: 33px"><?php echo $Field_Desc[2];?></label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type='hidden' value='<?php echo $data['FUNCT_CODE'];?>' id='functId'>
                                        <input type="text" class="form-control input-small" name="dept" id="dept" maxlength='<?php echo $Field_Len[2];?>' readonly  value='<?php echo $data['FUNCT_NAME'];?>'>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label"><?php echo $Field_Desc[3];?></label>
                                    </div>
                                     <div class="col-md-3">
                                    <?php 
                                    $sql1="select * from CAttendanceqry WHERE Emp_Code='$code'";
                                    $res1=query($query,$sql1,$pa,$opt,$ms_db);
                                    $data1=$fetch($res1);
                                    //echo $data1['Shift_Name'];
                                    ?>
                                    <input type='hidden' value='<?php echo $data1['ShiftMastId'];?>' id='shiftid'>
                                        <input type="text" class="form-control input-small" name="empshift" id="empshift" maxlength='<?php echo $Field_Len[3];?>' readonly value='<?php echo $data1['Shift_Name'];?>'>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 control-label" style="padding-left: 17px"><?php echo $Field_Desc[4];?></label>
                                    <div class="col-md-4">
                                        <input type="hidden" id="hidd_outtime" value="0">
                                        <label class="col-md-2" style="font-size: 13px;">
                                            Out HH</label>
                                            <select class="col-md-2" id="outHour" style="padding-left:0px !important;" onchange="showhrs();">
                                                <option value="00">00</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <label class="col-md-2" style="font-size: 13px;">
                                                Out MM
                                            </label>
                                            <select class="col-md-2" id="outMinute" style="padding-left:0px !important;" onchange="showhrs();">
                                                <?php 
                                                   for($i=0;$i<60;$i++){
                                                   
                                                       if(strlen($i)==1){
                                                           $t="0".$i;
                                                       }else{
                                                           $t=$i;
                                                       }
                                                   
                                                   echo'<option value="'.$t.'">'.$t.'</option>';
                                                   }
                                                   ?>
                                            </select>
                                        <div class="col-md-4">
                                        <label ><input type="radio" class="icheck" name="outap"  value="AM" onchange="showhrs()">AM
                                        </label>
                                        <label>
                                        <input type="radio" class="icheck" name="outap"  value="PM" checked onchange="showhrs()">PM
                                        </label>
                                    </div>
                                </div>
                                    <label class="col-md-2 control-label"><?php echo $Field_Desc[5];?></label>
                                    <div class="col-md-4">
                                            <input type="hidden" id="hidd_intime" value="0">
                                    <label class="col-md-2"  style="
                                       font-size: 13px;">In HH</label>
                                    <select class="col-md-2" id="inHour" style="padding-left:0px !important;" onchange="showhrs();">
                                       <option value="00">00</option>
                                       <option value="01">01</option>
                                       <option value="02">02</option>
                                       <option value="03">03</option>
                                       <option value="04">04</option>
                                       <option value="05">05</option>
                                       <option value="06">06</option>
                                       <option value="07">07</option>
                                       <option value="08">08</option>
                                       <option value="09">09</option>
                                       <option value="10">10</option>
                                       <option value="11">11</option>
                                       <option value="12">12</option>
                                    </select>
                                    <label class="col-md-2"  style="
                                       font-size: 13px;">In MM</label>
                                    <select class="col-md-2" id="inMinute" style="padding-left:0px !important;" onchange="showhrs();">
                                    <?php 
                                       for($i=0;$i<60;$i++){
                                       
                                           if(strlen($i)==1){
                                               $t="0".$i;
                                           }else{
                                               $t=$i;
                                           }
                                       
                                       echo'<option value="'.$t.'">'.$t.'</option>';
                                       }
                                       ?>
                                    </select>
                                    <div class="col-md-4">
                                       <label >
                                       <input type="radio" class="icheck"  name="inap"  value="AM" onchange="showhrs()">AM
                                       </label>
                                       <label>
                                       <input type="radio"  class="icheck" name="inap"  value="PM" checked onchange="showhrs()">PM
                                       </label>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <label class="col-md-4 control-label" style="padding-right: 93px;"><?php echo $Field_Desc[6];?></label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control input-small" name="totalhrs" id="totalhrs" maxlength='<?php echo $Field_Len[6];?>' disabled>
                                            </div>
                                    </div>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label" style="padding-left: 37px"><?php echo $Field_Desc[7];?></label>
                                    </div>
                                    <div class="col-md-7" style="padding-top: 14px;">
                                    <?php $sql="select * from LOVMast where LOV_Field='GateReason' and LOV_Active='Y' order by LOV_OrdNo";
                                        $result = query($query,$sql,$pa,$opt,$ms_db);
                                    while($row = $fetch($result)) {
                                           ?>
                                        <input type="radio" value="<?php echo $row['LOV_Value'];?>" name="reason" id="reason" onclick="reason_show();">

                                        <?php echo $row['LOV_Text'];?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                                <div class="form-group" id='Preason' style="display: none">
                                    <div class="row">
                                        <label class="col-md-2 control-label" style="padding-right: 42px;">Select Reason</label>

                                        <div class="col-md-6">
                                            <select id='selpreason' name='selpreason' onchange="showother1();">
                                            <option value=''>Select Personal Reason</option>
                                            <?php $sql="select * from LOVMast where LOV_Field='GatePReason' and LOV_Active='Y' order by LOV_OrdNo";
                                           $result = query($query,$sql,$pa,$opt,$ms_db);
                                           while($row = $fetch($result)) {
                                           ?>
                                           <option value='<?php echo $row['LOV_Value'];?>' ><?php echo $row['LOV_Text'];?></option>
                                       
                                        <?php } ?>
                                        </select>
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group" id='Oreason' style="display: none">
                                    <div class="row">
                                     <label class="col-md-2 control-label" style="padding-right: 42px;">Select Reason</label>

                                        <div class="col-md-6">
                                            <select id='seloreason' name='seloreasons' onchange="showother2();">
                                            <option value=''>Select Official Reason</option>
                                            <?php $sql="select * from LOVMast where LOV_Field='GateOReason' and LOV_Active='Y' order by LOV_OrdNo";
                                           $result = query($query,$sql,$pa,$opt,$ms_db);
                                           while($row = $fetch($result)) {
                                           ?>
                                        <option value='<?php echo $row['LOV_Value'];?>' ><?php echo $row['LOV_Text'];?></option>
                                        <?php } ?>
                                        </select>
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group" id='HSreason' style="display: none">
                                    <div class="row">
                                        <label class="col-md-2 control-label" style="padding-right: 42px;">Select Employee</label>
                                            <div class="col-md-4">
                                                <select id='selempreason' name='selempreason'>
                                                    <option value=''>Employees</option>
                                                    <?php $sql="select Emp_code,Emp_Name from EmpNameQry";
                                                   $result = query($query,$sql,$pa,$opt,$ms_db);
                                                   while($row = $fetch($result)) {
                                                   ?>
                                                    <option value='<?php echo $row['Emp_code'];?>' ><?php echo $row['Emp_Name']; echo " ("; echo $row['Emp_code']; echo ")";?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                         <label class="col-md-2 control-label" style="padding-right: 42px;">Select Reason</label>
                                            <div class="col-md-4">
                                                <select id='selhsreason' name='selhsreason'>
                                                    <option value=''>Select Reason</option>
                                                    <?php $sql="select * from LOVMast where LOV_Field='GateHSReason' and LOV_Active='Y' order by LOV_OrdNo";
                                                   $result = query($query,$sql,$pa,$opt,$ms_db);
                                                   while($row = $fetch($result)) {
                                                   ?>
                                                    <option value='<?php echo $row['LOV_Value'];?>' ><?php echo $row['LOV_Text'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>  
                                    </div>
                                </div>
                                <div class="form-group" id='otherreason' style="display: none">
                                    <div class="row">
                                        <label class="col-md-2 control-label" style="padding-right: 42px;">Enter Reason</label>

                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-small" name="other" id="other">
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                    <div class="row">
                                     <label class="col-md-2 control-label" style="padding-right: 77px;"><?php echo $Field_Desc[8];?></label>
                                     <div class="col-md-4" style="padding-top: 14px;">
                                            <?php $sql="select * from LOVMast where LOV_Field='GateExit' and LOV_Active='Y' order by LOV_OrdNo";
                                           $result = query($query,$sql,$pa,$opt,$ms_db);
                                           while($row = $fetch($result)) {
                                           ?>
                                        <input type="radio" value="<?php echo $row['LOV_Value'];?>" name="gateexit" id="gateexit" onclick="reason_show();">

                                        <?php echo $row['LOV_Text'];?>
                                        <?php } ?>
                                            </div>
                                            
                                        <label class="col-md-2 control-label"><?php echo $Field_Desc[9];?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control input-small" name="empremark" id="empremark" maxlength='<?php echo $Field_Len[9];?>' >
                                            </div>
                                        
                                    </div>
                            </div>
                            <div class="form-group" style="display: none">
                                    <div class="row">
                                                                         
                                        <label class="col-md-3 control-label"><?php echo $Field_Desc[10];?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control input-xlarge" name="deptremark" id="deptremark" maxlength='<?php echo $Field_Len[10];?>' disabled>
                                            </div>
                                        
                                    </div>
                            </div>
                            <div class="form-group" style="display: none">
                                    <div class="row">
                                                                         
                                        <label class="col-md-3 control-label" style="padding-right: 37px;"><?php echo $Field_Desc[11];?></label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control input-small" name="inchargename" id="inchargename" maxlength='<?php echo $Field_Len[11];?>' disabled>
                                            </div>
                                         <label class="col-md-1 control-label"><?php echo $Field_Desc[12];?></label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control input-small" name="ECno" id="ECno" maxlength='<?php echo $Field_Len[12];?>' disabled>
                                            </div>
                                            <label class="col-md-1 control-label"><?php echo $Field_Desc[13];?></label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control input-small" name="sign" id="sign" maxlength='<?php echo $Field_Len[13];?>' disabled>
                                            </div>
                                        
                                    </div>
                            </div>
                             <div class="form-group">
                                    <div class="row">
                                                                         
                                        <label class="col-md-2 control-label"><?php echo $Field_Desc[14];?></label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control input-xlarge
                                                " name="secremark" id="secremark" maxlength='<?php echo $Field_Len[14];?>' disabled>
                                            </div>
                                         <label class="col-md-1 control-label"><?php echo $Field_Desc[15];?></label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control input-small" name="gateno" id="gateno" maxlength='<?php echo $Field_Len[15];?>' disabled>
                                            </div>
                                            
                                        
                                    </div>
                            </div>
                            <div class="form-group">
                                    <div class="row">
                                                                         
                                        <label class="col-md-2 control-label" style="padding-right: 48px;"><?php echo $Field_Desc[16];?></label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control input-xlarge
                                                " name="hrremarks" id="hrremarks" maxlength='<?php echo $Field_Len[16];?>' disabled>
                                            </div>
                                         
                                            
                                    </div>
                            </div>
                            <input type="hidden" name="levellist" id="levellist" value="">
                        <div class="row">
                           <div class="form-group" id="According_WorkFlow">
                              <div class="col-md-12">
                                 <label class='col-md-2 control-label' style="padding-right: 59px;"><?php echo $Field_Desc[17]; ?></label>   
                                 <div class="col-md-10" id="showlevel1"> 
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-actions">
                           <div class="row">
                              <div class="col-md-offset-4 col-md-9">
                                 <button type="button" onclick="submitGatePass('<?php echo $code; ?>','<?php echo $mngrcode;?>');" class="btn btn-circle blue">Submit</button>
                                 <button type="button" class="btn btn-circle default">Cancel</button>
                              </div>
                           </div>
                        </div>
                        </div>
                        
                        </form>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>