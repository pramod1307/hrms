<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
<div class="page-content">
<!--modal-dialog -->
<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog lg">
		<!-- modal-content -->
		<div class="modal-content" >
			<div class="modal-header portlet box blue">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title white-txt"><div class="caption"><span>My Gate Pass Requests</span></div></h4>
			</div>

			<div class="modal-body" id="mygprequest">

				<?php //include ("content/view_myodrequest.php"); ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>
<!-- /.modal-dialog -->
<div class="row">
	<div class="col-md-12">			
		<!-- BEGIN CONDENSED TABLE PORTLET-->
		<div class="portlet box blue">

			<div class="portlet-title">
				<div class="caption">
					Gate Pass Requests			</div>
			</div>
			<div class="portlet-body">
				<div class="row">
                                <div class="col-md-12">

                                    <div class="btn-group ">
                                        <select class="form-control" onchange="getRelatedSelectbox(this.value,<?php echo $code;?>);">
                                            <option value="0">Select....</option>
                                            <option value="1">Request Applied On</option>
                                            <option value="2">Request Status</option>
                                            <option value="3">By Approver Name</option>
                                        </select>
                                    </div>

                                    <div class="btn-group" id="monthlySearch" style="display: none;margin-left: 45px;">
                                    	
                                        <div class="col-md-5">
	                                    <label class="control-label">
	                                    	From Date                
	                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" class="form-control" name="fromDate" id="fromDate" placeholder="dd/mm/yy">
										</div>
										<div class="col-md-5">	
										<label class="control-label">
	                                    	To Date                
	                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" class="form-control" name="toDate" id="toDate" placeholder="dd/mm/yy">
                                    	</div>
                                     	<div class="col-md-2" style="margin-top: 25px;">
                                     	<button class="btn" onclick="searchByDate('<?php echo $code;?>');">Go</button>
                                     	</div>
                                     
                                    </div>

                                    <div class="btn-group" id="actionSearch" style="display: none;margin-left: 45px;">
                                        <select class="form-control" 
                                        onchange="searchByStatus(this.value,'<?php echo $code;?>');" >
                                            <option value="">Select ..</option>
                                            <?php
                                            $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='status'";
                                            $result=query($query,$sql,$pa,$opt,$ms_db);
                                            while ($row = $fetch($result)){
                                                ?>
                                                <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                            <?php } ?>

                                        </select>
                                    </div>

                                    <div class="btn-group " id="bynameSearch" style="display: none;margin-left: 45px;">

                                            <!-- <input type="text" name="byname" class="form-control pull-left" id="byname" onchange="getInputValue(this.value);" placeholder="Enter Emp code or Name"> -->

                                            <div class="col-md-10">
                                            <label class="control-label"></label>
                                             <select class="form-control" name="byname"  id="byname" onchange="serchByCodeName(this.value,'<?php echo $code;?>');">
                                             <option value="0">Select ..</option>
                                            <?php 
                                                $sqlCrea="select approvedBy from gatepass";
                                                $resCrea=query($query,$sqlCrea,$pa,$opt,$ms_db);
                                                while ($rowCrea = $fetch($resCrea)) {
                                                  $arrEmp[]="'" .$rowCrea['approvedBy'] . "'";
                                                }

                                                $strEmp =  implode(",", $arrEmp) ;

                                                $sqlEmp="Select Emp_Code,EMP_NAME from HrdMastQry where Emp_Code in ($strEmp) ";
                                                $resEmp=query($query,$sqlEmp,$pa,$opt,$ms_db);
                                                while ($rowEmp =$fetch($resEmp)) {?>
                                                    <option value="<?php echo $rowEmp['Emp_Code']?>"><?php echo $rowEmp['EMP_NAME'].' ('.$rowEmp['Emp_Code'].')'?></option> 
                                              <?php  }
                                                
                                                 ?> 
                                             </select>
                                        </div> 

									
                                    </div>

                                </div>
                            </div>

				<table class="table table-striped table-bordered table-hover" id="sample_2">
                    <?php
                
                           $getMygatepassData=$gatepass_class_obj->getMygatepassData($code);
                        print_r($getMyODRequestData);


                          
                            $Id= $getMygatepassData[0];
                            $gatepass_date=$getMygatepassData[1];
                            $intime=$getMygatepassData[2];
                            $outtime=$getMygatepassData[3];
                            $totalhrs=$getMygatepassData[4];
                            $approvedBy=$getMygatepassData[5];
                            $reason=$getMygatepassData[6];
                            $action_status=$getMygatepassData[7];
                            $gpKey=$getMygatepassData[8];
                            $count=$getMygatepassData[9];
                            $user_remarks=$getMygatepassData[10];
                            $user_updated=$getMygatepassData[11];
                            $UpdatedOn=$getMygatepassData[12];
                            $action_remark=$getMygatepassData[13];

                             //echo $oDKey;

                    //  print_r($action_remark);
                ?>
				<thead>
				
				<tr>
					          <th>
                       Applied Date 
                    </th>

                    <th>
                       Approved By
                    </th>

                    <th>
                       Out Time - To DatIn Time
                    </th>

                    <th>
                       Total hrs
                    </th>
                     <th>
                       Reason
                    </th>
                     <th>
                       Approved On
                    </th>

                    <th>
                        Approved Remarks
                    </th>

                   

                    <th>
                        Status
                    </th>
				</tr>
				</thead>
				<tbody id="searchMyData">
				<?php
                   for ($i=0;$i<$count;$i++){
                   //echo '<pre>'; print_r($_SESSION); die;
                    	//$sqlWork="select LOV_Value,LOV_Text from LOVMast  where LOV_Field='".$natureOfWork[$i]."'";
                       // echo $sqlWork;
                        //$resWork=query($query,$sqlWork,$pa,$opt,$ms_db);
//                        $resData=$fetch($resWork);
                ?>
				<tr >

					<td>
						 <?php echo $gatepass_date[$i];?>
					</td>

					<td>
						<?php
                            $mngrcode=$approvedBy[$i];
                            $sql1="select EMP_NAME ,Emp_Code from HrdMastQry WHERE Emp_Code='$mngrcode'";
                            $res1=query($query,$sql1,$pa,$opt,$ms_db);
                            $data1=$fetch($res1);
                            echo isset($data1['EMP_NAME'])?$data1['EMP_NAME'].' ('.$data1['Emp_Code'].')':'N/A';
                        ?>
					</td>

					<td>
						 <?php echo $outtime[$i]." to ".$intime[$i];?>
					</td>

          <td>
             <?php echo $totalhrs[$i];?>
          </td>
					
					<td>
						<?php 
						 $sqlWork="select LOV_Text from LOVMast  where LOV_Field='GateReason' and LOV_Value ='".$reason[$i]."'";
                                    
                                    $resWork=query($query,$sqlWork,$pa,$opt,$ms_db);
                                    $resData=$fetch($resWork);
                                    
                                            echo $resData['LOV_Text'];
                                    
                            
						?>
                        
					</td>

          <td>
             <?php echo $UpdatedOn[$i];?>
          </td>
          <td>
             <?php echo $action_remark[$i];?>
          </td>


					<td>
						<?php if($action_status[$i] == "1"){?>
							<a class="mygp" data-toggle="modal" href="#large" 
							onclick="getmygpId('<?php echo $gpKey[$i];?>','1','<?php echo $code;?>');">
								<span class="label bg-blue-steel">
								Pending </span>
							</a>	
                            <?php } else if($action_status[$i]  == "2") {?>
                            	<a class="mygp" data-toggle="modal" href="#large"
                            	onclick="getmygpId('<?php echo $gpKey[$i];?>','2','<?php echo $code;?>');">
                                <span class="label label-success">
                               Approved
								 </span>
							 </a>
             

                            <?php } else if($action_status[$i]  == "3") {?>
                            	<a class="mygp" data-toggle="modal" href="#large"
                            	onclick="getmygpId('<?php echo $gpKey[$i];?>','3','<?php echo $code;?>');">
                                <span class="label label-danger">
                               Rejected
							 </span>
							 </a>
                            <?php } else if($action_status[$i] == "4") {?>
                            	<a class="mygp" data-toggle="modal" href="#large"
                            	onclick="getmygpId('<?php echo $gpKey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label bg-grey-cascade">
                               Cancel
							 </span>
							 </a>
                            <?php } else if($action_status[$i]  == "5") {?>
                                <a class="mygp" data-toggle="modal" href="#large"
                                onclick="getmygpId('<?php echo $gpKey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label bg-blue-steel">
                               Approved Cancel
                             </span>
                             </a>
                            <?php } 
                            else if($action_status[$i]  == "6") {?>
                                <a class="mygp" data-toggle="modal" href="#large"
                                onclick="getmygpId('<?php echo $gpKey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label bg-blue-steel">
                               Approved Cancellation
                             </span>
                             </a>
                            <?php } 
                            else if($action_status[$i]  == "7") {?>
                                <a class="mygp" data-toggle="modal" href="#large"
                                onclick="getmygpId('<?php echo $gpKey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label bg-blue-steel">
                               Approved Cancel Rejected
                             </span>
                             </a>
                            <?php } 


                            ?>
					</td>

				</tr>
				<?php } ?>
				</tbody>
				
				</table>
			</div>
		
		</div>


		<!-- END CONDENSED TABLE PORTLET-->


	</div>
</div>

</div>
</div>
