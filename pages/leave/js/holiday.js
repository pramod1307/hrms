function getTables1(tablename,pre,divid,status,name){

        $.ajax({
            type : 'POST',
            data:{type:'init',status:status,table:tablename,pre:pre,name:name },
            url  : 'ajax/holiday_ajax.php',
            beforeSend: function(){
                loading();
            },
            success: function(responseText){
                unloading();
                $("#"+divid).html(responseText);

            }
        });
}



$("#search").autocomplete({
        source: "holidaysearch.php", // name of controller followed by function
     }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        var inner_html = '<a>' +
            '<div class="list_item_container">' +
            '<span> ' + item.label + '</span></div></a>';

        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append(inner_html)
            .appendTo( ul );
    };



function showAdvanceFilter(sval,code)
{
    if(sval == '1'){
        $("#showorg").val('0');
         //   $("#emps").hide();
        $("#multisearch").show();
        $("#multisearch1").show();
        $("#multisearch2").show();
        $("#multisearch3").show();
        $('#company-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#business-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#sub-business-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
        //    $('#location-select').multiselect({
        //     includeSelectAllOption: true,
        //     enableFiltering: true
        // });
           $('#work-location-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#function-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#sub-function-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#cost-master-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#process-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#grade-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#designation-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
    }
    else{
            $("#showorg").val('1');
           // alert('hello');
           // $("#emps").show();
            $("#multisearch").hide();
            $("#multisearch1").hide();
            $("#multisearch2").hide();
            $("#multisearch3").hide();
    }
    var gadha= ['Select Company','Select Business','Select Sub-Business','Select Work Location','Select Function','Select Sub-Function','Select Cost Master','Select Process','Select Grade', 'Select Designation'];
    var gha = $(gadha).length;
    for(i=0;i<=gha;i++){
        $('.selectKunal').eq(i).find('.multiselect-selected-text').text(gadha[i]);
    }
}  

function submitholiday(){

    var hDate = $("#holiDate").val();
    var loc = $("#loc").val();
    var hname = $("#search").val();
    var comp = $("#company-select").val();
    var bus = $("#business-select").val();
    var subbus = $("#sub-business-select").val();
    var wloc = $("#work-location-select").val();
    var func = $("#function-select").val();
    var subfunc = $("#sub-function-select").val();
    var cost = $("#cost-master-select").val();
    var proc = $("#process-select").val();
    var grd = $("#grade-select").val();
    var desg = $("#designation-select").val();
    var type="insert";
 
    var locval = $('#locdiv').find('.multiselect-selected-text').text();
    var locres = locval.substring(0, 3);
    if(locres=='All'){
       // alert (meraChecker1);
     loc = '';
    }
    var compval = $('#compdiv').find('.multiselect-selected-text').text();
    var compres = compval.substring(0, 3);
    if(compres=='All'){
       // alert (meraChecker1);
     comp = '';
    }

 var busval = $('#busdiv').find('.multiselect-selected-text').text();
    var busres = busval.substring(0, 3);
    if(busres=='All'){
       // alert (meraChecker1);
     bus = '';
    }
     var subbusval = $('#subbusdiv').find('.multiselect-selected-text').text();
    var subbusres = subbusval.substring(0, 3);
    if(subbusres=='All'){
       // alert (meraChecker1);
     subbus = '';
    }
     var wlocval = $('#wlocdiv').find('.multiselect-selected-text').text();
    var wlocres = wlocval.substring(0, 3);
    if(wlocres=='All'){
       // alert (meraChecker1);
     wloc = '';
    }
     var funcval = $('#funcdiv').find('.multiselect-selected-text').text();
    var funcres = funcval.substring(0, 3);
    if(funcres=='All'){
       // alert (meraChecker1);
     func = '';
    }
     var subfuncval = $('#subfuncdiv').find('.multiselect-selected-text').text();
    var subfuncres = subfuncval.substring(0, 3);
    if(subfuncres=='All'){
       // alert (meraChecker1);
     subfunc = '';
    }
     var costval = $('#costdiv').find('.multiselect-selected-text').text();
    var costres = costval.substring(0, 3);
    if(costres=='All'){
       // alert (meraChecker1);
     cost = '';
    }

var grdval = $('#grddiv').find('.multiselect-selected-text').text();
    var grdres = grdval.substring(0, 3);
    if(grdres=='All'){
       // alert (meraChecker1);
     grd = '';
    }
    var procval = $('#procdiv').find('.multiselect-selected-text').text();
    var procres = procval.substring(0, 3);
    if(procres=='All'){
       // alert (meraChecker1);
     proc = '';
    }
    var dsgval = $('#desgdiv').find('.multiselect-selected-text').text();
    var dsgres = dsgval.substring(0, 3);
    if(dsgres=='All'){
       // alert (meraChecker1);
     desg = '';
    }
    if(hDate == "")
    {
        toasterrormsg("Select To Holiday Date");
        return false;
    }
    if(hname == "")
    {
        toasterrormsg("Enter Holiday Description");
        return false;
    }
        $.ajax({
            type:"POST",
            url:"ajax/holiday_ajax.php",
            data:{type:type, hDate:hDate, loc:loc, hname:hname, comp:comp, bus:bus, subbus:subbus, wloc:wloc
            , func:func, subfunc:subfunc, cost:cost, proc:proc, grd:grd, desg:desg},
            success: function(result){
                if(result ==1){
                    toastmsg("Successfully Holiday inserted")
                }else{
                     toasterrormsg("Error in insertion");
                }
            }
        });
   


}
function mymultiselect(){
$('a.multiselect-all').click(function(){
    var meraChecker = $(this).find('span').hasClass('checked')
if(meraChecker==true){
 $('.checker').children('span').addClass('checked'); 
}
else{
 $('.checker').children('span').removeClass('checked'); 
}    
    
});
}