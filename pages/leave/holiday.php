<?php
session_start();
//include "../db_conn.php";
include ('../db_conn.php');
include ('../configdata.php');
if((!isset($_SESSION['usercode']) || $_SESSION['usercode']=="")&& (!isset($_SESSION['usertype']) || $_SESSION['usertype']=="")){
	header('location: ../login/index.php');
}
$c_val["Admin"]["Announcement and Notification"]["Company Announcement"] = 'end1234';
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8"/>
    <title>Sequel- HRMS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- BEGIN PAGE STYLES -->
  
    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
    <link href="../../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/jstree/dist/themes/default/style.css"/>

    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" href="../../assets/admin/layout2/css/bootstrap-multiselect.css" type="text/css"/>

    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="../../assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="../../assets/admin/layout2/css/kunal.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->

    <link href="../css/toastr.css"  rel="stylesheet" type="text/css"/>
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../css/jquery-ui.css">
     <link href="../Attendance/css/sol.css"  rel="stylesheet" type="text/css"/>


    <link rel="shortcut icon" href="favicon.ico"/>
    <style type="text/css">
    ul.multiselect-container.dropdown-menu li:nth-child(1) {
    padding: 0;
}
    ul.multiselect-container.dropdown-menu li {
    padding: 0 0 0 20px;
}
</style>
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>

</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white custom-layout">
<!-- BEGIN HEADER -->
<?php  include('../include/header.php'); ?>

<div class="clearfix">
</div>
<div class="page-content-wrapper cus-dark-grey">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper modified">

            <div class="page-sidebar navbar-collapse collapse cus-dark-grey">

                <?php include('../include/leftMenu.php') ;?>

            </div>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
       
                <!-- BEGIN PAGE HEADER-->
                
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                
                <?php  include('content/holiday_content.php'); ?>

                <!-- END PAGE CONTENT -->
           
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
        <!--Cooming Soon...-->
        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php include('../include/footer.php') ?>
    <!-- END FOOTER -->
</div>
<script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="../../assets/admin/pages/scripts/table-managed.js"></script>

<script type="text/javascript" src="../../assets/admin/layout2/scripts/bootstrap-multiselect.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/moment.js"></script>
<script src="../js/toastr.js"></script>
<script src="../js/common.js"></script>
<script src="js/holiday.js"></script>
<script src="../Attendance/js/sol.js"  type="text/javascript"></script>
<script src="js/jquery.table2excel.js"></script>
<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
      // FormSamples.init();
        //ComponentsEditors.init();
        TableManaged.init();

    });
//     $('li.multiselect-all').mouseup(function(){
//     var meraChecker = $(this).find('span').hasClass('checked')
// if(meraChecker==true){
//  $('.checker').children('span').addClass('checked'); 
// }    
// });

    $('#holiDate').datepicker( {
            dateFormat : 'dd/mm/yy'
            
        // duration: 'fast'
        // }).focus(function() {
        //   $(".ui-datepicker-prev, .ui-datepicker-next").remove();
        
        });
      $(function() {
       // alert('1');
        getTables1('CompMast','COMP','company-select','1','a');
        getTables1('BussMast','buss','business-select','1','a');
        getTables1('subBussMast','subBuss','sub-business-select','1','a');
        //getTables('locMast','LOC','location-select','1','a');
        getTables1('WorkLocMast','WLoc','work-location-select','1','a');
        getTables1('FUNCTMast','FUNCT','function-select','1','a');
        getTables1('SubFunctMast','SubFunct','sub-function-select','1','a');
        getTables1('CostMast','cost','cost-master-select','1','a');
        getTables1('PROCMAST','PROC','process-select','1','a');
        getTables1('GrdMast','GRD','grade-select','1','a');
        getTables1('DsgMast','DSG','designation-select','1','a');

     // $('#my-default-text-select').searchableOptionList({
     //            showSelectAll: true,
     //             maxHeight: '250px',
     //              texts: {
     //                searchplaceholder: 'Select Employee'
     //            }
     //        });
     $('#my-default-text-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,

        });
   });  
    
    $('#loc').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,

        });
    $(window).load(function(){
    $('#locdiv').find('.multiselect-selected-text').text('Select Location');
    mymultiselect();
});
</script>




<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

