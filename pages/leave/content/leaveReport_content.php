<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Leave Reports
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form  enctype="multipart/form-data" id="form" name="reportform" class="form-horizontal form-row-seperated">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                       
                                            <select class="form-control" onchange="showfunction(this.value)" style="margin-top: 25px;" id="mainselect">
                                            <option value="">Select</option>
                                                <?php
                                                    
                                                if(Is_mngr($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
                                                    $usertypevalue =2;
                                                    $sql_special="select title from report_query where c_ID='2' AND view_mngr=1";
                                                }else if(Is_emp($_SESSION['usercode'],$query,$pa,$opt,$ms_db)){
                                                    $usertypevalue =3;
                                                    $sql_special="select title from report_query where c_ID='2' AND view_emp=1 ";
                                                }else{
                                                    $usertypevalue =1;
                                                    $sql_special="select title from report_query where c_ID='2'";
                                                }
                                                                
                                                $result_special=query($query,$sql_special,$pa,$opt,$ms_db);
                                                while($row_special = $fetch($result_special)){ ?>
                                                <option value="<?php echo $row_special['title'] ?>">
                                                    <?php echo $row_special['title'] ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                       
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group" id="multisearch2" style="display: none">    
                                    <div class="col-md-12" >
                                        <div class="col-md-4 selectKunal" style="margin-top: 25px;">
                                            <select id="sub-function-select" name="multiselect[]" multiple="multiple" style="width: 300px" onchange="hideDiv()"  >
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" style="margin-top: 25px;">
                                            <select id="cost-master-select" name="multiselect[]" multiple="multiple" style="width: 300px" onchange="hideDiv()"  >
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" style="margin-top: 25px;">
                                            <select id="process-select" name="multiselect[]" multiple="multiple" style="width: 300px" onchange="hideDiv()"  >

                                                  
                                            </select>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group" id="multisearch3" style="display: none">    
                                    <div class="col-md-12" >
                                        <div class="col-md-4 selectKunal" style="margin-top: 25px;">
                                            <select id="grade-select" name="multiselect[]" multiple="multiple" style="width: 300px" onchange="hideDiv()"  >
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" style="margin-top: 25px;">
                                            <select id="designation-select" name="multiselect[]" multiple="multiple" style="width: 300px" onchange="hideDiv()">
                                                
                                            </select>
                                        </div>
                                        <div class="col-md-4 selectKunal" style="margin-top: 25px;">
                                            <button type="button" class="btn  bg-blue" onclick="attReport();">Go
                                            </button>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <div class="form-group">    
                                    <div class="col-md-12" style="margin-top: 25px;" >
                                        <button type="button" style="float: right" class="btn  bg-blue" onclick="downloadReport('<?php echo $_SESSION['usercode']; ?>',<?php echo $usertypevalue; ?>);">Generate Report
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>