<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content cus-light-grey">

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Compensatory Off Request
                                    </div>
                                   
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="#" class="form-horizontal" id="compOffForm">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Work Done</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" name="fromDate" id="fromDate" placeholder="dd/mm/yyyy" onchange="get_workday('<?php echo $code;?>',this.value);">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Day</label>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" name="nodays11" 
                                                     value='' id="nodays11" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Reason</label>
                                                <div class="col-md-4">
                                                    <input type="textarea" name="reason" id="reason" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" style="text-align:left">Attendance For This Date</label>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <div id="time_val">
                                                        <table width="100%" cellpadding="0" cellspacing="2" border="0" id="detailtable">
                                                            <tbody>
                                                            <tr>
                                                                <th style="text-align:left">Day Type</th>
                                                                <th style="text-align:left">Actual In Time</th>
                                                                <th style="text-align:left">Actual Out Time </th>
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td style="text-align:left"></td>
                                                                <td style="text-align:left"></td>
                                                                <td style="text-align:left"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php 
                        $sqlq="select * from WorkFlow WHERE WFFor='Leave' and WEvents='Comp-off' order by WorkFlowID DESC ";

                        $resultq = query($query,$sqlq,$pa,$opt,$ms_db);
                        $row = $fetch($resultq);
                        $workflowmethod = $row['AppMethod'];
                        $sqlr="Select * from hrdmastQry where Emp_Code='$code'";
                        $resultr = query($query,$sqlr,$pa,$opt,$ms_db);
                        $rowr = $fetch($resultr);
                        $code1 = $rowr['MNGR_CODE'];
                        
                        ?>
                        <input type="hidden" name="wfmethod" id="wfmethod" value="<?php echo $workflowmethod; ?>">
                        <input type="hidden" name="autoapprover" id="autoapprover" value="<?php echo $code1; ?>">
                        <input type="hidden" id="levellist">
                        <input type="hidden" id="approver1"  value="<?php echo $mngrcode;?>">
                        <input type="hidden" id="levellist1" value="1; ;Reporting Manager">
                         <?php if($workflowmethod !='automatic'){ ?>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                   
                                                    <label class="col-md-3 control-label"><?php echo APPROVER_TEXT;?></label><strong> <span id="showlevel"></span></strong>
                                                </div>
                                            </div>
                        <?php } ?>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="button" id="subBtn" onclick="submitCompOff('<?php echo $code; ?>','<?php echo $mngrcode;?>');" class="btn btn-circle blue">Submit</button>
                                                        <button type="button" class="btn btn-circle default">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>