
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
       <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog lg">
        <!-- modal-content -->
        <div class="modal-content" >
            <div class="modal-header portlet box blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title white-txt"><div class="caption"><b>Approve Leave Request</b></div></h4>
            </div>
            <div class="modal-body" id="approveLevrequest">                
                <?php //include ("content/view_myodrequest.php"); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
        <!-- /.modal -->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Approve Leave Request
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">

                            </a>
                            
                        </div>
                    </div>
                     <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="btn-group selectAlign">
                                        <select class="form-control" onchange="getRelatedSelectbox(this.value);">
                                            <option value="">Select....</option>
                                            <option value="1">Request Applied On</option>
                                            <option value="2">Request Status</option>
                                            <option value="3">By Requester Name</option>
                                                                                    </select>
                                    </div>

                                    <div class="btn-group" id="monthlySearch" style="display: none;margin-left: 45px;">
                                        
                                        <div class="col-md-5">
                                        <label class="control-label">
                                            From Date                
                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="text" class="form-control date_for_search" name="fromDate" id="fromDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-5">  
                                        <label class="control-label">
                                            To Date                
                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="text" class="form-control date_for_search" name="toDate" id="toDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-2" style="margin-top: 25px;">
                                        <button class="btn submit" onclick="searchByDate('<?php echo $code;?>');">Go</button>
                                        </div>
                                     
                                    </div>

                                    <div class="btn-group" id="actionSearch" style="display: none;margin-left: 45px;">
                                        <select class="form-control" onchange="searchByStatus(this.value,'<?php echo $code;?>');">
                                            <option value="" >Select ..</option>
                                            <?php
                                            $sql="select * from LOVMast where LOV_Field='status'";
                                            $result=query($query,$sql,$pa,$opt,$ms_db);
                                            while ($row = $fetch($result)){
                                                ?>
                                                <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                            <?php } ?>

                                        </select>
                                    </div>

                                    <div class="btn-group" id="bynameSearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-10">
                                            <input type="text" name="byname" class="form-control pull-left" id="col5_filter" placeholder="Enter Emp code or Name">
                                        </div>
                                            
                                      
                                    </div>

                                 <div class="btn-group" id="byRequesterSearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-10">
                                             <select class="form-control" name="byRequest"  id="byRequest" onchange="serchByCodeName('byRequest');">
                                             <option value="0">Select ..</option>
                                            <?php
                                                $sqlCrea="select distinct CreatedBy from leave where ApprovedBy='$code' and flag='1' ";




                                            $resCrea=query($query,$sqlCrea,$pa,$opt,$ms_db);
                                                while ($rowCrea = $fetch($resCrea)) {
                                                  $arrEmp[]="'" .$rowCrea['CreatedBy'] . "'";
                                                }

                                                $strEmp =  implode(",", $arrEmp) ;

                                                $sqlEmp="Select Emp_Code,EMP_NAME from HrdMastQry where Emp_Code in ($strEmp) ";
                                                $resEmp=query($query,$sqlEmp,$pa,$opt,$ms_db);
                                                while ($rowEmp =$fetch($resEmp)) {?>
                                                    <option value="<?php echo $rowEmp['Emp_Code']?>"><?php echo $rowEmp['EMP_NAME']." (".$rowEmp['Emp_Code'].")";?></option> 
                                              <?php  }
                                                
                                                 ?> 
                                             </select>
                                          
                                        </div>
                                             
                                      
                                    </div>

                                </div>
                            </div>

                        </div>
			 <div style="float: right;padding: 2px;">
                     <button id="btnExport" onclick="fnExcelReport('sample_2');"> EXPORT </button>
                     <!--<input type="button" id="exportpdf" onclick="pdfexport();" value="Download PDF">-->
                    
                     </div>
                     <iframe id="txtArea1" style="display:none"></iframe>

                            <?php
                            if($code == "admin" || Is_hr($code,$query,$pa,$opt,$ms_db)) {
                                if(isset($_GET['page']) && !empty($_GET['page'])) {
                                    $limitStart = $_GET['page']  * 50;
                                    $offsetLimit = $limitStart + 50;                              
                                } else {
                                    $limitStart = 0;
                                    $offsetLimit = 50;
                                }
                                $sample_id = '';
                                $total_record_sql="select count(*) as records from leave where flag='1'";
                                $total_record = query($query,$total_record_sql,$pa,$opt,$ms_db);
                                while($total_rows = $fetch($total_record)) {
                                     $records_num_rows = $total_rows['records'];
                                }
                                $sql="SELECT *,convert(varchar(10),LvFrom,103)as LvFrom, 
                                convert(varchar(10),LvTo,103)as LvTo,
                                convert(varchar(27),updation_date)as updation_date,convert(varchar(27),userUpdatedOn) as userUpdatedOn FROM ( SELECT ROW_NUMBER() OVER(ORDER BY leaveID) AS ROWNUM, * FROM leave $whereEmp) leave1 WHERE ROWNUM BETWEEN $limitStart AND $offsetLimit AND flag='1'";

                                // $sql="select *,convert(varchar(10),LvFrom,103)as LvFrom, 
                                // convert(varchar(10),LvTo,103)as LvTo,
                                // convert(varchar(27),updation_date)as updation_date,convert(varchar(27),userUpdatedOn)as userUpdatedOn from leave where flag='1'";
                            } else {
                                $sample_id = 'sample_2';
                                $sql="select *,convert(varchar(10),LvFrom,103)as LvFrom, 
                                convert(varchar(10),LvTo,103)as LvTo,
                                convert(varchar(27),updation_date)as updation_date,convert(varchar(27),userUpdatedOn)as userUpdatedOn from leave where ApprovedBy='$code' and flag='1' order by Trn_Date desc";
                            }

                            $res=query($query,$sql,$pa,$opt,$ms_db);
                            $len = $num($res);
                            
                            $i=0;
                            ?>
                         <table class="table table-striped table-bordered table-hover" id="<?php echo $sample_id; ?>">
                            <thead>
                           <tr><th></th>
                                <th class="table-checkbox">
                                    <input type="checkbox" id="Allcheck" value="0" 
                                    onclick="allCheck('Allcheck','<?php echo $code;?>');" class="group-checkable" data-set="#sample_2 .checkboxes"/>
                                </th>

                                <th>
                                    From Date
                                </th>

                                <th>
                                    To Date
                                </th>
								<th>
                                    Applied Leave
                                </th>
                                 <th>
                                   Requested By
                                </th>
                                <th>
                                   Category
                                </th>
                                <th>
                                    No. Of Days
                                </th>
								

                                <th>
                                    Reason
                                </th>
                                 <th>
                                    Approved On
                                </th>
                                 <th>
                                    Approver Remarks
                                </th>

                               

                                <th>
                                    Status
                                </th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody id="searchData">
                            <?php
                                while ($row = $fetch($res)){
                            ?>

                            <tr class="odd gradeX"><td></td>
                            <?php if( $row['status']== 2 || $row['status']== 3 || $row['status']== 4 || $row['status']== 5){?>
                                    <td></td>
                                <?php }else { ?>
                                <td>
                                <input type="checkbox" class="checkboxes" id="<?php echo "Mulcheck".$i;?>" onclick="mulCheck('<?php echo "Mulcheck".$i;?>','<?php echo "key".$i;?>');" value="<?php echo $row['leaveID'];?>"/>
                                </td>
                                <?php } ?>
                                
                                <td>
                                    <?php echo $row['LvFrom'];?>
                                </td>
                                <td>
                                    <?php echo $row['LvTo'];?>
                                </td>
								<td>
                                    <?php echo $leave_class_obj->getemployee_leave_type($row['LvType']) ;?>
                                </td>
                                 <td >
                                    <?php
                                    $usercode=$row['CreatedBy'];
                                    $sql1="select EMP_NAME,GRD_NAME from HrdMastQry WHERE Emp_Code='$usercode'";
                                    $res1=query($query,$sql1,$pa,$opt,$ms_db);
                                    $data1=$fetch($res1);
                                    $cate=$data1['GRD_NAME'];
                                    echo $data1['EMP_NAME']." (".$usercode.")";
                                    ?>

                                </td>
                                  
                                <td>
                                    <?php
                                   
                                    echo $cate;
                                    ?>

                                </td>
                                <td>
                                    <?php echo $row['LvDays'];?>
                                </td>
                                <td>
                                    <?php echo $row['reason'];?>

                                </td>
                                   <td>

                        <?php if($row['status']  == "2" || $row['status']  == "3" || $row['status'] == "5" || $row['status'] == "6" || $row['status'] == "7")
                 {
                  echo $row['updation_date'];
                  //echo'1';
                 }
                         ?>
                    </td>
<td>
<?php
if($row['status']  == "2" || $row['status']  == "3" || $row['status'] == "5" || $row['status'] == "6" || $row['status'] == "7")                 {
                         echo $row['remark'];
            }
                         ?>
                    </td>
                                

                               
                                <td> <?php if($row['status'] == "1"){?>
										<a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprleaveId('<?php echo $row['leaveID'];?>','1','<?php echo $code;?>');">
                                <span class="label bg-blue-steel">
                                Pending </span>
                                    <?php } else if($row['status'] == "2") {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprleaveId('<?php echo $row['leaveID'];?>','1','<?php echo $code;?>');">
                                <span class="label label-success">
                                Approved </span>
                                    <?php } else if($row['status'] == "3") {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprleaveId('<?php echo $row['leaveID'];?>','1','<?php echo $code;?>');">
                                <span class="label label-danger">
                                Rejected </span>
                                    <?php } else if($row['status'] == "4") {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprleaveId('<?php echo $row['leaveID'];?>','1','<?php echo $code;?>');">
                                <span class="label bg-grey-cascade">
                                Cancel </span>
                                 <?php } else if($row['status'] == "5") {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprleaveId('<?php echo $row['leaveID'];?>','1','<?php echo $code;?>');">
                                <span class="label bg-blue-steel">
                                Approved Cancel </span>
                                    <?php }

else if($row['status'] == "6") {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprleaveId('<?php echo $row['leaveID'];?>','1','<?php echo $code;?>');">
                                <span class="label label-success">
                                Approved Cancellation </span>
                                    <?php }
                                    else if($row['status'] == "7") {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprleaveId('<?php echo $row['leaveID'];?>','1','<?php echo $code;?>');">
                                <span class="label label-danger">
                                Approved Cancel Rejected </span>
                                    <?php }

                                    ?>
                                       
                                </td>

                                <td><input type="hidden" value="
                                <?php echo $row['Levkey'];?>" 
                                name="key" id="key<?php echo $i;?>">
                                </td>
                            </tr>

                            <?php $i++; } ?>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-md-6">
                            <textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
                        </div>
						  <div class="col-md-6">
                        <label>
                            <input type="radio" name="action_radio_all"  class="icheck" id="action_radio_all" value="0"> Unplanned</label>
                              <label>
                            <input type="radio" name="action_radio_all"  class="icheck" id="action_radio_all" value="1"> Planned</label>
                        </div>
                            <div class="col-md-8" style="float: right;">
                            <label id="hide_LeaveReasons_combo1" style="display: none;" class="col-md-5">
                                <select class="form-control" id="LeaveReasonscombo1" onchange="change_LeaveReasons1();">
                                            <option value="" >Select ..</option>
                                            <?php
                                            $sql6="select * from LOVMast where LOV_Field='LeaveReasons'";
                                            $result6=query($query,$sql6,$pa,$opt,$ms_db);
                                            while ($row6 = $fetch($result6)){
                                                ?>
                                                <option value="<?php echo $row6['LOV_Value']?>"><?php echo $row6['LOV_Text']?></option>
                                            <?php } ?>

                                        </select></br>
                            </label>
                             <label id="hide_LeaveReasons_text1" style="display: none;" class="col-md-5">
                                <input type="text" name="othercombo" class="form-control" id="othercombo1" placeholder="Enter Your Unplanned Reason">
                                  </br>         
                            </label>
                           </div>
                        </div>
                            <div class="col-md-12">
                                <div class="col-md-offset-4 col-md-4 col-md-offset-4" style="margin-top: 20px;">
                                <input type="hidden" value="" id="selectedcheckbox" />
                                <input type="hidden" name="key" id="key" value="">

                                <select class="form-control" id="selected_status_div" onchange="showSubmitButton(this.value,'<?php echo $len;?>');">
                                    <option value="">Select Action..</option>
                                    <?php
                                        $sql="select * from LOVMast where LOV_Field='status' and LOV_Value in ('2','3')";
                                        $result=query($query,$sql,$pa,$opt,$ms_db);
                                       while ($row = $fetch($result)){
                                    ?>
                                    <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                    <?php } ?>
                                </select>
                                    </div>
                                <div class="col-md-offset-4" id="submitButtonDiv" style="display: none; margin-top: 19px;">
                                    <button class="btn btn-primary" id="submitButton" value="" onclick="submitApprovelRequest('selectedcheckbox','selected_status_div','0');">Submit</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
                <?php 
                    if($code == "admin" || Is_hr($code,$query,$pa,$opt,$ms_db) && $records_num_rows > 0):
                ?>
                <ul class="pagination pull-right" id="pagination2">
                <?php
                    if(isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] != 1):
                ?>
                    <li><a href="?page=1">FIRST</a></li>
                    <li><a href="?page=<?php echo ($_GET['page'] -1); ?>">&laquo;</a></li>
                    <?php
                    endif;
                    if(isset($_GET['page']) && !empty($_GET['page'])) {
                        $page = $_GET['page'];
                    } else {
                        $page = 1;
                    }
                    for($i= $page; $i< $page+8; $i++) {
                        if($page > floor($records_num_rows/50))
                            break;
                        ?>
                        <li><a href="?page=<?php echo $i; ?>"><?php echo $i; ?> </a></li>
                        <?php
                                    }
                    ?>
               <?php
                    if(isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] != floor($records_num_rows/50)):
                ?>
                    <li><a href="?page=<?php echo ($_GET['page']+1); ?>">&raquo;</a></li>
                <?php endif; ?>
                <?php if($_GET['page'] < floor($records_num_rows/50)): ?>
                     <li><a href="?page=<?php echo floor($records_num_rows/50); ?>">LAST</a></li>
                <?php endif; ?>
                </ul>
<!--                 <script> 
                     document.getElementById('sample_2_paginate').style.display = 'none';
                </script> -->
                <?php
                endif;
                ?>
            </div>
        </div>

        <!-- END PAGE CONTENT-->

    </div>
</div>