<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <!--modal-dialog -->
        <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog lg">
                <!-- modal-content -->
                <div class="modal-content" >
                    <div class="modal-header portlet box blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title white-txt"><div class="caption"><b>My Special Leave Request </b></div></h4>
                    </div>
                    <div class="modal-body" id="specialleave">
                        <?php //include ("content/view_myodrequest.php"); ?>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- /.modal-dialog -->
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN CONDENSED TABLE PORTLET-->
                <div class="portlet box blue-madison">

                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>My Special Leave Request
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="reload">
                            </a>
                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                           

                        </div>
                         <div style="float: right;padding: 2px;">
                     <button id="btnExport" onclick="fnExcelReport('sample_2');"> EXPORT </button>
                     <!--<input type="button" id="exportpdf" onclick="pdfexport();" value="Download PDF">-->
                    
                     </div>
                     <iframe id="txtArea1" style="display:none"></iframe>
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <?php
                            if($code == 'admin')
                            {
                               $sql="select CONVERT(VARCHAR(10),LeavTran.EffictiveDate,103) as EffictiveDate,LeavTran.LevYear,LeavTran.LvType,LeavTran.LvDays,CONVERT(VARCHAR(20),leave_Trans_history.timestamp,120) as timestamp,LeavTran.flag,LeavTran.Emp_Code,leave_Trans_history.Addedby,LeavTran.trankey,leave_Trans_history.remark from LeavTran,leave_Trans_history where LeavTran.trankey=leave_Trans_history.leavtran and LeavTran.flag=leave_Trans_history.status";
                            }
                            else
                            {
                                 $sql="select CONVERT(VARCHAR(10),LeavTran.EffictiveDate,103) as EffictiveDate,LeavTran.LevYear,LeavTran.LvType,LeavTran.LvDays,CONVERT(VARCHAR(20),leave_Trans_history.timestamp,120) as timestamp,LeavTran.flag,LeavTran.Emp_Code,leave_Trans_history.Addedby,LeavTran.trankey,leave_Trans_history.remark from LeavTran,leave_Trans_history where LeavTran.trankey=leave_Trans_history.leavtran and LeavTran.flag=leave_Trans_history.status and leave_Trans_history.createdby='$code'";
                            }
                            $res=query($query,$sql,$pa,$opt,$ms_db);
                            $len = $num($res);

                            ?>
                            <thead >
                            <tr>
                                <th>
                                    Applied On
                                </th>
                                 <th>
                                    Leave Year
                                </th>
                                <th>
                                    Effective Date
                                </th>
                                  <th>
                                    Approved By
                                </th>
                                 <th>
                                    Created By
                                </th>
                                <th>
                                    No. Of Days
                                </th>

                                <th>
                                    Type
                                </th>
                                 <th>
                                    Approved On
                                </th>
                                 <th>
                                    Approved Remark
                                </th>
                              
                                <th>
                                    Status
                                </th>
                            </tr>
                            </thead>
                            <tbody id="searchMyData">
                            <?php

                            while ($row = $fetch($res)){
                                $temp=$row['trankey'];
                                $sql1="select top 1 CONVERT(VARCHAR(20),timestamp,120) as timestamp from leave_Trans_history where leavtran='$temp' order by Id asc";
                                    $res1=query($query,$sql1,$pa,$opt,$ms_db);
                                    $row1 = $fetch($res1);
                                ?>
                                <tr>
                                    <td>
                                    <?php echo dateTimeFormat($row1['timestamp']);?>
                                      
                                    </td>
                                    <td>
                                        <?php echo $row['LevYear'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['EffictiveDate'];?>
                                    </td>
                                     <td>
                                        <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($row['Addedby']))))). "(".$row['Addedby'].")";?>
                                    </td>
                                     <td>
                                        <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($row['Emp_Code']))))). "(".$row['Emp_Code'].")";?>
                                    </td>
                                    <td>
                                        <?php echo $row['LvDays'];?>
                                    </td>
                                    <td>
                                        <?php echo $leave_class_obj->getemployee_leave_type($row['LvType']);?>
                                    </td>
                                     <td>
                                        <?php  if($row['flag'] != "1"){echo dateTimeFormat($row['timestamp']);}?>
                                    </td>
                                     <td>
                                        <?php echo $row['remark'];?>
                                    </td>
                                    <td>
                                        <?php if($row['flag'] == "1" || $row['flag'] == ""){?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial('<?php echo $row['trankey'];?>','1');">
                                <span class="label  bg-blue-steel">
                                Pending </span>
                                            </a>
                                        <?php } else if($row['flag'] == "2") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial('<?php echo $row['trankey'];?>','2');">
                                <span class="label label-success">
                               Approved
                                 </span>
                                            </a>
                                        <?php } else if($row['flag'] == "3") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial('<?php echo $row['trankey'];?>','3');">
                                <span class="label label-danger">
                               Rejected
                             </span>
                                            </a>
                                        <?php } else if($row['flag'] == "4") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial('<?php echo $row['trankey'];?>','4');">
                                <span class="label bg-grey-cascade">
                               Cancelled
                             </span>
                                            </a>
                                             <?php } else if($row['flag'] == "5") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial('<?php echo $row['trankey'];?>','4');">
                                <span class="label bg-grey-cascade">
                               Approved Cancel
                             </span>
                                            </a>
                                             <?php } else if($row['flag'] == "7") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial('<?php echo $row['trankey'];?>','4');">
                                <span class="label bg-grey-cascade">
                               Approved Cancel Rejected
                             </span>
                                            </a>
                                             <?php } else if($row['flag'] == "6") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial('<?php echo $row['trankey'];?>','4');">
                                <span class="label bg-grey-cascade">
                               Approved Cancellation
                             </span>
                                            </a>
                                        <?php }?>

                                    </td>

                                </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>

                </div>


                <!-- END CONDENSED TABLE PORTLET-->


            </div>
        </div>

    </div>
</div>

