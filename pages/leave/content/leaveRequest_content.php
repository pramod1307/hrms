<style type="text/css">
  
.outer {position:relative !important;height: 205px;
    overflow-x: auto;}
.inner {
  /*overflow-x:scroll !important;
  overflow-y:visible !important;*/
  /*width:400px !important;
  margin-left:10px !important;*/
}
</style>
<?php 
$special_leave=$leave_class_obj->special_leave($code);

$currentyear_num=$special_leave[0];
$special_leave_val=$special_leave[1];
//echo $currentyear_num;
// ($ro= $fetch($res)) {
$hrdsql = "select GRD_CODE from hrdmastqry where Emp_Code='$code'";
    $hrdres = query($query,$hrdsql,$pa,$opt,$ms_db);
    $hrdrow=$fetch($hrdres);
$grdtype=$hrdrow['GRD_CODE'];
 ?>


<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content cus-light-grey">

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    
                                    <div class="caption">
                                       <!-- Apply For Leave -->
                                    </div>
                                  
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="#" class="form-horizontal" id="leaveForm">
                                    <input type="hidden" name="empcode" id="empcode" value="<?php echo $code;?>">
                                        <div class="form-body" >
                                        <div class="col-md-8">
                                        <?php if($grdtype=='1')
                                        {?>
                                            <div class="outer">
                                                                  <div class="inner">
                                                                    <table class="table table-striped table-bordered table-hover">
                                                                        <tr style="background-color: #3ab174;
    font-weight: 800;font-size: 10px;">
                                                                          <th>Leave Type</th>
                                                                          <td>-Ve Allowed</td>
                                                                          <td>Half Day</td>
                                                                          <td>Minimum Days</td>
                                                                          <td>Maximum Days</td>
                                                                          <td>Leave Not Club With</td>
                                                                           <td>Valid Till</td>
                                                                        </tr>
                                                                        <tr style="font-size: 10px;">
                                                                          <th>Earned Leave (EL)</th>
                                                                          <td> Yes</td>
                                                                          <td>No</td>
                                                                          <td>3</td>
                                                                          <td>-</td>
                                                                          <td>-</td>
                                                                           <td>-</td>
                                                                        </tr>
                                                                        <tr style="font-size: 10px;">
                                                                          <th>Casual Leave (CL)</th>
                                                                          <td>No</td>
                                                                          <td>Yes</td>
                                                                          <td>0.5</td>
                                                                          <td>2</td>
                                                                          <td>-</td>
                                                                           <td>-</td>
                                                                        </tr>
                                                                        <tr style="font-size: 10px;">
                                                                          <th>Sick Leave (SL)</th>
                                                                          <td>No</td>
                                                                          <td>Yes</td>
                                                                          <td>0.5</td>
                                                                          <td>-</td>
                                                                          <td>-</td>
                                                                           <td>-</td>
                                                                        </tr>
                                                                        <tr style="font-size: 10px;">
                                                                          <th>Birthday/ Anniversary Leave</th>
                                                                          <td>-</td>
                                                                          <td>-</td>
                                                                          <td>0.5</td>
                                                                          <td>1</td>
                                                                          <td>-</td>
                                                                           <td>-</td>
                                                                        </tr>
                                                                        <tr style="font-size: 10px;">
                                                                          <th>Comp-off</th>
                                                                          <td>No</td>
                                                                          <td>Yes</td>
                                                                          <td>0.5</td>
                                                                          <td>-</td>
                                                                          <td>-</td>
                                                                           <td>30 days</td>
                                                                        </tr>
                                                                        <tr style="font-size: 10px;">
                                                                          <th>Shutdown Leave (SEL)</th>
                                                                          <td>No</td>
                                                                          <td>No</td>
                                                                          <td>-</td>
                                                                          <td>-</td>
                                                                          <td>-</td>
                                                                           <td>Next Shutdown</td>
                                                                        </tr>
                                                                    </table>                                           </div>
                                                                </div>
                                        <?php } ?>
                                                               <div class="portlet-title">
                                    
                                    <div class="caption" style="background: #4B8DF8;color: white;margin: 15px 0px 18px 0px;height: 30px;">
                                       <div style="font-size: 14px;margin-left: 10px;">Apply For Leave</div>
                                    </div>
                                  
                                </div>                                              
					                                 <div class="row">
                                            <div class="col-md-9">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">From Date</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="fromDate" id="fromDate" placeholder="dd/mm/yy" onchange="getAppliedDays();">
                                                </div> 
                                            </div>

                                            <div class="form-group" id="showforD" style="display:none">
                                                <label class="col-md-3 control-label">
                                                   
                                                </label>
                                                <div class="input-group col-md-9">
                                                    <div class="icheck-inline">
                                                        <label>
                                                        <input type="radio" name="radio1" id="radio1" value="1FH" onclick="makehalf();" class="icheck"> First Half</label>
                                                        <label>
                                                        <input type="radio" name="radio1" id="2fh" class="icheck" value="2FH" onclick="makehalf();"> Second half </label>
                                                        <label>
                                                        <input type="radio" name="radio1" id="fullDay" class="icheck" value="2FD" onclick="makeFull();" checked> Full Day </label>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                 <div  id="multipleDate">
                                                    <label class="col-md-3 control-label">To</label>
                                                    <div class="col-md-9">
                                                    <input type="hidden" id="dateCriteriadiv">
                                                    <input type="text" class="form-control" name="toDate" id="toDate" placeholder="dd/mm/yy"
                                                    onchange="getToDate();myAttendanceByDAte('<?php echo $code;?>','fromDate','toDate');" disabled>
                                                    <div id="err_attendance" style="color:red;"></div>
                                                        
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group" id="halfshow" style="display:none">
                                                <label class="col-md-3 control-label">
                                                       
                                                </label>
                                                <div class="input-group col-md-9">
                                                    <div class="icheck-inline">
                                                        <label>
                                                        <input type="radio" name="radio2"  class="icheck" id="1th" onclick="maketohalf();"> First Half</label>
                                                        <label>
                                                        <input type="radio" name="radio2" class="icheck" id="2th" onclick="maketohalf();"> Second half </label>
                                                        <label>
                                                        <input type="radio" name="radio2" id="fulltoDay" class="icheck" value="2FD" onclick="makeFull();" checked> Full Day </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none">
                                                <label class="col-md-3 control-label">
                                                       Leave Request For
                                                </label>

                                                <div class="input-group col-md-9">
                                                <input type="hidden" name="leaveFor" id="leaveFor" value="<?php echo $code;?>">
                                                    <div class="icheck-inline">
                                                        <label>
                                                        <input type="radio" name="radio3" checked class="icheck" onclick="empDetails('<?php echo $code?>','MySelf');" value="<?php echo $code; ?>"> MySelf</label>
                                                        
                                                        <?php 
                                                            $sql="select count(Emp_Code) as number from Hrdmastqry where MNGR_CODE='$code'";
                                                            $res=query($query,$sql,$pa,$opt,$ms_db);
                                                            $val=$fetch($res);
                                                            if($val['number'] >= 1){
                                                        ?>
                                                        <label style="display:none">
                                                        <input type="radio" name="radio3" class="icheck" onclick="empDetails('<?php echo $code?>','MyTeam')" disabled> MyTeam </label>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="empdetail" style="display: none;">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-5">
                
                                                    <select class="form-control" id="empCheck" name="empCheck" onclick="setLeaveFor(this.value);">
                                                    <option value="">select </option>
                                                        <?php $sql="select * from Hrdmastqry where MNGR_CODE='$code'";
                                                         $result = query($query,$sql,$pa,$opt,$ms_db);
                                                        while($row = $fetch($result)) { ?>
                                                            <option  value="<?php echo $row['Emp_Code'];?>" >
                                                                <?php echo $row['EMP_NAME']; ?>
                                                            </option>
                                                            <?php } ?>
                                                    </select>  
                                                 </div>
                                            </div>
                                             <?php
                        $sql="select Levkey from Leave WHERE CreatedBy='$code' group by Levkey";
                        $res=query($query,$sql,$pa,$opt,$ms_db);
                          // $Levkey_array=array();
                            $countel=0;
                           while ($rows = $fetch($res)){
                           $Levkey_val=$rows['Levkey'];
                          // echo $Levkey_val."aaa";
                             $sql1="select TOP 1 leaveID,CONVERT (VARCHAR(10),LvFrom,103 ) as LvFrom,CONVERT (VARCHAR(10),LvTo,103 ) as LvTo,LvDays,reason,ApprovedBy,status,Levkey from Leave WHERE Levkey='$Levkey_val' and lvtype='1' order by leaveID DESC ";
                               $res1=query($query,$sql1,$pa,$opt,$ms_db);
                          // $Levkey_array=array();
                              while ($rows1 = $fetch($res1)){
                                 if($rows1['status'] == '2'){
                                     $countel++;
                                 }
                              
                              }
                           }  
      
      
   

                            ?>
                                            <input type="hidden" id="el_used" value='<?php echo $countel?>'>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Apply Leave Type</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="leaveType" id="leaveType" onchange="checkholidays(this.value,'<?php echo $code;?>');">
                                                    <option value="">Select Type</option>
                                                     <?php if($empType == 1 || $empType == 2 ){
                                                         $month = array("06", "11");
                                                         //echo $data['sex'];

                                                         $month_val = date('m');
                                                         if(in_array($month_val,$month)){
                                                             if($data['MStatus'] == '2'){
                                                                 if($data['Sex'] == '1'){
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='5' AND LOV_Value!='7' AND LOV_Value!='9'";
                                                                 }
                                                                 else{
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='5' AND LOV_Value!='9'";
                                                                 }

                                                             }
                                                             else{
                                                                 if($data['Sex'] == '1'){
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='7' AND LOV_Value!='6' AND LOV_Value!='9'";
                                                                 }
                                                                 else{
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='6' AND LOV_Value!='9'";
                                                                 }

                                                             }
                                                         }
                                                         else{
                                                             if($data['MStatus'] == '2'){
                                                                 if($data['Sex'] == '1'){
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='5' AND LOV_Value!='7'  AND LOV_Value!='9'";
                                                                 }
                                                                 else{
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='5'  AND LOV_Value!='9'";
                                                                 }

                                                             }
                                                             else{
                                                                 if($data['Sex'] == '1'){
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='7' AND LOV_Value!='6'   AND LOV_Value!='9'";
                                                                 }
                                                                 else{
                                                                     $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND LOV_Value!='6' AND LOV_Value!='9'";
                                                                 }

                                                             }
                                                         }

                                                        $sql.= " AND (LOV_OrdNo='1' or LOV_OrdNo='10' or LOV_OrdNo='22')"; 

                                                           $result = query($query,$sql,$pa,$opt,$ms_db);
                                                            while($row = $fetch($result)){ ?>
                                                            <option  value="<?php echo $row['LOV_Value'] ?>">
                                                           <?php echo $row['LOV_Text'] ?>
                                                       </option>

                                                       <?php }
                                                   }

                                                   else if($empType == 3){

                                                        $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND (LOV_OrdNo='2' or LOV_OrdNo='10' or LOV_OrdNo='22')";
                                                                
                                                           $result = query($query,$sql,$pa,$opt,$ms_db);
                                                            while($row = $fetch($result)){  
                                                        ?>
                                                       <option  value="<?php echo $row['LOV_Value'] ?>">
                                                           <?php echo $row['LOV_Text'] ?>
                                                       </option>
                                                       <?php } }
                                                        else if($empType == 4){

                                                        $sql="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' AND (LOV_OrdNo='3' or LOV_OrdNo='10')";
                                                                
                                                           $result = query($query,$sql,$pa,$opt,$ms_db);
                                                            while($row = $fetch($result)){  
                                                        ?>
                                                       <option  value="<?php echo $row['LOV_Value'] ?>">
                                                           <?php echo $row['LOV_Text']; ?>
                                                       </option>
                                                       <?php } }
                                                         if($currentyear_num >0)
                                                       {
                                                           $sql_special="select LOV_Value,LOV_Text from LOVMast where LOV_Field='leave' and LOV_Value='10'";
                                                                
                                                           $result_special = query($query,$sql_special,$pa,$opt,$ms_db);
                                                           $row_special = $fetch($result_special)
                                                           ?>
                                                       <option  value="<?php echo $row_special['LOV_Value'] ?>">
                                                           <?php echo $row_special['LOV_Text'] ?>
                                                       </option>
                                                       <?php
                                                       }
                                                       ?>
                                                       
                                                    </select>
                                                    <div id="leaveErr" style="color:red;"></div>
                                                </div>
                                                
                                                <span id="usenextleave" style="display: none; float:left"><br>
                                                    <div class="col-md-12">
                                                        <input type="checkbox" value="0" id="DeviationLeave" name='DeviationLeave' class="setDL"  onchange="getDLLevel()" />Do you want to use Earned Leave of upcoming Year?
                                                        <div id="error_properapprover" style="color:red"></div>
                                                    </div>
                                                </span>


                                            </div>


                                                <div class="col-md-12" id="uploadMultiple" style="display:none;">
                                                    <div class="col-md-6">
                                                    <label class="control-label">Upload Attachments
                                                    </label></br>
                                                    <input type="file" name="uploadfile[]" id="uploadfile" onChange="logoimage_Validation();"/>
                                                        </div>
                                                    <div class="col-md-6">
                                                        <input type="button" value="+" onclick="addMoreFile()" ><span id="dialoginvalid"  style="color: #FF0000"></span>
                                                        </div>
                                                    <br><br>
                                                    <div id="uploadgroup">
                                                        <br> <br>
                                                    </div>
                                                    <br>
                                                   <!-- <div class="col-md-1" style="float: right">
                                                        <input type="button" value="upload" onclick="getUploadVal()">
                                                    </div>-->
                                                </div>
                                            <span id="message"></span>
						             <div class="form-group">
                                                <label class="col-md-3 control-label">Reason</label>
                                                <div class="col-md-9">
                                                <textarea name="reason"  rows="3" cols="15" id="reason" class="form-control"></textarea>
                                                </div>
                                            </div>
                <?php
                      $sqlleave="select LOV_Active from LOVMast where LOV_Field='LeaveFlag' and LOV_Text='Employee'";
                      $resultleave=query($query,$sqlleave,$pa,$opt,$ms_db);
                      $rowleave = $fetch($resultleave);
                      $leaveplanshow = $rowleave['LOV_Active'];
if($leaveplanshow == 'Y'){
                    ?>

            <div class="form-group">
              <label class="col-md-3 control-label">Leave </label>
                <div class="col-md-9">
                  <input type="radio" name="leave_plan"  class="icheck" id="unplan" value="0"> Unplanned</label>
                  <label>
                  <input type="radio" name="leave_plan"  class="icheck" id="plan" value="1"> Planned</label>
                  <label id="hide_LeaveReasons_combo"style="display: none;">
                  <input type="hidden" value="<?php echo $leaveplanshow ?>" id="showplanunplan">
    
                  <br>
                  <select class="form-control" id="LeaveReasonscombo" onchange="change_LeaveReasons();">
                    <option value="" >Select Reason for unplanned leave</option>
                    <?php
                      $sql6="select * from LOVMast where LOV_Field='LeaveReasons'";
                      $result6=query($query,$sql6,$pa,$opt,$ms_db);
                      while ($row6 = $fetch($result6)){
                    ?>
                      <option value="<?php echo $row6['LOV_Value']?>"><?php echo $row6['LOV_Text']?></option>
                    <?php } ?>
                  </select></br>
                  </label>
                  <label id="hide_LeaveReasons_text"style="display: none;">
                    <input type="text" name="othercombo" class="form-control" id="othercombo" placeholder="Enter Your Unplanned Reason"></br>         
                  </label>
                </div>
            </div>
          
<?php }?>
          </div>
                                        <div class="col-md-3">
                                            <div class="dashboard-stat dashboard-stat-v2 red">
                                              <div class="col-md-12 red-bg white-txt z-depth-1" style="padding: 10px;font-size: 12px;border-radius: 3px!important;text-align: center;">
                                              <strong id="noOfDays" class="" name="noOfDays" style="font-size: 30px;border-radius: 3px!important;display: block;">0</strong> 
                                            <span style="text-align: center;display: block;">Days </span>
                                              
                                            <input type="hidden" id="noofdaysnew">
                                                    
                                            </div>
                                              </div>
                                              </div></div>
 
                                              </div>
                                        <div class="col-md-4">
                                          
                                            <div class="clearfix" style="margin-bottom: 10px;">
                                            </div>                                            <div class="z-depth-1 clearfix">
                                               <p class="blue-bg col-md-12 white-txt m-0" style=" padding: 10px 12px;">My Leave Balance Details</p>
												<div id="my_leave" class='col-md-12 p-0'>
                                                
												</div>

                                                

                                                <div>
                                                   <!-- <label>Privilage Leave:</label>&nbsp;&nbsp;&nbsp;
                                                   <strong id="pl">0.0</strong> -->
                                                </div>
                                            </div>
                                            <div class="clearfix" style="margin-bottom: 10px;">
                                            </div>
                                           <div class="z-depth-1">
                                               <div class="green-bg white-txt m-0 col-md-12" style=" padding:0;height: 40px;">
                                                    <div class="col-md-5" style="padding: 10px 0px 0 10px;">Holidays</div>
                                                    <div class="col-md-4 activeSachin" style="padding: 15px 19px 8px 0px;" id="upcomming_div">
                                                        <a id="upcome" onclick="Leave.upcome();" class="white-txt"> Upcoming </a>
                                                    </div>
                                                    <div class="col-md-3" style="padding: 15px 19px 8px 27px;" id="past_div">
                                                        <a id="pastcome" onclick="Leave.pastcome();" class="white-txt">Past</a>
                                                    </div>
                                                </div>
												 <div class="clearfix" style="margin-bottom: 10px;" id="show_holiday">
                                                </div>
                                            </div>
                                          
                                           <!--<p  style=" padding: 4px 0;"><a href="#" data-toggle="collapse" data-target="#demo" style="float: right;"> <span class="glyphicon glyphicon-chevron-down"></span> Policies</a></p>-->
                                          

                                            <div id="demo" class="collapse">
                                            <div class='leaveCon clearfix' id="getLeaveTypePolicy11">
                                            
                                            </div>
                                            </div>
                                           
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" style="margin-top: 10px;"> 
                                                <label class="col-md-2 control-label" style="float: left">
                                                  <a href="#" data-toggle="collapse" data-target="#Notification" >Send Notification</a>
                                                </label>
                                                <div class="col-md-3 collapse" id="Notification">  
                                                  
                                                    
                                                    <select name="notificationto" id="notificationto" width="100%" class="form-control" data-placeholder="Select..." multiple="multiple">

                                                                        <?php $quer="select EMP_NAME, Emp_Code from Hrdmastqry where MNGR_CODE='$mngrcode'"; 
                                                                        $res=query($query,$quer,$pa,$opt,$ms_db);
                                                                        while ($ro= $fetch($res)) {
                                                                        ?>
                                                                            <option value='<?php echo $ro['Emp_Code'] ?>'><?php echo $ro['EMP_NAME'];?> (<?php echo $ro['Emp_Code'];?>)</option>
                                                                        <?php }?>    
                                                                </select>
                                                </div>

                                            </div>
                                         <p  style=" padding: 4px 0;"><a href="#" data-toggle="collapse" data-target="#deligats" style="float: left;"> <span class="glyphicon glyphicon-chevron-down"></span> Delegation</a></p>
                                        <div id="deligats" class="collapse">

                                            <div class="form-actions" style="padding: 5px;    color: purple;">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                    My Alternative Contact Details
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group" style="margin-top: 10px;"> 
                                                <label class="col-md-1 control-label">
                                                   Mobile
                                                </label>
                                                <div class="col-md-3">  
                                                   <input type="text" class="form-control" width="100%" name="empMobile">
                                                </div>

                                                <label class="col-md-1 control-label">
                                                   Email
                                                </label>
                                                <div class="col-md-3">  
                                                   <input type="text" class="form-control" onkeyup="validate.email(empEmail,error)" onchange="getForwardEmail(this.value);" id="empEmail" name="empEmail">
                                                </div>

                                                <label class="col-md-1 control-label">
                                                   Address
                                                </label>
                                                <div class="col-md-3">  
                                                   <input type="text" class="form-control" name="empAddress">
                                                </div>
                                            </div>
                                            <?php
                                            $sqlqsu="select FY,CONVERT (VARCHAR(10),FY_Start,103 ) as FY_Start from FinYear
                                                where FY_Type='L'";
                                               // echo $sqlqsu;
                                                $resultqsu=query($query,$sqlqsu,$pa,$opt,$ms_db);
                                                if($resultqsu){
                                                  $rowq1su = $fetch($resultqsu);
                                                  $fy=$rowq1su['FY'];
                                                }
                                                else{
                                                    $fy=date("Y");
                                                }

                                            ?>
                                            <input type="hidden" name="Leaveyear" id="Leaveyear" value="<?php echo $fy;?>">
                                            <input type="hidden" name="" id="dateCriteriadiv11">
                                            <input type="hidden" name="leaveyear_div" id="leaveyear_div" value="0">
                                            <input type="hidden" name="approvalReq" id="approvalReq" value="0">
											<input type="hidden" name="approvalReq1" id="approvalReq1" value="0">
											<input type="hidden" name="approvalReq2" id="approvalReq2" value="0">
                                            <div class="form-group" id="myselftable" style=" display:block;">
                                                
                                                <div class="col-md-12">
                                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                                    <div class="portlet box ">
                                                   <div class="portlet-body">
                                                        <div class="table-scrollable">
                                                            <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                               <th>Approval Required</th>
                                                                <th>Send To</th>
                                                                <th>Keep Pending till i return</th>
                                                                <th>Forward To My Contact Email</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                        <tr>
                                                        <td>Leave Application</td>
                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio4" id="levSend" onclick="closeDropDown('2');" class="icheck" value="">

                                                            <select name="sendTo" id="sendTo" class="form-control     input-medium select2me" onchange="getSendTo(this.value);" data-placeholder="Select..." style="display:none;">
                                                                      <option value=""></option>>
                                                                        <?php $quer="select EMP_NAME, Emp_Code from Hrdmastqry"; 
                                                                        $res=query($query,$quer,$pa,$opt,$ms_db);
                                                                        while ($ro= $fetch($res)) {
                                                                        ?>
                                                                            <option value='<?php echo $ro['Emp_Code'] ?>'><?php echo $ro['EMP_NAME'];?> (<?php echo $ro['Emp_Code'];?>)</option>
                                                                        <?php }?>    
                                                                </select>

                                                             </label>
                                                        </td>

                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio4"  class="icheck" onclick="closeDropDown('0');" value="Pending" checked>
                                                            
                                                            </label>
                                                        </td>

                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio4" id="forwardTo" onclick="closeDropDown('1');" class="icheck" value="">
                                                              
                                                            </label>
                                                        </td>

                                                        </tr>
                                                        <tr>
                                                            <td>On Duty Attendance</td>
                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio5" id="sendODTo" class="icheck" onclick="closeDropDown1('2');" value="">
                                                            <select name="sendODTo" id="oDDropDown" onchange="getODSendTo(this.value);" class="form-control input-medium select2me" data-placeholder="Select..." style="display:none;">
                                                              <option value=""></option>
                                                                <?php $quer="select EMP_NAME, Emp_Code from Hrdmastqry"; 
                                                                $res=query($query,$quer,$pa,$opt,$ms_db);
                                                                while ($ro= $fetch($res)) {
                                                                ?>
                                                                    <option value='<?php echo $ro['Emp_Code'] ?>'><?php echo $ro['EMP_NAME'];?> (<?php echo $ro['Emp_Code'];?>)</option>
                                                                <?php }?>    
                                                                </select> </label>
                                                        </td>

                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio5" onclick="closeDropDown1('0');" class="icheck" value="Pending" checked>
                                                            
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio5" id="forwardODTo"  class="icheck" onclick="closeDropDown1('1');" value="">
                                                             </label>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                        <td>Past Attendance</td>
                                                        <td>
                                                            <label>
                                                             <input type="radio" name="radio6" id="sendPastTo" class="icheck" value=""
                                                             onclick="closeDropDown2('2');">

                                                            <select name="sendPastTo" id="pastDropDown" onchange="getPastSend(this.value);" class="form-control input-medium select2me" data-placeholder="Select..." style="display:none;">
                                                          <option value=""></option>>
                                                            <?php $quer="select EMP_NAME, Emp_Code from Hrdmastqry"; 
                                                            $res=query($query,$quer,$pa,$opt,$ms_db);
                                                            while ($ro= $fetch($res)) {
                                                            ?>
                                                                <option value='<?php echo $ro['Emp_Code'] ?>'><?php echo $ro['EMP_NAME'];?> (<?php echo $ro['Emp_Code'];?>)</option>
                                                            <?php }?>    
                                                            </select></label>
                                                        </td>

                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio6" onclick="closeDropDown2('0');" class="icheck" value="Pending" checked>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label>
                                                            <input type="radio" name="radio6" id="forwardPastTo" class="icheck third" onclick="closeDropDown2('1');" value="">
                                                             </label>
                                                        </td>
                                                        </tr>
                                                                </tbody>
                                                                </table>
                                                            </div>
                                                        </div> 





                               






                                                    </div>
                                                    <!-- END SAMPLE TABLE PORTLET-->
                                                </div>
                                                </div>
                                            </div>

                                            <?php 
                        $sqlq="select AppMethod from WorkFlow WHERE WFFor='Leave' and WEvents='ALL' order by WorkFlowID DESC ";

                        $resultq = query($query,$sqlq,$pa,$opt,$ms_db);
                        $row = $fetch($resultq);
                        $workflowmethod = $row['AppMethod'];
                        $sqlr="Select * from hrdmastQry where Emp_Code='$code'";
                        $resultr = query($query,$sqlr,$pa,$opt,$ms_db);
                        $rowr = $fetch($resultr);
                        $code1 = $rowr['MNGR_CODE'];
                        
                        ?>
                                                
                                            <div class="form-group" id="apprMngr" style="display:none;">
                                               <div class="col-md-9">
                                               <div class="col-md-3">
                                                    <?php echo APPROVER_TEXT;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="col-md-3">
                                                <img class="img-circle" src="../Profile/upload_images/<?php echo $data1['EmpImage'];?>"
                                                style="width: 25%;height: 25%;">
                                                </div>    
                                                <div class="col-md-3">
                                                 <?php echo $data1['EMP_NAME'];?>, <?php echo $data1['DSG_NAME'];?>, <?php echo $data1['MailingAddress'];?> 
                                                <input type="hidden" id="levellist" value=' <?php echo $data1['Emp_Code'];?>'>
                                                <strong> <span id="showlevel1"></span></strong>
                                                </div>
                                                </div>
                                            </div>

                                            <input type="hidden" id="levellist" value="">
                                            <input type="hidden" name="wfmethod" id="wfmethod" value="<?php echo $workflowmethod; ?>">
                                            <input type="hidden" name="autoapprover" id="autoapprover" value="<?php echo $code1; ?>">
                                            <?php if($workflowmethod !='automatic'){ ?>
                                            <div class="row">
                                              <div class="form-group" id="According_WorkFlow">
                                                <div class="col-md-12">
                                                <label class="col-md-3 control-label"><?php echo APPROVER_TEXT;?></label>
                                                <div class="col-md-9" id="showlevel"> 
                                                </div>
                                                </div>
                                              </div>
                                            </div>

                                            <?php }?>

                                        <div class="form-actions" id="actionMyself">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="button" onclick="submitLeave('<?php echo $code; ?>',this.value);" class="btn btn-circle blue" id="leaveSubmit" value="<?php echo $mngrcode; ?>">Submit</button>
                                                    
                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>