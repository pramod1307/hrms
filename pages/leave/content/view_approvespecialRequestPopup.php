<?php
//include('../../db_conn.php');
//include('../../configdata.php');
include ('../ajax/leave_class.php');
$leave_class_obj=new leave_class();
$id=$_POST['id'];
$status=$_POST['status'];
$code=$_POST['code'];
$sql="select top 1 CONVERT(VARCHAR(10),LeavTran.EffictiveDate,103) as EffictiveDate,LeavTran.LevYear,LeavTran.LvType,LeavTran.LvDays,CONVERT(VARCHAR(20),leave_Trans_history.timestamp,120) as timestamp,LeavTran.flag,leave_Trans_history.Addedby,LeavTran.Emp_Code,leave_Trans_history.createdby,leave_Trans_history.remark from LeavTran,leave_Trans_history where LeavTran.trankey=leave_Trans_history.leavtran and trankey='$id' order by leave_Trans_history.Id desc";
$res=query($query,$sql,$pa,$opt,$ms_db);
$row=$fetch($res);
$actionUserCode=$row['Addedby'];
$sql2="select DSG_NAME, MailingAddress,EmpImage,EMP_NAME from hrdmastqry WHERE Emp_Code='$actionUserCode'";
$res2=query($query,$sql2,$pa,$opt,$ms_db);
$data3=$fetch($res2);
$sql11="select top 1 CONVERT(VARCHAR(20),timestamp,120) as timestamp from leave_Trans_history where leavtran='$id' order by Id asc";
//echo $sql11;
$res11=query($query,$sql11,$pa,$opt,$ms_db);
$row11 = $fetch($res11);
?>

<div class="portlet light bordered">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <!-- <h3 class="form-section">Address</h3> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Applied Date And time:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo dateTimeFormat($row11['timestamp']);?>
                                   
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Approved By:
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                     <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($row['Addedby']))))). "(".$row['Addedby'].")";?>
                                 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Leave Year:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['LevYear'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Effective Date:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['EffictiveDate'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Created By:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($row['Emp_Code']))))). "(".$row['Emp_Code'].")";?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Leave Types:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo $leave_class_obj->getemployee_leave_type($row['LvType']);
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">No Of Days:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['LvDays'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  if($row['flag'] != "1"){echo dateTimeFormat($row['timestamp']);}?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Approver Remark:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['remark'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
               

            </div>
           
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">

                    <div class="col-md-6">
                       <span class="appMan blue-bg">
                            <span class="appManImg">
                            <?php if($data3['EmpImage'] == ""){?>
                            <img src="../Profile/upload_images/change_img.png" class="img-circle img50">
                            <?php }else{?>
                            <img src="../Profile/upload_images/<?php echo $data3['EmpImage'];?>" class="img-circle img50">
                            <?php   } ?>
                            </span>
                            <span class="appManName" data-des="<?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($data3['DSG_NAME']))));?>">
                                <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($data3['EMP_NAME']))));?>
                            </span>
                        </span>
                        </div>
                       <div class="col-md-6">
                        <?php if($row['flag'] == "1" || $row['flag'] == ""){
                            echo "<span style='color:blue;'>Pending</span>";
                        }else if($row['flag'] == "2"){
                            echo "<span style='color:green;'>Approved </span>";
                        }else if($row['flag'] == "3"){
                            echo "<span style='color:red;'>Rejected </span>";
                        }else if($row['flag'] == "4"){
                            echo "<span style='color:red;'>Cancelled </span>";
                        }else if($row['flag'] == "5"){
                            echo "<span style='color:red;'>Approved Cancel </span>";
                        }else if($row['flag'] == "6"){
                            echo "<span style='color:red;'>Approved Cancellation</span>";
                        }else if($row['flag'] == "7"){
                            echo "<span style='color:red;'>Approved Cancel Rejected </span>";
                        }?>
                        </div>
                        
                    </div>
                </div>
            </div>
          
            <?php if($row['flag'] == "1" || $row['flag'] == "5" ){
                    $code=$row['createdby'];
                    $flag=$row['flag'];
                ?>
                <div class="form-actions">
                    <div class="row">

                        <div class="col-md-5">
                            <textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="button" onclick="submitApprovespecialRequest('<?php echo $id;?>','<?php echo $code;?>','<?php echo $flag;?>','2');" class="btn default">Approve</button>
                                </div>
                            </div>
                        </div>
                          <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="button" onclick="submitApprovespecialRequest('<?php echo $id;?>','<?php echo $code;?>','<?php echo $flag;?>','3');" class="btn default">Reject</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            <?php } 
            
                    ?>
        </form>
        <!-- END FORM-->
    </div>
</div>


