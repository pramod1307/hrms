<?php
//include('../../db_conn.php');
//include('../../configdata.php');
include ('../ajax/leave_class.php');
$leave_class_obj=new leave_class();
$id=$_POST['id'];
$status=$_POST['status'];
$code=$_POST['code'];
 $sql="select TOP 1 *,CONVERT (VARCHAR(10),LvFrom,103 ) as LvFrom,CONVERT (VARCHAR(10),LvTo,103 ) as LvTo, convert(varchar,Trn_Date,109) as Trn_Date,LvType,CONVERT (VARCHAR,userUpdatedOn,109 ) as userUpdatedOn, convert(varchar,updation_date,109) as UpdatedOn,remark from Leave WHERE Levkey='$id' order by leaveID DESC";
$res=query($query,$sql,$pa,$opt,$ms_db);
$row=$fetch($res);
//echo $sql;
$sql1="select ApprovedBy,status from leave WHERE Levkey='$id' order by ApprovedBy DESC";
$res1=query($query,$sql1,$pa,$opt,$ms_db);
$emp_count=0;
while ($data2=$fetch($res1))
{
             //echo $rows1['leaveID'];
            $emp_code_ID[$emp_count]=$data2['ApprovedBy'];
            $emp_code_status[$emp_count]=$data2['status'];
            $emp_code_userstatus[$emp_count]=$data2['user_remarks'];
            $emp_count++;
}
//print_r($emp_code_status);

$all_id="'" . implode("','", $emp_code_ID) . "'";

$actionUserCode=$row['ApprovedBy'];
$sql2="select h.Emp_Fname,h.Emp_Lname,h.DSG_NAME, h.MailingAddress,h.EmpImage from hrdmastqry as h WHERE h.Emp_Code IN($all_id) order by Emp_Code DESC";
$res2=query($query,$sql2,$pa,$opt,$ms_db);
$emp_name_count=0;
while ($data3=$fetch($res2))
{
   // echo $data3['Emp_Fname'];
        $Emp_Fname[$emp_name_count]=$data3['Emp_Fname']." ".$data3['Emp_Lname']." ";
         $DSG_NAME[$emp_name_count]=$data3['DSG_NAME'];
          $MailingAddress[$emp_name_count]=$data3['MailingAddress'];
           $EmpImage[$emp_name_count]=$data3['EmpImage'];
        $emp_name_count++;
}
//print_r($Emp_Fname);
$all_approved_emp=implode(",", $Emp_Fname);
?>

<div class="portlet light bordered">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <!-- <h3 class="form-section">Address</h3> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Applied Date And time:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php
                                    $date1=     date("d/m/Y g:i A",strtotime($row['Trn_Date']));
                                    echo $date1;
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                           Requested By:
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($row['CreatedBy'])))));?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            <?php echo APPROVER_TITLE;?>:
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($all_approved_emp))));?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            From Date:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['LvFrom'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            To Date:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['LvTo'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            No Of Days:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo $row['LvDays'];
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Leave Types:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo $leave_class_obj->getemployee_leave_type($row['LvType']);
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Reason:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['reason'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

               
            </div>
            <?php 
                for($k=0;$k<$emp_name_count;$k++)
            {
                if($emp_code_status[$k] == "2" || $emp_code_status[$k] == "3" || $emp_code_status[$k] == "5" || $emp_code_status[$k] == "6" || $emp_code_status[$k] == "7"){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                            <?php
                             $date1=     (!empty($row['UpdatedOn'])) ? date("d/m/Y g:i A",strtotime($row['UpdatedOn'])) : '';
                                    echo $date1;
                          
                            ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label class="col-md-6">Approver Remarks:</label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  
                                    echo $row['remark'];
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php } 
                
                 if($emp_code_status[$k] == "4" || $emp_code_status[$k] == "5" || $emp_code_status[$k] == "6" || $emp_code_status[$k] == "7"){
                    ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Employee Updation On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                            <?php
                             $date2=     (!empty($row['userUpdatedOn'])) ? date("d/m/Y g:i A",strtotime($row['userUpdatedOn'])) : '';
                                    echo $date2;
                          
                            ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label class="col-md-6">Employee Remarks:</label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  
                                    echo $row['user_remarks'];
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php }
                  ?>

            </div>
            <?php 
           
             //   echo $emp_code_status[$k]." \n";
               // echo "aaa".$k."ssss";
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">

                    <div class="col-md-6">
                       <span class="appMan blue-bg">
                            <span class="appManImg">
                            <?php if($data3['EmpImage'] == ""){?>
                            <img src="../Profile/upload_images/change_img.png" class="img-circle img50">
                            <?php }else{?>
                            <img src="../Profile/upload_images/<?php echo $EmpImage[$k];?>" class="img-circle img50">
                            <?php   } ?>
                            </span>
                            <span class="appManName" data-des="<?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($DSG_NAME[$k]))));?>">
                                <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($Emp_Fname[$k]))));?>
                            </span>
                        </span>
                        </div>
                        <div class="col-md-6">


                         <?php                             
                             $status_val = $row['status'];
                            $sqlL="select * from LOVMast WHERE LOV_Field='status' and LOV_Value='$status_val'";
                        $resL=query($query,$sqlL,$pa,$opt,$ms_db);
                        $dataL=$fetch($resL);
                        if($status_val == "1" || $status_val == "" || $status_val == "5" ){

                         echo "<span style='color:blue;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        else if($status_val == "2" || $status_val == "6"){
                            echo "<span style='color:green;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        else {
                            echo "<span style='color:red;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        ?>
        

                         </div>

                        
                    </div>
                </div>
            </div>
            <?php }?>



        </form>
        <!-- END FORM-->
    </div>
</div>


