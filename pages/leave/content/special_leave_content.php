<style type="text/css">
  
.outer {position:relative !important;height: 205px;
    overflow-x: auto;}
.inner {
  /*overflow-x:scroll !important;
  overflow-y:visible !important;*/
  /*width:400px !important;
  margin-left:10px !important;*/
}
</style>
<?php 
$year=date('Y');
$leave_year=$leave_class_obj->leave_year($year);
$currentyear=$leave_year[0];
$hrcode=HR_APPROVER;
  $sql="select EMP_NAME,OEMailID,EmpImage,DSG_NAME from HrdMastQry where Emp_Code='$hrcode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
          $row=$fetch($result);
        $EMP_NAME=$row['EMP_NAME'];
       // $OEMailID=$row['OEMailID'];
        $EmpImage=$row['EmpImage'];
        $DSG_NAME=$row['DSG_NAME'];

//echo $currentyear_num;
// ($ro= $fetch($res)) {
 ?>


<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content cus-light-grey">

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_0">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    
                                    <div class="caption">
                                       Apply For Special Leave
                                    </div>
                                  
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form action="#" class="form-horizontal" id="leaveForm">
                                    <input type="hidden" name="empcode" id="empcode" value="<?php echo $code;?>">
                                    <input type="hidden" id="currentyear" value="<?php echo $year;?>">
                                        <div class="form-body" >
                                        <div class="col-md-8">
                                                              
					                                 <div class="row">
                                            <div class="col-md-9">
                                            <div class="form-group">
                                                   <label class="col-md-3 control-label">Apply Date</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="fromDate" id="fromDate" placeholder="dd/mm/yy" value="<?php echo date('d/m/Y H:i:s');?>" readonly>
                                                </div> 
                                            </div>
                                             <div class="form-group">
                                                 <div  id="multipleDate">
                                                    <label class="col-md-3 control-label">Employees List</label>
                                                    <div class="col-md-9">
                                                       
                                                    <select name="notificationto" id="notificationto" width="100%" class="form-control" data-placeholder="Select..." multiple="multiple">

                                                                        <?php $quer="select EMP_NAME, Emp_Code from Hrdmastqry where MNGR_CODE='$code'"; 
                                                                        $res=query($query,$quer,$pa,$opt,$ms_db);
                                                                        while ($ro= $fetch($res)) {
                                                                        ?>
                                                                            <option value='<?php echo $ro['Emp_Code'] ?>'><?php echo $ro['EMP_NAME'];?> (<?php echo $ro['Emp_Code'];?>)</option>
                                                                        <?php }?>    
                                                                </select>
                                                    <div id="err_attendance" style="color:red;"></div>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                                <div class="form-group">
                                                   <label class="col-md-3 control-label">Effective Date</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="toDate" id="toDate" placeholder="dd/mm/yy" value="<?php echo date('d/m/Y');?>">
                                                </div> 
                                            </div>
                                                <div class="form-group">
                                                   <label class="col-md-3 control-label">No Of Days</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="noofdays" id="noofdays" placeholder="Only Numeric Value Allow" onKeyUp="numericFilter(this);">
                                                </div> 
                                            </div>
                                           
                                        
                                 
                                        </div>
                                      </div>
 
                                              </div>
                                        
                                        <div class="clearfix"></div>
                                     
                                            <div class="form-group" id="apprMngr">
                                               <div class="col-md-9" style="margin-top: 10px">
                                               <div class="col-md-3">
                                                    <?php echo APPROVER_TEXT;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                 <span id="showlevel"><span class="appMan blue-bg">
                                                    <span class="appManImg"><img class="img-circle img50" src="../Profile/upload_images/<?php echo $EmpImage;?>">  </span>    <span class="appManName" data-des="<?php echo $DSG_NAME;?>"><?php echo $EMP_NAME;?></span></span><input type="hidden" id="approver_leave_ids1" value="1"><input type="hidden" id="approverId" value="<?php echo $hrcode;?> "></span>

                                                </div>
                                            </div>


                                        <div class="form-actions" id="actionMyself">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="button" onclick="submitspecialLeave();" class="btn btn-circle blue">Submit</button>
                                                    
                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                       <div class="col-md-12">

                                                <table id="CJExcel_ErrorLog" cellpadding="0" cellspacing="0" width="100%"> </table>

                                            </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>