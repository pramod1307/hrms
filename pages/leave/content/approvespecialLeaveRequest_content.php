
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
       <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog lg">
        <!-- modal-content -->
        <div class="modal-content" >
                    <div class="modal-header portlet box blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title white-txt"><div class="caption"><span>Approve Special Leave </span></div></h4>
                    </div>
                    <div class="modal-body" id="myleave">
                        <?php //include ("content/view_myodrequest.php"); ?>
                    </div>
                </div>
        <!-- /.modal-content -->
    </div>
</div>
        <!-- /.modal -->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Approve Special Leave Request
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">

                            </a>
                            
                        </div>
                    </div>
                     <div class="portlet-body">
                       
			 <div style="float: right;padding: 2px;">
                     <button id="btnExport" onclick="fnExcelReport('sample_2');"> EXPORT </button>
                     <!--<input type="button" id="exportpdf" onclick="pdfexport();" value="Download PDF">-->
                    
                     </div>
                     <iframe id="txtArea1" style="display:none"></iframe>
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                            <?php
                            if(($code == 'admin' || Is_hr($code,$query,$pa,$opt,$ms_db))) {
                                $sql="select CONVERT(VARCHAR(10),LeavTran.EffictiveDate,103) as EffictiveDate,LeavTran.LevYear,LeavTran.LvType,LeavTran.LvDays,CONVERT(VARCHAR(20),leave_Trans_history.timestamp,120) as timestamp,LeavTran.flag,LeavTran.Emp_Code,leave_Trans_history.createdby,LeavTran.trankey,leave_Trans_history.remark from LeavTran,leave_Trans_history where LeavTran.trankey=leave_Trans_history.leavtran and LeavTran.flag=leave_Trans_history.status";
                            } else {
                                $sql="select CONVERT(VARCHAR(10),LeavTran.EffictiveDate,103) as EffictiveDate,LeavTran.LevYear,LeavTran.LvType,LeavTran.LvDays,CONVERT(VARCHAR(20),leave_Trans_history.timestamp,120) as timestamp,LeavTran.flag,LeavTran.Emp_Code,leave_Trans_history.createdby,LeavTran.trankey,leave_Trans_history.remark from LeavTran,leave_Trans_history where LeavTran.trankey=leave_Trans_history.leavtran and LeavTran.flag=leave_Trans_history.status and leave_Trans_history.Addedby='$code'";
                            }

                           
                            $res=query($query,$sql,$pa,$opt,$ms_db);
                            $len = $num($res);
                            
                            $i=0;
                            ?>
                           <tr>
                                <!--<th class="table-checkbox">
                                    <input type="checkbox" id="Allcheck" value="0" 
                                    onclick="allCheck('Allcheck','<?php echo $code;?>');" class="group-checkable" data-set="#sample_2 .checkboxes"/>
                                </th>-->

                                <th>
                                    Request Applied Date
                                </th>
                                 <th>
                                    Leave Year
                                </th>
                                <th>
                                    Effective Date
                                </th>
                                  <th>
                                    Requested By
                                </th>
                                 <th>
                                    Created By
                                </th>
                                <th>
                                    No. Of Days
                                </th>

                                <th>
                                    Type
                                </th>
                                <th>
                                    Approved On
                                </th>
                                 <th>
                                    Approved Remark
                                </th>
                                <th>
                                    Status
                                </th>
                               
                            </tr>
                            </thead>

                            <tbody id="searchData">
                            <?php
                                while ($row = $fetch($res)){
                                   $temp=$row['trankey'];
                                $sql1="select top 1 CONVERT(VARCHAR(20),timestamp,120) as timestamp from leave_Trans_history where leavtran='$temp' order by Id asc";
                                    $res1=query($query,$sql1,$pa,$opt,$ms_db);
                                    $row1 = $fetch($res1);
                            ?>

                            <tr class="odd gradeX">
                            <?php //if( $row['status']== 2 || $row['status']== 3 || $row['status']== 4 || $row['status']== 5){?>
                                    <!--<td></td>-->
                                <?php// }else { ?>
                                <!--<td>
                                <input type="checkbox" class="checkboxes" id="<?php echo "Mulcheck".$i;?>" onclick="mulCheck('<?php echo "Mulcheck".$i;?>','<?php echo "key".$i;?>');" value="<?php echo $row['leaveID'];?>"/>
                                </td>-->
                                <?php// } ?>
                                
                                 <td>
                                    <?php echo dateTimeFormat($row1['timestamp']);?>
                                      
                                    </td>
                                    <td>
                                        <?php echo $row['LevYear'];?>
                                    </td>
                                    <td>
                                        <?php echo $row['EffictiveDate'];?>
                                    </td>
                                     <td>
                                        <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($row['createdby']))))). "(".$row['createdby'].")";?>
                                    </td>
                                     <td>
                                        <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($row['Emp_Code']))))). "(".$row['Emp_Code'].")";?>
                                    </td>
                                    <td>
                                        <?php echo $row['LvDays'];?>
                                    </td>
                                    <td>
                                        <?php echo $leave_class_obj->getemployee_leave_type($row['LvType']);?>
                                    </td>
                                     <td>
                                        <?php if($row['flag'] != "1"){echo dateTimeFormat($row['timestamp']);}?>
                                    </td>
                                     <td>
                                        <?php echo $row['remark'];?>
                                    </td>
                                    <td>
                                        <?php if($row['flag'] == "1" || $row['flag'] == ""){?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial1('<?php echo $row['trankey'];?>','1');">
                                <span class="label  bg-blue-steel">
                                Pending </span>
                                            </a>
                                        <?php } else if($row['flag'] == "2") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial1('<?php echo $row['trankey'];?>','2');">
                                <span class="label label-success">
                               Approved
                                 </span>
                                            </a>
                                        <?php } else if($row['flag'] == "3") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial1('<?php echo $row['trankey'];?>','3');">
                                <span class="label label-danger">
                               Rejected
                             </span>
                                            </a>
                                        <?php } else if($row['flag'] == "4") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial1('<?php echo $row['trankey'];?>','4');">
                                <span class="label bg-grey-cascade">
                               Cancelled
                             </span>
                                            </a>
                                             <?php } else if($row['flag'] == "5") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial1('<?php echo $row['trankey'];?>','5');">
                                <span class="label bg-grey-cascade">
                               Approved Cancel
                             </span>
                                            </a>
                                            <?php } else if($row['flag'] == "7") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial1('<?php echo $row['trankey'];?>','5');">
                                <span class="label bg-grey-cascade">
                               Approved Cancel Rejected
                             </span>
                                            </a>
                                             <?php } else if($row['flag'] == "6") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyspecial1('<?php echo $row['trankey'];?>','6');">
                                <span class="label bg-grey-cascade">
                               Approved Cancellation
                             </span>
                                            </a>
                                        <?php }?>

                                    </td>

                            </tr>

                            <?php $i++; } ?>
                            </tbody>
                        </table>

                        <!--<div class="row">
                            <div class="col-md-6">
                            <textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
                        </div>
						  <div class="col-md-6">
                        <label>
                            <input type="radio" name="action_radio_all"  class="icheck" id="action_radio_all" value="0"> Unplanned</label>
                              <label>
                            <input type="radio" name="action_radio_all"  class="icheck" id="action_radio_all" value="1"> Planned</label>
                        </div>
                            <div class="col-md-8" style="float: right;">
                            <label id="hide_LeaveReasons_combo1" style="display: none;" class="col-md-5">
                                <select class="form-control" id="LeaveReasonscombo1" onchange="change_LeaveReasons1();">
                                            <option value="" >Select ..</option>
                                            <?php
                                            $sql6="select * from LOVMast where LOV_Field='LeaveReasons'";
                                            $result6=query($query,$sql6,$pa,$opt,$ms_db);
                                            while ($row6 = $fetch($result6)){
                                                ?>
                                                <option value="<?php echo $row6['LOV_Value']?>"><?php echo $row6['LOV_Text']?></option>
                                            <?php } ?>

                                        </select></br>
                            </label>
                             <label id="hide_LeaveReasons_text1" style="display: none;" class="col-md-5">
                                <input type="text" name="othercombo" class="form-control" id="othercombo1" placeholder="Enter Your Unplanned Reason">
                                  </br>         
                            </label>
                           </div>
                        </div>
                            <div class="col-md-12">
                                <div class="col-md-offset-4 col-md-4 col-md-offset-4" style="margin-top: 20px;">
                                <input type="hidden" value="" id="selectedcheckbox" />
                                <input type="hidden" name="key" id="key" value="">

                                <select class="form-control" id="selected_status_div" onchange="showSubmitButton(this.value,'<?php echo $len;?>');">
                                    <option value="">Select Action..</option>
                                    <?php
                                        $sql="select * from LOVMast where LOV_Field='status' and LOV_Value in ('2','3')";
                                        $result=query($query,$sql,$pa,$opt,$ms_db);
                                       while ($row = $fetch($result)){
                                    ?>
                                    <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                    <?php } ?>
                                </select>
                                    </div>
                                <div class="col-md-offset-4" id="submitButtonDiv" style="display: none; margin-top: 19px;">
                                    <button class="btn btn-primary" id="submitButton" value="" onclick="submitApprovelRequest('selectedcheckbox','selected_status_div','0');">Submit</button>
                                </div>

                            </div>
                        </div>-->
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTENT-->
    </div>
</div>