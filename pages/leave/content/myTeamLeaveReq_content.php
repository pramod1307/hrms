<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <!--modal-dialog -->
        <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog lg">
                <!-- modal-content -->
                <div class="modal-content" >
                    <div class="modal-header portlet box blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title white-txt"><div class="caption"><span>My Team Leave Transactions</span></div></h4>
                    </div>
                    <div class="modal-body" id="myteamleave">
                        <?php //include ("content/view_myodrequest.php"); ?>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- /.modal-dialog -->
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN CONDENSED TABLE PORTLET-->
                <div class="portlet box blue">

                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>My Team Leave Transactions
                        </div>
                      
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="btn-group ">
                                        <select class="form-control" onchange="getRelatedSelectbox(this.value);">
                                            <option value="">Select....</option>
                                            <option value="1">Request Applied On</option>
                                            <option value="2">Request Status</option>
                                            <option value="3">By Requester Name</option>
                                        </select>
                                    </div>

                                    <div class="btn-group" id="monthlySearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                From Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="fromPastDate" id="fromDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                To Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="toPastDate" id="toDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-2" style="margin-top: 25px;">
                                        <button class="btn" onclick="searchByDate('<?php echo $code;?>')">Go</button>
                                        </div>
                                    </div>

                                    <div class="btn-group" id="actionSearch" style="display: none;margin-left: 45px;">
                                        <select class="form-control" onchange="searchByStatus(this.value,'<?php echo $code;?>');">
                                            <option value="">Select ..</option>
                                            <?php
                                            $sql="select * from LOVMast where LOV_Field='status'";
                                            $result=query($query,$sql,$pa,$opt,$ms_db);
                                            while ($row = $fetch($result)){
                                                ?>
                                                <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                            <?php } ?>

                                        </select>
                                    </div>

                                    <div class="btn-group" id="bynameSearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-10">
                                             <select class="form-control" name="byRequest"  id="byRequest" onchange="serchByCodeName('byRequest');">
                                             <option value="0">Select ..</option>
                                            <?php
                                                $sqlCrea="select distinct CreatedBy from leave where ApprovedBy='$code' and flag='1' ";




                                            $resCrea=query($query,$sqlCrea,$pa,$opt,$ms_db);
                                                while ($rowCrea = $fetch($resCrea)) {
                                                  $arrEmp[]="'" .$rowCrea['CreatedBy'] . "'";
                                                }

                                                $strEmp =  implode(",", $arrEmp) ;

                                                $sqlEmp="Select Emp_Code,EMP_NAME from HrdMastQry where Emp_Code in ($strEmp) ";
                                                $resEmp=query($query,$sqlEmp,$pa,$opt,$ms_db);
                                                while ($rowEmp =$fetch($resEmp)) {?>
                                                    <option value="<?php echo $rowEmp['Emp_Code']?>"><?php echo $rowEmp['EMP_NAME']." (".$rowEmp['Emp_Code'].")";?></option> 
                                              <?php  }
                                                
                                                 ?> 
                                             </select>
                                          
                                        </div>
                                             
                                      
                                    </div>

                                </div>
                            </div>

                        </div>
			 <div style="float: right;padding: 2px;">
                     <button id="btnExport" onclick="fnExcelReport('sample_2');"> EXPORT </button>
                     <!--<input type="button" id="exportpdf" onclick="pdfexport();" value="Download PDF">-->
                    
                     </div>
                     <iframe id="txtArea1" style="display:none"></iframe>
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <?php
                         $getMyTeamLeaveRequestDataManager=$leave_class_obj->getMyTeamLeaveRequestDataManager($code);
//print_r($getMyTeamLeaveRequestDataManager);
                            $leaveID= $getMyTeamLeaveRequestDataManager[0];
                            $LvFrom=$getMyTeamLeaveRequestDataManager[1];
                            $LvTo=$getMyTeamLeaveRequestDataManager[2];
                            $LvDays=$getMyTeamLeaveRequestDataManager[3];
                            $LvType=$getMyTeamLeaveRequestDataManager[4];
                            $reason=$getMyTeamLeaveRequestDataManager[5];
                            $ApprovedBy=$getMyTeamLeaveRequestDataManager[6];
                            $status=$getMyTeamLeaveRequestDataManager[7];
                            $Levkey=$getMyTeamLeaveRequestDataManager[8];
                            $CreatedBy=$getMyTeamLeaveRequestDataManager[9];
                            $updation_date=$getMyTeamLeaveRequestDataManager[10];
                            $remark=$getMyTeamLeaveRequestDataManager[11];
                            $userUpdatedOn=$getMyTeamLeaveRequestDataManager[12];
                            $user_remarks=$getMyTeamLeaveRequestDataManager[13];
                            $trn_Date=$getMyTeamLeaveRequestDataManager[14];
                            $count_flag=$getMyTeamLeaveRequestDataManager[15];

                          //  $user_remark=$getMyMarkPastRequestData[13];
                           // $user_UpdatedOn=$getMyMarkPastRequestData[14];
                            

                         //  echo "<pre>"; print_r($remark) ;
                            //exit;
                           /* $sql="select *,CONVERT (VARCHAR(10),LvFrom,103 ) as LvFrom,CONVERT (VARCHAR(10),LvTo,103 ) as LvTo from Leave WHERE CreatedBy='$code' and flag='1'";
                            $res=query($query,$sql,$pa,$opt,$ms_db);
                            $len = $num($res);*/

                            ?>
                            <thead >
                            <tr><th></th>
                            <th>
                                Applied Date And time
                            </th>
                                <th>
                                    From Date 
                                </th>

                                <th>
                                    To Date
                                </th>

                                <th>
                                    No Of days
                                </th>
                                 <th>
                                    Leave Type
                                </th>
                                   <th>
                                    Approved By
                                </th>
                                <th>
                                    Reason
                                </th>
                                <th>
                                    Approved On
                                </th>
                                <th>
                                    Approver Remarks
                                </th>

                             
                                <th>
                                    Status
                                </th>
                            </tr>
                            </thead>
                            <tbody id="searchMyData">
                            <?php

                            for ($i=0;$i<$count_flag;$i++){


                                ?>
                                <tr><td></td>

                                    <td><?php echo $trn_Date[$i].' -('.str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($CreatedBy[$i]))))). "-".$CreatedBy[$i].")";?></td>

                                    <td>
                                        <?php echo $LvFrom[$i];?>
                                    </td>
                                    <td>
                                        <?php echo $LvTo[$i];?>
                                    </td>
                                    <td>
                                        <?php echo $LvDays[$i];?>
                                    </td>
                                    <td>
                                        <?php echo $leave_class_obj->getemployee_leave_type($LvType[$i]);?>
                                    </td>
                                      <td>
                                    <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($ApprovedBy[$i]))))). "(".$ApprovedBy[$i].")";?>
                                       
                                    </td>
                                    <td>
                                        <?php echo $reason[$i];?>
                                    </td>
    <td>

                        <?php if($status[$i] == "2" || $status[$i] == "3" || $status[$i] == "5" || $status[$i] == "6" || $status[$i] == "7")
                 {
                  echo $updation_date[$i];
                 }
                         ?>
                    </td>
<td>
<?php
if($status[$i] == "2" || $status[$i] == "3"|| $status[$i] == "5" || $status[$i] == "6" || $status[$i] == "7" )
                 {
                         echo $remark[$i];
            }
                         ?>
                    </td>

                                  

                                    <td>
                                        <?php if($status[$i] == "1" || $status[$i] == ""){?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyleaveId('<?php echo $Levkey[$i];?>','1','<?php echo $code;?>');">
								<span class="label  bg-blue-steel">
								Pending </span>
                                            </a>
                                        <?php } else if($status[$i] == "2") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyleaveId('<?php echo $Levkey[$i];?>','2','<?php echo $code;?>');">
                                <span class="label label-success">
                               Approved
								 </span>
                                            </a>
                                        <?php } else if($status[$i] == "3") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyleaveId('<?php echo $Levkey[$i];?>','3','<?php echo $code;?>');">
                                <span class="label label-danger">
                               Rejected
							 </span>
                                            </a>
                                        <?php } else if($status[$i] == "4") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyleaveId('<?php echo $Levkey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label bg-grey-cascade">
                               Cancel
							 </span>
                                            </a>
                                             <?php } else if($status[$i] == "5") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyleaveId('<?php echo $Levkey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label bg-blue-steel">
                               Approved Cancel
                             </span>
                                            </a>
                                        <?php }
                                        else if($status[$i] == "6") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyleaveId('<?php echo $Levkey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label label-success">
                               Approved Cancellation
                             </span>
                                            </a>
                                        <?php }
                                        else if($status[$i] == "7") {?>
                                            <a class="myod" data-toggle="modal" href="#large"
                                               onclick="getmyleaveId('<?php echo $Levkey[$i];?>','4','<?php echo $code;?>');">
                                <span class="label label-danger">
                               Approved Cancel Rejected
                             </span>
                                            </a>
                                        <?php }



                                        ?>


                                    </td>

                                </tr>
                                <?php
                                if($LvType[$i]==8)
                                {
                                ?>
                                 <tr><td></td>
                                    <td><?php echo $LevTrn_Date[$i].' '.getCurrentEmpCodeName();?></td>
                                    <td>
                                        <?php echo $LvFrom[$i];?>
                                    </td>
                                    <td>
                                        <?php echo $LvTo[$i];?>
                                    </td>
                                    <td>
                                        <?php echo $LvDays[$i];?>
                                    </td>
                                    <td>
                                        <?php echo $leave_class_obj->getemployee_leave_type('1');?>
                                    </td>
                                      <td>
                                    <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($leave_class_obj->getemployee_name($ApprovedBy[$i]))))). "(".$ApprovedBy[$i].")";?>
                                       
                                    </td>
                                    <td>
                                        <?php echo $reason[$i];?>
                                    </td>
    <td>

                        <?php if($status[$i] == "2" || $status[$i] == "3" || $status[$i] == "5" || $status[$i] == "6" || $status[$i] == "7")
                 {
                  echo $updation_date[$i];
                 }
                         ?>
                    </td>
<td>
<?php
if($status[$i] == "2" || $status[$i] == "3"|| $status[$i] == "5" || $status[$i] == "6" || $status[$i] == "7" )
                 {
                         echo $remark[$i];
            }
                         ?>
                    </td>

                                  

                                    <td>
                                        


                                    </td>

                                </tr>

                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END CONDENSED TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>

