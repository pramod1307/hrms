<?php
include('../../db_conn.php');
include('../../configdata.php');
include ('../../main_class.php');
$main_class_obj=new main_class();
$id=$_POST['id'];
$status=$_POST['status'];
$code=$_POST['code'];
$sql="select *,CONVERT (VARCHAR(10),LvFrom,103 ) as LvFrom,CONVERT (VARCHAR(10),LvTo,103 ) as LvTo,convert(varchar,Trn_Date,109) as Trn_Date,CONVERT (VARCHAR(19),userUpdatedOn) as userUpdatedOn,CONVERT (VARCHAR(19),updation_date) as updation_date from Leave WHERE leaveID='$id'";
//echo $sql;
$res=query($query,$sql,$pa,$opt,$ms_db);
$row=$fetch($res);

$sql1="select * from hrdmastqry WHERE Emp_Code='$code'";
$res1=query($query,$sql1,$pa,$opt,$ms_db);
$data2=$fetch($res1);

$actionUserCode=$row['ApprovedBy'];
$sql2="select EMP_NAME,DSG_NAME, MailingAddress,EmpImage from hrdmastqry WHERE Emp_Code='$actionUserCode'";
$res2=query($query,$sql2,$pa,$opt,$ms_db);
$data3=$fetch($res2);

?>

<div class="portlet light bordered">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <!-- <h3 class="form-section">Address</h3> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Applied Date And time:
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php
                                   $date1=     date("d/m/Y g:i A",strtotime($row['Trn_Date']));
                                    echo $date1;
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Applied Leave:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php
                                  
                                    echo $main_class_obj->getemployee_leave_type_main($row['LvType']);
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Requested By:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php
                                    $reqcode=$row['CreatedBy'];
                                    $sql1="select EMP_NAME from HrdMastQry WHERE Emp_Code='$reqcode'";
                                    $res1=query($query,$sql1,$pa,$opt,$ms_db);
                                    $data1=$fetch($res1);
                                    echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($data1['EMP_NAME']))))." (".$reqcode.")";
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">From Date:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['LvFrom'];?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">To Date:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['LvTo'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">No. Of Days:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php echo $row['LvDays'];
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Reason:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['reason'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                      <?php if($row['status'] == "1" || $row['status'] == ""){?>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                           
                            <div class="col-md-6">
                            </div>
                             <?php
                      $sqlleave="select LOV_Active from LOVMast where LOV_Field='LeaveFlag' and LOV_Text='Manager'";
                      $resultleave=query($query,$sqlleave,$pa,$opt,$ms_db);
                      $rowleave = $fetch($resultleave);
                      $leaveplanshow = $rowleave['LOV_Active'];
                    if($leaveplanshow == 'Y'){
                    ?>
                        <div class="col-md-6">
                        <input type="hidden" name="planunplan" id="planunplan" value="<?php echo $leaveplanshow;?>">
                                <label>
                            <input type="radio" name="radio_action"  class="icheck" id="action_radio" value="0"> Unplanned</label></br>
                            <label id="hide_LeaveReasons_combo"style="display: none;">
                                <select class="form-control" id="LeaveReasonscombo" onchange="change_LeaveReasons();">
                                            <option value="" >Select ..</option>
                                            <?php
                                            $sql6="select * from LOVMast where LOV_Field='LeaveReasons'";
                                            $result6=query($query,$sql6,$pa,$opt,$ms_db);
                                            while ($row6 = $fetch($result6)){
                                                ?>
                                                <option value="<?php echo $row6['LOV_Value']?>"><?php echo $row6['LOV_Text']?></option>
                                            <?php } ?>

                                        </select></br>
                            </label>
                             <label id="hide_LeaveReasons_text"style="display: none;">
                                <input type="text" name="othercombo" class="form-control" id="othercombo" placeholder="Enter Your Unplanned Reason">
                                  </br>         
                            </label>
                             <label>
                            <input type="radio" name="radio_action"  class="icheck" id="action_radio" value="1" > Planned </label>
                        </div>
                        <?php } ?>
                        </div>
                    </div>
                    <?php }
                    else
                        {
                            if($row['planned_unplanned']==0)
                                {?>
                                    <div class="col-md-12">
                                        <div class="form-group m-0">
                                            <div class="col-md-6">Leave is:</div>
                                                <div class="col-md-6">
                                                   <?php  if($row['planned_unplanned'] == 0){
                                                            echo "Unplanned";
                                                            
                                                            }
                                                            else{
                                                                echo "Planned";
                                                                }
                                                        ?></br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                    <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Reason for unplanned leave:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  $databaseleave= $row['leaveReason'];
                                    $sql6="select * from LOVMast where LOV_Field='LeaveReasons' and LOV_Value='$databaseleave'";
                                      $result6=query($query,$sql6,$pa,$opt,$ms_db);
                                      $row6 = $fetch($result6);
                                      echo $row6['LOV_Text'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                if($databaseleave == 6) { ?>
                 <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Other Reason:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['reasonText'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }


                           
                                }
                                elseif ($row['planned_unplanned']==1) {
                                ?>
                                           <div class="col-md-12">
                        <div class="form-group m-0">
                           <div class="row">
                            <div class="col-md-12">
                                        <div class="form-group m-0">
                                            <div class="col-md-6">Leave is:</div>
                                                <div class="col-md-6">
                                                   <?php  if($row['planned_unplanned'] == 0){
                                                            echo "Unplanned";
                                                            echo $row['leaveReason'];
                                                            }
                                                            else{
                                                                echo "Planned";
                                                                }
                                                        ?></br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    
                        </div>
                        </br>
                    </div>
<?php
                                }
                                ?>

                    <?php }?>
					<?php
					//echo "aaaaaa".$row['attachments']."bbbbb";
                    if($row['attachments']!="" and $row['attachments']!=" ")
                    {
                            
                    ?>
                       <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Attachment:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <a href="upload/<?php echo $row['attachments'];?>" target="_blank">View</a>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                    ?>

                </div>
            </div>
            <?php if($row['status'] == "2" || $row['status'] == "3" || $row['status'] == "5" || $row['status'] == "6" || $row['status'] == "7"){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                            <?php
                              echo $row['updation_date'];
                          
                            ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label class="col-md-6">Approver Remarks:</label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  
                                    echo $row['remark'];
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php } 
                
                 if($row['status'] == "4" || $row['status'] == "5" || $row['status'] == "6" || $row['status'] == "7"){
                    ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Employee Updation On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                            <?php
                             echo $row['userUpdatedOn'];
                          
                            ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label class="col-md-6">Employee Remarks:</label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  
                                    echo $row['user_remarks'];
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php }
                  ?>


            
            <div class="row">
                <div class="col-md-12">
                         <div class="col-md-6">
                        <span class="appMan blue-bg">
                        <?php if($data3['EmpImage'] == ""){?>
                            <span class="appManImg" >
                            <img class="img-circle img50" src="../Profile/upload_images/change_img.png" >
                            </span>
                        <?php }else{?>
                            <span class="appManImg" >
                            <img class="img-circle img50" src="../Profile/upload_images/<?php echo $data3['EmpImage'];?>" >
                            </span>
                        <?php   } ?>
                        <span class="appManName" data-des="<?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($data3['DSG_NAME']))));?>">
                        <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($data3['EMP_NAME']))));?>
                        </span>
                        </span>
                        </div>
                        <div class="col-md-6">


                         <?php                              $status_val = $row['action_status'];
                             $sqlL="select * from LOVMast WHERE LOV_Field='status' and LOV_Value='$status_val'";
                        $resL=query($query,$sqlL,$pa,$opt,$ms_db);
                        $dataL=$fetch($resL);
                        if($status_val == "1" || $status_val == ""){

                         echo "<span style='color:blue;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        else if($status_val == "2" || $status_val == "6"){
                            echo "<span style='color:green;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        else {
                            echo "<span style='color:red;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        ?>
        

                         </div>
                    
                       
                       
                    </div>
                </div>
                </div>
            </div>
             <?php if($row['status'] == "1"){ ?>   
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                             <div class="col-md-6">
                            <textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
                        </div>
                            <div class="col-md-3"> 
                            <input type="hidden" id="emp_ID" value="<?php echo $row['leaveID'];?>">     
                            <button type="button" onclick="submitApprovelRequest('emp_ID','2','1');" class="btn default green">Approved</button>
                            </div>

                            <div class="col-md-3">      
                            <button type="button" onclick="submitApprovelRequest('emp_ID','3','1');" class="btn default red">Rejected
                            </button>
                            </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
          
            <?php }
                    if($row['status'] == "5"){ ?>   
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                             <div class="col-md-6">
                            <textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
                        </div>
                            <div class="col-md-3"> 
                            <input type="hidden" id="emp_ID" value="<?php echo $row['leaveID'];?>">     
                            <button type="button" onclick="submitApprovelRequest('emp_ID','2','1');" class="btn default green">Approved</button>
                            </div>
                             <div class="col-md-3">      
                            <button type="button" onclick="submitApprovelRequest('emp_ID','3','1');" class="btn default red">Rejected
                            </button>
                            </div>
                          
                                
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
          
            <?php } ?>
        </form>
        <!-- END FORM-->
    </div>
</div>
<script type="text/javascript">
    
    $("input[name=radio_action]:radio").change(function () {
       var a= $("input[name='radio_action']:checked").val();
       if(a==0)
       {
        $('#hide_LeaveReasons_combo').css({'display':'block'});
       }
       else
       {
        $('#hide_LeaveReasons_combo').css({'display':'none'});
        $('#hide_LeaveReasons_text').css({'display':'none'});
       }
   // alert("q"+a);
   
});
function change_LeaveReasons()
{
    var val1=$("#LeaveReasonscombo option:selected").text();
    if(val1==='Other')
    {
       // alert(val1); 
        $('#hide_LeaveReasons_text').css({'display':'block'});
       }
       else
       {
        $('#hide_LeaveReasons_text').css({'display':'none'});
      
       }
    
}
</script>


