<?php
include('../../db_conn.php');
include('../../configdata.php');
$id=$_POST['id'];
$status=$_POST['status'];
$code=$_POST['code'];
$sql="select *,CONVERT (VARCHAR(10),wd_date,103) as wd_date,CONVERT (VARCHAR,CreatedOn,109) as CreatedOn,convert(char(8), actual_INtime, 108) as actual_INtime,convert(char(8), actual_OUTtime, 108) as actual_OUTtime,CONVERT (VARCHAR,UpdatedOn,109) as UpdatedOn,CONVERT (VARCHAR,user_updated,109) as user_updated from compOff WHERE compOffId='$id'";
$res=query($query,$sql,$pa,$opt,$ms_db);
$row=$fetch($res);
$UserCode=$row['CreatedBy'];
$actionUserCode=$row['approvedBy'];
$sql2="select DSG_NAME, MailingAddress,EmpImage,EMP_NAME from hrdmastqry WHERE Emp_Code='$actionUserCode'";
$res2=query($query,$sql2,$pa,$opt,$ms_db);
$data3=$fetch($res2);
$sql5="select DSG_NAME, MailingAddress,EmpImage,EMP_NAME from hrdmastqry WHERE Emp_Code='$UserCode'";
$res5=query($query,$sql5,$pa,$opt,$ms_db);
$data5=$fetch($res5);
?>
<div class="portlet light bordered">
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <!-- <h3 class="form-section">Address</h3> -->
                <div class="row">
                    <div class="form-group m-0">
                        <label class="col-md-6">
                        <strong>Applied Date And time:</strong></label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php
                                 $date1=     date("d/m/Y g:i A",strtotime($row['CreatedOn']));
                                    echo $date1;
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group m-0">
                        <label class="col-md-6">
                            <strong>Compensatory Off Applied For:</strong>
                        </label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  echo $row['wd_date'];?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group m-0">
                        <label class="col-md-6">
                        <strong>No. of Days:</strong></label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  echo $row['noOfDays'];?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="form-group m-0">
                        <label class="col-md-6">
                        <strong>Reason:</strong></label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  echo $row['reason'];?>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="form-group m-0">
                        <label class="col-md-6">
                        <strong>In Time:</strong></label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  echo $row['actual_INtime'];?>
                            </p>
                        </div>
                        
                    </div>
                    
                </div>
                <div class="row">
                    
                    <div class="form-group m-0">
                        <label class="col-md-6">
                        <strong>Out Time:</strong></label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  echo $row['actual_OUTtime'];?>
                            </p>
                        </div>
                        
                    </div>
                </div>
<?php
                if($row['action_status'] == "2" || $row['action_status'] == "3" || $row['action_status'] == "5" || $row['action_status'] == "6" || $row['action_status'] == "7" ){?>
                <div class="row">
                   
                        <div class="form-group m-0">
                            <label class="col-md-6"> <strong>Approved On:</strong></label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                            <?php
                            $date1=   (!empty($row['UpdatedOn'])) ?  date("l d/m/Y g:i A",strtotime($row['UpdatedOn'])) : '';
                                                              echo $date1;
                          
                            ?>
                                </p>
                            </div>
                        </div>
                    
                </div>

                <div class="row">
                   
                        <label class="col-md-6"> <strong>Approver Remarks:</strong></label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  
                                    echo $row['action_remark'];
                                ?>
                            </p>
                        </div>
                    
                </div>
                <?php } 
                
                 if($row['action_status'] == "4" || $row['action_status'] == "5" || $row['action_status'] == "6" || $row['action_status'] == "7"){
                    ?>
                <div class="row">
                    
                        <div class="form-group m-0">
                            <label class="col-md-6"> <strong>Employee Updation On:</strong></label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                            <?php
                             $date2=  (!empty($row['user_updated'])) ?   date("l d/m/Y g:i A",strtotime($row['user_updated'])) : '';
                                    echo $date2;
                          
                            ?>
                                </p>
                            </div>
                       
                    </div>
                </div>

                <div class="row">
                    
                        <label class="col-md-6"> <strong>Employee Remarks:</strong></label>
                        <div class="col-md-6">
                            <p class="form-control-static">
                                <?php  
                                    echo $row['user_remarks'];
                                ?>
                            </p>
                        </div>
                    
                </div>
                <?php }
                  ?>

                <div class="row">
                    
                    <div class="form-group m-0">
                        <!-- On <?php echo date("Y-m-d h:i:sa");?><br> -->
                         <?php if($row['action_status'] != "4") {?>
                        <div class="col-md-6">
                            <span class="appMan blue-bg">
                                <span class="appManImg">
                                    <?php if($data3['EmpImage'] == ""){?>
                                    <img class="img-circle img50" src="../Profile/upload_images/change_img.png" >
                                    <?php }else{?>
                                    <img class="img-circle img50" src="../Profile/upload_images/<?php echo $data3['EmpImage'];?>" >
                                    <?php  } ?>
                                </span>
                                <span class="appManName" data-des="<?php echo $data3['DSG_NAME'];?>">
                                    <?php echo $data3['EMP_NAME'];?>
                                </span>
                            </span>
                        </div>
                        <?php } else{ ?>
                        <div class="col-md-6">
                            <span class="appMan blue-bg">
                                <span class="appManImg">
                                    <?php if($data5['EmpImage'] == ""){?>
                                    <img class="img-circle img50" src="../Profile/upload_images/change_img.png" >
                                    <?php }else{?>
                                    <img class="img-circle img50" src="../Profile/upload_images/<?php echo $data5['EmpImage'];?>" >
                                    <?php  } ?>
                                </span>
                                <span class="appManName" data-des="<?php echo $data5['DSG_NAME'];?>">
                                    <?php echo $data5['EMP_NAME'];?>
                                </span>
                            </span>
                        </div>
                        <?php } ?>
                        <div class="col-md-6">
                            <?php
                                $status_val = $row['action_status'];
                        $sqlL="select * from LOVMast WHERE LOV_Field='status' and LOV_Value='$status_val'";
                        $resL=query($query,$sqlL,$pa,$opt,$ms_db);
                        $dataL=$fetch($resL);
                        if($status_val == "1" || $status_val == "" || $status_val == "5"){

                         echo "<span style='color:blue;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        else if($status_val == "2" || $status_val == "6"){
                            echo "<span style='color:green;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        else {
                            echo "<span style='color:red;'>";ECHO $dataL['LOV_Text'];echo"</span>";
                        }
                        ?>
                        </div>
                       
                    </div>
                    
                </div>
              
            </form>
            <!-- END FORM-->
        </div>
    </div>