<?php
  include '../../db_conn.php';
  include ('../../configdata.php');
  date_default_timezone_set("Asia/Kolkata");
  include ('../../Attendance/Events/weekyOff-v2.php');
  include ('../../Attendance/Events/holidays-v2.php');
  include ('../../global_class.php');
  include ('../../main_class.php');

  
  $global_obj = new global_class();
  $main_obj = new main_class();
  global $num;
 
  if($_REQUEST['type'] == "workday"){
      $code    = $_REQUEST['code'];
      $fordate = str_replace('/', '-', $_REQUEST['fromDate']);
      $daytype = '';
      $inTime  = '';
      $outTime = '';
      $newDate = date('Y-m-d',strtotime($fordate)); 

      // To check applied day is on holiday
      $public_holidays= getHolidays($newDate,$newDate,$code);
      // print_r($public_holidays);
      for($i=0;$i<count($public_holidays); $i++){ 
        $array2[] = $public_holidays[$i]['start'];
      }
      if(in_array($newDate,$array2)) {
        $daytype="Holiday";
      }else{
        $daytype=0;
      }

      // To check applied day is in weeklyoff
      if($daytype == '0'){
        $weeklyOff = getWeeklyOff($code,$newDate,$newDate);
        $array1 = array();
        for($i=0;$i<count($weeklyOff); $i++){ 
          $array1[] = $weeklyOff[$i]['start'];
        }
        if(in_array($newDate,$array1)){ 
          $daytype="Weekly_Off";
        }else{
          $daytype=0;
        }
      }

      // Applied day is on working day
      if($daytype == '0'){
          $daytype = 'Working';
      }

      $sqlSetup="select CONVERT(VARCHAR(5),Halfday, 108) as Halfday,CONVERT(VARCHAR(5),Fullday, 108) as Fullday,IS_OD_ALLOWED,For_whom,For_whom_OD from compoff_setup where ApplicableFor='$daytype'";
      $resSetup=query($query,$sqlSetup,$pa,$opt,$ms_db);
      $rownum=$num($resSetup);
      $rowSetup = $fetch($resSetup);

      $Required_halfday = $rowSetup['Halfday'];
      $Required_fullday = $rowSetup['Fullday'];
      $IS_OD_ALLOWED = $rowSetup['IS_OD_ALLOWED'];
      $jsonData = $rowSetup['For_whom'];
      $jsonOdData = $rowSetup['For_whom_OD'];
      $returnNoOfDays   = "";
      $tableHtml        = "";

      if($global_obj -> getEmployeeOUDetail($code, $newDate, $jsonData)) {

          if($rownum >0) {
          /*------------- Attendance------------*/
          /* Calculate total working time for Holiday or Weekly off */
          $att_sql="select Shiftmendatryhrs,Overstay,odStatus,odType,date,Shift_start,Shift_end,Intime,Outtime,CONVERT(varchar(5), DATEADD(minute, DATEDIFF(minute, Intime, Outtime), 0), 114) workTime, CONVERT(varchar(5), DATEADD(minute, DATEDIFF(minute, Shift_start,Shift_end), 0), 114) mendatehours  from attendancesch where Emp_code='".$code."' AND date = '".$newDate."'";
          $att_res=query($query,$att_sql,$pa,$opt,$ms_db);
          $mark_row = $fetch($att_res);
          $shiftStartTime = date('h:i A', strtotime($mark_row['Shift_start']));
          $shiftEndTime = date('h:i A', strtotime($mark_row['Shift_end']));
          $shiftTimeDiff = $mark_row['mendatehours'];
          $attSchinTime = $inTime = date('h:i A', strtotime($mark_row['Intime']));
          $outTime = date('h:i A',strtotime($mark_row['Outtime'])); 
          $totalhours = $mark_row['workTime'];

          $shift_start_timestamp = strtotime($mark_row['date']." ".$shiftStartTime);
          $shift_end_timestamp = strtotime($mark_row['date']." ".$shiftEndTime);
          if($shift_end_timestamp < $shift_start_timestamp) {
            $dt = $mark_row['date']." ".$shiftEndTime;
            $shift_end_timestamp = strtotime($dt . ' +1 day');
          }
          $morning_start_timestamp = strtotime($mark_row['date'] .'+1 day');  /*Get next date morning 12 am time stamp*/  
                  
          /* Calculate total working time for working day */
         if($daytype == 'Working') { 
               $totalhours = 0;
               $attall_sql = "SELECT cast(MIN(PunchDate) as date) as punch_Date ,cast(MIN(PunchDate) as time) as Intime , cast(MAX(PunchDate) as time) as Outtime, CONVERT(varchar(5), DATEADD(minute, DATEDIFF(minute, cast(MIN(PunchDate) as time), cast(MAX(PunchDate) as time)), 0), 114) workTime from attendanceall where cast(PunchDate as date) = '$newDate' and EMP_CODE = '".$code."'"; 

              $att_all_result = query($query,$attall_sql,$pa,$opt,$ms_db);
              $att_rslt_row = $fetch($att_all_result);
              $inTime = date('h:i A', strtotime($att_rslt_row['Intime']));
              $outTime = date('h:i A',strtotime($att_rslt_row['Outtime']));
              $worktime =  $att_rslt_row['workTime']; /*Calculate total working time on working day*/
              // echo "hello ".$shiftTimeDiff; die;
              if(strtotime($worktime) > strtotime($shiftTimeDiff)) {

                  $time1 = strtotime($shiftTimeDiff);
                  $time2 = strtotime($worktime);
                  $totalhours = abs(($time2 - $time1)/3600);
              }
              
         }
         /*Calculate total hours if shift ends beyond next day for working day/holiday/weeklyoff */
          if($shift_start_timestamp < $morning_start_timestamp && $shift_end_timestamp >= $morning_start_timestamp) {
              $next_date = date('Y-m-d', strtotime($mark_row['date']. '+1day'));
              $nextDayShiftObj = $main_obj->getEmployeeShiftByDate($code, $next_date);
              $nextDay_shiftIntime = $nextDayShiftObj['allData']['Shift_From'];
              $shiftstart_strachtime = $nextDayShiftObj['allData']['ShiftStart_SwTime'];
              $last_time = strtotime($nextDay_shiftIntime) - strtotime($shiftstart_strachtime);
              $last_time_with_date = date('Y-m-d H:i:s', strtotime($next_date) + $last_time);
              $attallc_sql = "SELECT PunchDate, cast((PunchDate) as time) as out_time from attendanceall where cast((PunchDate) as date) = '".$next_date."' and cast(PunchDate as datetime) <='".$last_time_with_date."' and EMP_CODE = '".$code."'";
              $attc_res=query($query,$attallc_sql,$pa,$opt,$ms_db);
              $markc_row = $fetch($attc_res);
              $nextDay_shiftOuttime = $markc_row['out_time'];
              $totalhours = abs((strtotime($nextDay_shiftOuttime) - strtotime($shiftEndTime))/3600);

          }   

          /*Calculate number of days for working/Holiday/Weekly off */
          // echo $totalhours; die;
          if(!empty($totalhours)) {
              if($totalhours >= $Required_halfday) {
                $returnNoOfDays ="0.5";
              }
              if($totalhours >= $Required_fullday) { 
                $returnNoOfDays ="1";
              }
           }

           /*Check OD is allowed for this day type*/
          if(($IS_OD_ALLOWED == 1) && !empty($mark_row['odStatus']) && ($mark_row['odStatus'] =='Approved')) {
              $returnNoOfDays = "";
              /*Check OD is allowed for this day type for configuration level*/
              if($global_obj -> getEmployeeOUDetail($code, $newDate, $jsonOdData)) {
                  /*Calculate number of days for OD */
                  if(trim($mark_row['odType']) == 'Second Half' || trim($mark_row['odType']) == 'First Half') {
                    $returnNoOfDays ="0.5";
                  }
                  if(trim($mark_row['odType']) == 'Full Day') { 
                    $returnNoOfDays ="1";
                  }
              } else {
                  echo 0;
                  exit;
              }   
          }

          $tableHtml.="<table width='100%' cellpadding='0' cellspacing='2' border='0' id='detailtable'><tbody><tr><th style='text-align:left'>Day Type</th><th style='text-align:left'>Planned In Time</th><th style='text-align:left'>Planned Out Time </th><th style='text-align:left'>Actual In Time</th><th style='text-align:left'>Actual Out Time </th></tr><tr>";
          $tableHtml.="<td>".$daytype."</td>";
          $tableHtml.="<td>".$shiftStartTime."</td>";
          $tableHtml.="<td>".$shiftEndTime."</td>";
          $tableHtml.="<td>".$inTime."</td>";
          $tableHtml.="<td>".$outTime."</td>";
          $tableHtml.="</tr><table>";

          if($tableHtml == '' || $returnNoOfDays == '') {
            echo json_encode(array('success'=>FALSE,'table'=>$tableHtml,'noOfDays'=>$returnNoOfDays));
          } else {
            echo json_encode(array('success'=>TRUE,'table'=>$tableHtml,'noOfDays'=>$returnNoOfDays));
          }

        } else {
          echo 8;
        }
      }
    else{
      echo 0;
    }
  }

 
  if($_REQUEST['type'] == "add"){ 
      $fromDate     = dateConversion($_REQUEST['fromDate']);
      $daysval      = $_REQUEST['daysval'];
      $reason       = $_REQUEST['reason'];
      $week_val     = $_REQUEST['week_val'];
      $plan_in      = $_REQUEST['plan_in'];
      $plan_out     = $_REQUEST['plan_out'];
      $actual_in    = $_REQUEST['actual_in'];
      $actual_out   = $_REQUEST['actual_out'];
      $level        = $_REQUEST['level'];
      $code         = $_REQUEST['code'];
      $autoapprover = $_REQUEST['autoapprover'];

      if(empty($autoapprover)){
          $lev = explode(",", $level);
      }else{
          $lev = explode(",", $autoapprover);
      }
      if($wfmethod == 'automatic'){
          $approvingstatus ='2';
      } else{
          $approvingstatus ='1';
      }

      $checksql="select count(compOffId) as num from compOff where CreatedBy='$code' and wd_date='$fromDate' and action_status IN('1','2','5','7') ";
      $checkRes= query($query,$checksql,$pa,$opt,$ms_db);
      $numAtten= $fetch($checkRes);

      if($numAtten['num'] >= 1){
          echo 2;
      } else {
          for($i=0;$i<count($lev);$i++){
              if($i==0){
                  $flag = '1';
              } else {
                  $flag= '0';
              }
              
              $sql="insert into compOff (wd_date,reason,noOfDays,day_type,planned_INtime,planned_OUTtime,actual_INtime,actual_OUTtime
                ,CreatedBy,approvedBy,action_status,flag_val,CreatedOn) VALUES
                ('$fromDate','$reason','$daysval','$week_val','$plan_in','$plan_out','$actual_in','$actual_out','$code','$lev[$i]','$approvingstatus','$flag',GETDATE())";
              $result = query($query,$sql,$pa,$opt,$ms_db);
              
              if($result){
                  echo 1;
                  if($flag == 1){
                      $queMail= "Select Emp_Name,OEMailID from HrdMastQry where Emp_Code ='$lev[$i]'";
                      $resMail= query($query,$queMail,$pa,$opt,$ms_db);
                      $appMail=$fetch($resMail);
                      $requesterSql="Select Emp_Name,Emp_Code from HrdMastQry where Emp_Code ='$code'";
                      $requesterRes = query($query,$requesterSql,$pa,$opt,$ms_db);
                      $requesterRow= $fetch($requesterRes);
                      $to=trim($appMail['OEMailID']);
                      $mes = ucwords(strtolower($appMail[0])).",";
                      $m =str_replace(' ', '', $mes);

                      $fDate = $_POST['fromDate'];
                      $ApproverName=$m;
                      $reqName=$requesterRow[0];
                      $reqCode=$requesterRow[1];
                      $subject =getSubject('16','Create CompOff');
                      $message =getBody('16','Create CompOff');
                      eval("\$subject = \"$subject\";");
                      eval("\$message = \"$message\";");
                      str_replace('$', "", $subject);
                      str_replace('$', "", $message);
                      $mail1=mymailer('donotreply@sequelone.com',$subject,$message,$appMail['OEMailID']);
                  }
              } else {
                  echo 0;
              }
          }
          
      }
  }
 
  function dateConversion($sDate){
      $aDate = explode('/', $sDate);
      @$sMySQLTimestamp = sprintf( '%s-%s-%s 00:00:00', @$aDate[2], @$aDate[1], @$aDate[0] );
      return $sMySQLTimestamp;
  }
function sum_the_time($time1, $time2) {
  $times = array($time1, $time2);
  $seconds = 0;
  foreach ($times as $time)
  {
    list($hour,$minute,$second) = explode(':', $time);
    $seconds += $hour*3600;
    $seconds += $minute*60;
    $seconds += $second;
  }
  $hours = floor($seconds/3600);
  $seconds -= $hours*3600;
  $minutes  = floor($seconds/60);
  $seconds -= $minutes*60;
  return "{$hours}:{$minutes}:{$seconds}";
}
?>