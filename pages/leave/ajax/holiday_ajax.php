
<?php
include ('../../db_conn.php');
include ('../../configdata.php');

if($_POST['type']=="init"){
    $tablename=$_POST['table'];
    $pre=$_POST['pre'];
    $status=$_POST['status'];
    $name=$_POST['name'];
    if($pre=='buss' || $pre=='subBuss'){
        $col_name=$pre.'Name' ;
        $col_status='status';
        $col_code=$pre."_code";
        $col_id=$pre."ID";
    }
    elseif($pre=='TYPE' || $pre=='FUNCT' || $pre=='SubFunct' || $pre=='Country'|| $pre=='City'){
        $col_name=$pre.'_Name' ;
        $col_status='status';
        $col_code=$pre."_code";
        $col_id=$pre."ID";
    }
    elseif ($pre=='H'){
        $col_name=$pre.'desc' ;
        $col_status=$pre.'_status';
        $col_code=$pre."code";
        $col_id="HolidayID";
    }
    elseif ($pre=='WLoc'){
        $col_name=$pre.'_Name' ;
        $col_status='status';
        $col_code=$pre."_code";
        $col_id="WorkLocID";
    }
    elseif ($pre=='COMP'){
        $col_name=$pre.'_NAME' ;
        $col_status='Comp_status';
        $col_code=$pre."_CODE";
        $col_id=$pre."ID";    }
    else{
        $col_name=$pre.'_Name' ;
        $col_status=$pre.'_status';
        $col_code=$pre."_code";
        $col_id=$pre."ID";
    }
    ?>
       <?php
        $sql = "SELECT $col_code,$col_id,$col_name,$col_status FROM $tablename ";
        $resultq=query($query,$sql,$pa,$opt,$ms_db);
        //$list=1;
        while( $row = $fetch($resultq)) {
            ?>
            <option value="<?php echo $row[1] ?>">
                <?php echo $row[2].'('.$row[0].')'; ?>
            </option>
        <?php
            //$list++ ;
        }
        //echo $list;
        ?>
    <?php
}
else if($_POST['type']=="insert"){
    $hDate=$_POST['hDate'];
    $loc=implode(",",$_POST['loc']);
    $hname=$_POST['hname'];
    $comp=implode(",",$_POST['comp']);
    $bus=implode(",",$_POST['bus']);
    $subbus=implode(",",$_POST['subbus']);
    $wloc=implode(",",$_POST['wloc']);
    $func=implode(",",$_POST['func']);
    $subfunc=implode(",",$_POST['subfunc']);
    $cost=implode(",",$_POST['cost']);
    $proc=implode(",",$_POST['proc']);
    $grd=implode(",",$_POST['grd']);
    $desg=implode(",",$_POST['desg']);


    $sqlq = "INSERT INTO HOLIDAYS(HDATE,LOC_CODE,HCODE,HDESC,H_Status,COMP_CODE,BUS_CODE,SUBBUS_CODE,WLOC_CODE,FUNC_CODE,SUBFUNC_CODE,COST_CODE,PROC_CODE,GRD_CODE,DSG_CODE) VALUES 
  (convert(datetime,'$hDate',103),'$loc','','$hname','1','$comp','$bus','$subbus','$wloc','$func','$subfunc','$cost','$proc','$grd','$desg')";
  
    $resultq=query($query,$sqlq,$pa,$opt,$ms_db);
    if($resultq){
        echo "1";
    }
    else{
        echo "2";
    }
}