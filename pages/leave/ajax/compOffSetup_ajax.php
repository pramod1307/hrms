<?php
  include '../../db_conn.php';
  include ('../../configdata.php');
  date_default_timezone_set("Asia/Kolkata");
  include ('../../Attendance/Events/weekyOff-v2.php');
  include ('../../Attendance/Events/holidays-v2.php');
  global $num;
 
  if($_REQUEST['type'] == "workday"){
      $code    = $_REQUEST['code'];
      $fordate = str_replace('/', '-', $_REQUEST['fromDate']);
      $daytype = '';
      $inTime  = '';
      $outTime = '';
      $newDate = date('Y-m-d',strtotime($fordate)); 

      $sqlSetup="select Holiday,Weekly,Shiftday,convert(char(5),Halfday,108) as Halfday,convert(char(5),Fullday,108) as Fullday from compoff_setup where compSetID=1";
      $resSetup=query($query,$sqlSetup,$pa,$opt,$ms_db);
      $rowSetup = $fetch($resSetup);

      
          $public_holidays= getHolidays($newDate,$newDate,$code);
         // print_r($$public_holidays);
          for($i=0;$i<count($public_holidays); $i++){ 
              $array2[] = $public_holidays[$i]['start'];
          }
          if(in_array($newDate,$array2)) {
              $daytype="Holiday";
          }
          else
          {
            $daytype=0;
          }
      if($daytype == '0'){
          $weeklyOff = getWeeklyOff($code,$newDate,$newDate);
          $array1 = array();
          for($i=0;$i<count($weeklyOff); $i++){ 
              $array1[] = $weeklyOff[$i]['start'];
          }
          if(in_array($newDate,$array1)){ 
              $daytype="Weekly_Off";
          }
          else
          {
            $daytype=0;
          }
      }
      if($daytype == '0'){
          $daytype = 'Working';
      }
//echo $daytype;
      //$Required_halfday = $rowSetup['Halfday'];
$Required_halfday = '04:00';
$Required_fullday = '04:00';
      //$Required_fullday = $rowSetup['Fullday'];
      $returnNoOfDays   = "";
      $tableHtml        = "";

      /*-------------Mark Past Attendance------------*/
      $mark_sql="select * from attendancesch where Emp_code='".$code."' AND date = '".$newDate."'";
      $mark_res=query($query,$mark_sql,$pa,$opt,$ms_db);
      $mark_result = $fetch($mark_res);
      $shiftendtime=$mark_result['Shift_end'];
      $shiftid=$mark_result['ShiftId'];
      //$mark_past_id=$mark_result['mark_past_id'];
      $Shiftmendatryhrs=$mark_result['Shiftmendatryhrs'];
      $Shiftstart=$mark_result['Shift_start'];
$odstatus=$mark_result['odStatus'];
      $shiftendt=$newDate.' '.$shiftendtime;
      $shiftendt=date('Y-m-d H:i',strtotime($shiftendt));
      if(!empty($mark_result['mark_past_id']))
      {
          $overStay = $mark_result['Overstay'];
           $InTime=$mark_result['Intime'];
          $OutTime=$mark_result['Outtime'];
          $totalhours=$mark_result['Outtime'];
      }
      else
      {
          $tim='08:30';
          $shiftendtime1=date("H:i",strtotime($shiftendtime));
//secs = strtotime($shiftendtime1)-strtotime("00:00:00");
  //        $result = date("H:i",strtotime($tim)+$secs);
           $r= sum_the_time($tim, $shiftendtime1); 
         // $restime=strtotime($shiftendtime1)+strtotime($tim);
          //echo $result."qqq".$shiftendtime1;
          if($r > 24)
          {
            $m=1;
            $date2=date("Y-m-d", strtotime($newDate. ' + ' . $m . 'day'));
            $secs = strtotime($shiftendtime1)-strtotime("00:00:00");
            $restime = date("H:i",strtotime($tim)+$secs);
            //echo "sachin";
          }
          else
          {
            $date2=$newDate;
            $restime=$r;
            //echo "yadav";
          }
          $datetime1=$date2.' '.$restime;
          //$result = substr($myStr, 0, 5);
          $att_sql="select top 1 convert(varchar(16),PunchDate,121) as PunchDate from ATTENDANCEall where EMP_CODE='".$code."' and CAST(PunchDate AS DATETIME) <='$datetime1' and (CAST(PunchDate AS DATE)='$date2' OR CAST(PunchDate AS DATE)='$newDate') order by PunchDate desc";
          $m_res=query($query,$att_sql,$pa,$opt,$ms_db);
          $ma_result = $fetch($m_res);
          /*$att_sql="SELECT * FROM fnPunchTime('$code','$newDate')";
          $m_res=query($query,$att_sql,$pa,$opt,$ms_db);
          $ma_result = $fetch($m_res);*/
          if($mark_result['Intime']!='' && strtotime($mark_result['Intime']) >0)
          {
                $InTime=$mark_result['Intime'];
          }
          else
          {
               $InTime=$shiftendt;
          }
         // $InTime=;
//echo $shiftendtime;
          if($ma_result['PunchDate']!='')
          {
               $OutTime=$ma_result['PunchDate'];
          }

 	if(strtotime($shiftendtime) < strtotime('00:01:00'))
          {//echo strtotime($shiftendtime) ;
               $OutTime=$shiftendt;
          }
	else
          {
		$OutTime=$mark_result['Outtime'];
          }
          
          
        }//echo $OutTime;
	//$InTime=date("Y-m-d H:i", strtotime($InTime));
         //echo $OutTime."ppppp".$shiftendt.date('Y-m-d').$newDate;
          $date_a = new DateTime($OutTime);
          $date_b = new DateTime($shiftendt);
          $interval = date_diff($date_a,$date_b);
		if(date('Y-m-d')!=$newDate){
          $overStay=$interval->format('%H:%i');}else{$overStay='00:00';}
          $date_c = new DateTime($OutTime);
          $date_d = new DateTime($InTime);
          $interval = date_diff($date_c,$date_d);
          $totalhours=$interval->format('%h:%i');
         
//echo "rrr".$overStay."qqqq".$Required_halfday;
         //echo $Required_halfday."qqqqqq".$overStay.$newdate;
          if($daytype == "Holiday" || $daytype == "Weekly_Off"){
              if(!empty($totalhours)){
                 if(strtotime($totalhours)>=strtotime($Required_halfday) )
                    {
                      $returnNoOfDays ="1";
                    }
                    else if(strtotime($totalhours)>=strtotime($Required_fullday) )

                    {
                     $returnNoOfDays ="1";
                    }
              }
 if($odstatus =='Approved')
		{ 			$returnNoOfDays ="1";

		}

          }else
          {
              if(!empty($overStay)){
                 if(strtotime($overStay)>=strtotime($Required_halfday) )
                    {
                      $returnNoOfDays ="1";
                    }
                    else  if(strtotime($overStay)>=strtotime($Required_fullday) )
                    {
                     $returnNoOfDays ="1";
                    }
              }
          }
        //echo "yyy".$daytype."qqqqq".$returnNoOfDays."aaaaaaa".$totalhours;

        if($daytype == 'Working'){  
          if(strtotime($InTime)!=strtotime($OutTime))
          {
            $temp_inpunch=date("H:i", strtotime($InTime));
            $temp_outpunch=date("H:i", strtotime($OutTime));
          }
          else{
            $temp_inpunch='';
            $temp_outpunch='';
          }
            $shift_In=date("H:i", strtotime($mark_result['Shift_start']));
            $shift_out=date("H:i", strtotime($mark_result['Shift_end']));      
            
          }
          else  if($daytype == "Holiday" || $daytype == "Weekly_Off")
          {
           if(strtotime($InTime)!=strtotime($OutTime))
          { //echo $InTime."aaaaaaaaa".$OutTime;
            $temp_inpunch=date("H:i", strtotime($InTime));
            $temp_outpunch=date("H:i", strtotime($OutTime));
          }else{
            $temp_inpunch='';
            $temp_outpunch='';
          }
            $shift_In='';
            $shift_out='';
          }

      else{
          $temp_inpunch='';
          $temp_outpunch='';
          $shift_In='';
          $shift_out='';
      }
        $tableHtml.="<table width='100%' cellpadding='0' cellspacing='2' border='0' id='detailtable'><tbody><tr><th style='text-align:left'>Day Type</th><th style='text-align:left'>Planned In Time</th><th style='text-align:left'>Planned Out Time </th><th style='text-align:left'>Actual In Time</th><th style='text-align:left'>Actual Out Time </th></tr><tr>";
            
              $tableHtml.="<td>".$daytype."</td>";
		$tableHtml.="<td>".$shift_In."</td>";
              $tableHtml.="<td>".$shift_out."</td>";
              $tableHtml.="<td>".$temp_inpunch."</td>";
              $tableHtml.="<td>".$temp_outpunch."</td>";
             
              $tableHtml.="</tr><table>";
              echo json_encode(array('success'=>FALSE,'table'=>$tableHtml,'noOfDays'=>$returnNoOfDays));
    
  }

 
  if($_REQUEST['type'] == "add"){ 
      $fromDate     = dateConversion($_REQUEST['fromDate']);
      $daysval      = $_REQUEST['daysval'];
      $reason       = $_REQUEST['reason'];
      $week_val     = $_REQUEST['week_val'];
      $plan_in      = $_REQUEST['plan_in'];
      $plan_out     = $_REQUEST['plan_out'];
      $actual_in    = $_REQUEST['actual_in'];
      $actual_out   = $_REQUEST['actual_out'];
      $level        = $_REQUEST['level'];
      $code         = $_REQUEST['code'];
      $autoapprover = $_REQUEST['autoapprover'];
      if(empty($autoapprover)){
          $lev = explode(",", $level);
      }else{
          $lev = explode(",", $autoapprover);
      }
      
      if($wfmethod == 'automatic'){
          $approvingstatus ='2';
      } else{
          $approvingstatus ='1';
      }

      $checksql="select count(compOffId) as num from compOff where CreatedBy='$code' and wd_date='$fromDate' and action_status IN('1','2','5','7') ";
      $checkRes= query($query,$checksql,$pa,$opt,$ms_db);
      $numAtten= $fetch($checkRes);
      
      if($numAtten['num'] >= 1){
          echo 2;
      } else {
          for($i=0;$i<count($lev);$i++){
              if($i==0){
                  $flag = '1';
              } else {
                  $flag= '0';
              }
              
              $sql="insert into compOff (wd_date,reason,noOfDays,day_type,planned_INtime,planned_OUTtime,actual_INtime,actual_OUTtime
                ,CreatedBy,approvedBy,action_status,flag_val,CreatedOn) VALUES
                ('$fromDate','$reason','$daysval','$week_val','$plan_in','$plan_out','$actual_in','$actual_out','$code','$lev[$i]','$approvingstatus','$flag',GETDATE())";
              $result = query($query,$sql,$pa,$opt,$ms_db);
              
              if($result){
                  echo 1;
                  if($flag == 1){
                      $queMail= "Select Emp_Name,OEMailID from HrdMastQry where Emp_Code ='$lev[$i]'";
                      $resMail= query($query,$queMail,$pa,$opt,$ms_db);
                      $appMail=$fetch($resMail);
                      $requesterSql="Select Emp_Name,Emp_Code from HrdMastQry where Emp_Code ='$code'";
                      $requesterRes = query($query,$requesterSql,$pa,$opt,$ms_db);
                      $requesterRow= $fetch($requesterRes);
                      $to=trim($appMail['OEMailID']);
                      $mes = ucwords(strtolower($appMail[0])).",";
                      $m =str_replace(' ', '', $mes);

                      $fDate = $_POST['fromDate'];
                      $ApproverName=$m;
                      $reqName=$requesterRow[0];
                      $reqCode=$requesterRow[1];
                      $subject =getSubject('16','Create CompOff');
                      $message =getBody('16','Create CompOff');
                      eval("\$subject = \"$subject\";");
                      eval("\$message = \"$message\";");
                      str_replace('$', "", $subject);
                      str_replace('$', "", $message);
                      $mail1=mymailer('donotreply@sequelone.com',$subject,$message,$appMail['OEMailID']);
                  }
              }else{
                  echo 0;
              }
          }
          
      }
  }
 
  function dateConversion($sDate){
      $aDate = explode('/', $sDate);
      @$sMySQLTimestamp = sprintf( '%s-%s-%s 00:00:00', @$aDate[2], @$aDate[1], @$aDate[0] );
      return $sMySQLTimestamp;
  }
function sum_the_time($time1, $time2) {
  $times = array($time1, $time2);
  $seconds = 0;
  foreach ($times as $time)
  {
    list($hour,$minute,$second) = explode(':', $time);
    $seconds += $hour*3600;
    $seconds += $minute*60;
    $seconds += $second;
  }
  $hours = floor($seconds/3600);
  $seconds -= $hours*3600;
  $minutes  = floor($seconds/60);
  $seconds -= $minutes*60;
  return "{$hours}:{$minutes}:{$seconds}";
}
?>