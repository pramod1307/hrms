function changeStatus(status,id){

$.ajax({
            type:"POST",
            url:"ajax/attendance_Allowance_ajax.php",
            data:{type:'statusUpdate', status:status, id:id},
            success: function(result){
                if(result ==1){
                    toastmsg("Successfully Updated");
                    location.reload();
                }
                else{
                     toasterrormsg("Error in updation");
                }
            }
        });

}

/*--------- Start Add/Edit Attendance Allowance ----------*/
function attAllow_action(id,action){

    if(action == 'edit'){

        var datastring = 'id=' + id + '&action=' + action;
        $.ajax({
            type:"POST",
            url:"ajax/attendance_Allowance_ajax.php",
            data:{type:'checkstatus', id:id},
            success: function(result){
                if(result ==1){
                    $.ajax({
                        type: "POST",
                        url: "ajax/edit/edit_AttendanceAllowance_ajax.php",
                        data: datastring,
                        success: function (html) {

                            $('#modalbody').html(html);

                            $('#attAllowpopup').modal({
                                show: true

                            });
                            // var loc_val = $('#hiddenloc').val();
                            // var split_loc = loc_val.split(",");
                            // for(i=0;i<split_loc.length;i++){
                            //     alert(split_loc[i]);
                            //     $('[value="split_loc['+i+']"]').prop('checked',true);
                            // }
                           getSelectedVal();

                            $('#close').click(function() {
                                location.reload();
                            });
                        }
                    });
                }
                else{
                     toasterrormsg("Please First Active Rule");
                }
            }
        });
       

    }
    else{
        $.ajax({
            type: "POST",
            url: "ajax/add/add_AttendanceAllowance_ajax.php",
            data: datastring,
            success: function (data) {

                $('#modalbody').html(data);

                $('#attAllowpopup').modal({
                    show: true

                });
                $('#close').click(function() {
                    location.reload();
                });
            }
        });


    }


}
/*--------- End Add/Edit Attendance Allowance ----------*/


$(document).ready(function(){

    var counter = 2;


    $("#addButton").click(function () {

    if(counter>10){
            alert("Only 10 textboxes allow");
            return false;
    }
   
     
    var temp1=parseInt($("#hideVal").val()); 
    var leaveVal = $('#days'+temp1).val();
    var ratesVal = $('#rates'+temp1).val();
    
    if(leaveVal == ''){
        toasterrormsg("Enter No. of Leave/Absent");
        return false;
    }
    if(ratesVal ==''){
        toasterrormsg("Enter Rates");
        return false;

    }

    var newTextBoxDiv = $(document.createElement('div'))
         .attr({id: 'TextBoxDiv' + counter,
            class: "row"
            });

    newTextBoxDiv.after().html('<label class="col-md-3 control-label">Min No. of Leave/Absent </label>' +
          '<div class="col-md-1"><input type="text" class="form-control" onkeypress="return isNumber(event)" name="mindays" id="mindays' + counter + '"></div><label class="col-md-3 control-label">Max No. of Leave/Absent </label>' +
          '<div class="col-md-1"><input type="text" class="form-control" onkeypress="return isNumber(event)" name="maxdays" id="maxdays' + counter + '"></div><label class="col-md-1 control-label" style="text-align:left;">Rates</label><div class="col-md-2">'+
            '<input type="text" class="form-control" name="rates" onkeypress="return isNumber(event)" id="rates' + counter + '"</div></br>');

    newTextBoxDiv.appendTo("#TextBoxesGroup");

 if(counter>=2)
    {
        $("#removeButton").show();
    }

    counter++;
   
    var temp=parseInt($("#hideVal").val());
    temp=temp+1;
    $("#hideVal").val(temp);
    //alert(textboxcount);
     });

     $("#removeButton").click(function () {
    if(counter==2){

          alert("No more textbox to remove");

          return false;
       }
    counter--;
    var temp=parseInt($("#hideVal").val());
    temp=temp-1;
    $("#hideVal").val(temp);
   // alert(textboxcount);

    if(counter==2)
    {
        $("#removeButton").hide();
    }

        $("#TextBoxDiv" + counter).remove();

     });


     
});
$("#getButtonValue").click(function () {

    var msg = '';
    for(i=1; i<counter; i++){
      msg += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
    }
          alert(msg);
     });



function submitAttendAllow(action){

    var noOfTextbox = parseInt($("#hideVal").val());
 //   var datastring = $("#form_sample_1").serialize();

   // alert(datastring);

    
    // //console.log(daysArray);    
var daysArray1 =[];
    var daysArray2 =[];
    var ratesArray =[];
    if(action != "add"){
        type='update'; 
        daysArray1= $("#mindays1").val();
        daysArray2= $("#maxdays1").val();
        ratesArray = $("#rates1").val();

    }
    else{
         type='insert';
    
    for($i=1;$i<=noOfTextbox;$i++){
        
        daysArray1.push($("#mindays"+$i).val());
    }
    for($i=1;$i<=noOfTextbox;$i++){
        
        daysArray2.push($("#maxdays"+$i).val());
    }
    for($i=1;$i<=noOfTextbox;$i++){
        
        ratesArray.push($("#rates"+$i).val());
    }
    }

    if(action != "add"){
   // alert("..."+shiftsArray[0]+"...");
        if(daysArray1[0] == " " || daysArray1.length == 0)
        {
            toasterrormsg("Enter Min Day");
            return false;
        }
        if(daysArray2[0] == " " || daysArray2.length == 0)
        {
            toasterrormsg("Enter Max Day");
            return false;
        }
        if(ratesArray == " " || ratesArray.length == 0)
        {
            toasterrormsg("Enter Amount");
            return false;
        }
   }
   else{

        for(var i=0;i<noOfTextbox;i++){
            
            //alert(shiftsArray[i]);
            if(daysArray1[i] == "" || daysArray1.length == 0 ||daysArray1[i] == undefined)
            {
                toasterrormsg("Enter Min Day");
                return false;
            }
        }
        for(var i=0;i<noOfTextbox;i++){
            
            //alert(shiftsArray[i]);
            if(daysArray2[i] == "" || daysArray2.length == 0 ||daysArray2[i] == undefined)
            {
                toasterrormsg("Enter Max Day");
                return false;
            }
        }
        for(var j=0;j<noOfTextbox;j++){
            //alert("---"+ratesArray[$i]+"---");
            if(ratesArray[j] == "" || ratesArray.length == 0 || ratesArray[j] == undefined)
            {
                toasterrormsg("Enter Amount");
                return false;
            }
        }

   }
   //alert(action);
    // if(hDate == "")
    // {
    //     toasterrormsg("Select To Holiday Date");
    //     return false;
    // }
    // if(hname == "")
    // {
    //     toasterrormsg("Enter Holiday Description");
    //     return false;
    // }
        $.ajax({
            type:"POST",
            url:"ajax/attendance_Allowance_ajax.php",
            data:{type:type,daysArray1:daysArray1,daysArray2:daysArray2,ratesArray:ratesArray,action:action},
            success: function(result){
                if(result ==3){
                    toasterrormsg("Rule Already Exist");
                }
                else if(result ==2){
                     toasterrormsg("Error in insertion");
                }
                else if(result ==4){
                     toastmsg("Updated Successfully");
                     location.reload();
                }
                else if(result ==5){
                     toasterrormsg("Error in updation");
                }
                else{
                    toastmsg("Successfully inserted");
                    location.reload();
                }
            }
        });
   


}

function getpay(){
 var grd_code = $("#grd").val();
    $.ajax({
            type:"POST",
            
            url:"ajax/attendance_Allowance_ajax.php",
            data:{type:'payval', grd_code:grd_code},
            success: function(html){
                $("#paycycle").html(html);
            }
        });
}
function getemp(){
    var grd_code = $("#grd").val();

    $.ajax({
            type:"POST",
            
            url:"ajax/attendance_Allowance_ajax.php",
            data:{type:'empval', grd_code:grd_code},
            success: function(html){
                $("#emp").html(html);
                $("#emp").multiselect('rebuild');
                 $('#emp').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,

        });
    $('#empdiv').find('.multiselect-selected-text').text('Select Employee');
   $('#emp').siblings().find('li').css('margin-left','10px')
            }

        });
}

function getData(){

    var datastring = $("#attendanceCal").serialize();
    var year =$("#year").val();
    var month =$("#month").val();
    var grade =$("#grd").val();
    if(year == ''){
        toasterrormsg("Please Select Year");
        return false;
    }
    if(month == ''){
        toasterrormsg("Please Select Month");
        return false;
    }
    if(grade == ''){
        toasterrormsg("Please Select Grade");
        return false;
    }
   //alert("pramod");
    $.ajax({

        type: "POST",
        url: "ajax/attendanceCal.php?type=empdata",
        data: datastring,
         beforeSend: function(){
                loading();
            },
        success: function(html) {
            unloading();
            $("#tablediv").show();
            $("#showtable").html(html);
        },
        error: function(html){
alert("error");
        }
       
    });

   }
function showAdvanceFilter(sval,code)
{
    if(sval == '1'){
        $("#showorg").val('0');
         //   $("#emps").hide();
        $("#multisearch").show();
        $("#multisearch1").show();
        $("#multisearch2").show();
        $("#multisearch3").show();
        $('#company-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#business-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#sub-business-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
        //    $('#location-select').multiselect({
        //     includeSelectAllOption: true,
        //     enableFiltering: true
        // });
           $('#work-location-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#function-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#sub-function-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#cost-master-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#process-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#grade-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });
           $('#designation-select').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true
        });

    }
    else{
            $("#showorg").val('1');
           // alert('hello');
           // $("#emps").show();
            $("#multisearch").hide();
            $("#multisearch1").hide();
            $("#multisearch2").hide();
            $("#multisearch3").hide();
    }
    var gadha= ['Select Company','Select Business','Select Sub-Business','Select Work Location','Select Function','Select Sub-Function','Select Cost Master','Select Process','Select Grade', 'Select Designation'];
    var gha = $(gadha).length;
    for(i=0;i<=gha;i++){
        $('.selectKunal').eq(i).find('.multiselect-selected-text').text(gadha[i]);
    }
    if(sval == '1'){
         getSelectedVal();
    }
   
}  


function mymultiselect(){
$('a.multiselect-all').click(function(){
    var meraChecker = $(this).find('span').hasClass('checked')
if(meraChecker==true){
 $('.checker').children('span').addClass('checked'); 
}
else{
 $('.checker').children('span').removeClass('checked'); 
}    
    
});
}





function getSelectedVal(){

var y,kunal;
$('.hiddenloc').each(function(){
y = $(this).val();
kunal = y.split(",");
var faltuAray = [];
var xi = kunal.length;
for (i =0 ; i < xi; i++) {
 var kd = kunal[i];   
$(this).siblings('.multiselect-native-select').find('.multiselect-container [value="'+kd+'"]').prop('checked',true).addClass('bhondu'+i);
$(this).siblings('.multiselect-native-select').find('.multiselect-container [value="'+kd+'"]').parents('li').addClass('active');
var faltuObj = $('.bhondu'+i).parent().text(); 

faltuAray.push(faltuObj)    
}
var faltuLength = faltuAray.length;
$(this).siblings('.multiselect-native-select').find('button').attr('title',faltuAray);
$(this).siblings('.multiselect-native-select').find('button span').text(faltuLength+' selected');
//alert("aaa"+kunal[0]+"bbbb");
//console.log(kunal[0]);
//console.log(xi);
// if(kunal[0]==" "||kunal[0]==null||kunal[0]==undefined){
//     console.log('12121');
// }
if(xi==1 && (kunal[0]==" "||kunal[0]==null||kunal[0]==undefined) ){
    //console.log('monika');
$(this).siblings('.multiselect-native-select').find('.multiselect-container input').prop('checked',true).addClass('bhondu');
$(this).siblings('.multiselect-native-select').find('.multiselect-container input').parents('li').addClass('active');
$(this).siblings('.multiselect-native-select').find('.multiselect-container label').text();
$(this).siblings('.multiselect-native-select').find('button span').text('All selected');
}
y =""
kunal.length = 0
})

}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

// function AllowanceCal(){
//     var gyear = $("#year").val();
//     var gmonth = $("#month").val();
//     var emp_code = $("#emp").val();
//     $.ajax({
//                         url: "../../pages/Attendance/calling_page.php?type=payrollCycle&emp_code="+emp_code,
//                         type: 'POST',
//                         data: {monthGet: gmonth},
//                         success: function (result) {
                         
//                          console.log(result);
                            
//                         }
//                     });
            
// }

 