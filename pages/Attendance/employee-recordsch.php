<?php
//ini_set("display_errors","on"); error_reporting(E_ALL);
date_default_timezone_set('America/New_York');
ini_set('max_execution_time', 0);
ini_set("memory_limit", "-1");
ini_set("output_buffering", "On");
ini_set("upload_max_filesize", "20M");
ini_set("post_max_size", "21M");
include ('../db_conn.php');
include ('../global_class.php');
include ('../configdata.php');
include ('Events/holidays-v2.php');
include ('Events/weekyOff-v2.php');
include ('Events/empAttendance-v2.php');
include ('Events/empLeaves-v2.php');
$globalClassObj	=	new global_class();
$enddatesch=date('Y-m-d');
//$enddatesch='2016-12-30';
//$startdatesch='2016-12-01';
$startdatesch= date('Y-m-d', strtotime("-30 days"));
$start = strtotime($startdatesch);
$end = strtotime($enddatesch);
$days_between = ceil(abs($end - $start) / 86400);
//echo $days_between;
//echo $enddatesch."aaa".$startdatesch;
$sql_del="delete from attendancesch where date between '$startdatesch' and '$enddatesch'";
$result=query($query,$sql_del,$pa,$opt,$ms_db);
$sql="select Emp_Code,EMP_NAME from HrdMastQry where GRD_CODE='3' and Status_Code='01'";
$result=query($query,$sql,$pa,$opt,$ms_db);
$rowcount=$num($result);
//echo $rowcount;
$u=0;
while($res=$fetch($result))
{
	//echo "aaa".$u;
	$emp=$res['Emp_Code'];
	//$emp='Dummy0005';
	$schdata=getEmployeeAttendance($emp,$startdatesch,$enddatesch);
	$phpObj = json_decode($schdata,true);
	//echo "<pre>"; print_r($phpObj); echo "</pre>";
	for($j=0;$j<30;$j++)
	{
		//echo $day1=date("Y-m-d", strtotime($startdatesch . ' + ' . $j . 'day'));
		$dat=$phpObj[$j]['start'];
		$instart=$phpObj[$j]['startTime'];
		$endtime=$phpObj[$j]['endTime'];
		$shiftInOutTime=$phpObj[$j]['shiftInOutTime'];
		preg_match('#\((.*?)\)#', $shiftInOutTime, $match);
		//print $match[1];
		$instart1=date("H:i", strtotime($instart));
		$endtime1=date("H:i", strtotime($endtime));
		$shiftInOutTime=explode("-",$match[1]);
		$shiftintime =date("H:i", strtotime($shiftInOutTime[0]));
		$shiftouttime =date("H:i", strtotime($shiftInOutTime[1]));

		/************************************************/
		if($instart=="" || $endtime=="")
		{
			$tworkinghrs=0;
			$latecomming=0;
			$earlygoing=0;
		}
		else
		{
		
		$int1=(timeDiff($instart,$endtime)/60)/60;
		$int2=(timeDiff($shiftintime,$shiftouttime)/60)/60;
		//echo $int2;
    	if($int1 <=0){
        	 $endDate =date('Y-m-d',strtotime($dat. "+1 days"));
     	}else{
             $endDate=$dat;
     	}
     	if($int2 <=0){
        	 $endDate1 =date('Y-m-d',strtotime($dat. "+1 days"));
     	}else{
             $endDate1=$dat;
     	}
     	$timestamp = new DateTime();
     	$t1=$dat." ".$instart1;
     	$t2=$endDate." ".$endtime1;
        $dat1 = new DateTime($t1);//print_r($dat1);
        $date2 = new DateTime($t2);
        $interval = $dat1->diff($date2);
        $tworkinghrs=$interval->h .":" . $interval->i;
        $t3=$dat." ".$shiftintime;
        $dat2 = new DateTime($t3);
        $t4=$dat." ".$shiftouttime;
        $dat3 = new DateTime($t4);
        //echo "aa".$j."aaaaaaaaaaaaaaa";
        if($dat2 >= $dat1)
        {
        	$latecomming=0;
        }
        else
        {
        	$interval = $dat1->diff($dat2);
        	$latecomming=$interval->h .":" . $interval->i;
        }
        if($date2 >= $dat3)
        {
        	$earlygoing=0;
        }
        else
        {
        	$interval = $date2->diff($dat3);
        	$earlygoing=$interval->h .":" . $interval->i;
        }
        /*$date2 = new DateTime($endDate." ".$endtime1);
        $interval = $dat1->diff($date2);
        $tworkinghrs=$interval->h .":" . $interval->i;*/
    	}
        /*******************************/
        $sqlOT="select finalOtHour from overtime_balances where OT_Date='$dat' and Emp_code='$emp'";
		$resultOT=query($query,$sqlOT,$pa,$opt,$ms_db);
		if($resOT=$fetch($resultOT))
		{
			$OT_hrs=$resOT['finalOtHour'];
		}
		else
		{
			$OT_hrs="";	
		}
		$Schedule_Shift=$phpObj[$j]['shiftName'];
		$sql11="select s.Shift_Name from rost_change r,ShiftMast s where r.shiftMaster=s.ShiftMastId and r.RosterDate='$dat' and r.Emp_code='$emp'";
		$result11=query($query,$sql11,$pa,$opt,$ms_db);
		if($res11=$fetch($result11))
		{
			$actualshift=$res11['Shift_Name'];
		}
		else
		{
			$actualshift=$Schedule_Shift;	
		}
		
		$statsunew=$phpObj[$j]['presetShortStatus'];
		$sql1="insert into attendancesch(Emp_code,Schedule_Shift,Actual_Shift,Shift_start,Shift_end,Intime,Outtime,Latemin,Earlymin,date,Overtime,Workinghrs,status)values('$emp','$Schedule_Shift','$actualshift','$shiftInOutTime[0]','$shiftInOutTime[1]','$instart','$endtime','$latecomming','$earlygoing','$dat','$actualshift','$tworkinghrs','$statsunew')";
	echo $sql1;
		$result123=query($query,$sql1,$pa,$opt,$ms_db);
		//echo "emp_code=".$emp."date=".$dat."status=".$phpObj[$j]['presetShortStatus']."Shiftschedule=".$phpObj[$j]['shiftName']."Actual shift=".$actualshift."Intime=".$instart."Outtime=".$endtime."Total working hrs=".$tworkinghrs."latecomming=".$latecomming."earlygoing=".$earlygoing."<br>";
			//echo $sql1;die;
	}
	
	 //echo count($phpObj);
	//echo "<pre>"; print_r($phpObj); echo "</pre>";
	 
	$u++;
}

//print_r($schdata);
function countNumberOfDaysFromTwoDates($firstDate, $secondDate){
	return abs(floor(strtotime($firstDate) / (60 * 60 * 24)) - floor(strtotime($secondDate) / (60 * 60 * 24)));
}

//check for calendar request
if(isset($_GET['fromType']) && $_GET['fromType'] == 'calendar'){
	$emp_code  	= $_GET['emp_code'];
	$startDate	=	$_POST['start'];
	$endDate	=	$_POST['end'];
	
	//check first date or start date
	$dayRequest = date('d', strtotime($startDate));

	//if given day is already 1 then do same otherwise get next month first date
	$finalStartDate = ($dayRequest != 1) ? strtotime('first day of next month', strtotime($startDate)):strtotime($date);
	$month_end = strtotime('last day of this month', $finalStartDate);

	$startDate = date('Y-m-d', $finalStartDate);
	$endDate   = date('Y-m-d', $month_end);

	/*if(date('m', strtotime($endDate)) == date('m'))
		$endDate = date('Y-m-d');*/

	$isStatus = true;
	//echo $startDate.' - '.$endDate; die;
	getEmployeeAttendance($emp_code, $startDate, $endDate);

}

//call payroll calendar or defauult start date and date record
if((isset($_GET['fromType']) && $_GET['fromType'] == 'payrollCalendar') || $_GET['fromType'] == 'teamCal'){
	$emp_code  	= $_GET['emp_code'];
	$startDate	=	$_POST['start'];
	$endDate	=	$_POST['end'];
	getEmployeeAttendance($emp_code, $startDate, $endDate);
}

function getEmployeeShiftByDateNew($empCode, $date){
    $date = trim($date);
    if(empty($empCode) || empty($date)) return '';

    global $HTTP_HOST;
    global $query;
    global $sql;
    global $pa;
    global $opt;
    global $ms_db;
    global $fetch;
    global $num;

    /*$SqlQuery = "SELECT ShiftPatternMastID, Shift_Name, Shift_From,Shift_To, cast(rosterstart as date) attfrom,cast(rosterend as date) attto 
			FROM Rosterqry 
			WHERE EMP_CODE='".$empCode."' AND 
			DATEADD(month, DATEDIFF(month, 0, '".$date."'), 0) 
			BETWEEN DATEADD(month, DATEDIFF(month, 0, rosterstart), 0) AND DATEADD(month, DATEDIFF(month, 0, rosterend), 0)";*/

    /*$SqlQuery = "select RosterName from ( (select a.Emp_Code,convert(varchar(10),a.start_rost,103) as start_rost,convert(varchar(10),a.end_rost,103) as 
	end_rost,a.RosterName,a.auto_period,a.created_on from Roster_schema a inner join (select max(RostID) as RostID from Roster_schema 
	where Emp_Code IN ('".$empCode."') and end_rost <= '".$date."' group by Emp_Code) b on a.RostID = b.RostID) 
	union 
	(select Emp_Code, convert(varchar(10),start_rost,103) as start_rost,convert(varchar(10),end_rost,103) as end_rost,RosterName,auto_period,created_on 
	from Roster_schema where Emp_Code IN ('".$empCode."') and start_rost >= '".$date."' and end_rost <= '".$date."' )) a order by a.created_on asc";*/

	$SqlQuery = "SELECT * FROM RosterQry WHERE EMP_CODE = '".$empCode."' AND '".$date."' BETWEEN RosterStart AND RosterEnd";

    $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
    $returnData = array();
    if($num($result) > 0){
	    $row = $fetch($result);	    
		    $returnData['shiftName']	= $row['Shift_Name'];
			$returnData['shiftInOutTime']='('.date('h:i A', strtotime($row['Shift_From'])).'-'.date('h:i A', strtotime($row['SHIFT_TO'])).')';
	}
    return $returnData;
}

//calculate all attenndance from start date to end date and return json response
function getEmployeeAttendance($empCode, $startDate, $endDate) {
	//echo $empCode.", ".$startDate.", ".$endDate; die;
	$empCode 	= trim($empCode);
	$startDate 	= trim($startDate);
	$endDate 	= trim($endDate);
	$emp_code 	= $empCode;
	$endDate 	= (empty($endDate))?$startDate:$endDate;

	$startDate = date('Y-m-d', strtotime($startDate));
	$endDate = date('Y-m-d', strtotime($endDate));

	//check date is valid or not
	$startDateFormat = date_parse($startDate);
	$endDateFormat = date_parse($endDate);

	if(empty($empCode) || empty($startDate) || empty($endDate) || $startDateFormat['error_count'] > 0 || $endDateFormat['error_count'] > 0) return false;

	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	global $firstRosterStart;
	$step = '+1 day';
	$date_output_format = 'Y-m-d';
	
	//find employee auto shift and upload into roster schema
	$startDateMonth = date('m', strtotime($startDate));	$startDateYear = date('Y', strtotime($startDate));
	$endDateMonth = date('m', strtotime($endDate));	$endDateYear = date('Y', strtotime($endDate));
	$globalClassObj	=	new global_class();
	if($startDateMonth == $endDateMonth){	
		$globalClassObj->uploadAutoshift( $empCode, $startDateMonth, $startDateYear );
	}
	if($startDateMonth != $endDateMonth)
	{
		$globalClassObj->uploadAutoshift( $empCode, $startDateMonth, $startDateYear );
		$globalClassObj->uploadAutoshift( $empCode, $endDateMonth, $endDateYear );
	}

	$daysInMonth 	= countNumberOfDaysFromTwoDates($startDate,$endDate); //calculate days of two dates
	$firstDate 		= date('Y-m-d', strtotime($startDate)); //$year.'-'.$month.'-01';					//first date
	$lastDate 		= date('Y-m-d', strtotime($endDate)); //$year.'-'.$month.'-'.$daysInMonth;		//last date
	$firstDateNum 	= date('d', strtotime($startDate));										//first date number
	$lastDateNum 	= $daysInMonth;								//last date number
    
    $current = strtotime($startDate);
    $last = strtotime($endDate);
    $currentYear = date('Y', strtotime($startDate));

    while( $current <= $last ) {
        $dateValue = date($date_output_format, $current);
        $dayValue= date('d', $current);
        $current = strtotime($step, $current);
        $result[$dateValue] = array('type'=>'null','title'=>'','shiftName'=>'','shiftInOutTime'=>'','formattedDate'=>date('l, d M Y',strtotime($dateValue)),'presetStatus'=>'','presetShortStatus'=>'','start'=>$dateValue,'className'=>'','birthdate'=>array(),'annidate'=>array(),'startTime'=>'','endTime'=>'','allDay'=>true,'status'=>'','reason'=>'','timeSpan'=>'');
    }

    $getEmpRosterDate = "SELECT top 1 cast(RosterStart as date) as attFrom, cast(RosterEnd as date) as attTo from rosterqry where emp_code='".$empCode."' order by RostId asc";

	$resultq00=query($query,$getEmpRosterDate,$pa,$opt,$ms_db);
	if($num($resultq00)){
		$rowq00 = $fetch($resultq00);
		$firstRosterStart = explode('-', $rowq00['attFrom']);
		$firstRosterEnd = explode('-', $rowq00['attTo']);		
	}

	$sqlq0="SELECT ShiftPatternMastID, Shift_Name, Shift_From,Shift_To, cast(rosterstart as date) attfrom,cast(rosterend as date) attto 
			FROM Rosterqry 
			WHERE EMP_CODE='".$empCode."' AND 
			DATEADD(month, DATEDIFF(month, 0, '".$startDate."'), 0) 
			BETWEEN DATEADD(month, DATEDIFF(month, 0, rosterstart), 0) AND DATEADD(month, DATEDIFF(month, 0, rosterend), 0)";

	$resultq0=query($query,$sqlq0,$pa,$opt,$ms_db);

	if($num($resultq0)){
		while ($rowq0 = $fetch($resultq0)){
			$rosterStart = $rowq0['attfrom'];
			$rosterEnd 	= $rowq0['attto'];
			$shift_From = date('g:i a', strtotime($rowq0['Shift_From']));
			$shift_To 	= date('g:i a', strtotime($rowq0['Shift_To']));
			$shift_Name = $rowq0['Shift_Name'];
		}
	}

	$birthdates 	= array();
	//$annidates		= getAnniDates($month,$year);
	$annidates		= array();
	$holidays 		= getHolidays($startDate, $endDate, $emp_code);
	$leaves 		= getLeaves($startDate, $endDate, $emp_code);

	$leavesFilterArray = array();
	if(!empty($leaves)){
		foreach($leaves AS $leaveKey => $leaveValue){
			$cleanDate = date('Y-m-d', strtotime($leaveValue['start']));
			$leavesDates[] = $cleanDate;
			$leavesFilterArray[$cleanDate] = array($leaveValue['status'],'leave');
		} 
	}

	$firstDateNumc 	= date('d', strtotime($startDate)); //01;
	$lastDateNumc 	= date('d', strtotime($endDate)); //01;
	$weeklyOffs 	= getWeeklyOff($emp_code,$startDate,$endDate);  								// dependent events 

	$weeklyHolidayDates = array_merge($weeklyOffs,$holidays);
	$weeklyHolidayCombineDates	=	array();
	foreach($weeklyHolidayDates as $key=>$value) { $weeklyHolidayCombineDates[] = trim($value['start']); }
	$attendances 	= getAttendance($emp_code,$startDate,$endDate,$weeklyHolidayCombineDates);   // solely dependent events 

	$attregularised	= getAttendanceRegularised($emp_code,$startDate,$endDate);
	$getODRequest	= getODRequest($emp_code, $startDate, $endDate);
	$gatePassHours	= getGatePassHours($emp_code, $startDate, $endDate);

	//Filter array of markpast attendance
	$attRegularisedFilterArray = array();
	if(!empty($attregularised)){
		foreach($attregularised AS $attRegKey => $attReguValue){
			$attRegularDates[] = $attReguValue['start'];
			$attRegularisedFilterArray[$attReguValue['start']] = array($attReguValue['status'],'leave');
		}
	}
	
	// add birthdays list on calendar date event--------------------------------------------------------
	if($birthdates !=null || count($birthdates) !=0){
		for($m=0;$m<=count($birthdates);$m++){
			$date1 = explode('-', $birthdates[0]['start']);
			$cleanDate = date('Y-m-d', strtotime($birthdates[0]['start']));
			array_push($result[$cleanDate]['birthdate'],$birthdates[0]['bemp_record']);
			unset($birthdates[0]);
			$birthdates = array_values($birthdates);
			$m=0;

		}
	}


	//add anniversaries list on calendar date event-----------------------------------------------------
	if($annidates !=null || count($annidates) !=0){
		for($m=0;$m<=count($annidates);$m++){
			$date1 = explode('-', $annidates[0]['start']);
			$cleanDate = date('Y-m-d', strtotime($annidates[0]['start']));
			array_push($result[$cleanDate]['annidate'],$annidates[0]['bemp_record']);
			unset($annidates[0]);
			$annidates = array_values($annidates);
			$m=0;
		}
	}

	//add gate pass hours on calendar date event-----------------------------------------------------
	if($gatePassHours["result"] !=null || count($gatePassHours["result"]) !=0){
		foreach($gatePassHours["result"] AS $key => $value){
			$cleanDate = date('Y-m-d', strtotime($value['start']));
			$result[$cleanDate]['gatePassLate'] = array('start' => $cleanDate, 'totalHours' => $value['time'], 'reason' => $value['reason']);
		}
		if( $gatePassHours["count"] >= 2 ) {
			$result[$cleanDate]['gatePassLatePenalty'] = array('gatePassPenaltyMsssage' => "GatePass Movement Penalty - ".$gatePassHours["count"]);	
		}

	}

	// add attendance from cattendanceqry table in calendar date events---------------------------------
	if(!empty($attendances)){
		for($a=0;$a<count($attendances);$a++){
			$keyPair 	=	date('Y-m-d', strtotime($attendances[$a]['start']));

			if($result[$keyPair]['type']=='weeklyoff' || $result[$keyPair]['type']=='pholiday'){
				$result[$keyPair]['className'] = array($result[$keyPair]['className'].' '.$attendances[$a]['className'][0]);
			}else{
				$result[$keyPair]['className']=$attendances[$a]['className'];	
			}

			$result[$keyPair]['type'] = $attendances[$a]['type'];
			$result[$keyPair]['title'] = $attendances[$a]['title'];
			$result[$keyPair]['shiftName'] = $attendances[$a]['shiftName'];
			$result[$keyPair]['shiftInOutTime'] = $attendances[$a]['shiftInOutTime'];
			$result[$keyPair]['presetShortStatus'] = $attendances[$a]['presetShortStatus'];
			$result[$keyPair]['presetStatus'] = $attendances[$a]['presetStatus'];
			$result[$keyPair]['startTime'] = $attendances[$a]['startTime'];
			$result[$keyPair]['endTime'] = $attendances[$a]['endTime'];
			$result[$keyPair]['timeSpan'] = $attendances[$a]['timeSpan'];
			$result[$keyPair]['attendanceMessage'] = $attendances[$a]['attendanceMessage'];
			$result[$keyPair]['lates'] = $attendances[$a]['lates'];
			$result[$keyPair]['isLateComing'] = $attendances[$a]['isLateComing'];
			$result[$keyPair]['isEarlyGoing'] = $attendances[$a]['isEarlyGoing'];
			$result[$keyPair]['outTimeWithDate'] = $attendances[$a]['outTimeWithDate'];
			
			/******************gate pass warning/penalty*************/
			foreach($gatePassHours['gatePassMessage'] AS $gatePassDate => $gateValues) {
				if( isset($gatePassHours['gatePassMessage'][$keyPair]) ){
					$result[$keyPair]['attendanceMessage'] = (!empty($attendances[$a]['attendanceMessage'])) ? $attendances[$a]['attendanceMessage'].' and '.$gatePassHours['gatePassMessage'][$keyPair]['message']:$gatePassHours['gatePassMessage'][$keyPair]['message'];

					if($gatePassHours['gatePassMessage'][$keyPair]['isPenalty']){
						$result[$keyPair]['title'] = 'Half Day';
						$result[$keyPair]['presetShortStatus'] = 'H';
						$result[$keyPair]['presetStatus'] = 'Half Day';
						$result[$keyPair]['className'] = 'yellow circle-legend';
					}
				}
			}

			if(($attendances[$a]['startTime'] == '' && $attendances[$a]['endTime'] != '') || ($attendances[$a]['startTime'] != '' && $attendances[$a]['endTime'] == ''))
			{
				$result[$keyPair]['className']=array('miss-punch circle-legend');;	
			}

			if($result[$attendances[$a]['num']]['presetStatus'] != 'Absent'){
				//$arrayCheckOfDuplicateEvent variable used for check if employee worked on weekly off or holiday
				$arrayCheckOfDuplicateEvent['attendance'][$keyPair] = $result[$keyPair]['className'];
			}
		}
	}

	// add regularised attendance in calendar date events (status, reason)-------------------------------
	if(!empty($attregularised)){
		for($c=0;$c<count($attregularised); $c++){
			$date1 = explode('-', $attregularised[$c]['start']);
			$cleanDate = date('Y-m-d', strtotime($attregularised[$c]['start']));
			$shiftData = getEmployeeShiftByDateNew($emp_code, $cleanDate);
			if(count($shiftData) > 0){
				$result[$cleanDate]['shiftName']	='Shift : '.$shiftData['shiftName'];
				$result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
			}

			if($result[$cleanDate]['type']=='weeklyoff' || $result[$cleanDate]['type']=='pholiday'){
				$result[$cleanDate]['className'] = array($result[$cleanDate]['className'][0].' '.$attregularised[$c]['className'][0]);
			} else {
				$result[$cleanDate]['className']=$attregularised[$c]['className'];	
			}

			//$arrayCheckOfDuplicateEvent variable used for check if employee worked on weekly off or holiday
			$arrayCheckOfDuplicateEvent['attendanceRegularised'][] = $attregularised[$c]['startTime'];

			$result[$cleanDate]['type']=$attregularised[$c]['type'];
			$result[$cleanDate]['title']=$attregularised[$c]['title'];
			$result[$cleanDate]['presetStatus'] = $attregularised[$c]['title'];
			$result[$cleanDate]['reason'] = $attregularised[$c]['reason'];
			$result[$cleanDate]['status'] = $attregularised[$c]['status'];
			$result[$cleanDate]['presetShortStatus']=$attregularised[$c]['presetShortStatus'];
			$result[$cleanDate]['startTime'] = $attregularised[$c]['startTime'];
			$result[$cleanDate]['endTime'] = $attregularised[$c]['endTime'];
					
		}
	}

	// add approved/pending leaves in calendar date event -----------------------------------------------
	if(!empty($leaves)){
		for($b=0;$b<count($leaves);$b++){
			$date1 = explode('-', $leaves[$b]['start']);
			$leaveDate = date('Y-m-d', strtotime($leaves[$b]['start']));

			if($result[$leaveDate]['type']=='weeklyoff' || $result[$leaveDate]['type']=='pholiday'){
				$result[$leaveDate]['className'] = array($result[$leaveDate]['className'][0].' '.$leaves[$b]['className'][0]);
			}else{
				$result[$leaveDate]['className']= ($leaves[$b]['status'] != 'Pending' && $leaves[$b]['status'] != 'Absent') ? 'blue_over circle-legend' : $leaves[$b]['className'][0];	
			}
			
			$result[$leaveDate]['type']=$leaves[$b]['type'];
			$result[$leaveDate]['title']=$leaves[$b]['title'];
			$result[$leaveDate]['presetShortStatus']=$leaves[$b]['presetShortStatus'];
			$result[$leaveDate]['status']=$leaves[$b]['status'];
			$result[$leaveDate]['reason']=$leaves[$b]['reason'];
			$result[$leaveDate]['firststatus'] = $leaves[$b]['status'];
			$result[$leaveDate]['LvType'] = $leaves[$b]['lvType'];
			
			//Check if user applied regularised attendance on same OD date
			if(in_array($leaves[$b]['start'],$attRegularDates)){
				$result[$leaveDate]['title']= 'Leave & AttReg';
				$result[$leaveDate]['presetStatus'] = 'Leave and '.$leaves[$b]['title'];
				$result[$leaveDate]['status'] = $leaves[$b]['status'].'/'.$attRegularisedFilterArray[$leaves[$b]['start']][0];
				$result[$leaveDate]['firststatus'] = $leaves[$b]['status'];
				$result[$leaveDate]['secondstatus'] = $attRegularisedFilterArray[$leaves[$b]['start']][0];
				
				$result[$leaveDate]['status'] = '(Leave & AttReg)';
				if($leaves[$b]['status'] == 'Approved' && $attRegularisedFilterArray[$leaves[$b]['start']][0] == 'Approved'){
					$result[$leaveDate]['title']= 'Present';	
					$result[$leaveDate]['className'] = array("green2 circle-legend");					
				}else if(($leaves[$b]['status'] == 'Approved' || $attRegularisedFilterArray[$leaves[$b]['start']][0] == 'Approved') && ($leaves[$b]['status'] != 'Approved' || $attRegularisedFilterArray[$leaves[$b]['start']][0] != 'Approved')){
					$result[$leaveDate]['className'] = array("presentAbsent circle-legend");
				}else{
					$result[$leaveDate]['title']= 'Absent';
					$result[$leaveDate]['className'] = array("red circle-legend");
				}
			}
		}
	}
	
	// add public holidays on specific festival date in calendar date event-----------------------------
	for($j=0;$j<count($holidays);$j++){
		$date1 = explode('-', $holidays[$j]['start']);
		$cleanDate = date('Y-m-d', strtotime($holidays[$j]['start']));
		$result[$cleanDate]['type']='pholiday';

		$shiftData = getEmployeeShiftByDateNew($emp_code, $holidays[$j]['start']);
		if(count($shiftData) > 0){
			$result[$cleanDate]['shiftName']	='Shift : '.$shiftData['shiftName'];
			$result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
		}

		$result[$cleanDate]['title'] = (isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? 'POH' : $holidays[$j]['title'];
		$result[$cleanDate]['presetShortStatus']='F';
		$result[$cleanDate]['className']=(isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? $arrayCheckOfDuplicateEvent['attendance'][$cleanDate][0].' '."bgnone" : array("green");
		//replace original class to present status class when POW, POH status found
			$result[$cleanDate]['className'] = ($result[$cleanDate]['title'] == 'POW' || $result[$cleanDate]['title'] == 'POH') ? "green2 circle-legend bgnone" : $result[$cleanDate]['className'];
	}

	// add OD request in calendar date events (status, reason)-------------------------------
	if(!empty($getODRequest)){
		for($c=0;$c<count($getODRequest); $c++){
			$date1 = explode('-', $getODRequest[$c]['start']);
			$cleanDate = date('Y-m-d', strtotime($getODRequest[$c]['start']));
			$shiftData = getEmployeeShiftByDateNew($emp_code, $getODRequest[$c]['start']);
			if(count($shiftData) > 0){
				$result[$cleanDate]['shiftName']	='Shift : '.$shiftData['shiftName'];
				$result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
			}

			if($result[$cleanDate]['type']=='weeklyoff' || $result[$cleanDate]['type']=='pholiday'){
				$result[$cleanDate]['className'] = array($result[$cleanDate]['className'][0].' '.$getODRequest[$c]['className'][0]);
			}else{
				$result[$cleanDate]['className']=$getODRequest[$c]['className'];	
			}
			
			$result[$cleanDate]['type']=$getODRequest[$c]['type'];			
			$result[$cleanDate]['title']=$getODRequest[$c]['title'];
			$result[$cleanDate]['presetStatus'] = $getODRequest[$c]['title'];
			$result[$cleanDate]['status'] = $getODRequest[$c]['status'];
			$result[$cleanDate]['firststatus'] = $getODRequest[$c]['status'];
			
			$result[$cleanDate]['reason'] = $getODRequest[$c]['reason'];
			$result[$cleanDate]['presetShortStatus']= $getODRequest[$c]['presetShortStatus'];
			$result[$cleanDate]['startTime'] = $getODRequest[$c]['startTime'];
			$result[$cleanDate]['endTime'] = $getODRequest[$c]['endTime'];
			
			//Check if user applied leave request on same OD date
			if(in_array($cleanDate,$leavesDates)){
				$result[$cleanDate]['title']= 'OD & Leave';
				$result[$cleanDate]['presetStatus'] = $getODRequest[$c]['title'].' and Leave';
				
				$result[$cleanDate]['status'] = $getODRequest[$c]['status'].'/'.$leavesFilterArray[$cleanDate][0];
				
				$result[$cleanDate]['firststatus'] = $getODRequest[$c]['status'];
				$result[$cleanDate]['secondstatus'] = $leavesFilterArray[$cleanDate][0];
				$result[$cleanDate]['flagType'] = 'odandleave';				
				
				$result[$cleanDate]['status'] = '(OD & Leave)';

				if($getODRequest[$c]['status'] == 'Approved' && $leavesFilterArray[$cleanDate][0] == 'Approved'){
					$result[$cleanDate]['title']= 'Present';	
					$result[$cleanDate]['className'] = array("green2 circle-legend");					
				}else if(($getODRequest[$c]['status'] == 'Approved' || $leavesFilterArray[$cleanDate][0] == 'Approved') 
					&& ($getODRequest[$c]['status'] != 'Approved' || $leavesFilterArray[$cleanDate][0] != 'Approved')) {
					$result[$cleanDate]['className'] = array("presentAbsent circle-legend");
				}else{
					$result[$cleanDate]['title']= 'Absent';
					$result[$cleanDate]['className'] = array("red circle-legend");
				}
			}
			
			//Check if user applied regularised attendance on same OD date
			if(in_array($cleanDate,$attRegularDates)){
				$result[$cleanDate]['title']= 'AttReg & OD';
				$result[$cleanDate]['presetStatus'] = 'Attn Reg. and '.$getODRequest[$c]['title'];
				$result[$cleanDate]['status'] = $attRegularisedFilterArray[$cleanDate][0].'/'.$getODRequest[$c]['status'];
				$result[$cleanDate]['firststatus'] = $attRegularisedFilterArray[$cleanDate][0];
				$result[$cleanDate]['secondstatus'] = $getODRequest[$c]['status'];
				$result[$cleanDate]['flagType'] = 'odandattnreq';
				
				$result[$cleanDate]['status'] = '(AttReg & OD)';
				if($getODRequest[$c]['status'] == 'Approved' && $attRegularisedFilterArray[$cleanDate][0] == 'Approved'){
					$result[$cleanDate]['title']= 'Present';	
					$result[$cleanDate]['className'] = array("green2 circle-legend");					
				}else if(($getODRequest[$c]['status'] == 'Approved' || $attRegularisedFilterArray[$cleanDate][0] == 'Approved') && ($getODRequest[$c]['status'] != 'Approved' || $attRegularisedFilterArray[$cleanDate][0] != 'Approved')){
					$result[$cleanDate]['className'] = array("presentAbsent circle-legend");
				}else{
					$result[$cleanDate]['title']= 'Absent';
					$result[$cleanDate]['className'] = array("red circle-legend");
				}
			}
		}
	}

	// add empty date event as Absent in calendar date event ---------------------------------------------
	$current = strtotime($firstDate);
	$last = (isset($_POST['requestFrom']) && $_POST['requestFrom'] == 'payrollCalendar') ? strtotime($lastDate) : strtotime($endDate);
	$currentDateConverter = strtotime(date('Y-m-d'));
	while( $current <= $last ) {
        $syncDate = date('Y-m-d', $current);

        $shiftData = getEmployeeShiftByDateNew($emp_code, $syncDate);
		if(count($shiftData) > 0){
			$result[$syncDate]['shiftName']	='Shift : '.$shiftData['shiftName'];
			$result[$syncDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
		}

        $current = strtotime($step, $current);
        if($currentDateConverter >=  $current){
			if($result[$syncDate]['type']=='null'){
				$result[$syncDate]['type']='empty_attendance';
				$result[$syncDate]['title']='Absent';
				$result[$syncDate]['presetShortStatus']='A';
				$result[$syncDate]['presetStatus']='Absent';
				$result[$syncDate]['timeSpan']=0;
				$result[$syncDate]['className']=array('red circle-legend');
			}
		}
    }
	
	

	// add employee wise weeklyoff on calendar date event-----------------------------------------------
	if(!empty($weeklyOffs)){
		for($i=0;$i<count($weeklyOffs);$i++){
			$date2 = explode('-', $weeklyOffs[$i]['start']);
			$cleanDate = date('Y-m-d', strtotime($weeklyOffs[$i]['start']));
			$result[$cleanDate]['type']='weeklyoff';


			$shiftData = getEmployeeShiftByDateNew($emp_code, $cleanDate);
			if(count($shiftData) > 0){
				$result[$cleanDate]['shiftName']	='Shift : '.$shiftData['shiftName'];
				$result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
			}

			/******************gate pass warning/penalty*************/
			foreach($gatePassHours['gatePassMessage'] AS $gatePassDate => $gateValues) {
				if( isset($gatePassHours['gatePassMessage'][$cleanDate]) ){
					$result[$cleanDate]['attendanceMessage'] = (!empty($attendances[$a]['attendanceMessage'])) ? $attendances[$a]['attendanceMessage'].' and '.$gatePassHours['gatePassMessage'][$cleanDate]['message']:$gatePassHours['gatePassMessage'][$cleanDate]['message'];

					if($gatePassHours['gatePassMessage'][$cleanDate]['isPenalty']){
						$result[$cleanDate]['title'] = 'Half Day';
						$result[$cleanDate]['presetShortStatus'] = 'H';
						$result[$cleanDate]['presetStatus'] = 'Half Day';
						$result[$cleanDate]['className'] = 'yellow circle-legend';
					}
				}
			}

			$result[$cleanDate]['title'] = (isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? 'POW' : 'Weekly Off' ;
			$result[$cleanDate]['presetShortStatus']='W';

			$result[$cleanDate]['className']= (isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? $arrayCheckOfDuplicateEvent['attendance'][$cleanDate][0].' '."bgnone" : array("green");

			//replace original class to present status class when POW, POH status found
			$result[$cleanDate]['className'] = ($result[$cleanDate]['title'] == 'POW' || $result[$cleanDate]['title'] == 'POH') ? "green2 circle-legend bgnone" : $result[$cleanDate]['className'];
		}
	}
//prd($result);
	// unset weekly offs if they are on holiday's date--------------------------------------------------
	if($holidays != null && $weeklyOffs != null){
		for($i=0;$i<count($holidays);$i++){
			for($j=0;$j<count($weeklyOffs);$j++){
				$shiftData = getEmployeeShiftByDateNew($emp_code, $holidays[$i]['start']);
				$cleanDate = date('Y-m-d', strtotime($holidays[$i]['start']));
				if(count($shiftData) > 0){
					$result[$cleanDate]['shiftName']	='Shift : '.$shiftData['shiftName'];
					$result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
				}
				if(in_array($cleanDate,$weeklyOffs[$j],true)){
					unset($weeklyOffs[$j]);
					$weeklyOffs = array_values($weeklyOffs);
				}
				
			}
		}
	}




	foreach($result AS $key => $value){
		$data[] = $value;
	}

 	return json_encode($data,true);
}


if($_GET['type'] == 'payrollCalenndarHTML'){
	$emp_code = $_GET['emp_code'];
	if(empty($_POST['monthGet'])){
		
		$orgMastValue = $globalClassObj->getPayCycleOrgMastDt();
        $payCycleQuery = "SELECT TOP 1 HRDT.Emp_Code,HRDT.Comp_Code,PAYC.PY_Start
		FROM HrdTran HRDT,PayCycle PAYC
		WHERE
		HRDT.COMP_CODE=PAYC.PY_COMP AND ".$orgMastValue."=PY_OrgMastDt AND PAYC.PY_TYPE='F' AND HRDT.Emp_Code = '".$emp_code."'";

		$resultPayCycle = query($query, $payCycleQuery, $pa, $opt, $ms_db);
		$startDate = date('Y-m-d');

		if ($num($resultPayCycle)) {
		    $rowPayCycle = $fetch($resultPayCycle);
		    $PY_Start = $rowPayCycle['PY_Start'];
		    $startDate = date('Y').'-'.date('m').'-'.$PY_Start;
		    $startDate = date("Y-m-d", strtotime( "-1 month", strtotime( $startDate ) ));
		}

		$endDate = date("Y-m-d", strtotime( "+1 month", strtotime( $startDate ) ));
	}


	if(!empty($_POST['monthGet']) && $_POST['monthGet'] == 'next'){
		$startDate = date("Y-m-d", strtotime( "+1 month", strtotime( $_POST['startDate'] ) ));
		$endDate = date("Y-m-d", strtotime( "+1 month", strtotime( $_POST['endDate'] ) ));
	}

	if(!empty($_POST['monthGet']) && $_POST['monthGet'] == 'previous'){
		$startDate = date("Y-m-d", strtotime( "-1 month", strtotime( $_POST['startDate'] ) ));
		$endDate = date("Y-m-d", strtotime( "-1 month", strtotime( $_POST['endDate'] ) ));
	}

	//get month of start date
	$startDateMonth = date('m',strtotime($startDate));
	$startDateYear = date('Y',strtotime($startDate));

	//get month of end date
	$endDateMonth = date('m',strtotime($endDate));
	$endDateYear = date('Y',strtotime($endDate));

	$previousDates = "startDate: ".$startDate." endDate:".$endDate;
	/***************************/
	//Check actual grade code and replace previous start/end date
	$actualGradeCode = "SELECT TRN_DATE,DATEPART(DAY,Trn_Date) Trn_Date,Emp_Code,Grd_Code,PayCycle.PY_Start FROM HrdTran,PayCycle
				WHERE HrdTran.Grd_Code=PayCycle.PY_OrgMastDt AND HrdTran.Comp_Code=PayCycle.PY_Comp
				AND Emp_Code='14067' /*AND '2016-12-12' > Trn_Date AND Trn_Date < '2017-01-12'*/
				AND Trn_Date < '".$startDate."'
				ORDER BY Trn_Date DESC";
	$actualGradeCodeQuery = query($query, $actualGradeCode, $pa, $opt, $ms_db);
	if ($num($actualGradeCodeQuery)) {
	    $actualGradeCodeResult = $fetch($actualGradeCodeQuery);
		$PY_Start = $actualGradeCodeResult['PY_Start'];
	    $startDate = $startDateYear.'-'.$startDateMonth.'-'.$PY_Start;
	    //$startDate = date("Y-m-d", strtotime( "-1 month", strtotime( $startDate ) ));
	    $endDate = date("Y-m-d", strtotime( "+1 month", strtotime( $startDate ) ));
	}
	
	/***************************/

	$weekDayNumber = date('N', strtotime($startDate));
	$calendarMonthNameTitle = date('d F\'y', strtotime($startDate)).' to '.date('d F\'y', strtotime( "-1 day", strtotime($endDate)));
	$mainTitle = "My Payroll Calendar of ".date('F - y',strtotime($endDate));
	$days = abs(floor(strtotime($startDate) / (60 * 60 * 24)) - floor(strtotime($endDate) / (60 * 60 * 24)));

	$html .= "<tr>";
    for ($i = 1; $i <= 7; $i++) {
        $dow_text = date('D', strtotime("Sunday +{$weekDayNumber} days"));
        $html .= "<th class='myheader fc-day-header fc-widget-header fc-".strtolower($dow_text)."'>" . $dow_text . "</th>";
        $weekDayNumber = $weekDayNumber + 1;
    }
    $html .= "</tr>";
    $daysRowCount = $daysNumber = 0;
    for ($daysRow = 1; $daysRow <= $days; $daysRow++) {
        $daysRowCount++;
        if ($daysRowCount == 1) {
            $html .= "<tr>";
        }

        $numberRow = $daysNumber++;
        $date = date("d", strtotime($startDate . ' + ' . $numberRow . 'day'));
        $rowId = date("Y-m-d", strtotime($startDate . ' + ' . $numberRow . 'day'));

        $html .= "<td id='cal_" . $rowId . "' class='fc-event-container' style='height:108px'> <div class='datelabel'>" . $date . "</div></td>";

        if ($daysRowCount == 7) {
            $html .= "</tr>";
            $daysRowCount = 0;
        }
    }

    echo json_encode(array("empCode"=>$emp_code, "PreviousDates" => $previousDates, "NewStartDate"=>$startDate, "NewEndDate"=>$endDate, "htmlTable" => $html, "startDate" => $startDate, "endDate" => $endDate, "calendarMonthNameTitle" => $calendarMonthNameTitle, "mainTitle" => $mainTitle));
}
function timeDiff($firstTime,$lastTime) {
    $firstTime=strtotime($firstTime);
    $lastTime=strtotime($lastTime);
    $timeDiff=$lastTime-$firstTime;
    return $timeDiff;
}
?>