<?php
date_default_timezone_set('Asia/Kolkata');
include('../db_conn.php');
include('../configdata.php');
include('../global_class.php');
$globalClassObj = new global_class();
//check for calendar request
if (isset($_GET['fromType']) && $_GET['fromType'] == 'calendar') {
    $emp_code  = $_GET['emp_code'];
    $startDate = $_POST['start'];
    $endDate   = $_POST['end'];
    
    //check first date or start date
    $dayRequest = ltrim(date('d', strtotime($startDate)), 0);
    
    //if given day is already 1 then do same otherwise get next month first date
    $finalStartDate = ($dayRequest != 1) ? strtotime('first day of next month', strtotime($startDate)) : strtotime($startDate);
    $month_end      = strtotime('last day of this month', $finalStartDate);
    
    $startDate = date('Y-m-d', $finalStartDate);
    $endDate   = date('Y-m-d', $month_end);
    
    //getEmployeeAttendance($emp_code, $startDate, $endDate);
    getAttendanceFromSchedular($emp_code, $startDate, $endDate);
}

//call payroll calendar or defauult start date and date record
if ((isset($_GET['fromType']) && $_GET['fromType'] == 'payrollCalendar') || $_GET['fromType'] == 'teamCal') {
    $emp_code  = $_GET['emp_code'];
    $startDate = $_POST['start'];
    $endDate   = $_POST['end'];
    //getEmployeeAttendance($emp_code, $startDate, $endDate);
    getAttendanceFromSchedular($emp_code, $startDate, $endDate);
}

function getWeeklyOff($empCode, $todayDate)
{
    global $query;
    global $pa;
    global $opt;
    global $ms_db;
    global $num;
    global $fetch;
    $employeeShiftDetailQuery = "SELECT CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) Shift_From_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $todayDate . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
END as Shift_To_Date,

CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_MFrom AS DATETIME) AS DATETIME) Shift_MFrom_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_MFrom AS TIME)> CAST(ShiftMast.Shift_MTo AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $todayDate . "') AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
END as Shift_MTo_Date,ShiftMast.*, ShiftPatternMast.* FROM Roster_schema, att_roster, ShiftMast, ShiftPatternMast
            WHERE Roster_schema.Emp_Code = '" . $empCode . "' AND '" . $todayDate . "' BETWEEN Roster_schema.start_rost AND Roster_schema.end_rost
            AND Roster_schema.RosterName = att_roster.roster
            AND att_roster.shiftMaster = ShiftMast.ShiftMastId
            AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid";
    
    $employeeShiftDetailResult = query($query, $employeeShiftDetailQuery, $pa, $opt, $ms_db);
    
    if ($num($employeeShiftDetailResult) <= 0) {
        $employeeShiftDetailQuery  = "SELECT TOP 1 CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_From AS DATETIME) AS DATETIME) Shift_From_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_From AS TIME)> CAST(ShiftMast.Shift_To AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $todayDate . "') AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_To AS DATETIME) AS DATETIME)
END as Shift_To_Date,

CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_MFrom AS DATETIME) AS DATETIME) Shift_MFrom_Date,
CASE 
WHEN (CAST(ShiftMast.Shift_MFrom AS TIME)> CAST(ShiftMast.Shift_MTo AS TIME)) THEN CAST(CAST(DATEADD(d, 1, '" . $todayDate . "') AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
ELSE CAST(CAST('" . $todayDate . "' AS DATETIME)+ CAST(ShiftMast.Shift_MTo AS DATETIME) AS DATETIME)
END as Shift_MTo_Date,ShiftMast.*, ShiftPatternMast.* FROM Roster_schema, att_roster, ShiftMast, ShiftPatternMast WHERE Roster_schema.end_rost<='" . $todayDate . "' AND Roster_schema.Emp_Code = '" . $empCode . "' AND Roster_schema.auto_period = 1 AND Roster_schema.RosterName = att_roster.roster AND att_roster.shiftMaster = ShiftMast.ShiftMastId AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid ORDER BY end_rost DESC";
        $employeeShiftDetailResult = query($query, $employeeShiftDetailQuery, $pa, $opt, $ms_db);
    }
    
    $shiftPatterns = array(
        '7',
        '7',
        '7',
        '7',
        '7',
        '7'
    );
    
    if ($num($employeeShiftDetailResult) > 0) {
        $employeeShiftDetailResultRow = $fetch($employeeShiftDetailResult);
        $shiftPatterns                = array(
            $employeeShiftDetailResultRow['WeeklyOff1'],
            $employeeShiftDetailResultRow['WeeklyOff2'],
            $employeeShiftDetailResultRow['WeeklyOff3'],
            $employeeShiftDetailResultRow['WeeklyOff4'],
            $employeeShiftDetailResultRow['WeeklyOff5']
        );
    }
    
    $start       = new DateTime(date('Y-m-01', strtotime($todayDate)));
    $end         = new DateTime(date('Y-m-t', strtotime($todayDate)));
    $interval    = new DateInterval('P1D');
    $dateRange   = new DatePeriod($start, $interval, $end);
    $weekSet     = '';
    $weekNumber  = 0;
    $weeks       = array();
    $weekSetTemp = array();
    
    for ($date = $start; $date <= $end; $date->modify('+1 day')) {
        if (!in_array($date->format("W"), $weekSetTemp) && isset($shiftPatterns[$weekNumber])) {
            $weekSetTemp[] = $date->format("W");
            $weekSet       = explode(',', $shiftPatterns[$weekNumber]);
            $weekNumber++;
        }
        if (in_array($date->format('N'), $weekSet)) {
            //$weeks[$date->format("W")][] = $date->format('Y-m-d') . ' - ' . $date->format('N');
            $weeks[$date->format("W") . "_" . $date->format('N')] = $date->format('Y-m-d');
        }
    }
    return (in_array($todayDate, $weeks)) ? true : false;
}


function getMissingDates($myDates, $dateStart, $dateEnd)
{
    $missingDates = array();
    $dateStart    = date_create($dateStart);
    $dateEnd      = date_create($dateEnd);
    $interval     = new DateInterval('P1D');
    $period       = new DatePeriod($dateStart, $interval, $dateEnd);
    for ($day = $dateStart; $day <= $dateEnd; $day->modify('+1 day')) {
        $formatted = $day->format("Y-m-d");
        if (!in_array($formatted, $myDates))
            $missingDates[] = $formatted;
    }
    return $missingDates;
}

function getNullStatus($dateValue)
{
    return array(
        'type' => 'null',
        'title' => '',
        'shiftName' => '',
        'shiftInOutTime' => '',
        'formattedDate' => date('l, d M Y', strtotime($dateValue)),
        'presetStatus' => '',
        'presetShortStatus' => '',
        'start' => $dateValue,
        'className' => '',
        'birthdate' => array(),
        'annidate' => array(),
        'startTime' => '',
        'endTime' => '',
        'allDay' => true,
        'status' => '',
        'reason' => '',
        'timeSpan' => ''
    );
}

function getAttendanceFromSchedular($empCode, $startDate, $endDate, $args = array())
{
    global $HTTP_HOST;
    global $query;
    global $sql;
    global $pa;
    global $opt;
    global $ms_db;
    global $fetch;
    global $num;
    //echo $empCode.", ".$startDate.", ".$endDate; die;

    $startDate = date('Y-m-d', strtotime($startDate));
    $endDate   = date('Y-m-d', strtotime($endDate));
    
    //if (isset($_SESSION['companyDBData']['COMP_CODE']) && $_SESSION['companyDBData']['COMP_CODE'] == 'maxlife') {
        
        $dateForGetPreviousRecord = '2017-04-12';
        
        if (strtotime($startDate) <= strtotime($dateForGetPreviousRecord)) {
            include_once('employee-back-attendence.php');
            
            if (strtotime($endDate) > strtotime($dateForGetPreviousRecord)) {
                $startDate       = date("Y-m-d", strtotime("+1 day", strtotime($dateForGetPreviousRecord)));
                $payrollCalendar = 1;
            }
            
            if (!isset($payrollCalendar)) {
                foreach ($result AS $key => $value) {
                    $data[] = $value;
                }
                
                if (isset($args) && $args['responseType'] == 'array')
                    return json_encode($data);
                
                echo json_encode($data, true);
                exit;
            }
        }
    //}
    

    $getAttendanceQuery  = "SELECT * FROM attendancesch WITH (NOLOCK) WHERE Emp_code = '" . $empCode . "' AND date BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY date";
    $getAttendanceResult = query($query, $getAttendanceQuery, $pa, $opt, $ms_db);
    $recordFindArray     = array();
    
    if ($num($getAttendanceResult) > 0) {
        while ($row = $fetch($getAttendanceResult)) {

            $row['final_status']        = trim($row['final_status']);
            $row['Leavestatus']         = trim($row['Leavestatus']);
            $row['is_mark_past']        = trim($row['is_mark_past']);
            $row['odStatus']            = trim($row['odStatus']);
            $row['Intime']              = trim($row['Intime']);
            $row['Outtime']             = trim($row['Outtime']);
            $row['Shift_start']         = trim($row['Shift_start']);
            $row['Shift_end']           = trim($row['Shift_end']);
            $lateRemark                 = '';
            $thisDate                   = date('Y-m-d', strtotime($row['date']));
            $recordFindArray[$thisDate] = $thisDate;
            $inTime                     = (strtotime($row['Intime']) <= 0) ? "" : date('h:i A', strtotime($row['Intime']));
            $outTime                    = (strtotime($row['Outtime']) <= 0) ? "" : date('h:i A', strtotime($row['Outtime']));
            $shiftStartEndTime          = array();
            if ($row['Shift_start'] != '00:00:00.0000000') {
                $shiftStartEndTime[] = (strtotime($row['Shift_start']) > 0 && !empty($row['Shift_start'])) ? date('h:i A', strtotime($row['Shift_start'])) : '';
            }
            
            if ($row['Shift_end'] != '00:00:00.0000000') {
                $shiftStartEndTime[] = (strtotime($row['Shift_end']) > 0 && !empty($row['Shift_end'])) ? date('h:i A', strtotime($row['Shift_end'])) : '';
            }
            
            $shiftTime = ($row['isWeeklyOff'] == 1 || $row['isHoliday'] == 1) ? '' : "Shift :" . (!empty($row['Actual_Shift'])) ? $row['Actual_Shift'] : $row['Schedule_Shift'];
            if ($row['isWeeklyOff'] || $row['isHoliday']) {                
                $lateRemark = '';
            } else if ($row['Lateflag'] == 1) {
                $lateRemark = 'Late Coming';
            } else if ($row['Earlyflag'] == 1) {
                $lateRemark = 'Early Going';
            } else if ($row['Lateflag'] == 1 && $row['Earlyflag'] == 1) {
                $lateRemark = 'Late Coming & Early Going';
            }
            $shiftInOutTime   = ($row['isWeeklyOff'] == 1 || $row['isHoliday'] == 1 || count($shiftStartEndTime) <= 0) ? '' : "(" . implode(' - ', $shiftStartEndTime) . ")";
            $multiEventStatus = '';
            $POWPOHTime = '';
            if ($row['isWeeklyOff'] == 1 && (!empty($inTime) || !empty($outTime))) {
                $shiftTime = '';
                $multiEventStatus = 'Present On Weekly Off';
            }
            if ($row['isHoliday'] == 1 && (!empty($inTime) || !empty($outTime))) {
                $shiftTime = '';
                $multiEventStatus = 'Present On ' . ucwords($row['holidayTitle']);
                $POWPOHTime =   '('.$inTime." - ".$outTime.")";
            }
            if ($row['isHoliday'] == 1 && empty($inTime) && empty($outTime)) {
                $shiftTime = '';
                $multiEventStatus = ": " . ucwords($row['holidayTitle']);
            }
            
            if ($row['isWeeklyOff'] == 1 || $row['isHoliday'] == 1) {
                $shiftTime = '';
            }
            if ($row['isWeeklyOff'] == 1 && (!empty($inTime) || !empty($outTime))) {
                $shiftTime = '';
                $POWPOHTime =   '('.$inTime." - ".$outTime.")";
            }
            if (!empty($row['Leavestatus'])) {
                $multiEventStatus = "<br>" . $row['leaveNature'] . ' - ' . $row['LeaveTypeText'] . ' <br>Leave Status: ' . $row['Leavestatus'];
            }
            if ($row['is_mark_past'] == 1) {
                $multiEventStatus = "<br>MarkPast Attendance: " . $row['markPastStatus'] . "<br>Mark Past Remark:" . $row['markpast_remark'];
            }
            if (!empty($row['Leavestatus']) && $row['is_mark_past'] == 1) {
                $multiEventStatus = "<br>" . $row['leaveNature'] . ' ' . $row['LeaveTypeText'] . ' (' . $row['Leavestatus'] . ')' . "<br>MarkPast Attendance: " . $row['markPastStatus'] . "<br>Mark Past Remark:" . $row['markpast_remark'];
            }
            if (!empty($row['odStatus'])) {
                $multiEventStatus = "<br>" . $row['odType'] . ' OD - ' . $row['odStatus'] . '<br>OD Reason: ' . $row['odReason'];
            }
            if (!empty($row['odStatus']) && !empty($row['Leavestatus'])) {
                $multiEventStatus = "<br>" . $row['odType'] . ' OD - ' . $row['odStatus'] . '<br>OD Reason: ' . $row['reason'] . '<br>' . $row['leaveNature'] . ' - ' . $row['LeaveTypeText'] . ' <br>Leave Status: ' . $row['Leavestatus'];
            }
            if (($outTime != '' || $inTime != '') && !empty($row['Leavestatus'])) {
                $multiEventStatus = "<br>" . $row['leaveNature'] . ' - ' . $row['LeaveTypeText'] . ' <br>Leave Status: ' . $row['Leavestatus'];
            }
            if (!empty($row['odStatus']) && $row['is_mark_past'] == 1) {
                $multiEventStatus = "<br>" . $row['odType'] . ' OD - ' . $row['odStatus'] . '<br>OD Reason:' . $row['odReason'] . "<br>MarkPast Attendance: " . $row['markPastStatus'] . "<br>Mark Past Remark:" . $row['markpast_remark'];
            }
            if (($outTime != '' || $inTime != '') && !empty($row['odStatus'])) {
                $multiEventStatus = "<br>" . $row['odType'] . ' OD - ' . $row['odStatus'] . '<br>OD Reason:' . $row['odReason'];
            }
            
            if (strtotime($thisDate) > strtotime(date('Y-m-d')) && ($row['final_status'] == 'Absent' || $row['final_status'] == '')) {
                $result[] = getNullStatus($thisDate);
            } else {
                $result[] = array(
                    /* "type" => empty_attendance */
                    "title" => $row['final_status'],
                    "shiftName" => $shiftTime,
                    "shiftInOutTime" => $shiftInOutTime,
                    "formattedDate" => date('l, d M Y', strtotime($thisDate)),
                    "presetStatus" => $row['final_status'] . "<br>",
                    "presetShortStatus" => $row['status'],
                    "start" => $thisDate,
                    "className" => $row['html_class'],
                    "birthdate" => Array(),
                    "annidate" => Array(),
                    "startTime" => $inTime,
                    "endTime" => $outTime,
                    "allDay" => 1,
                    "status" => $row['final_status'],
                    "reason" => '',
                    "timeSpan" => 0,
                    "isLateComing" => $row['Lateflag'],
                    "isEarlyGoing" => $row['Earlyflag'],
                    "attendanceMessage" => $lateRemark,
                    "firstpresetStatus" => (!empty($multiEventStatus) > 0) ? $multiEventStatus : '',
                    "POWPOHTime" => $POWPOHTime
                );
            }
        }
    }
    
    $missingDates = getMissingDates($recordFindArray, $startDate, $endDate);
    
    if (count($missingDates) > 0) {
        foreach ($missingDates as $key => $value) {
            $unix_MissingDate = strtotime($value);
            $unix_todayDate   = strtotime(date('Y-m-d'));
            if ($unix_MissingDate <= $unix_todayDate) {
                $recordFindArray[$value] = $value;

                $result[]                = array(

                    'type' => 'empty_attendance',
                    'title' => 'Absent',
                    'shiftName' => '',
                    'shiftInOutTime' => '',
                    'formattedDate' => date('l, d M Y', strtotime($value)),
                    'presetStatus' => '',
                    'presetShortStatus' => 'A',
                    'start' => $value,
                    'className' => 'red circle-legend',
                    'birthdate' => array(),
                    'annidate' => array(),
                    'startTime' => '',
                    'endTime' => '',
                    'allDay' => true,
                    'status' => '',
                    'reason' => '',
                    'timeSpan' => ''
                );
            } else {
                if (getWeeklyOff($empCode, $value)) {
                    $recordFindArray[$value] = $value;

                    $result[]                = array(

                        'type' => 'weeklyoff',
                        'title' => 'Weekly Off',
                        'shiftName' => '',
                        'shiftInOutTime' => '',
                        'formattedDate' => date('l, d M Y', strtotime($value)),
                        'presetStatus' => '',
                        'presetShortStatus' => 'W',
                        'start' => $value,
                        'className' => 'green',
                        'birthdate' => array(),
                        'annidate' => array(),
                        'startTime' => '',
                        'endTime' => '',
                        'allDay' => true,
                        'status' => '',
                        'reason' => '',
                        'timeSpan' => ''
                    );
                }
            }
        }
    }

    

    $missingDates = getMissingDates($recordFindArray, $startDate, $endDate);
    
    if (count($missingDates) > 0) {
        foreach ($missingDates as $key => $value) {
            $unix_MissingDate = strtotime($value);
            $unix_todayDate   = strtotime(date('Y-m-d'));

            $result[]         = getNullStatus($value);
        }
    }
    

    if (isset($args) && $args['responseType'] == 'array') {
        return json_encode($result);
    }
    
    echo json_encode($result);
}

if ($_GET['type'] == 'payrollCalenndarHTML') {
    $emp_code = $_GET['emp_code'];
    if (empty($_POST['monthGet'])) {
        
        $orgMastValue  = $globalClassObj->getPayCycleOrgMastDt();
        $payCycleQuery = "SELECT TOP 1 HRDT.Emp_Code,HRDT.Comp_Code,PAYC.PY_Start
        FROM HrdTran HRDT,PayCycle PAYC
        WHERE
        HRDT.COMP_CODE=PAYC.PY_COMP AND " . $orgMastValue . "=PY_OrgMastDt AND PAYC.PY_TYPE='F' AND HRDT.Emp_Code = '" . $emp_code . "'";
        
        $resultPayCycle = query($query, $payCycleQuery, $pa, $opt, $ms_db);
        $startDate      = date('Y-m-d');
        
        if ($num($resultPayCycle)) {
            $rowPayCycle = $fetch($resultPayCycle);
            $PY_Start    = $rowPayCycle['PY_Start'];
            $startDate   = date('Y') . '-' . date('m') . '-' . $PY_Start;
            $startDate   = date("Y-m-d", strtotime("-1 month", strtotime($startDate)));
        }
        
        $endDate = date("Y-m-d", strtotime("+1 month", strtotime($startDate)));
    }
    
    
    if (!empty($_POST['monthGet']) && $_POST['monthGet'] == 'next') {
        $startDate = date("Y-m-d", strtotime("+1 month", strtotime($_POST['startDate'])));
        $endDate   = date("Y-m-d", strtotime("+1 month", strtotime($_POST['endDate'])));
    }
    
    if (!empty($_POST['monthGet']) && $_POST['monthGet'] == 'previous') {
        $startDate = date("Y-m-d", strtotime("-1 month", strtotime($_POST['startDate'])));
        $endDate   = date("Y-m-d", strtotime("-1 month", strtotime($_POST['endDate'])));
    }
    
    //get month of start date
    $startDateMonth = date('m', strtotime($startDate));
    $startDateYear  = date('Y', strtotime($startDate));
    
    //get month of end date
    $endDateMonth = date('m', strtotime($endDate));
    $endDateYear  = date('Y', strtotime($endDate));
    
    $previousDates        = "startDate: " . $startDate . " endDate:" . $endDate;
    /*     * ************************ */
    //Check actual grade code and replace previous start/end date
    $actualGradeCode      = "SELECT TRN_DATE,DATEPART(DAY,Trn_Date) Trn_Date,Emp_Code,Grd_Code,PayCycle.PY_Start FROM HrdTran,PayCycle
                WHERE HrdTran.".$orgMastValue."=PayCycle.PY_OrgMastDt AND HrdTran.Comp_Code=PayCycle.PY_Comp
                AND Emp_Code='" . $emp_code . "' /*AND '2016-12-12' > Trn_Date AND Trn_Date < '2017-01-12'*/
                AND Trn_Date < '" . $startDate . "'
                ORDER BY Trn_Date DESC";
    $actualGradeCodeQuery = query($query, $actualGradeCode, $pa, $opt, $ms_db);
    if ($num($actualGradeCodeQuery)) {
        $actualGradeCodeResult = $fetch($actualGradeCodeQuery);
        $PY_Start              = $actualGradeCodeResult['PY_Start'];
        $startDate             = $startDateYear . '-' . $startDateMonth . '-' . $PY_Start;
        //$startDate = date("Y-m-d", strtotime( "-1 month", strtotime( $startDate ) ));
        $endDate               = date("Y-m-d", strtotime("+1 month", strtotime($startDate)));
    }
    
    /*     * ************************ */
    
    $weekDayNumber          = date('N', strtotime($startDate));
    $calendarMonthNameTitle = date('d F\'y', strtotime($startDate)) . ' to ' . date('d F\'y', strtotime("-1 day", strtotime($endDate)));
    $mainTitle              = "My Payroll Calendar of " . date('F - y', strtotime($endDate));
    $days                   = abs(floor(strtotime($startDate) / (60 * 60 * 24)) - floor(strtotime($endDate) / (60 * 60 * 24)));
    
    $html .= "<tr>";
    for ($i = 1; $i <= 7; $i++) {
        $dow_text = date('D', strtotime("Sunday +{$weekDayNumber} days"));
        $html .= "<th class='myheader fc-day-header fc-widget-header fc-" . strtolower($dow_text) . "'>" . $dow_text . "</th>";
        $weekDayNumber = $weekDayNumber + 1;
    }
    $html .= "</tr>";
    $daysRowCount = $daysNumber = 0;
    for ($daysRow = 1; $daysRow <= $days; $daysRow++) {
        $daysRowCount++;
        if ($daysRowCount == 1) {
            $html .= "<tr>";
        }
        
        $numberRow = $daysNumber++;
        $date      = date("d", strtotime($startDate . ' + ' . $numberRow . 'day'));
        $rowId     = date("Y-m-d", strtotime($startDate . ' + ' . $numberRow . 'day'));
        
        $html .= "<td id='cal_" . $rowId . "' class='fc-event-container' style='height:108px'> <div class='datelabel'>" . $date . "</div></td>";
        
        if ($daysRowCount == 7) {
            $html .= "</tr>";
            $daysRowCount = 0;
        }
    }
    
    echo json_encode(array(
        "empCode" => $emp_code,
        "PreviousDates" => $previousDates,
        "NewStartDate" => $startDate,
        "NewEndDate" => $endDate,
        "htmlTable" => $html,
        "startDate" => $startDate,
        "endDate" => $endDate,
        "calendarMonthNameTitle" => $calendarMonthNameTitle,
        "mainTitle" => $mainTitle
    ));
}
?>