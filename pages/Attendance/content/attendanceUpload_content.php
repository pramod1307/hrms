<div class="page-content-wrapper">
			<div class="page-content cus-light-grey">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
        <div class="modal fade bs-modal-lg" id="large11" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog lg">
						<div class="modal-content" style="width:850px;">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title"><div class="caption"><b></b></div></h4>
							</div>
							<div class="modal-body">                
             
                </div>
							
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-globe"></i>Employee Attendance upload
								</div>
								
							</div>
							<div class="portlet-body">
								<div class="table-toolbar">
									<div class="row">
										
					                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
					                        <div class="form-group">
						                        <div class="col-md-8">
						                      		<div id="createerr" style="color:red;text-align: center;"></div>
						                        	<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">Upload Attendance  (.csv) file <span class="required">*</span>
						                       		</label>
						                        	<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
						                        		<input type="file" class="form-control has-feedback-left" name="file1" id="file1">
						                        	</div>
						                      	</div>
						                       <div class="col-md-3">
						                            <a href="attendance.csv" class="btn btn-block green">
						                                Download blank Template <i class="fa fa-download"></i>
						                            </a>
						                        </div>
						                        <div class="col-md-4">
						                            Download blank template for Upload Attendance.
						                        </div>
					                        </div>
					                    
					                      	<div class="ln_solid"></div>
					                      	<div class="form-group">
						                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
													<button type="button" onclick="upload_csv_file('file1');" class="btn btn-success">Upload</button>
								                </div>
					                    	</div>
					                    </form>
									
									</div>
								</div>
								<table class="table table-striped table-bordered table-hover" id="error_div" style="display: none;">
									<thead>
									<tr>
										<th class='odd gradeX' width="200px">Row NO</th>
										<th class='odd gradeX' width="1000px">Suggestions</th>
									</tr>
									<tbody id="searchMyData">
									</tbody>
								</table>
							
								<!-- <table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
								<tr class='odd gradeX'>
									<th>
									Employee Code <span style='color:red;'>*</span>
									</th>
									<th>
									Date of Leaving <span style='color:red;'>*</span>
									</th>
									<th>
									Date of Settlement <span style='color:red;'>*</span>
									</th>
									<th>
									Date of Resignation <span style='color:red;'>*</span>
									</th> 
									<th>
									Leaving Reason <span style='color:red;'>*</span>
									</th>
									<th>
									Employee status <span style='color:red;'>*</span>
									</th> 
								</tr>
								</thead>
								<tbody id="refresh_leave_data">
				
								</tbody>
								</table> -->
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->
			</div>
		</div>