<div class="tab-pane" id="tab_4">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Shift Allowance Calculation
            </div>

        </div>
        <div class="portlet-body">
            <!-- BEGIN FORM-->
        <form action="#" id="shiftAllowance" method="post"  class="form-horizontal" enctype="multipart/form-data">
        <div class="row">
            <div class="form-body">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 control-label">Year</label>
                            <div class="col-md-3" id="yeardiv" style="padding-top: 9px;">
                                <select id="year" style="width: 100px" name="year">
                                <option value="">Year</option>
                                    <?php 
                                        $y = 2010;
                                        for($i=1;$i<30;$i++){
                                    ?>        
                                        <option value="<?php echo $y+$i; ?>">
                                                    <?php echo $y+$i; ?>
                                        </option>
                                              
                                            <?php }?>
                                </select>
                            </div>
                        <label class="col-md-2 control-label">Month</label>
                            <div class="col-md-3" id="monthdiv" style="padding-top: 9px;">
                                <select id="month" style="width: 100px" name="month">
                                <option value="">Month</option>
                                    <?php 
                                        $sql1="select * from LOVMAst where LOV_Field='Month' order by CAST(LOV_Value AS int)"; 
                                        $result1 = query($query,$sql1,$pa,$opt,$ms_db);
                                            while($row1 = $fetch($result1)){ ?> 
                                                <option value="<?php echo $row1['LOV_Value'] ?>">
                                                    <?php echo $row1['LOV_Text']; ?>
                                                </option>
                                            <?php }?>
                                </select>
                            </div>
                    </div>
                </div>

               <div class="form-group">
                    <div class="row">
                        <label class="col-md-2 control-label">Category</label>
                            <div class="col-md-3" id="grddiv">
                                <select id="grd" name="grade" style="width: 150px;height: 32px;" onchange="getemp();">
                                    <option value=''>Grade</option>
                                    <?php 
                                        $sql3="select GRDID,GRD_NAME from GrdMast"; 
                                        $result3 = query($query,$sql3,$pa,$opt,$ms_db);
                                            while($row3 = $fetch($result3)){ ?> 
                                            <option value="<?php echo $row3['GRDID'] ?>">
                                                    <?php echo $row3['GRD_NAME']; 
                                        ?>
                                            </option>
                                                <?php }?>
                                </select>
                            </div>
                            <label class="col-md-2 control-label">Empolyee</label>
                            <div class="col-md-3" id="empdiv">
                                <select id="emp" name="multiselect[]" multiple="multiple" style="width: 150px">

    

                                   
                                </select>
                            </div>
                            <!-- <label class="col-md-2 control-label">Pay Cycle</label>
                            <div class="col-md-3" id="paydiv">
                                <select id="paycycle" name="paycycle" style="width: 150px;height: 32px;" onchange="getemp()">
                                    <option value=''>Select PayCycle</option>
                                   
                                </select>
                            </div> -->
                          

                        <!-- <label class="col-md-2 control-label">Employee</label>
                            <div class="col-md-3" id="empdiv">
                                <select id="emp" name="multiselect[]" multiple="multiple" style="width: 300px">
                                    <?php 
                                        $sql2="select Emp_Code,Emp_FName,Emp_LName from HrdMast"; 
                                        $result2 = query($query,$sql2,$pa,$opt,$ms_db);
                                            while($row2 = $fetch($result2)){ ?> 
                                            <option value="<?php echo $row2['Emp_Code'] ?>">
                                                    <?php echo $row2['Emp_FName']; 
                                                    echo " "; echo $row2['Emp_LName']; 
                                                    echo "- ("; echo $row2['Emp_Code']; echo ")"; ?>
                                            </option>
                                                <?php }?>
                                </select>
                            </div> -->
                       
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn green pull-right" id="suballow" onclick="getData();"><i class="fa fa-check"></i>Calculate
                    </button>
                </div>
                <div style="display:none" id="tablediv" style="margin-left: 30px;">
                   <div class="row pull-right" style=" margin-bottom: 10px;margin-right: 14px;margin-top: 10px">
                     <button id="btnExport" onclick="fnExcelReport('sample_1');"> Export in excel </button>
                     <!-- <input type="button" id="exportpdf" onclick="pdfexport();" value="Download PDF">
                     -->
                    </div>
                     <iframe id="txtArea1" style="display:none"></iframe>
                    <div  class="col-md-12" id='showtable' style="width: 98%;overflow-x: auto;"></div>
                </div>

                  <!-- <div class="form-group">
                <div class="row">
                  <label class="col-md-2 control-label">Empolyee</label>
                            <div class="col-md-3" id="empdiv">
                                <select id="emp" name="multiselect[]" multiple="multiple" style="width: 150px">
                                    <option value=''>Select Employee</option>
                                   
                                </select>
                            </div>
                </div>
                </div> -->

                
                                  
    
    </div>
    
</form>




            <!-- END FORM-->
        </div>


    </div>
</div>

