<?php
session_start();
include ('../db_conn.php');
include ('../configdata.php');
include ('../main_class.php');
$main_class_obj=new main_class();
if ((!isset($_SESSION['usercode']) || $_SESSION['usercode'] == "") && (!isset($_SESSION['usertype']) || $_SESSION['usertype'] == "")) {
    header('location: ../login/index.php');
}
$c_val = 'end1234';

$male = 'images.png';
$female = 'images.jpg.jpg';

if (isset($_GET['emp_code']) && !empty($_GET['emp_code'])) {
    $emp_code = $_GET['emp_code'];
} else {
    $emp_code = $_SESSION['usercode'];
}

//get employee payroll date here
$emp_cate = strtolower($main_class_obj->getemployee_category_main($emp_code));


//end


$sqlq11 = "SELECT * FROM HrdMastQry WHERE Emp_Code = '" . $emp_code . "' and Status_Code='01'";
$resultq11 = query($query, $sqlq11, $pa, $opt, $ms_db);
if ($num($resultq11)) {

    while ($rowq11 = $fetch($resultq11)) {


        if ($rowq11['EmpImage'] != "") {
            $empimage = $rowq11['EmpImage'];
        } else if ($rowq11['EmpImage'] == "" && $rowq11['Gender'] == 'Male') {
            $empimage = htmlentities(stripslashes($male));
        } elseif ($rowq11['EmpImage'] == "" && $rowq11['Gender'] == 'Female') {
            $empimage = htmlentities(stripslashes($female));
        }
        $empcode = $emp_code;
        $empname = stringTextFormat($rowq11['EMP_NAME']);
        $empdsg = stringTextFormat($rowq11['DSG_NAME']);
        $empdept = stringTextFormat($rowq11['DEPT_NAME']);
    }
}
?>



<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]--> <!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>HRMS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
        <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/select2/select2.css"/>
        <link href="../../assets/global/plugins/fullcalendar/fullcalendar.css" rel="stylesheet"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="../../assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/admin/layout2/css/kunal.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/admin/layout2/css/custom-calendar.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>

        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
        <script type="text/javascript">
            var HTTP_HOST = '<?php echo $_SERVER['HTTP_HOST']; ?>';
        </script>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white custom-layout">
        <!-- BEGIN HEADER -->
<?php include('../include/header.php'); ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <div class="page-content-wrapper cus-dark-grey">
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper modified">
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse cus-dark-grey">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<?php include('../include/leftMenu.php') ?>
                        <!-- END SIDEBAR MENU -->
                    </div>
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content cus-light-grey">
                        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Modal title</h4>
                                    </div>
                                    <div class="modal-body">
                                        Widget settings form goes here
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn blue">Save changes</button>
                                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                        <!-- BEGIN STYLE CUSTOMIZER -->
                        <!-- END STYLE CUSTOMIZER -->
                        <!-- BEGIN PAGE CONTENT-->
                        <div class="row">
                            <div class="col-md-12">

                                <div class="leave-status">
                                    <!-- BEGIN ALERTS PORTLET-->
                                    <div class="portlet light z-depth-1 clearfix">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                
                                                <div class="btn-group">
                                                    <button type="button" class="dropdown-toggle brn white-bg" data-toggle="dropdown">
                                                        <span class="hidden-sm hidden-xs caption-subject  bold"></span>&nbsp;
                                                    </button>

                                                </div>

                                            </div>

<?php /* ?><div class="tabbable-line">
  <ul class="nav nav-tabs fl-r">
  <?php if(isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] =='grpcal' || $_GET['type'] =='grpsum'){
  }else{ ?>
  <li class="green-active <?php if(!isset($_GET['type'])){echo 'active'; }?>">
  <a href="#tab_15_2" data-toggle="tab" <?php if(!isset($_GET['type'])){echo 'aria-expanded="true"'; }?>>
  Personal Calendars </a>
  </li>
  <?php } ?>
  <?php if(isset($_GET['emp_code']) && !empty($_GET['emp_code'])){

  }else{?>
  <?php if(isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] =='grpsum'){
  }else{ ?>
  <li <?php if($_GET['type'] =='grpcal'){echo 'class="green-active active"'; }?>>
  <a href="#tab_15_3" data-toggle="tab" <?php if($_GET['type'] =='grpcal'){echo 'aria-expanded="true"'; }?>>
  Group Calendars </a>
  </li>
  <?php } ?>
  <?php if(isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] =='grpcal'){
  }else{ ?>
  <li <?php if($_GET['type'] =='grpsum'){echo 'class="green-active active"'; }?>>
  <a href="#tab_15_4" data-toggle="tab" onclick="setCal" <?php if($_GET['type'] =='grpsum'){echo 'aria-expanded="true"'; }?>>
  Group Summary </a>
  </li>
  <?php } ?>
  <?php } ?>
  </ul>
  </div><?php */ ?>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="tab-content">
                                            <?php if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'grpcal' || $_GET['type'] == 'grpsum') {
                                                
                                            } else {
                                                ?>
                                                    <div class="tab-pane active" id="tab_15_2">
                                                        <div class="col-md-2">

                                                            <div class="text-left" style="margin-top: 42px;">
                                                                <img class="user-pic img-circle" style="width:60%;" src="../../pages/Profile/upload_images/<?php echo $empimage; ?>">
                                                            </div>
                                                            <div class="text-left" style="margin-top:10px;">
    <?php echo "<b>" . $empname . "</b> <span style='font-size:.9em;color:#999'>(" . $empcode . ")</span><br>"; ?>

                                                                <?php if (!empty($empdsg)) {
                                                                    echo $empdsg . "<br>";
                                                                } ?>
                                                                <?php if (!empty($empdept)) {
                                                                    echo $empdept . "<br>";
                                                                } ?>

                                                            </div>
                                                            <?php
//Get All team members relevent to current user

                                                            $where = ($emp_code == 'admin' || Is_hr($emp_code,$query, $pa, $opt, $ms_db)) ? "WHERE Status_Code = '01'" : "WHERE MNGR_CODE = '" . $emp_code . "' AND Status_Code = '01'";
                                                                $teamMembersQuery = "SELECT Emp_Code, EMP_NAME, MNGR_CODE FROM HrdMastQry " . $where;

                                                            $teamMembersQueryResult = query($query, $teamMembersQuery, $pa, $opt, $ms_db);
                                                            if ($num($teamMembersQueryResult)) {
                                                                ?>
                                                                <label>Employee Calendar:</label>
                                                                <select name="teamMembers" class="jq_payroll_employees form-control select2me">
                                                                    <option value="<?php echo $empcode; ?>"><?php echo $empname . ' (' . $emp_code . ')'; ?></option>                                     <?php
                                                                    while ($teamMembers = $fetch($teamMembersQueryResult)) {
                                                                        ?>
                                                                        <option value="<?php echo $teamMembers['Emp_Code']; ?>"><?php echo stringTextFormat($teamMembers['EMP_NAME']) . ' (' . $teamMembers['Emp_Code'] . ')'; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                                </select>
    <?php } ?>

                                                        </div>
                                                        <div class="col-md-10">
                                                            <div class="loadingCaps alert alert-danger">Loading data please wait...</div>
                                                            <h2 style="width: 50%; float: left;" id="calendarMonthNameTitle"></h2>
                                                            <!-- <div class="" style="margin-top: 14px;">
                                                                <a class="btn btn-primary" href="javascript:void(0)" onclick="payrollCalendar.previous();" title="Previous">Previous</a>
                                                                <a class="btn btn-primary" href="javascript:void(0)" onclick="payrollCalendar.next();" title="Next">Next</a>
                                                            </div> -->

                                                            <div class="fc-button-group pull-right" style="margin-top: 25px;">
                                                            <button type="button" onclick="payrollCalendar.previous();" class="fc-prev-button fc-button fc-state-default fc-corner-left"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
                                                            <button type="button" onclick="payrollCalendar.next();" class="fc-next-button fc-button fc-state-default"><span class="fc-icon fc-icon-right-single-arrow"></span>
                                                            </button>
                                                            </div>
                                                            <?php if($emp_cate != '' || $emp_cate  !== null){ ?>
                                                            <input type="hidden" id="emp_code" value="<?php echo $emp_code; ?>" >
                                                            <!-- <div id="calendar" class="has-toolbar"></div> -->
                                                            <table class='table' id='payCalHtml'></table>
                                                            <ul class="legendlist">
                                                                <li><span class="preset-legent"></span> Present</li>
                                                                <li><span class="absent-legend"></span> Absent</li>
                                                                <li><span class="leave-legend"></span> Leave</li>
                                                                <li><span class="od-legend"></span> OD</li>
                                                                <li><span class="halfday-legend"></span> Half Day</li>
                                                                <li><span class="holiday-legend"></span> Holiday</li>
                                                                <li><span class="miss-punch"></span> Miss Punch</li>
                                                            </ul>
                                                            <div id="eventContent" title="Event Details" style="display:none;">
                                                                <span id="shiftName"></span>
                                                                <span id="shiftInOutTime"></span><br><br>
                                                                <span id="presetStatus"></span>
                                                                <span id="firstpresetStatus"></span>
                                                                <span id="secondpresetStatus"></span>
                                                                <span id="leaveReason"></span>
                                                                <br>
                                                                <span id="startTime"></span><span id="endTime"></span>
                                                                <span id="birthdayRecords"></span>
                                                                <span id="anniversaryRecords"></span>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                <?php } ?>

                                                <?php if (isset($_GET['emp_code']) && !empty($_GET['emp_code'])) {
                                                    
                                                } else {
                                                    ?>
    <?php if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'grpsum') {
        
    } else {
        ?>
                                                        <div class="tab-pane <?php if ($_GET['type'] == 'grpcal') {
            echo 'active';
        } ?>" id="tab_15_3">
                                                            <!-- start here  -->

                                                            <div class="col-md-12">
                                                                <div id="group_calendar">
                                                                </div>
                                                            </div>

                                                            <!-- end here -->
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'grpcal') {
                                                        
                                                    } else {
                                                        ?>
                                                        <div class="tab-pane <?php if ($_GET['type'] == 'grpsum') {
                                                            echo 'active';
                                                        } ?>" id="tab_15_4">
                                                            <div class="col-md-12">
                                                                <div id="group_summary">
                                                                </div>
                                                            </div>
                                                        </div>
    <?php } ?>
<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END ALERTS PORTLET-->
                                </div>
                                <!-- END PAGE CONTENT-->
                            </div>

                        </div>
                    </div>

                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <!--Cooming Soon...-->
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2014 &copy; Metronic by keenthemes.
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="../../assets/global/plugins/respond.min.js"></script>
        <script src="../../assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>

        <script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../assets/global/scripts/materialize.min.js" type="text/javascript"></script>
        <script src="../../assets/global/scripts/init.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="../../assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- <script src="../../assets/global/plugins/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="../../assets/global/plugins/flot/jquery.flot.categories.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="../../assets/global/plugins/moment.js"></script>
        <script src="../../assets/global/plugins/fullcalendar/fullcalendar.js"></script>
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="../../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
        <script src="../../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
        <script src="../../assets/admin/pages/scripts/calendar.js"></script>
        <!-- <script src="../../assets/admin/pages/scripts/ecommerce-index.js"></script> -->
        <script type="text/javascript" src="../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/bootstrap-daterangepicker/moment.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="../../assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
        <script src="../../assets/admin/pages/scripts/components-pickers.js"></script>
        <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script  type="text/javascript" src="../js/common.js"></script>
        <script  type="text/javascript" src="js/group_calendar.js"></script>
        <script  type="text/javascript" src="js/group_summary.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script  type="text/javascript">
            $(document).ready(function () {

                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
               // Demo.init(); // init demo features
                // EcommerceIndex.init();
                Calendar.init();
                ComponentsPickers.init();
                //TableManaged.init();

                payrollCalendar.makeCalendarHTML("<?php echo $emp_code; ?>");
                

                $(".jq_payroll_employees").change(function(){
                    payrollCalendar.startDate = payrollCalendar.endDate  = "";
                    payrollCalendar.previousMonthFlag = payrollCalendar.nextMonthFlag  = false;          
                    payrollCalendar.makeCalendarHTML($(this).val());
                });

                $(document).on('click', '.payrollPopup', function() {
                       payrollCalendar.payrollPopup($(this).attr('data-id'));
                });
            });

            var payrollCalendar={
                dataItems: [],
                jsonItems: {},
                item: {},
                startDate: "",
                endDate: "",
                previousMonthFlag: false,
                nextMonthFlag: false,
		        empCode: 0,

                makeCalendarHTML: function(userId){

                    var monthFlag = '';
                    if(payrollCalendar.nextMonthFlag == true){ monthFlag = 'next'; }
                    if(payrollCalendar.previousMonthFlag == true){ monthFlag = 'previous'; }

                    $.ajax({
                        url: "../../pages/Attendance/employee-record-v1.php?type=payrollCalenndarHTML&emp_code="+userId,
                        //url: "../../pages/Attendance/employee-record.php?type=payrollCalenndarHTML&emp_code="+userId,
			//url: "../../pages/Attendance/calling_page.php?type=payrollCalenndarHTML&emp_code="+userId,
                        type: 'POST',
                        data: {monthGet: monthFlag, startDate: payrollCalendar.startDate, endDate: payrollCalendar.endDate},
                        success: function (result) {
                            $("#payCalHtml").html('');
                            var parseResult = $.parseJSON(result);
                            $("#calendarMonthNameTitle").text(parseResult.calendarMonthNameTitle);
                            $(".caption-subject").text(parseResult.mainTitle);
                            $("#payCalHtml").html(parseResult.htmlTable);
                            payrollCalendar.startDate = parseResult.startDate;
                            payrollCalendar.endDate = parseResult.endDate;
		            payrollCalendar.empCode = parseResult.empCode;
                            payrollCalendar.calendarAjax(userId);
                        }
                    });
                },

                next: function(){
                    payrollCalendar.nextMonthFlag = true;
                    payrollCalendar.previousMonthFlag = false;
                    payrollCalendar.makeCalendarHTML(payrollCalendar.empCode);
                },

                previous: function(){
                    payrollCalendar.nextMonthFlag = false;
                    payrollCalendar.previousMonthFlag = true;
                    payrollCalendar.makeCalendarHTML(payrollCalendar.empCode);
                },

                payrollPopup: function(userKey){ 
            
                    if(payrollCalendar.item.hasOwnProperty(userKey)){
                        $('#birthdayRecords').html('');
                        $('#anniversaryRecords').html('');
                        $("#shiftName").html('');
                        $("#shiftInOutTime").html('');
                        $("#presetStatus").html('');
                        $("#firstpresetStatus").html('');
                        $("#secondpresetStatus").html('');
                        $("#startTime").html('');
                        $("#endTime").html('');
                        $("#leaveReason").html('');
                        var lateComingMessage = '';
                       
                       if(typeof payrollCalendar.item[userKey]['attendanceMessage'] !== 'undefined' && payrollCalendar.item[userKey]['attendanceMessage'] != ''){
                        lateComingMessage +='<span class="att-time"><font style="color: #cc3300;">'+payrollCalendar.item[userKey]['attendanceMessage']+'</span>';
                        }
                        
                        $("#shiftName").html(payrollCalendar.item[userKey]['shiftName']);
                        $("#shiftInOutTime").html(payrollCalendar.item[userKey]['shiftInOutTime']); 
                        
                        if(payrollCalendar.item[userKey]["POWPOHTime"] != ''){
                            $("#startTime").html( payrollCalendar.item[userKey]["POWPOHTime"] );
                        }
                        var tempHTML = '';
                        if(payrollCalendar.item[userKey]['title'] !== 'POW' && payrollCalendar.item[userKey]['title'] !== 'POH') {
                            if($.trim(payrollCalendar.item[userKey]['startTime']) != '') {
                                tempHTML = " ("+ $.trim(payrollCalendar.item[userKey]['startTime'])+" - " + ")";
                            }
                            if($.trim(payrollCalendar.item[userKey]['endTime']) != '') {
                                tempHTML = " ("+ " - " + $.trim(payrollCalendar.item[userKey]['endTime']) + ")";
                            }
                            if($.trim(payrollCalendar.item[userKey]['startTime']) != '' && $.trim(payrollCalendar.item[userKey]['endTime']) != '' ) {
                                tempHTML = " ("+ $.trim(payrollCalendar.item[userKey]['startTime'])+" - "+$.trim(payrollCalendar.item[userKey]['endTime']) + ")";
                            }           
                            tempHTML += (lateComingMessage != '') ? "<br>"+lateComingMessage : '';
                            $("#presetStatus").html( payrollCalendar.item[userKey]['title'] + tempHTML );
                        }
                        
                        $("#firstpresetStatus").html(payrollCalendar.item[userKey]['firstpresetStatus']);
                        $("#eventContent").dialog({ modal: true, title: payrollCalendar.item[userKey]['formattedDate'], width:"230"});
                    }
                },

                calendarAjax: function(userId) {
                    $(".fc-event").remove();
                    var object = this;
                    $.ajax({
                        //url: "../../pages/Attendance/calling_page.php?type=myNewEvents&emp_code="+userId,
                        url: "../../pages/Attendance/employee-record-v1.php?auth=admin&fromType=payrollCalendar&emp_code="+userId,
                        type: 'POST',
                        data: {start: payrollCalendar.startDate, end: payrollCalendar.endDate, 'requestFrom': 'payrollCalendar'},
                        success: function (result) {
                            eventData = $.parseJSON(result);
                            object.setDataInCalendar(eventData);
                        }
                    });
                    /*alert(payrollCalendar.startDate+" and "+payrollCalendar.endDate);
                    $.ajax({
                        url: "../../pages/Attendance/employee-record.php?auth=admin&fromType=calendar&emp_code="+userId,
                        type: 'POST',
                        data: {start: payrollCalendar.startDate, end: payrollCalendar.endDate, 'requestFrom': 'payrollCalendar' },
                        success: function (result) {
                            eventData = $.parseJSON(result);
                            object.setDataInCalendar(eventData);
                        }
                    });*/
                },

                setDataInCalendar: function(eventData){
           
                    var tempData = {};
                    $.each(eventData, function (k, event) {
                        startDate = event.start;
                        startDate = startDate.replace(/\D/g,'');

                        payrollCalendar.item[startDate]=  event;
                        titleHtml = '';

                        /*if ((event.startTime == '' && event.endTime != '') || (event.startTime != '' && event.endTime == '') )

                        {
                            titleHtml +=
                                    '<span class="fc-title">' +
                                    (htmlEscape('Miss Punch' || '') || '&nbsp;') + // we always want one line of height
                                    '</span>';

                        } else {*/

                            titleHtml +=
                                    '<span class="fc-title">' +
                                    (htmlEscape(event.title || '') || '&nbsp;') + // we always want one line of height
                                    '</span>';

                       // }

if(event.title != 'POW' && event.title != 'POH')
        {
if(typeof event.isLateComing !== undefined && event.isLateComing == 1 && typeof event.isEarlyGoing !== undefined && event.isEarlyGoing == 1){
    titleHtml += '<span class="att-time"><font style="color: #cc3300;">'+((event.startTime !== '') ? event.startTime : '')+'-'+((event.endTime !== '') ? event.endTime : '')+'</font></span>';
}else if(typeof event.isLateComing !== undefined && event.isLateComing == 1 && typeof event.isEarlyGoing !== undefined && event.isEarlyGoing == 0){
    titleHtml += '<span class="att-time"><font style="color: #cc3300;">'+((event.startTime !== '') ? event.startTime : '')+'</font>-'+((event.endTime !== '') ? event.endTime : '')+'</span>';
}else if(typeof event.isLateComing !== undefined && event.isLateComing == 0 && typeof event.isEarlyGoing !== undefined && event.isEarlyGoing == 1){
    titleHtml += '<span class="att-time">'+((event.startTime !== '') ? event.startTime : '')+'-<font style="color: #cc3300;">'+((event.endTime !== '') ? event.endTime : '')+'</font></span>';
}else{
    titleHtml +='<span class="att-time">'+((event.startTime !== '') ? event.startTime : '')+'-'+((event.endTime !== '') ? event.endTime : '')+'</span>';
}
}

if(event.type=='null'){
        titleHtml +='<span class="att-time"></span>';
}
titleHtml = titleHtml.replace('<span class="att-time">-</span>', "");
                        /*if (event.type == 'attendance') {
if(typeof event.isLateComing !== undefined && event.isLateComing == 1 && typeof event.isEarlyGoing !== undefined && event.isEarlyGoing == 1){
    titleHtml += '<span class="att-time"><font style="color: #cc3300;">'+event.startTime+'-'+event.endTime+'</font></span>';
}else if(typeof event.isLateComing !== undefined && event.isLateComing == 1 && typeof event.isEarlyGoing !== undefined && event.isEarlyGoing == 0){
    titleHtml += '<span class="att-time"><font style="color: #cc3300;">'+event.startTime+'</font>-'+event.endTime+'</span>';
}else if(typeof event.isLateComing !== undefined && event.isLateComing == 0 && typeof event.isEarlyGoing !== undefined && event.isEarlyGoing == 1){
    titleHtml += '<span class="att-time">'+event.startTime+'-<font style="color: #cc3300;">'+event.endTime+'</font></span>';
}else if(event.title == 'POW' || event.title == 'POH'){
    titleHtml +='<span class="att-time">'+event.startTime+'-'+event.endTime+'</span>';
}else{
    titleHtml +='<span class="att-time">'+event.startTime+'-'+event.endTime+'</span>';
}
                        } else if (event.type == 'null') {
                            titleHtml += '<span class="att-time"></span>';
                        } else if (event.type == 'leave') {
                            var stt = '';
                            if (event.status == 'Pending')
                                stt = 'Leave-Pending';
                            else if (event.status == 'Approved')
                                stt = '';
                            else
                                stt = event.status;
                            titleHtml += '<span class="att-time">' + stt + '</span>';
                        } else if (event.type == 'attRegularised') {
                            var stt = '';
                            if (event.status == 'Pending')
                                stt = 'Attn Regularised-Pending';
                            else
                                stt = event.status;
                            titleHtml += '<span class="att-time">' + stt + '</span>';
                        } else if (event.type == 'odrequest') {
                            var stt = '';
                            if (event.status == 'Pending')
                                stt = 'OD Request-Pending';
                            else
                                stt = event.status;
                            titleHtml += '<span class="att-time">' + stt + '</span>';
                        } else if (event.type == 'empty_attendance') {
                            titleHtml += '<span class="att-time"></span>';
                        }*/
						/*if(event.birthdate != undefined){
							if (event.birthdate.length > 0 || event.annidate.length > 0) {
								titleHtml += '<span class="cal-event"><i aria-hidden="true" style="color:pink" class="fa fa-birthday-cake"></i></span>';
							}
						}
						*/
                        skinCss = '';
                        result = '<a onclick="payrollCalendar.payrollPopup(\''+startDate+'\')" class="fc-day-grid-event fc-event fc-start fc-end payrollPopup '+event.className+'"' +
                                    (event.url ?
                                        ' href="' + htmlEscape(event.url) + '"' :
                                        ''
                                        ) +
                                    (skinCss ?
                                        ' style="' + skinCss + '"' :
                                        ''
                                        ) +
                                '>' +
                                    '<div class="fc-content">' +
                                        (titleHtml) +
                                    '</div>'
                                '</a>';

                        $("#cal_" + event.start).append(result);
                        //$("#cal_"+event.startTime).append("stuff you want to append"); 
                    });
                    //this.item = tempData;
                    //console.log(payrollCalendar.item);
                    $('.loadingCaps').hide();
                }


            };
             
            var entityMap = {
              "&": "&amp;",
              "<": "&lt;",
              ">": "&gt;",
              '"': '&quot;',
              "'": '&#39;',
              "/": '&#x2F;'
            };

            function htmlEscape(string) {
              return String(string).replace(/[&<>"'\/]/g, function (s) {
                return entityMap[s];
              });
            }
            function wordInString(s, word){
              return new RegExp( '\\b' + word + '\\b', 'i').test(s);
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>