<?php
session_start();
include_once('../db_conn.php');
include_once('../configdata.php');
include ("class/attRegularize_class.php");

$ATTRegularize_class=new ATTRegularize_class();
    $getForm=$ATTRegularize_class->getForm();   
    //print_r($getForm);
    $Field_Desc=$getForm[0];
    $Field_Len=$getForm[1];
    $DefaultValue=$getForm[2];
    $Nullable=$getForm[3];
    $MinLength=$getForm[4];
    $num=$getForm[5];   

if((!isset($_SESSION['usercode']) || $_SESSION['usercode']=="")&& (!isset($_SESSION['usertype']) || $_SESSION['usertype']=="")){
    header('location: ../login/index.php');
}
$code=$_SESSION['usercode'];
$sql="select * from hrdmastqry WHERE Emp_Code='$code'";
$res=query($query,$sql,$pa,$opt,$ms_db);
$data=$fetch($res);
$mngrcode=$data['MNGR_CODE'];
$sql1="select DSG_NAME, MailingAddress,EmpImage,EMP_NAME from hrdmastqry WHERE Emp_Code='$mngrcode'";
$res1=query($query,$sql1,$pa,$opt,$ms_db);
$data1=$fetch($res1);
$c_val= 'end1234';
//set employee markpast date limit
$calendarDateLimit = 30;
$calendarDateLimit =  $calendarDateLimit <= 0 ? $calendarDateLimit : -$calendarDateLimit ;
$missPunchMessage = "<small>Note: You can request for miss punch from ".date('d/m/Y', strtotime($calendarDateLimit." days"))." to ".date('d/m/Y')."</small>";
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Sequel- HRMS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="../../assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <link href="../../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/jstree/dist/themes/default/style.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
        <link rel="stylesheet" href="../../assets/admin/layout2/css/bootstrap-multiselect.css" type="text/css"/>

        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="../../assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/admin/layout2/css/kunal.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link href="../css/toastr.css"  rel="stylesheet" type="text/css"/>
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <link rel="shortcut icon" href="favicon.ico"/>

        <style>
        #loading {
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        position: fixed;
        display: block;
        opacity: 0.8;
        background-color: #1a1a1a;
        z-index: 99;
        text-align: center;
        }
        #loading-image {
        position: absolute;
        top: 30%;
        left: 35%;
        z-index: 100;
        }
        </style>
         <style type="text/css">
        .multiselect-container{
            max-height: 200px;
    overflow: auto;
padding-left:27px;
        }
    </style>
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white custom-layout">
        <div id="loading">
            <img id="loading-image" src="../Profile/images/ajax-loader1.gif" alt="Loading..." />
        </div>
        <!-- BEGIN HEADER -->
        <?php  include('../include/header.php'); ?>
        <div class="clearfix">
        </div>
        <div class="page-content-wrapper cus-dark-grey">
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper modified">
                    <div class="page-sidebar navbar-collapse collapse cus-dark-grey">
                        <?php include('../include/leftMenu.php') ;?>
                    </div>
                </div>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
                    <div class="page-content cus-light-grey">
                        <!-- BEGIN PAGE CONTENT-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_0">
                                            <div class="portlet box blue">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        Attendance Regularize
                                                        <!-- <i class="fa fa-gift"></i> -->
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="#" class="form-horizontal" id="arForm">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <?php $currDate = date("d/m/Y");?>
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-2 control-label">Employee</label>
                                                                        <div class="col-md-4">
                                                                            <select class="select2me" id="emp" name="emp"
                                                                                style="width: 292px;height: 33px;" onchange="getTime()">
                                                                                <option value="">Employee</option>
                                                                                <?php
                                                                                $sql="Select Emp_Code,EMP_NAME from hrdmastqry order by EMP_NAME";
                                                                                $result = query($query,$sql,$pa,$opt,$ms_db);
                                                                                while($row = $fetch($result)) { ?>
                                                                                <option value="<?php echo $row['Emp_Code']?> "> <?php echo $row['EMP_NAME']; echo " ("; echo $row['Emp_Code']; echo ")";?></option>";
                                                                                <?php }
                                                                                ?>
                                                                                
                                                                            </select>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-2 control-label">Date</label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" class="form-control input-medium pull-left" name="fromDate" id="fromDate" placeholder="dd/mm/yy" onchange="getTime();showdiv()">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id='timediv' style="display: none">
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-6">
                                                                            <label class="col-md-3 control-label">Actual In Time</label>
                                                                            <input class="col-md-3" type='textbox' id="acutalin" disabled style="border:none;">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label class="col-md-3">Update In HH</label>
                                                                            <select class="col-md-3" id="inHour">
                                                                                <?php
                                                                                for($i=0;$i<24;$i++){
                                                                                if(strlen($i)==1){
                                                                                $t1="0".$i;
                                                                                }else{
                                                                                $t1=$i;
                                                                                }
                                                                                echo'<option value="'.$t1.'">'.$t1.'</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <label class="col-md-3">Update In MM</label>
                                                                            <select class="col-md-3" id="inMinute">
                                                                                <?php
                                                                                for($j=0;$j<60;$j++){
                                                                                if(strlen($j)==1){
                                                                                $t2="0".$j;
                                                                                }else{
                                                                                $t2=$j;
                                                                                }
                                                                                echo'<option value="'.$t2.'">'.$t2.'</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-6">
                                                                            <label class="col-md-3 control-label">Actual Out Time</label>
                                                                            <input class="col-md-3" type='textbox' id="acutalout" disabled style="border:none;">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label class="col-md-3">Update Out HH</label>
                                                                            <select class="col-md-3" id="outHour">
                                                                                <?php
                                                                                for($i=0;$i<24;$i++){
                                                                                if(strlen($i)==1){
                                                                                $t1="0".$i;
                                                                                }else{
                                                                                $t1=$i;
                                                                                }
                                                                                echo'<option value="'.$t1.'">'.$t1.'</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <label class="col-md-3">Update Out MM</label>
                                                                            <select class="col-md-3" id="outMinute">
                                                                                <?php
                                                                                for($j=0;$j<60;$j++){
                                                                                if(strlen($j)==1){
                                                                                $t2="0".$j;
                                                                                }else{
                                                                                $t2=$j;
                                                                                }
                                                                                echo'<option value="'.$t2.'">'.$t2.'</option>';
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         <div class="row">
                                                            <div class="col-md-12">
                                                                <button type="button" onclick="updateTime();" class="btn blue pull-right">Update</button>
                                                                
                                                            </div>
                                                        </div>
                                                        </div>
                                                     
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Mark Past Attendance Exception
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                      <form  enctype="multipart/form-data" id="form" name="attendanceRegularizefrom" class="form-horizontal form-row-seperated">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3" >
                                        <label class="control-label" style="padding-left: 33px"><?php echo $Field_Desc[0];?></label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-3 selectKunal" id="locdiv" >
                                        <select id="empname" name="multiselect[]" multiple="multiple" style="width: 300px">
                                        
                                         <?php 
                                        $sqlemp=" select EMP_NAME,Emp_Code from HrdMastQry order by EMP_NAME"; 
                                        $resultemp = query($query,$sqlemp,$pa,$opt,$ms_db);
                                                while($rowemp = $fetch($resultemp)){ ?> 
                                                    <option value="<?php echo $rowemp['Emp_Code'] ?>">
                                                    <?php echo $rowemp['EMP_NAME']; echo" (";
                                                    echo $rowemp['Emp_Code']; echo")";?>
                                                    </option>
                                                <?php }?>
                                        </select>
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label"><?php echo $Field_Desc[1];?></label>
                                    </div>    
                                    <div class="col-md-3">
                                        <input type="text" class="form-control input-small" name="allowday" id="allowday" maxlength='<?php echo $Field_Len[1];?>' onkeypress="return isNumber(event)" >
                                    </div>
                                </div>
                           
                                                            <div class="col-md-12">
                                                                <button type="button" onclick="insertExp();" class="btn blue pull-right">Insert</button>
                                                                
                                                            </div>
                                                        
                            </div>
                           
                            
                                
                             <div class="form-group">
                             <div class="col-md-12">
                             <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead><tr class='odd gradeX'>
                                   <!--  <th>S.No.</th> -->
                                    <th>Employee</th>
                                    <th>Allowed Days</th>
                                    <th>Created On</th>
                                     <th>Status</th>
                                    </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                $sqlq="select Id,status,Emp_Code,days,Convert(varchar(26),createdOn,109) as createdOn from markpastAttendanceExp order by Id desc";
                                $resultq=query($query,$sqlq,$pa,$opt,$ms_db);
                                if($num($resultq)) {

                                    while ($rowq = $fetch($resultq)) {
                                        ?>
                                        <tr>
                                            
                                            <!-- <td><?php echo $i?></td> -->
                                            <td><?php  $codeE= $rowq['Emp_Code'];
                                            $sqlq1="select EMP_NAME from HrdMastQry where Emp_Code='$codeE'";
                                                $resultq1=query($query,$sqlq1,$pa,$opt,$ms_db);
                                                $emprow = $fetch($resultq1);
                                                echo $emprow['EMP_NAME']; echo " ("; echo $rowq['Emp_Code']; echo")";?></td;>
                                            <td><?php echo $rowq['days'];?></td>
                                            <td><?php echo $rowq['createdOn'];?></td>
                                             <td><input type="button" id="stid" class="btn btn-default"
                            value='<?php if($rowq['status']=="1"){ echo"Active"; }else{ echo"Inactive"; } ?>'
                            onclick="changeStatus('<?php echo $rowq['status'] ?>','<?php echo $rowq['Id']; ?>','<?php echo $rowq['Emp_Code']; ?>')" /></td>


                                        </tr>

                                        <?php
                                        $i++;
                                    }
                                }

                                ?>

                                </tbody>
                            </table>
                        </div>
                             </div>   

                        
                        </div>
                        
                                               
                        </form>
                    </div>
                    
                </div>
               
            </div>
        </div>
                    <!-- END PAGE CONTAINER-->
                </div>
                <!-- BEGIN CONTENT -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- Cooming Soon...-->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <?php include('../include/footer.php') ?>
        </div>
        <!-- END FOOTER -->
    </div>
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="../../assets/global/plugins/respond.min.js"></script>
    <script src="../../assets/global/plugins/excanvas.min.js"></script>
    <![endif]-->
    <script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="../../assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="../../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
    <!-- END CORE PLUGINS -->
    <script type="text/javascript" src="../../assets/admin/pages/scripts/components-dropdowns.js"></script>
    <script type="text/javascript" src="../../assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="../../assets/global/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="../../assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="../../assets/admin/layout2/scripts/bootstrap-multiselect.js"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="../../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
    <script src="../../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
     <script src="../../assets/admin/pages/scripts/table-managed.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/moment.js"></script>
    <script src="../js/toastr.js"></script>
    <script src="../js/common.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
    jQuery(document).ready(function() {
    // initiate layout and plugins
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Demo.init(); // init demo features
     TableManaged.init();
    ComponentsDropdowns.init();

    jQuery('#empname').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
enableCaseInsensitiveFiltering:true,

        });

    $('#locdiv').find('.multiselect-selected-text').text('Employee');
    $(function() {
    $( "#fromDate" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: "dd/mm/yy",
    
    });
    
    
    });
    });
     
    function showdiv(){
    $("#timediv").show();
    }
    function getTime(){
    
    var date = $("#fromDate").val();
    var emp = $("#emp").val();
    if(date !='' && emp != ''){
    $.ajax({
    type: "POST",
    url: "ajax/attendanceRegularize.php",
    data: {date:date, emp:emp, type:'gettime'},
    dataType: 'json',
    success: function (result) {
    
    //alert(result[0]);
    if(result != ''){
        var aiTime = result[0];
        var aoTime = result[1];
        var i =aiTime.split(":");
        var o =aoTime.split(":");

    $("#inHour").val(i[0]);
    $("#inMinute").val(i[1]);
    $("#outHour").val(o[0]);
    $("#outMinute").val(o[1]);
   
    $("#acutalin").val(result[0]);
    
    $("#acutalout").val(result[1]);
  
    }
    else{
        $("#acutalin").val("0");
    
    $("#acutalout").val("0");
    }

    
    
    }
    });
    }
    }
    function updateTime(){
    var date = $("#fromDate").val();
    var emp = $("#emp").val();
    var intime = $("#acutalin").val();
    var outtime = $("#acutalout").val();
    var inH = $("#inHour").val();
    var inM = $("#inMinute").val();
    var outH = $("#outHour").val();
    var outM = $("#outMinute").val();
    if(emp == '' || emp == undefined || emp == ' '){
    toasterrormsg("Select Employee");
    return false;
    }
    if(date == '' || date == undefined || date == ' '){
    toasterrormsg("Select Date");
    return false;
    }
    
    if(date !='' && emp != ''){
    $.ajax({
    type: "POST",
    url: "ajax/attendanceRegularize.php",
    data: {date:date, emp:emp, intime:intime, outtime:outtime, inH:inH, inM:inM,
    outH:outH, outM:outM, type:'updatetime'},
    dataType: 'json',
    success: function (result) {
    if(result ==1){
    toastmsg("Updated Successfully");
location.reload();
    }
    else{
    toasterrormsg("Error in updation");
    }
    
    
    }
    });
    }
    }

    function insertExp(){
        var empname = $("#empname").val();
        var allowday = $("#allowday").val();
       // alert(allowday);
        if(empname == '' || empname == undefined || empname == ' '){
    toasterrormsg("Select Employee");
    return false;
    }
    if(allowday == '' || allowday == undefined || allowday == ' '){
    toasterrormsg("Enter No. of Days");
    return false;
    }

        $.ajax({
        type: "POST",
        url: "ajax/attendanceRegularize.php",
        data: {empname:empname, allowday:allowday, type:'insertexp'},
        success: function (result) {
        if(result ==1){
        toastmsg("Updated Successfully");
        location.reload();

        }
        else{
        toasterrormsg("Error in updation");
        }
        
        
        }
        });

    }

    function gettable_val(){
    var fromDate = $("#fromDate").val();
    $.ajax({
    type: "POST",
    url: "ajax/rostlist.php?type=data1",
    data: fromDate,
    cache:false,
    success: function (result) {
    //alert(result);
    //location.reload();
    $("#levellist").val(result);
    levelfunc();
    }
    });
    }
    function levelfunc(){
    var level_list = $("#levellist").val();
    var l = level_list.split(';');
    var emp_code=$("#empcode").val();
    //alert(l[2]);
    $.ajax({
    type: "POST",
    url: "ajax/rostlist.php?type=data2",
    data: {dataval:l[2], emp_code:emp_code},
    cache:false,
    success: function (result) {
    //alert(result);
    //location.reload();
    $("#showlevel1").html(result);
    emp_code=result;
    }
    });
    }
    
    $(window).load(function() {
    $('#loading').hide();
    });

    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function changeStatus(status,id,empCode){

$.ajax({
            type:"POST",
            url:"ajax/attendanceRegularize.php",
            data:{type:'statusUpdate', status:status, id:id, empCode:empCode},
            success: function(result){
                if(result ==1){
                    toastmsg("Successfully Updated");
                    location.reload();
                }
                else if(result == 3){
                    toasterrormsg("Exception already Active for this Employee");
                }
                else{
                     toasterrormsg("Error in updation");
                }
            }
        });

}
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>