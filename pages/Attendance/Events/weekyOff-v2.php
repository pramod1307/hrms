<?php 

function getSundays($y,$m,$dayNo){ 
		    $date = "$y-$m-01";
		    $first_day = date('N',strtotime($date));
		    $first_day = $dayNo - $first_day + 1;
		    $last_day =  date('t',strtotime($date));

		    $days = array();
		    for($i=$first_day; $i<=$last_day; $i=$i+7 ){
		        if($i>0){
		        	$days[] = $i;
		    	}
		    }
		    return  $days;
		}

function getWeeklyOff($empCode, $startDate, $endDate)
{
    global $query;
    global $pa;
    global $opt;
    global $ms_db;
    global $num;
    global $fetch;
    
    date_default_timezone_set('Asia/Kolkata');
    $current = strtotime($startDate);
    $last    = strtotime($endDate);
    while ($current <= $last) {
        $todayDate = date('Y-m-d', $current);
        $current   = strtotime('+1 day', $current);
        
        $whereCondition = '';
        //check employee auto period flag off record
        $autoPeriodOffQuery = "SELECT TOP 1 * FROM Roster_schema WHERE Emp_Code = '" . $empCode . "' AND '" . $todayDate . "' BETWEEN CAST(start_rost AS DATE) AND CAST(end_rost AS DATE) AND auto_period = 0";
        $autoPeriodOffQueryResult = query($query, $autoPeriodOffQuery, $pa, $opt, $ms_db);
        
        if($num($autoPeriodOffQueryResult) > 0){
            $whereCondition = " Roster_schema.RosterName= att_roster.roster AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid AND att_roster.shiftMaster = shiftMast.ShiftMastId  AND Roster_schema.Emp_Code = '" . $empCode . "' AND '" . $todayDate . "' BETWEEN CAST(Roster_schema.start_rost AS DATE) AND CAST(Roster_schema.end_rost AS DATE) AND Roster_schema.auto_period = 0 ORDER BY Roster_schema.end_rost DESC";
        }else{
            $whereCondition = " Roster_schema.RosterName= att_roster.roster AND att_roster.shiftPattern = ShiftPatternMast.ShiftPatternMastid AND att_roster.shiftMaster = shiftMast.ShiftMastId  AND Roster_schema.Emp_Code = '" . $empCode . "' AND DAY('" . $todayDate . "') BETWEEN DAY(Roster_schema.start_rost) AND DAY(Roster_schema.end_rost) AND Roster_schema.auto_period = 1 AND Roster_schema.end_rost <= '" . $todayDate . "' ORDER BY Roster_schema.end_rost DESC";
        }
        
        $employeeShiftDetailQuery = "SELECT TOP 1 WeeklyOff1, WeeklyOff2, WeeklyOff3, WeeklyOff4, WeeklyOff5 FROM Roster_schema, att_roster, ShiftMast, ShiftPatternMast WHERE ".$whereCondition;
        
        $employeeShiftDetailResult = query($query, $employeeShiftDetailQuery, $pa, $opt, $ms_db);
        
        $shiftPatterns = array(
            '7',
            '7',
            '7',
            '7',
            '7',
            '7'
        );
        
        if ($num($employeeShiftDetailResult) > 0) {
            $employeeShiftDetailResultRow = $fetch($employeeShiftDetailResult);
            $shiftPatterns                = array(
                $employeeShiftDetailResultRow['WeeklyOff1'],
                $employeeShiftDetailResultRow['WeeklyOff2'],
                $employeeShiftDetailResultRow['WeeklyOff3'],
                $employeeShiftDetailResultRow['WeeklyOff4'],
                $employeeShiftDetailResultRow['WeeklyOff5']
            );
        }
        
        $start       = new DateTime(date('Y-m-01', strtotime($todayDate)));
        $end         = new DateTime(date('Y-m-t', strtotime($todayDate)));
        $interval    = new DateInterval('P1D');
        $dateRange   = new DatePeriod($start, $interval, $end);
        $weekSet     = '';
        $weekNumber  = 0;
        $weeks       = array();
        $weekSetTemp = array();
        
        for ($date = $start; $date <= $end; $date->modify('+1 day')) {
            if (!in_array($date->format("W"), $weekSetTemp) && isset($shiftPatterns[$weekNumber])) {
                $weekSetTemp[] = $date->format("W");
                $weekSet       = explode(',', $shiftPatterns[$weekNumber]);
                $weekNumber++;
            }
            if (in_array($date->format('N'), $weekSet)) {
                //$weeks[$date->format("W")][] = $date->format('Y-m-d') . ' - ' . $date->format('N');
                $weeks[$date->format("W") . "_" . $date->format('N')] = $date->format('Y-m-d');
            }
        }
        if (in_array($todayDate, $weeks)) {
            $finalRecords[] = array(
                'start' => $todayDate
            );
        }
    }
    return $finalRecords;
}
?>