<?php 

function getLeaves($startDate,$endDate,$emp_code){
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	$daysInMonth = abs(floor(strtotime($startDate) / (60 * 60 * 24)) - floor(strtotime($endDate) / (60 * 60 * 24)));
	$startDayOfMonth = date('m', strtotime($startDate));
	$endDayOfMonth = date('m', strtotime($endDate));
	$step = '+1 day';
	$date_output_format = 'Y-m-d';
	$result1= array();
	$startDateTimeStamp = strtotime($startDate);
	$sqlq="SELECT cast(LvFrom as date) as startDate,cast(LvTo as date) as endDate,status,reason,LvType,FromHalf FROM leave WHERE leaveID IN (SELECT MAX(leaveID) FROM leave GROUP BY LevKey) AND (LvFROM BETWEEN '".$startDate."' AND '".$endDate."' OR LvTO BETWEEN '".$startDate."' AND '".$endDate."') AND status  NOT IN( '4' , '3')  and createdby='".$emp_code."'
		ORDER BY LvFrom";
	$resultq=query($query,$sqlq,$pa,$opt,$ms_db);
	
	if($resultq){
		$tempArray4=$num($resultq);
	}else{
		$tempArray4=-1;
	}
	$varArray = array();
	if($tempArray4>0) {
		while ($rowq = $fetch($resultq)){

			$s = $rowq['startDate'];
			$e = $rowq['endDate'];

			if($rowq['status']==1){
				$status='Pending';
			}else if($rowq['status']==2){
				$status='Approved';
			}else if($rowq['status']==3){
				$status='Rejected';
			}else if($rowq['status']==4){
				$status='Cancelled';
			}else if($rowq['status']==5){
				$status='Approved Cancel - Pending';
			}else if($rowq['status']==6){
				$status='Approved Cancellation Request';
			}else if($rowq['status']==7){
				$status='Approved Cancellation - Rejected';
			}

			$rowq['FromHalf'] = trim($rowq['FromHalf']);
			$leaveNature = 'full_day';
			switch ($rowq['FromHalf']) {
				case '1FH':
					$leaveNature = 'first_half';
					break;
				
				case '2FH':
					$leaveNature = 'second_half';
					break;

				case '2FD':
					$leaveNature = 'full_day';
					break;
			}

			$current = strtotime($s);
    		$last = strtotime($e);

    		if($current == $last){
    			$varArray[] = array('date'=>date('Y-m-d', $current),'status'=>$status,'reason'=>$rowq['reason'],'LvType'=>$rowq['LvType'],'leaveNature'=>$leaveNature);
    		}else{
    			while( $current <= $last ) {
 				$leaveDate = date($date_output_format, $current);
			        $current = strtotime($step, $current);
				if($startDateTimeStamp < $current){
			        	$varArray[] = array('date'=>$leaveDate,'status'=>$status,'reason'=>$rowq['reason'],'LvType'=>$rowq['LvType'],'leaveNature'=>$leaveNature);
				}
			    }
    		}
		}
	}

	for($j=0; $j<count($varArray); $j++){
		if($varArray[$j]['status']=='Approved' || $varArray[$j]['status']=='Approved Cancel - Pending'){
			$result1[] = array('type'=>'leave','title'=>'Leave','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("blue_over circle-legend"),'presetShortStatus'=>'P','lvType'=>$varArray[$j]['LvType'],'leaveNature'=>$varArray[$j]['leaveNature']);
		}else if($varArray[$j]['status']=='Pending'){
			$result1[] = array('type'=>'leave','title'=>'Absent','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','lvType'=>$varArray[$j]['LvType'],'leaveNature'=>$varArray[$j]['leaveNature']);
		}else if($varArray[$j]['status']=='Approved Cancellation - Rejected' || $varArray[$j]['status']=='Approved Cancellation Request'){
			$result1[] = array('type'=>'leave','title'=>'Leave','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','lvType'=>$varArray[$j]['LvType'],'leaveNature'=>$varArray[$j]['leaveNature']);
		}else{
			$result1[] = array('type'=>'leave','title'=>'Leave','reason'=>$varArray[$j]['reason'],'status'=>'Absent','start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','lvType'=>$varArray[$j]['LvType']);
		}
	}
//echo "<pre>"; print_r($result1); die;
	return $result1;
}


function getAttendanceRegularised($emp_code, $startDate, $endDate){
	
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	$daysInMonth = abs(floor(strtotime($startDate) / (60 * 60 * 24)) - floor(strtotime($endDate) / (60 * 60 * 24)));
	$firstDate= $startDate;
	$lastDate = $endDate;
	$startDayOfMonth = date('m', strtotime($startDate));
	$endDayOfMonth = date('m', strtotime($endDate));
	$step = '+1 day';
	$date_output_format = 'Y-m-d';

	$startDateTimeStamp = strtotime($startDate);

	$result= array();
	$sqlq1="SELECT cast(date_from as date) as date_from,cast(date_to as date) as date_to,intime,outtime,action_status,notMarkingReason,remarks FROM markpastattendance WHERE markPastId IN (SELECT MAX(markPastId) fROM markpastattendance GROUP BY AttnKey) AND (date_FROM BETWEEN '".$firstDate."' AND '".$lastDate."' OR date_TO BETWEEN '".$firstDate."' AND '".$lastDate."') AND action_status  NOT IN( '4' , '3')  and createdby='".$emp_code."' ORDER BY date_from";


	$resultq1=query($query,$sqlq1,$pa,$opt,$ms_db);
	
	if($resultq1){
		$tempArray41=$num($resultq1);
	}else{
		$tempArray41=-1;
	}
	$varArray = Array();
	if($tempArray41>0) {
		while ($rowq1 = $fetch($resultq1)){


			$s = $rowq1['date_from'];
			$e = $rowq1['date_to'];
			
			$status =$rowq1['Action'];

			$start = explode('-', $s);
			$end = explode('-', $e);
			
			if($start[1]<$month){
				$startFrom = 1;
			}else{
				$startFrom = $start[2];
			}

			if($end[1]>$month){
				$endTo = $daysInMonth;
			}else{
				$endTo = $end[2];
			}
			
			if($rowq1['action_status']==1){
				$status='Pending';
			}else if($rowq1['action_status']==2){
				$status='Approved';
			}else if($rowq1['action_status']==3){
				$status='Rejected';
			}else if($rowq1['action_status']==4){
				$status='Cancelled';
			}else if($rowq1['action_status']==5){
				$status='Approved Cancel - Pending';
			}else if($rowq1['action_status']==6){
				$status='Approved Cancellation Request';
			}else if($rowq1['action_status']==7){
				$status='Approved Cancellation - Rejected';
			}

			$current = strtotime($s);
    		$last = strtotime($e);

    		$inTime = (!empty($rowq1['intime']))?date('h:i A', strtotime($rowq1['intime'])):'N/A';
    		$outTime = (!empty($rowq1['outtime']))?date('h:i A', strtotime($rowq1['outtime'])):'N/A';

    		if($current == $last){
    			$varArray[] = array('date'=>date('Y-m-d',$current),'status'=>$status,'startTime'=>$inTime,'endTime'=>$outTime,'reason'=>$rowq1['remarks']);
    		}else{
    			while( $current <= $last ) {
			        $syncDate = date($date_output_format, $current);
			        $current = strtotime($step, $current);
				if($startDateTimeStamp < $current){
			        	$varArray[] = array('date'=>$syncDate,'status'=>$status,'startTime'=>$inTime,'endTime'=>$outTime,'reason'=>$rowq1['remarks']);
				}
			    }
    		}

		}

		for($j=0; $j<count($varArray); $j++){
			if($varArray[$j]['status']=='Approved' || $varArray[$j]['status']=='Approved Cancel - Pending'){
				$result[] = array('type'=>'attRegularised','title'=>'Attendance Regularised','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("green2 circle-legend"),'presetShortStatus'=>'P','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime']);
			}else if($varArray[$j]['status']=='Pending'){
				$result[] = array('type'=>'attRegularised','title'=>'Absent','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime']);
			}else if($varArray[$j]['status']=='Approved Cancellation - Rejected' || $varArray[$j]['status']=='Approved Cancellation Request'){
				$result[] = array('type'=>'attRegularised','title'=>'Attendance Regularised','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime']);
			}else{
				$result[] = array('type'=>'attRegularised','title'=>'Attendance Regularised','reason'=>$varArray[$j]['reason'],'status'=>'Absent','start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime']);
			}
		}
	}

	return $result;

} 



function getODRequest($emp_code, $startDate, $endDate) {
	
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	$daysInMonth = abs(floor(strtotime($startDate) / (60 * 60 * 24)) - floor(strtotime($endDate) / (60 * 60 * 24)));
	$firstDate= $startDate;
	$lastDate = $endDate;
	$startDayOfMonth = date('m', strtotime($startDate));
	$endDayOfMonth = date('m', strtotime($endDate));
	$step = '+1 day';
	$date_output_format = 'Y-m-d';
	$startDateTimeStamp = strtotime($startDate);
	$result= array();
	$sqlq1="SELECT cast(date_from as date) as date_from,cast(date_to as date) as date_to,intime,outtime,action_status,reason,natureOfWorkCause fROM outOnWorkRequest WHERE outWorkId IN (SELECT MAX(outWorkId) fROM outOnWorkRequest GROUP BY oDKey) AND (date_FROM BETWEEN '".$firstDate."' AND '".$lastDate."' OR date_TO BETWEEN '".$firstDate."' AND '".$lastDate."') and action_status NOT IN( '4' , '3') and createdby='".$emp_code."' ORDER BY date_from";

	$resultq1=query($query,$sqlq1,$pa,$opt,$ms_db);
	
	if($resultq1){
		$tempArray41=$num($resultq1);
	}else{
		$tempArray41=-1;
	}
	$varArray = Array();
	if($tempArray41>0) {
		while ($rowq1 = $fetch($resultq1)){


			$s = $rowq1['date_from'];
			$e = $rowq1['date_to'];
			
			if($rowq1['action_status']==1){
				$status='Pending';
			}else if($rowq1['action_status']==2){
				$status='Approved';
			}else if($rowq1['action_status']==3){
				$status='Rejected';
			}else if($rowq1['action_status']==4){
				$status='Cancelled';
			}else if($rowq1['action_status']==5){
				$status='Approved Cancel - Pending';
			}else if($rowq1['action_status']==6){
				$status='Approved Cancellation Request';
			}else if($rowq1['action_status']==7){
				$status='Approved Cancellation - Rejected';
			}
 
			$current = strtotime($s);
    		$last = strtotime($e);
    		$inTime = trim($rowq1['intime']); //(!empty($rowq1['intime']))?date('h:i A', strtotime($rowq1['intime'])):'N/A';
    		$outTime = trim($rowq1['outtime']); //(!empty($rowq1['outtime']))?date('h:i A', strtotime($rowq1['outtime'])):'N/A';
    		if($current == $last){
    			$varArray[] = array('date'=>date('Y-m-d',$current),'status'=>$status,'startTime'=>$inTime ,'endTime'=>$outTime,'reason'=>$rowq1['reason'],'natureOfWorkCause'=>$rowq1['natureOfWorkCause']);
    		}else{
    			while( $current <= $last ) {
			        $syncDate = date($date_output_format, $current);
			        $current = strtotime($step, $current);
				if($startDateTimeStamp < $current){
			        	$varArray[] = array('date'=> $syncDate,'status'=>$status,'startTime'=>$inTime ,'endTime'=>$outTime,'reason'=>$rowq1['reason'],'natureOfWorkCause'=>$rowq1['natureOfWorkCause']);
				}
			    }
    		}
		}

		for($j=0; $j<count($varArray); $j++){
			if($varArray[$j]['status']=='Approved' || $varArray[$j]['status']=='Approved Cancel - Pending'){
				$result[] = array('type'=>'odrequest','title'=>'OD Request','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("green2 circle-legend"),'presetShortStatus'=>'P','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime'],'natureOfWorkCause'=>$varArray[$j]['natureOfWorkCause']);
			}else if($varArray[$j]['status']=='Pending'){
				$result[] = array('type'=>'odrequest','title'=>'Absent','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime'],'natureOfWorkCause'=>$varArray[$j]['natureOfWorkCause']);
			}else if($varArray[$j]['status']=='Approved Cancellation - Rejected' || $varArray[$j]['status']=='Approved Cancellation Request'){
				$result[] = array('type'=>'odrequest','title'=>'OD Request','reason'=>$varArray[$j]['reason'],'status'=>$varArray[$j]['status'],'start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime'],'natureOfWorkCause'=>$varArray[$j]['natureOfWorkCause']);
			}else{
				$result[] = array('type'=>'odrequest','title'=>'OD Request','reason'=>$varArray[$j]['reason'],'status'=>'Absent','start'=>$varArray[$j]['date'],'className'=>array("red circle-legend"),'presetShortStatus'=>'A','startTime'=>$varArray[$j]['startTime'],'endTime'=>$varArray[$j]['endTime'],'natureOfWorkCause'=>$varArray[$j]['natureOfWorkCause']);
			}
		}
	}
	return $result;

} 
	

?>