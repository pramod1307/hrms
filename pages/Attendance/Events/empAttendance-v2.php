<?php
include("attendance-class-v2.php");

function getMinutes($OutTime,$InTime){
	return (abs(strtotime($OutTime) - strtotime($InTime)) / 60);	
}


function getAttendance($emp_code, $startDate, $endDate,$weeklyHolidayCombineDates = ''){
	$hello = getShiftWiseRecord($emp_code, $startDate, $endDate,$weeklyHolidayCombineDates);
	return $hello;
}

function checkEmployeeOTRequest($empCode, $date){
	global $flag;
	global $flag1;
	global $lates;
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	$date = date('Y-m-d',strtotime($date));
	
	$selectQuery = "SELECT overTimeHours,OT_Approver,appStatus FROM overtime_Balances WHERE Emp_Code = '".$empCode."' AND CAST(OT_Date AS DATE) = '".$date."'";
	$resultQuery = query($query,$selectQuery,$pa,$opt,$ms_db);
	if($num($resultQuery) > 0){
		$resultArray 	= $fetch($resultQuery);
		
		$approverArray 	= (!empty($resultArray['OT_Approver'])) ? explode(',', $resultArray['OT_Approver']) : '';
		$approverStatusArray = (!empty($resultArray['appStatus'])) ? explode(',', $resultArray['appStatus']) : '';
		if( end($approverStatusArray) == '2' && count($approverArray) == count($approverStatusArray) ){
			return $resultArray['overTimeHours'];
		}
	}
	return '';

}

function getShiftWiseRecord($emp_code, $startDate, $endDate,$weeklyHolidayCombineDates = ''){
	//var_dump("stop script here"); die; 
	global $flag;
	global $flag1;
	global $lates;
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;

	$RowData = $hello = array();

	$sqlq00 = "select * from cattendanceqry where EMP_CODE='".$emp_code."' and  ATTDate between '".$startDate."' and '".$endDate."'";
	
	$resultq00 = query($query,$sqlq00,$pa,$opt,$ms_db);
	if($resultq00){
		$tempArray00 = $num($resultq00);
	}else{
		$tempArray00 = -1;
	}
	if($tempArray00 > 0) {
		$resultArray = array();
		$obj = new ProcessAttendance();
		while ($rowq00 = $fetch($resultq00)){
			

			//check if current matched in any one from weeklyoff or holidays
			$isWeeklyHolidayDate = (in_array(trim($rowq00['ATTDATE']), $weeklyHolidayCombineDates)) ? 1 : 0 ;

			if($isWeeklyHolidayDate) {
				//chnage intime and out to punch time if dateis in weekly off or holiday
				$getActualPunchTimeQueryTop = "SELECT * FROM fnPunchTime('".$rowq00['EMP_CODE']."','".$rowq00['ATTDATE']."')"; 
				$getActualPunchTimeResultTop = query($query,$getActualPunchTimeQueryTop,$pa,$opt,$ms_db);
				if($num($getActualPunchTimeResultTop) > 0) {
					$getActualPunchTimeRowTop 	= $fetch($getActualPunchTimeResultTop);
					$rowq00['OUT_TIME'] = (!empty($getActualPunchTimeRowTop["Outtime"])) ? $getActualPunchTimeRowTop["Outtime"]: $rowq00['OUT_TIME'];
					$rowq00['IN_TIME'] = (!empty($getActualPunchTimeRowTop["Intime"])) ? $getActualPunchTimeRowTop["Intime"]: $rowq00['IN_TIME'];
				}
			}

			$rowq00['EMP_CODE'] = trim($rowq00['EMP_CODE']);
			$otTime = '';
			//check and get employee OT time
			$otTime = checkEmployeeOTRequest($rowq00['EMP_CODE'], $rowq00['ATTDATE']);

			$finalOuttime 	 = '';
			$isOt = false;
			$finalSWEnd_Time = $rowq00['ShiftEnd_SwTime'];

			if(empty($otTime)){
				$finalSWEnd_TimeArray = (!empty($finalSWEnd_Time)) ? explode(':', $finalSWEnd_Time) : '';
				$finalSWEnd_Time = (!empty($finalSWEnd_TimeArray)) ? date('M d Y H:i',strtotime('+'.$finalSWEnd_TimeArray[0].' hour +'.$finalSWEnd_TimeArray[1].' minutes',strtotime($rowq00['Shiftto']))) : '';
			}
 
			/*Check OT request and add into shiftEndtime+SwEnd_TIme*/
			if(!empty($otTime)) {
				$isOt = true;
				//get actual punch from next date
				$getActualPunchTimeQuery = "SELECT * FROM fnPunchTime('".$rowq00['EMP_CODE']."','".$rowq00['ATTDATE']."')"; 
				$getActualPunchTimeResult = query($query,$getActualPunchTimeQuery,$pa,$opt,$ms_db);
				if($num($getActualPunchTimeResult) > 0){
					$getActualPunchTimeRow 	= $fetch($getActualPunchTimeResult);
					$rowq00['OUT_TIME'] = (!empty($getActualPunchTimeRow["Outtime"])) ? $getActualPunchTimeRow["Outtime"]: $tempOutTime;
				}

				$otTimeArray = explode(":",$otTime);
				$finalOuttimeWithOT = date('M d Y H:i',strtotime('+'.$otTimeArray[0].' hour +'.$otTimeArray[1].' minutes',strtotime($rowq00['Shiftto'])));

				$ShiftEnd_SwTimeArray = (!empty($rowq00['ShiftEnd_SwTime'])) ? explode(":",$rowq00['ShiftEnd_SwTime']) : '';

 				if(count($ShiftEnd_SwTimeArray) > 0){
 					$finalSWEnd_Time = date('M d Y H:i',strtotime($finalOuttimeWithOT));
 					//$finalSWEnd_Time = date('M d Y H:i',strtotime('+'.$ShiftEnd_SwTimeArray[0].' hour +'.$ShiftEnd_SwTimeArray[1].' minutes',strtotime($finalOuttimeWithOT)));
 				}

			}else{
				if(!$isWeeklyHolidayDate){
					$getActualInTimeQuery = "SELECT TOP 1 * FROM
										( SELECT 
										CAST( CAST(CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [START_SHIFT_DATETIME],
										CAST( CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(SHIFT_TO AS DATETIME) AS DATETIME) + CAST(ShiftEnd_SwTime AS DATETIME) AS DATETIME) AS [END_SHIFT_DATETIME],
										AttendanceAll.PunchDate AS InTime, AttendanceAll.EMP_CODE AS ATT_EMP_CODE
										FROM RosterQry,AttendanceAll WHERE RosterQry.EMP_CODE = '".$rowq00['EMP_CODE']."' AND '".$rowq00['ATTDATE']."' BETWEEN RosterQry.RosterStart AND RosterQry.RosterEnd ) rosterView
										WHERE (InTime BETWEEN START_SHIFT_DATETIME AND END_SHIFT_DATETIME) AND ATT_EMP_CODE = '".$rowq00['EMP_CODE']."' ORDER BY InTime ASC";
					$getActualInTimeResult = query($query,$getActualInTimeQuery,$pa,$opt,$ms_db);
					if($num($getActualInTimeResult) > 0) {
						$getActualInTimeRow 	= $fetch($getActualInTimeResult);
						$rowq00['IN_TIME'] = $getActualInTimeRow['InTime'];
					}
					/*store out time in variable and will use when OT will find*/
					$tempOutTime = $rowq00['Outtime'];

					$getActualOutTimeQuery = "SELECT TOP 1 * FROM
											( SELECT 
											CAST( CAST(CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [START_SHIFT_DATETIME],
											CAST( CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(SHIFT_TO AS DATETIME) AS DATETIME) + CAST(ShiftEnd_SwTime AS DATETIME) AS DATETIME) AS [END_SHIFT_DATETIME],
											AttendanceAll.PunchDate AS OutTime, AttendanceAll.EMP_CODE AS ATT_EMP_CODE
											FROM RosterQry,AttendanceAll WHERE RosterQry.EMP_CODE = '".$rowq00['EMP_CODE']."' AND '".$rowq00['ATTDATE']."' BETWEEN RosterQry.RosterStart AND RosterQry.RosterEnd ) rosterView
											WHERE (OutTime BETWEEN START_SHIFT_DATETIME AND END_SHIFT_DATETIME) AND ATT_EMP_CODE = '".$rowq00['EMP_CODE']."' ORDER BY OutTime DESC";
					$getActualOutTimeResult = query($query,$getActualOutTimeQuery,$pa,$opt,$ms_db);
					if($num($getActualOutTimeResult) > 0) {
						$getActualOutTimeRow 	= $fetch($getActualOutTimeResult);
						$rowq00['OUT_TIME'] = ( strtotime($rowq00['IN_TIME']) == strtotime($getActualOutTimeRow['OutTime']))  ? '' : $getActualOutTimeRow['OutTime'];
					}
				}
			}


			/*check and replace in punch time if swap time is less*/
			/*if(!empty($rowq00['IN_TIME'])){
				$tempInTinme = date('M d Y H:i', strtotime($rowq00['IN_TIME']));
				$ShiftStart_SwTimeArray = (!empty($rowq00['ShiftStart_SwTime'])) ? explode(":",$rowq00['ShiftStart_SwTime']) : '';
				$finalSWStart_Time = date('M d Y H:i',strtotime('-'.$ShiftStart_SwTimeArray[0].' hour +'.$ShiftStart_SwTimeArray[1].' minutes',strtotime($rowq00['ShiftFrom'])));
				if(!empty($rowq00['IN_TIME']) && strtotime($tempInTinme) < strtotime($finalSWStart_Time) ){
					$rowq00['IN_TIME'] = '';
				}
			}*/

			/*check and replace out punch time if swap time is less*/
			if($isOt){
				$tempOutTinme = date('M d Y H:i', strtotime($rowq00['OUT_TIME']));
				if(!empty($rowq00['OUT_TIME']) && strtotime($tempOutTinme) > strtotime($finalSWEnd_Time) ){
					$rowq00['OUT_TIME'] = '';
				}
			}
			
			$inputArray[] = array(
                    'empCode'           =>$rowq00['EMP_CODE'],
                    'attDate'           =>$rowq00['ATTDATE'],
                    'inTime'            =>$rowq00['IN_TIME'],
                    'outTime'           =>$rowq00['OUT_TIME'],
                    'shiftName'         =>$rowq00['Shift_Name'],
                    'actual_shiftFrom'  =>$rowq00['ShiftFrom'],
                    'actual_shiftTo'    =>$rowq00['Shiftto'],
                    'shiftFrom'         =>$rowq00['ShiftFrom'],
                    'shiftTo'           =>(!empty($finalSWEnd_Time) && !empty($otTime)) ? $finalSWEnd_Time : $rowq00['Shiftto'],
                    'shiftMFrom'        =>$rowq00['ShiftMFrom'],
                    'shiftMTo'          =>(!empty($finalSWEnd_Time) && !empty($otTime)) ? $finalSWEnd_Time : $rowq00['ShiftMTo'],
                    'ShiftStartSwTime'  =>$rowq00['ShiftStart_SwTime'],
                    'ShiftEndSwTime'    =>$rowq00['ShiftEnd_SwTime'],
                    'lateAllow'         =>(!empty($rowq00['LateAllow'])) ? $rowq00['LateAllow']: 0,
                    'lateAllowCycle'    =>(!empty($rowq00['LateAllowCycle'])) ? $rowq00['LateAllowCycle']: 0,
                    'lateAllowGPrd'     =>(!empty($rowq00['LateAllowGPrd'])) ? $rowq00['LateAllowGPrd']: 0,
                    'erlyAllow'         =>(!empty($rowq00['ErlyAllow'])) ? $rowq00['ErlyAllow'] : 0,
                    'erlyAllowCycle'    =>(!empty($rowq00['ErlyAllowCycle'])) ? $rowq00['ErlyAllowCycle'] : 0,
                    'erlyAllowGPrd'     =>(!empty($rowq00['ErlyAllowGPrd'])) ? $rowq00['ErlyAllowGPrd'] : 0,
                    'mHrsFul'           =>getMinutes($rowq00['MHrsFul'],'00:00:00'),
                    'lates'             =>$rowq00['lates'],
                    'mHrsHalf'          =>getMinutes($rowq00['MHrsHalf'],'00:00:00'),
                    "BusArrival"        =>(!empty($rowq00['Buslate'])) ? $rowq00['Buslate'] : 0,
                    "isWeeklyHolidayDate" => $isWeeklyHolidayDate,
                ) ;

			$obj->initializeObject( array(
                    'empCode'           =>$rowq00['EMP_CODE'],
                    'attDate'           =>$rowq00['ATTDATE'],
                    'inTime'            =>$rowq00['IN_TIME'],
                    'outTime'           =>$rowq00['OUT_TIME'],
                    'shiftName'         =>$rowq00['Shift_Name'],
                    'actual_shiftFrom'  =>$rowq00['ShiftFrom'],
                    'actual_shiftTo'    =>$rowq00['Shiftto'],
                    'shiftFrom'         =>$rowq00['ShiftFrom'],
                    'shiftTo'           =>(!empty($finalSWEnd_Time) && !empty($otTime)) ? $finalSWEnd_Time : $rowq00['Shiftto'],
                    'shiftMFrom'        =>$rowq00['ShiftMFrom'],
                    'shiftMTo'          =>(!empty($finalSWEnd_Time) && !empty($otTime)) ? $finalSWEnd_Time : $rowq00['ShiftMTo'],
                    'ShiftStartSwTime'  =>$rowq00['ShiftStart_SwTime'],
                    'ShiftEndSwTime'    =>$rowq00['ShiftEnd_SwTime'],
                    'lateAllow'         =>(!empty($rowq00['LateAllow'])) ? $rowq00['LateAllow']: 0,
                    'lateAllowCycle'    =>(!empty($rowq00['LateAllowCycle'])) ? $rowq00['LateAllowCycle']: 0,
                    'lateAllowGPrd'     =>(!empty($rowq00['LateAllowGPrd'])) ? $rowq00['LateAllowGPrd']: 0,
                    'erlyAllow'         =>(!empty($rowq00['ErlyAllow'])) ? $rowq00['ErlyAllow'] : 0,
                    'erlyAllowCycle'    =>(!empty($rowq00['ErlyAllowCycle'])) ? $rowq00['ErlyAllowCycle'] : 0,
                    'erlyAllowGPrd'     =>(!empty($rowq00['ErlyAllowGPrd'])) ? $rowq00['ErlyAllowGPrd'] : 0,
                    'mHrsFul'           =>getMinutes($rowq00['MHrsFul'],'00:00:00'),
                    'lates'             =>$rowq00['lates'],
                    'mHrsHalf'          =>getMinutes($rowq00['MHrsHalf'],'00:00:00'),
                    "BusArrival"        =>(!empty($rowq00['Buslate'])) ? $rowq00['Buslate'] : 0,
                    "isWeeklyHolidayDate" => $isWeeklyHolidayDate,
                ) );
			$resultArray[] = $obj->getResult();
		}

	}
/*	
	echo "<pre>"; print_r($inputArray);
	echo "---------------------";
	prd($resultArray);*/
	return $resultArray;
}	

?>