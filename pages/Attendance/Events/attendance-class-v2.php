<?php
Class ProcessAttendance
{
    var $empCode;
    var $attDate;
    var $inTime;
    var $actualInTime;
    var $outTime;
    var $actualOutTime;
    var $shiftName;
    var $shiftFrom;
    var $shiftTo;
    var $actual_shiftFrom;
    var $actual_shiftMTo;
    var $shiftMFrom;
    var $shiftMTo;
    var $lateAllow;
    var $lateAllowCycle;
    var $lateAllowGPrd;
    var $erlyAllow;
    var $erlyAllowCycle;
    var $erlyAllowGPrd;
    var $mHrsFul;
    var $mHrsHalf;
    var $ShiftStartSwTime;
    var $ShiftEndSwTime;
    var $BusArrival;
    var $hoursSpent;
    var $actualHoursSpent;
    var $isLateComingPenalty;
    var $isEarlyGoingPenalty;
    var $attendanceMessage;
    var $isCommonCycle;
    var $isCommonCycleTime;
    var $isWeeklyHolidayDate;
    var $isValidForMaxlife;
    var $isOT;
    
    public static $totalLate = 0;
    public static $totalEarly = 0;
    public static $earlyGoingCountMessage = 1;
    public static $lateComingCountMessage = 1;
    
    public function initializeObject($options)
    { 
        $this->empCode          = $options['empCode'];
        $this->attDate          = $options['attDate'];
        $this->inTime           = $options['inTime'];
        $this->outTime          = $options['outTime'];
        $this->shiftName        = $options['shiftName'];
        $this->actual_shiftFrom = $options['actual_shiftFrom'];
        $this->shiftFrom        = $options['shiftFrom'];
        $this->actual_shiftTo   = $options['actual_shiftTo'];
        $this->shiftTo          = $options['shiftTo'];
        $this->shiftMFrom       = $options['shiftMFrom'];
        $this->shiftMTo         = $options['shiftMTo'];
        $this->ShiftStartSwTime = $options['ShiftStartSwTime'];
        $this->ShiftEndSwTime   = $options['ShiftEndSwTime'];
        $this->lateAllow        = $options['lateAllow'];
        $this->lateAllowCycle   = $options['lateAllowCycle'];
        $this->lateAllowGPrd    = $options['lateAllowGPrd'];
        $this->erlyAllow        = $options['erlyAllow'];
        $this->erlyAllowCycle   = $options['erlyAllowCycle'];
        $this->erlyAllowGPrd    = $options['erlyAllowGPrd'];
        $this->mHrsFul          = $options['mHrsFul'];
        $this->mHrsHalf         = $options['mHrsHalf'];
        $this->BusArrival       = $options['BusArrival'];
        $this->isWeeklyHolidayDate  = $options['isWeeklyHolidayDate'];
        $this->hoursSpent           = 0;
        $this->actualHoursSpent     = 0;
        $this->isLateComingPenalty = 0;
        $this->isEarlyGoingPenalty = 0;
        $this->isCommonCycle       = 1;
        $this->isCommonCycleTime   = 1234567890000;
	$this->isValidForMaxlife   = 0;
        $this->isOT                = $options['isOT'];
        //change actual cycle time
        if($this->isCommonCycle){
            $this->erlyAllowCycle = $this->isCommonCycleTime;
            $this->lateAllowCycle = $this->isCommonCycleTime;
        }

        if ($this->BusArrival || $this->getInTime() != "" && ($this->getInTime() <= ($this->getShiftMFrom() - $this->lateAllowGPrd))) {
            $this->actualInTime = $this->getShiftMFrom();
        } else {
            $this->actualInTime = $this->getInTime();
        }

        if ($this->getOutTime() != '' && ($this->getOutTime() >= ($this->getShiftMTo() - $this->erlyAllowGPrd))) {
            $this->actualOutTime = $this->getShiftMTo();
        } else {
            $this->actualOutTime = $this->getOutTime();
        }

        if (!empty($this->actualInTime) && !empty($this->actualOutTime)) {
            $this->hoursSpent       = $this->getOutTime() - $this->getInTime();
            $this->actualHoursSpent = $this->actualOutTime - $this->actualInTime;
        }

        $this->setTotalLate();
        $this->setTotalEarly();
    }
    
    public function getResult()
    {
		/*															This Code is For Max Life																	*/
		
		if(isset($_SESSION['companyDBData']['COMP_CODE']) && $_SESSION['companyDBData']['COMP_CODE'] == 'maxlife'){
			
				if($this->getInTime() == '' && $this->getOutTime() == ''){
						$presetShortStatus			=	$this->presetShortStatus();
						$presetStatus				=	$this->presetStatus();
						$type						=	'attendance';
						$title						=	$this->presetTitle();
						$className	 				=	$this->getCssClassName();
				
				}else if($this->getInTime() != '' || $this->getOutTime() != ''){
						$presetShortStatus			=	"P";
						$presetStatus				=	"Present";
						$type						=	'attendance';
						$title						=	'Present';
						$className	 				=	 array("green2 circle-legend");
						$this->isValidForMaxlife	=	1;
				}else{
						$presetShortStatus			=	$this->presetShortStatus();
						$presetStatus				=	$this->presetStatus();
						$type						=	'attendance';
						$title						=	$this->presetTitle();
						$className	 				=	$this->getCssClassName();	
				}
		}else{
						$presetShortStatus			=	$this->presetShortStatus();
						$presetStatus				=	$this->presetStatus();
						$type						=	'attendance';
						$title						=	$this->presetTitle();
						$className	 				=	$this->getCssClassName();
		}
		
		/* 															End Code 							*/	
		
        return $returnAray = array(
            "type" => "attendance",
            "shiftName" => "Shift - ".$this->shiftName,
            "shiftInOutTime" => $this->shiftInOutTimeFormat(),
            "num" => $this->getTotalLate(),
            "start" => date('Y-m-d', strtotime($this->attDate)),
            "startTime" => $this->formatTime($this->getInTime()),
            "endTime" => $this->formatTime($this->getOutTime()),
            "shiftInOutTime" => $this->shiftInOutTimeFormat(),
            "presetShortStatus" => $presetShortStatus,
            "presetStatus" => $presetStatus,
            "title" => $title,
            "timeSpan" => $this->formatHoursSpent(),
            "timeSpanClean" => $this->formatHoursSpentClean(),
            "className" => $className,
            "BusArrival" => $this->BusArrival,
            "lates" => $this->getTotalLate(),
            "totalLate" => $this->getTotalLateCycle(),
            "totalEarly" => $this->getTotalEarlyCycle(),
            "isLateComing" => $this->isLateComingPenalty(),
            "isEarlyGoing" => $this->isEarlyGoingPenalty(),
            "attendanceMessage" => $this->getLateEarlyMessage(),
            "shiftFrom" => $this->formatTime($this->getActualShiftFrom()),
            "shiftTo" => $this->formatTime($this->getActualShiftTo()),
            "outTimeWithDate" => $this->outTime,
            "isValidForMaxlife" => $this->isValidForMaxlife
            );
		
    }
    
    public function formatTime($time)
    {
        return (!empty($time)) ? date("g:i A", $time) : "";
    }
    
    public function formatDate($time)
    {
        return (!empty($time)) ? date("Y-m-d", $time) : "";
    }

    public function shiftInOutTimeFormat(){
        return "(".$this->formatTime($this->getShiftFrom()) ." - ". $this->formatTime($this->getShiftTo()).")";
    }
    
    public function formatHoursSpent()
    {
        $hours = intval($this->hoursSpent / 3600);
        $min   = intval(($this->hoursSpent - ($hours * 3600)) / 60);
        return $hours . " hours  and " . $min . " minutes";
    }
    
    public function formatHoursSpentClean()
    {
        $hours = intval($this->hoursSpent / 3600);
        $min   = intval(($this->hoursSpent - ($hours * 3600)) / 60);
        return $hours . ":" . $min;
    }
    
    public function getInTime()
    {
        return (!empty($this->inTime)) ? strtotime($this->inTime) : "";
    }
    
    public function getOutTime()
    {
        return (!empty($this->outTime)) ? strtotime($this->outTime) : "";
    }
    
    public function getShiftFrom()
    {
        return (!empty($this->shiftFrom)) ? strtotime($this->shiftFrom) : "";
    }  

    public function getActualShiftFrom()
    {
        return (!empty($this->actual_shiftFrom)) ? strtotime($this->actual_shiftFrom) : "";
    }
    
    public function getActualShiftTo()
    {
        return (!empty($this->actual_shiftTo)) ? strtotime($this->actual_shiftTo) : "";
    }
    

    public function getShiftTo()
    {
        return (!empty($this->shiftTo)) ? strtotime($this->shiftTo) : "";
    }
    
    public function getShiftMFrom()
    {
        return (!empty($this->shiftMFrom)) ? strtotime($this->shiftMFrom) - ($this->lateAllow * 60) : "";
    }
    
    public function getShiftSwapStart()
    {
        return (!empty($this->ShiftStartSwTime)) ? strtotime($this->ShiftStartSwTime) : "";
    }
    
    public function getShiftSwapEnd()
    {
        return (!empty($this->ShiftEndSwTime)) ? strtotime($this->ShiftEndSwTime) : "";
    }

    public function getShiftMinEarlyTime()
    {
        return (!empty($this->shiftMTo)) ? strtotime($this->shiftMTo) - ($this->erlyAllow * 60) : "";
    }
    
    public function getShiftMTo()
    {
        return (!empty($this->shiftMTo)) ? strtotime($this->shiftMTo) : "";
    }
    
    public function isValidLateCycle(){ 
        return ( $this->lateAllowCycle > $this->getTotalLateCycle() ) ? true : false;
    }

    public function isValidEarlyCycle()
    { 
        return ( $this->erlyAllowCycle >= $this->getTotalEarlyCycle() ) ? true : false;
    }

    public function changeCycleCount(){
        if($this->getTotalLate() == $this->erlyAllowCycle || $this->getTotalLate() == $this->lateAllowCycle ){
            self::$totalLate = $this->isCommonCycleTime;
            self::$totalEarly = $this->isCommonCycleTime;
        }          
    }

    public function isInOfficeTime()
    {
        return ($this->getInTime() <= ($this->getShiftFrom() + ($this->lateAllow * 60))) || ($this->getOutTime() >= ($this->getShiftTo() - ($this->erlyAllow * 60))) || ($this->getOutTime() <= $this->getShiftTo() && $this->getInTime() <= $this->getShiftTo());
    }
    
    public function isHalfDay()
    {
        return ($this->isInOfficeTime() && $this->isCompletedHalfHours()/* && (!$this->isLateComing() || !$this->isEarlyGoing())*/);
    }
    
    public function isFullDay()
    {
        return ($this->isInOfficeTime() && $this->isCompletedFullHours() && (!$this->isLateComing() || !$this->isEarlyGoing()));
    }
    
    public function isAbsent()
    {
        return ($this->isInOfficeTime() && (bool($this->actualHoursSpent) || !$this->isLateComing() || !$this->isEarlyGoing()));
    }
    
    public function isCompletedFullHours()
    {
        if( ($this->getInTime() > ($this->getShiftFrom() + ($this->lateAllow * 60)) || $this->getOutTime() < ($this->getShiftMTo() - ($this->erlyAllow * 60))) && !$this->isOT)
            return false;

        return ((($this->mHrsFul * 60) <= $this->actualHoursSpent) || 
            ($this->lateAllowCycle >= $this->getTotalLateCycle() && $this->erlyAllowCycle >= $this->getTotalEarlyCycle()) && 
            ($this->getShiftMinEarlyTime() <= $this->actualOutTime) ||
            ( ($this->getTotalLateCycle() <= $this->lateAllowCycle && $this->isLateComingPenalty() ) || ($this->isEarlyGoingPenalty() && $this->erlyAllowCycle >= $this->getTotalEarlyCycle() ) )
            );
    }
    
    public function isCompletedHalfHours()
    {
        return (($this->mHrsHalf * 60) <= $this->actualHoursSpent);
    }
    
    public function isLateComingPenalty(){
        return $this->isLateComingPenalty;
    }

    public function isEarlyGoingPenalty(){
        return $this->isEarlyGoingPenalty;
    }

    public function isPenaltyOver(){
        return ( ($this->getTotalLateCycle() == $this->lateAllowCycle && $this->getTotalEarlyCycle() == $this->erlyAllowCycle) && (!$this->isLateComingPenalty() && !$this->isEarlyGoingPenalty()) ) ? true : false ;
    }

    public function isLateComing()
    {
        if ($this->getInTime() != "" && $this->getOutTime() != "" && ($this->getInTime() >= $this->getShiftMFrom()) && ($this->getInTime() <= ($this->getShiftFrom() + ($this->lateAllow * 60)))) {
            return false;
        } else {
            return true;
        }
    }
    
    public function isEarlyGoing()
    {
        if ($this->getOutTime() != "" && $this->getInTime() != "" && ($this->getOutTime() <= $this->getShiftMTo()) && ($this->getOutTime() >= ($this->getShiftTo() - ($this->erlyAllow * 60)))) {
            return false;
        } else {
            return true;
        }
    }
    
    public function getTotalLate()
    {
        return self::$totalLate + self::$totalEarly;
    }
    
    public function getTotalLateCycle()
    {
        return self::$totalLate;
    }

    public function getTotalEarlyCycle()
    {
        return self::$totalEarly;
    }

    public function setTotalLate()
    {
        if($this->isWeeklyHolidayDate) { return true; }

        if  (!$this->BusArrival &&  
            ($this->getInTime() != "" && 
            ($this->getInTime() >= $this->getShiftMFrom()) && 
            ($this->getInTime() <= ($this->getShiftFrom() + ($this->lateAllow * 60))) &&
            ($this->getInTime() > ($this->getShiftFrom() + ($this->lateAllowGPrd * 60))) 
            )
            ) {
            $this->isLateComingPenalty = 1;
        }
        if (!$this->BusArrival && 
            ($this->getInTime() != "" && 
            ($this->getInTime() >= $this->getShiftMFrom()) && 
            ($this->getInTime() <= ($this->getShiftFrom() + ($this->lateAllow * 60))) &&
            ($this->getInTime() > ($this->getShiftFrom() + ($this->lateAllowGPrd * 60))) 
            //&& $this->isValidLateCycle()
            )
            ) {
            self::$totalLate++;
        }
        
    }
    
    public function getTotalEarly()
    {
        return self::$totalEarly;
    }
    
    public function setTotalEarly()
    {
        if($this->isWeeklyHolidayDate) { return true; }

        if ($this->getOutTime() != "" && $this->isValidEarlyCycle() &&
           ($this->getOutTime() <= $this->getShiftMTo()) && 
           ( $this->getOutTime() < ($this->getShiftMTo() - ($this->erlyAllowGPrd * 60)) 
             &&
             $this->getOutTime() >= ($this->getShiftMTo() - ($this->erlyAllow * 60))
            )
            && $this->isValidEarlyCycle()
           ) {
            $this->isEarlyGoingPenalty = 1;
        }
        if ($this->getOutTime() != "" && $this->isValidEarlyCycle() &&
           ($this->getOutTime() <= $this->getShiftMTo()) && 
           ( $this->getOutTime() < ($this->getShiftMTo() - ($this->erlyAllowGPrd * 60)) 
             &&
             $this->getOutTime() >= ($this->getShiftMTo() - ($this->erlyAllow * 60))
            )
           ) {
            self::$totalEarly++;
        }
        
    }
    
    public function presetShortStatus()
    {
        if ($this->isFullDay()) {
            return "P";
        } elseif ($this->isHalfDay()) {
            return "H";
        } else {
            return "A";
        }
    }
    
    public function presetStatus()
    {   
        switch ($this->presetShortStatus()) {
            case "P":
                return "Present";
                break;
            case "A":
                return "Absent";
                break;
            case "H":
                return "Half Day";
                break;
        }
    }
    
    public function presetTitle()
    {
        switch ($this->presetShortStatus()) {
            case "P":
                return "Present";
                break;
            case "A":
                return "Absent";
                break;
            case "H":
                return "Half Day";
                break;
        }
    }
    
    public function getCssClassName()
    {
        $arr = array();
        switch ($this->presetShortStatus()) {
            case "P":
                $arr[0] = "green2 circle-legend";
                break;
            case "A":
                $arr[0] = "red circle-legend";
                break;
            case "H":
                $arr[0] = "yellow circle-legend ";
                break;
        }
        return $arr;
    }

    public function getLateEarlyMessage(){
        $message = array();
        $isBothPaneltFlag = $isLateComingPaneltyFlag = $isEarlyGoingPaneltFlag = false;
        //check late coming penalty
        if($this->getTotalLateCycle() != 0 &&  $this->getTotalEarlyCycle() != 0 && $this->getTotalLateCycle() > $this->lateAllowCycle && $this->getTotalEarlyCycle() > $this->erlyAllowCycle && !$this->isLateComingPenalty() && !$this->isEarlyGoingPenalty() && $this->isLateComingPenalty() && $this->isEarlyGoingPenalty()) { 
            $isBothPaneltFlag = true;
            $message[] = 'Late Coming/Early Going Penalty';
        } else if($this->getTotalLateCycle() != 0 && $this->getTotalLateCycle() > $this->lateAllowCycle && $this->isLateComingPenalty() ) {
            $isLateComingPaneltyFlag = true;
            $message[] = 'Late Coming Penalty';
        } else if($this->getTotalEarlyCycle() != 0 && $this->getTotalEarlyCycle() > $this->erlyAllowCycle && $this->isEarlyGoingPenalty && $this->isEarlyGoingPenalty()) {
            $isEarlyGoingPaneltFlag = true;
            $message[] = 'Early Going Penalty';
        }

        //warnings
        if(!$isBothPaneltFlag && !$isLateComingPaneltyFlag && !$isEarlyGoingPaneltFlag){
            if ($this->isLateComingPenalty() && !$this->isEarlyGoingPenalty() && !$isLateComingPaneltyFlag ){
                $message[] = (self::$lateComingCountMessage++).' Late Coming Warning';
            } else if (!$this->isLateComingPenalty() && $this->isEarlyGoingPenalty() && !$isEarlyGoingPaneltFlag ){
                $message[] = (self::$earlyGoingCountMessage++).' Early Going Warning';
            } else if($this->isLateComingPenalty() && $this->isEarlyGoingPenalty() && !$isBothPaneltFlag){
                $message[] = (self::$lateComingCountMessage++).' Late Coming & '.(self::$earlyGoingCountMessage++).' Early Going Warning';
            }
        }
        //change total cycle time
        if($this->isCommonCycle) $this->changeCycleCount();
        

        return implode(' and ', $message);
    }
}