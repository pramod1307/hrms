<?php
include ('../../configdata.php');
include ('../../define_email_data.php');
//$define_email_data_obj=new define_email_data();

class attendance_class{
    function __construct(){
      
    }
    function getMyMarkPastRequestData($code){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;

        if($code=='admin')
        {
            $sql="select AttnKey from markPastAttendance group by AttnKey,CreatedOn order by CreatedOn desc";
        }
        else{
          $sql="select AttnKey from markPastAttendance WHERE CreatedBy='$code' group by AttnKey,CreatedOn order by CreatedOn desc";
        }
      // echo $sql;
        $res=query($query,$sql,$pa,$opt,$ms_db);
      // $Levkey_array=array();
        $count=0;
       while ($rows = $fetch($res)){
       $AttnKey_val=$rows['AttnKey'];
      // echo $Levkey_val."aaa";
          $sql1="select TOP 1 markPastId,CreatedBy,CONVERT(VARCHAR(10),date_from, 103) as date_from,CONVERT(VARCHAR(10),date_to, 103) as date_to,
          CONVERT (VARCHAR(27),CreatedOn,109 ) as CreatedOn,approvedBy,
          notMarkingReason,action_status,AttnKey,CONVERT (VARCHAR(27),UpdatedOn,109 ) as UpdatedOn,CONVERT (VARCHAR(27),userUpdatedOn,109 ) as userUpdatedOn,action_remark,user_remarks,actualIn,actualOut,intime,outtime from markPastAttendance WHERE AttnKey='$AttnKey_val' order by markPastId DESC ";
           $res1=query($query,$sql1,$pa,$opt,$ms_db);
      // $Levkey_array=array();
          while ($rows1 = $fetch($res1)){
             //echo $rows1['markPastId'];
            $markPastId[$count]=$rows1['markPastId'];
            $CreatedBy[$count] = $rows1['CreatedBy'];
            $date_from[$count]=$rows1['date_from'];
            $date_to[$count]=$rows1['date_to'];
            $CreatedOn[$count]=$rows1['CreatedOn'];
            $approvedBy[$count]=$rows1['approvedBy'];
            $notMarkingReason[$count]=$rows1['notMarkingReason'];
            $action_status[$count]=$rows1['action_status'];
            $AttnKey[$count]=$rows1['AttnKey'];
            $UpdatedOn[$count]=$rows1['UpdatedOn'];
            $action_remark[$count]=$rows1['action_remark'];
            $user_remarks[$count]=$rows1['user_remarks'];
            $user_UpdatedOn[$count]=$rows1['userUpdatedOn'];
            $actualIn[$count]=$rows1['actualIn'];
            $actualOut[$count]=$rows1['actualOut'];
            $intime[$count]=$rows1['intime'];
            $outtime[$count]=$rows1['outtime'];
            $count++;
          }
       }  
      

      /*return array($markPastId,$CreatedBy,$date_from,$date_to,$CreatedOn,$approvedBy,$notMarkingReason,$action_status,$AttnKey,$UpdatedOn,$action_remark,$user_remarks,$user_UpdatedOn,$count);
*/
      return array($markPastId,$date_from,$date_to,$CreatedOn,$approvedBy,$notMarkingReason,$action_status,$AttnKey,$UpdatedOn,$action_remark,$user_remarks,$user_UpdatedOn,$actualIn,$actualOut,$intime,$outtime,$count);
      //return $sql;
    }

    function getMyODRequestData($code){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;

        //echo $code;
        if($code == 'admin')
        {
          $sql="select oDKey from outOnWorkRequest group by oDKey,CreatedOn order by CreatedOn desc";
        }
        else
        {
          $sql="select oDKey from outOnWorkRequest WHERE CreatedBy='$code' group by oDKey,CreatedOn order by CreatedOn desc";
        }
      // echo $sql;
        $res=query($query,$sql,$pa,$opt,$ms_db);
      // $Levkey_array=array();
        $count=0;
       while ($rows = $fetch($res)){
       $oDKey=$rows['oDKey'];
      // echo $Levkey_val."aaa";
          $sql1="select TOP 1 outWorkId,CreatedBy,CONVERT(VARCHAR(10),date_from , 103) as date_from,CONVERT(VARCHAR(10),date_to, 103) as date_to,CONVERT (VARCHAR(27),CreatedOn,109 ) as CreatedOn,approvedBy,natureOfWork,action_status,oDKey,CONVERT (VARCHAR(27),UpdatedOn,109 ) as UpdatedOn,CONVERT (VARCHAR(27),userUpdatedOn,109 ) as userUpdatedOn,action_remark,user_remarks from outOnWorkRequest WHERE oDKey='$oDKey' order by outWorkId DESC ";
         // echo $sql1;
           $res1=query($query,$sql1,$pa,$opt,$ms_db);
      // $Levkey_array=array();
          while ($rows1 = $fetch($res1)){
             //echo $rows1['markPastId'];
            $outWorkId[$count]=$rows1['outWorkId'];
            $CreatedBy[$count] = $rows1['CreatedBy'];
            $date_from[$count]=$rows1['date_from'];
            $date_to[$count]=$rows1['date_to'];
            $CreatedOn[$count]=$rows1['CreatedOn'];
            $approvedBy[$count]=$rows1['approvedBy'];
            $natureOfWork[$count]=$rows1['natureOfWork'];
            $action_status[$count]=$rows1['action_status'];
            $oDKey1[$count]=$rows1['oDKey'];
            $UpdatedOn[$count]=$rows1['UpdatedOn'];
            $action_remark[$count]=$rows1['action_remark'];
            $user_remarks[$count]=$rows1['user_remarks'];
            $user_UpdatedOn[$count]=$rows1['userUpdatedOn'];
            $count++;
          
          }
       }  
      
      return array($outWorkId,$CreatedBy,$date_from,$date_to,$CreatedOn,$approvedBy,$natureOfWork,$action_status,$oDKey1,$UpdatedOn,$action_remark,$user_remarks,$user_UpdatedOn,$count);
      //return $sql;
    }

}

