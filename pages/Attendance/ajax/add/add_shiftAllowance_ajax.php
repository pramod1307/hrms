<?php

//include "../../db_conn.php";
include ('../../../db_conn.php');
include ('../../../configdata.php');
?>
<style type="text/css">
    ul.multiselect-container.dropdown-menu li:nth-child(1) {
    padding: 0;
}
    ul.multiselect-container.dropdown-menu li {
    padding: 0 0 0 20px;
}
</style>
<form action="#" id="form_sample_1" method="post"  class="form-horizontal" enctype="multipart/form-data">
    <div class="modal-header portlet box blue">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title white-txt"><b>New Shift Allowance Master</b> </h4>
    </div>
    <div class="modal-body " style="max-height: 300px;overflow-y: auto;overflow-x:hidden !important;">
        <div class="form-body">
            <div id='TextBoxesGroup'>
           
                <div class="form-group" id="TextBoxDiv1">
                    <div class="row">
                         <label class="col-md-2 control-label">Select Shift</label>
                            <div class="col-md-3" id="shiftdiv" style="padding-top: 9px;">
                                <select id="shift1" name='shift[]'style="width: 100px">
                                <option value="">Shifts</option>
                                    <?php 
                                        $sql1="select Shift_Code,ShiftMastId,Shift_Name from ShiftMast"; 
                                        $result1 = query($query,$sql1,$pa,$opt,$ms_db);
                                            while($row1 = $fetch($result1)){ ?> 
                                                <option value="<?php echo $row1['Shift_Name'] ?>">
                                                    <?php echo $row1['Shift_Name']; ?>
                                                </option>
                                            <?php }?>
                                </select>
                            </div>
                        
                        <label class="col-md-1 control-label" style="text-align:left;">Amount</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="rates[]" id="rates1" onkeypress="return isNumber(event)" >
                                <input type="hidden" id="hideVal" value='1'>
                            </div>

                         
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <button type="button" id="addButton" class="btn green" style="    margin-left: 546px;" ><i class="fa fa-plus">Add Row</i>
                    </button>
                     <button type="button" id="removeButton" class="btn green" style="display: none"><i class="fa fa-plus">Remove Row</i>
                    </button>
                </div>
               
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn green" id="saveAttAllow" onclick="submitshiftAllow('add');"><i class="fa fa-check"></i>Save
        </button>
    </div>
</form>
<script src="js/shiftAllowance.js"></script>