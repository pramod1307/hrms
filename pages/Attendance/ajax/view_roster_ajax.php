    <?php 
    include('../../db_conn.php');
    include('../../configdata.php');
    include('../../global_class.php');
    include('../Events/weekyOff-v2.php');
    include('../Events/holidays-v2.php');
    @session_start();

    include('../../attendanceClass.php');

    // Check that the class exists before trying to use it
    
    if (class_exists('AttendaceClass')) {
        $obj_attendaceRulesClass = new AttendaceClass();
    }

    if(isset($_SESSION['usercode']) && $_SESSION['usercode'] == '') die("Session Problem");
        
    $username=$_SESSION['usercode'];

    if($_POST['type']=="showroster"){
        //roster listing start here -----------------------------------

        $month = $_POST['month'];
        $year = $_POST['year'];
        $weeklyOffsData = '';

        $startDate = date('Y-m-d', strtotime('first day of this month', strtotime(date($year.'-'.$month.'-01'))));
        $endDate = date('Y-m-d', strtotime('last day of this month', strtotime(date($year.'-'.$month.'-01'))));

        $monthNames = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $currentMonth = date('F',strtotime($monthNames[$month-1].' ' . $year));
        $prevMonth = date('F',strtotime($monthNames[$month-1].' ' . $year . ' -1 month'));      
        $nextMonth = date('F',strtotime($monthNames[$month-1].' ' . $year . ' +1 month'));
        $monthNames = array("prevMonth" => $prevMonth, "nextMonth" => $nextMonth, "currentMonth" => $currentMonth);
    
        $limitStart = (isset($_POST['limitStart']) && !empty($_POST['limitStart'])) ? $_POST['limitStart'] : 0;
        $offsetLimit = (isset($_POST['offsetLimit']) && !empty($_POST['offsetLimit'])) ? $_POST['offsetLimit'] : 10;
        $myteam = array();
        $rosterListType = isset($_POST["rosterListType"])?$_POST["rosterListType"]:'';
        $rosterTypeCondition = "EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";

        if(!empty($rosterListType)){
            switch ($rosterListType) {
                case "assignedRoster":
                    $rosterTypeCondition = "WHERE EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";
                    $rosterTypeConditionWithAnd = " AND EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";
                    break;

                case "unAssignedRoster":
                    $rosterTypeCondition = "WHERE EMP_CODE NOT IN (SELECT EMP_CODE FROM Roster_schema)";
                    $rosterTypeConditionWithAnd = " AND EMP_CODE NOT IN (SELECT EMP_CODE FROM Roster_schema)";
                    break;

                case "allRoster":
                    $rosterTypeCondition = "";
                    $rosterTypeConditionWithAnd = "";
                    break;
            }

        } else {
            $rosterTypeCondition = "WHERE EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";
            $rosterTypeConditionWithAnd = " AND EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";
        }
        
       

        $whereEmp = ($username == 'admin') ? $rosterTypeCondition : "WHERE Emp_Code='$username' ".$rosterTypeConditionWithAnd;
        //---------------------------------filter start here -----------------------------------
        
        $whereCondition = " AND (";
        $whereCondition.=(!empty($_POST['CompanyName']))? " hrdmastqry.COMP_CODE in (".implode(",",$_POST['CompanyName']).") OR" : "";
        $whereCondition.=(!empty($_POST['functionName']))?" hrdmastqry.FUNCT_CODE in (".implode(",",$_POST['functionName']).") OR" : "";
        $whereCondition.=(!empty($_POST['sfunction']))?" hrdmastqry.SFUNCT_CODE in (".implode(",",$_POST['sfunction']).") OR" : "";
        $whereCondition.=(!empty($_POST['grade']))?" hrdmastqry.GRD_CODE in (".implode(",",$_POST['grade']).") OR" : "";
        $whereCondition.=(!empty($_POST['location1']))?" hrdmastqry.LOC_CODE in (".implode(",",$_POST['location1']).") AND" : "";
        $whereCondition.=(!empty($_POST['mname']))?" hrdmastqry.MNGR_CODE in (".implode(",",$_POST['mname']).") OR" : "";
        $whereCondition.=(!empty($_POST['wlocation']))?" hrdmastqry.WLOC_CODE in (".implode(",",$_POST['wlocation']).") OR" : "";
        $whereCondition.=(!empty($_POST['designation']))?" hrdmastqry.DSG_CODE in (".implode(",",$_POST['designation']).") OR" : "";
        $whereCondition.=(!empty($_POST['bu']))?" hrdmastqry.BussCode in (".implode(",",$_POST['bu']).") OR" : "";
        $whereCondition.=(!empty($_POST['sbu']))?" hrdmastqry.SUBBuss_Code in (".implode(",",$_POST['sbu']).") OR" : "";
        $whereCondition.=(!empty($_POST['employeeType']))?" hrdmastqry.TYPE_CODE in (".implode(",",$_POST['employeeType']).") OR" : "";
        $whereCondition.=(!empty($_POST['process']))?" hrdmastqry.PROC_CODE in (".implode(",",$_POST['process']).") OR" : "";
        $whereCondition.=(!empty($_POST['ccenter']))?" hrdmastqry.COST_CODE in (".implode(",",$_POST['ccenter']).") OR" : "";
        $whereCondition.=(!empty($_POST['division']))?" hrdmastqry.Divi_Code in (".implode(",",$_POST['division']).") OR" : "";
        
        $whereCondition = rtrim($whereCondition," OR");
        $whereCondition.=")";

        if($whereCondition == " AND ()"){
            $whereCondition = "";
        }
        //---------------------------------filter end here -------------------------------------

        $whereEmp = $whereEmp.$whereCondition;
    
        $sql="SELECT Emp_Code,EmpImage,Emp_FName,Emp_Title,Emp_MName,Emp_LName,DSG_NAME FROM ( SELECT ROW_NUMBER() OVER(ORDER BY EMP_CODE) AS ROWNUM, * FROM HrdMastQry $whereEmp ) HrdMastQry1 WHERE ROWNUM BETWEEN $limitStart AND $offsetLimit ";
        

      

        $result=query($query, $sql, $pa, $opt, $ms_db);
        
        $resultNew=query($query, $sql, $pa, $opt, $ms_db);
        


        $row3=$fetch($result);
        


        $myteam['id']=$row3['Emp_Code'];
        $myteam['Fname']=$row3['Emp_FName'];
        $myteam['name']=$row3['Emp_Title'] . " " . $row3['Emp_FName'] . " " . $row3['Emp_MName'] . " " . $row3['Emp_LName'];
        
        if($row3['EmpImage']==""||$row3['EmpImage']=="NULL" ){
            $myteam['image']= "../Profile/upload_images/images (1).jpg";
        }
        else{
            $myteam['image']= "../Profile/upload_images/".$row3['EmpImage'];
        }

        $myteam['dsg'] = $row3['DSG_NAME'];
        $myteam['children'] = array();

        if($username != 'admin'){

            $rosterTypeCondition = "EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";
            if(!empty($rosterListType)){
                if($rosterListType == "assignedRoster"){
                    $rosterTypeCondition = "WHERE EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";
                    $rosterTypeConditionWithAnd = " AND EMP_CODE IN (SELECT EMP_CODE FROM Roster_schema)";
                }else if ($rosterListType == "unAssignedRoster") {
                    $rosterTypeCondition = "WHERE EMP_CODE NOT IN (SELECT EMP_CODE FROM Roster_schema)";
                    $rosterTypeConditionWithAnd = " AND EMP_CODE NOT IN (SELECT EMP_CODE FROM Roster_schema)";
                }else if ($rosterListType == "allRoster") {
                    $rosterTypeCondition = "";
                }
            }
             //echo '<pre>'; print_r($_SESSION['selectedRole']);
            //$sql4="SELECT * FROM HrdMastQry where MNGR_CODE='$username' ";
            
            $whereMngr = "WHERE MNGR_CODE='$username' ".$rosterTypeConditionWithAnd;
            
            
            /*if($username == 'admin')
            $sql4="SELECT Emp_Code,EmpImage,Emp_FName,Emp_Title,Emp_MName,Emp_LName,DSG_NAME FROM ( SELECT ROW_NUMBER() OVER(ORDER BY EMP_CODE) AS ROWNUM, * FROM HrdMastQry $whereMngr) HrdMastQry2 WHERE ROWNUM BETWEEN $limitStart AND $offsetLimit";
            else
            $sql4="SELECT Emp_Code,EmpImage,Emp_FName,Emp_Title,Emp_MName,Emp_LName,DSG_NAME FROM ( SELECT ROW_NUMBER() OVER(ORDER BY EMP_CODE) AS ROWNUM, * FROM HrdMastQry $whereMngr) HrdMastQry2 WHERE ROWNUM BETWEEN $limitStart AND $offsetLimit";*/
            
            $sql4="SELECT Emp_Code,EmpImage,Emp_FName,Emp_Title,Emp_MName,Emp_LName,DSG_NAME FROM ( SELECT ROW_NUMBER() OVER(ORDER BY EMP_CODE) AS ROWNUM, * FROM HrdMastQry $whereMngr) HrdMastQry2 WHERE ROWNUM BETWEEN $limitStart AND $offsetLimit";

                
            $result4=query($query, $sql4, $pa, $opt, $ms_db);

            while($row2=$fetch($result4)){
                if($username==$row2['Emp_Code']){
                    continue;
                }
                if($row2['EmpImage']==""||$row2['EmpImage']=="NULL" ){
                    $image= "../Profile/upload_images/images (1).jpg";
                }
                else{
                    $image= "../Profile/upload_images/".$row2['EmpImage'];
                }
                $myteam['children'][$row2['Emp_Code']] = array(
                    "Fname" => $row2['Emp_FName'],
                    "name" => $row2['Emp_Title'] . " " . $row2['Emp_FName'] . " " . $row2['Emp_MName'] . " " . $row2['Emp_LName'],
                    "image" =>$image,
                    "dsg" => $row2['DSG_NAME']);
               //$team['myteam1'][]['email'] = $row2['OEMailD'];
            }       

        }else{
            while($rowNew=$fetch($resultNew)){
                if($username==$rowNew['Emp_Code']){
                    continue;
                }
                if($rowNew['EmpImage']==""||$rowNew['EmpImage']=="NULL" ){
                    $image= "../Profile/upload_images/images (1).jpg";
                }
                else{
                    $image= "../Profile/upload_images/".$rowNew['EmpImage'];
                }
                $myteam['children'][$rowNew['Emp_Code']] = array(
                    "Fname"     => $rowNew['Emp_FName'],
                    "name"      => $rowNew['Emp_Title'] . " " . $rowNew['Emp_FName'] . " " . $rowNew['Emp_MName'] . " " . $rowNew['Emp_LName'],
                    "image"     => $image,
                    "dsg"       => $rowNew['DSG_NAME']);
            }
        }
        
        //echo '<pre>'; print_r($myteam); echo '</pre>';

        if(!empty($myteam['id']))
        $allEmployee[] = $myteam['id'];
        else
        $allEmployee = '';
    
    
        foreach ($myteam['children'] as $id => $TDA) {
            if(!empty($id)) { $allEmployee[] = $id; }
        }

        $date = $year."-".$month."-".'01';
        $date1 = $year."-01-".'01';
        $date2 = $year."-12-".'31';
        $numberofdays = get_number_of_days_in_month($month,$year);
        $date2 =   $year.'-'.$month."-".$numberofdays ;

        //find employee auto shift and upload into roster schema
        $globalClassObj =   new global_class();
        //$globalClassObj->uploadAutoshift( $allEmployee, $month, $year );
        
        //get employee payroll cycle
        $payrollCycles = array();
        $payrollCycles = $globalClassObj->getEmployeePayrollCycle( $allEmployee );
        $holidaysList = array();
        
        $useForRestrictPerPayrollCount      =    implode(",",$allEmployee); // This variable is use for restrict payroll count
            foreach($allEmployee AS $empCodeValue){
            $weeklyOffs     = getWeeklyOff($empCodeValue,$startDate,$endDate);  
            if(isset($weeklyOffs) && count($weeklyOffs) > 0){
                foreach ($weeklyOffs as $key => $value) {
                    if (isset($value['start'])) {
                        $weeklyOffDate = date('d/m/Y',strtotime($value['start']));
                        $weeklyOffsData[$empCodeValue][$weeklyOffDate] = $weeklyOffDate;
                    }
                }
            }
            $holidaysData   = getHolidays($startDate,$endDate,$empCodeValue);
            if(isset($holidaysData) && count($holidaysData) > 0){
                foreach ($holidaysData as $key => $value) {
                    $holidayDate = date('d/m/Y',strtotime($value['start']));
                    $holidaysList[$empCodeValue][$holidayDate] = array('title'=>$value['title'],'start'=>$holidayDate);
                }
            }
        }
        
        $employee = "'".implode("','",$allEmployee)."'";
        //Auto shift query start
        $autoShiftData = array();
        if ($rosterListType == "unAssignedRoster") {
            $autoShiftQuery = "SELECT EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)  AS ShiftDate, DBO.fnGetShift(EMP_CODE,CONVERT(VARCHAR(10),PunchDate,20)) AS SHIFT FROM ATTENDANCEALL WHERE EMP_CODE IN (".$employee.") AND MONTH(PunchDate) = '".$month."' AND YEAR(PunchDate) = '".$year."' GROUP BY EMP_CODE, CONVERT(VARCHAR(10),PunchDate,20)";
            $autoShiftResult = query($query, $autoShiftQuery, $pa, $opt, $ms_db);
            if($num($autoShiftResult) > 0) {
                while ($rowAutoShift = $fetch($autoShiftResult)) {
                    if(isset($rowAutoShift['EMP_CODE']) && !empty($rowAutoShift['EMP_CODE'])){
                        $empCodeAS = trim($rowAutoShift['EMP_CODE']);
                        $autoShiftData[$empCodeAS][date('j',strtotime($rowAutoShift['ShiftDate']))] = $rowAutoShift['SHIFT'];
                    }                   
                }
            }
        }
        //end
        $sqlD1 = "select max(RostID) as RostID from Roster_schema where Emp_Code IN ($employee) and end_rost <= '$date' group by Emp_Code";
        
        $sqlD1 = "select a.Emp_Code,convert(varchar(10),a.start_rost,103) as start_rost,convert(varchar(10),a.end_rost,103) as end_rost,a.RosterName,a.auto_period,a.created_on from Roster_schema a inner join ($sqlD1) b on a.RostID = b.RostID";
        
        $sqlD2 = "select Emp_Code, convert(varchar(10),start_rost,103) as start_rost,convert(varchar(10),end_rost,103) as end_rost,RosterName,auto_period,created_on from Roster_schema where Emp_Code IN ($employee) and start_rost >= '$date' and start_rost <= '$date2' ";
        
        $sqlD = " ($sqlD1) union ($sqlD2)";
        //----------------------------
        $sqlD = "select * from ($sqlD) a order by a.created_on asc";
        //----------------------------
        
        //$sqlD = "SELECT EMP_CODE as Emp_Code, convert(varchar(10),RosterStart,103) as start_rost,convert(varchar(10),RosterEnd,103) as end_rost,RosterName,auto_period FROM ROSTERQRY WHERE EMP_CODE IN($employee) AND RosterStart BETWEEN '$date' AND '$date2'";
        $sqlD = "SELECT RS.RostID, RS.EMP_CODE, RS.RosterName,convert(varchar(10), RS.start_rost,103) AS RosterStart,convert(varchar(10),RS.end_rost,103) AS [RosterEnd],RC.[type_name],
CASE WHEN RS.start_rost=RC.RosterDate AND RC.[type_name]='approve' and MONTH('".$date2."')=MONTH(RC.RosterDate) 
THEN 
RC.Shift_Code ELSE SM.Shift_Code END Shift_Code, RS.auto_period

FROM ROSTER_SCHEMA RS
LEFT OUTER JOIN ShiftChangeqry RC ON RS.start_rost=RC.RosterDate AND RC.Emp_code=RS.Emp_Code 
INNER JOIN ATT_ROSTER AR ON RS.RosterName=AR.roster AND RS.start_rost=AR.RosterDate
INNER JOIN ShiftMast SM ON  AR.shiftMaster=SM.ShiftMastId 
INNER JOIN ShiftPatternMast SP ON AR.shiftPattern=SP.ShiftPatternMastid 
WHERE RS.auto_period=1 and start_rost IN 
(
      SELECT MAX(start_rost) fROM Roster_schema 
      WHERE end_rost <= '".$date2."' AND Roster_schema.Emp_Code IN($employee)
      AND   ROSTER_SCHEMA.auto_period=1 AND Roster_schema.RosterName LIKE 'rost-upload%'
      GROUP BY DAY(start_rost),EMP_CODE

)
AND RS.Emp_Code IN($employee) AND RS.RosterName LIKE 'rost-upload%'
ORDER BY RS.start_rost";
        //echo $sqlD;  die;
        
        $resultD = query($query, $sqlD, $pa, $opt, $ms_db);
        $data = array();
        $scheme = array();
        $i = 0;
        while($rowD = $fetch($resultD)){
            $monthp='/'.$month.'/';
            $fordatep = str_replace('/', '-', $rowD['RosterStart']);
            $startD= date('m', strtotime($fordatep));
            //echo $startD = date("f", strtotime())."aaaa";
             $endp = str_replace('/', '-', $rowD['RosterEnd']);
            $endD= date('m', strtotime($endp));
            $t='/'.$startD.'/';
            //$endD = date("m", strtotime());
             $st_date = str_replace($t, $monthp , $rowD['RosterStart']);
            //die;
            $t1='/'.$endD.'/';
            $en_date = str_replace($t1, $monthp , $rowD['RosterEnd']);   
            $rowD['Emp_Code'] = trim($rowD['EMP_CODE']);
            $data[$rowD['Emp_Code']][] = array("totalDay"=>$numberofdays, "actionMonth"=>$month ,"startdate" =>$st_date , "enddate" => $en_date, "RosterName" => $rowD['RosterName'], "auto" => $rowD['auto_period']);
            $scheme[$rowD['RosterName']] = '';
        }
        //echo '<pre>'; print_r($scheme); die;
        $SSN = array();
        foreach($scheme as $SN => $val){
            $SSN[] = $SN;
        }
        
        $schemeN = "'".implode("','",$SSN)."'";


         $sqlS = "select c.*,d.ShiftPattern_Name from (select a.*,b.Shift_Name,b.Shift_Code, cast(b.Shift_From as varchar(8)) as st ,
        cast(b.Shift_To as varchar(8)) as en from (select convert(varchar(10),RosterDate,103) as RosterDate,
        shiftMaster,shiftPattern,auto_period,roster from att_roster where roster IN ($schemeN)) as a join ShiftMast b 
        on a.shiftMaster=b.ShiftMastId) c join ShiftPatternMast d on c.shiftPattern=d.ShiftPatternMastid";
        $resultS = query($query, $sqlS, $pa, $opt, $ms_db);
        $ss = array();
        while($rowS = $fetch($resultS)){
            $ss[$rowS['roster']][$rowS['RosterDate']] = array("SM" => array("id" => $rowS['shiftMaster'], "name"=>$rowS['Shift_Name'], "shift_code"=>$rowS['Shift_Code'],"start"=>$rowS['st'],"end"=>$rowS['en']),"SP" => array("id"=>$rowS['shiftPattern'], "name"=>$rowS['ShiftPattern_Name']),"auto_period" => $rowS['auto_period']);
            
        }
        
        $changed = array();
        $rq = array();
        $sq = array();
    
        $sqlD = "select convert(varchar(50),created_on,100) as created_on,convert(varchar(10),RosterDate,103) as RosterDate,Emp_code,shiftMaster,shiftPattern,type_name,swap_id,CreatedBy,oldShiftMaster from rost_change where Emp_code IN ($employee) and RosterDate between '$date1' AND '$date2' ORDER BY created_on DESC";
        $resultD = query($query, $sqlD, $pa, $opt, $ms_db); 
        while($rowD = $fetch($resultD)){

            //get shift name
            $sqlDNew = "SELECT Shift_Code,Shift_Name FROM ShiftMast WHERE ShiftMastId = '".$rowD['shiftMaster']."'";
            $resultDNew = query($query, $sqlDNew, $pa, $opt, $ms_db);
            $rowDNew = $fetch($resultDNew);

            $changed[$rowD['Emp_code']][$rowD['RosterDate']] = array("createdDate"=>$rowD['created_on'],"OSM"=>$rowD['oldShiftMaster'],"SM" => $rowD['shiftMaster'],"SP" => $rowD['shiftPattern'],"type_name" => $rowD['type_name'],'shift_code'=>$rowDNew['Shift_Code'],'Shift_Name'=>$rowDNew['Shift_Name'],'ApprovedID'=>$rowD['CreatedBy']);
            if($rowD['type_name'] == "request"){
                $rq[$rowD['Emp_code']][$rowD['RosterDate']] = array("createdDate"=>$rowD['created_on'],"OSM"=>$rowD['oldShiftMaster'],"SM" => $rowD['shiftMaster'],"SP" => $rowD['shiftPattern'],"type_name" => $rowD['type_name'],'shift_code'=>$rowDNew['Shift_Code'],'Shift_Name'=>$rowDNew['Shift_Name']);
            }
            if($rowD['type_name'] == "swapR"){
                $sq[$rowD['Emp_code']][$rowD['swap_id']][$rowD['RosterDate']] = array("createdDate"=>$rowD['created_on'],"OSM"=>$rowD['oldShiftMaster'],"SM" => $rowD['shiftMaster'],"SP" => $rowD['shiftPattern'],"type_name" => $rowD['type_name'],'shift_code'=>$rowDNew['Shift_Code'],'Shift_Name'=>$rowDNew['Shift_Name']);
            }
        }
        
        echo json_encode(array("useForRestrictPerPayrollCount"=>$useForRestrictPerPayrollCount,"data"=> $data, "numberofdays" => $numberofdays, "details" => $myteam, "scheme" => $ss,"changeD"=>$changed,"rq" => $rq, "sq" => $sq,"monthNames" => $monthNames,"holidays"=>$holidaysList,"weeklyOffsData" => $weeklyOffsData, 'autoShiftData' => $autoShiftData, "payrollCycles" => $payrollCycles));
    
        //roster listing end here ---------------------------------------

    } else if($_POST['type'] == "showroster_requests") {
        $sql="SELECT rost_change.ID, rost_change.shiftMaster, rost_change.shiftPattern, CONVERT(VARCHAR(19), rost_change.rosterDate, 120) AS rosterDate, CONVERT(VARCHAR(19), rost_change.created_on, 120) AS createdOn,
        CONVERT(VARCHAR(8), ShiftMast.Shift_From, 24) AS Shift_From, CONVERT(VARCHAR(8), ShiftMast.Shift_To, 24) AS Shift_To, ShiftPatternMast.ShiftPattern_Name
        from rost_change 
        INNER JOIN ShiftMast ON ShiftMast.ShiftMastId = rost_change.shiftMaster
        INNER JOIN ShiftPatternMast ON ShiftPatternMast.ShiftPatternMastid = rost_change.shiftPattern
        where rost_change.Emp_code = '$username' ";
        $result=query($query, $sql, $pa, $opt, $ms_db);
        if($num($result) > 0){ 
            while($rowD = $fetch($result)){
                $resultRow[] = array(
                    "ID" => $rowD["ID"],
                    "rosterDate" => dateFormat($rowD["rosterDate"]),
                    "Shift_From" => timeFormat($rowD["Shift_From"]),
                    "Shift_To" => timeFormat($rowD["Shift_To"]),
                    "ShiftPattern_Name" => $rowD["ShiftPattern_Name"],
                    "createdOn" => dateTimeFormat($rowD["createdOn"])
                    );
            }
            echo json_encode($resultRow);
        }else{
            echo json_encode(array());
        }
        exit();

    }
    elseif($_POST['type']=="subrost") {
        $empid=$_POST['eid'];
        $year=$_POST['year'];
        $month=$_POST['month'];
        $day=$_POST['day'];
        $sm=$_POST['sm'];
        $sp = $_POST['sp'];
        $SMid_old = $_POST['SMid_old'];
        $rtype = trim($_POST['LT']);
        $d=strlen($day);
        if($d==1){
            $da="0".$day;
        }else{
            $da=$day;
        }
        $rostdate=$year."-".$month."-".$da ;
        
        //When manager cancel roster change request
        if($rtype == 'cancel'){
            $tomorrow = date('Y-m-d',strtotime($rostdate . "+1 days"));
            
            //find rostername for delete att roster
            $sqlRosterGet = "SELECT RosterName FROM Roster_schema WHERE Emp_Code = '".$empid."' AND start_rost = '".$tomorrow."' AND end_rost = '".$tomorrow."'";
            $resultRosterGet        = query($query, $sqlRosterGet, $pa, $opt, $ms_db);
            $fetchRosterGet         = $fetch($resultRosterGet);
            $removableRosterName    = $fetchRosterGet['RosterName'];

            //delete att roster
            $sqlDeleteAttRoster = "DELETE FROM att_roster WHERE roster = '".$removableRosterName."' AND RosterDate = '".$tomorrow."'";

            //delete roster schema
            $sqlDeleteRosterSchema = "DELETE FROM Roster_schema WHERE RosterName = '".$removableRosterName."' AND Emp_Code = '".$empid."' AND start_rost = '".$tomorrow."' AND end_rost = '".$tomorrow."'";

            //delete rost change
            $sqlDeleteRostChange = "DELETE FROM rost_change WHERE Emp_code = '".$empid."' and RosterDate = '".$rostdate."'";

            //Delete from database
            query($query, $sqlDeleteAttRoster, $pa, $opt, $ms_db);
            query($query, $sqlDeleteRosterSchema, $pa, $opt, $ms_db);
            query($query, $sqlDeleteRostChange, $pa, $opt, $ms_db);

            /**
            * Start trigger for update out on duty event
            * **/
            $obj_attendaceRulesClass->initializeObject(array('empCode' => $empid, 'todayDateTime' => date('Y-m-d', strtotime($rostdate))));  
           /*
            *End here
            */


            echo 4;
            die;
        }

        $isWorkFlowAutomaticQuery = "SELECT TOP 1 AppMethod FROM WorkFlow WHERE WFFor = 'Attendance' AND AppMethod = 'automatic' ORDER BY WorkFlowID DESC";
        $isWorkFlowAutomaticResult = query($query, $isWorkFlowAutomaticQuery, $pa, $opt, $ms_db);
        $isAutomaticWorkflow = False;
        if ($num($isWorkFlowAutomaticResult) > 0) {
            $isWorkFlowAutomaticRow = $fetch($isWorkFlowAutomaticResult);
            if (strtolower($isWorkFlowAutomaticRow['AppMethod']) == 'automatic') {
                $isAutomaticWorkflow = TRUE;
            }
        }
        if ($rtype == 1 || $rtype == 'approve' || $isAutomaticWorkflow) {
            $rosterFlag = TRUE;
        } else {
            $rosterFlag = FALSE;
        }
            
        //Check request roster of employee in request month
        if(!$rosterFlag){
            
            //$checkExistRosterQuery = "SELECT COUNT(*) AS count FROM rost_change WHERE DATEPART(yyyy,RosterDate) = '".$year."' AND DATEPART(mm,RosterDate) = '".$month."' AND Emp_code = '".$empid."' AND CreatedBy= '".$empid."'";
            $globalClassObj =   new global_class();
            $payrollCycles = array();
            
            $payrollCycles = $globalClassObj->getEmployeePayrollCycle($empid);
            
            
            if($day <= $payrollCycles[$empid]['endDate']){
                 $Startdate = $year."-".$month."-".$payrollCycles[$empid]['startDate']; 
                 $Startdate = date("Y-m-d", strtotime( "-1 month", strtotime( $Startdate ) ));
                 $EndDate =   $year.'-'.$month."-".$payrollCycles[$empid]['endDate'] ;
            }else{
                    $Startdate = $year."-".$month."-".$payrollCycles[$empid]['startDate']; 
                    $EndDate =   $year.'-'.$month."-".$payrollCycles[$empid]['endDate'] ;
                    $EndDate = date("Y-m-d", strtotime( "+1 month", strtotime( $EndDate ) ));
            }
            
            $checkExistRosterQuery = "SELECT COUNT(*) AS count FROM rost_change WHERE  RosterDate between '".$Startdate."' AND '".$EndDate."' AND Emp_code = '".$empid."' AND CreatedBy= '".$empid."'";
            $checkExistRosterResult = query($query, $checkExistRosterQuery, $pa, $opt, $ms_db);
            $existRosterResult = $fetch($checkExistRosterResult);

            if($existRosterResult['count'] >= 2){
                echo 3;
                exit();
            }
        }
        
        $sqlC = "select * from rost_change where Emp_code = '$empid' and RosterDate = '$rostdate' /*and type_name = 'request'*/";
        $resultC = query($query, $sqlC, $pa, $opt, $ms_db);
        $numrow = $num($resultC); 
        $logedInUserId  =   $_SESSION['usercode'];
        
        if($numrow > 0){
            $deleteNonAprroveRequest = "DELETE FROM rost_change WHERE Emp_code = '".$empid."' AND type_name = 'request' AND RosterDate = '$rostdate'";
            @query($query, $deleteNonAprroveRequest, $pa, $opt, $ms_db);
            //update
            $sql="update rost_change SET shiftMaster ='$sm' , shiftPattern ='$sp' , type_name ='$rtype' where Emp_code = '$empid' and RosterDate = '$rostdate'";
            // insert 
        }else{
            $deleteNonAprroveRequest = "DELETE FROM rost_change WHERE Emp_code = '".$empid."' AND RosterDate = '$rostdate'";
            @query($query, $deleteNonAprroveRequest, $pa, $opt, $ms_db);
            if($rosterFlag){ $rtype = 'approve'; }
            $sql="insert into rost_change(Emp_code,shiftMaster,shiftPattern,RosterDate,type_name,CreatedBy,oldShiftMaster) values ('$empid','$sm','$sp','$rostdate','$rtype','$logedInUserId','$SMid_old')";
            //$sql="insert into rost_change(Emp_code,shiftMaster,shiftPattern,RosterDate,type_name) values ('$empid','$sm','$sp','$rostdate','approve')";
        }
        
        

    //echo$sql;
        $result = query($query, $sql, $pa, $opt, $ms_db);
        if($result){            
            
            $checkAutoShiftQuery = "SELECT RosterName FROM Roster_schema WHERE start_rost = '" . $rostdate . "' AND Emp_Code =  '" . $empid . "' AND auto_period = 0 ORDER BY end_rost DESC";
            $checkAutoShiftQueryResult = query($query, $checkAutoShiftQuery, $pa, $opt, $ms_db);
            $checkAutoShiftQueryResultRows = $num($checkAutoShiftQueryResult);

            if ($checkAutoShiftQueryResultRows > 0) {
                while ($checkAutoShiftQueryRow = $fetch($checkAutoShiftQueryResult)) {
                    $deleteRequestRosterName = $checkAutoShiftQueryRow['RosterName'];
                    $deleteAttRoster = "DELETE FROM att_roster WHERE roster = '".$deleteRequestRosterName."'";
                    $deleteRosterSchemaRoster = "DELETE FROM Roster_schema WHERE RosterName = '".$deleteRequestRosterName."'";
                    @query($query, $deleteAttRoster, $pa, $opt, $ms_db);
                    @query($query, $deleteRosterSchemaRoster, $pa, $opt, $ms_db);
                }
            }
            
            if($rosterFlag){                
                $rosterName = "Ros-". uniqid() . rand(1, 999999999999);
                $currentDateTime = date('Y-m-d h:i:s');
                
                $attRosterQuery =   "INSERT INTO att_roster( roster,shiftMaster,shiftPattern,RosterDate,auto_period,created_on,updated_on) values ('".$rosterName."', '".$sm."', '".$sp."', '".$rostdate."','0','".$currentDateTime."','".$currentDateTime."')";

                $rosterSchemaQuery  =   "INSERT INTO Roster_schema(RosterName, Emp_Code , created_on, updated_on, start_rost, end_rost, auto_period,auto_period_flag) values ('".$rosterName."', '".$empid."','".$currentDateTime."','".$currentDateTime."','".$rostdate."','".$rostdate."', '0', '1')";

                query($query, $attRosterQuery, $pa, $opt, $ms_db);
                query($query, $rosterSchemaQuery, $pa, $opt, $ms_db);
            }

            /**
            * Start trigger for update out on duty event
            * **/
                $obj_attendaceRulesClass->initializeObject(array('empCode' => $empid, 'todayDateTime' => date('Y-m-d', strtotime($rostdate))));  
           /*
            *End here
            */

            echo 1;
        }
        else{
            echo 2;
        }
    //echo $sql;
        
    }elseif($_POST['type']=="subswitch"){
        $data = $_POST['data'];
        $LT = $_POST['LT'];
        $empid = $_POST['id'];
        $randval = $_POST['randomVar'];
        
        if($LT == 'request'){ 
            $LT = 'swapR';
        }


        foreach($data as $day => $RA){
            $rostdate = $RA['year']."-".$RA['month']."-".$RA['day'];
            $sp = $RA['SP'];
            $sm = $RA['SM'];
            
            $sqlC = "select * from rost_change where Emp_code = '$empid' and RosterDate = '$rostdate'";
            $resultC = query($query, $sqlC, $pa, $opt, $ms_db);
            $numrow = $num($resultC); 
            
            if($numrow > 0){
                //update
                $sql="update rost_change SET shiftMaster ='$sm' , shiftPattern ='$sp' , type_name ='$LT',swap_id = '$randval' where Emp_code = '$empid' and RosterDate = '$rostdate'";
                
                // insert 
            }else{
                //Check request roster of employee in request month
                $checkExistRosterQuery = "SELECT COUNT(*) AS count FROM rost_change WHERE DATEPART(yyyy,RosterDate) = '".$RA['year']."' AND DATEPART(mm,RosterDate) = '".$RA['month']."' AND Emp_code = '".$empid."'";
                $checkExistRosterResult = query($query, $checkExistRosterQuery, $pa, $opt, $ms_db);
                $existRosterResult = $fetch($checkExistRosterResult);

                if($existRosterResult['count'] >= 2){
                    echo 3;
                    exit();
                }
                $sql="insert into rost_change(Emp_code,shiftMaster,shiftPattern,RosterDate,type_name,swap_id) values ('$empid','$sm','$sp','$rostdate','$LT','$randval')";
            }
            $result = query($query, $sql, $pa, $opt, $ms_db);
            
        } 
        if($result){
            echo 1;
        }
        else{
            echo 2; 
        }    
    }

    function get_number_of_days_in_month($month, $year) {
        // Using first day of the month, it doesn't really matter
        $date = $year."-".$month."-1";
        return date("t", strtotime($date));
    }



    function advanceSearchOption(){
                                
$whereCondition = "";
$whereCondition.=(!empty($_POST['CompanyName']))? " hrdmastqry.COMP_CODE in (".implode(",",$_POST['CompanyName']).") AND" : "";
$whereCondition.=(!empty($_POST['functionName']))?" hrdmastqry.FUNCT_CODE in (".implode(",",$_POST['functionName']).") AND" : "";
$whereCondition.=(!empty($_POST['sfunction']))?" hrdmastqry.SFUNCT_CODE in (".implode(",",$_POST['sfunction']).") AND" : "";
$whereCondition.=(!empty($_POST['grade']))?" hrdmastqry.GRD_CODE in (".implode(",",$_POST['grade']).") AND" : "";
$whereCondition.=(!empty($_POST['location1']))?" hrdmastqry.LOC_CODE in (".implode(",",$_POST['location1']).") AND" : "";
$whereCondition.=(!empty($_POST['mname']))?" hrdmastqry.MNGR_CODE in (".implode(",",$_POST['mname']).") AND" : "";
$whereCondition.=(!empty($_POST['wlocation']))?" hrdmastqry.WLOC_CODE in (".implode(",",$_POST['wlocation']).") AND" : "";
$whereCondition.=(!empty($_POST['designation']))?" hrdmastqry.DSG_CODE in (".implode(",",$_POST['designation']).") AND" : "";
$whereCondition.=(!empty($_POST['bu']))?" hrdmastqry.BussCode in (".implode(",",$_POST['bu']).") AND" : "";
$whereCondition.=(!empty($_POST['sbu']))?" hrdmastqry.SUBBuss_Code in (".implode(",",$_POST['sbu']).") AND" : "";
$whereCondition.=(!empty($_POST['employeeType']))?" hrdmastqry.TYPE_CODE in (".implode(",",$_POST['employeeType']).") AND" : "";
$whereCondition.=(!empty($_POST['process']))?" hrdmastqry.PROC_CODE in (".implode(",",$_POST['process']).") AND" : "";
$whereCondition.=(!empty($_POST['ccenter']))?" hrdmastqry.COST_CODE in (".implode(",",$_POST['ccenter']).") AND" : "";
$whereCondition.=(!empty($_POST['division']))?" hrdmastqry.Divi_Code in (".implode(",",$_POST['division']).") AND" : "";
$whereCondition.=(!empty($_POST['employee']))?" hrdmastqry.Emp_Code in (".implode(",",$_POST['employee']).") AND" : "";

return rtrim($whereCondition,"AND");
        

    }

?>