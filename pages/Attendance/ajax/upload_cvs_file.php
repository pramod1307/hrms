<?php

session_start();
ini_set('MAX_EXECUTION_TIME', -1);
include ('../../db_conn.php');
include ('../../configdata.php');
include ('../../global_class.php');

$global_obj = new global_class();

include('../../attendanceClass.php');

if (class_exists('AttendaceClass')) {
    $obj_attendaceRulesClass = new AttendaceClass();
}

function exportCSV($headings = false, $rows = false, $filename = false) {
    # Ensure that we have data to be able to export the CSV
    if ((!empty($headings)) AND ( !empty($rows))) {
        # modify the name somewhat
        $name = ($filename !== false) ? $filename . ".csv" : "export.csv";

        # Set the headers we need for this to work
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $name);

        # Start the ouput
        $output = fopen('php://output', 'w');

        # Create the headers
        fputcsv($output, $headings);

        # Then loop through the rows
        foreach ($rows as $row) {
            # Add the rows to the body
            fputcsv($output, $row);
        }

        # Exit to close the stream off
        exit();
    }

    # Default to a failure
    return false;
}

function validateDate($date, $format = 'y-M-j') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function validateDateTime($date, $format = 'd-M-Y H:i:s') {
    /* $d = DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) == $date; */
    $date = date('Y-m-d H:i:s', strtotime($date));
    return (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', $date, $m1)) ? true : false;
}

//echo "1";die;
if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'error_log') {
    if (isset($_SESSION['AttendanceUpload_csv']) && !empty($_SESSION['AttendanceUpload_csv'])) {
        $list = $_SESSION['AttendanceUpload_csv']['result'];
        $heading = $_SESSION['AttendanceUpload_csv']['heading'];
        exportCSV($heading, $list, 'AttendanceUpload_csv');
    }
} else {
    //echo "entered";die;

    $total = 6;
    $tempVariable = 1;
    $employeeIds = "-15";

    $_SESSION['AttendanceUpload_csv'] = '';
    $output = "";
    $filename = $_FILES["file"]["tmp_name"];

    if ($_FILES["file"]["size"] > 0) {

        $_SESSION['AttendanceUpload_csv']['heading'] = array("Date", "Card No", "Emp Code", "In Punch", "Out Punch", "Latecoming", "Result Status");
        $file = fopen($filename, "r");
        fgetcsv($file, 1000, ",");  // pop the headers
        //$uninsert=0;
        $uninsert1 = 0;
        $row = 0;
        $cars = array();
        $temp1 = 0;
        $temp2 = 0;
        $temp3 = 0;
        $uninsert = array();
        $success = array();
        $error = 0;
        //echo "fdfddg";
        $i = 1;
        //$statusMast		=	"select * from statusMast";
        //$res=query($query,$statusMast,$pa,$opt,$ms_db);
        $number = $num($res);
        // if($number >0)
        // {
        // 	while($data=$fetch($res)) {
        // 		$key 					=	$data['Status_Code'];
        // 		$statusOfEmployee[$key] = 	strtoupper($data['Status_Name']);
        // 	}	
        // }
        $i = 0;
        while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE) {
            //echo '<pre>'; print_r($emapData);
            $x = o;
            $numcols = count($emapData);
            //echo $numcols." ".$total."  ";
            if ($numcols != $total) {
                echo "Uploaded template must be same as downloaded blank format!";
                exit;
            } else {
                //echo "enter1";exit;
                if (!empty($emapData)) {
                    //print_r($emapData[0]);
                    if ($emapData[0] != '') {
                        //echo"enter3";
                        //$checkError	=	var_dump(validateDate($emapData[0]));
                        //echo $checkError;
                        if (!validateDate($emapData[0])) {
                            $uninsert[$tempVariable][0] = "Date is not in valid format.it should be (dd-mmm-yy)";
                            $error = 1;
                        }
                    } else {
                        $uninsert[$tempVariable][1] = "Date is mandatory";
                        $error = 1;
                    }

                    if ($emapData[1] != '') {
                        //print_r($emapData[1]);
                        $sql = "select * from HrdMastQry where accessID ='" . trim($emapData[1]) . "' and Status_code = 01";
                        $res = query($query, $sql, $pa, $opt, $ms_db);
                        $number = $num($res);
                        //echo $number;
                        if ($number == 0) {
                            $uninsert[$tempVariable][2] = "Card Number does not exist or not Activated";
                            $error = 1;
                        }
                    } else {
                        $uninsert[$tempVariable][2] = "Card Number is mandatory";
                        $error = 1;
                    }

                    if ($emapData[2] != '') {
                        //print_r($emapData[1]);
                        $sql = "select * from HrdMastQry where Emp_code ='" . trim($emapData[2]) . "' and Status_code = 01";
                        $res = query($query, $sql, $pa, $opt, $ms_db);
                        $number = $num($res);
                        //echo $number;
                        if ($number == 0) {
                            $uninsert[$tempVariable][2] = "Employee Code does not exist or not Activated";
                            $error = 1;
                        }
                    } else {
                        $uninsert[$tempVariable][3] = "Employee Code is mandatory";
                        $error = 1;
                    }


                    if ($emapData[3] != '') {
                        //print_r($emapData[3]);
                        //$checkError = var_dump(validateDateTime($emapData[3]));
                        //echo $checkError;exit;
                        if (!validateDateTime($emapData[3])) {
                            $uninsert[$tempVariable][4] = "Punch In Time is not in valid Format";
                            $error = 1;
                        }
                    }

                    if ($emapData[4] != '') {
                        //print_r($emapData[3]);
                        //$checkError = var_dump(validateDateTime($emapData[3]));
                        //echo $checkError;
                        if (!validateDateTime($emapData[4])) {
                            $uninsert[$tempVariable][5] = "Punch Out Time is not in valid Format";
                            $error = 1;
                        }
                    }



                    if ($emapData[5] != '') {
                        //print_r($emapData[5]);
                        if ($emapData[5] != '0' && $emapData[5] != '1') {
                            $uninsert[$tempVariable][7] = "Late Coming value must be in 0/1";
                            $error = 1;
                        }
                    }
                    if ($emapData[3] != '' && $emapData[4] != '') {
                        if ($error == 0) {
                            $success[$tempVariable] = 1;

                            $sql_in = "insert into attendanceall(CARDNO,EMP_CODE,PunchDate,Latecoming) values ('" . trim($emapData[1]) . "','" . trim($emapData[2]) . "','" . trim($emapData[3]) . "','" . trim($emapData[5]) . "')";
                            //echo $sql_in;
                            $sql_out = "insert into attendanceall(CARDNO,EMP_CODE,PunchDate,Latecoming) values ('" . trim($emapData[1]) . "','" . trim($emapData[2]) . "','" . trim($emapData[4]) . "','" . trim($emapData[5]) . "')";
                            $result_in = query($query, $sql_in, $pa, $opt, $ms_db);
                            $result_out = query($query, $sql_out, $pa, $opt, $ms_db);                            
                            $_SESSION['AttendanceUpload_csv']['result'][] = array($emapData[0], $emapData[1], $emapData[2], $emapData[3], $emapData[4], $emapData[5], "Successfully Inserted !");
                        }
                    } else if ($emapData[3] != '' && $emapData[4] == '') {
                        if ($error == 0) {
                            $success[$tempVariable] = 1;
                            $sql_in = "insert into attendanceall('CARDNO','EMP_CODE','PunchDate','Latecoming') values ('" . trim($emapData[1]) . "','" . trim($emapData[2]) . "','" . trim($emapData[3]) . "','" . trim($emapData[5]) . "')";
                            $result_in = query($query, $sql_in, $pa, $opt, $ms_db);
                            $_SESSION['AttendanceUpload_csv']['result'][] = array($emapData[0], $emapData[1], $emapData[2], $emapData[3], $emapData[4], $emapData[5], "Successfully Inserted !");
                        }
                    } else if ($emapData[3] == '' && $emapData[4] != '') {
                        if ($error == 0) {
                            $success[$tempVariable] = 1;
                            $sql_out = "insert into attendanceall('CARDNO','EMP_CODE','PunchDate','Latecoming') values ('" . trim($emapData[1]) . "','" . trim($emapData[2]) . "','" . trim($emapData[4]) . "','" . trim($emapData[5]) . "')";
                            $result_out = query($query, $sql_out, $pa, $opt, $ms_db);
                            $_SESSION['AttendanceUpload_csv']['result'][] = array($emapData[0], $emapData[1], $emapData[2], $emapData[3], $emapData[4], $emapData[5], "Successfully Inserted !");
                        }
                    } else if ($emapData[3] == '' && $emapData[4] == '') {
                        $uninsert[$tempVariable][6] = "Punch In Time and Punch Out Time both are Empty";
                        $error = 1;
                    }


                    if ($error == 1) {
                        $errorMessage = implode(",", $uninsert[$tempVariable]);
                        $_SESSION['AttendanceUpload_csv']['result'][] = array($emapData[0], $emapData[1], $emapData[2], $emapData[3], $emapData[4], $emapData[5], $errorMessage);
                    }
                    
                    /***attendance trigger start - Code by Pramod**/
                    if ($error == 0 && ($result_in || $result_out) ) {
                        $obj_attendaceRulesClass->initializeObject(array('empCode' => trim($emapData[2]), 'todayDateTime' => date('Y-m-d', strtotime(trim($emapData[0])))));
                    }
                    /***end***/
                }
            }
            $error = 0;

            $tempVariable++;
        }

        if (!empty($uninsert)) {
            foreach ($uninsert as $key => $value) {
                $mystring = implode(',<br />', $value);
                $output .= '<tr><td style="color: red;" width="200px">Row No. ' . $key . '</td><td style="color: red;" width="1000px">' . $mystring . '</td></tr>';
            }
            $output .= '<tr><td style="color: red;" width="200px"><a href="javascript:void(0);" class="btn btn-block red error_log_file">
														Download Error Log <i class="fa fa-download"></i>
													</a></td></tr>';
        }

        if (!empty($success)) {
            $output .= '<tr><td style="color: green;" width="1000px">Row No.';
            foreach ($success as $key => $value) {

                $output .= $key . ', ';
            }
            $output .= 'Inserted Successfully !!!!</td></tr>';
        }

        $returndata = array("output" => $output, "employeeId" => $employeeIds);
        echo json_encode($returndata);

        fclose($file);
    }
}
?>
