<?php

session_start();
/* ini_set("display_errors","on");
error_reporting(E_ALL); */
include '../../db_conn.php';
include('../../configdata.php');
include('../../global_class.php');
$global_obj = new global_class();
include('../../attendanceClass.php');
ini_set('max_execution_time', 0); //300 seconds = 5 minutes
set_time_limit(0);
// Check that the class exists before trying to use it
if (class_exists('AttendaceClass')) {
    $obj_attendaceRulesClass = new AttendaceClass();
}
if ($_POST['type'] == 'upload_roster') {
    $filename = $_FILES['file']['name'];
    if (pathinfo($filename, PATHINFO_EXTENSION) == 'csv') {
        $_SESSION['roster_csv'] = '';
        $attRosterFields        = array(
            "roster",
            "shiftMaster",
            "shiftPattern",
            "rosterDate",
            "auto_period",
            "created_on",
            "updated_on"
        );
        $rosterSchemaFields     = array(
            "RosterName",
            "Emp_Code",
            "start_rost",
            "end_rost",
            "auto_period",
            "created_on",
            "updated_on"
        );
        
        //save heading in session
        $_SESSION['roster_csv']['heading'] = array(
            "Emp_Code",
            "start_rost",
            "end_rost",
            "auto_period (Put only 0 or 1)",
            "Shift",
            "ShiftPattern",
            "Result Status",
            "Remarks"
        );
        
        //open uploaded csv file with read only mode
        $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
        
        //skip first line
        fgetcsv($csvFile);
        $error                  = $success = array();
        $rowNumber              = 1;
        $finalQueryAttRoster    = $rosterSchemaQuery = $rosterSchemaValues = '';
        $finalQueryRosterSchema = $attRosterQuery = $attRosterValues = '';
        // Set query variables
        $attRosterQuery         = 'INSERT INTO att_roster ';
        $attRosterQuery .= '(';
        $attRosterQuery .= implode(',', $attRosterFields);
        $attRosterQuery .= ')';
        
        $rosterSchemaQuery = 'INSERT INTO Roster_schema ';
        $rosterSchemaQuery .= '(';
        $rosterSchemaQuery .= implode(',', $rosterSchemaFields);
        $rosterSchemaQuery .= ')';
        $i = 0;
        while (($row = fgetcsv($csvFile)) !== FALSE) {
            
            $errorStr    = array();
            $rosterName  = 'rost-upload' . uniqid() . rand(1, 999999999999);
            $empCode     = $row[0];
            $start_rost  = trim($row[1]);
            $end_rost    = (!empty($row[2])) ? $row[2] : $start_rost;
            $auto_period = $row[3];
            
            $shiftMaster  = $global_obj->getEmpShiftMasterIdByName($row[4]);
            $shiftPattern = $global_obj->getEmpShiftPatternIdByName($row[5]);
            
            $rosterStatus = $global_obj->checkRosterExist($rosterName, $start_rost, $empCode);
            $userStatus   = $global_obj->checkUserExist($empCode);
            
            $start_rost_format = date_parse($start_rost);
            $end_rost_format   = date_parse($end_rost);
            
            if ($rosterStatus == false)
                $errorStr[] = 'Roster already exist';
            if ($userStatus == false)
                $errorStr[] = 'Employee (' . $empCode . ') does not exists';
            if ($empCode == '')
                $errorStr[] = 'Employee can not be blank';
            if ($start_rost == '')
                $errorStr[] = 'Roster start date can not be blabk';
            if ($shiftMaster == '')
                $errorStr[] = 'Shift is wrong';
            if ($shiftPattern == '')
                $errorStr[] = 'Shift pattern is wrong';
            if ($start_rost_format["error_count"] != 0)
                $errorStr[] = 'Start date in wrong format. Format should be in DD-MMM-YYYY';
            if ($end_rost_format["error_count"] != 0)
                $errorStr[] = 'End date in wrong format. Format should be in DD-MMM-YYYY';
            if ($auto_period != '0' && $auto_period != '1')
                $errorStr[] = 'Auto period value must be 0 and 1';
            
            if (!empty($errorStr)) {
                $error[] = implode(', ', $errorStr) . ' - errors are getting on uploading line #' . $rowNumber;
                
                //set errors for csv download
                $_SESSION['roster_csv']['result'][] = array(
                    $empCode,
                    $start_rost,
                    $row[2],
                    $row[3],
                    $row[4],
                    $row[5],
                    "Error",
                    implode(', ', $errorStr)
                );
            } else {
                $success[] = "Successfully Insered Line #" . $rowNumber;
                
                
                $start_rost = $start_rost_new = date('Y-m-d', strtotime($start_rost));
                $end_rost   = $end_rost_new = date('Y-m-d', strtotime($end_rost));
                $count      = 1;
                
                while (strtotime($start_rost) <= strtotime($end_rost)) {
                    $auto_period   = 1;
                    //check if roster already exist on same dates
                    $isRosterMappedQuery  = "SELECT RosterName FROM Roster_schema WHERE Emp_Code = '" . $empCode . "' AND CAST(start_rost AS DATE) = '" . $start_rost . "' AND CAST(end_rost AS DATE) = '" . $start_rost . "' AND auto_period='" . $auto_period . "'";
                    $isRosterMappedResult = query($query, $isRosterMappedQuery, $pa, $opt, $ms_db);
                    
                    if ($num($isRosterMappedResult) > 0) {
                        while ($isRosterMappedRow = $fetch($isRosterMappedResult)) {
                            $deletedRosterName  = $isRosterMappedRow['RosterName'];
                            $deleteRosterSchema = "DELETE FROM Roster_schema WHERE RosterName = '" . $deletedRosterName . "'";
                            $deleteAttRoster    = "DELETE FROM att_roster WHERE roster = '" . $deletedRosterName . "'";
                            query($query, $deleteRosterSchema, $pa, $opt, $ms_db);
                            query($query, $deleteAttRoster, $pa, $opt, $ms_db);
                        }
                    }
                    
                    
                    $rosterName    = 'rost-upload' . uniqid() . rand(1, 999999999999);
                    $rostersName[] = $rosterName;
                    
                    $attRosterValues .= "('" . $rosterName . "','" . $shiftMaster . "','" . $shiftPattern . "','" . $start_rost . "','" . $auto_period . "',GETDATE(),GETDATE()),";
                    $rosterSchemaValues .= "('" . $rosterName . "','" . $empCode . "','" . $start_rost . "','" . $start_rost . "','" . $auto_period . "',GETDATE(),GETDATE()),";
                    
                    if ($count >= 100) {
                        $finalQueryAttRoster    = $rosterSchemaQuery . ' values ' . rtrim($rosterSchemaValues, ',');
                        $finalQueryRosterSchema = $attRosterQuery . ' values ' . rtrim($attRosterValues, ',');
                        
                        if (!empty($finalQueryAttRoster) && !empty($finalQueryRosterSchema)) {
                            query($query, $finalQueryAttRoster, $pa, $opt, $ms_db);
                            query($query, $finalQueryRosterSchema, $pa, $opt, $ms_db);
                        }
                        
                        $rosterSchemaValues = $attRosterValues = $finalQueryAttRoster = $finalQueryRosterSchema = '';
                        $count              = 0;
                    }
                    $count++;
                    $start_rost = date('Y-m-d', strtotime(' +1 day', strtotime($start_rost)));
                }
                if ($i == '5') {
                    //echo $finalQueryAttRoster.'TEST';
                    //echo $finalQueryRosterSchema.'TEST1'; die;
                }
                if (!empty($rosterSchemaValues) && !empty($attRosterValues)) {
                    $finalQueryAttRoster    = $rosterSchemaQuery . ' values ' . rtrim($rosterSchemaValues, ',');
                    $finalQueryRosterSchema = $attRosterQuery . ' values ' . rtrim($attRosterValues, ',');
                    
                    if (!empty($finalQueryAttRoster) && !empty($finalQueryRosterSchema)) {
                        query($query, $finalQueryAttRoster, $pa, $opt, $ms_db);
                        query($query, $finalQueryRosterSchema, $pa, $opt, $ms_db);
                    }
                    $rosterSchemaValues = $attRosterValues = $finalQueryAttRoster = $finalQueryRosterSchema = '';
                }
                
                while(strtotime($start_rost_new) <= strtotime($end_rost_new)){
                    $obj_attendaceRulesClass->initializeObject(array('empCode' => $empCode, 'todayDateTime' => $start_rost_new));
                    $start_rost_new     =   date('Y-m-d', strtotime(' +1 day', strtotime($start_rost_new)));
                }
                
                //Hold in session for download in csv
                $_SESSION['roster_csv']['result'][] = array(
                    $empCode,
                    $start_rost,
                    $row[2],
                    $row[3],
                    $row[4],
                    $row[5],
                    "Uploaded"
                );
            }
            $rowNumber++;
            $i++;
        }
        
        
        $message = array(
            "success" => implode('.<br>', $success),
            "errors" => implode('.<br>', $error)
        );
        echo json_encode(array(
            "message" => $message
        ));
    } else {
        $message = array(
            "errors" => 'The file-extension of uploaded file is wrong! Needs to be ".csv".'
        );
        echo json_encode(array(
            "message" => $message
        ));
    }
}

if ($_REQUEST['type'] == 'download_roster_result') {
    
    if (isset($_SESSION['roster_csv']) && !empty($_SESSION['roster_csv'])) {
        $list    = $_SESSION['roster_csv']['result'];
        $heading = $_SESSION['roster_csv']['heading'];
        
        $global_obj->exportCSV($heading, $list, 'shift-roster');
    } else {
        die("Error getting...");
    }
}
?>