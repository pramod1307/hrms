<?php

global $HTTP_HOST;
global $query;
global $sql;
global $pa;
global $opt;
global $ms_db;
global $fetch;
global $num;


if ($endDate > $dateForGetPreviousRecord) {
    $sqlq00 = "SELECT * FROM attendance_backup where Emp_code='" . $empCode . "' and  att_date between '" . $startDate . "' and '" . $dateForGetPreviousRecord . "' order by att_date";
} else {
   $sqlq00 = "SELECT * FROM attendance_backup where Emp_code='" . $empCode . "' and  att_date between '" . $startDate . "' and '" . $endDate . "' order by att_date";
}

$resultq00 = query($query, $sqlq00, $pa, $opt, $ms_db);
if ($resultq00) {
    $tempArray00 = $num($resultq00);
} else {
    $tempArray00 = -1;
}

/*else if(trim($rowq00['att_status']) == 'HLD'){
$type						=	'attendance';
$title						=	'Half Day';
$formattedDate				=	date("D, M j Y",strtotime($rowq00['att_date']));
$presetStatus				=	"Half Day";
$presetShortStatus			=	"H";
$start						=	$rowq00['att_date'];
$startTime					=	date("g:i a",strtotime($rowq00['in_time']));
$endTime					=	date("g:i a",strtotime($rowq00['out_time']));
$className					=	'yellow circle-legend'; */

if ($tempArray00 > 0) {
    
    while ($rowq00 = $fetch($resultq00)) {
        $attStatus      = explode("~", $rowq00['att_status']);
        $attStatusCOUNT = COUNT($attStatus);
        $attStatusCOUNT--;
        $rowq00['att_status'] = $attStatus[$attStatusCOUNT];
        
       if(empty($rowq00['in_time']) || strtotime($rowq00['in_time']) <= 0){
			$inTiming	=	'';
		}else{
			$inTiming	=	date("g:i a", strtotime($rowq00['in_time']));
		}
		
		if(empty($rowq00['out_time']) || strtotime($rowq00['out_time']) <= 0){
			$outTiming	=	'';
		}else{
			$outTiming	=	date("g:i a", strtotime($rowq00['out_time']));
		}
		
			
       if (trim($rowq00['att_status']) == 'P') {
            $type              = 'attendance';
            $title             = 'Present';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "Present";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green2 circle-legend';
        } else if (trim($rowq00['att_status']) == 'A') {
            $type              = 'empty_attendance';
            $title             = 'Absent';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "Absent";
            $presetShortStatus = "A";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'red circle-legend';
        } else if (trim($rowq00['att_status']) == 'W') {
            $type              = 'weeklyoff';
            $title             = 'weeklyoff';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "";
            $presetShortStatus = "W";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green';
        } else if (trim($rowq00['att_status']) == 'L') {
            $type              = 'Leave';
            $title             = 'Leave';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "Leave";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'blue_over circle-legend';
        } else if (trim($rowq00['att_status']) == 'MIS') {
            $type              = 'Miss Punch';
            $title             = 'Miss Punch';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "MIS";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'miss-punch circle-legend';
        } else if (trim($rowq00['att_status']) == 'H_CL2') {
            $type              = 'Half Day Leave';
            $title             = 'Half Day Leave';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "H_CL2";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'presentAbsent circle-legend';
        } else if (trim($rowq00['att_status']) == 'OD') {
            $type              = 'OD';
            $title             = 'OD';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "OD";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green2 circle-legend';
        } else if (trim($rowq00['att_status']) == 'POH') {
            $type              = 'POH';
            $title             = 'Present on Holiday';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "Present";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green circle-legend';
        } else if (trim($rowq00['att_status']) == 'POW') {
            $type              = 'POW';
            $title             = 'Present on Weekly off';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "Present";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green circle-legend';
        } else if (trim($rowq00['att_status']) == 'SRT') {
            $type              = 'Present';
            $title             = 'Present';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "Present";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'blue_over circle-legend';
        } else if (trim($rowq00['att_status']) == 'C-P') {
            $type              = 'Present';
            $title             = 'Present';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "Present";
            $presetShortStatus = "P";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green2 circle-legend';
        } else if (trim($rowq00['att_status']) == 'WOH') {
            $type              = 'WOH';
            $title             = 'Additional Holiday';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "P";
            $presetShortStatus = "WOH";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green2 circle-legend bgnone';
        } else if (trim($rowq00['att_status']) == 'COFF') {
            $type              = 'COFF';
            $title             = 'Leave';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "P";
            $presetShortStatus = "Present";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'blue_over circle-legend';
        } else if (trim($rowq00['att_status']) == 'H' || trim($rowq00['att_status']) == 'HLD') {
            $type              = 'H';
            $title             = 'Holiday';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "H";
            $presetShortStatus = "Present";
            $start             = $rowq00['att_date'];
            $startTime         = $inTiming;
            $endTime           = $outTiming;
            $className         = 'green';
        }else if (empty($rowq00['att_status']) || trim($rowq00['att_status']) == '') {
            $type              = '';
            $title             = '';
            $formattedDate     = date("D, M j Y", strtotime($rowq00['att_date']));
            $presetStatus      = "";
            $presetShortStatus = "";
            $start             = $rowq00['att_date'];
            $startTime         = '';
            $endTime           = '';
            $className         = '';
        }
        
        $result[] = array(
            "type" => $type,
            "shiftName" => "",
            "shiftInOutTime" => "",
            "formattedDate" => $formattedDate,
            "num" => "",
            "start" => $start,
            "startTime" => $startTime,
            "endTime" => $endTime,
            "shiftInOutTime" => "",
            "presetShortStatus" => $presetShortStatus,
            "presetStatus" => $presetStatus,
            "title" => $title,
            "timeSpan" => "",
            "className" => array(
                $className
            ),
            "BusArrival" => "",
            "lates" => "",
            "totalLate" => "",
            "totalEarly" => "",
            "isLateComing" => "",
            "isEarlyGoing" => "",
            "attendanceMessage" => "",
            "shiftFrom" => "",
            "shiftTo" => "",
            "outTimeWithDate" => ""
        );
    }
}

return $result;

?>