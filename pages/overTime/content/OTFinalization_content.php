<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <!--modal-dialog -->
        <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog lg">
                <!-- modal-content -->
                <div class="modal-content" >
                    <div class="modal-header portlet box blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title white-txt"><div class="caption"><span>OverTime Finalization</span></div></h4>
                    </div>
                    <div class="modal-body" id="myleave">
                        <?php //include ("content/view_myodrequest.php"); ?>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- /.modal-dialog -->
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN CONDENSED TABLE PORTLET-->
                <div class="portlet box blue">

                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>OverTime Finalization
                        </div>
                      
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <!--<div class="col-md-12">

                                    <div class="btn-group ">
                                        <select class="form-control" onchange="getRelatedSelectbox(this.value);">
                                            <option value="">Select....</option>
                                            <option value="1">Request Applied On</option>
                                            <option value="2">Request Status</option>
                                           
                                        </select>
                                    </div>

                                    <div class="btn-group" id="monthlySearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                From Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="fromPastDate" id="fromDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                To Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="toPastDate" id="toDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-2" style="margin-top: 25px;">
                                        <button class="btn" onclick="searchByDate('<?php echo $code;?>')">Go</button>
                                        </div>
                                    </div>

                                    <div class="btn-group" id="actionSearch" style="display: none;margin-left: 45px;">
                                        <select class="form-control" onchange="searchByStatus(this.value,'<?php echo $code;?>');">
                                            <option value="">Select ..</option>
                                            <?php
                                            $sql="select * from LOVMast where LOV_Field='status'";
                                            $result=query($query,$sql,$pa,$opt,$ms_db);
                                            while ($row = $fetch($result)){
                                                ?>
                                                <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                            <?php } ?>

                                        </select>
                                    </div>

                                    <div class="btn-group " id="bynameSearch" style="display: none;margin-left: 45px;">

                                        <input type="text" name="byname" class="form-control pull-left" id="byname" onchange="getInputValue(this.value);" placeholder="Enter Emp code or Name">


                                        <button class="pull-right" id="inputvalue" value=""
                                                onclick="serchByCodeName(this.value,'<?php echo $code;?>');">Go</button>

                                    </div>

                                </div>-->
                            </div>

                        </div>
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <?php
                           $getOTKeyFinal=$OT_Finalization_class->getOTKeyFinal($code);
                            // print_r($getOTKeyFinal); die;
                            $OT_Key=$getOTKeyFinal[0];
                            $numRow=$getOTKeyFinal[1]; 

                            ?>
                            <thead >
                            <tr>
                            <th></th>
                            <th>
                                Apply Date
                            </th>
                             <th>
                                Requested By
                            </th>
                            <th>Cost Center</th>
                                <th>
                                     Category
                                </th>

                                <th>
                                    Shift
                                </th>

                                <th>
                                    OT Date
                                </th>
                                 <th>
                                     OT Start Time
                                </th>
                                   <th>
                                    OT End Time
                                </th>
                                <th>
                                   Total OT
                                </th>
                                <th>
                                    1st Approved On
                                </th>
                                <th>
                                    1st Approved Remarks
                                </th>
                                <th>
                                    2nd Approved On
                                </th>
                                <th>
                                    2nd Approved Remarks
                                </th>
                                <th>
                                    3rd Approved On
                                </th>
                                <th>
                                    3rd Approved Remarks
                                </th>

                             
                                <th>
                                    Status
                                </th>
                            </tr>
                            </thead>
                            <tbody id="searchMyData">
                            <?php
                                $currentDate=date("Y-m-d");
                            for ($j=0;$j<$numRow;$j++){


                                $sql="select top 1 replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as applyDate,OT_Category,OT_Shift, REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE, OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,appStatus,
                                    replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7) as First_Approver,
                                    replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7) as sec_Approver,
                                    replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as third_Approver,*      
     from overtime_Balances  where CreatedBy='$code' and OT_Key='$OT_Key[$j]' and OT_Date <= '$currentDate' ";


                                $res=query($query,$sql,$pa,$opt,$ms_db);
                                $len = $num($res);
                            
                            
                                $i=0;
                              
                                while ($row= $fetch($res)) { // print_r($row);
                                   $st=explode(",", $row['appStatus']);
                                  $remark = $row['App_OTRemark'];
                                    $apremark = explode(',', $remark);
                                    $totalremark = count($apremark);
                           
                                   $stlen=count($st);
                                   
                                    if($st[$stlen-1] == 2){
                                ?>

                                <tr><td></td>
                                    <td><?php echo $row['applyDate'];?></td>
                                    <td><?php $getEmployeeName=$OT_Finalization_class->getEmployeeNameByID($row['CreatedBy']);
                                        $reqName=$getEmployeeName[1];
                                    echo $reqName.' ('.$row['CreatedBy'].')';?></td>
                                    <td><?php $getCostName=$OT_Finalization_class->getCostName($code);
                                        echo  $getCostName[1];
                                    ?></td>
                                    <td>
                                        <?php echo $otcategory=$OT_Finalization_class->getappliedOT_Category($row['OT_Category']);?>
                                    </td>
                                    <td>
                                        <?php echo $row['OT_Shift'] ;?>
                                    </td>
                                    <td>
                                        <?php echo $row['OT_DATE'] ; ?>
                                    </td>
                                    <td>
                                        <?php  echo $row['OT_StartTime'];?>
                                    </td>
                                      <td>
                                        <?php echo $row['OT_EndTime'];?>
                                       
                                    </td>
                                    <td>
                                        <?php echo $row['overTimeHours'];?>
                                    </td>
                                      <td>
                                        <?php if($row['First_Approver'] == 0){
echo "";}
else{
echo $row['First_Approver']; }?>
                                    </td>
                                  <td>
                                  <?php if(isset($apremark[0]))
                                    echo $apremark[0]; ?>
                                       
                                    </td>
                                  
                                  <td>
                                        <?php if($row['sec_Approver'] == 0){
echo "";}
else{
echo $row['sec_Approver']; }
?>
                                    </td>
                                  
                                  <td>
                                         <?php if(isset($apremark[1]))
                                    echo $apremark[1]; ?>  
                                    </td>
                                  
                                   <td>
                                        <?php if($row['third_Approver'] == 0){
echo "";}
else{
echo $row['third_Approver']; }?>
                                    </td>
                                    <td>
                                         <?php if(isset($apremark[2]))
                                    echo $apremark[2]; ?>  
                                    </td>
                                  
                                  
                                  
                                  
                                    <td>
                                    
                                    <a class="myod" href="../overTime/overTimeFinalizationDetails.php?OTKey=<?php echo $OT_Key[$j];?>" 
                                    >
                                        <span class="label label-success">
                                            Finalization 
                                        </span>
                                    </a>
                                    </td>

                                </tr>
                                <?php $i++; } } }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END CONDENSED TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>

