<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <!--modal-dialog -->
        <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog lg">
                <!-- modal-content -->
                <div class="modal-content" >
                    <div class="modal-header portlet box blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title white-txt"><div class="caption"><span>My OT Request -Manager View </span></div></h4>
                    </div>
                    <div class="modal-body" id="managerDetailInfo">
                        <?php //include ("content/view_myodrequest.php"); ?>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- /.modal-dialog -->
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN CONDENSED TABLE PORTLET-->
                <div class="portlet box blue">

                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>My OT Request -Manager View
                        </div>
                      
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">

                                    
                                    <div class="btn-group" id="monthlySearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                From Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="fromPastDate" id="fromDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                To Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="toPastDate" id="toDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-2" style="margin-top: 25px;">
                                        <button class="btn" onclick="">Go</button>
                                        </div>
                                    </div>

                                    <div class="btn-group" id="actionSearch" style="display: none;margin-left: 45px;">
                                        <select class="form-control" onchange="">
                                            <option value="">Select ..</option>
                                            <?php /*
                                            $sql="select * from LOVMast where LOV_Field='status'";
                                            $result=query($query,$sql,$pa,$opt,$ms_db);
                                            while ($row = $fetch($result)){
                                                ?>
                                                <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                            <?php } 
											*/
											?>

                                        </select>
                                    </div>

                                    <div class="btn-group " id="bynameSearch" style="display: none;margin-left: 45px;">

                                        <input type="text" name="byname" class="form-control pull-left" id="byname" onchange="getInputValue(this.value);" placeholder="Enter Emp code or Name">


                                        <button class="pull-right" id="inputvalue" value=""
                                                onclick="serchByCodeName(this.value,'<?php echo $code;?>');">Go</button>

                                    </div>

                                </div>
                            </div>

                        </div>
						
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                           
                            <thead >
                            <tr><th>S.No</th>
                            <th>
                                Applied Date And time
                            </th>
                            <th>OT Type</th>
                            <th>
                                Requested By
                            </th>
                            <th>
                                Cost Center
                            </th>
                                <th>
                                   Category
                                </th>

                                <th>
                                    Shift
                                </th>

                                <th>
                                    OT Date
                                </th>
                                 <th>
                                    OT Start Time
                                </th>
                                   <th>
                                    OT End Time
                                </th>
                                <th>
                                    Total OT
                                </th>
									                          
                                <th>
                                    Reason
                                </th>
                                 <th>
                                    1st Approved On
                                </th>
                                <th>
                                    1st Approved Remarks
                                </th>
                                <th>
                                    2nd Approved On
                                </th>
                                <th>
                                    2nd Approved Remarks
                                </th>
                                <th>
                                    3rd Approved On
                                </th>
                                <th>
                                    3rd Approved Remarks
                                </th>
								<th>
									Detail View
								</th>
                            </tr>
                            </thead>
                            <tbody id="searchMyData">
                           
                                
								
								<?php
										if(isset($_SESSION['usercode']) && $_SESSION['usercode'] != ''){
											$id = $_SESSION['usercode'];
											//$id = 10146;
										}else{
											echo "Somthing wrong on this page sorry!!!!"; die;
										}
						
										//echo '<pre>'; print_r($_SESSION); echo '</pre>'; die;
										$getDataToTable = new OTRequestemployee_class();
										$getEmployeeName=$getDataToTable ->getEmployeeName($id);
											$reqName=$getEmployeeName[1];

                                       

										$getData=$getDataToTable->getManagerData($id); 
                                        $getCostName=$getDataToTable->getCostName($code);
                                        //print_r($getData);
                                        $otkey=$getData[0];
                                        $len=$getData[1];
										for ($j=0; $j < $len; $j++) { 

                                       $sql12=" select top 1 REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE,
         replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as OT_ApplyDateTime,
         replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7)  as FirstApprover,
         replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7) as secApprover,
         replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as thirdApprover,OT_Category,OT_Shift,OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,appStatus,App_OTRemark
 ,OT_Type,OT_NewFunction from overtime_Balances where CreatedBy='$id' and OT_Key='$otkey[$j]'";
                                        $res12=query($query,$sql12,$pa,$opt,$ms_db);
//if($count==3)break;
                                        $row=$fetch($res12);
//echo "<pre>";print_r($res);
                                         $DataStatus= $row['appStatus'];
                                        $status_val =explode(',', $DataStatus);
                                        $DataRemark= $row['App_OTRemark'];
                                        $remark_val =explode(',', $DataRemark);


								?>
								<tr><td><?php echo $j+1;?></td>
                                    <td>
									<?php  echo $row['OT_ApplyDateTime']; ?>
									</td>
                                    <td><?php if($row['OT_Type'] == "1"){ echo "Deviation";}else{ echo "Standard";}  ?>
                                        
                                    </td>
                                     <td>
                                    <?php  echo $reqName.' ('.$id.')'; ?>
                                    </td>
                                    <td><?php echo $getCostName[1];?></td>
                                    <td>
                                    <?php  echo $getDataToTable->getappliedOT_Category($row['OT_Category']); ?>   
                                    </td>
                                    <td>
                                     <?php  echo $row['OT_Shift']; ?>  
                                    </td>
                                    <td>
                                     <?php  echo $row['OT_DATE']; ?>  
                                    </td>
                                    <td>
                                    <?php  echo $row['OT_StartTime']; ?>   
                                    </td>
                                     <td>
									<?php  echo $row['OT_EndTime']; ?>
                                    </td>
                                    <td>
                                    <?php  echo $row['overTimeHours']; ?>   
                                    </td>
                                    <td>
                                    <?php  echo $getDataToTable->getappliedOT_Reason($row['OT_NewFunction']); ?>   
                                    </td>
                                    <td>    
                                    <?php  if($row['FirstApprover'] == 0){
                                        echo " ";
                                    }else{
                                        echo $row['FirstApprover'];
                                    }                   
                                    ?>                
                                    </td>
                                    <td>
                                    <?php  if(isset($remark_val[0]))
                                        echo $remark_val[0];
                                    ?>   
                                    </td>
                                    <td>
                                    <?php  if($row['secApprover'] == 0){
                                        echo " ";
                                    }else{
                                        echo $row['secApprover'];
                                    }                   
 
                                    ?>
                                    </td>
                                    <td>
                                    <?php if(isset($remark_val[1]))
                                    echo $remark_val[1];  ?>   
                                    </td>
                                    <td>
                                     <?php   if($row['thirdApprover'] == 0){
                                        echo " ";
                                        }else{
                                            echo $row['thirdApprover'];
                                        }                   

                                    ?>
                                    </td>
                                    <td>
                                    <?php  if(isset($remark_val[2]))
                                        echo $remark_val[2]; ?>   
                                    </td>
									<td>
											<a class="myod myOdData" data-toggle="modal" href="#large" onclick="getapprKey('<?php echo $id; ?>','<?php  echo $otkey[$j]; ?>');">
												<span class="label bg-blue-steel">
													View 
												</span>
                                            </a>
									</td>
                                </tr>
								<?php
										}
								?>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END CONDENSED TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>

