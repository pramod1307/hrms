<?php 
	$getForm=$OTRequest_class->getForm();	
	//print_r($getForm);
	$Field_Desc=$getForm[0];
	$Field_Len=$getForm[1];
	$DefaultValue=$getForm[2];
	$Nullable=$getForm[3];
	$MinLength=$getForm[4];
	$num=$getForm[5];	

	$getTable=$OTRequest_class->getTable();
	//print_r($getTable);

	$Field_TDesc=$getTable[0];
	$Field_TLen=$getTable[1];
	$DefaultTValue=$getTable[2];
	$TNullable=$getTable[3];
	$TMinLength=$getTable[4];
	$Tnum=$getTable[5];	


?>
<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            Over Time Request
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">


                            </a>
                        </div>
                    </div>

                    <div class="portlet-body form">
                      
                        <div id="alert"></div>

                    <form  enctype="multipart/form-data" id="form" name="OTform" class="form-horizontal form-row-seperated">
                        <div class="form-body">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-4" style="margin-top: 35px;">
                                        <div style="float: right;">
                                        OverTime Type
                                        </div>
                                    </div>

                                    <div class="col-md-4" style="margin-top: 25px;">
                                        <div class="input-group">
                                        <div class="icheck-inline">
                                            <label>
                                            <input type="radio" name="ot_type" id="past" value="1" class="icheck">
                                            Deviation</label>
                                            <label>
                                            <input type="radio" name="ot_type" id="future" class="icheck" value="2" checked >Standard </label>
                                            
                                        </div>
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>

                            

                        	<div class="form-group">
                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[0];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >
                                    <input type="hidden" id="applyDate_null" value="<?php echo $Nullable[0];?>">
                                    
                                    <input type="text" name="applyDate" id="applyDate" value="<?php echo date("d-M-Y h:i:a");?>" class="form-control" maxlength='<?php echo $Field_Len[0];?>' style="margin-top: 25px;" readonly>  
                                     
                                    </div>
                                  
                            
                                </div>
                            </div>


							<div class="form-group">
								<?php
                                    $getCategory=$OTRequest_class->getCategory();
                                    //print_r($getCategory);
                                    $LOV_Text=$getCategory[0];
                                    $LOV_Value=$getCategory[1];
                                    $len=$getCategory[2];
                                 ?>

                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[1];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >
                                    	<input type="hidden" id="category_null" value="<?php echo $Nullable[1];?>">                                     
                                        <select class="form-control"  name="category" id="category" style="margin-top: 25px;" onchange="getCatId(this.value);">
                                        <option value="NO">Select Category</option>>
                                        <?php for($i=0; $i<$len; $i++){?>            
                                            <option  value="<?php echo $LOV_Value[$i];?>">
                                                <?php echo $LOV_Text[$i];?>
                                            </option>
                                            
                                        <?php }?>
                                        </select>
                                       
                                    </div>
                                  
                            
                                </div>
                            </div>

                            <div class="form-group">
								<?php
                                    $getShift=$OTRequest_class->getShift();
                                    //print_r($getShift);
                                    $shift=$getShift[0];
                                    $Shift_Code=$getShift[1];
                                    $len=$getShift[2];
                                 ?>

                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 10px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[6];?>
                                </div>
                                </div>
                                    <div class="col-md-8" >                                    
                                    <select class="form-control" onchange="getShiftId();" name="shift[]" id="shift" style="margin-top: 25px;" multiple="multiple"><!-- 
                                            <option value="NO">Select Shift</option> -->
                                        <?php for($i=0; $i<$len; $i++){?>            
                                            <option value="<?php echo $Shift_Code[$i];?>">
                                            <?php echo $shift[$i];?>
                                            </option>
                                            
                                        <?php }?>
                                    </select>
                                     </div>
                                <input type="hidden" id="shift_null" value="<?php echo $Nullable[6];?>">
                                </div>
                            </div>  


                            <div class="form-group">
                                <?php  $emp=array();
                                       
                                    $getMyTeam=$OTRequest_class->getMyTeam($code);
                                   // print_r($getMyTeam); die;
                                    $emp_code=$getMyTeam[0];
                                    $Emp_Name=$getMyTeam[1];
                                    $len=$getMyTeam[2];

                                   
                                    for ($i=0; $i <$len ; $i++) { 
                                        $emp[$i]=$emp_code[$i];
                                      
                                    }
                                   $EmpStr=implode("','", $emp); 
                                    $empstr="'".$EmpStr."'";
                                 ?>
                                <div class="col-md-12">
                                    <div class="col-md-4" style="margin-top: 35px;">
                                        <div style="float: right;" >
                                         <?php echo $Field_Desc[2];?>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-4" style="margin-top: 25px;" >
                                       <input type="text" class="form-control " name="from" id="from"  placeholder="MM-DD-YY" readonly />
                                       
                                       <input type="hidden" id="otdate_null" value="<?php echo $Nullable[2];?>" style="display:block;">
                                       <input type="hidden" name="team" id="team" value="<?php echo $empstr;?>">

                                        <input type="text" class="form-control " name="pastdate" id="pastdate"  placeholder="MM-DD-YY" style="display:none;" readonly /> 
                                    </div>
                                </div>
                            </div>

                           

							<div class="form-group">
                                <div class="col-md-4">
                                    <div class="col-md-6" style="margin-top: 35px;">
                                        <div style="float: right;">
                                            <?php echo $Field_Desc[3];?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 date" style="margin-top: 25px;">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-24" id="Time1" onchange="time_calculate111();">
                                            <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="col-md-6" style="margin-top: 35px;">
                                        <div style="float: right;">
                                            <?php echo $Field_Desc[4];?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 date" style="margin-top: 25px;">
                                       
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-24" id="Time2" onchange="time_calculate111();">
                                            <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="col-md-6" style="margin-top: 35px;">
                                        <div style="float: right;">
                                            <?php echo $Field_Desc[5];?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 date" style="margin-top: 25px;">
                                        <input id="totalHour" class="form-control" value="" type="text" placeholder="Hours" / readonly>
                                        <input type="hidden" id="totalTime_null" value="<?php echo $Nullable[5];?>" >
                                    </div>
                                </div>
                            </div>

                           

							                          
							<div class="form-group">

								<div class="col-md-12">
	                               
	                               <div  style="border: 1px solid #7cacfa;">
										<div>
											
											<table >
                                                <thead>
                                                <tr>
                                                    <th class="table-checkbox">
                                                    <!--<input type="checkbox" id="All" onclick="allCheck('All','<?php// echo $code;?>'); allPayCycle('All','<?php //echo $code;?>');" />-->
                                                    </th>
                                                    <th><?php echo $Field_TDesc[1];?></th>
                                                    <th><?php echo $Field_TDesc[2];?></th>
                                                    <th><?php echo $Field_TDesc[3];?></th>
                                                    <th><?php echo $Field_TDesc[4];?></th>
                                                    <th><?php echo $Field_TDesc[5];?></th>
                                                    <th><?php echo $Field_TDesc[6];?></th>
                                                    <th><?php echo $Field_TDesc[7];?></th>
                                                    <th><?php echo $Field_TDesc[8];?></th>
                                                    <!-- <th><?php //echo $Field_TDesc[10];?></th>--> 
                                                    <th><?php echo $Field_TDesc[10];?></th>
                                                    <th><?php echo $Field_TDesc[11];?></th>
                                                    <!--<th><?php echo $Field_TDesc[12];?></th> 
                                                    <th><?php echo $Field_TDesc[13];?></th>--> 
                                                </tr>
                                                </thead>
                                                <tbody id="showTable">
                                               
                                                
                                                </tbody>
                                                </table>
											
										</div>
									</div>
                            
                                </div>
								
							</div> 

                             

                    
							<div class="form-group" style="display:none;">
								<?php
                                    $getPurpose=$OTRequest_class->getPurpose();
                                    
                                    $LOV_Text=$getPurpose[0];
                                    $LOV_Value=$getPurpose[1];
                                    $len=$getPurpose[2];
                                 ?>

                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[8];?>
                                </div>
                                </div>
                                    <div class="col-md-4" > 
                                    		<input type="hidden" id="applyDate_null" value="<?php echo $Nullable[8];?>">                                    
                                            <select class="form-control"  name="purpose" id="purpose" style="margin-top: 25px;" onclick="getPurpose(this.value);">
                                            <option value="">Select Purpose</option>
                                            <?php for($i=0; $i<$len; $i++){?>            
                                                <option value="<?php echo $LOV_Value[$i];?>" <?php if($LOV_Value[$i] == 2){echo "Selected";}?>>
                                                <?php echo $LOV_Text[$i];?>
                                                </option>
                                                
                                            <?php }?>
                                            </select>
                                       
                                    </div>
                                  
                            
                                </div>
                            </div>  

                            
                            <div class="form-group" id="new" >
								<?php
                                    $getNewReqOpt=$OTRequest_class->getNewReqOpt();
                                    
                                    $LOV_Text=$getNewReqOpt[0];
                                    $LOV_Value=$getNewReqOpt[1];
                                    $len=$getNewReqOpt[2];
                                 ?>

                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                 <?php echo $Field_Desc[9];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >   
                                    		<input type="hidden" id="newRequest_null" value="<?php echo $Nullable[9];?>">                                  
                                            <select class="form-control"  name="newRequest" id="newRequest" style="margin-top: 25px;" onchange="getother(this.value);">
                                            <option value="">Select </option>>
                                            <?php for($i=0; $i<$len; $i++){?>            
                                                <option value="<?php echo $LOV_Value[$i];?>">
                                                <?php echo $LOV_Text[$i];?>
                                                </option>
                                                
                                            <?php }?>
                                            </select>
                                       
                                    </div>
                                  
                            
                                </div>
                            </div>

                            <div class="form-group" id="div_other" style="display:none;">
                            	<div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                 <?php echo $Field_Desc[10];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >   
                                    	<input type="hidden" id="other_null" value="<?php echo $Nullable[9];?>">
                            			<input type="text" name="other" class="form-control" id="other" style="margin-top: 25px;" maxlength="<?php echo $TMinLength[10];?>" />
                            		</div>
                            	</div>
                            </div> 

                              <?php 
                                $sqlq="select * from WorkFlow WHERE WFFor='overtime' and WEvents='standard' order by WorkFlowID DESC ";

                                $resultq = query($query,$sqlq,$pa,$opt,$ms_db);
                                $row = $fetch($resultq);
                                $workflowmethod = $row['AppMethod'];
                                $sqladvance="select * from WorkFlow WHERE WFFor='overtime' and WEvents='deviation' order by WorkFlowID DESC ";

                                $resultadvance = query($query,$sqladvance,$pa,$opt,$ms_db);
                                $rowadvance = $fetch($resultadvance);
                                $advanceworkflowmethod = $rowadvance['AppMethod'];

                                $sqlr="Select * from hrdmastQry where Emp_Code='$code'";
                                $resultr = query($query,$sqlr,$pa,$opt,$ms_db);
                                $rowr = $fetch($resultr);
                                $code1 = $rowr['MNGR_CODE'];
                            ?>

                            <input type="hidden" name="wfmethod" id="wfmethod" value="<?php echo $workflowmethod; ?>">
                            <input type="hidden" name="advancewfmethod" id="advancewfmethod" value="<?php echo $advanceworkflowmethod; ?>">
                            <input type="hidden" name="autoapprover" id="autoapprover" value="<?php echo $code1; ?>">

							<input type="hidden" id="levellist" value='<?php echo $data1['Emp_Code'];?>'>
                            <input type="hidden" id="selectedcheckbox" value=''>
                            <input type="hidden" id="selectedshfid" value=''>
                             <input type="hidden" id="selectedquartlimit" value=''>
                            <input type="hidden" name="finYear" id="finYear" value=''>

                            <input type="hidden" name="endShift" id="endShift" value="0"/>

                            <input type="hidden" id="catId" value="">

                            <input type="hidden" name="shfid" id="shfid" value="">

                            <input type="hidden" name="error" id="error" value="0">

                            <input type="hidden" name="advanceLevel" id="advanceLevel" value="0">
                        
							<div class="form-group" id="approverDiv" style="display: <?php echo ($workflowmethod!='automatic')?'block':'none'?>">
								<div class="col-md-12">
									<div class="col-md-4" style="margin-top: 35px;" >
                                        <div style="float: right;">
                                			 Approve By
                                		</div>   
                            		 </div>

                            		 <div class="col-md-4"  id="showlevel" style="margin-top: 25px;">
                                          
                            		 </div>
								</div>
							</div>
                            
                            <?php //if($_REQUEST['error'] == "0"){?>
        
                            <div class="form-actions" id="actionMyself">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" onclick="submitOverTime('<?php echo $code;?>');" class="btn btn-circle blue" id="leaveSubmit" value="">Submit</button>
                                        
                                    </div>
                                </div>
                            </div>
                            
							<?php //} ?>

                        </div>
                        </form>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>