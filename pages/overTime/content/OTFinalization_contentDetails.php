<?php 
date_default_timezone_set("Asia/Kolkata");
    $getForm=$OT_Finalization_class->getForm(); 
     //print_r($getForm); 
    $Field_Desc=$getForm[0];
    $num12=$getForm[1];   

    $getTable=$OT_Finalization_class->getTable();   
    //print_r($getTable); 
    $Field_TDesc=$getTable[0];
    $numTab=$getTable[1];   


     $getFornValue=$OT_Finalization_class->getFornValue(); 
     $interval=0;
    // echo $parts;
?>

<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
       <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog lg">
            <!-- modal-content -->
            <div class="modal-content" >
                <div class="modal-header portlet box blue">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title white-txt"><div class="caption"><b>Finalization OverTime
                    </b></div></h4>
                </div>
                <div class="modal-body" id="overTimeFinal">                
                    
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
      </div>
        <!-- /.modal -->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>OverTime Finalization
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">

                            </a>
                            
                        </div>
                    </div>

                    <!--ReadOnly Part -->
                    
                        <?php 
                            $getDetailsFinal=$OT_Finalization_class->getDetailsFinal($parts,$code);
                            //print_r($getDetailsFinal); die;
                            $OT_Category=$getDetailsFinal[0];
                            $OT_Date=$getDetailsFinal[1];
                            $OT_Shift=$getDetailsFinal[2];
                            $OT_StartTime=$getDetailsFinal[3];
                            $OT_EndTime=$getDetailsFinal[4];
                            $OT_overTimeHours=$getDetailsFinal[5];
                            $OT_purpose=$getDetailsFinal[6];
                            $OT_ApplyDate=$getDetailsFinal[7];
                            $OT_NewFunction=$getDetailsFinal[8];
                            $OT_Other=$getDetailsFinal[9];
                            $i=$getDetailsFinal[10];
                        ?>

                    <div class="portlet-body form">
                      <form  enctype="multipart/form-data" id="form" name="OTform" class="form-horizontal form-row-seperated">
                        <div class="form-body">

                            <div class="form-group">
                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[0];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >
                                    <input type="text" name="applyDate" id="applyDate" value="<?php echo  $OT_ApplyDate;?>" class="form-control"  style="margin-top: 25px;" readonly>  
                                     
                                    </div>
                                  
                            
                                </div>
                            </div>


                            <div class="form-group">
                                

                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[1];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >
                                                                        
                                        <input type="text" class="form-control"  name="category" id="category" style="margin-top: 25px;" value="<?php echo $catName=$OT_Finalization_class->getappliedOT_Category($OT_Category);?>" readonly>
                                       
                                    </div>
                                  
                            
                                </div>
                            </div>

                            <div class="form-group">
                                
                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[6];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >                                
                                        <input type="text" class="form-control " name="shift" id="shift" style="margin-top: 25px;" value="<?php echo $OT_Shift;?>" readonly> 
                                    </div>
                                  
                            
                                </div>
                            </div>  


                            <div class="form-group">
                                
                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;" >
                                <?php echo $Field_Desc[2];?>
                                </div>
                                </div>
                                
                                <div class="col-md-4" style="margin-top: 25px;" >
                                       <input type="text" class="form-control " name="from" id="from" value="<?php echo $OT_Date;?>" placeholder="MM-DD-YY" readonly/>
                                      
                                 </div>
                                  
                            
                                </div>
                            </div>

                           

                            <div class="form-group">
                                <div class="col-md-4">
                                <div class="col-md-6" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[3];?>
                                </div>
                                </div>
                                    <div class="col-md-6 date" style="margin-top: 25px;">
                                    

                                    <div class="input-group">
                                        <input type="text" class="form-control" value="<?php echo $OT_StartTime;?>" id="Time1" readonly />
                                        <span class="input-group-btn">
                                        <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                        </span>
                                    </div>
                                    </div>
                                  
                            
                                </div>

                                <div class="col-md-4">
                                <div class="col-md-6" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[4];?>
                                </div>
                                </div>
                                    <div class="col-md-6 date" style="margin-top: 25px;">
                                    
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="<?php echo $OT_EndTime?>" id="Time2" readonly />
                                        <span class="input-group-btn">
                                        <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                        </span>
                                    </div>
                                    </div>
                                  
                            
                                </div>


                                <div class="col-md-4">
                                <div class="col-md-6" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php echo $Field_Desc[5];?>
                                </div>
                                </div>
                                    <div class="col-md-6 date" style="margin-top: 25px;">
                                    <input id="totalHour" class="form-control" value="<?php echo $OT_overTimeHours;?>" type="text" placeholder="Hours" readonly />
                                   
                                    </div>
                                </div>
                            </div>

                    
                            <!--<div class="form-group">
                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                <?php //echo $Field_Desc[8];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >                                    
                                        <input type="text" class="form-control"  name="purpose" id="purpose" value="<?php //echo $purName=$OT_Finalization_class->getappliedOT_Purpose($OT_purpose);?>" style="margin-top: 25px;" readonly/>
                                       
                                    </div>
                                  
                            
                                </div>
                            </div> --> 

                            <?php if($OT_NewFunction){
                                $newreq=$OT_Finalization_class->getappliedOT_NewRequest($OT_NewFunction);
                                ?>
                            <div class="form-group" id="new" >
                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                 <?php echo $Field_Desc[9];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >   
                                        <input type="text" class="form-control"  name="newRequest" value="<?php echo $newreq;?>" id="newRequest" style="margin-top: 25px;" ReadOnly>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
    
                            <?php if($OT_NewFunction == '9'){?>
                            <div class="form-group" id="div_other" >
                                <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 35px;">
                                <div style="float: right;">
                                 <?php echo $Field_Desc[10];?>
                                </div>
                                </div>
                                    <div class="col-md-4" >   
                                        <input type="text" name="other" class="form-control" id="other" style="margin-top: 25px;" value="<?php echo $OT_Other;?>"  ReadOnly/>
                                    </div>
                                </div>
                            </div> 
                            <?php }?>
                        </div>
                        </form>
                    </div>
                    
                    <!--End ReadOnly Part -->

                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="btn-group selectAlign">
                                        <select class="form-control" id="selectbox" onchange="getRelatedselectedbox('<?php echo $parts;?>','<?php echo $code;?>');">
                                            <option value="">Select....</option>
                                            <option value="1">All</option>
						                    <option value="2">Standard</option>

                                            <option value="3">Exceptional</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                        </div>
                    <div style="float: right;padding: 2px;">
                     <button id="btnExport" onclick="fnExcelReport('sample_2');"> EXPORT </button>
                     <!--<input type="button" id="exportpdf" onclick="pdfexport();" value="Download PDF">-->
                    
                     </div>
                     <iframe id="txtArea1" style="display:none"></iframe>

                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <?php
                                   $getEmpTableDetails=$OT_Finalization_class->getEmpTableDetails($parts,$code);

                                    //print_r($getEmpTableDetails); //die;
                                    $Emp_Code=$getEmpTableDetails[0];
                                    $OT_Date=$getEmpTableDetails[1];
                                    $OT_StartTime=$getEmpTableDetails[2];
                                    $OT_EndTime=$getEmpTableDetails[3];
                                    $OT_overTimeHours=$getEmpTableDetails[4];
                                    $OT_ApplyDate=$getEmpTableDetails[5];
                                    $status=$getEmpTableDetails[6];
                                    $finalRemark=$getEmpTableDetails[7];
                                    $OT_Shift=$getEmpTableDetails[8];
                                    $i=$getEmpTableDetails[9];
                                ?>
                                <tr>
                                    <th class="table-checkbox">
                                        <!--<input type="checkbox" id="Allcheck" value="0" 
                                        onclick="allCheck('Allcheck','<?php// echo $code;?>');" class="group-checkable" data-set="#sample_2 .checkboxes"/>-->
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[0];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[1];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[2];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[3];?>
                                    </th>

                                    <th>
                                       <?php echo $Field_TDesc[4];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[5];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[6];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[7];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[8];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[9];?>
                                    </th>

                                    <th>
                                       <?php echo $Field_TDesc[10];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[11];?>
                                    </th>

                                    <th>
                                       <?php echo $Field_TDesc[12];?>
                                    </th>

                                    <th>
                                        <?php echo $Field_TDesc[13];?>
                                    </th>
                                    <th></th>
                                </tr>

                                <tbody id="searchData" >
                                    <?php for($j=0;$j<$i; $j++) { 

                                        /*-------Get Shift Id-----------*/
                                           $shiftIdSql="select ShiftMastId from ShiftMast where Shift_Name ='$OT_Shift[$j]' or Shift_Code='$OT_Shift[$j]'";
                                            $shiftIdresult=query($query, $shiftIdSql, $pa, $opt, $ms_db);
                                            
                                            $shiftIdRow = $fetch($shiftIdresult);
                                            $strShiftId =$shiftIdRow['ShiftMastId'];
                                                
                                        /*-------Get Shift Id-----------*/
					if($status[$j]=='' || $strShiftId==''){
					 $SqlQuery = "SELECT Emp_code,convert(char(5),Shift_start,108) AS shift_from,convert(char(5),Shift_end,108) AS SHIFT_TO, Actual_Shift as shift_name,Intime,Outtime,statustitle FROM attendancesch WHERE date='$OT_Date[$j]'  AND EMP_CODE ='$Emp_Code[$j]'"; 
							}
							else{
                                      	$SqlQuery = "SELECT Emp_code,convert(char(5),Shift_start,108) AS shift_from,convert(char(5),Shift_end,108) AS SHIFT_TO, Actual_Shift as shift_name,Intime,Outtime,statustitle FROM attendancesch WHERE date='$OT_Date[$j]'  AND EMP_CODE ='$Emp_Code[$j]' AND shiftid ='$strShiftId' "; 
							}
                                        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
                                        $row = $fetch($result);
					$attInTime =(isset($row['Intime']) && strtotime($row['Intime']) > 0)?date('H:i',strtotime($row['Intime'])):'';

                                        if(strtotime($row['Outtime']) >0){ $n=date('H:i',strtotime($row['Outtime']));}else{$n='  ';}
                                        $attOutTime =$n;
                                         $statustitle= $row['statustitle'];
                                         $shiftTo1   = $row['SHIFT_TO'];
                                         $cal= (!empty($OT_Date[$j]))  ? date('Y-m-d',strtotime($OT_Date[$j]))." " : '';
					$cal1 = (isset($row['Outtime']) && strtotime($row['Outtime']) > 0)?date('H:i',strtotime($row['Outtime'])):'';

                                    ?>


                                    <tr class="odd gradeX">
                                        <td></td>
                                        <td><?php echo $OT_Date[$j];?></td>

                                        <td><?php echo $Emp_Code[$j];?></td>

                                        <td><?php $getEmployeeName=$OT_Finalization_class->getEmployeeNameByID($Emp_Code[$j]);
                                                $reqName=$getEmployeeName[1];
                                            echo $reqName.' ('.$Emp_Code[$j].')';?>
                                            
                                        </td>
                                        <td><?php echo date("H:i", strtotime($OT_StartTime[$j]));?></td>

                                        <td><?php echo date("H:i", strtotime($OT_EndTime[$j]));?></td>

                                        <td><?php echo $planOtHour=date("H:i", strtotime($OT_overTimeHours[$j]));?>
                                            
                                        </td>

                                            <?php if($attInTime == '00:00' || $attInTime == '' || $attOutTime == '00:00' || $attOutTime == ''){?>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <?php if($status[$j] == 2 ){?>
                                            <td>
                                                <span class="label label-success" >
                                                            Approved
                                                </span>
                                                
                                            </td>
                                            <td>
                                                <span class="label label-danger"></span>
                                            </td>
                                            <td>
                                                <input type="text" name="finalRemark" id="finalRemark" readonly value="<?php echo $finalRemark[$j];?>" >
                                            </td>
                                            <?php }else if($status[$j] == 3){?>
                                            <td>
                                                <span class="label label-success" ></span>
                                            </td>
                                            <td>

                                                <span class="label label-danger">
                                                            Rejected 
                                                </span>
                                            </td>
                                            <td>
                                                <input type="text" name="finalRemark" id="finalRemark" readonly value="<?php echo $finalRemark[$j];?>" />
                                            </td>
                                                                  
                                            </td>
                                            <?php }else{?>
                                            <td>
                                                <div class="tooltip23">
                                                    <a href="javascript:void(0)"  style="pointer-events: none;cursor: default;" >
                                                    <span class="label label-success" style="pointer-events: none;">
                                                                    Approve 
                                                    </span> </a>
                                                    <span class="tooltiptext23">In or Out Punch Is Missed</span>
                                                </div>
                                            </td>                                   
                                            <td>
                                                <div class="tooltip23">
                                                    <a href="javascript:void(0)"  style="pointer-events: none;cursor: default;" >
                                                    <span class="label label-danger" style="pointer-events: none;">
                                                                Reject 
                                                    </span>
                                                    </a>
                                                    <span class="tooltiptext23">In or Out Punch Is Missed</span>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text" name="finalRemark" id="finalRemark<?php echo $j;?>">
                                            </td>
                                            <?php }?>
                                            <td>
                                                <?php 
                                                /*------Start Add hours-------*/
                                                    echo "0";
                                                /*------End Add hours-------*/
                                            ?>
                                            </td>
                                            <?php }else{?>
                                            <td>
                                                <?php
                                                     echo $attInTime; //IN Time Of Employee
         
                                                ?>
                                            </td>

                                            <td>
                                                <?php 
                                               
                                                    echo $attOutTime;//Out Time Of Employee
                                                
                                                ?>             
                                            </td>

                                            <td>
                                                <?php  
                                                        /*-------If OT Date is weekly off or holiday-------*/
                                                              if($statustitle == 'weeklyoff' || $statustitle == 'pholiday' || $statustitle == 'POW' || $statustitle == 'POH')
                                                              {     if($attInTime == '00:00' && $attOutTime == '00:00'){
                                                                        $newtime=0;
                                                                    }else{ 
                                                                       $shiftstrart = $shiftout = '';
									$k1=$cal.$attInTime;
									$k2=$cal.$attOutTime;

                                                                        $shiftstrart = new DateTime($k1);
                                                                        $shiftout = new DateTime($k2);
                                                                      	//print_r($shiftstrart);
									//print_r($shiftout);

                                                                        //starttime always less than outtime
                                                                        if(strtotime($k1) < strtotime($k2))
                                                                        { //echo"kk";
                                                                         $interval = $shiftstrart->diff($shiftout);
                                                                         $newtime = $interval->format('%h').":".$interval->format('%i');
                                                                        }
                                                                        else
                                                                        {	//echo strtotime($k1)."  "; //echo strtotime($k2);
													
                                                                           $newtime=0;
                                                                        }
                                                                    }
                                                                    
                                                                    //print_r($shiftout);
                                                              }
                                                              /*------If OT Date is Not weekly off or Holiday--------*/
                                                              else
                                                              {      
                                                                    if($attOutTime == "00:00"){
                                                                    
                                                                        $newtime= $interval;
                                                                    }else{ 
                                                                        $shiftEnd = $OutTime = '';
                                                                        if(!empty($cal) && !empty($shiftTo1)) {
                                                                           //echo $cal.$shiftTo1;
                                                                            $t=$cal.$shiftTo1;

									    //$t=$cal; //.$shiftTo1;
                                                                            $shiftEnd =new DateTime($t);
                                                                            //echo $cal1."ppp";
                                                                            $t2=$cal1; //.$attOutTime;
                                                                            $OutTime =new DateTime($t2);

                                                                        }//var_dump($shiftEnd);var_dump($OutTime);
                                                                        if($shiftEnd < $OutTime)
                                                                       {   
                                                                            $interval = $shiftEnd->diff($OutTime);// var_dump($interval);
                                                                            $newtime=$interval->format('%h').":".$interval->format('%i');
                                                                       }
                                                                       else
                                                                       {
                                                                          $newtime=0;
                                                                       }        
                                                                   }
                                                              }
                                                            
                                                             echo  $newtime; //End Show Actual OT Hours
                                                              
                                                ?>
                                            </td>

                                            <td>
                                                <?php 
                                                     /*---------Start Show Final OT Hours------------*/   
                                                        if(strtotime($OT_overTimeHours[$j]) <= strtotime($newtime)){
                                                           $p2= $OT_overTimeHours[$j];
                                                        }else{
                                                            $p2= $newtime;
                                                        }
                                                        echo $p2;
                                                    /*---------End Show Final OT Hours------------*/
                                                  ?>   
                                            </td>

                                            <?php if($status[$j] == 2 ){?>
                                                <td>
                                                    <span class="label label-success" >
                                                                Approved
                                                    </span>
                                                    
                                                </td>
                                                <td>
                                                    <span class="label label-danger"></span>
                                                </td>
                                                <td><input type="text" name="finalRemark" id="finalRemark" readonly value="<?php echo $finalRemark[$j];?>" >
                                                </td>
                                            <?php }else if($status[$j] == 3){?>
                                                <td>
                                                    <span class="label label-success" ></span>
                                                </td>
                                                <td>

                                                    <span class="label label-danger">
                                                                Rejected 
                                                    </span>
                                                </td>
                                                <td><input type="text" name="finalRemark" id="finalRemark" readonly value="<?php echo $finalRemark[$j];?>" /></td>
                                                                      
                                                </td>
                                            <?php }else{?>
                                                <td>
                                                  <a href="javascript:void(0)" onclick="return finalApproveNew('2','<?php echo $Emp_Code[$j];?>','<?php echo $parts;?>','<?php echo $code;?>','<?php echo $j;?>','<?php echo date('Y-m-d',strtotime($OT_Date[$j]));?>','<?php echo $p2;?>');">
                                                    <span class="label label-success" >
                                                                    Approve 
                                                    </span> </a>
                                                </td>                                   
                                                <td>
                                                    <a href="javascript:void(0)" onclick="return finalApproveNew('3','<?php echo $Emp_Code[$j];?>','<?php echo $parts;?>','<?php echo $code;?>','<?php echo $j;?>','<?php echo "finalRemark".$j;?>','<?php echo date('Y-m-d',strtotime($OT_Date[$j]));?>','<?php echo $p2;?>');">
                                                    <span class="label label-danger">
                                                                Reject 
                                                    </span>
                                                    </a>
                                               
                                                </td>
                                                <td><input type="text" name="finalRemark" id="finalRemark<?php echo $j;?>"></td>
                                            <?php }?>
                                            <td>
                                                <?php 
                                                    /*------Start Add hours-------*/ //echo $newtime."--".$p2;
                                                    if(strtotime($newtime) > strtotime($p2)){
                                                        $cal=date("Y-m-d",strtotime($OT_Date[$j]))." ".$newtime;
                                                        $cal1=date("Y-m-d",strtotime($OT_Date[$j]))." ".$p2;
                                                        $shiftEnd = new DateTime($cal);
                                                        $OutTime = new DateTime($cal1);
                                                        $interval = $shiftEnd->diff($OutTime);
                                                                             $newtime1=$interval->format('%h').":".$interval->format('%i');
                                                        echo $newtime1;
                                                    }
                                                    else
                                                    {

                                                    echo "0";
                                                    }
                                                /*------End Add hours-------*/
                                                ?>
                                            </td>
                                            <?php } ?>
                                            <td></td>
                                    </tr>

                                    <?php } ?>
                                </tbody>

                            </thead>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTENT-->
    </div>
</div>
<?php 
function timeDiff($firstTime,$lastTime) {
    $firstTime=strtotime($firstTime);
    $lastTime=strtotime($lastTime);
    $timeDiff=$lastTime-$firstTime;
    return $timeDiff;
}
?>