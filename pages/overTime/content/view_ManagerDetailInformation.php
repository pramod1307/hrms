<?php
include('../../configdata.php');
include ("../class/OTRequestemployee_class.php");//include ("../class/OTRequest_class.php");
//$OTRequest_class=new OTRequest_class();
 $ManagerId		=	$_POST['ManagerId'];
 $otKey    		= 	$_POST['otkey'];

 
 if(isset($_POST['ManagerId']) && $_POST['ManagerId']== '' && isset($_POST['otkey']) && $_POST['otkey'] == ''){
	 
	 echo "<b>Something wrong on this page!!!!</b>"; die;
 }
	$getDataToTable = new OTRequestemployee_class();
	$getData=$getDataToTable->getManagerDataForAjax($ManagerId,$otKey); 
    //print_r($getData);
	$sql="select Emp_Code  from overtime_Balances WHERE OT_Key='$otKey'";
 //echo $sql;
 $res=query($query,$sql,$pa,$opt,$ms_db);
 $i=0;
 $e=0;
 $a=0;
while($row=$fetch($res)){
    $empname[$i]=$row['Emp_Code'];
    $i++;
}

 $totalEmp=count($empname);

for ($k=0; $k < $totalEmp; $k++) { 
     $sql1="select EMP_NAME + ' (' + Emp_code +')' as name from hrdmastqry WHERE Emp_Code='$empname[$k]'";
    $res1=query($query,$sql1,$pa,$opt,$ms_db);
    while($d1=$fetch($res1)) {
        $data1[$e]=$d1['name'];
        $e++;
    }
}
	
?>

<div class="portlet light bordered">

    <div class="portlet-body form">
		<?php
			while ($row=$fetch($getData)) {
					
		?>	
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <!-- <h3 class="form-section">Address</h3> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Applied Date And time
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['OT_ApplyDateTime']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Requested By
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php 
					$getEmployeeName=$getDataToTable->getEmployeeName($ManagerId);
                    //print_r($getEmployeeName);
					$reqName=$getEmployeeName[1];

                                       echo $reqName.' ('.$ManagerId.')';

  ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                           Cost Center
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  $getCostName=$getDataToTable->getCostName($ManagerId);
                                        
                                    echo $getCostName[1]; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Category:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $getDataToTable->getappliedOT_Category($row['OT_Category']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Shift:</label>
                            <div class="col-md-6">
                                <?php echo $row['OT_Shift']; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">OverTime Date:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['OT_DATE'];?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6"> Over Time Start Time <br> Over Time End Time:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['OT_StartTime']."<br>".$row['OT_EndTime'] ;?>
                                </p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Total Overtime Hours</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row['overTimeHours']; ?> 
                                </p>
                            </div>
                        </div>
                    </div>
					<div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Total No. of Employee</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php for ($l=0; $l <$totalEmp ; $l++) { 
                                        echo $data1[$l]."<br>";
                                    }
                                    
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
					
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Reason:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $getDataToTable->getappliedOT_Reason($row['OT_NewFunction']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php   $remark = $row['App_OTRemark'];
                            $ex_remark = explode(',', $remark);
                            $totalremark = count($ex_remark);
                            if($totalremark >= 1  && ($ex_remark[0] != '' || $row['FirstApprover'] !='')){

                     ?>
                      <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">First Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                       <?php if($row['FirstApprover'] == 0){
echo "";}
else{
echo $row['FirstApprover']; } ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">First Approver Remarks:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $ex_remark[0]; ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    
                    <?php } 
                    if($totalremark >= 2){

                    
                    ?>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Second Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                  <?php if($row['secApprover'] == 0){
echo "";}
else{
echo $row['secApprover']; } ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Second Approver Remarks:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                   <?php  echo $ex_remark[1]; ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    
                    <?php }
                     if($totalremark >=3 ){
                    ?>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Third Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php if($row['thirdApprover'] == 0){
echo "";}
else{
echo $row['thirdApprover']; } ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Third Approver Remarks:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                   <?php  echo $ex_remark[2]; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>




				   
				   <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Status:</label>
                            <div class="col-md-6">
                                <?php 
								if(isset($row['appStatus']) && $row['appStatus'] != ''){
									$status=explode(",", $row['appStatus']); 
									//echo '<pre>'; print_r($status);
									 $totalStatusCount 	=	count($status)-1;
	
										
 										if($status[$totalStatusCount] == "3" || $status[0]=="3" || $status[1]=="3"){ 
                                            						echo "<span style='color:red;'>Rejected </span>";
                                        					}
										else if($status[$totalStatusCount] == "1" || $status[$totalStatusCount] == ""){ 

											echo "<span style='color:blue;'>Pending </span>";
										}else if($status[$totalStatusCount] == "2"){ 
											echo "<span style='color:green;'>Approved </span>";
										}else if($status[$totalStatusCount] == "4"){ 
											echo "<span style='color:red;'>Cancelled </span>";
										}
								}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
          
           
             
        </form>
		<?php
			}
		?>
        <!-- END FORM-->
    </div>
</div>



