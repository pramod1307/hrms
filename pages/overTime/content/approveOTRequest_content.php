
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
       <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog lg">
        <!-- modal-content -->
        <div class="modal-content" >
            <div class="modal-header portlet box blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title white-txt"><div class="caption"><b>Approve OverTime Request</b></div></h4>
            </div>
            <div class="modal-body" id="approveOTRequest">                
                <?php //include ("content/view_myodrequest.php"); ?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
        <!-- /.modal -->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Approve OverTime Request
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">

                            </a>
                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <!--<div class="col-md-12">

                                    <div class="btn-group selectAlign">
                                        <select class="form-control" onchange="getRelatedSelectbox(this.value);">
                                            <option value="">Select....</option>
                                            <option value="1">Request Applied On</option>
                                            <option value="2">Request Status</option>
                                            <option value="3">By Requester Name</option>
                                            
                                        </select>
                                    </div>

                                    <div class="btn-group" id="monthlySearch" style="display: none;margin-left: 45px;">
                                        
                                        <div class="col-md-5">
                                        <label class="control-label">
                                            From Date                
                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="text" class="form-control date_for_search" name="fromDate" id="fromDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-5">  
                                        <label class="control-label">
                                            To Date                
                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="text" class="form-control date_for_search" name="toDate" id="toDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-2" style="margin-top: 25px;">
                                        <button class="btn submit" onclick="searchByDate('<?php echo $code;?>');">Go</button>
                                        </div>
                                     
                                    </div>

                                    <div class="btn-group" id="actionSearch" style="display: none;margin-left: 45px;">
                                        <select class="form-control" onchange="searchByStatus(this.value,'<?php echo $code;?>');">
                                            <option value="" >Select ..</option>
                                            <?php
                                            $sql="select * from LOVMast where LOV_Field='status'";
                                            $result=query($query,$sql,$pa,$opt,$ms_db);
                                            while ($row = $fetch($result)){
                                                ?>
                                                <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                            <?php } ?>

                                        </select>
                                    </div>

                                    <div class="btn-group" id="bynameSearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-10">
                                            <input type="text" name="byname" class="form-control pull-left" id="col5_filter" onchange="getInputValue();" placeholder="Enter Emp code or Name">
                                        </div>
                                            
                                      
                                    </div>

                                    <div class="btn-group" id="byRequesterSearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-10">
                                             <select class="form-control" name="byRequest"  id="byRequest" onchange="serchByCodeName('byRequest');">
                                             <option value="0">Select ..</option>
                                             <?php 
                                                $sqlCrea="select distinct CreatedBy from overtime_Balances where  OT_Approver LIKE  '%$code%'";
                                                $resCrea=query($query,$sqlCrea,$pa,$opt,$ms_db);
                                                while ($rowCrea = $fetch($resCrea)) {
                                                  $arrEmp[]="'" .$rowCrea['CreatedBy'] . "'";
                                                }

                                                $strEmp =  implode(",", $arrEmp) ;

                                                $sqlEmp="Select Emp_Code,EMP_NAME from HrdMastQry where Emp_Code in ($strEmp) ";
                                                $resEmp=query($query,$sqlEmp,$pa,$opt,$ms_db);
                                                while ($rowEmp =$fetch($resEmp)) {?>
                                                    <option value="<?php echo $rowEmp['Emp_Code']?>"><?php echo $rowEmp['EMP_NAME']." (".$rowEmp['Emp_Code'].")";?></option> 
                                              <?php  }
                                                
                                                 ?>
                                             </select>
                                          
                                        </div>
                                             
                                      
                                    </div>

                                </div>-->
                            </div>

                        </div>

                        <?php
                            $getOTKey=$OTRequest_class->getOTKey();
                            $getCostName=$OTRequest_class->getCostName($code);
			    
                            //print_r($getOTKey);

                            $OT_Key=$getOTKey[0]; //print_r($OT_Key);
                            $totalEmp=$getOTKey[1];
                            $status=$getOTKey[2];
                            $approver=$getOTKey[3];
                            $numRow=$getOTKey[4];
                            $firstApp=$getOTKey[5];
                            $secApp=$getOTKey[6];
                            $thirdApp=$getOTKey[7];
                            $remarkApp=$getOTKey[8];

                           
                                                             
                        ?>
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                            <tr><th></th>
                                <th class="table-checkbox">
                                    <input type="checkbox" id="Allcheck" value="0" 
                                    onclick="allOTCheck('Allcheck','<?php echo $code;?>');" class="group-checkable" data-set="#sample_2 .checkboxes"/>
                                </th>
                                

                                <th>
                                    Apply Date
                                </th>
                                <th>
                                    Requested By
                                </th>
                                <th>
                                    Cost Center
                                </th>
                                 <th>
                                    OT Type
                                </th>
                                <th>
                                    Category
                                </th>
				                <th>
                                    Shift
                                </th>
                                 <th>
                                   OT Date
                                </th>
                                 
                                 <th>
                                   OT Start Time
                                </th>
                                <th>
                                    OT End Time
                                </th>
                                <th>
                                    Total OT Hours
                                </th>
                                <th>
                                    Total Number Of Employee
                                </th>

                                <th>Reason</th>
                                <th>
                                    1st Approved On
                                </th>
                                <th>
                                    1st Approved Remarks
                                </th>
                                <th>
                                    2nd Approved On
                                </th>
                                <th>
                                    2nd Approved Remarks
                                </th>
                                <th>
                                    3rd Approved On
                                </th>
                                <th>
                                    3rd Approved Remarks
                                </th>

                                <th>status</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody id="searchData">
                            <?php echo $numRow;
                                for($j=0; $j<$numRow; $j++){

                                $arrAppr=explode(",", $approver[$j]);// print_r($arrAppr);
                                $appkey=array_search($code, $arrAppr);

                                $arrStatus=explode(",", $status[$j]);
                                $arrRemark=explode(",", $remarkApp[$j]);                                

                                // approver level key 0-first,1- seceon,2-3rd level
                                if($appkey == 0 ){ 
                               	$sql="select top 1 replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as applyDate,
                                    replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7) as First_Approver,
                                    replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7) as sec_Approver,
                                    replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as third_Approver,OT_Category,OT_Shift,REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE, OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,appStatus,*                        
                                    from overtime_Balances where OT_Approver  LIKE '%$code%' and OT_Key='$OT_Key[$j]' and flag in ('R','2','0','2R','3R','F','3') order by OT_ApplyDateTime Desc"; 

                                }else if($appkey == 1 ){
                                   $sql="select top 1 replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as applyDate,
                                    replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7) as First_Approver,
                                    replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7) as sec_Approver,
                                    replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as third_Approver,OT_Category,OT_Shift,REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE, OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,appStatus,*                 
                                    from overtime_Balances where OT_Approver  LIKE '%$code%' and OT_Key='$OT_Key[$j]' and flag in ('2','3','2R','3R','F')  order by Id Desc ";

                                }else if($appkey == 2){
                                  $sql="select top 1 replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as applyDate,
                                    replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7) as First_Approver,
                                    replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7) as sec_Approver,
                                    replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as third_Approver,OT_Category,OT_Shift,REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE, OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,appStatus,*    
                                    from overtime_Balances where OT_Approver  LIKE '%$code%' and OT_Key='$OT_Key[$j]' and flag in('3','3R','F') order by Id Desc ";
                                }

                               //echo $sql;
                                $res=query($query,$sql,$pa,$opt,$ms_db);
                                $len = $num($res);
                            
                            
                                $i=0;
                              
                                while ($row= $fetch($res)) { 
                                    $ap=explode(",", $row['OT_Approver']);
                                    $apst=explode(",", $row['appStatus']);
                                    $apremark=explode(",", $row['App_OTRemark']);            
                                    $aplevel;
                                    $stlevel;
                                    for($a=0;$a<count($ap); $a++){
                                        if($code == $ap[$a]){
                                            $aplevel=$a;
                                            $stlevel=$apst[$a];
                                            break;
                                        }
                                    }

				                $getEmployeeName=$OTRequest_class->getEmployeeNameByID($row['CreatedBy']);
				                //print_r($getEmployeeName);
				                $reqName=$getEmployeeName[1];
                                    
                            ?>
                            
                            <tr class="odd gradeX">
                               <td></td>
                                <td>
                                <?php if($stlevel == 1){?>
                                <input type="checkbox" class="checkboxes" id="<?php echo "MulOtcheck".$i;?>" onclick="mulCheck('<?php echo "MulOtcheck".$i;?>');" value="<?php echo $row['OT_Key'];?>"/>
                                <?php }?>
                                <input type="hidden" id="<?php echo "approverid".$i;?>" value="<?php echo $row['OT_Approver'];?>">

                                </td>
                                
                                
                                <td>
                                    <?php echo $row['applyDate'];?>
                                </td>
                                
                                <td>
                                    <?php echo $reqName."(".$row['CreatedBy'].")";?>
                                </td>
                                <td><?php echo $getCostName[1];?></td>
                                <td>
                                    <?php if($row['OT_Type']=='1')
                                     { echo "Deviation";}else{echo "Standard";}?>
                                </td>
                                <td>
                                    <?php echo $otcategory=$OTRequest_class->getappliedOT_Category($row['OT_Category']);?>
                                </td>
                                <!--<td>
                                    <?php if($row['OT_Type']=='1')
                                     { echo "Deviation";}else{echo "Standard";}?>
                                </td>-->
								<td>
                                    <?php echo $row['OT_Shift'] ;?>
                                </td>
                                 <td >
                                   <?php echo $row['OT_DATE'] ;?>
                                </td>
                                
                                <td>
                                    <?php
                                   
                                     echo $row['OT_StartTime'];
                                    ?>

                                </td>
                                <td>
                                    <?php echo $row['OT_EndTime'];?>
                                </td>
                                <td>
                                    <?php echo $row['overTimeHours'];?>
                                </td>

                               
                                <td>
                                    <?php echo $totalEmp[$j];?>   
                                </td>

                                <td>  
                                    <?php echo $otpurpose=$OTRequest_class->getappliedOT_Reason($row['OT_NewFunction']);?> 
                                </td>
                                 <td>
                                 <?php if($row['First_Approver'] == 0){
                                    echo "";}
                                    else{
                                        echo $row['First_Approver']; 
                                    }?>
                                </td>
                                <td>
                                    <?php if(isset($apremark[0]))
                                    echo $apremark[0]; ?>
                                </td>
                                <td>
                                    <?php if($row['sec_Approver'] == 0){
                                    echo "";}
                                    else{
                                        echo $row['sec_Approver']; }
                                    ?>
                               </td>
                                <td>
                                     <?php if(isset($apremark[1]))
                                    echo $apremark[1]; ?>  
                                </td>
                                <td>
                                  <?php if($row['third_Approver'] == 0){
                                        echo "";}
                                    else{
                                    echo $row['third_Approver']; }?>
                                </td>
                                <td>
                                     <?php if(isset($apremark[2]))
                                    echo $apremark[2]; ?>  
                                </td>

                                <td> <?php $st=explode(",", $row['appStatus']);
                                          //echo $st[0]; 
                                          $stlen=count($st);
                                      ?>
                                    <?php if($st[0] == "3") {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                                            onclick="getapprKey('<?php echo $OT_Key[$j];?>','3','<?php echo $code;?>','<?php echo $row['OT_Approver'];?>');">
                                            <span class="label label-danger">
                                            Rejected
                                            </span>
                                    <?php } else if($st[$appkey] == "1" || $st[$appkey] == ""
                                        /*$st[$stlen-1] == "1" || $st[$stlen-1] == ""*/){?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                                            onclick="getapprKey('<?php echo $OT_Key[$j];?>','1','<?php echo $code;?>','<?php echo $row['OT_Approver'];?>');">
                                            <span class="label bg-blue-steel">
                                                Pending 
                                            </span>
                                    <?php } else if($st[$appkey] == "2"/*$st[$stlen-1] == "2"*/) {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                                            onclick="getapprKey('<?php echo $OT_Key[$j];?>','2','<?php echo $code;?>','<?php echo $row['OT_Approver'];?>');">
                                            <span class="label label-success">
                                                Approved 
                                            </span>
                                    <?php } else if($st[$appkey] == "3"/*$st[$stlen-1] == "3"*/) {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                                            onclick="getapprKey('<?php echo $OT_Key[$j];?>','3','<?php echo $code;?>','<?php echo $row['OT_Approver'];?>');">
                                            <span class="label label-danger">
                                            Rejected
                                            </span>
                                    <?php } else if($st[$appkey] == "4"/*$st[$stlen-1] == "4"*/) {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprKey('<?php echo $OT_Key[$j];?>','4','<?php echo $code;?>','<?php echo $row['OT_Approver'];?>');">
                                <span class="label bg-grey-cascade">
                                Cancelled </span>
                                 <?php } else if($st[$appkey] == "5"/*$st[$stlen-1] == "5"*/) {?>
                                        <a class="myod" data-toggle="modal" href="#large" 
                            onclick="getapprKey('<?php echo $OT_Key[$j];?>','5','<?php echo $code;?>','<?php echo $row['OT_Approver'];?>');">
                                <span class="label bg-grey-cascade">
                                Cancelled Requested </span>
                                    <?php }?>  
                                    <?php //if() $row['appStatus'];?>
                                </td>

                                <td><input type="hidden" value="
                                <?php echo $row['OT_Key'];?>" 
                                name="key" id="key<?php echo $i;?>">
                                </td>
                            </tr>

                            <?php $i++; } }?>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-md-6">
                            <textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
                        </div>
						    
                        </div>
                            <div class="col-md-12">
                                <div class="col-md-offset-4 col-md-4 col-md-offset-4" style="margin-top: 20px;">
                                <input type="hidden" value="" id="selectedcheckbox" />
                                
                                <select class="form-control" id="selected_status_div" onchange="showSubmitButton(this.value,'<?php echo $len;?>');">
                                    <option value="">Select Action..</option>
                                    <?php
                                        $sql="select * from LOVMast where LOV_Field='status' and LOV_Value in ('2','3')";
                                        $result=query($query,$sql,$pa,$opt,$ms_db);
                                       while ($row = $fetch($result)){
                                    ?>
                                    <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                    <?php } ?>
                                </select>
                                    </div>
                                <div class="col-md-offset-4" id="submitButtonDiv" style="display: none; margin-top: 19px;">
                                    <button class="btn btn-primary" id="submitButton" value="" onclick="submitApprovelRequest(this.value,'<?php echo $code;?>','<?php echo $aplevel;?>');">Submit</button>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTENT-->
    </div>
</div>