<div class="page-content-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="page-content">
        <!--modal-dialog -->
        <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog lg">
                <!-- modal-content -->
                <div class="modal-content" >
                    <div class="modal-header portlet box blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title white-txt"><div class="caption"><span>My OT Request -Employee View </span></div></h4>
                    </div>
                    <div class="modal-body" id="myleave">
                        <?php //include ("content/view_myodrequest.php"); ?>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- /.modal-dialog -->
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN CONDENSED TABLE PORTLET-->
                <div class="portlet box blue">

                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>My OT Request -Employee View
                        </div>
                      
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-12">

                                    
                                    <div class="btn-group" id="monthlySearch" style="display: none;margin-left: 45px;">
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                From Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="fromPastDate" id="fromDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-5">
                                            <label class="control-label">
                                                To Date
                                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" class="form-control date_for_search" name="toPastDate" id="toDate" placeholder="dd/mm/yy">
                                        </div>
                                        <div class="col-md-2" style="margin-top: 25px;">
                                        <button class="btn" onclick="">Go</button>
                                        </div>
                                    </div>

                                    <div class="btn-group" id="actionSearch" style="display: none;margin-left: 45px;">
                                        <select class="form-control" onchange="">
                                            <option value="">Select ..</option>
                                            <?php /*
                                            $sql="select * from LOVMast where LOV_Field='status'";
                                            $result=query($query,$sql,$pa,$opt,$ms_db);
                                            while ($row = $fetch($result)){
                                                ?>
                                                <option value="<?php echo $row['LOV_Value']?>"><?php echo $row['LOV_Text']?></option>
                                            <?php } 
											*/
											?>

                                        </select>
                                    </div>

                                    <div class="btn-group " id="bynameSearch" style="display: none;margin-left: 45px;">

                                        <input type="text" name="byname" class="form-control pull-left" id="byname" onchange="getInputValue(this.value);" placeholder="Enter Emp code or Name">


                                        <button class="pull-right" id="inputvalue" value=""
                                                onclick="serchByCodeName(this.value,'<?php echo $code;?>');">Go</button>

                                    </div>

                                </div>
                            </div>

                        </div>
						
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                           
                            <thead >
                            <tr>
                                <th>S.No</th>
                                <th>
                                    Applied Date And time
                                </th>
                                <th>
                                    Requested By
                                </th>
                                <th>Cost Center</th>
                                <th>
                                   Category
                                </th>

                                <th>
                                    Shift
                                </th>

                                <th>
                                    OT Date
                                </th>
                                 <th>
                                    OT Start Time
                                </th>
                                   <th>
                                    OT End Time
                                </th>
                                <th>
                                    Total OT Hours
                                </th>
				                <th>
                                    Final OT Hours
                                </th>

                                <th style="display:none;">
                                    OT Balances
                                </th>
									                           
                                <th>
                                    Reason
                                </th>
                                <th>
                                    1st Approved On
                                </th>
                                <th>
                                    1st Approved Remarks
                                </th>
                                <th>
                                    2nd Approved On
                                </th>
                                <th>
                                    2nd Approved Remarks
                                </th>
                                <th>
                                    3rd Approved On
                                </th>
                                <th>
                                    3rd Approved Remarks
                                </th>
                                
                            </tr>
                            </thead>
                            <tbody id="searchMyData">
								<?php
										if(isset($_SESSION['usercode']) && $_SESSION['usercode'] != ''){
											$id = $_SESSION['usercode'];
											
										}else{
											echo "Somthing wrong on this page sorry!!!!"; die;
										}
						
										//echo '<pre>'; print_r($_SESSION); echo '</pre>'; 
										$getDataToTable = new OTRequestemployee_class(); 
										$getData=$getDataToTable->getData($id); 
                    					$getCostName=$getDataToTable->getCostName($id);
                    					//print_r($getData); 
                                        $i=1;
										while ($row=$fetch($getData)) {
                                        $DataStatus= $row['appStatus'];
                                        $status_val =explode(',', $DataStatus);
                                        $DataRemark= $row['App_OTRemark'];
                                        $remark_val =explode(',', $DataRemark);
								?>
					               <tr><td><?php echo $i;?></td>
                                    <td>
									<?php  echo $row['OT_ApplyDateTime']; ?>
									</td>
                                    
                                    <td>
                                    <?php  $getEmployeeName=$getDataToTable->getEmployeeName($row['CreatedBy']);
					                       $reqName=$getEmployeeName[1];

                                       echo $reqName.' ('.$row['CreatedBy'].')';
                                    ?>
                                    </td>
                                    <td><?php echo $getCostName[1];?></td>
                                    <td>
                                    <?php  echo $getDataToTable->getappliedOT_Category($row['OT_Category']); ?>   
                                    </td>
                                    <td>
                                     <?php  echo $row['OT_Shift']; ?>  
                                    </td>
                                    <td>
                                     <?php  echo $row['OT_DATE']; ?>  
                                    </td>
                                    <td>
                                    <?php  echo $row['OT_StartTime']; ?>   
                                    </td>
                                     <td>
									<?php  echo $row['OT_EndTime']; ?>
                                    </td>
                                    <td>
                                    <?php  echo $row['overTimeHours']; ?>   
                                    </td>
				                    <td>
                                        <?php echo $row['finalOtHour'];?>
                                    </td>
				                    <td style="display:none;">
                                        <?php echo  $getDataToTable->getEmployeeOT_Balances($id,date('Y-m-d',strtotime($row['OT_DATE'])));?> 
                                   </td>
                                    <td>
                                    <?php  echo $getDataToTable->getappliedOT_Reason($row['OT_NewFunction']); echo $row['OT_NewFunction']; ?>   
                                    </td>
                                    <td>
                                    <?php if($row['FirstApprover'] == 0){
                                        echo "";}
                                        else{
                                        echo $row['FirstApprover']; }?>
                                    </td>

                                    <td>
                                    <?php if(isset($remark_val[0]))
                                    echo $remark_val[0]; ?> 
                                    </td>
                                    <td>

                                    <?php if($row['secApprover'] == 0){
                                        echo "";}
                                        else{
                                        echo $row['secApprover']; }?>
                                  
                                    </td>
                                    <td>
                                    <?php if(isset($remark_val[1]))
                                        echo $remark_val[1]; ?>  
                                    </td>
                                    <td>
                                    <?php if($row['thirdApprover'] == 0){
                                        echo "";}
                                        else{
                                        echo $row['thirdApprover']; }?>
                                                                            
                                    </td>
                                    <td>
                                        <?php if(isset($remark_val[2]))
                                            echo $remark_val[2]; ?>
                                    </td>
                                    
                                </tr>
								<?php
									$i++;	}
								?>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END CONDENSED TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>

