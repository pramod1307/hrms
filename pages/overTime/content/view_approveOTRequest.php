<?php
include('../../configdata.php');
 include ("../class/OTRequest_class.php");
 $OTRequest_class=new OTRequest_class();

 $otkey=$_POST['otkey'];
 $appStatus=$_POST['status'];
 $loginCode=$_POST['code'];//login code
 $apprCode=$_REQUEST['approverid'];
 $aplevel;
 $stlevel;

 $sql="select Emp_Code  from overtime_Balances WHERE OT_Key='$otkey'";
 //echo $sql;
 $res=query($query,$sql,$pa,$opt,$ms_db);
 $i=0;
 $e=0;
 $a=0;
while($row=$fetch($res)){
    $empname[$i]=$row['Emp_Code'];
    $i++;
}

 $totalEmp=count($empname);

for ($k=0; $k < $totalEmp; $k++) { 
     $sql1="select EMP_NAME + '-' + Emp_code as name from hrdmastqry WHERE Emp_Code='$empname[$k]'";
    $res1=query($query,$sql1,$pa,$opt,$ms_db);
    while($d1=$fetch($res1)) {
        $data1[$e]=$d1['name'];
        $e++;
    }
}



 $actionUserCode=explode(",", $apprCode);
 $level=count($actionUserCode); 
 for($j=0; $j<$level; $j++){

     $sql2="select EMP_NAME,DSG_NAME, MailingAddress,EmpImage from hrdmastqry WHERE Emp_Code='$actionUserCode[$j]'";
    $res2=query($query,$sql2,$pa,$opt,$ms_db);
    while ($d2=$fetch($res2)) {
        $dataimg[$j]=$d2['EmpImage'];
        $dataDsgName[$j]=$d2['DSG_NAME'];
        $dataMailAddr[$j]=$d2['MailingAddress'];
        $dataEmpName[$j]=$d2['EMP_NAME'];
    }
     

}


$sql3="select top 1 REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE,
replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7)
          as applyDate,
         replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7)
          as First_Approver,
         replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7)
          as sec_Approver,
         replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as third_Approver,* from overtime_Balances where OT_Key='$otkey'";
 $res3=query($query,$sql3,$pa,$opt,$ms_db);

 $row3=$fetch($res3);



?>

<div class="portlet light bordered">

    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <!-- <h3 class="form-section">Address</h3> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Applied Date:
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php
                                   
                                    echo $row3['applyDate'];
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Requested By:
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php
$getEmployeeName=$OTRequest_class->getEmployeeNameByID($row3['CreatedBy']);
								$reqName=$getEmployeeName[1];

                                    echo $reqName.' ('.$row3['CreatedBy'].')';
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">
                            Cost Center:
                            </label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php

                                   $getCostName=$OTRequest_class->getCostName($row3['CreatedBy']);
                                    echo $getCostName[1];
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Category:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php
                                  
                                    echo $OTRequest_class->getappliedOT_Category($row3['OT_Category']);
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Shift:</label>
                            <div class="col-md-6">
                                <?php echo $row3['OT_Shift']; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">OverTime Date:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row3['OT_DATE'];?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">OT Start Time <br> End Time:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $row3['OT_StartTime']."<br>".$row3['OT_EndTime'] ;?>
                                </p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Employee's Name:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php for ($l=0; $l <$totalEmp ; $l++) { 
                                        echo $data1[$l]."<br>";
                                    }
                                    
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Reason:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $getReason=$OTRequest_class->getappliedOT_Reason($row3['OT_NewFunction']);?>
                                </p>
                            </div>
                        </div>
                    </div>
                      <?php   $remark = $row3['App_OTRemark'];
                            $ex_remark = explode(',', $remark);
                            $totalremark = count($ex_remark);
                            if($totalremark >= 1  && ($ex_remark[0] != '' || $row['First_Approver'] !='')){

                     ?>
                      <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">First Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  if($row3['First_Approver'] == 0){
echo "";}
else{
echo $row3['First_Approver']; } ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">First Approver Remarks:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                    <?php  echo $ex_remark[0]; ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    
                    <?php } 
                    if($totalremark >= 2){

                    
                    ?>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Second Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                   <?php  if($row3['sec_Approver'] == 0){
echo "";}
else{
echo $row3['sec_Approver']; }
 ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Second Approver Remarks:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                   <?php  echo $ex_remark[1]; ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    
                    <?php }
                     if($totalremark >=3 ){
                    ?>
                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Third Approved On:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                   <?php if($row3['third_Approver'] == 0){
echo "";}
else{
echo $row3['third_Approver']; } ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group m-0">
                            <label class="col-md-6">Third Approver Remarks:</label>
                            <div class="col-md-6">
                                <p class="form-control-static">
                                   <?php  echo $ex_remark[2]; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>


                    <?php 
                        $st=explode(",", $row3['appStatus']);
                       // print_r($st); //echo $level;
                    for($m=0; $m<$level; $m++){?>
                    <div class="row">
                    <div class="col-md-12">

                        <div class="col-md-6">
                         
                        <?php if($st[$m] == "1" || $st[$m] == ""){ 
                            echo "<span style='color:blue;'>Pending </span>";
                        }else if($st[$m] == "2"){ 
                            echo "<span style='color:green;'>Approved </span>";
                        }else if($st[$m] == "3"){ 
                            echo "<span style='color:red;'>Rejected </span>";
                        }else if($st[$m] == "4"){ 
                            echo "<span style='color:red;'>Cancelled </span>";
                        }?>
                        </div>

                        <div class="col-md-6">
                        <span class="appMan blue-bg" style="width:250px;">
                        <?php if($dataimg[$m] == ""){?>
                            <span class="appManImg" >
                            <img class="img-circle img50" src="../Profile/upload_images/change_img.png" >
                            </span>
                        <?php }else{?>
                            <span class="appManImg" >
                            <img class="img-circle img50" src="../Profile/upload_images/<?php echo $dataimg[$m];?>" >
                            </span>
                        <?php   } ?>
                        <span class="appManName" data-des="<?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($dataDsgName[$m]))));?>">
                        <?php echo str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($dataEmpName[$m]))));?>
                        </span>
                        </span>
                        </div>
                    </div>
                </div>
                <?php }?>

                </div>
            </div>
            
            <?php for($n=0; $n<$level; $n++){
                        if($loginCode == $actionUserCode[$n]){
                             $aplevel=$n;
                             $stlevel=$st[$n];
                             break;
                        }
                    } 
            if($stlevel == "1" || $stlevel == ""){ ?>   
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                             <div class="col-md-6">
                            <textarea class="form-control input-medium" name="actRemarks" id="actRemarks" placeholder="Please Enter Remarks"></textarea>
                        </div>
                            <div class="col-md-3"> 
                            <input type="hidden" id="selectedcheckbox" value="<?php echo $row3['OT_Key'];?>">     
                            <button type="button" onclick="submitApprovelRequest('2','<?php echo $loginCode;?>','<?php echo $aplevel;?>');" class="btn default green">Approved</button>
                            </div>

                            <div class="col-md-3">      
                            <button type="button" onclick="submitApprovelRequest('3','<?php echo $loginCode;?>','<?php echo $aplevel;?>');" class="btn default red">Rejected
                            </button>
                            </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
          
            <?php }
            if($stlevel == "5"){ ?>   
                <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                           
                            <div class="col-md-3"> 
                            <input type="hidden" id="emp_ID" value="<?php echo $row3['OT_Key'];?>">     
                            <button type="button" onclick="submitApprovelRequest('emp_ID','4','1');" class="btn default green">Approved</button>
                            </div>

                          
                                
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
          
            <?php } ?>
             
        </form>
        <!-- END FORM-->
    </div>
</div>



