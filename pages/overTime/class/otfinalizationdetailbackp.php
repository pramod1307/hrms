<?php

class OT_Finalization_class{
    function __construct(){
      
    }

    function getFinYear(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select FY,FY_Type,convert(varchar(10),FY_Start,103)as FY_Start from FinYear where FY_Type='F' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        while ($row=$fetch($result)) {
            $FinalYear=$row['FY'];
            $FY_Type=$row['FY_Type'];
            $FY_Start=$row['FY_Start'];
            $i++;
        }
        return array($FinalYear,$FY_Type,$FY_Start,$i);
    }

    function getForm(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc from TablesFields where Table_Name='overTime' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_Desc[$i]=$row['Field_Desc'];
           
            $i++;
        }

        return array($Field_Desc,$i);
    }

    function getTable(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc from TablesFields where Table_Name='overTimeFinal' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_TDesc[$i]=$row['Field_Desc'];
            $i++;
        }

        return array($Field_TDesc,$i);
    } 
   
    function getFornValue(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select * from overtime_balances";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_TDesc[$i]=$row['Field_Desc'];
            $i++;
        }

        return array($Field_TDesc,$i);

    }

    function getCostName($mngrcode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select COST_CODE,COST_NAME from hrdmastqry where Emp_Code='$mngrcode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);
        
        return array($row['COST_CODE'],$row['COST_NAME']);

    }

    /*--------Start OT Finalization List Function----------*/

    function getOTKeyFinal($code){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select OT_Key,cast(OT_ApplyDateTime as date) from overtime_Balances where CreatedBy='$code' group by OT_Key,cast(OT_ApplyDateTime as date) order by cast(OT_ApplyDateTime as date) desc";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row= $fetch($result)) {
            $OT_Key[$i]=$row['OT_Key'];
            $i++;
        }

        return array($OT_Key,$i);


    }

    function getappliedOT_Category($catID){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select LOV_Text from LOVMast where LOV_Field='OTCategory' AND LOV_VAlue='$catID'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
       $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getappliedOT_Purpose($purId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTPurpose' AND LOV_Value='$purId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getappliedOT_NewRequest($newReqId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTNewReqQues' AND LOV_Value='$newReqId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getDetailsFinal($OT_Key,$code){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

      $sql="select top 1  REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OTDATE,
      replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as OTApplyDateTime
,* from overtime_Balances where CreatedBy='$code' and OT_Key='$OT_Key'";

        //$sql="select OT_Key from overtime_Balances where CreatedBy='$code' group by OT_Key";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row= $fetch($result)) {
            $OT_Category=$row['OT_Category'];
            $OT_Date=$row['OTDATE'];
            $OT_Shift=$row['OT_Shift'];
            $OT_StartTime=$row['OT_StartTime'];
            $OT_EndTime=$row['OT_EndTime'];
            $OT_overTimeHours=$row['overTimeHours'];
            $OT_purpose=$row['OT_purpose'];
            $OT_AppyDateTime=$row['OTApplyDateTime'];
            $OT_NewFunction=$row['OT_NewFunction'];
            $OT_Other=$row['OT_Other'];
            $i++;
        }

        return array($OT_Category,$OT_Date,$OT_Shift,$OT_StartTime,$OT_EndTime,$OT_overTimeHours,$OT_purpose,$OT_AppyDateTime,$OT_NewFunction,$OT_Other,$i);
    }


    function getEmpTableDetails($otkey,$code){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OTDATE,
replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as OTApplyDateTime,* from overtime_Balances where CreatedBy='$code' AND OT_Key='$otkey'";
      $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row= $fetch($result)) {
            $Emp_Code[$i]=$row['Emp_Code'];
            $OT_Date[$i]=$row['OTDATE'];
            $OT_StartTime[$i]=$row['OT_StartTime'];
            $OT_EndTime[$i]=$row['OT_EndTime'];
            $OT_overTimeHours[$i]=$row['overTimeHours'];
            $OT_AppyDateTime[$i]=$row['OTApplyDateTime'];
            $status[$i]=$row['status'];
            $finalRemark[$i]=$row['finalRemark'];
            $i++;
        }

        return array($Emp_Code,$OT_Date,$OT_StartTime,$OT_EndTime,$OT_overTimeHours,$OT_AppyDateTime,$status,$finalRemark,$i);

    }

    function getEmployeeShiftByDate($empCode, $date,$othours1){
        $date = date('Y-m-d',strtotime($date));
        if(empty($empCode) || empty($date)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        $date1=$date;
        $explodeDate = explode('-', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];


        $SqlQuery =  "SELECT rost.EMP_CODE, 
        convert(varchar(10),RosterStart,103) as RosterStart,
        convert(varchar(10),RosterEnd,103) as RosterEnd,
        convert(char(5),rost.SHIFT_TO) AS SHIFT_TO,  
        convert(char(5),c.IN_TIME,108) AS IN_TIME, 
        convert(char(5),c.OUT_TIME,108) AS OUT_TIME,convert(char(16),OUT_TIME,121) as new_out
        FROM rosterQry rost
        LEFT JOIN CAttendanceqry C ON LEFT(CONVERT(VARCHAR,C.ATTDATE, 120), 10)='$date1' AND c.emp_code=rost.emp_code  
        WHERE '$convertDate' BETWEEN RosterStart AND RosterEnd AND rost.EMP_CODE ='$empCode'
        ";


        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
           $i=0;
        while ($row = $fetch($result)) {
            
		
		  $sql1="select intime,outtime,AttnKey,COUNT(AttnKey) as countno from markPastAttendance where  '$date1' between CONVERT (VARCHAR(10),date_from,121) and CONVERT (VARCHAR(10),date_to,121) and CreatedBy='$empCode' group by AttnKey,intime,outtime"; 
		$sql11result = query($query,$sql1,$pa,$opt,$ms_db);
		$sql111row = $fetch($sql11result);
		$count1=$sql111row['countno'];
		if($count1 > 0)
		{
			$Shift_To=$row['SHIFT_TO'];
			$in_time=date("H:i",strtotime($sql111row['intime']));
			$out_timep=date("H:i",strtotime($sql111row['outtime']));
			$out_time=$out_timep;
			$out_time_new=$date1.' '.$out_timep;

		}
        else
        {
             $out_timep=$row['new_out'];
           
            $Shift_To=$row['SHIFT_TO'];
            $in_time=$row['IN_TIME'];
            $getActualPunchTimeQuery = "SELECT convert(char(16),Outtime,121) as Outtime FROM fnPunchTime('".$empCode."','".$date1."')"; 
            
            $getActualPunchTimeResult = query($query,$getActualPunchTimeQuery,$pa,$opt,$ms_db);
            $count11=0;
                if ($getActualPunchTimeRow=$fetch($getActualPunchTimeResult)) {
    		        $temp=$getActualPunchTimeRow["Outtime"];
                   $count11++;
                }
            //echo $num($getActualPunchTimeResult);
            if($count11 > 0){
            //$getActualPunchTimeRow  = $fetch($getActualPunchTimeResult);
            //echo "aa".$getActualPunchTimeRow["Outtime"];
            $out_time11 = (!empty($temp)) ? $temp: $out_timep;
 	       $rostersql = "select top 1 convert(char(5),ShiftEnd_SwTime,108) AS ShiftEnd_SwTime,convert(char(5),SHIFT_TO,108) AS Shiftto from RosterQry where  '$date1' between CONVERT (VARCHAR(10),RosterStart,121) and CONVERT (VARCHAR(10),RosterEnd,121) and EMP_CODE='$empCode' order by RostID DESC"; 
            $rosterResult = query($query,$rostersql,$pa,$opt,$ms_db);
        	$rowq00= $fetch($rosterResult);
            $siftendswap=$rowq00['ShiftEnd_SwTime'];
        	$otTimeArray = explode(":",$othours1);
        	$endshifttime=$date1.' '.$rowq00['Shiftto'];
        	$newtemphours=date('Y-m-d H:i',strtotime('+'. $otTimeArray[0].' hour +'. $otTimeArray[1].' minutes',strtotime($endshifttime)));
	        $swapArray = explode(":",$siftendswap);
            $newtemphours=date('Y-m-d H:i',strtotime('+'. $swapArray[0].' hour +'. $swapArray[1].' minutes',strtotime($newtemphours)));
            //print_r($newtemphours);
if(strtotime($newtemphours) >= strtotime($out_time11)) 
{
            $out_time_new=$out_time11;
            $out_time11 = strtotime($out_time11);
            $out_time= date('H:i', $out_time11);
}
}
            }
          // echo $out_time;
            $i++;
        }

        return array(
            $Shift_To,
            $in_time,
            $out_time,
            $i,
            $out_time_new
        );
    }

    

/*--------End OT Finalization List Function----------*/

    function getEmployeeNameByID($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select Emp_Code,Emp_Name from EmpNameQry where EMP_CODE ='$empCode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        while ($row=$fetch($result)) {
            $Emp_Code=$row['Emp_Code'];
            $Emp_Name=$row['Emp_Name'];
            
        }

        return array($Emp_Code,$Emp_Name,$i);
    }

}

?>