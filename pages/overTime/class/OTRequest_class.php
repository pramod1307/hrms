<?php //ini_set('display_errors', 'on'); error_reporting(E_ALL);
//include ('../../configdata.php');
ini_set('max_execution_time', 0);
ini_set("memory_limit", "-1");
ini_set("output_buffering", "On");
ini_set("upload_max_filesize", "20M");
include ('../languages/overTimeMail/oTMail.php');
include ('../../global_class.php');
include ('../../Attendance/Events/holidays-v2.php');
include ('../../Attendance/Events/weekyOff-v2.php');
include ('../../Attendance/Events/empAttendance-v2.php');
include ('../../Attendance/Events/empLeaves-v2.php');

class OTRequest_class{
    function __construct(){
      
    }


/*--------Start OT Request Function------------*/
function getEmployeeShiftByDateNew($empCode, $date){
        $date = trim($date);
        if(empty($empCode) || empty($date)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $SqlQuery = "select RosterName from ( (select a.Emp_Code,convert(varchar(10),a.start_rost,103) as start_rost,convert(varchar(10),a.end_rost,103) as 
        end_rost,a.RosterName,a.auto_period,a.created_on from Roster_schema a inner join (select max(RostID) as RostID from Roster_schema 
        where Emp_Code IN ('".$empCode."') and end_rost <= '".$date."' group by Emp_Code) b on a.RostID = b.RostID) 
        union 
        (select Emp_Code, convert(varchar(10),start_rost,103) as start_rost,convert(varchar(10),end_rost,103) as end_rost,RosterName,auto_period,created_on 
        from Roster_schema where Emp_Code IN ('".$empCode."') and start_rost >= '".$date."' and end_rost <= '".$date."' )) a order by a.created_on asc";
        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
        $returnData = array();
        if($num($result) > 0){
            $row = $fetch($result);
            $rosterName = $row['RosterName'];
            $SqlShiftQuery = "  SELECT ShiftMast.Shift_Name, ShiftMast.Shift_From, ShiftMast.Shift_To FROM att_roster 
            INNER JOIN ShiftMast ON att_roster.shiftMaster = ShiftMast.ShiftMastId
            WHERE roster = '".$rosterName."'";
            $SqlShiftResult=query($query, $SqlShiftQuery, $pa, $opt, $ms_db);
            if($num($SqlShiftResult) > 0){
                $finalResult = $fetch($SqlShiftResult);
                $returnData['shiftName']    = $finalResult['Shift_Name'];
                $returnData['shiftInOutTime']='('.date('h:i A', strtotime($finalResult['Shift_From'])).'-'.date('h:i A', strtotime($finalResult['Shift_To'])).')';
            }
        }
        return $returnData;
    }

    function countNumberOfDaysFromTwoDates($firstDate, $secondDate){
        return abs(floor(strtotime($firstDate) / (60 * 60 * 24)) - floor(strtotime($secondDate) / (60 * 60 * 24)));
    }

    function getFinYear(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select FY,FY_Type,convert(varchar(10),FY_Start,103)as FY_Start from FinYear where FY_Type='F' and FY='".date('Y')."' "; 
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $FinalYear=$row['FY'];
            $FY_Type=$row['FY_Type'];
            $FY_Start=$row['FY_Start'];
            $i++;
        }
        return array($FinalYear,$FY_Type,$FY_Start,$i);
    }

    function getForm(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc,Field_Len,DefaultValue,Nullable,MinLength from TablesFields where Table_Name='overTime' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_Desc[$i]=$row['Field_Desc'];
            $Field_Len[$i]=$row['Field_Len'];
            $DefaultValue[$i]=$row['DefaultValue'];
            $Nullable[$i]=$row['Nullable'];
            $MinLength[$i]=$row['MinLength'];
            $i++;
        }

        return array($Field_Desc,$Field_Len,$DefaultValue,$Nullable,$MinLength,$i);
    }

    function getTable(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc,Field_Len,DefaultValue,Nullable,MinLength from TablesFields where Table_Name='OverTime_Table' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_TDesc[$i]=$row['Field_Desc'];
            $Field_TLen[$i]=$row['Field_Len'];
            $DefaultTValue[$i]=$row['DefaultValue'];
            $TNullable[$i]=$row['Nullable'];
            $TMinLength[$i]=$row['MinLength'];
            $i++;
        }

        return array($Field_TDesc,$Field_TLen,$DefaultTValue,$TNullable,$TMinLength,$i);
    }


    function getCategory(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTCategory' and LOV_Active='Y' ";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);   
    }


    function getShiftStartTime($shftcode){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="SELECT convert(varchar(10),Shift_From,108) as Shift_From FROM SHIFTMAST WHERE Shift_Code='$shftcode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        while ($row=$fetch($result)) {
            $Shift_From=$row['Shift_From'];
        }

        return array($Shift_From);
    }


    function getShift(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="SELECT Shift_Code+'- ('+Shift_Name+')' as shift,Shift_Code FROM SHIFTMAST";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$shift[$i]=$row['shift'];
        	$Shift_Code[$i]=$row['Shift_Code'];
        	$i++;
        }

        return array($shift,$Shift_Code,$i);
    }


    function getMyTeam($mngrcode){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        //$sql="Select Emp_Code, Emp_Name+' - ('+EMP_CODE+')' as Emp_Name FROM HrdMastQry where MNGR_CODE='$mngrcode' ";

        $sql="select Emp_Code, Emp_Name+' - ('+EMP_CODE+')' as Emp_Name from HrdMastQry where mngr_code='$mngrcode' and Status_Code='01' and GRD_NAME='Associates'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$Emp_Code[$i]=$row['Emp_Code'];
        	$Emp_Name[$i]=$row['Emp_Name'];
        	$i++;
        }

        return array($Emp_Code,$Emp_Name,$i);
    }

    function getCostName($mngrcode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select COST_CODE,COST_NAME from hrdmastqry where Emp_Code='$mngrcode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);
        
        return array($row['COST_CODE'],$row['COST_NAME']);

    }

    function getPurpose(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTPurpose' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);
    }


    function getNewReqOpt(){
    	
    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTNewReqQues' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);
    }

    /*function getEmployeeShiftByDate($empCode, $date,$shift){
        $date = trim($date);
        if(empty($empCode) || empty($date) || empty($shift)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];//m/d/y
        $attdate=$explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

         

         $SqlQuery = "SELECT rost.EMP_CODE, 
        convert(varchar(10),RosterStart,103) as RosterStart,
        convert(varchar(10),RosterEnd,103) as RosterEnd,
        convert(char(5), rost.shift_from,108) AS shift_from, 
        convert(char(5),rost.SHIFT_TO) AS SHIFT_TO, rost.shift_name
        FROM rosterQry rost
        WHERE '$convertDate' BETWEEN RosterStart AND RosterEnd AND RosterName NOT LIKE 'rosterAutoShift%' AND rost.EMP_CODE in ($empCode) AND rost.Shift_Code in 
        ($shift) ";
    
        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
      
           $i=0;
        while ($row = $fetch($result)) {

            $Shift_From[$i]=$row['shift_from'];
            $Shift_To[$i]=$row['SHIFT_TO'];
            $Shift_Name[$i]=$row['shift_name'];
            $EMP_CODE[$i]=$row['EMP_CODE'];
            $i++;
        }

        return array(
            $Shift_From,
            $Shift_To,
            $Shift_Name,
            $EMP_CODE,
            $i
        );
    }*/

    function getEmployeeShiftByDate($empCode,$date,$shift,$cate11){
        $date = trim($date);
        if(empty($empCode) || empty($date) || empty($shift)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;

        //$fordate = str_replace('/', '-', $date);
        //$newDate=date("Y-m-d",strtotime($fordate));
	
	$sqw="SELECT ShiftMastId,Shift_Name FROM ShiftMast WHERE Shift_CODE IN ($shift)";
	$ressqw=query($query, $sqw, $pa, $opt, $ms_db);
	while ($resrow = $fetch($ressqw)) {
                $ShiftMa[]=$resrow ['ShiftMastId'];
            }
	 $shiftidss=implode(',',$ShiftMa);
        /*-------Get Shift Id-----------*/
            $shiftIdSql="select ShiftMastId from ShiftMast where Shift_Code in ($shift)";
            $shiftIdresult=query($query, $shiftIdSql, $pa, $opt, $ms_db);
            $i=0;
            while($shiftIdRow = $fetch($shiftIdresult)){
                $shiftIdArr[$i] =$shiftIdRow['ShiftMastId'];
                $i++;
            }
            $strShiftId=implode(',', $shiftIdArr);
        /*-------Get Shift Id-----------*/

        /*-------Get roster----------*/
            //$rosterSql="select distinct EMP_CODE from Roster_schema where RosterName NOT LIKE 'rosterAutoShift%' AND EMP_CODE in ($empCode)";
             $rosterSql="select distinct EMP_CODE from ROSTER_SCHEMA where emp_code in (SELECT distinct EMP_CODE FROM HRDTRAN WHERE GRD_CODE='1' AND Status_Code='01' AND  EMP_CODE in ($empCode))";

            $RosterResult=query($query, $rosterSql, $pa, $opt, $ms_db);

            while ($RosterRow = $fetch($RosterResult)) {
                $RosterEmp[]=$RosterRow['EMP_CODE'];
            }

            for($s=0; $s< count($RosterEmp); $s++){
                $strRoster[]="'".trim($RosterEmp[$s])."'";
            }

             $newEmpCode=implode(",", $strRoster);


        /*-------Get roster----------*/

	/*$empSql="SELECT rost.EMP_CODE, 
        convert(varchar(10),RosterStart,103) as RosterStart,
        convert(varchar(10),RosterEnd,103) as RosterEnd,
        convert(char(5), rost.shift_from,108) AS shift_from, 
        convert(char(5),rost.SHIFT_TO,108) AS SHIFT_TO, rost.shift_name,CASE WHEN '$date'=
	CASE WK1
				WHEN 7 THEN DATEADD(DD, 1 - DATEPART(DW, '$date'), '$date') --Sunday
				WHEN 6 THEN DATEADD(DD, 7 - DATEPART(DW, '$date'), '$date') --Saturday
				WHEN 5 THEN DATEADD(DD, 6 - DATEPART(DW, '$date'), '$date') --Friday
				WHEN 4 THEN DATEADD(DD, 5 - DATEPART(DW, '$date'), '$date') --Thrusday
				WHEN 3 THEN DATEADD(DD, 4 - DATEPART(DW, '$date'), '$date') --Wednesday 
				WHEN 2 THEN DATEADD(DD, 3 - DATEPART(DW, '$date'), '$date') --Tuesday 
				WHEN 1 THEN DATEADD(DD, 2 - DATEPART(DW, '$date'), '$date') --Monday 
			END
			THEN 'Weekly off' else 'Working' 
			END [Status]
         
        FROM rosterQry rost WHERE '$date' BETWEEN RosterStart AND RosterEnd AND rost.EMP_CODE in ($newEmpCode) AND rost.SHIFTMASTER in 
        ($shiftidss) order by rost.EMP_CODE asc";*///echo "aaaa".$newEmpCode."qqqqq";
	$empSql="SELECT * FROM (
SELECT RS.Emp_Code, 
convert(varchar(10),RS.start_rost,103) as RosterStart,
convert(varchar(10),RS.end_rost,103) as RosterEnd,

CASE WHEN DAY(RS.start_rost) = DAY(RC.RosterDate)  AND RC.[type_name]='approve' and '$date'=RC.RosterDate
THEN convert(char(5), RC.shift_from,108) ELSE convert(char(5), SM.shift_from,108) END shift_from,

CASE WHEN DAY(RS.start_rost) = DAY(RC.RosterDate)  AND RC.[type_name]='approve' and '$date'=RC.RosterDate
THEN convert(char(5), RC.SHIFT_TO,108) ELSE convert(char(5),SM.SHIFT_TO,108)  END SHIFT_TO,

CASE WHEN DAY(RS.start_rost) = DAY(RC.RosterDate)  AND RC.[type_name]='approve' and '$date'=RC.RosterDate
THEN RC.shift_name ELSE SM.shift_name END shift_name,

CASE WHEN DAY(RS.start_rost) = DAY(RC.RosterDate)  AND RC.[type_name]='approve' and '$date'=RC.RosterDate
THEN RC.shiftMaster ELSE SM.ShiftMastId END shift_code,

CASE WHEN '$date'=
CASE SP.WeeklyOff1
      WHEN 7 THEN DATEADD(DD, 1 - DATEPART(DW, '$date'), '$date') --Sunday
      WHEN 6 THEN DATEADD(DD, 7 - DATEPART(DW, '$date'), '$date') --Saturday
      WHEN 5 THEN DATEADD(DD, 6 - DATEPART(DW, '$date'), '$date') --Friday
      WHEN 4 THEN DATEADD(DD, 5 - DATEPART(DW, '$date'), '$date') --Thrusday
      WHEN 3 THEN DATEADD(DD, 4 - DATEPART(DW, '$date'), '$date') --Wednesday 
      WHEN 2 THEN DATEADD(DD, 3 - DATEPART(DW, '$date'), '$date') --Tuesday 
      WHEN 1 THEN DATEADD(DD, 2 - DATEPART(DW, '$date'), '$date') --Monday 
END
THEN 'Weekly off' else 'Working' 
END [Status]

FROM Roster_schema RS
LEFT OUTER JOIN ShiftChangeqry RC ON RC.Emp_code=RS.Emp_Code AND DAY(RS.start_rost) = DAY(RC.RosterDate) 
INNER JOIN ATT_ROSTER AR ON RS.RosterName=AR.roster AND RS.start_rost=AR.RosterDate
INNER JOIN ShiftMast SM ON  AR.shiftMaster=SM.ShiftMastId 
INNER JOIN ShiftPatternMast SP ON AR.shiftPattern=SP.ShiftPatternMastid 
WHERE  rs.auto_period=1 AND CONVERT(VARCHAR(12),(RS.start_rost))+CONVERT(VARCHAR(20),RS.Emp_Code) IN 
(
        SELECT CONVERT(VARCHAR(12),MAX(start_rost))+CONVERT(VARCHAR(20),EMP_CODE) fROM Roster_schema 
        WHERE end_rost <= '$date' AND Roster_schema.Emp_Code IN($newEmpCode) 
        and Roster_schema.auto_period='1'
         
        GROUP BY DAY(start_rost),EMP_CODE
)
AND RS.Emp_Code IN ($newEmpCode)  
AND DAY(RS.start_rost) = DAY('$date')  ) A
where a.shift_code in ($shiftidss) ORDER BY Emp_Code";
	
		
	$empresult=query($query, $empSql, $pa, $opt, $ms_db); 
	 $i=0;
	while($emprow = $fetch($empresult)){
	    /*if($cate11=='4' && $emprow['Status']=="Weekly off")
	{
            continue;
	}
	else
	{*/
		$EMP_CODE[$i]=$emprow['Emp_Code'];
	    $EMP_Status[$i]=$emprow['Status'];
		if($EMP_Status[$i]=="Weekly off")
		{
			$Shift_From[$j]="";
            		$Shift_To[$j]="";
            		$Shift_Name[$j]="";
		}
		else
		{
			$Shift_From[$i]=$emprow['shift_from'];
            		$Shift_To[$i]=$emprow['SHIFT_TO'];
            		$Shift_Name[$i]=$emprow['shift_name'];
		}
	/*}*/


			$i=$i+1;
	}        
        //echo $SqlQuery = "SELECT Emp_code,convert(char(5),Shift_start,108) AS shift_from,convert(char(5),Shift_end,108) AS SHIFT_TO, Schedule_Shift as shift_name,convert(char(5),Intime,108) as Intime,convert(char(5),Outtime,108) as Outtime,statustitle FROM attendancesch WHERE date='$date'  AND EMP_CODE in ($newEmpCode) AND shiftid in 
        //($strShiftId)  order by Emp_code asc"; 
	$stremp="'" . implode("','", $EMP_CODE) . "'"; 
	$SqlQuery = "SELECT convert(char(5),Intime,108) as Intime,convert(char(5),Outtime,108) as Outtime,final_status FROM attendancesch WHERE date='$date'  AND EMP_CODE in ($stremp)  
        order by Emp_code asc"; 
        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
      
          $j=0;
        while ($row = $fetch($result)) {
	
            $inTime[$j] = $row['Intime'];
            $outTime[$j] = $row['Outtime'];
            $status[$j] =$row['final_status'];//echo  "qqq".$status[$j]."aaaa";
		if($status[$j]!='')
		{
			$EMP_Status[$j]=$status[$j];
		}
		if($EMP_Status[$j]=="Holiday" || $EMP_Status[$j] == "POH" )
		{//echo "sss".$j.$status[$j];
			$sqlAdd="select isAdditionalHoliday from HOLIDAYS where HDATE='$date' ";
         		$resultADD=query($query, $sqlAdd, $pa, $opt, $ms_db);
                        $rowAdd=$fetch($resultADD);
                        if($rowAdd['isAdditionalHoliday'] == 1){
                                $EMP_Status[$j] = 'Additional Holiday';
                         }

			$Shift_From[$j]="";
            		$Shift_To[$j]="";
            		$Shift_Name[$j]="";
		}
            $j++;
        }

        return array(
            $Shift_From,
            $Shift_To,
            $Shift_Name,
            $EMP_CODE,
            $inTime,
            $outTime,
            $EMP_Status,
            $i
        );
    }

    function getEmployeeName($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select Emp_Code,Emp_Name from EmpNameQry where EMP_CODE in($empCode)";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Emp_Code[$i]=$row['Emp_Code'];
            $Emp_Name[$i]=$row['Emp_Name'];
            $i++;
        }

        return array($Emp_Code,$Emp_Name,$i);
    }

    function getEmployeeNameByID($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select Emp_Code,Emp_Name from EmpNameQry where EMP_CODE ='$empCode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        while ($row=$fetch($result)) {
            $Emp_Code=$row['Emp_Code'];
            $Emp_Name=$row['Emp_Name'];
            
        }

        return array($Emp_Code,$Emp_Name,$i);
    }

    function overTimeGlobal(){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Id,overTimeName,overTimeHours from overtime_global ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $ot_id[$i]=$row['Id'];
            $ot_name[$i]=$row['overTimeName'];
            $ot_hours[$i]=$row['overTimeHours'];
            $i++;
        }

        return array($ot_id,$ot_name,$ot_hours,$i);
    }

    function checkOTRequest($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        $sql="select Emp_Code,overTimehours,OT_Date from overtime_Balances where Emp_Code in ($empCode) AND OT_Date='$convertDate'";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        $i=0;
        if($numRow >= 1){
            
            while ($row=$fetch($result)) {
                $reqHours[$i]=$row['overTimehours'];
                $i++;
            }
            return array($reqHours,$i);
        }
    }

    /*----------Insertion Process-----------*/

    function checkOTRequestById($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[1].'-'.$explodeDate[0];

        $sql="select Emp_Code,OT_Date from overtime_Balances where Emp_Code ='$empCode' AND OT_Date='$convertDate'";
       
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
       
        if($numRow >= 1){
            
            return $empCode;
        }else{
            return 0;
        }
    }

    function insertOTRequestById($empCode,$otdate,$category,$shift,$startTime,$endTime,$totalHour,$purpose,$newFunReq,$other,$approver,$OT_Key,$ast,$mngrCode,$otType,$wfmethod){
          
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($otdate);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[1].'-'.$explodeDate[0];
        
        //echo $ast; die;

        //echo $shift; 
         $sql="insert into overtime_Balances (Emp_Code,overTimeHours,OT_Date,OT_StartTime,OT_EndTime,OT_NewFunction,OT_Other,OT_purpose,OT_Approver,OT_Key,OT_Shift,OT_Category,appStatus,CreatedBy,OT_Type,workflowused) values
        ('$empCode','$totalHour','$convertDate','$startTime','$endTime','$newFunReq','$other','$purpose','$approver','$OT_Key','$shift','$category','$ast','$mngrCode','$otType','$wfmethod')";
       
	    $result=query($query,$sql,$pa,$opt,$ms_db); 
        if ($result) {
            return 1;
        }      

    }

   
   function quaterlyOTRequestById($empCode,$otdate,$totalHour,$finYear){
        //echo $empCode.'-'.$otdate.'-'.$totalHour.'-'.$finYear; die;
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        global $newHourOt;
        global $quat;

         $FirstQ=array();
         $SecQ=array();
         $ThirdQ=array();
         $FourthQ=array();
        
        $date = trim($otdate);
        
        $year = trim($finYear);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[1].'-'.$explodeDate[0];//yy/mm/dd
       
        $explodeFin = explode('/', $year);
       
        $convertYear = $explodeFin[2];

        // Start getQuarters
           
        $quarter=$this->getQuarters($year);
        
        $FirstQ=$quarter['1Q']; //print_r($quarter); 
        $SecQ=$quarter['2Q'];
        $ThirdQ=$quarter['3Q'];
        $FourthQ=$quarter['4Q'];

        $qm=ltrim($explodeDate[1],'0'); 

        for($i=0;$i<3;$i++){ 
            if($FirstQ[$i] == $qm){  
                $quat=1;
                break;
            }
        }

        for($sc=0; $sc<3; $sc++){
            if($SecQ[$sc] == $qm){ 
                $quat=2;
                break;
            }
            
        }

        for($tr=0;$tr<3; $tr++){ 
            if($ThirdQ[$tr] == $qm){ 
                $quat=3;
                break;
            }
        }

        for($fr=0;$fr<3;$fr++){ 
            if($FourthQ[$fr] == $qm){  
                $quat=4;
                break;
            }
        }

        

        //End getQuarters

        $sql="select hourOT from quartely_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
         $result=query($query,$sql,$pa,$opt,$ms_db);
         $numRow=$num($result);
         $row=$fetch($result);
         $times = array();

        $times[] = $row['hourOT'];
        $times[] = $totalHour;

        $newHourOt=$this->AddPlayTime($times);
         //$newHourOt=$row['hourOT'] + $totalHour;
        
         if($numRow >= 1){
             $sql="update quartely_OT set hourOT='$newHourOt' where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
            $result=query($query,$sql,$pa,$opt,$ms_db);
            if($result){
                return 3;
            }
         }else{

               $payd=$this->getPayCycleByEmp($empCode);
              
                        if($quat == 1){
                            if($FirstQ[0] == 1){
                                $smon=12;
                                 $mon=$FirstQ[2];
                                 $sconvertYear=$convertYear;
                            }else{
                                $smon=$FirstQ[0]-1;
                                $mon=$FirstQ[2];
                                $sconvertYear=$convertYear;
                            }
                           
                        }if($quat == 2){
                             if($SecQ[0] == 1){
                                $smon=12;
                                 $mon=$SecQ[2];
                                 $sconvertYear=$convertYear;
                            }else{
                             $smon=$SecQ[0]-1;
                            $mon=$SecQ[2];
                            $sconvertYear=$convertYear;
                            }
                        }
                        if($quat == 3){
                            if($ThirdQ[0] == 1){
                                $smon=12;
                                $mon=$ThirdQ[2];
                                $sconvertYear=$convertYear;
                            }else{
                             $smon=$ThirdQ[0]-1;
                             $mon=$ThirdQ[2];
                             $sconvertYear=$convertYear;
                            }
                        }
                        if($quat == 4){
                             if($FourthQ[0] == 1){
                                $smon=12;
                                $mon=$FourthQ[2];
                                $sconvertYear=$convertYear;
                                $convertYear=$convertYear + 1;
                            }else{
                            $smon=$FourthQ[0]-1;
                            $mon=$FourthQ[2];
                            $sconvertYear=$convertYear;
                            $convertYear=$convertYear + 1;
                            }
                        }
                    
                        // $smon=$explodeDate[0];
                        // $mon=$explodeDate[0]+1;
                    
                    $startDate=$sconvertYear.'-'.$smon.'-'.$payd;
                    $endDate = $convertYear.'-'.$mon.'-'.($payd-1);
              
                
              $sql="insert into quartely_OT 
              (yearOT,quartelyOT,hourOT,Emp_Code,startDate,endDate) values
              ('$explodeFin[2]','$quat','$totalHour','$empCode','$startDate','$endDate')";
                
              $result=query($query,$sql,$pa,$opt,$ms_db); 
              if($result){
                return 3;
              }
         }
    }

    function yearlyOTRequestById($empCode,$otdate,$totalHour,$finYear){

       //echo $empCode.'-'.$otdate.'-'.$totalHour.'-'.$finYear; die;
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        global $newHourOt;
        global $quat;

         $FirstQ=array();
         $SecQ=array();
         $ThirdQ=array();
         $FourthQ=array();
        
        $year = trim($finYear);

        $explodeFin = explode('/', $year);//dd/mm/yy
       
        $startYear= $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];//yy/mm//dd
        $lastDate= date("t", strtotime($startYear));
        $endYear = ($explodeFin[2]+1).'-'.($explodeFin[1]-1).'-'.$lastDate;

       
         $sql="select hourOT from yearly_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
         $result=query($query,$sql,$pa,$opt,$ms_db);
         $numRow=$num($result);
         $row=$fetch($result);
          $times = array();

          $times[] = $row['hourOT'];
          $times[] = $totalHour;

        $newHourOt=$this->AddPlayTime($times);
         //$newHourOt=$row['hourOT'] + $totalHour;
        
         if($numRow >= 1){
             $sql="update yearly_OT set hourOT='$newHourOt' where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
            $result=query($query,$sql,$pa,$opt,$ms_db);
            if($result){
                return 4;
            }
         }else{

              $sql="insert into yearly_OT 
              (yearOT,hourOT,Emp_Code,startDate,endDate) values
              ('$explodeFin[2]','$totalHour','$empCode','$startYear','$endYear')";
                
              $result=query($query,$sql,$pa,$opt,$ms_db); 
              if($result){
                return 4;
              }
         }
    }

   /*--------------End Insertion Process---------------------*/

    function checkMonthlyLimit($empCode,$date){
        //echo $empCode; echo $date;
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        //$curMonth = date($explodeDate[0], time());
        //$curQuarter = ceil($curMonth/3);

        //$quartely=$explodeDate[0]/3;
          $sql="select hourOT from monthly_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
           
            while ($row=$fetch($result)) {
                $reqHours=$row['hourOT'];
                
            }
            return $reqHours;
         }
    }

    function checkYearlyLimit($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        
        $sql="select hourOT from monthly_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
           
            while ($row=$fetch($result)) {
                $reqHours=$row['hourOT'];
                
            }
            return $reqHours;
         }
    }

    function getQuarters($finnYear){//dd/mm//yy
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        
        $date = trim($finnYear); 
        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];//dd/mm/yy


        for($i=0; $i<3; $i++){

            $FQuart[$i]= ($explodeDate[1]) + ($i);
            //geting months here in one quarter [which month is  coming in one quater
        }

        //print_r($FQuart);
        if($FQuart[2] == 12){
            $SecTemp=1;
        }else{
            $SecTemp=$FQuart[2] + 1;
        }
        
        for($i=0; $i< 3; $i++){

            $SecQuart[$i]= ($SecTemp) + ($i);
        }

          //print_r($SecQuart);
        if($SecQuart[2] == 12){ 
            $thirdTemp=1;
        }else{
            $thirdTemp= $SecQuart[2] + 1;
        }
        
        for($i=0; $i< 3; $i++){

            $thirdQuart[$i]= ($thirdTemp) + ($i);
        }

       // print_r($thirdQuart);

        if($thirdQuart[2] == 12){ 
            $fourthTemp=1;
        }else{
             $fourthTemp= $thirdQuart[2] + 1;
        }

        for($i=0; $i< 3; $i++){

            $fourthQuart[$i]= ($fourthTemp) + ($i);
        }
        //print_r($fourthQuart);

        return array("1Q"=>$FQuart, "2Q"=>$SecQuart, "3Q"=>$thirdQuart, "4Q"=>$fourthQuart);
    }
   
    function checkQuarterlyLimit($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        $sql="select hourOT from quartely_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";


        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
            
            while ($row=$fetch($result)) {
                 $reqHours=$row['hourOT'];
                
            }
            return $reqHours;
         }
    }

    function getPayCycleByEmp($empCode){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


         $sql="select PY_Start from paycycle where py_OrgMastdt in (select grd_code from HrdMastQry where emp_code = '$empCode' )";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
           
            while ($row=$fetch($result)) {
                $payStart=$row['PY_Start'];
               
            }
            return $payStart;
         }
    }

    function sendAppMailById($approverId,$empCode,$otdate,$totalHour,$mngrCode){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $appId=explode(",", $approverId);


        $queMail= "Select Emp_Name,OEMailID from HrdMastQry where Emp_Code ='$appId[0]'";
        $resMail= query($query,$queMail,$pa,$opt,$ms_db);
        $appMail=$fetch($resMail);
        $Emp_Name=$appMail['Emp_Name'];

        $requesterSql="Select Emp_Name from HrdMastQry where Emp_Code ='$empCode'";
        $requesterRes = query($query,$requesterSql,$pa,$opt,$ms_db);
        $requesterRow= $fetch($requesterRes);
        $Emp_Name=$requesterRow['Emp_Name'];

        $to=trim($appMail['OEMailID']);
        $mes = rtrim(ucwords(strtolower($appMail[0])));
        

        // $subject= "gfjguj";
        // $message="Dear ".$mes.",<br><br>";
        // $message.="You are requested to approve my ".$totalHour." Hours OverTime of ".$otdate." ";
        
        // $message.="Regards <br>";
        // $message.=$mngrCode[0]. "-";
        $OverTimeType = 'Overtime Request';
        $ApproverName = $mes;
        $TotalHours = $totalHour;
        $FromDate = $otdate;
        $RequesterName = $Emp_Name;
        $RequesterEmpCode = $mngrCode[0];
        $subject =getSubject('31','Create OverTime');
        $message =getBody('31','Create OverTime');
        eval("\$subject = \"$subject\";");
        eval("\$message = \"$message\";");
        str_replace('$', "", $subject);
        str_replace('$', "", $message);
        $mail1=mymailer('donotreply@sequelone.com',$subject,$message,$to);
        if($mail){
            echo 7;
        }
    }

    /*----------Advance Approver ID----------- (ihave to do according workflow)*/

    //based on quarter leave limit
    function checkAdvanceApprover($Qua,$totalHour){
        $tot=explode(":", $Qua);
	       $subtot=explode(":", $totalHour);
        $globalVal=$this->overTimeGlobal();
        //print_r($globalVal); die;
        
        
        $arrhours=$globalVal[2];
        //echo $arrhours[3];
         //die;
        //for ($i=0; $i < count($arrhours); $i++) { 
            if($tot[0] < 0)
            {   
                return 1; 
                break;
            }else if($tot[0] > $arrhours[3])
            {   
                return 1; 
                break;
            }else if($tot[0] < $subtot[0]){
                return 1; 
                break;
            }else{ 
                return 2; 
                break;
            }
       // }
    }

    function getAdvanceAppID($code){ //echo $code;

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

       //$code=$_REQUEST['emp_code'];
       //$sql="select * from WorkFlow WHERE WFFor='overTime' and WorkFlowID=23";
        $sql="select * from WorkFlow WHERE WFFor='overtime' and WEvents='deviation' order by WorkFlowID DESC";
       $result=query($query,$sql,$pa,$opt,$ms_db);
    
        $row=$fetch($result);
        $dataval=$row['Approver'];

        $lev = explode(',',$dataval);
        //print_r($lev);
            for($i=0;$i<count($lev);$i++){
                if($lev[$i]=='Reporting Manager'){
                    $sqlr="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultr = query($query,$sqlr,$pa,$opt,$ms_db);
                    while($rowr = $fetch($resultr)){
                        $code1 = $rowr['MNGR_CODE'];
                        $sql1="Select * from hrdmastQry where Emp_Code='$code1'";
                        $result1 = query($query,$sql1,$pa,$opt,$ms_db);
                        while($row1 = $fetch($result1)){
                        
                            $approverID[]= $row1['Emp_Code']; 
                        }

                    }
                }


                if($lev[$i] == 'L2'){
                    $sql7="Select MNGR_CODE from hrdmastQry where Emp_Code='$code'";
                    $result7 = query($query,$sql7,$pa,$opt,$ms_db);
                    while($row7 = $fetch($result7)){
                        $code7 = $row7['MNGR_CODE'];
                        $sql9="select MNGR_CODE from hrdmastQry where Emp_Code='$code7'";
                        $result9=query($query,$sql9,$pa,$opt,$ms_db);
                        $row9=$fetch($result9);
                        $code9=$row9['MNGR_CODE'];
                        $sql8="Select EmpImage,DSG_NAME,EMP_NAME,Emp_Code from hrdmastQry where Emp_Code='$code9'";
                        $result8 = query($query,$sql8,$pa,$opt,$ms_db);
                        while($row8 = $fetch($result8)){
                        echo "<span class='appMan blue-bg'>
                                <span class='appManImg'>";
                                    if($row8['EmpImage'] == ""){
                                     echo "<img class='img-circle img50' src='../Profile/upload_images/images (2).jpg' >"; }else{
                                     echo "<img class='img-circle img50' src='../Profile/upload_images/".$row8['EmpImage']."' >";}
                        echo      "  </span>";
                        echo    "    <span class='appManName' data-des='".$row8['DSG_NAME']."'>";
                                        echo $row8['EMP_NAME']; 

                                echo"</span></span>";
                            $approverID[]= $row8['Emp_Code']; 
                        }

                    }
                }

                if($lev[$i]=='Functional Manager'){
                    $sqlf="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultf = query($query,$sqlf,$pa,$opt,$ms_db);
                    while($rowf = $fetch($resultf)){
                        $code2 = $rowf['MNGR_CODE2'];
                        $sql2="Select * from hrdmastQry where Emp_Code='$code2'";
                        $result2 = query($query,$sql2,$pa,$opt,$ms_db);
                        while($row2 = $fetch($result2)){
                            
                            $approverID[]=$row2['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Functional Head'){
                    $sqlfh="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultfh = query($query,$sqlfh,$pa,$opt,$ms_db);
                    while($rowfh = $fetch($resultfh)){
                        $code3 = $rowfh['FunctionHead_Code'];
                        $sql3="Select * from hrdmastQry where Emp_Code='$code3'";
                        $result3 = query($query,$sql3,$pa,$opt,$ms_db);
                        while($row3 = $fetch($result3)){
                            
                            $approverID[]= $row3['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Bussiness Unit Head'){
                    $sqlb="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultb = query($query,$sqlb,$pa,$opt,$ms_db);
                    while($rowb = $fetch($resultb)){
                        $code4 = $rowb['BussUnitHead_Code'];
                        $sql4="Select * from hrdmastQry where Emp_Code='$code4'";
                        $result4 = query($query,$sql4,$pa,$opt,$ms_db);
                        while($row4 = $fetch($result4)){
                            
                            $approverID[] = $row4['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Sub Functional Head'){
                    $sqlsf="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultsf = query($query,$sqlsf,$pa,$opt,$ms_db);
                    while($rowsf = $fetch($resultsf)){
                        $code5 = $rowsf['SubFunctionHead_Code'];
                        $sql5="Select * from hrdmastQry where Emp_Code='$code5'";
                        $result5 = query($query,$sql5,$pa,$opt,$ms_db);
                        while($row5 = $fetch($result5)){
                            
                            $approverID[]=$row5['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Sub Bussiness Head'){
                    $sqlsb="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultsb = query($query,$sqlsb,$pa,$opt,$ms_db);
                    while($rowsb = $fetch($resultsb)){
                        $code6 = $rowsb['SubBussHead_Code'];
                        $sql6="Select * from hrdmastQry where Emp_Code='$code6'";
                        $result6 = query($query,$sql6,$pa,$opt,$ms_db);
                        while($row6 = $fetch($result6)){
                           
                            $approverI[] = $row6['Emp_Code'];
                        }

                    }
                }

            }
             $appID = implode(',',$approverID);

        return $appID;

    }

    /*function advanceApproverName($code){
            //$code=$_REQUEST['code']; 
            //return $code;
            $workflow = Array();
            $level= Array();
            $sqlq="select * from WorkFlow WHERE WFFor='overTime' and WorkFlowID=10009 ";
            $resultq = query($query,$sqlq,$pa,$opt,$ms_db);
            {
                while($row = $fetch($resultq)) {

                    $sql="Select * from hrdmastQry";

                    $datalevel = $row['ForMenu'];
                   // echo $_POST['data'];

                    $datalevel=json_decode($datalevel,true);

                    //print_r($datalevel);
                    //print_r($datalevel[0]['children'][$b]['id']);


                    $sql.=" where ";
                    for ($i=0;$i<count($datalevel);$i++){
                    if($datalevel[$i]['text']=="Company"){
                        for($b=0;$b<count($datalevel[$i]['children']);$b++) {
                            $sql .= " Comp_code='" . substr($datalevel[$i]['children'][$b]['id'],1)."'";
                            if($b<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql .= " and ";
                    }

                    if($datalevel[$i]['text']=="Business Unit"){
                        for($b=0;$b<count($datalevel[$i]['children']);$b++) {
                            $sql .= " Busscode='" . substr($datalevel[$i]['children'][$b]['id'],1)."'";
                            if($b<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql .= " and ";
                    }

                    if($datalevel[$i]['text']=="Location"){

                        for($l=0;$l<count($datalevel[$i]['children']);$l++) {
                            $sql .= " loc_Code='" . substr($datalevel[$i]['children'][$l]['id'],1)."'";
                            if($l<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Working Location"){

                        for($wl=0;$wl<count($datalevel[$i]['children']);$wl++) {
                            $sql .= " WLOC_code='" . substr($datalevel[$i]['children'][$wl]['id'],1)."'";
                            if($wl<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Function"){

                        for($f=0;$f<count($datalevel[$i]['children']);$f++) {
                            $sql .= " FUNCT_code='" . substr($datalevel[$i]['children'][$f]['id'],1)."'";
                            if($f<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Sub Function"){

                        for($sf=0;$sf<count($datalevel[$i]['children']);$sf++) {
                            $sql .= " SFUNCT_code='" . substr($datalevel[$i]['children'][$sf]['id'],1)."'";
                            if($sf<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }
                    if($datalevel[$i]['text']=="Grade"){

                        for($g=0;$g<count($datalevel[$i]['children']);$g++) {
                            $sql .= " GRD_code='" . substr($datalevel[$i]['children'][$g]['id'],1)."'";
                            if($g<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }
                    if($datalevel[$i]['text']=="Employee Type"){

                        for($et=0;$et<count($datalevel[$i]['children']);$et++) {
                            $sql .= " TYPE_code='" . substr($datalevel[$i]['children'][$et]['id'],1)."'";
                            if($et<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Process"){

                        for($pr=0;$pr<count($datalevel[$i]['children']);$pr++) {
                            $sql .= " PROC_code='" . substr($datalevel[$i]['children'][$pr]['id'],1)."'";
                            if($pr<count($datalevel[$i]['children'])-1){

                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    }
                    $sql.="Status_Code='01'";

                    echo $sql;
                    $result = query($query,$sql,$pa,$opt,$ms_db);
                    if ($num($result) > 0) {
                        $list=array();

                        while($row1 = $fetch($result)) {
                            $list[] = $row1['Emp_Code'];


                        }
                        if(in_array($code,$list)){
                            //echo $row['LevelNo']; echo ";";
                            //echo $row['NoDays']; echo ";";
                            return $row['Approver'];

                            exit;

                        }

                    }
                    else{
                        return 2 ;
                    }


                }
            }

    }*/

    /*----------End Advance Approver ID--------*/
/*--------End OT Request Function-----------*/


/*--------Start OT Approver Function----------*/

    function getOTKey(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        //$sql1="select OT_Key,OT_ApplyDateTime,count(id) as totalEmp from overtime_Balances  group by OT_Key,OT_ApplyDateTime order by OT_ApplyDateTime desc";
	
	$sql1="select OT_Key,cast(OT_ApplyDateTime as DATE),count(id) as totalEmp from overtime_Balances  group by OT_Key,cast(OT_ApplyDateTime as DATE) order by cast(OT_ApplyDateTime as DATE) desc";

        $result1=query($query,$sql1,$pa,$opt,$ms_db);
        $i=0;
        while ($row1= $fetch($result1)) {

            $OT_Key[$i]=$row1['OT_Key'];
            $totalEmp[$i]=$row1['totalEmp'];

           $sql2="select appStatus,OT_Approver,REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OTDATE,
           replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as OTApplyDateTime,
           replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7) as First_Approver,
           replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7) as sec_Approver,
           replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as third_Approver,
           App_OTRemark from overtime_Balances where OT_Key='$OT_Key[$i]'";
            $result2=query($query,$sql2,$pa,$opt,$ms_db);
            $row2=$fetch($result2);

            $status[$i]=$row2['appStatus'];
            $approver[$i]=$row2['OT_Approver'];
            $firstApproved[$i]=$row2['First_Approver'];
            $secApproved[$i]=$row2['sec_Approver'];
            $thirdApproved[$i]=$row2['third_Approver'];
            $remark[$i]=$row2['App_OTRemark'];
            $i++;
        }

        return array($OT_Key,$totalEmp,$status,$approver,$i,$firstApproved,$secApproved,$thirdApproved,$remark);

    }

    function getappliedOT_Category($catID){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select LOV_Text from LOVMast where LOV_Field='OTCategory' AND LOV_VAlue='$catID'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
       $row= $fetch($result);

        return $row['LOV_Text'];
    }
    
   function getappliedOT_Purpose($purId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTPurpose' AND LOV_Value='$purId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getappliedOT_Reason($reasId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTNewReqQues' AND LOV_Value='$reasId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

/*--------End OT Approver Function----------*/


    function AddPlayTime($times) {
        $minutes =0;
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;

       
        return sprintf('%02d:%02d', $hours, $minutes);
    }


function getHolidays($startDate,$endDate,$empCode = ''){
    global $query;
    global $pa;
    global $opt;
    global $ms_db;
    global $num;
    global $fetch;
    $daysInMonth = cal_days_in_month(0, $month, $year);
    $firstDate= $year.'-'.$month.'-01';
    $lastDate = $year.'-'.$month.'-'.$daysInMonth;

    $where  =   "";
    //get employees data
    $getEmployeeDataSql = "SELECT LOC_CODE,COMP_CODE,BussCode AS BUS_CODE ,SUBBuss_CODE AS SUBBUS_CODE,WLOC_CODE,FUNCT_CODE AS FUNC_CODE,SFUNCT_CODE AS SUBFUNC_CODE,COST_CODE,PROC_CODE,GRD_CODE,DSG_CODE FROM HrdMastQry WHERE Emp_Code = '".$empCode."'";
    $getEmployeeDataSqlResult = query($query,$getEmployeeDataSql,$pa,$opt,$ms_db);
    if(sqlsrv_num_rows($getEmployeeDataSqlResult) > 0){
        $getEmployeeData = $fetch($getEmployeeDataSqlResult);
        //echo '<pre>'; print_r($getEmployeeData); echo '</pre>'; 
        /*
        $where .= ($getEmployeeData['LOC_CODE'] == '') ?  "" : " AND ( ','+LOC_CODE+',' LIKE '%,".$getEmployeeData['LOC_CODE'].",%' )";
        $where .= ($getEmployeeData['COMP_CODE'] == '') ?  "" : " AND ( ','+COMP_CODE+',' LIKE '%,".$getEmployeeData['COMP_CODE'].",%' )";
        $where .= ($getEmployeeData['BUS_CODE'] == '') ?  "" : " AND ( ','+BUS_CODE+',' LIKE '%,".$getEmployeeData['BUS_CODE'].",%' )";
        $where .= ($getEmployeeData['SUBBUS_CODE'] == '') ?  "" : " AND ( ','+SUBBUS_CODE+',' LIKE '%,".$getEmployeeData['SUBBUS_CODE'].",%' )";
        $where .= ($getEmployeeData['WLOC_CODE'] == '') ?  "" : " AND ( ','+WLOC_CODE+',' LIKE '%,".$getEmployeeData['WLOC_CODE'].",%' )";
        $where .= ($getEmployeeData['FUNC_CODE'] == '') ?  "" : " AND ( ','+FUNC_CODE+',' LIKE '%,".$getEmployeeData['FUNC_CODE'].",%' )";
        $where .= ($getEmployeeData['SUBFUNC_CODE'] == '') ?  "" : " AND ( ','+SUBFUNC_CODE+',' LIKE '%,".$getEmployeeData['SUBFUNC_CODE'].",%' )";
        $where .= ($getEmployeeData['COST_CODE'] == '') ?  "" : " AND ( ','+COST_CODE+',' LIKE '%,".$getEmployeeData['COST_CODE'].",%' )";
        $where .= ($getEmployeeData['PROC_CODE'] == '') ?  "" : " AND ( ','+PROC_CODE+',' LIKE '%,".$getEmployeeData['PROC_CODE'].",%' )";
        $where .= ($getEmployeeData['GRD_CODE'] == '') ?  "" : " AND ( ','+GRD_CODE+',' LIKE '%,".$getEmployeeData['GRD_CODE'].",%' )";
        $where .= ($getEmployeeData['DSG_CODE'] == '') ?  "" : " AND ( ','+DSG_CODE+',' LIKE '%,".$getEmployeeData['DSG_CODE'].",%' )";
    
        */
    }
    //echo $where;
    $result1= array();
    //$sqlq="select CONVERT(char(10), HDATE,126) HDATE,LOC_CODE,HCODE,HDESC,HolidayID,H_status  from holidays where cast(HDATE as date) BETWEEN '".$startDate."' AND '".$endDate."'";

    /*   Query For Holidays which are applicable for all Employees without any restriction */
    
    $queryForAll    =   "SELECT HDATE,HDESC FROM Holidays WHERE ( HDATE BETWEEN '".$startDate."' AND '".$endDate."' ) AND H_status=1 
                        AND (LOC_CODE is null or LOC_CODE = '') 
                        AND (COMP_CODE is null or COMP_CODE = '')
                        AND (BUS_CODE is null or BUS_CODE = '')
                        AND (SUBBUS_CODE is null or SUBBUS_CODE = '')
                        AND (WLOC_CODE is null or WLOC_CODE = '')
                        AND (FUNC_CODE is null or FUNC_CODE = '')
                        AND (SUBFUNC_CODE is null or SUBFUNC_CODE = '')
                        AND (COST_CODE is null or COST_CODE = '')
                        AND (PROC_CODE is null or PROC_CODE = '')
                        AND (GRD_CODE is null or GRD_CODE = '')
                        AND (DSG_CODE is null or DSG_CODE = '')";
                        
    $resultq=query($query,$queryForAll,$pa,$opt,$ms_db);
    
    if($resultq){
        $tempArray4=sqlsrv_num_rows($resultq);
    }else{
        $tempArray4=-1;
    }
    if($tempArray4>0) {
        while ($rowq = $fetch($resultq)){
            $result1[] =array('title'=>$rowq['HDESC'],'start'=>date('Y-m-d', strtotime($rowq['HDATE'])));
            $forAllHolidays     .=  "'".$rowq['HDATE']."',";
        }
        
    }   
    $forAllHolidays =   rtrim($forAllHolidays, ",");
    
    if($forAllHolidays != ''){
            $wherefor   =   " AND HDATE NOT IN (".$forAllHolidays.")";
    
    }
    
    /*  END Process  */
    
     $queryNew  =   "SELECT * FROM Holidays WHERE ( HDATE BETWEEN '".$startDate."' AND '".$endDate."' ) AND  H_status=1".$wherefor."";
    $resultq=query($query,$queryNew,$pa,$opt,$ms_db);
    
    if($resultq){
        $tempArray4=sqlsrv_num_rows($resultq);
    }else{
        $tempArray4=-1;
    }
    
    if($tempArray4>0) {
        
        while($rowy = $fetch($resultq)){
                    
                    if(trim($rowy['LOC_CODE']) !=''){   $where .= (trim($getEmployeeData['LOC_CODE'] == '')) ?  "" : " AND ( ','+LOC_CODE+',' LIKE '%,".$getEmployeeData['LOC_CODE'].",%' )"; }
                    if(trim($rowy['COMP_CODE']) !=''){ $where .= (trim($getEmployeeData['COMP_CODE'] == '')) ?  "" : " AND ( ','+COMP_CODE+',' LIKE '%,".$getEmployeeData['COMP_CODE'].",%' )"; }
                    if(trim($rowy['BUS_CODE']) !=''){ $where .= (trim($getEmployeeData['BUS_CODE'] == '')) ?  "" : " AND ( ','+BUS_CODE+',' LIKE '%,".$getEmployeeData['BUS_CODE'].",%' )"; }
                    if(trim($rowy['SUBBUS_CODE']) !=''){ $where .= (trim($getEmployeeData['SUBBUS_CODE'] == '')) ?  "" : " AND ( ','+SUBBUS_CODE+',' LIKE '%,".$getEmployeeData['SUBBUS_CODE'].",%' )"; }
                    if(trim($rowy['WLOC_CODE']) !=''){ $where .= (trim($getEmployeeData['WLOC_CODE'] == '')) ?  "" : " AND ( ','+WLOC_CODE+',' LIKE '%,".$getEmployeeData['WLOC_CODE'].",%' )"; }
                    if(trim($rowy['FUNC_CODE']) !=''){ $where .= (trim($getEmployeeData['FUNC_CODE'] == '')) ?  "" : " AND ( ','+FUNC_CODE+',' LIKE '%,".$getEmployeeData['FUNC_CODE'].",%' )"; } 
                    if(trim($rowy['SUBFUNC_CODE']) !=''){ $where .= (trim($getEmployeeData['SUBFUNC_CODE'] == '')) ?  "" : " AND ( ','+SUBFUNC_CODE+',' LIKE '%,".$getEmployeeData['SUBFUNC_CODE'].",%' )"; }
                    if(trim($rowy['COST_CODE']) !=''){ $where .= (trim($getEmployeeData['COST_CODE'] == '')) ?  "" : " AND ( ','+COST_CODE+',' LIKE '%,".$getEmployeeData['COST_CODE'].",%' )"; }
                    if(trim($rowy['PROC_CODE']) !=''){ $where .= (trim($getEmployeeData['PROC_CODE'] == '')) ?  "" : " AND ( ','+PROC_CODE+',' LIKE '%,".$getEmployeeData['PROC_CODE'].",%' )"; }
                    if(trim($rowy['GRD_CODE']) !=''){ $where .= (trim($getEmployeeData['GRD_CODE'] == '')) ?  "" : " AND ( ','+GRD_CODE+',' LIKE '%,".$getEmployeeData['GRD_CODE'].",%' )"; }
                    if(trim($rowy['DSG_CODE']) !=''){ $where .= (trim($getEmployeeData['DSG_CODE'] == '')) ?  "" : " AND ( ','+DSG_CODE+',' LIKE '%,".$getEmployeeData['DSG_CODE'].",%' )"; }
            
                echo $sqlq = "SELECT HDATE,HDESC,IsAdditionalHoliday FROM Holidays WHERE HDATE='".$rowy['HDATE']."' AND H_status=1 ".$where." ";
                
                $resultp=query($query,$sqlq,$pa,$opt,$ms_db);
    
                if($resultq){
                    $tempArray4=sqlsrv_num_rows($resultp);
                }else{
                    $tempArray4=-1;
                }
                
                if($tempArray4>0) {
                    while ($rowq = $fetch($resultp)){ var_dump($rowq); die;
                        $result1[] =array('title'=>$rowq['HDESC'],'isAdditionalHoliday'=>$rowq['IsAdditionalHoliday'],'start'=>date('Y-m-d', strtotime($rowq['HDATE'])));
                    }
                    
                }
                $where  = "";
        }
        
        }
    
        //echo '<pre>'; print_r($result1); echo '</pre>'; die;
         //die;
    
        return $result1;
    }



    function getWeeklyOff($emp_code,$startDate,$endDate){

        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        $daysInMonth = abs(floor(strtotime($startDate) / (60 * 60 * 24)) - floor(strtotime($endDate) / (60 * 60 * 24)));
        $firstDate= $startDate; //$year.'-'.$month.'-01';
        $lastDate = $endDate; //$year.'-'.$month.'-'.$daysInMonth;


        $startDay   = date('d', strtotime($startDate));
        $month = date('m', strtotime($firstDate));
        $year = date('Y', strtotime($firstDate));   

        $endDay     = date('d', strtotime($endDate));
        $endMonth = date('m', strtotime($endDate));
        $endYear = date('Y', strtotime($endDate));

        $startDateEndDay = date('t',strtotime($firstDate));
        $endDateEndDay = date('t',strtotime($lastDate));
        
        
            if($startDay==1 && $month == $endMonth){
                $payCycle[] = array('from'=>$startDate,'to'=>$endDate);
            }else{
                $payCycle[]=array('from'=>$startDay.'-'.$month.'-'.$year,'to'=>$startDateEndDay.'-'.$month.'-'.$year);
                $payCycle[]=array('from'=>'01-'.$endMonth.'-'.$endYear,'to'=>$endDay.'-'.$endMonth.'-'.$endYear);
            }

            $result= array();
            $monthlyRecord = Array();
            $finalRecords = Array();
            foreach ($payCycle as $key => $value) {
            //pr($value); 
            $firstDate = $value['from'];
            $lastDate = $value['to'];
            $fdate = date('d', strtotime($firstDate));
            $fmonth = date('m', strtotime($firstDate));
            $fyear = date('Y', strtotime($firstDate));


            $ldate = date('d', strtotime($lastDate));
            $lmonth = date('m', strtotime($lastDate));
            $lyear = date('Y', strtotime($lastDate));

            //set default pattern to sunday
            $attto_timestamp = strtotime($lastDate);
            $ShiftPatternMastidSql = "SELECT ShiftPatternMastid FROM ShiftPatternMast WHERE ShiftPattern_Name = 'AutoShiftP' AND ShiftPattern_Code = '01'";

            $ShiftPatternMastidResult=query($query,$ShiftPatternMastidSql,$pa,$opt,$ms_db);
            $ShiftPatternMastData = $fetch($ShiftPatternMastidResult);
            $result[] =array('patternId'=>(!empty($ShiftPatternMastData['ShiftPatternMastid'])) ? $ShiftPatternMastData['ShiftPatternMastid'] : 0,'from'=>$fdate,'to'=>$ldate);

            

            $sqlq="SELECT ShiftPatternMastID,cast(rosterstart as date) attfrom,cast(rosterend as date) attto 
            FROM Rosterqry 
            WHERE EMP_CODE='".$emp_code."' 
            AND ((DATEPART(MM, rosterstart) <='".$month."'AND (DATEPART(YY, rosterstart)='".$year."' OR DATEPART(YY, rosterend)='".$year."') ) 
            OR   (DATEPART(MM, rosterend ) >= '".$month."' AND (DATEPART(YY, rosterstart)='".$year."' OR DATEPART(YY, rosterend)='".$year."')))
            ORDER BY rosterstart ASC";
            $resultq=query($query,$sqlq,$pa,$opt,$ms_db);
            if($resultq){
                $tempArray2 = $num($resultq);
            }else{
                $tempArray2=-1;
            }


            if($tempArray2>0) {
                $result= array();
                while ($rowq = $fetch($resultq)){
                    $f = explode('-',$rowq['attfrom']);
                    
                    $t = explode('-',$rowq['attto']);
                    $attto_timestamp = strtotime($rowq['attto']);

                    $result[] =array('patternId'=>$rowq['ShiftPatternMastID'],'from'=>$fdate,'to'=>$ldate);
                }

            
            }else{
                $sqlq1="SELECT TOP 1 ShiftPatternMastID FROM AttRoster WHERE EMP_CODE='".$emp_code."' AND automatic_period='1'
                    ORDER BY UPDATED_ON DESC";
                
                $resultq1=query($query,$sqlq1,$pa,$opt,$ms_db);
                
                if($resultq1){
                    $tempArray3 = $num($resultq1);
                }else{
                    $tempArray3=-1;
                }

                if($tempArray3>0) {
                    while ($rowq1 = $fetch($resultq1)){
                        $result[] =array('patternId'=>$rowq1['ShiftPatternMastID'],'from'=>$fdate,'to'=>$ldate);
                    }
                }
            }

            $tempArray = array();
            $tempFromTo = array();

            for($i=0;$i<count($result); $i++){
                $tempArray[] = $result[$i]['patternId'];
                $tempFromTo[] =array($result[$i]['from'],$result[$i]['to']);
            }

            $ShiftPatternMastids = implode(', ', $tempArray);
            
            $weeklyOffArrayForMonth = Array();
            $sqlq2 = "SELECT WeeklyOff1, WeeklyOff2, WeeklyOff3, WeeklyOff4, WeeklyOff5 FROM ShiftPatternMast WHERE ShiftPatternMastid in (".$ShiftPatternMastids.")";
            $resultq2=query($query,$sqlq2,$pa,$opt,$ms_db);
            if($resultq2){
                $tempArray1 = $num($resultq2);
            }else{
                $tempArray1 =-1;
            }
            if($tempArray1>0) {
                while ($rowq2 = $fetch($resultq2)){
                    //var_dump($rowq2);die;
                    $givenArray[] = array($rowq2['WeeklyOff1'], $rowq2['WeeklyOff2'], $rowq2['WeeklyOff3'], $rowq2['WeeklyOff4'], $rowq2['WeeklyOff5']);
                }
            }

            //$monthlyRecord = Array();
            //$finalRecords = Array();
            for($j=1;$j<=7;$j++){
                $days = $this->getSundays($fyear,$fmonth,$j);
                $monthlyRecord[$j] = $days;
            }

            for($p=0;$p<count($tempFromTo);$p++){
                $r = (int)$tempFromTo[$p][0];
                $k = 0;
                
                if($r<=$tempFromTo[$p][1]){
                    while($k<count($givenArray[$p])){
                        $givenDaysPerWeek = explode(',',$givenArray[$p][$k]);
                        for($q=0;$q<count($givenDaysPerWeek);$q++){
                            for($h=0;$h<count($monthlyRecord);$h++){
                                try{
                                    @$d = $monthlyRecord[(int)$givenDaysPerWeek[$q]][$k];
                                    if($d >= $tempFromTo[$p][0] && $d<=$tempFromTo[$p][1]){
                                        $dateff1 = "{$fyear}-{$fmonth}-".sprintf("%02d", $d);
                                        //echo $attto_timestamp . ' and ' . strtotime($dateff1);
                                        //Check roster end date must be greater then to current date
                                        if($attto_timestamp > strtotime($dateff1))
                                            $finalRecords[] =array('start'=>$dateff1);
                                        goto end3;
                                    }
                                }catch(Exception $e){
                                }
                                
                            }
                            end3:
                        }$k++;
                    }
                }
            }
        }
        sort($finalRecords);
        $startDatef=date('Y-m-d',strtotime($startDate));
        $newFinalArray  = array();
        foreach($finalRecords AS $key => $value){ //echo $startDatef;
            if(strtotime($value['start']) == strtotime($startDatef) && !in_array($startDatef, $newFinalArray)){
                $newFinalArray[] = date('m/d/Y', strtotime($value['start']));
            }
        }
        //print_r($finalRecords); 
        return $newFinalArray;
    }

    function getSundays($y,$m,$dayNo){ 
            $date = "$y-$m-01";
            $first_day = date('N',strtotime($date));
            $first_day = $dayNo - $first_day + 1;
            $last_day =  date('t',strtotime($date));

            $days = array();
            for($i=$first_day; $i<=$last_day; $i=$i+7 ){
                if($i>0){
                    $days[] = $i;
                }
            }
            return  $days;
    }



   //calculate all attenndance from start date to end date and return json response
  
 
    function getEmployeeAttendance($empCode, $startDate, $endDate) {
        //echo $empCode.", ".$startDate.", ".$endDate; die;
        $empCode    = trim($empCode);
        $startDate  = trim($startDate);
        $endDate    = trim($endDate);
        $emp_code   = $empCode;
        $endDate    = (empty($endDate))?$startDate:$endDate;

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        
        $dateForGetPreviousRecord   =   '2017-01-31';
        
        
        if($startDate <= $dateForGetPreviousRecord){
                include_once('employee-back-attendence.php');
        }else{

        //check date is valid or not
        $startDateFormat = date_parse($startDate);
        $endDateFormat = date_parse($endDate);

        if(empty($empCode) || empty($startDate) || empty($endDate) || $startDateFormat['error_count'] > 0 || $endDateFormat['error_count'] > 0) return false;

        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        global $firstRosterStart;
        $step = '+1 day';
        $date_output_format = 'Y-m-d';
        
        //find employee auto shift and upload into roster schema
        $startDateMonth = date('m', strtotime($startDate)); $startDateYear = date('Y', strtotime($startDate));
        $endDateMonth = date('m', strtotime($endDate)); $endDateYear = date('Y', strtotime($endDate));
        $globalClassObj =   new global_class();
        

        $daysInMonth    = $this->countNumberOfDaysFromTwoDates($startDate,$endDate); //calculate days of two dates
        $firstDate      = date('Y-m-d', strtotime($startDate)); //$year.'-'.$month.'-01';                   //first date
        $lastDate       = date('Y-m-d', strtotime($endDate)); //$year.'-'.$month.'-'.$daysInMonth;      //last date
        $firstDateNum   = date('d', strtotime($startDate));                                     //first date number
        $lastDateNum    = $daysInMonth;                             //last date number
        
        $current = strtotime($startDate);
        $last = strtotime($endDate);
        $currentYear = date('Y', strtotime($startDate));

        while( $current <= $last ) {
            $dateValue = date($date_output_format, $current);
            $dayValue= date('d', $current);
            $current = strtotime($step, $current);
            $result[$dateValue] = array('type'=>'null','title'=>'','shiftName'=>'','shiftInOutTime'=>'','formattedDate'=>date('l, d M Y',strtotime($dateValue)),'presetStatus'=>'','presetShortStatus'=>'','start'=>$dateValue,'className'=>'','birthdate'=>array(),'annidate'=>array(),'startTime'=>'','endTime'=>'','allDay'=>true,'status'=>'','reason'=>'','timeSpan'=>'');
        }

        $getEmpRosterDate = "SELECT top 1 cast(RosterStart as date) as attFrom, cast(RosterEnd as date) as attTo from rosterqry where emp_code='".$empCode."' order by RostId asc";

        $resultq00=query($query,$getEmpRosterDate,$pa,$opt,$ms_db);
        if($num($resultq00)){
            $rowq00 = $fetch($resultq00);
            $firstRosterStart = explode('-', $rowq00['attFrom']);
            $firstRosterEnd = explode('-', $rowq00['attTo']);       
        }

        $sqlq0="SELECT ShiftPatternMastID, Shift_Name, Shift_From,Shift_To, cast(rosterstart as date) attfrom,cast(rosterend as date) attto 
                FROM Rosterqry 
                WHERE EMP_CODE='".$empCode."' AND 
                DATEADD(month, DATEDIFF(month, 0, '".$startDate."'), 0) 
                BETWEEN DATEADD(month, DATEDIFF(month, 0, rosterstart), 0) AND DATEADD(month, DATEDIFF(month, 0, rosterend), 0)";

        $resultq0=query($query,$sqlq0,$pa,$opt,$ms_db);

        if($num($resultq0)){
            while ($rowq0 = $fetch($resultq0)){
                $rosterStart = $rowq0['attfrom'];
                $rosterEnd  = $rowq0['attto'];
                $shift_From = date('g:i a', strtotime($rowq0['Shift_From']));
                $shift_To   = date('g:i a', strtotime($rowq0['Shift_To']));
                $shift_Name = $rowq0['Shift_Name'];
            }
        }

        $birthdates     = array();
        //$annidates        = getAnniDates($month,$year);
        $annidates      = array();
        $holidays       = getHolidays($startDate, $endDate, $emp_code);
        $leaves         = getLeaves($startDate, $endDate, $emp_code);

        $leavesFilterArray = array();
        if(!empty($leaves)){
            foreach($leaves AS $leaveKey => $leaveValue){
                $cleanDate = date('Y-m-d', strtotime($leaveValue['start']));
                $leavesDates[] = $cleanDate;
                $leavesFilterArray[$cleanDate] = array($leaveValue['status'],'leave',$leaveValue['reason'],$leaveValue['leaveNature']);
            } 
        }

        $firstDateNumc  = date('d', strtotime($startDate)); //01;
        $lastDateNumc   = date('d', strtotime($endDate)); //01;
        $weeklyOffs     = getWeeklyOff($emp_code,$startDate,$endDate);                                  // dependent events 

        $weeklyHolidayDates = array_merge($weeklyOffs,$holidays);
        $weeklyHolidayCombineDates  =   array();
        foreach($weeklyHolidayDates as $key=>$value) { $weeklyHolidayCombineDates[] = trim($value['start']); }
        $attendances    = getAttendance($emp_code,$startDate,$endDate,$weeklyHolidayCombineDates);   // solely dependent events 

        $attregularised = getAttendanceRegularised($emp_code,$startDate,$endDate);
        $getODRequest   = getODRequest($emp_code, $startDate, $endDate);
        $gatePassHours  = getGatePassHours($emp_code, $startDate, $endDate);

        //Filter array of markpast attendance
        $attRegularisedFilterArray = array();
        if(!empty($attregularised)){
            foreach($attregularised AS $attRegKey => $attReguValue){
                $attRegularDates[] = $attReguValue['start'];
                $attRegularisedFilterArray[$attReguValue['start']] = array($attReguValue['status'],'leave',$attReguValue['reason'],'('.$attReguValue['startTime'].' - '.$attReguValue['endTime'].')');
            }
        }
        
        // add birthdays list on calendar date event--------------------------------------------------------
        if($birthdates !=null || count($birthdates) !=0){
            for($m=0;$m<=count($birthdates);$m++){
                $date1 = explode('-', $birthdates[0]['start']);
                $cleanDate = date('Y-m-d', strtotime($birthdates[0]['start']));
                array_push($result[$cleanDate]['birthdate'],$birthdates[0]['bemp_record']);
                unset($birthdates[0]);
                $birthdates = array_values($birthdates);
                $m=0;

            }
        }


        //add anniversaries list on calendar date event-----------------------------------------------------
        if($annidates !=null || count($annidates) !=0){
            for($m=0;$m<=count($annidates);$m++){
                $date1 = explode('-', $annidates[0]['start']);
                $cleanDate = date('Y-m-d', strtotime($annidates[0]['start']));
                array_push($result[$cleanDate]['annidate'],$annidates[0]['bemp_record']);
                unset($annidates[0]);
                $annidates = array_values($annidates);
                $m=0;
            }
        }

        //add gate pass hours on calendar date event-----------------------------------------------------
        if($gatePassHours["result"] !=null || count($gatePassHours["result"]) !=0){
            foreach($gatePassHours["result"] AS $key => $value){
                $cleanDate = date('Y-m-d', strtotime($value['start']));
                $result[$cleanDate]['gatePassLate'] = array('start' => $cleanDate, 'totalHours' => $value['time'], 'reason' => $value['reason']);
            }
            if( $gatePassHours["count"] >= 2 ) {
                $result[$cleanDate]['gatePassLatePenalty'] = array('gatePassPenaltyMsssage' => "GatePass Movement Penalty - ".$gatePassHours["count"]); 
            }

        }

        // add attendance from cattendanceqry table in calendar date events---------------------------------
        if(!empty($attendances)){

            for($a=0;$a<count($attendances);$a++){
                
                $keyPair    =   date('Y-m-d', strtotime($attendances[$a]['start']));
                
                if($result[$keyPair]['type']=='weeklyoff' || $result[$keyPair]['type']=='pholiday'){
                    $result[$keyPair]['className'] = array($result[$keyPair]['className'].' '.$attendances[$a]['className'][0]);
                }else{
                    $result[$keyPair]['className']=$attendances[$a]['className'];   
                }

                $result[$keyPair]['type'] = $attendances[$a]['type'];
                $result[$keyPair]['title'] = $attendances[$a]['title'];
                $result[$keyPair]['shiftName'] = $attendances[$a]['shiftName'];
                $result[$keyPair]['shiftInOutTime'] = $attendances[$a]['shiftInOutTime'];
                $result[$keyPair]['presetShortStatus'] = $attendances[$a]['presetShortStatus'];
                $result[$keyPair]['presetStatus'] = $attendances[$a]['presetStatus'];
                $result[$keyPair]['startTime'] = $attendances[$a]['startTime'];
                $result[$keyPair]['endTime'] = $attendances[$a]['endTime'];
                $result[$keyPair]['timeSpan'] = $attendances[$a]['timeSpan'];
                $result[$keyPair]['attendanceMessage'] = $attendances[$a]['attendanceMessage'];
                $result[$keyPair]['lates'] = $attendances[$a]['lates'];
                $result[$keyPair]['isLateComing'] = $attendances[$a]['isLateComing'];
                $result[$keyPair]['isEarlyGoing'] = $attendances[$a]['isEarlyGoing'];
                
                /******************gate pass warning/penalty*************/
                foreach($gatePassHours['gatePassMessage'] AS $gatePassDate => $gateValues) {
                    if( isset($gatePassHours['gatePassMessage'][$keyPair]) ){
                        $result[$keyPair]['attendanceMessage'] = (!empty($attendances[$a]['attendanceMessage'])) ? $attendances[$a]['attendanceMessage'].' and '.$gatePassHours['gatePassMessage'][$keyPair]['message']:$gatePassHours['gatePassMessage'][$keyPair]['message'];

                        if($gatePassHours['gatePassMessage'][$keyPair]['isPenalty']){
                            $result[$keyPair]['title'] = 'Half Day';
                            $result[$keyPair]['presetShortStatus'] = 'H';
                            $result[$keyPair]['presetStatus'] = 'Half Day';
                            $result[$keyPair]['className'] = 'yellow circle-legend';
                        }
                    }
                }
                
                $result[$keyPair]['isValidForMaxlife'] = $attendances[$a]['isValidForMaxlife'];
                
                if($attendances[$a]['isValidForMaxlife'] == 0 && (($attendances[$a]['startTime'] == '' && $attendances[$a]['endTime'] != '') || ($attendances[$a]['startTime'] != '' && $attendances[$a]['endTime'] == '')))
                {
                    
                    $result[$keyPair]['className']=array('miss-punch circle-legend');;  
                }

                if($result[$attendances[$a]['num']]['presetStatus'] != 'Absent'){
                    //$arrayCheckOfDuplicateEvent variable used for check if employee worked on weekly off or holiday
                    $arrayCheckOfDuplicateEvent['attendance'][$keyPair] = $result[$keyPair]['className'];
                    $arrayCheckOfDuplicateEvent['attendancePunch'][$keyPair] = array('startTime'=>$attendances[$a]['startTime'], 'endTime' => $attendances[$a]['endTime']);
                }
            }
        }
        
        //prd($attendances);
        // add regularised attendance in calendar date events (status, reason)-------------------------------
        if(!empty($attregularised)){
            for($c=0;$c<count($attregularised); $c++){
                $date1 = explode('-', $attregularised[$c]['start']);
                $cleanDate = date('Y-m-d', strtotime($attregularised[$c]['start']));
                $shiftData = $this->getEmployeeShiftByDateNew($emp_code, $cleanDate);
                if(count($shiftData) > 0){
                    $result[$cleanDate]['shiftName']    ='Shift : '.$shiftData['shiftName'];
                    $result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
                }

                if($result[$cleanDate]['type']=='weeklyoff' || $result[$cleanDate]['type']=='pholiday'){
                    $result[$cleanDate]['className'] = array($result[$cleanDate]['className'][0].' '.$attregularised[$c]['className'][0]);
                } else {
                    $result[$cleanDate]['className']=$attregularised[$c]['className'];  
                }

                //$arrayCheckOfDuplicateEvent variable used for check if employee worked on weekly off or holiday
                $arrayCheckOfDuplicateEvent['attendanceRegularised'][] = $attregularised[$c]['startTime'];
                $arrayCheckOfDuplicateEvent['attendanceRegularisedDate'][] = $attregularised[$c]['reason'];

                $result[$cleanDate]['type']=$attregularised[$c]['type'];
                $result[$cleanDate]['title']=$attregularised[$c]['title'];
                $result[$cleanDate]['presetStatus'] = $attregularised[$c]['title'];
                $result[$cleanDate]['reason'] = $attregularised[$c]['reason'];
                $result[$cleanDate]['status'] = $attregularised[$c]['status'];
                $result[$cleanDate]['presetShortStatus']=$attregularised[$c]['presetShortStatus'];
                $result[$cleanDate]['startTime'] = $attregularised[$c]['startTime'];
                $result[$cleanDate]['endTime'] = $attregularised[$c]['endTime'];
                        
            }
        }

        // add approved/pending leaves in calendar date event -----------------------------------------------
        if(!empty($leaves)){
            for($b=0;$b<count($leaves);$b++){
                $date1 = explode('-', $leaves[$b]['start']);
                $leaveDate = date('Y-m-d', strtotime($leaves[$b]['start']));

                if($result[$leaveDate]['type']=='weeklyoff' || $result[$leaveDate]['type']=='pholiday'){
                    $result[$leaveDate]['className'] = array($result[$leaveDate]['className'][0].' '.$leaves[$b]['className'][0]);
                }else{
                    $result[$leaveDate]['className']= ($leaves[$b]['status'] != 'Pending' && $leaves[$b]['status'] != 'Absent' && $leaves[$b]['status'] != 'Approved Cancellation - Rejected' && $leaves[$b]['status'] != 'Approved Cancellation Request') ? 'blue_over circle-legend' : $leaves[$b]['className'][0];   
                }
                
                $result[$leaveDate]['type']=$leaves[$b]['type'];
                $result[$leaveDate]['title']=$leaves[$b]['title'];
                $result[$leaveDate]['presetShortStatus']=$leaves[$b]['presetShortStatus'];
                $result[$leaveDate]['status']=$leaves[$b]['status'];
                $result[$leaveDate]['reason']=$leaves[$b]['reason'];
                $result[$leaveDate]['firststatus'] = $leaves[$b]['status'];
                $result[$leaveDate]['LvType'] = $leaves[$b]['lvType'];
                
                //Check if user mark punch on same OD date
                if(array_key_exists($leaveDate, $arrayCheckOfDuplicateEvent['attendance'])){
                    $result[$leaveDate]['title'] = 'Leave Request';
                    $result[$leaveDate]['presetStatus'] = $leaves[$b]['title'].' and Present';
                    $result[$leaveDate]['status'] = $leaves[$b]['status'];
                    $result[$leaveDate]['firststatus'] = $leaves[$b]['status'];
                    $result[$leaveDate]['secondstatusForPunch'] = '<br />Punch Time: ('.$arrayCheckOfDuplicateEvent['attendancePunch'][$leaveDate]['startTime'].' - '.$arrayCheckOfDuplicateEvent['attendancePunch'][$leaveDate]['endTime'].')';
                    $result[$leaveDate]['flagType'] = 'leaveandpunch';
                    $result[$leaveDate]['status'] = ucfirst(str_replace('_', ' ', $leaves[$b]['leaveNature'])).' Leave + Present';
                    if($leaves[$b]['status'] == 'Approved' && !empty($arrayCheckOfDuplicateEvent['attendance'][$leaveDate]) ){
                        $result[$leaveDate]['title']= 'Present';
                        $result[$leaveDate]['className'] = array("green2 circle-legend");                   
                    }else if(($leaves[$b]['status'] == 'Approved' || !empty($arrayCheckOfDuplicateEvent['attendance'][$leaveDate])) 
                        && ($leaves[$b]['status'] != 'Approved' || empty($arrayCheckOfDuplicateEvent['attendance'][$leaveDate]) )) {
                        $result[$leaveDate]['className'] = array("presentAbsent circle-legend");
                    }else{
                        $result[$leaveDate]['title']= 'Absent';
                        $result[$leaveDate]['className'] = array("red circle-legend");
                    }
                }

                //Check if user not mark punch on same OD date
                if(!array_key_exists($leaveDate, $arrayCheckOfDuplicateEvent['attendance']) && $leaves[$b]['leaveNature'] != 'full_day'){
                    
                    $result[$leaveDate]['presetStatus'] = $leaves[$b]['title'].' and Absent';
                    $result[$leaveDate]['status'] = $leaves[$b]['status'];
                    $result[$leaveDate]['firststatus'] = $leaves[$b]['status'];
                    $result[$leaveDate]['flagType'] = 'leaveandpunch';
                    $result[$leaveDate]['status'] = ($leaves[$b]['leaveNature'] != 'full_day') ? '(Leave & Absent)' : $result[$leaveDate]['status'];

                    $result[$leaveDate]['status'] = ($result[$leaveDate]['leaveNature'] != 'full_day' && empty($arrayCheckOfDuplicateEvent['attendance'][$leaveDate])) ? ucfirst(str_replace('_', ' ', $result[$leaveDate]['leaveNature'])).' Leave + Absent' : $result[$leaveDate]['status'];

                    if($leaves[$b]['status'] == 'Approved' && $result[$leaveDate]['natureOfWorkCause'] == 'full_day' ){
                        $result[$leaveDate]['title']= 'OD Request';
                        $result[$leaveDate]['className'] = array("green2 circle-legend");                   
                    }else if( ($leaves[$b]['leaveNature'] != 'full_day') && ($leaves[$b]['status'] == 'Approved' || !empty($arrayCheckOfDuplicateEvent['attendance'][$leaveDate])) 
                        && ($leaves[$b]['status'] != 'Approved' || empty($arrayCheckOfDuplicateEvent['attendance'][$leaveDate]) )) {
                        $result[$leaveDate]['className'] = array("presentAbsent circle-legend");
                    }else{
                        $result[$leaveDate]['title']= 'Absent';
                        $result[$leaveDate]['className'] = array("red circle-legend");
                    }
                }

                //Check if user applied regularised attendance on same OD date
                if(in_array($leaves[$b]['start'],$attRegularDates)){
                    $result[$leaveDate]['title']= 'Leave & AttReg';
                    $result[$leaveDate]['presetStatus'] = 'Leave and '.$leaves[$b]['title'];
                    $result[$leaveDate]['status'] = ucfirst(str_replace('_', ' ', $leaves[$b]['leaveNature'])).' Leave and AttReg';
                    $result[$leaveDate]['firststatus'] = $leaves[$b]['status'];
                    $result[$leaveDate]['secondstatus'] = $attRegularisedFilterArray[$leaves[$b]['start']][0].'<br>AttReg Reason: '.$attRegularisedFilterArray[$leaveDate][2].'<br>AttReg Time: '.$attRegularisedFilterArray[$leaveDate][3];
                    
                    //$result[$leaveDate]['status'] = '(Leave & AttReg)';
                    if($leaves[$b]['status'] == 'Approved' && $attRegularisedFilterArray[$leaves[$b]['start']][0] == 'Approved'){
                        $result[$leaveDate]['title']= 'Present';    
                        $result[$leaveDate]['className'] = array("green2 circle-legend");                   
                    }else if(($leaves[$b]['status'] == 'Approved' || $attRegularisedFilterArray[$leaves[$b]['start']][0] == 'Approved') && ($leaves[$b]['status'] != 'Approved' || $attRegularisedFilterArray[$leaves[$b]['start']][0] != 'Approved')){
                        $result[$leaveDate]['className'] = array("presentAbsent circle-legend");
                    }else{
                        $result[$leaveDate]['title']= 'Absent';
                        $result[$leaveDate]['className'] = array("red circle-legend");
                    }
                }
            }
        }
        
        // add OD request in calendar date events (status, reason)-------------------------------
        if(!empty($getODRequest)){
            for($c=0;$c<count($getODRequest); $c++){
                $date1 = explode('-', $getODRequest[$c]['start']);
                $cleanDate = date('Y-m-d', strtotime($getODRequest[$c]['start']));
                $shiftData =$this->getEmployeeShiftByDateNew($emp_code, $getODRequest[$c]['start']);
                if(count($shiftData) > 0){
                    $result[$cleanDate]['shiftName']    ='Shift : '.$shiftData['shiftName'];
                    $result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
                }

                if($result[$cleanDate]['type']=='weeklyoff' || $result[$cleanDate]['type']=='pholiday'){
                    $result[$cleanDate]['className'] = array($result[$cleanDate]['className'][0].' '.$getODRequest[$c]['className'][0]);
                }else{
                    $result[$cleanDate]['className']=$getODRequest[$c]['className'];    
                }
                
                $result[$cleanDate]['type']=$getODRequest[$c]['type'];          
                $result[$cleanDate]['title']=$getODRequest[$c]['title'];
                $result[$cleanDate]['presetStatus'] = $getODRequest[$c]['title'];
                $result[$cleanDate]['status'] = $getODRequest[$c]['status'];
                $result[$cleanDate]['firststatus'] = $getODRequest[$c]['status'];
                
                $result[$cleanDate]['reason'] = $getODRequest[$c]['reason'];
                $result[$cleanDate]['presetShortStatus']= $getODRequest[$c]['presetShortStatus'];
                $result[$cleanDate]['startTime'] = date('h:i A', strtotime($getODRequest[$c]['startTime']));
                $result[$cleanDate]['endTime'] = date('h:i A', strtotime($getODRequest[$c]['endTime']));
      
                //OD type like first_half,second_half or full_day
                $result[$cleanDate]['natureOfWorkCause'] = $getODRequest[$c]['natureOfWorkCause'];
                
                $outOnDates[$cleanDate] = $getODRequest[$c]['natureOfWorkCause'];
     
                //Check if user mark punch on same OD date
                if(array_key_exists($cleanDate, $arrayCheckOfDuplicateEvent['attendance'])){
                    $result[$cleanDate]['title'] = 'OD Request';
                    $result[$cleanDate]['presetStatus'] = $getODRequest[$c]['title'].' and Present';
                    $result[$cleanDate]['status'] = $getODRequest[$c]['status'];
                    $result[$cleanDate]['firststatus'] = $getODRequest[$c]['status'];
                    $result[$cleanDate]['secondstatus'] = "Present By Punch";
                    $result[$cleanDate]['secondstatusForPunch'] = '<br />Punch Time: ('.$arrayCheckOfDuplicateEvent['attendancePunch'][$cleanDate]['startTime'].' - '.$arrayCheckOfDuplicateEvent['attendancePunch'][$cleanDate]['endTime'].')';
                    $result[$cleanDate]['flagType'] = 'odandpunch';
                    $result[$cleanDate]['status'] = ucfirst(str_replace('_', ' ', $result[$cleanDate]['natureOfWorkCause'])).' OD + Present';
                    if($getODRequest[$c]['status'] == 'Approved' && !empty($arrayCheckOfDuplicateEvent['attendance'][$cleanDate]) ){
                        $result[$cleanDate]['title']= 'Present';
                        $result[$cleanDate]['className'] = array("green2 circle-legend");                   
                    }else if(($getODRequest[$c]['status'] == 'Approved' || !empty($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) 
                        && ($getODRequest[$c]['status'] != 'Approved' || empty($arrayCheckOfDuplicateEvent['attendance'][$cleanDate]) )) {
                        $result[$cleanDate]['className'] = array("presentAbsent circle-legend");
                    }else{
                        $result[$cleanDate]['title']= 'Absent';
                        $result[$cleanDate]['className'] = array("red circle-legend");
                    }
                }
                //Check if user not mark punch on same OD date
                if(!array_key_exists($cleanDate, $arrayCheckOfDuplicateEvent['attendance'])){
                    
                    $result[$cleanDate]['presetStatus'] = $getODRequest[$c]['title'].' and Absent';
                    $result[$cleanDate]['status'] = $getODRequest[$c]['status'];
                    $result[$cleanDate]['firststatus'] = $getODRequest[$c]['status'];
                    $result[$cleanDate]['secondstatus'] = "Present By Punch";
                    $result[$cleanDate]['flagType'] = 'odandpunch';
                    $result[$cleanDate]['status'] = ($result[$cleanDate]['natureOfWorkCause'] != 'full_day') ? '(OD & Absent)' : $result[$cleanDate]['status'];

                    $result[$cleanDate]['status'] = ($result[$cleanDate]['natureOfWorkCause'] != 'full_day' && empty($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? ucfirst(str_replace('_', ' ', $result[$cleanDate]['natureOfWorkCause'])).' OD + Absent' : $result[$cleanDate]['status'];

                    if($getODRequest[$c]['status'] == 'Approved' && $result[$cleanDate]['natureOfWorkCause'] == 'full_day' ){
                        $result[$cleanDate]['title']= 'OD Request';
                        $result[$cleanDate]['className'] = array("green2 circle-legend");                   
                    }else if( ($result[$cleanDate]['natureOfWorkCause'] != 'full_day') && ($getODRequest[$c]['status'] == 'Approved' || !empty($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) 
                        && ($getODRequest[$c]['status'] != 'Approved' || empty($arrayCheckOfDuplicateEvent['attendance'][$cleanDate]) )) {
                        $result[$cleanDate]['className'] = array("presentAbsent circle-legend");
                    }else{
                        $result[$cleanDate]['title']= 'Absent';
                        $result[$cleanDate]['className'] = array("red circle-legend");
                    }
                }

                //Check if user applied leave request on same OD date
                if(in_array($cleanDate,$leavesDates)){
                    $result[$cleanDate]['title']= 'OD & Leave';
                    $result[$cleanDate]['presetStatus'] = $getODRequest[$c]['title'].' and Leave';
                    
                    $result[$cleanDate]['status'] = $getODRequest[$c]['status'].'/'.$leavesFilterArray[$cleanDate][0];
                    
                    $result[$cleanDate]['firststatus'] = $getODRequest[$c]['status'];
                    $result[$cleanDate]['secondstatus'] = $leavesFilterArray[$cleanDate][0].'<br>Leave Reason:'.$leavesFilterArray[$cleanDate][2];
                    $result[$cleanDate]['flagType'] = 'odandleave';             
                    
                    $result[$cleanDate]['status'] = '(OD & Leave)';

                    if($getODRequest[$c]['status'] == 'Approved' && $leavesFilterArray[$cleanDate][0] == 'Approved'){
                        $result[$cleanDate]['title']= 'Present';    
                        $result[$cleanDate]['className'] = array("green2 circle-legend");                   
                    }else if(($getODRequest[$c]['status'] == 'Approved' || $leavesFilterArray[$cleanDate][0] == 'Approved') 
                        && ($getODRequest[$c]['status'] != 'Approved' || $leavesFilterArray[$cleanDate][0] != 'Approved')) {
                        $result[$cleanDate]['className'] = array("presentAbsent circle-legend");
                    }else{
                        $result[$cleanDate]['title']= 'Absent';
                        $result[$cleanDate]['className'] = array("red circle-legend");
                    }
                }
                
                //Check if user applied regularised attendance on same OD date
                if(in_array($cleanDate,$attRegularDates)){
                    $result[$cleanDate]['title']= 'AttReg & OD';
                    $result[$cleanDate]['presetStatus'] = 'Attn Reg. and '.$getODRequest[$c]['title'];
                    $result[$cleanDate]['status'] = $attRegularisedFilterArray[$cleanDate][0].'/'.$getODRequest[$c]['status'];
                    $result[$cleanDate]['firststatus'] = $attRegularisedFilterArray[$cleanDate][0].'<br>Reason: '.$attRegularisedFilterArray[$cleanDate][2];
                    $result[$cleanDate]['secondstatus'] = $getODRequest[$c]['status'];
                    $result[$cleanDate]['flagType'] = 'odandattnreq';
                    
                    $result[$cleanDate]['status'] = '(AttReg & OD)';
                    if($getODRequest[$c]['status'] == 'Approved' && $attRegularisedFilterArray[$cleanDate][0] == 'Approved'){
                        $result[$cleanDate]['title']= 'Present';    
                        $result[$cleanDate]['className'] = array("green2 circle-legend");                   
                    }else if(($getODRequest[$c]['status'] == 'Approved' || $attRegularisedFilterArray[$cleanDate][0] == 'Approved') && ($getODRequest[$c]['status'] != 'Approved' || $attRegularisedFilterArray[$cleanDate][0] != 'Approved')){
                        $result[$cleanDate]['className'] = array("presentAbsent circle-legend");
                    }else{
                        $result[$cleanDate]['title']= 'Absent';
                        $result[$cleanDate]['className'] = array("red circle-legend");
                    }
                }
            }
        }

        // add public holidays on specific festival date in calendar date event-----------------------------
        for($j=0;$j<count($holidays);$j++){
            $date1 = explode('-', $holidays[$j]['start']);
            $cleanDate = date('Y-m-d', strtotime($holidays[$j]['start']));
            $result[$cleanDate]['type']='pholiday';

                $shiftData = $this->getEmployeeShiftByDateNew($emp_code, $holidays[$j]['start']);
                if(count($shiftData) > 0){
                    $result[$cleanDate]['shiftName']    ='Shift : '.$shiftData['shiftName'];
                    $result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
                }

                $result[$cleanDate]['title'] = (isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? 'POH' : $holidays[$j]['title'];
                //check if same day OD Request
                $result[$cleanDate]['title'] = (array_key_exists($cleanDate, $outOnDates)) ? 'POH & '.ucfirst(str_replace('_', ' ', $outOnDates[$cleanDate])).' OD' : $result[$cleanDate]['title'];

                $result[$cleanDate]['presetShortStatus']='F';
                $result[$cleanDate]['className']=(isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? $arrayCheckOfDuplicateEvent['attendance'][$cleanDate][0].' '."bgnone" : array("green");
                //replace original class to present status class when POW, POH status found
                    $result[$cleanDate]['className'] = ($result[$cleanDate]['title'] == 'POW' || $result[$cleanDate]['title'] == 'POH') ? "green2 circle-legend bgnone" : $result[$cleanDate]['className'];
            }

            // add empty date event as Absent in calendar date event ---------------------------------------------
            $current = strtotime($firstDate);
            $last = (isset($_POST['requestFrom']) && $_POST['requestFrom'] == 'payrollCalendar') ? strtotime($lastDate) : strtotime($endDate);
            $currentDateConverter = strtotime(date('Y-m-d'));
            while( $current <= $last ) {
                $syncDate = date('Y-m-d', $current);

                $shiftData = $this->getEmployeeShiftByDateNew($emp_code, $syncDate);
                if(count($shiftData) > 0){
                    $result[$syncDate]['shiftName'] ='Shift : '.$shiftData['shiftName'];
                    $result[$syncDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
                }

                $current = strtotime($step, $current);
                if($currentDateConverter >=  $current){
                    if($result[$syncDate]['type']=='null'){
                        $result[$syncDate]['type']='empty_attendance';
                        $result[$syncDate]['title']='Absent';
                        $result[$syncDate]['presetShortStatus']='A';
                        $result[$syncDate]['presetStatus']='Absent';
                        $result[$syncDate]['timeSpan']=0;
                        $result[$syncDate]['className']=array('red circle-legend');
                    }
                }
            }
            
            

            // add employee wise weeklyoff on calendar date event-----------------------------------------------
            if(!empty($weeklyOffs)){
                for( $i = 0; $i < count($weeklyOffs); $i++ ) {
                    $date2 = explode('-', $weeklyOffs[$i]['start']);
                    $cleanDate = date('Y-m-d', strtotime($weeklyOffs[$i]['start']));
                    if(strtotime($cleanDate) <= strtotime($endDate) && strtotime($cleanDate) >= strtotime($startDate) ){
                    $result[$cleanDate]['type']='weeklyoff';


                    $shiftData = $this->getEmployeeShiftByDateNew($emp_code, $cleanDate);
                    if(count($shiftData) > 0){
                        $result[$cleanDate]['shiftName']    ='Shift : '.$shiftData['shiftName'];
                        $result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
                    }

                    /******************gate pass warning/penalty*************/
                    foreach($gatePassHours['gatePassMessage'] AS $gatePassDate => $gateValues) {
                        if( isset($gatePassHours['gatePassMessage'][$cleanDate]) ){
                            $result[$cleanDate]['attendanceMessage'] = (!empty($attendances[$a]['attendanceMessage'])) ? $attendances[$a]['attendanceMessage'].' and '.$gatePassHours['gatePassMessage'][$cleanDate]['message']:$gatePassHours['gatePassMessage'][$cleanDate]['message'];

                            if($gatePassHours['gatePassMessage'][$cleanDate]['isPenalty']){
                                $result[$cleanDate]['title'] = 'Half Day';
                                $result[$cleanDate]['presetShortStatus'] = 'H';
                                $result[$cleanDate]['presetStatus'] = 'Half Day';
                                $result[$cleanDate]['className'] = 'yellow circle-legend';
                            }
                        }
                    }

                    $result[$cleanDate]['title'] = (isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? 'POW' : 'Weekly Off' ;

                    //check if same day OD Request
                    $result[$cleanDate]['title'] = (array_key_exists($cleanDate, $outOnDates)) ? 'POW & '.ucfirst(str_replace('_', ' ', $outOnDates[$cleanDate])).' OD' : $result[$cleanDate]['title'];

                    //check if same day leave Request
                    $result[$cleanDate]['title'] = (array_key_exists($cleanDate, $leavesFilterArray)) ? 'POW & '.ucfirst(str_replace('_', ' ', $leavesFilterArray[$cleanDate][3])).' Leave' : $result[$cleanDate]['title'];

                    $result[$cleanDate]['presetShortStatus']='W';

                    $result[$cleanDate]['className']= (isset($arrayCheckOfDuplicateEvent['attendance'][$cleanDate])) ? $arrayCheckOfDuplicateEvent['attendance'][$cleanDate][0].' '."bgnone" : array("green");

                    
                    $result[$cleanDate]['className'] = (strpos($result[$cleanDate]['title'], 'POW') !== false || strpos($result[$cleanDate]['title'], 'POH') !== false) ?
                        "green2 circle-legend bgnone" :
                        $result[$cleanDate]['className'];
                    }
                }
            }
        
            // unset weekly offs if they are on holiday's date--------------------------------------------------
            if($holidays != null && $weeklyOffs != null){
                for($i=0;$i<count($holidays);$i++){
                    for($j=0;$j<count($weeklyOffs);$j++){
                        $shiftData = $this->getEmployeeShiftByDateNew($emp_code, $holidays[$i]['start']);
                        $cleanDate = date('Y-m-d', strtotime($holidays[$i]['start']));
                        if(count($shiftData) > 0){
                            $result[$cleanDate]['shiftName']    ='Shift : '.$shiftData['shiftName'];
                            $result[$cleanDate]['shiftInOutTime']= $shiftData['shiftInOutTime'];
                        }
                        if(in_array($cleanDate,$weeklyOffs[$j],true)){
                            unset($weeklyOffs[$j]);
                            $weeklyOffs = array_values($weeklyOffs);
                        }
                        
                    }
                }
            }
            
        }
        //echo '<pre>'; print_r($result); echo '</pre>'; die;

        foreach($result AS $key => $value){
            $data[] = $value;
        }

        return $data;
        //echo json_encode($data,true);
    }



}
?>