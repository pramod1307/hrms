<?php
//include ('../../configdata.php');


class OTRequestemployee_class{
    function __construct(){
      
    }

	function getData($id){
		
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

		
        
        $is_hr = Is_hr($id,$query,$pa,$opt,$ms_db,$fetch); // return true if loged in user has HR role assigned
        if($id == 'admin' || $is_hr) {

            $sql="select REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE,
             REPLACE(CONVERT(VARCHAR(11), OT_ApplyDateTime, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(OT_ApplyDateTime as smalldatetime), 109),':00:000', ' '), 8)), 11) as OT_ApplyDateTime,
             REPLACE(CONVERT(VARCHAR(11), FirstApprover, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(FirstApprover as smalldatetime), 109),':00:000', ' '), 8)), 11) as FirstApprover,
             REPLACE(CONVERT(VARCHAR(11), secApprover, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(secApprover as smalldatetime), 109),':00:000', ' '), 8)), 11) as secApprover,
             REPLACE(CONVERT(VARCHAR(11), thirdApprover, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(thirdApprover as smalldatetime), 109),':00:000', ' '), 8)), 11) as thirdApprover,
             OT_Category,OT_Shift,OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,App_OTRemark,CreatedBy,OT_NewFunction,finalOtHour from overtime_Balances order by cast(OT_ApplyDateTime as date) desc";
            
        } else {
           $sql="select REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE,
             REPLACE(CONVERT(VARCHAR(11), OT_ApplyDateTime, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(OT_ApplyDateTime as smalldatetime), 109),':00:000', ' '), 8)), 11) as OT_ApplyDateTime,
             REPLACE(CONVERT(VARCHAR(11), FirstApprover, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(FirstApprover as smalldatetime), 109),':00:000', ' '), 8)), 11) as FirstApprover,
             REPLACE(CONVERT(VARCHAR(11), secApprover, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(secApprover as smalldatetime), 109),':00:000', ' '), 8)), 11) as secApprover,
             REPLACE(CONVERT(VARCHAR(11), thirdApprover, 106), ' ', '-') +' '+ RIGHT('0' + LTRIM(Right(REPLACE(CONVERT(varchar(MAX),CAST(thirdApprover as smalldatetime), 109),':00:000', ' '), 8)), 11) as thirdApprover,
             OT_Category,OT_Shift,OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,App_OTRemark,CreatedBy,OT_NewFunction,finalOtHour from overtime_Balances where Emp_Code='$id' order by cast(OT_ApplyDateTime as date) desc";
        }

        $result=query($query,$sql,$pa,$opt,$ms_db);
		
		return $result;
        
    }
	function getManagerDataForAjax($id,$key){
		
		global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

		

        $sql="select top 1 replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as OT_ApplyDateTime,replace(convert(varchar(19),FirstApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,FirstApprover,100),8)),7) as FirstApprover,replace(convert(varchar(19),secApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,secApprover,100),8)),7) as secApprover,
            replace(convert(varchar(19),thirdApprover,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,thirdApprover,100),8)),7) as thirdApprover,
                OT_Category,OT_Shift,REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OT_DATE, OT_StartTime,OT_EndTime,overTimeHours,OT_purpose,appStatus,OT_NewFunction,
                    App_OTRemark from overtime_Balances where CreatedBy='$id' and OT_Key='".$key."'";

		
        $result=query($query,$sql,$pa,$opt,$ms_db);
		
		return $result;
		
	}
    function getManagerData($id){
		
		global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

		
       //$sql="select DISTINCT(OT_Key),OT_ApplyDateTime,OT_Category,OT_Shift,OT_DATE,OT_StartTime,OT_EndTime,overTimeHours,OT_purpose from overtime_Balances where CreatedBy=".$id."";
        $sql="select OT_Key,cast(OT_ApplyDateTime as date) from overtime_Balances where CreatedBy='$id' group by OT_Key,cast(OT_ApplyDateTime as DATE)  order by cast(OT_ApplyDateTime as date) desc";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
		while ($row =$fetch($result)) {
            $otkey[$i]=$row['OT_Key'];
            $i++;
        }
		return array($otkey,$i);
		
	}
    function getCostName($mngrcode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select COST_CODE,COST_NAME from hrdmastqry where Emp_Code='$mngrcode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);
        
        return array($row['COST_CODE'],$row['COST_NAME']);

    }
	function getappliedOT_Category($catID){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select LOV_Text from LOVMast where LOV_Field='OTCategory' AND LOV_VAlue='$catID'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
       $row= $fetch($result);
		
        return $row['LOV_Text'];
    }
   
	function getappliedOT_Purpose($purId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTPurpose' AND LOV_Value='$purId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getappliedOT_Reason($reasId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTNewReqQues' AND LOV_Value='$reasId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getEmployeeName($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Emp_Code,Emp_Name from EmpNameQry where EMP_CODE ='$empCode' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
      
        while ($row=$fetch($result)) {
            $Emp_Code=$row['Emp_Code'];
            $Emp_Name=$row['Emp_Name'];
           
        }

        return array($Emp_Code,$Emp_Name);
    }


    function getEmployeeOT_Balances($empCode,$OT_Date){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sqlGlobBal="select overTimeHours from overtime_global where Id=4";
        $resGlobBal=query($query,$sqlGlobBal,$pa,$opt,$ms_db);
        
        if($resGlobBal){
             $rowGlobBal=$fetch($resGlobBal);

            $sql="select hourOT from quartely_ot where Emp_code='$empCode' and '$OT_Date' between startDate and endDate";
            $result=query($query,$sql,$pa,$opt,$ms_db);
          
            while ($row=$fetch($result)) {
                 $hourOT=$row['hourOT'];
                $test=explode(":", $rowGlobBal['overTimeHours']);
                $test1=explode(":", $hourOT);
                //print_r($test); print_r($test1);
                if($test[1] < $test1[1]){
                    $hour   =  $test[0] - 1;
                    $minutes  =  $test[1]+60;
                    
                    $minutes = sprintf("%02d", $minutes-$test1[1]);
                    $hour    =  sprintf("%02d", $hour-$test1[0]);             
                                
                }else{
                    $minutes = $test[1]-$test1[1];
                    $hour    =   $test[0]-$test1[0];           
                    $minutes =  sprintf("%02d", $minutes);
                    $hour    =  sprintf("%02d", $hour); 
                }
                $quarterly=$hour.":".$minutes;
            }
        }
        return $quarterly;
    }

    
}

?>