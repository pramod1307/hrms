<?php

class OT_Finalization_class{
    function __construct(){
      
    }

    function getFinYear(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select FY,FY_Type,convert(varchar(10),FY_Start,103)as FY_Start from FinYear where FY_Type='F' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        while ($row=$fetch($result)) {
            $FinalYear=$row['FY'];
            $FY_Type=$row['FY_Type'];
            $FY_Start=$row['FY_Start'];
            $i++;
        }
        return array($FinalYear,$FY_Type,$FY_Start,$i);
    }

    function getForm(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc from TablesFields where Table_Name='overTime' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_Desc[$i]=$row['Field_Desc'];
           
            $i++;
        }

        return array($Field_Desc,$i);
    }

    function getTable(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc from TablesFields where Table_Name='overTimeFinal' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_TDesc[$i]=$row['Field_Desc'];
            $i++;
        }

        return array($Field_TDesc,$i);
    } 
   
    function getFornValue(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select * from overtime_balances";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_TDesc[$i]=$row['Field_Desc'];
            $i++;
        }

        return array($Field_TDesc,$i);

    }

    function getCostName($mngrcode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select COST_CODE,COST_NAME from hrdmastqry where Emp_Code='$mngrcode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);
        
        return array($row['COST_CODE'],$row['COST_NAME']);

    }

    /*--------Start OT Finalization List Function----------*/

    function getOTKeyFinal($code){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select OT_Key,cast(OT_ApplyDateTime as date) from overtime_Balances where CreatedBy='$code' group by OT_Key,cast(OT_ApplyDateTime as date) order by cast(OT_ApplyDateTime as date) desc";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row= $fetch($result)) {
            $OT_Key[$i]=$row['OT_Key'];
            $i++;
        }

        return array($OT_Key,$i);


    }

    function getappliedOT_Category($catID){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select LOV_Text from LOVMast where LOV_Field='OTCategory' AND LOV_VAlue='$catID'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
       $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getappliedOT_Purpose($purId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTPurpose' AND LOV_Value='$purId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getappliedOT_NewRequest($newReqId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTNewReqQues' AND LOV_Value='$newReqId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

    function getDetailsFinal($OT_Key,$code){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

      $sql="select top 1  REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OTDATE,
      replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as OTApplyDateTime
,* from overtime_Balances where CreatedBy='$code' and OT_Key='$OT_Key'";

        //$sql="select OT_Key from overtime_Balances where CreatedBy='$code' group by OT_Key";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row= $fetch($result)) {
            $OT_Category=$row['OT_Category'];
            $OT_Date=$row['OTDATE'];
            $OT_Shift=$row['OT_Shift'];
            $OT_StartTime=$row['OT_StartTime'];
            $OT_EndTime=$row['OT_EndTime'];
            $OT_overTimeHours=$row['overTimeHours'];
            $OT_purpose=$row['OT_purpose'];
            $OT_AppyDateTime=$row['OTApplyDateTime'];
            $OT_NewFunction=$row['OT_NewFunction'];
            $OT_Other=$row['OT_Other'];
            $i++;
        }

        return array($OT_Category,$OT_Date,$OT_Shift,$OT_StartTime,$OT_EndTime,$OT_overTimeHours,$OT_purpose,$OT_AppyDateTime,$OT_NewFunction,$OT_Other,$i);
    }


    function getEmpTableDetails($otkey,$code){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select REPLACE(CONVERT(VARCHAR(11), OT_Date, 106), ' ', '-') as OTDATE,
replace(convert(varchar(19),OT_ApplyDateTime,106),' ','-') + ' ' + RIGHT('0'+LTRIM(RIGHT(CONVERT(varchar,OT_ApplyDateTime,100),8)),7) as OTApplyDateTime,* from overtime_Balances where CreatedBy='$code' AND OT_Key='$otkey'";
      $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row= $fetch($result)) {
            $Emp_Code[$i]=$row['Emp_Code'];
            $OT_Date[$i]=$row['OTDATE'];
            $OT_StartTime[$i]=$row['OT_StartTime'];
            $OT_EndTime[$i]=$row['OT_EndTime'];
            $OT_overTimeHours[$i]=$row['overTimeHours'];
            $OT_AppyDateTime[$i]=$row['OTApplyDateTime'];
            $status[$i]=$row['status'];
            $finalRemark[$i]=$row['finalRemark'];
            $OT_Shift[$i]=$row['OT_Shift'];
            $i++;
        }

        return array($Emp_Code,$OT_Date,$OT_StartTime,$OT_EndTime,$OT_overTimeHours,$OT_AppyDateTime,$status,$finalRemark,$OT_Shift,$i);

    }

    /*function getEmployeeShiftByDate($empCode, $date,$othours1){
        $date = date('Y-m-d',strtotime($date));
        if(empty($empCode) || empty($date)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        $date1=$date;
        $explodeDate = explode('-', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];


        $SqlQuery =  "SELECT rost.EMP_CODE, 
        convert(varchar(10),RosterStart,103) as RosterStart,
        convert(varchar(10),RosterEnd,103) as RosterEnd,
        convert(char(5),rost.SHIFT_TO) AS SHIFT_TO,  
        convert(char(5),c.IN_TIME,108) AS IN_TIME, 
        convert(char(5),c.OUT_TIME,108) AS OUT_TIME,convert(char(16),OUT_TIME,121) as new_out
        FROM rosterQry rost
        LEFT JOIN CAttendanceqry C ON LEFT(CONVERT(VARCHAR,C.ATTDATE, 120), 10)='$date1' AND c.emp_code=rost.emp_code  
        WHERE '$convertDate' BETWEEN RosterStart AND RosterEnd AND rost.EMP_CODE ='$empCode'
        ";


        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
           $i=0;
        while ($row = $fetch($result)) {
            
		$sql1="select intime,outtime,AttnKey,COUNT(AttnKey) as countno from markPastAttendance where  '$date1' between CONVERT (VARCHAR(10),date_from,121) and CONVERT (VARCHAR(10),date_to,121) and CreatedBy='$empCode' group by AttnKey,intime,outtime"; 
		$sql11result = query($query,$sql1,$pa,$opt,$ms_db);
		$sql111row = $fetch($sql11result);
		$count1=$sql111row['countno'];
		if($count1 > 0)
		{
			$Shift_To=$row['SHIFT_TO'];
			$in_time=date("H:i",strtotime($sql111row['intime']));
			$out_timep=date("H:i",strtotime($sql111row['outtime']));
			$out_time=$out_timep;
			$out_time_new=$date1.' '.$out_timep;

		}
        else
        {
            $out_timep=$row['new_out'];
            $Shift_To=$row['SHIFT_TO'];
            $in_time=$row['IN_TIME'];
            $getActualPunchTimeQuery = "SELECT convert(char(16),Outtime,121) as Outtime FROM fnPunchTime('".$empCode."','".$date1."')"; 
           
            $getActualPunchTimeResult = query($query,$getActualPunchTimeQuery,$pa,$opt,$ms_db);
            $count11=0;
                if ($getActualPunchTimeRow=$fetch($getActualPunchTimeResult)) {
    		        $temp=$getActualPunchTimeRow["Outtime"];
                   $count11++;
                }
            //echo $num($getActualPunchTimeResult);
            if($count11 > 0){
            //$getActualPunchTimeRow  = $fetch($getActualPunchTimeResult);
            //echo "aa".$getActualPunchTimeRow["Outtime"];
            $out_time11 = (!empty($temp)) ? $temp: $out_timep;
 	       $rostersql = "select top 1 convert(char(5),ShiftEnd_SwTime,108) AS ShiftEnd_SwTime,convert(char(5),SHIFT_TO,108) AS Shiftto from RosterQry where  '$date1' between CONVERT (VARCHAR(10),RosterStart,121) and CONVERT (VARCHAR(10),RosterEnd,121) and EMP_CODE='$empCode' order by RostID DESC"; 
            $rosterResult = query($query,$rostersql,$pa,$opt,$ms_db);
        	$rowq00= $fetch($rosterResult);
            $siftendswap=$rowq00['ShiftEnd_SwTime'];
        	$otTimeArray = explode(":",$othours1);
        	$endshifttime=$date1.' '.$rowq00['Shiftto'];
        	$newtemphours=date('Y-m-d H:i',strtotime('+'. $otTimeArray[0].' hour +'. $otTimeArray[1].' minutes',strtotime($endshifttime)));
	        $swapArray = explode(":",$siftendswap);
            $newtemphours=date('Y-m-d H:i',strtotime('+'. $swapArray[0].' hour +'. $swapArray[1].' minutes',strtotime($newtemphours)));
            //print_r($newtemphours);
            if(strtotime($newtemphours) >= strtotime($out_time11)) 
            {
                        $out_time_new=$out_time11;
                        $out_time11 = strtotime($out_time11);
                        $out_time= date('H:i', $out_time11);
            }
            }
            }
          // echo $out_time;
            $i++;
        }

        return array(
            $Shift_To,
            $in_time,
            $out_time,
            $i,
            $out_time_new
        );
    }*/

    function getMinutes($OutTime,$InTime){
        return (abs(strtotime($OutTime) - strtotime($InTime)) / 60);    
    }

    function getMarkPastAttendance($empCode, $date,$othours1){
        $date = date('Y-m-d',strtotime($date));
        if(empty($empCode) || empty($date)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        $date1=$date;
        $explodeDate = explode('-', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];


        $SqlQuery =  "SELECT convert(char(7),SHIFT_TO) AS SHIFT_TO FROM rosterQry WHERE '$convertDate' BETWEEN RosterStart AND RosterEnd AND EMP_CODE ='$empCode' ";
          
        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
           
        while ($row = $fetch($result)) {
            
        $shiftTo=$row['SHIFT_TO'];

           $sql1="select intime,outtime,markPastId,action_status,CONVERT (VARCHAR(10),outDate,121) as outDate,CONVERT (VARCHAR(10),inDate,121) as inDate from markPastAttendance where '$date1' between CONVERT (VARCHAR(10),date_from,121) and CONVERT (VARCHAR(10),date_to,121) and CreatedBy='$empCode' order by markPastId asc
            ";

            $sql11result = query($query,$sql1,$pa,$opt,$ms_db);
            while($sql111row = $fetch($sql11result)){
                $action_status[]=$sql111row['action_status'];
                $in_time1[]=$sql111row['intime'];
                $out_timep1[]=$sql111row['outtime'];
                $inDate[]=$sql111row['inDate'];
                $outDate[]=$sql111row['outDate'];
            }
           // print_r($action_status);
            if(end($action_status)==2)
            {
                 $in_time=date("H:i",strtotime($in_time1[0]));
                 $out_timep=date("H:i",strtotime($out_timep1[0]));
                 $calOutDate=date("Y-m-d",strtotime($outDate[0]));
                 $out_time=$out_timep;
                 $out_time_new=$calOutDate.' '.$out_timep;
            }
            
            
        }
         return array(
            $in_time,
            $out_time,
            $out_time_new,
	    $shiftTo
        );  
    } 

    function getEmployeeShiftByDate($emp_code, $startDate, $endDate,$weeklyHolidayCombineDates = '') {
        //var_dump("stop script here"); die; 
        global $flag;
        global $flag1;
        global $lates;
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;

        $RowData = $hello = array();

        $sqlq00 = "select * from cattendanceqry where EMP_CODE='".$emp_code."' and  CAST(ATTDate AS DATE) between '".$startDate."' and '".$endDate."'";
    
            $resultq00 = query($query,$sqlq00,$pa,$opt,$ms_db);

            if($resultq00){
                $tempArray00 = $num($resultq00);
            }else{
                $tempArray00 = -1;
            }
            if($tempArray00 > 0) {
                $resultArray = array();
                $obj = new ProcessAttendance();
                while ($rowq00 = $fetch($resultq00)){
                    

            //check if current matched in any one from weeklyoff or holidays
            $isWeeklyHolidayDate = (in_array(trim($rowq00['ATTDATE']), $weeklyHolidayCombineDates)) ? 1 : 0 ;

            if($isWeeklyHolidayDate) {
                //chnage intime and out to punch time if dateis in weekly off or holiday
                $getActualPunchTimeQueryTop = "SELECT * FROM fnPunchTime('".$rowq00['EMP_CODE']."','".$rowq00['ATTDATE']."')"; 
                $getActualPunchTimeResultTop = query($query,$getActualPunchTimeQueryTop,$pa,$opt,$ms_db);
                if($num($getActualPunchTimeResultTop) > 0) {
                    $getActualPunchTimeRowTop   = $fetch($getActualPunchTimeResultTop);
                    $rowq00['OUT_TIME'] = (!empty($getActualPunchTimeRowTop["Outtime"])) ? $getActualPunchTimeRowTop["Outtime"]: $rowq00['OUT_TIME'];
                    $rowq00['IN_TIME'] = (!empty($getActualPunchTimeRowTop["Intime"])) ? $getActualPunchTimeRowTop["Intime"]: $rowq00['IN_TIME'];
                }
            }

            $rowq00['EMP_CODE'] = trim($rowq00['EMP_CODE']);
            $otTime = '';
            //check and get employee OT time
            $otTime =$this->checkEmployeeOTRequest($rowq00['EMP_CODE'], $rowq00['ATTDATE']);

            $finalOuttime    = '';
            $isOt = false;
            $finalSWEnd_Time = $rowq00['ShiftEnd_SwTime'];

            if(empty($otTime)){
                $finalSWEnd_TimeArray = (!empty($finalSWEnd_Time)) ? explode(':', $finalSWEnd_Time) : '';
                $finalSWEnd_Time = (!empty($finalSWEnd_TimeArray)) ? date('M d Y H:i',strtotime('+'.$finalSWEnd_TimeArray[0].' hour +'.$finalSWEnd_TimeArray[1].' minutes',strtotime($rowq00['Shiftto']))) : '';
            }
 
            /*Check OT request and add into shiftEndtime+SwEnd_TIme*/
            if(!empty($otTime)) {
                $isOt = true;
                //get actual punch from next date
                $getActualPunchTimeQuery = "SELECT * FROM fnPunchTime('".$rowq00['EMP_CODE']."','".$rowq00['ATTDATE']."')"; 
                $getActualPunchTimeResult = query($query,$getActualPunchTimeQuery,$pa,$opt,$ms_db);
                if($num($getActualPunchTimeResult) > 0){
                    $getActualPunchTimeRow  = $fetch($getActualPunchTimeResult);
                    $rowq00['OUT_TIME'] = (!empty($getActualPunchTimeRow["Outtime"])) ? $getActualPunchTimeRow["Outtime"]: $tempOutTime;
                }

                $otTimeArray = explode(":",$otTime);
                $finalOuttimeWithOT = date('M d Y H:i',strtotime('+'.$otTimeArray[0].' hour +'.$otTimeArray[1].' minutes',strtotime($rowq00['Shiftto'])));

                $ShiftEnd_SwTimeArray = (!empty($rowq00['ShiftEnd_SwTime'])) ? explode(":",$rowq00['ShiftEnd_SwTime']) : '';

                /*if(count($ShiftEnd_SwTimeArray) > 0){
                    $finalSWEnd_Time = date('M d Y H:i',strtotime('+'.$ShiftEnd_SwTimeArray[0].' hour +'.$ShiftEnd_SwTimeArray[1].' minutes',strtotime($finalOuttimeWithOT)));
                }*/

            }else{
                if(!$isWeeklyHolidayDate){
                    $getActualInTimeQuery = "SELECT TOP 1 * FROM
                                        ( SELECT 
                                        CAST( CAST(CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [START_SHIFT_DATETIME],
                                        CAST( CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(SHIFT_TO AS DATETIME) AS DATETIME) + CAST(ShiftEnd_SwTime AS DATETIME) AS DATETIME) AS [END_SHIFT_DATETIME],
                                        AttendanceAll.PunchDate AS InTime, AttendanceAll.EMP_CODE AS ATT_EMP_CODE
                                        FROM RosterQry,AttendanceAll WHERE RosterQry.EMP_CODE = '".$rowq00['EMP_CODE']."' AND '".$rowq00['ATTDATE']."' BETWEEN RosterQry.RosterStart AND RosterQry.RosterEnd ) rosterView
                                        WHERE (InTime BETWEEN START_SHIFT_DATETIME AND END_SHIFT_DATETIME) AND ATT_EMP_CODE = '".$rowq00['EMP_CODE']."' ORDER BY InTime ASC";
                    $getActualInTimeResult = query($query,$getActualInTimeQuery,$pa,$opt,$ms_db);
                    if($num($getActualInTimeResult) > 0) {
                        $getActualInTimeRow     = $fetch($getActualInTimeResult);
                        $rowq00['IN_TIME'] = $getActualInTimeRow['InTime'];
                    }
                    /*store out time in variable and will use when OT will find*/
                    $tempOutTime = $rowq00['Outtime'];

                    $getActualOutTimeQuery = "SELECT TOP 1 * FROM
                                            ( SELECT 
                                            CAST( CAST(CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(Shift_From AS DATETIME) AS DATETIME) AS DATETIME) - CAST(ShiftStart_SwTime AS DATETIME) AS DATETIME) AS [START_SHIFT_DATETIME],
                                            CAST( CAST(CAST('".$rowq00['ATTDATE']."' AS DATETIME)+ CAST(SHIFT_TO AS DATETIME) AS DATETIME) + CAST(ShiftEnd_SwTime AS DATETIME) AS DATETIME) AS [END_SHIFT_DATETIME],
                                            AttendanceAll.PunchDate AS OutTime, AttendanceAll.EMP_CODE AS ATT_EMP_CODE
                                            FROM RosterQry,AttendanceAll WHERE RosterQry.EMP_CODE = '".$rowq00['EMP_CODE']."' AND '".$rowq00['ATTDATE']."' BETWEEN RosterQry.RosterStart AND RosterQry.RosterEnd ) rosterView
                                            WHERE (OutTime BETWEEN START_SHIFT_DATETIME AND END_SHIFT_DATETIME) AND ATT_EMP_CODE = '".$rowq00['EMP_CODE']."' ORDER BY OutTime DESC";
                    $getActualOutTimeResult = query($query,$getActualOutTimeQuery,$pa,$opt,$ms_db);
                    if($num($getActualOutTimeResult) > 0) {
                        $getActualOutTimeRow    = $fetch($getActualOutTimeResult);
                        $rowq00['OUT_TIME'] = ( strtotime($rowq00['IN_TIME']) == strtotime($getActualOutTimeRow['OutTime']))  ? '' : $getActualOutTimeRow['OutTime'];
                    }
                }
            }


            /*check and replace in punch time if swap time is less*/
            /*if(!empty($rowq00['IN_TIME'])){
                $tempInTinme = date('M d Y H:i', strtotime($rowq00['IN_TIME']));
                $ShiftStart_SwTimeArray = (!empty($rowq00['ShiftStart_SwTime'])) ? explode(":",$rowq00['ShiftStart_SwTime']) : '';
                $finalSWStart_Time = date('M d Y H:i',strtotime('-'.$ShiftStart_SwTimeArray[0].' hour +'.$ShiftStart_SwTimeArray[1].' minutes',strtotime($rowq00['ShiftFrom'])));
                if(!empty($rowq00['IN_TIME']) && strtotime($tempInTinme) < strtotime($finalSWStart_Time) ){
                    $rowq00['IN_TIME'] = '';
                }
            }*/

            /*check and replace out punch time if swap time is less*/
            if($isOt){
                $tempOutTinme = date('M d Y H:i', strtotime($rowq00['OUT_TIME']));
                if(!empty($rowq00['OUT_TIME']) && strtotime($tempOutTinme) > strtotime($finalSWEnd_Time) ){
                    $rowq00['OUT_TIME'] = '';
                }
            }
            
            $obj->initializeObject( array(
                    'empCode'           =>$rowq00['EMP_CODE'],
                    'attDate'           =>$rowq00['ATTDATE'],
                    'inTime'            =>$rowq00['IN_TIME'],
                    'outTime'           =>$rowq00['OUT_TIME'],
                    'shiftName'         =>$rowq00['Shift_Name'],
                    'actual_shiftFrom'  =>$rowq00['ShiftFrom'],
                    'actual_shiftTo'    =>$rowq00['Shiftto'],
                    'shiftFrom'         =>$rowq00['ShiftFrom'],
                    'shiftTo'           =>(!empty($finalSWEnd_Time) && !empty($otTime)) ? $finalSWEnd_Time : $rowq00['Shiftto'],
                    'shiftMFrom'        =>$rowq00['ShiftMFrom'],
                    'shiftMTo'          =>(!empty($finalSWEnd_Time) && !empty($otTime)) ? $finalSWEnd_Time : $rowq00['ShiftMTo'],
                    'ShiftStartSwTime'  =>$rowq00['ShiftStart_SwTime'],
                    'ShiftEndSwTime'    =>$rowq00['ShiftEnd_SwTime'],
                    'lateAllow'         =>(!empty($rowq00['LateAllow'])) ? $rowq00['LateAllow']: 0,
                    'lateAllowCycle'    =>(!empty($rowq00['LateAllowCycle'])) ? $rowq00['LateAllowCycle']: 0,
                    'lateAllowGPrd'     =>(!empty($rowq00['LateAllowGPrd'])) ? $rowq00['LateAllowGPrd']: 0,
                    'erlyAllow'         =>(!empty($rowq00['ErlyAllow'])) ? $rowq00['ErlyAllow'] : 0,
                    'erlyAllowCycle'    =>(!empty($rowq00['ErlyAllowCycle'])) ? $rowq00['ErlyAllowCycle'] : 0,
                    'erlyAllowGPrd'     =>(!empty($rowq00['ErlyAllowGPrd'])) ? $rowq00['ErlyAllowGPrd'] : 0,
                    'mHrsFul'           =>$this->getMinutes($rowq00['MHrsFul'],'00:00:00'),
                    'lates'             =>$rowq00['lates'],
                    'mHrsHalf'          =>$this->getMinutes($rowq00['MHrsHalf'],'00:00:00'),
                    "BusArrival"        =>(!empty($rowq00['Buslate'])) ? $rowq00['Buslate'] : 0,
                    "isWeeklyHolidayDate" => $isWeeklyHolidayDate,
                ) );
            $resultArray[] = $obj->getResult();
        }

        }
    
        /*  echo "<pre>"; print_r($inputArray);
        echo "---------------------";
        prd($resultArray);*/
        return $resultArray;
    }   


    function checkEmployeeOTRequest($empCode, $date){
        global $flag;
        global $flag1;
        global $lates;
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        $date = date('Y-m-d',strtotime($date));
        
        $selectQuery = "SELECT overTimeHours,OT_Approver,appStatus FROM overtime_Balances WHERE Emp_Code = '".$empCode."' AND CAST(OT_Date AS DATE) = '".$date."'";
        $resultQuery = query($query,$selectQuery,$pa,$opt,$ms_db);
        if($num($resultQuery) > 0){
            $resultArray    = $fetch($resultQuery);
            
            $approverArray  = (!empty($resultArray['OT_Approver'])) ? explode(',', $resultArray['OT_Approver']) : '';
            $approverStatusArray = (!empty($resultArray['appStatus'])) ? explode(',', $resultArray['appStatus']) : '';
            if( end($approverStatusArray) == '2' && count($approverArray) == count($approverStatusArray) ){
                return $resultArray['overTimeHours'];
            }
        }
        return '';

    }


/*--------End OT Finalization List Function----------*/

    function getEmployeeNameByID($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select Emp_Code,Emp_Name from EmpNameQry where EMP_CODE ='$empCode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        while ($row=$fetch($result)) {
            $Emp_Code=$row['Emp_Code'];
            $Emp_Name=$row['Emp_Name'];
            
        }

        return array($Emp_Code,$Emp_Name,$i);
    }

}

?>