<?php
//include ('../../configdata.php');
include ('../languages/overTimeMail/oTMail.php');


class OTRequest_class{
    function __construct(){
      
    }


/*--------Start OT Request Function------------*/


    function getFinYear(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select FY,FY_Type,convert(varchar(10),FY_Start,103)as FY_Start from FinYear where FY_Type='F' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $FinalYear=$row['FY'];
            $FY_Type=$row['FY_Type'];
            $FY_Start=$row['FY_Start'];
            $i++;
        }
        return array($FinalYear,$FY_Type,$FY_Start,$i);
    }

    function getForm(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc,Field_Len,DefaultValue,Nullable,MinLength from TablesFields where Table_Name='overTime' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_Desc[$i]=$row['Field_Desc'];
            $Field_Len[$i]=$row['Field_Len'];
            $DefaultValue[$i]=$row['DefaultValue'];
            $Nullable[$i]=$row['Nullable'];
            $MinLength[$i]=$row['MinLength'];
            $i++;
        }

        return array($Field_Desc,$Field_Len,$DefaultValue,$Nullable,$MinLength,$i);
    }

    function getTable(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Field_Desc,Field_Len,DefaultValue,Nullable,MinLength from TablesFields where Table_Name='OverTime_Table' and Field_Active='Y' order by Field_No";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Field_TDesc[$i]=$row['Field_Desc'];
            $Field_TLen[$i]=$row['Field_Len'];
            $DefaultTValue[$i]=$row['DefaultValue'];
            $TNullable[$i]=$row['Nullable'];
            $TMinLength[$i]=$row['MinLength'];
            $i++;
        }

        return array($Field_TDesc,$Field_TLen,$DefaultTValue,$TNullable,$TMinLength,$i);
    }


    function getCategory(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTCategory' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);   
    }


    function getShiftStartTime($shftcode){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="SELECT convert(varchar(10),Shift_From,108) as Shift_From FROM SHIFTMAST WHERE Shift_Code='$shftcode'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        while ($row=$fetch($result)) {
            $Shift_From=$row['Shift_From'];
        }

        return array($Shift_From);
    }


    function getShift(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="SELECT Shift_Code+'- ('+Shift_Name+')' as shift,Shift_Code FROM SHIFTMAST";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$shift[$i]=$row['shift'];
        	$Shift_Code[$i]=$row['Shift_Code'];
        	$i++;
        }

        return array($shift,$Shift_Code,$i);
    }


    function getMyTeam($mngrcode){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="Select Emp_Code, Emp_Name+' - ('+EMP_CODE+')' as Emp_Name FROM HrdMastQry where MNGR_CODE='10545' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$Emp_Code[$i]=$row['Emp_Code'];
        	$Emp_Name[$i]=$row['Emp_Name'];
        	$i++;
        }

        return array($Emp_Code,$Emp_Name,$i);
    }


    function getPurpose(){

    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTPurpose' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);
    }


    function getNewReqOpt(){
    	
    	global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select LOV_VAlue,LOV_Text from LOVMast where LOV_Field='OTNewReqQues' ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
        	$LOV_Text[$i]=$row['LOV_Text'];
        	$LOV_VAlue[$i]=$row['LOV_VAlue'];
        	$i++;
        }

        return array($LOV_Text,$LOV_VAlue,$i);
    }

    function getEmployeeShiftByDate($empCode, $date){
        $date = trim($date);
        if(empty($empCode) || empty($date)) return '';

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];


        $SqlQuery =  "SELECT rost.EMP_CODE, 
        convert(varchar(10),RosterStart,103) as RosterStart,
        convert(varchar(10),RosterEnd,103) as RosterEnd,
        convert(char(5), rost.shift_from,108) AS shift_from, 
        convert(char(5),rost.SHIFT_TO) AS SHIFT_TO, rost.shift_name, 
        convert(char(5),c.IN_TIME,108) AS IN_TIME, 
        convert(char(5),c.OUT_TIME,108) AS OUT_TIME
        FROM rosterQry rost
        LEFT JOIN Attendanceqry C ON C.ATTDATE='$convertDate' AND c.emp_code=rost.emp_code  
        WHERE '$convertDate' BETWEEN RosterStart AND RosterEnd AND rost.EMP_CODE in ($empCode)
        ";


        $result=query($query, $SqlQuery, $pa, $opt, $ms_db);
           $i=0;
        while ($row = $fetch($result)) {

            $Shift_From[$i]=$row['shift_from'];
            $Shift_To[$i]=$row['SHIFT_TO'];
            $Shift_Name[$i]=$row['shift_name'];
            $in_time[$i]=$row['IN_TIME'];
            $out_time[$i]=$row['OUT_TIME'];

            $i++;
        }

        return array(
            $Shift_From,
            $Shift_To,
            $Shift_Name,
            $in_time,
            $out_time,
            $i
        );
    }

    function getEmployeeName($empCode){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

         $sql="select Emp_Code,Emp_Name from EmpNameQry where EMP_CODE in($empCode)";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $Emp_Code[$i]=$row['Emp_Code'];
            $Emp_Name[$i]=$row['Emp_Name'];
            $i++;
        }

        return array($Emp_Code,$Emp_Name,$i);
    }

    function overTimeGlobal(){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $sql="select Id,overTimeName,overTimeHours from overtime_global ";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        $i=0;
        while ($row=$fetch($result)) {
            $ot_id[$i]=$row['Id'];
            $ot_name[$i]=$row['overTimeName'];
            $ot_hours[$i]=$row['overTimeHours'];
            $i++;
        }

        return array($ot_id,$ot_name,$ot_hours,$i);
    }

    function checkOTRequest($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        $sql="select Emp_Code,overTimehours,OT_Date from overtime_Balances where Emp_Code in ($empCode) AND OT_Date='$convertDate'";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        $i=0;
        if($numRow >= 1){
            
            while ($row=$fetch($result)) {
                $reqHours[$i]=$row['overTimehours'];
                $i++;
            }
            return array($reqHours,$i);
        }
    }

    /*----------Insertion Process-----------*/

    function checkOTRequestById($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

         $sql="select Emp_Code,OT_Date from overtime_Balances where Emp_Code ='$empCode' AND OT_Date='$convertDate'";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
       
        if($numRow >= 1){
            
            return $empCode;
        }else{
            return 0;
        }
    }

    function insertOTRequestById($empCode,$otdate,$category,$shift,$startTime,$endTime,$totalHour,$purpose,$newFunReq,$other,$approver,$OT_Key,$ast,$mngrCode){
          
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($otdate);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        //$appsta=implode(",", $appStatus);
        //echo $ast; die;

          $sql="insert into overtime_Balances (Emp_Code,overTimeHours,OT_Date,OT_StartTime,OT_EndTime,OT_NewFunction,OT_Other,OT_purpose,OT_Approver,OT_Key,OT_Shift,OT_Category,appStatus,CreatedBy) values('$empCode','$totalHour','$convertDate','$startTime','$endTime','$newFunReq','$other','$purpose','$approver','$OT_Key','$shift','$category','$ast','$mngrCode')";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        if ($result) {
            return 1;
        }      

    }


    function monthelyOTRequestById($empCode,$otdate,$totalHour,$finYear){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        global $newHourOt;
        global $quat;
         $FirstQ=array();
         $SecQ=array();
         $ThirdQ=array();
         $FourthQ=array();
        
        $date = trim($otdate);
        
        $year = trim($finYear);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];//yy/mm/dd
       
        $explodeFin = explode('/', $year);
       
        $convertYear = $explodeFin[2];

        // Start getQuarters
           
        $quarter=$this->getQuarters($year);
        
        $FirstQ=$quarter['1Q']; //print_r($quarter); 
        $SecQ=$quarter['2Q'];
        $ThirdQ=$quarter['3Q'];
        $FourthQ=$quarter['4Q'];

        $qm=ltrim($explodeDate[0],'0');

        for($i=0;$i<3;$i++){ 
            if($FirstQ[$i] == $qm){  
                $quat=1;
                break;
            }
        }

        for($sc=0; $sc<3; $sc++){
            if($SecQ[$sc] == $qm){ 
                $quat=2;
                break;
            }
            
        }

        for($tr=0;$tr<3; $tr++){ 
            if($ThirdQ[$tr] == $qm){ 
                $quat=3;
                break;
            }
        }

        for($fr=0;$fr<3;$fr++){ 
            if($FourthQ[$fr] == $qm){  
                $quat=4;
                break;
            }
        }

        

        //End getQuarters

         $sql="select hourOT from monthly_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
         $result=query($query,$sql,$pa,$opt,$ms_db);
          $numRow=$num($result);
         $row=$fetch($result);
         $newHourOt=$row['hourOT'] + $totalHour;
        
         if($numRow >= 1){
             $sql="update monthly_OT set hourOT='$newHourOt' where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
            $result=query($query,$sql,$pa,$opt,$ms_db);
            if($result){
                return true;
            }
         }else{

               $payd=$this->getPayCycleByEmp($empCode);
               if($explodeDate[1] >= $payd ){
                    if($explodeDate[0] == 1){
                        $mon=$explodeDate[0];
                        $smon=12;
                    }else if($explodeDate[0] == 12){
                        $smon=12;
                        $mon=1;
                    }else{
                        $smon=$explodeDate[0];
                        $mon=$explodeDate[0]+1;
                    }
                    $startDate=$convertYear.'-'.$smon.'-'.$payd;
                    $endDate = $convertYear.'-'.$mon.'-'.($payd-1);
               }
                else if($explodeDate[1] < $payd){
                    if($explodeDate[0] == 1){
                        $mon=$explodeDate[0];
                        $smon=12;
                    }else{
                        $smon=$mon-1;
                        $mon=$explodeDate[0];
                    }
                    $mon=$explodeDate[0];
                    $startDate=$convertYear.'-'.$smon.'-'.$payd;
                    $endDate = $convertYear.'-'.$mon.'-'.($payd-1);
                }

              $sql="insert into monthly_OT 
              (yearOT,quartelyOT,monthlyOT,hourOT,Emp_Code,startDate,endDate) values
              ('$explodeFin[2]','$quat','$mon','$totalHour','$empCode','$startDate','$endDate')";
                
              $result=query($query,$sql,$pa,$opt,$ms_db); 
              if($result){
                return 2;
              }
         }
    }

   function quaterlyOTRequestById($empCode,$otdate,$totalHour,$finYear){
        //echo $empCode.'-'.$otdate.'-'.$totalHour.'-'.$finYear; die;
       global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        global $newHourOt;
        global $quat;

         $FirstQ=array();
         $SecQ=array();
         $ThirdQ=array();
         $FourthQ=array();
        
        $date = trim($otdate);
        
        $year = trim($finYear);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];//yy/mm/dd
       
        $explodeFin = explode('/', $year);
       
        $convertYear = $explodeFin[2];

        // Start getQuarters
           
        $quarter=$this->getQuarters($year);
        
        $FirstQ=$quarter['1Q']; //print_r($quarter); 
        $SecQ=$quarter['2Q'];
        $ThirdQ=$quarter['3Q'];
        $FourthQ=$quarter['4Q'];

        $qm=ltrim($explodeDate[0],'0');

        for($i=0;$i<3;$i++){ 
            if($FirstQ[$i] == $qm){  
                $quat=1;
                break;
            }
        }

        for($sc=0; $sc<3; $sc++){
            if($SecQ[$sc] == $qm){ 
                $quat=2;
                break;
            }
            
        }

        for($tr=0;$tr<3; $tr++){ 
            if($ThirdQ[$tr] == $qm){ 
                $quat=3;
                break;
            }
        }

        for($fr=0;$fr<3;$fr++){ 
            if($FourthQ[$fr] == $qm){  
                $quat=4;
                break;
            }
        }

        

        //End getQuarters

         $sql="select hourOT from quartely_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
         $result=query($query,$sql,$pa,$opt,$ms_db);
         $numRow=$num($result);
         $row=$fetch($result);
         $newHourOt=$row['hourOT'] + $totalHour;
        
         if($numRow >= 1){
             $sql="update quartely_OT set hourOT='$newHourOt' where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
            $result=query($query,$sql,$pa,$opt,$ms_db);
            if($result){
                return 3;
            }
         }else{

               $payd=$this->getPayCycleByEmp($empCode);
              
                        if($quat == 1){
                            if($FirstQ[0] == 1){
                                $smon=12;
                                 $mon=$FirstQ[2];
                            }else{
                                $smon=$FirstQ[0]-1;
                                $mon=$FirstQ[2];
                            }
                           
                        }if($quat == 2){
                             if($SecQ[0] == 1){
                                $smon=12;
                                 $mon=$SecQ[2];
                            }else{
                             $smon=$SecQ[0]-1;
                            $mon=$SecQ[2];
                            }
                        }
                        if($quat == 3){
                            if($ThirdQ[0] == 1){
                                $smon=12;
                                $mon=$ThirdQ[2];
                            }else{
                             $smon=$ThirdQ[0]-1;
                             $mon=$ThirdQ[2];
                            }
                        }
                        if($quat == 4){
                             if($FourthQ[0] == 1){
                                $smon=12;
                                $mon=$FourthQ[2];
                            }else{
                           $smon=$FourthQ[0]-1;
                            $mon=$FourthQ[2];
                            }
                        }
                    
                        // $smon=$explodeDate[0];
                        // $mon=$explodeDate[0]+1;
                    
                    $startDate=$convertYear.'-'.$smon.'-'.$payd;
                    $endDate = $convertYear.'-'.$mon.'-'.($payd-1);
              
                
              $sql="insert into quartely_OT 
              (yearOT,quartelyOT,hourOT,Emp_Code,startDate,endDate) values
              ('$explodeFin[2]','$quat','$totalHour','$empCode','$startDate','$endDate')";
                
              $result=query($query,$sql,$pa,$opt,$ms_db); 
              if($result){
                return 3;
              }
         }
    }

    function yearlyOTRequestById($empCode,$otdate,$totalHour,$finYear){

       //echo $empCode.'-'.$otdate.'-'.$totalHour.'-'.$finYear; die;
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;
        global $newHourOt;
        global $quat;

         $FirstQ=array();
         $SecQ=array();
         $ThirdQ=array();
         $FourthQ=array();
        
        $year = trim($finYear);

        $explodeFin = explode('/', $year);//dd/mm/yy
       
        $startYear= $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];//yy/mm//dd
        $lastDate= date("t", strtotime($startYear));
        $endYear = ($explodeFin[2]+1).'-'.($explodeFin[1]-1).'-'.$lastDate;

       
         $sql="select hourOT from yearly_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
         $result=query($query,$sql,$pa,$opt,$ms_db);
         $numRow=$num($result);
         $row=$fetch($result);
         $newHourOt=$row['hourOT'] + $totalHour;
        
         if($numRow >= 1){
             $sql="update yearly_OT set hourOT='$newHourOt' where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";
            $result=query($query,$sql,$pa,$opt,$ms_db);
            if($result){
                return 4;
            }
         }else{

              $sql="insert into yearly_OT 
              (yearOT,hourOT,Emp_Code,startDate,endDate) values
              ('$explodeFin[2]','$totalHour','$empCode','$startYear','$endYear')";
                
              $result=query($query,$sql,$pa,$opt,$ms_db); 
              if($result){
                return 4;
              }
         }
    }

   /*--------------End Insertion Process---------------------*/

    function checkMonthlyLimit($empCode,$date){
        //echo $empCode; echo $date;
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        //$curMonth = date($explodeDate[0], time());
        //$curQuarter = ceil($curMonth/3);

        //$quartely=$explodeDate[0]/3;
          $sql="select hourOT from monthly_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
           
            while ($row=$fetch($result)) {
                $reqHours=$row['hourOT'];
                
            }
            return $reqHours;
         }
    }

    function checkYearlyLimit($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        
        $sql="select hourOT from monthly_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
           
            while ($row=$fetch($result)) {
                $reqHours=$row['hourOT'];
                
            }
            return $reqHours;
         }
    }

    function getQuarters($finnYear){//dd/mm//yy
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        
        $date = trim($finnYear); 
        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[0].'/'.$explodeDate[1].'/'.$explodeDate[2];//dd/mm/yy


        for($i=0; $i<3; $i++){

            $FQuart[$i]= ($explodeDate[1]) + ($i);
            //geting months here in one quarter [which month is  coming in one quater
        }

        //print_r($FQuart);
        if($FQuart[2] == 12){
            $SecTemp=1;
        }else{
            $SecTemp=$FQuart[2] + 1;
        }
        
        for($i=0; $i< 3; $i++){

            $SecQuart[$i]= ($SecTemp) + ($i);
        }

          //print_r($SecQuart);
        if($SecQuart[2] == 12){ 
            $thirdTemp=1;
        }else{
            $thirdTemp= $SecQuart[2] + 1;
        }
        
        for($i=0; $i< 3; $i++){

            $thirdQuart[$i]= ($thirdTemp) + ($i);
        }

       // print_r($thirdQuart);

        if($thirdQuart[2] == 12){ 
            $fourthTemp=1;
        }else{
             $fourthTemp= $thirdQuart[2] + 1;
        }

        for($i=0; $i< 3; $i++){

            $fourthQuart[$i]= ($fourthTemp) + ($i);
        }
        //print_r($fourthQuart);

        return array("1Q"=>$FQuart, "2Q"=>$SecQuart, "3Q"=>$thirdQuart, "4Q"=>$fourthQuart);
    }
   
    function checkQuarterlyLimit($empCode,$date){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $date = trim($date);

        $explodeDate = explode('/', $date);
        $convertDate = $explodeDate[2].'-'.$explodeDate[0].'-'.$explodeDate[1];

        $sql="select hourOT from quartely_OT where Emp_Code ='$empCode' AND convert(varchar(10),CAST('$convertDate' as date)) BETWEEN convert(varchar(10),CAST(startDate as date)) AND convert(varchar(10),CAST(endDate AS date))";


        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
            
            while ($row=$fetch($result)) {
                $reqHours=$row['hourOT'];
                
            }
            return $reqHours;
         }
    }

    function getPayCycleByEmp($empCode){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


         $sql="select PY_Start from paycycle where py_OrgMastdt in (select grd_code from HrdMastQry where emp_code = '$empCode' )";

        $result=query($query,$sql,$pa,$opt,$ms_db);
        $numRow=$num($result);
        if($numRow >= 1){
           
            while ($row=$fetch($result)) {
                $payStart=$row['PY_Start'];
               
            }
            return $payStart;
         }
    }


    /*function getHolidays($month,$year){
        global $query;
        global $pa;
        global $opt;
        global $ms_db;
        global $num;
        global $fetch;
        $daysInMonth = cal_days_in_month(0, $month, $year);
        $firstDate= $year.'-'.$month.'-01';
        $lastDate = $year.'-'.$month.'-'.$daysInMonth;

        $result1= array();
        $sqlq="select cast(HDATE as date) HDATE,LOC_CODE,HCODE,HDESC,HolidayID,H_status  from holidays where cast(HDATE as date) BETWEEN '".$firstDate."' AND '".$lastDate."'";

        $resultq=query($query,$sqlq,$pa,$opt,$ms_db);
        
        if($resultq){
            $tempArray4=$num($resultq);
        }else{
            $tempArray4=-1;
        }
        if($tempArray4>0) {
            while ($rowq = $fetch($resultq)){
                $result1[] =array('title'=>$rowq['HDESC'],'start'=>$rowq['HDATE']);
            }
            return $result1;
        }
        

        //return json_encode($result1);
    }*/

    function sendAppMailById($approverId,$empCode,$otdate,$totalHour,$mngrCode){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

        $appId=explode(",", $approverId);


        $queMail= "Select Emp_Name,OEMailID from HrdMastQry where Emp_Code ='$appId[0]'";
        $resMail= query($query,$queMail,$pa,$opt,$ms_db);
        $appMail=$fetch($resMail);
        $Emp_Name=$appMail['Emp_Name'];

        $requesterSql="Select Emp_Name from HrdMastQry where Emp_Code ='$empCode'";
        $requesterRes = query($query,$requesterSql,$pa,$opt,$ms_db);
        $requesterRow= $fetch($requesterRes);
        $Emp_Name=$requesterRow['Emp_Name'];

        $to=trim($appMail['OEMailID']);
        $mes = rtrim(ucwords(strtolower($appMail[0])));
        

        $subject= "gfjguj";
        $message="Dear ".$mes.",<br><br>";
        $message.="You are requested to approve my ".$totalHour." Hours OverTime of ".$otdate." ";
        
        $message.="Regards <br>";
        $message.=$mngrCode[0]. "-";
        $mail1=mymailer('donotreply@sequelone.com',$subject,$message,$to);
        if($mail){
            echo 7;
        }
    }

    /*----------Advance Approver ID----------- (ihave to do according workflow)*/

    function checkAdvanceApprover($totalHour){
        $tot=explode(":", $totalHour);
        $globalVal=$this->overTimeGlobal();
        //print_r($globalVal);
        //echo $tot[0];
        $arrhours=$globalVal[2];
        for ($i=0; $i < count($arrhours); $i++) { 
            if($tot[0] > $arrhours[$i])
            {
                return 1; 
                break;
            }else{
                return 2; break;
            }
        }
    }

    function getAdvanceAppID($code){ //echo $code;

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;

       //$code=$_REQUEST['emp_code'];
       $sql="select * from WorkFlow WHERE WFFor='overTime' and WorkFlowID=23";
       $result=query($query,$sql,$pa,$opt,$ms_db);
    
        $row=$fetch($result);
        $dataval=$row['Approver'];

        $lev = explode(',',$dataval);
        //print_r($lev);
            for($i=0;$i<count($lev);$i++){
                if($lev[$i]=='Reporting Manager'){
                    $sqlr="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultr = query($query,$sqlr,$pa,$opt,$ms_db);
                    while($rowr = $fetch($resultr)){
                        $code1 = $rowr['MNGR_CODE'];
                        $sql1="Select * from hrdmastQry where Emp_Code='$code1'";
                        $result1 = query($query,$sql1,$pa,$opt,$ms_db);
                        while($row1 = $fetch($result1)){
                        
                            $approverID[]= $row1['Emp_Code']; 
                        }

                    }
                }

		

                if($lev[$i]=='Functional Manager'){
                    $sqlf="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultf = query($query,$sqlf,$pa,$opt,$ms_db);
                    while($rowf = $fetch($resultf)){
                        $code2 = $rowf['MNGR_CODE2'];
                        $sql2="Select * from hrdmastQry where Emp_Code='$code2'";
                        $result2 = query($query,$sql2,$pa,$opt,$ms_db);
                        while($row2 = $fetch($result2)){
                            
                            $approverID[]=$row2['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Function Head'){
                    $sqlfh="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultfh = query($query,$sqlfh,$pa,$opt,$ms_db);
                    while($rowfh = $fetch($resultfh)){
                        $code3 = $rowfh['FunctionHead_Code'];
                        $sql3="Select * from hrdmastQry where Emp_Code='$code3'";
                        $result3 = query($query,$sql3,$pa,$opt,$ms_db);
                        while($row3 = $fetch($result3)){
                            
                            $approverID[]= $row3['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Bussiness Unit Head'){
                    $sqlb="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultb = query($query,$sqlb,$pa,$opt,$ms_db);
                    while($rowb = $fetch($resultb)){
                        $code4 = $rowb['BussUnitHead_Code'];
                        $sql4="Select * from hrdmastQry where Emp_Code='$code4'";
                        $result4 = query($query,$sql4,$pa,$opt,$ms_db);
                        while($row4 = $fetch($result4)){
                            
                            $approverID[] = $row4['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Sub Functional Head'){
                    $sqlsf="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultsf = query($query,$sqlsf,$pa,$opt,$ms_db);
                    while($rowsf = $fetch($resultsf)){
                        $code5 = $rowsf['SubFunctionHead_Code'];
                        $sql5="Select * from hrdmastQry where Emp_Code='$code5'";
                        $result5 = query($query,$sql5,$pa,$opt,$ms_db);
                        while($row5 = $fetch($result5)){
                            
                            $approverID[]=$row5['Emp_Code'];
                        }

                    }
                }
                if($lev[$i]=='Sub Bussiness Head'){
                    $sqlsb="Select * from hrdmastQry where Emp_Code='$code'";
                    $resultsb = query($query,$sqlsb,$pa,$opt,$ms_db);
                    while($rowsb = $fetch($resultsb)){
                        $code6 = $rowsb['SubBussHead_Code'];
                        $sql6="Select * from hrdmastQry where Emp_Code='$code6'";
                        $result6 = query($query,$sql6,$pa,$opt,$ms_db);
                        while($row6 = $fetch($result6)){
                           
                            $approverI[] = $row6['Emp_Code'];
                        }

                    }
                }

            }
             $appID = implode(',',$approverID);

        return $appID;
    
        
    
    }

    /*function advanceApproverName($code){
            //$code=$_REQUEST['code']; 
            //return $code;
            $workflow = Array();
            $level= Array();
            $sqlq="select * from WorkFlow WHERE WFFor='overTime' and WorkFlowID=10009 ";
            $resultq = query($query,$sqlq,$pa,$opt,$ms_db);
            {
                while($row = $fetch($resultq)) {

                    $sql="Select * from hrdmastQry";

                    $datalevel = $row['ForMenu'];
                   // echo $_POST['data'];

                    $datalevel=json_decode($datalevel,true);

                    //print_r($datalevel);
                    //print_r($datalevel[0]['children'][$b]['id']);


                    $sql.=" where ";
                    for ($i=0;$i<count($datalevel);$i++){
                    if($datalevel[$i]['text']=="Company"){
                        for($b=0;$b<count($datalevel[$i]['children']);$b++) {
                            $sql .= " Comp_code='" . substr($datalevel[$i]['children'][$b]['id'],1)."'";
                            if($b<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql .= " and ";
                    }

                    if($datalevel[$i]['text']=="Business Unit"){
                        for($b=0;$b<count($datalevel[$i]['children']);$b++) {
                            $sql .= " Busscode='" . substr($datalevel[$i]['children'][$b]['id'],1)."'";
                            if($b<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql .= " and ";
                    }

                    if($datalevel[$i]['text']=="Location"){

                        for($l=0;$l<count($datalevel[$i]['children']);$l++) {
                            $sql .= " loc_Code='" . substr($datalevel[$i]['children'][$l]['id'],1)."'";
                            if($l<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Working Location"){

                        for($wl=0;$wl<count($datalevel[$i]['children']);$wl++) {
                            $sql .= " WLOC_code='" . substr($datalevel[$i]['children'][$wl]['id'],1)."'";
                            if($wl<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Function"){

                        for($f=0;$f<count($datalevel[$i]['children']);$f++) {
                            $sql .= " FUNCT_code='" . substr($datalevel[$i]['children'][$f]['id'],1)."'";
                            if($f<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Sub Function"){

                        for($sf=0;$sf<count($datalevel[$i]['children']);$sf++) {
                            $sql .= " SFUNCT_code='" . substr($datalevel[$i]['children'][$sf]['id'],1)."'";
                            if($sf<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }
                    if($datalevel[$i]['text']=="Grade"){

                        for($g=0;$g<count($datalevel[$i]['children']);$g++) {
                            $sql .= " GRD_code='" . substr($datalevel[$i]['children'][$g]['id'],1)."'";
                            if($g<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }
                    if($datalevel[$i]['text']=="Employee Type"){

                        for($et=0;$et<count($datalevel[$i]['children']);$et++) {
                            $sql .= " TYPE_code='" . substr($datalevel[$i]['children'][$et]['id'],1)."'";
                            if($et<count($datalevel[$i]['children'])-1){
                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    if($datalevel[$i]['text']=="Process"){

                        for($pr=0;$pr<count($datalevel[$i]['children']);$pr++) {
                            $sql .= " PROC_code='" . substr($datalevel[$i]['children'][$pr]['id'],1)."'";
                            if($pr<count($datalevel[$i]['children'])-1){

                                $sql.= " or ";
                            }
                        }
                        $sql.= " and ";
                    }

                    }
                    $sql.="Status_Code='01'";

                    echo $sql;
                    $result = query($query,$sql,$pa,$opt,$ms_db);
                    if ($num($result) > 0) {
                        $list=array();

                        while($row1 = $fetch($result)) {
                            $list[] = $row1['Emp_Code'];


                        }
                        if(in_array($code,$list)){
                            //echo $row['LevelNo']; echo ";";
                            //echo $row['NoDays']; echo ";";
                            return $row['Approver'];

                            exit;

                        }

                    }
                    else{
                        return 2 ;
                    }


                }
            }

    }*/




    /*----------End Advance Approver ID--------*/
/*--------End OT Request Function-----------*/


/*--------Start OT Approver Function----------*/

    function getOTKey(){

        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql1="select OT_Key,count(id) as totalEmp from overtime_Balances  group by OT_Key";
        $result1=query($query,$sql1,$pa,$opt,$ms_db);
        $i=0;
        while ($row1= $fetch($result1)) {
            $OT_Key[$i]=$row1['OT_Key'];
            $totalEmp[$i]=$row1['totalEmp'];
            $i++;
        }

        return array($OT_Key,$totalEmp,$i);

    }

    function getappliedOT_Category($catID){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select LOV_Text from LOVMast where LOV_Field='OTCategory' AND LOV_VAlue='$catID'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
       $row= $fetch($result);

        return $row['LOV_Text'];
    }
    
   function getappliedOT_Purpose($purId){
        global $HTTP_HOST;
        global $query;
        global $sql;
        global $pa;
        global $opt;
        global $ms_db;
        global $fetch;
        global $num;


        $sql="select * from LOVMast where LOV_Field='OTPurpose' AND LOV_Value='$purId'";
        $result=query($query,$sql,$pa,$opt,$ms_db);
        
        $row= $fetch($result);

        return $row['LOV_Text'];
    }

/*--------End OT Approver Function----------*/




}

?>