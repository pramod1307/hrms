<?php 

function getHolidays($startDate,$endDate,$empCode = ''){
	
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	$daysInMonth = cal_days_in_month(0, $month, $year);
	$firstDate= $year.'-'.$month.'-01';
	$lastDate = $year.'-'.$month.'-'.$daysInMonth;

	$where	=	"";
	//get employees data
	$getEmployeeDataSql = "SELECT LOC_CODE,COMP_CODE,BussCode AS BUS_CODE ,SUBBuss_CODE AS SUBBUS_CODE,WLOC_CODE,FUNCT_CODE AS FUNC_CODE,SFUNCT_CODE AS SUBFUNC_CODE,COST_CODE,PROC_CODE,GRD_CODE,DSG_CODE FROM HrdMastQry WHERE Emp_Code = '".$empCode."'";
	$getEmployeeDataSqlResult = query($query,$getEmployeeDataSql,$pa,$opt,$ms_db);
	if($num($getEmployeeDataSqlResult) > 0){
		$getEmployeeData = $fetch($getEmployeeDataSqlResult);
		//echo '<pre>'; print_r($getEmployeeData); echo '</pre>'; 
		/*
		$where .= ($getEmployeeData['LOC_CODE'] == '') ?  "" : " AND ( ','+LOC_CODE+',' LIKE '%,".$getEmployeeData['LOC_CODE'].",%' )";
		$where .= ($getEmployeeData['COMP_CODE'] == '') ?  "" : " AND ( ','+COMP_CODE+',' LIKE '%,".$getEmployeeData['COMP_CODE'].",%' )";
		$where .= ($getEmployeeData['BUS_CODE'] == '') ?  "" : " AND ( ','+BUS_CODE+',' LIKE '%,".$getEmployeeData['BUS_CODE'].",%' )";
		$where .= ($getEmployeeData['SUBBUS_CODE'] == '') ?  "" : " AND ( ','+SUBBUS_CODE+',' LIKE '%,".$getEmployeeData['SUBBUS_CODE'].",%' )";
		$where .= ($getEmployeeData['WLOC_CODE'] == '') ?  "" : " AND ( ','+WLOC_CODE+',' LIKE '%,".$getEmployeeData['WLOC_CODE'].",%' )";
		$where .= ($getEmployeeData['FUNC_CODE'] == '') ?  "" : " AND ( ','+FUNC_CODE+',' LIKE '%,".$getEmployeeData['FUNC_CODE'].",%' )";
		$where .= ($getEmployeeData['SUBFUNC_CODE'] == '') ?  "" : " AND ( ','+SUBFUNC_CODE+',' LIKE '%,".$getEmployeeData['SUBFUNC_CODE'].",%' )";
		$where .= ($getEmployeeData['COST_CODE'] == '') ?  "" : " AND ( ','+COST_CODE+',' LIKE '%,".$getEmployeeData['COST_CODE'].",%' )";
		$where .= ($getEmployeeData['PROC_CODE'] == '') ?  "" : " AND ( ','+PROC_CODE+',' LIKE '%,".$getEmployeeData['PROC_CODE'].",%' )";
		$where .= ($getEmployeeData['GRD_CODE'] == '') ?  "" : " AND ( ','+GRD_CODE+',' LIKE '%,".$getEmployeeData['GRD_CODE'].",%' )";
		$where .= ($getEmployeeData['DSG_CODE'] == '') ?  "" : " AND ( ','+DSG_CODE+',' LIKE '%,".$getEmployeeData['DSG_CODE'].",%' )";
	
		*/
	}
    //echo $where;
	$result1= array();
	//$sqlq="select CONVERT(char(10), HDATE,126) HDATE,LOC_CODE,HCODE,HDESC,HolidayID,H_status  from holidays where cast(HDATE as date) BETWEEN '".$startDate."' AND '".$endDate."'";

	/*   Query For Holidays which are applicable for all Employees without any restriction */
	
	$queryForAll	=	"SELECT HDATE,HDESC FROM Holidays WHERE ( HDATE BETWEEN '".$startDate."' AND '".$endDate."' ) AND H_status=1 
						AND (LOC_CODE is null or LOC_CODE = '') 
						AND (COMP_CODE is null or COMP_CODE = '')
						AND (BUS_CODE is null or BUS_CODE = '')
						AND (SUBBUS_CODE is null or SUBBUS_CODE = '')
						AND (WLOC_CODE is null or WLOC_CODE = '')
						AND (FUNC_CODE is null or FUNC_CODE = '')
						AND (SUBFUNC_CODE is null or SUBFUNC_CODE = '')
						AND (COST_CODE is null or COST_CODE = '')
						AND (PROC_CODE is null or PROC_CODE = '')
						AND (GRD_CODE is null or GRD_CODE = '')
						AND (DSG_CODE is null or DSG_CODE = '')";
						
	$resultq=query($query,$queryForAll,$pa,$opt,$ms_db);
	
	if($resultq){
		$tempArray4=$num($resultq);
	}else{
		$tempArray4=-1;
	}
	if($tempArray4>0) {
		while ($rowq = $fetch($resultq)){
			$result1[] =array('title'=>$rowq['HDESC'],'start'=>date('Y-m-d', strtotime($rowq['HDATE'])));
			$forAllHolidays		.=	"'".$rowq['HDATE']."',";
		}
		
	}	
	$forAllHolidays	=	rtrim($forAllHolidays, ",");
	
	if($forAllHolidays != ''){
			$wherefor	=	" AND HDATE NOT IN (".$forAllHolidays.")";
	
	}
	
	/*  END Process  */
	
	 $queryNew	=	"SELECT * FROM Holidays WHERE ( HDATE BETWEEN '".$startDate."' AND '".$endDate."' ) AND  H_status=1".$wherefor."";
	$resultq=query($query,$queryNew,$pa,$opt,$ms_db);
	
	if($resultq){
		$tempArray4=$num($resultq);
	}else{
		$tempArray4=-1;
	}
	
	if($tempArray4>0) {
		
		while($rowy = $fetch($resultq)){
					
					if(trim($rowy['LOC_CODE']) !=''){   $where .= (trim($getEmployeeData['LOC_CODE'] == '')) ?  "" : " AND ( ','+LOC_CODE+',' LIKE '%,".$getEmployeeData['LOC_CODE'].",%' )"; }
					if(trim($rowy['COMP_CODE']) !=''){ $where .= (trim($getEmployeeData['COMP_CODE'] == '')) ?  "" : " AND ( ','+COMP_CODE+',' LIKE '%,".$getEmployeeData['COMP_CODE'].",%' )"; }
					if(trim($rowy['BUS_CODE']) !=''){ $where .= (trim($getEmployeeData['BUS_CODE'] == '')) ?  "" : " AND ( ','+BUS_CODE+',' LIKE '%,".$getEmployeeData['BUS_CODE'].",%' )"; }
					if(trim($rowy['SUBBUS_CODE']) !=''){ $where .= (trim($getEmployeeData['SUBBUS_CODE'] == '')) ?  "" : " AND ( ','+SUBBUS_CODE+',' LIKE '%,".$getEmployeeData['SUBBUS_CODE'].",%' )"; }
					if(trim($rowy['WLOC_CODE']) !=''){ $where .= (trim($getEmployeeData['WLOC_CODE'] == '')) ?  "" : " AND ( ','+WLOC_CODE+',' LIKE '%,".$getEmployeeData['WLOC_CODE'].",%' )"; }
					if(trim($rowy['FUNC_CODE']) !=''){ $where .= (trim($getEmployeeData['FUNC_CODE'] == '')) ?  "" : " AND ( ','+FUNC_CODE+',' LIKE '%,".$getEmployeeData['FUNC_CODE'].",%' )"; } 
					if(trim($rowy['SUBFUNC_CODE']) !=''){ $where .= (trim($getEmployeeData['SUBFUNC_CODE'] == '')) ?  "" : " AND ( ','+SUBFUNC_CODE+',' LIKE '%,".$getEmployeeData['SUBFUNC_CODE'].",%' )"; }
					if(trim($rowy['COST_CODE']) !=''){ $where .= (trim($getEmployeeData['COST_CODE'] == '')) ?  "" : " AND ( ','+COST_CODE+',' LIKE '%,".$getEmployeeData['COST_CODE'].",%' )"; }
					if(trim($rowy['PROC_CODE']) !=''){ $where .= (trim($getEmployeeData['PROC_CODE'] == '')) ?  "" : " AND ( ','+PROC_CODE+',' LIKE '%,".$getEmployeeData['PROC_CODE'].",%' )"; }
					if(trim($rowy['GRD_CODE']) !=''){ $where .= (trim($getEmployeeData['GRD_CODE'] == '')) ?  "" : " AND ( ','+GRD_CODE+',' LIKE '%,".$getEmployeeData['GRD_CODE'].",%' )"; }
					if(trim($rowy['DSG_CODE']) !=''){ $where .= (trim($getEmployeeData['DSG_CODE'] == '')) ?  "" : " AND ( ','+DSG_CODE+',' LIKE '%,".$getEmployeeData['DSG_CODE'].",%' )"; }
			
				$sqlq = "SELECT HDATE,HDESC FROM Holidays WHERE HDATE='".$rowy['HDATE']."' AND H_status=1 ".$where." ";
				
				$resultp=query($query,$sqlq,$pa,$opt,$ms_db);
	
				if($resultq){
					$tempArray4=$num($resultp);
				}else{
					$tempArray4=-1;
				}
				
				if($tempArray4>0) {
					while ($rowq = $fetch($resultp)){
						$result1[] =array('title'=>$rowq['HDESC'],'start'=>date('Y-m-d', strtotime($rowq['HDATE'])));
					}
					
				}
				$where  = "";
		}
		
	}
	
//echo '<pre>'; print_r($result1); echo '</pre>'; die;
	//die;
	
return $result1;
	//return json_encode($result1);
}

/**Calculate employee gatepass movement hours**/
function getGatePassHours($empCode,$startDate, $endDate){
                global $query;
                global $pa;
                global $opt;
                global $ms_db;
                global $num;
                global $fetch;
                $result2 = array();
                $globalClassObj =             new global_class();
                $startDate = date('Y-m-d', strtotime($startDate));
                $endDate = date('Y-m-d', strtotime($endDate));
                $resultCount = 0;
                $sqlq2 ="SELECT * FROM gatePass WHERE Emp_Code = '".$empCode."' AND gatepass_date BETWEEN '".$startDate."' AND '".$endDate."'";
                $resultq2=query($query,$sqlq2,$pa,$opt,$ms_db);
                $countgateMovement = $num($resultq2);
                if($num($resultq2) >0) {
                                $resultCount = $countgateMovement;
                                $rowCount = 1;
                                if($num($resultq2) > 1){
                                                while ($rowq2 = $fetch($resultq2)){
                                                                $date = date('Y-m-d', strtotime($rowq2['gatepass_date']));
                                                                $totalCount[$date] = $totalCount[$date] + $rowq2['totalHours'];
                                                                if($rowq2['reason'] == 1){ $gatePassMessage[$date] = array( "message" => ($rowCount > 2) ? "Gate Movement Penalty" : "Gate Movement Warning", "isPenalty" => ($rowCount > 2) ? 1 : 0  ); }
                                                                $result2[] = array('start'=>$date,'time'=>$totalCount[$date],'reason'=>'');
                                                                $rowCount++;
                                                }                                              
                                } else {
                                                $rowq2 = $fetch($resultq2);
                                                $resonSubreasonArray = $globalClassObj->getEmployeeGatePassData($rowq2['reason'], $rowq2['subreason']);
                                                $date = date('Y-m-d', strtotime($rowq2['gatepass_date']));
                                                $totalCount[$date] = $totalCount[$date] + $rowq2['totalHours'];
                                                if($rowq2['reason'] == 1){ $gatePassMessage[$date] = ($rowCount > 2) ? "Gatepass Penalty" : "Gatepass Warning"; }
                                                $result2[] = array('start'=>$date,
                                                                'time'=>$rowq2['totalHours'],
                                                                'reason'=> implode(' - ', $resonSubreasonArray)
                                                                );
                                                $rowCount++;                   
                                }
                }
                return array("result" => $result2, "count" => $resultCount, "gatePassMessage" => $gatePassMessage);
}


function getBirthDates($month,$year){
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	$result2=array();
	$sqlq2 ="SELECT '".$year."'+'-'+RIGHT('00'+CONVERT(NVARCHAR(2),cast(DATEPART(MONTH, DOB) as VARCHAR(2))),2)+'-'+RIGHT('00'+CONVERT(NVARCHAR(2),cast(DATEPART(DAY, DOB) as VARCHAR(2))),2) as cDOB,
			EMP_NAME,OEMailID,isnull(DSG_NAME,'') DSG_NAME,isnull(WLOC_NAME,'') WLOC_NAME,isnull(LOC_NAME,'') LOC_NAME 
			FROM HrdMastQry where DOB != '' 
			AND RIGHT('00'+CONVERT(NVARCHAR(2),cast(DATEPART(MONTH, DOB) as VARCHAR(2))),2)='".$month."'  
			ORDER BY cDOB ASC";
	$resultq2=query($query,$sqlq2,$pa,$opt,$ms_db);
	if($resultq2){
		$tempArray5=$num($resultq2);
	}else{
		$tempArray5=-1;
	}
	if($tempArray5>0) {
		while ($rowq2 = $fetch($resultq2)){
			$result2[] =array('start'=>$rowq2['cDOB'],'bemp_record'=>array('bemp_name'=>$rowq2['EMP_NAME'],'bemp_email'=>$rowq2['OEMailID'],'bemp_dsg'=>$rowq2['DSG_NAME'],'bemp_wloc'=>$rowq2['WLOC_NAME'],'bemp_loc'=>$rowq2['LOC_NAME']));
		}

		//var_dump($result2);die;
		return $result2;
	}

}

function getAnniDates($month,$year){
	global $query,$pa,$opt,$ms_db,$num,$fetch;
	$result3=array();
	$sqlq3 ="SELECT '".$year."'+'-'+RIGHT('00'+CONVERT(NVARCHAR(2),cast(DATEPART(MONTH, Anniversary) as VARCHAR(2))),2)+'-'+RIGHT('00'+CONVERT(NVARCHAR(2),cast(DATEPART(DAY, Anniversary) as VARCHAR(2))),2) as cAnniversary,
			EMP_NAME,OEMailID,isnull(DSG_NAME,'') DSG_NAME,isnull(WLOC_NAME,'') WLOC_NAME,isnull(LOC_NAME,'') LOC_NAME 
			FROM HrdMastQry where Anniversary != '' 
			AND RIGHT('00'+CONVERT(NVARCHAR(2),cast(DATEPART(MONTH, Anniversary) as VARCHAR(2))),2)='".$month."'  
			ORDER BY cAnniversary ASC";
	$resultq3=query($query,$sqlq3,$pa,$opt,$ms_db);
	if($resultq3){
		$tempArray53=$num($resultq3);
	}else{
		$tempArray53=-1;
	}
	if($tempArray53>0) {
		while ($rowq3 = $fetch($resultq3)){
			$result3[] =array('start'=>$rowq3['cAnniversary'],'bemp_record'=>array('bemp_name'=>$rowq3['EMP_NAME'],'bemp_email'=>$rowq3['OEMailID'],'bemp_dsg'=>$rowq3['DSG_NAME'],'bemp_wloc'=>$rowq3['WLOC_NAME'],'bemp_loc'=>$rowq3['LOC_NAME']));
		}
		return $result3;
	}
	
}

?>