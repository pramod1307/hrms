<?php 
function getSundays($y,$m,$dayNo){ 
		    $date = "$y-$m-01";
		    $first_day = date('N',strtotime($date));
		    $first_day = $dayNo - $first_day + 1;
		    $last_day =  date('t',strtotime($date));

		    $days = array();
		    for($i=$first_day; $i<=$last_day; $i=$i+7 ){
		        if($i>0){
		        	$days[] = $i;
		    	}
		    }
		    return  $days;
		}

function getWeeklyOff($emp_code,$startDate,$endDate){
	global $query;
	global $pa;
	global $opt;
	global $ms_db;
	global $num;
	global $fetch;
	$daysInMonth = abs(floor(strtotime($startDate) / (60 * 60 * 24)) - floor(strtotime($endDate) / (60 * 60 * 24)));
	$firstDate= $startDate; //$year.'-'.$month.'-01';
	$lastDate = $endDate; //$year.'-'.$month.'-'.$daysInMonth;


	$startDay 	= date('d', strtotime($startDate));
	$month = date('m', strtotime($firstDate));
	$year = date('Y', strtotime($firstDate));	

	$endDay		= date('d', strtotime($endDate));
	$endMonth = date('m', strtotime($endDate));
	$endYear = date('Y', strtotime($endDate));

	$startDateEndDay = date('t',strtotime($firstDate));
	$endDateEndDay = date('t',strtotime($lastDate));
	
	
	if($startDay==1 && $month == $endMonth){
		$payCycle[] = array('from'=>$startDate,'to'=>$endDate);
	}else{
		$payCycle[]=array('from'=>$startDay.'-'.$month.'-'.$year,'to'=>$startDateEndDay.'-'.$month.'-'.$year);
		$payCycle[]=array('from'=>'01-'.$endMonth.'-'.$endYear,'to'=>$endDay.'-'.$endMonth.'-'.$endYear);
	}

	$result= array();
	/*$sqlq="SELECT ShiftPatternMastID,cast(rosterstart as date) attfrom,cast(rosterend as date) attto 
			FROM Rosterqry 
			WHERE EMP_CODE='".$emp_code."' 
			AND ((DATEPART(MM, rosterstart) <='".$month."'AND DATEPART(YY, rosterstart)='".$year."') 
			AND   (DATEPART(MM, rosterend ) >= '".$month."' AND DATEPART(YY, rosterstart)='".$year."'))
			ORDER BY rosterstart ASC";*/
	
$monthlyRecord = Array();
		$finalRecords = Array();
	foreach ($payCycle as $key => $value) {
		//pr($value); 
		$firstDate = $value['from'];
		$lastDate = $value['to'];
		$fdate = date('d', strtotime($firstDate));
		$fmonth = date('m', strtotime($firstDate));
		$fyear = date('Y', strtotime($firstDate));


		$ldate = date('d', strtotime($lastDate));
		$lmonth = date('m', strtotime($lastDate));
		$lyear = date('Y', strtotime($lastDate));

		//set default pattern to sunday
		$attto_timestamp = strtotime($lastDate);
		$ShiftPatternMastidSql = "SELECT ShiftPatternMastid FROM ShiftPatternMast WHERE ShiftPattern_Name = 'AutoShiftP' AND ShiftPattern_Code = '01'";

		$ShiftPatternMastidResult=query($query,$ShiftPatternMastidSql,$pa,$opt,$ms_db);
		$ShiftPatternMastData = $fetch($ShiftPatternMastidResult);
		$result[] =array('patternId'=>(!empty($ShiftPatternMastData['ShiftPatternMastid'])) ? $ShiftPatternMastData['ShiftPatternMastid'] : 0,'from'=>$fdate,'to'=>$ldate);

		/*echo $sqlq = "SELECT ShiftPatternMastID,cast(rosterstart as date) attfrom,cast(rosterend as date) attto 
          FROM Rosterqry 
          WHERE EMP_CODE='".$emp_code."' AND 
          DATEADD(month, DATEDIFF(month, 0, '".$firstDate."'), 0) 
          BETWEEN DATEADD(month, DATEDIFF(month, 0, rosterstart), 0) AND DATEADD(month, DATEDIFF(month, 0, rosterend), 0)";
*/

        $sqlq="SELECT ShiftPatternMastID,cast(rosterstart as date) attfrom,cast(rosterend as date) attto 
		FROM Rosterqry 
		WHERE EMP_CODE='".$emp_code."' 
		AND ((DATEPART(MM, rosterstart) <='".$month."'AND DATEPART(YY, rosterstart)='".$year."') 
		OR   (DATEPART(MM, rosterend ) >= '".$month."' AND DATEPART(YY, rosterstart)='".$year."'))
		ORDER BY rosterstart ASC";
		$resultq=query($query,$sqlq,$pa,$opt,$ms_db);
		if($resultq){
			$tempArray2 = $num($resultq);
		}else{
			$tempArray2=-1;
		}


		if($tempArray2>0) {
			$result= array();
			while ($rowq = $fetch($resultq)){
				$f = explode('-',$rowq['attfrom']);
				
				$t = explode('-',$rowq['attto']);
				$attto_timestamp = strtotime($rowq['attto']);

				$result[] =array('patternId'=>$rowq['ShiftPatternMastID'],'from'=>$fdate,'to'=>$ldate);
			}

		
		}else{
			$sqlq1="SELECT TOP 1 ShiftPatternMastID FROM AttRoster WHERE EMP_CODE='".$emp_code."' AND automatic_period='1'
				ORDER BY UPDATED_ON DESC";
			
			$resultq1=query($query,$sqlq1,$pa,$opt,$ms_db);
			
			if($resultq1){
				$tempArray3 = $num($resultq1);
			}else{
				$tempArray3=-1;
			}

			if($tempArray3>0) {
				while ($rowq1 = $fetch($resultq1)){
					$result[] =array('patternId'=>$rowq1['ShiftPatternMastID'],'from'=>$fdate,'to'=>$ldate);
				}
			}
		}

		$tempArray = array();
		$tempFromTo = array();

		for($i=0;$i<count($result); $i++){
			$tempArray[] = $result[$i]['patternId'];
			$tempFromTo[] =array($result[$i]['from'],$result[$i]['to']);
		}

		$ShiftPatternMastids = implode(', ', $tempArray);
		
		$weeklyOffArrayForMonth = Array();
		$sqlq2 = "SELECT WeeklyOff1, WeeklyOff2, WeeklyOff3, WeeklyOff4, WeeklyOff5 FROM ShiftPatternMast WHERE ShiftPatternMastid in (".$ShiftPatternMastids.")";
		$resultq2=query($query,$sqlq2,$pa,$opt,$ms_db);
		if($resultq2){
			$tempArray1 = $num($resultq2);
		}else{
			$tempArray1 =-1;
		}
		if($tempArray1>0) {
			while ($rowq2 = $fetch($resultq2)){
				//var_dump($rowq2);die;
				$givenArray[] = array($rowq2['WeeklyOff1'], $rowq2['WeeklyOff2'], $rowq2['WeeklyOff3'], $rowq2['WeeklyOff4'], $rowq2['WeeklyOff5']);
			}
		}

		//$monthlyRecord = Array();
		//$finalRecords = Array();
		for($j=1;$j<=7;$j++){
			$days = getSundays($fyear,$fmonth,$j);
			$monthlyRecord[$j] = $days;
		}

		for($p=0;$p<count($tempFromTo);$p++){
			$r = (int)$tempFromTo[$p][0];
			$k = 0;
			
			if($r<=$tempFromTo[$p][1]){
				while($k<count($givenArray[$p])){
					$givenDaysPerWeek = explode(',',$givenArray[$p][$k]);
					for($q=0;$q<count($givenDaysPerWeek);$q++){
						for($h=0;$h<count($monthlyRecord);$h++){
							try{
								@$d = $monthlyRecord[(int)$givenDaysPerWeek[$q]][$k];
								if($d >= $tempFromTo[$p][0] && $d<=$tempFromTo[$p][1]){
									$dateff1 = "{$fyear}-{$fmonth}-".sprintf("%02d", $d);
									//echo $attto_timestamp . ' and ' . strtotime($dateff1);
									//Check roster end date must be greater then to current date
									if($attto_timestamp > strtotime($dateff1))
										$finalRecords[] =array('start'=>$dateff1);
									goto end3;
								}
							}catch(Exception $e){
							}
							
						}
						end3:
					}$k++;
				}
			}
		}
	}

	return $finalRecords;
}
?>