//kajal
var validateTime=2;

function getFinancialYear(){
    var type="FinYear";
    //alert("1");
    $.ajax({
        type: "POST",
        url: "ajax/calculate_diff_time_ajax.php",
        data: {type:type},
        cache:false,
        success: function (result) {
          //alert(result);
          $("#finYear").val(result);

        }
    });
}


 $('input[type=radio][name=ot_type]').change(function() {
        if (this.value == '1') {
          
           $("#from").css('display','none');
           $("#pastdate").css('display','block');
           //location.reload();
          
        }
        else if (this.value == '2') {
           
           $("#pastdate").css('display','none');//datepicker({  maxDate: new Date() });
           $("#from").css('display','block');
           location.reload();
          
            
        }
    });



function getCatId(catid){
    $("#catId").val(catid);
     var type="AllSelectShift";
    if(catid == "1" || catid == "2"){ 
      //alert(catid);
      $.ajax({
          type:"POST",
          url: "ajax/calculate_diff_time_ajax.php",
            data: {type: type},
            dataType:"json",
            success: function (data) {
                //alert(data);
                var html1=''; 
                for(var key in data)
                {    
                   
                    html1+='<option value="'+key+'" selected>'+data[key]+'</option>';
                }
               
                $('#shift').multiselect('destroy');
                $('#shift').html(html1);
               

                    $('#shift').multiselect({
                        includeSelectAllOption: true,
                        enableFiltering: true
                    });

                    getSelectedFun();

              }

      });
    }else{
        $.ajax({
          type:"POST",
          url: "ajax/calculate_diff_time_ajax.php",
            data: {type: type},
            dataType:"json",
            success: function (data) {
                //alert(data);
                var html1=''; 
                for(var key in data)
                {    
                   
                    html1+='<option value="'+key+'" >'+data[key]+'</option>';
                }
               
                $('#shift').multiselect('destroy');
                $('#shift').html(html1);
               

                    $('#shift').multiselect({
                        includeSelectAllOption: true,
                        enableFiltering: true
                    });

              }

      });
    }

    $("#shift").val("NO");
    $("#from").val(" ");
    $("#pastdate").val(" ");
}


function getSelectedFun(){
    var selected = $("#shift option:selected");
    var message = "";
    selected.each(function () {
        message += $(this).val() + ",";
     });
      //alert(message);
    $("#shfid").val(message);
}



function getShiftId(){ 
  var selected = $("#shift option:selected");
    var message = "";
        selected.each(function () {
            message += $(this).val() + ",";
        });
       // alert(message);
    $("#shfid").val(message);
    $("#from").val(" ");
    $("#pastdate").val(" ");
}

var isStartFlag = false;

function time_calculate111()
{
  console.log("Start: "+ isStartFlag);
	if(isStartFlag == false) { isStartFlag = true; return true; }
  console.log("End: "+ isStartFlag);
  
	var time1 = ($("#Time1").val()); 
  var time2 =($("#Time2").val()) ;
  var ottype=$('input[name=ot_type]:checked').val();//alert(ottype);
  var curdate=now();
  var cd=curdate.split('/');
  var data=cd[1]+'/'+cd[0]+'/'+cd[2];
  if(ottype == "1"){
    var date=$("#pastdate").val();
  }else if(ottype == "2"){
    date=$("#from").val();
  }
  //alert(data);
  var type ="calculation";

   $.ajax({
            type: "POST",
            url: "ajax/calculate_diff_time_ajax.php",
          data:{type:type,time1:time1,time2:time2,date:date},
            success: function(result){
            //alert(result);
              $("#totalHour").val(result);    
            }
    });
$('.checkboxes').prop('checked',false);
/*$('input:checkbox.checkboxes').each(function(){
var sthisval=(this.checked ? $(this):"");alert(sthisval);
});*/
}

function getPurpose(val){
	if(val==2){
		$("#new").css('display','block');	
	}else{
		$("#new").css('display','none');
	}
	
}
    
var dateToday = new Date();
var dates = $("#from").datepicker({
    //defaultDate: "+1w",
    dateFormat: "dd/mm/yy",
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    onSelect: function(selectedDate) {
    var catid=$("#catId").val();
    var shiftid=$("#shfid").val();
    if(catid =="NO" || catid == "" || catid == undefined){
        toasterrormsg("Select Category");
        $("#from").val("DD/MM/YY");
        $("#category").css('border-color','red');
        return false;
    }else{
        $("#category").css('border-color','');
        if(shiftid =="NO" || shiftid == "" || shiftid == undefined){
            toasterrormsg("Select OverTime Shift From Drop Down");
            $("#from").val("MM/DD/YY");
            $("#shift").css('border-color','red');
            return false;
        }else{
            $("#shift").css('border-color','');
            var option = this.id == "from" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            dates.not(this).datepicker("option", option, date);

            getEmpDetailsByDate(selectedDate);
            //getShiftTime(selectedDate);
            
        }
    
    }
    
  }
});


var pastdates = $("#pastdate").datepicker({

    //defaultDate: "+1w",
    dateFormat: "dd/mm/yy",
    changeMonth: true,
    numberOfMonths: 1,
    //maxDate: dateToday,
    onSelect: function(selectedDate) {
    var catid=$("#catId").val();
    var shiftid=$("#shfid").val();
    if(catid =="NO" || catid == "" || catid == undefined){
        toasterrormsg("Select Category");
        $("#from").val("MM/DD/YY");
        $("#category").css('border-color','red');
        return false;
    }else{
        $("#category").css('border-color','');
        if(shiftid =="NO" || shiftid == "" || shiftid == undefined){
            toasterrormsg("Select OverTime Shift From Drop Down");
            $("#from").val("MM/DD/YY");
            $("#shift").css('border-color','red');
            return false;
        }else{
            $("#shift").css('border-color','');
            //var option = this.id == "from" ? "maxDate" : "minDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            //dates.not(this).datepicker("option", option, date);

            getEmpDetailsByDate(selectedDate);
            //getShiftTime(selectedDate);
            
        }
    
    }
    
  }
});


function getEmpDetailsByDate(dateText){
    var empCode= $("#team").val(); 
    var finYear=$("#finYear").val();
    var ot_type=$('input[name=ot_type]:checked', '#form').val();
    var catId=$("#catId").val();
    var d=dateText.split('/');
    var date=d[1]+'/'+d[0]+'/'+d[2];
    var string=$("#shfid").val();
    var shiftid1=string.slice(0,-1);
    var type="getShift";
    //alert(shiftid1); return false;
    $.ajax({
            type: "POST",
            url: "ajax/calculate_diff_time_ajax.php?type="+type,
            data:{date:date,empCode:empCode, finYear:finYear,shift:shiftid1,category:catId},
             beforeSend: function(){
            loading();
            },
            success: function(result){
                  unloading();
                //alert(result);
                //$("#totalHour").val(result);
                //getShiftTime(selectedDate);
                $("#showTable").html(result);
                
            }
          });
}


function getShiftTime(otDate,id){
    //var type="getShiftTime";
    var type="getShiftTime1";

    var catid=$("#catId").val();
    var shiftid=$("#shfid").val();
    var d=otDate.split('/');
    var date=d[1]+'/'+d[0]+'/'+d[2];
    var currTime=getCurrentTime();
    var endShiftTime=$("#et"+id).html();
    var ot_type=$('input[name=ot_type]:checked', '#form').val();
    var currD=now();
    var cd=currD.split('/');
    var currDate=cd[1]+'/'+cd[0]+'/'+cd[2];
    var convCurrDate=cd[2]+'-'+cd[0]+'-'+cd[1];
    var diff = 0;
    if (date == currDate) {
      var day=convCurrDate+' ';
        if(catid == 4 && ot_type == "2"){
            
            if(document.getElementById("mulCheck"+id).checked==true){
               $.ajax({
                type: "POST",
                url: "ajax/calculate_diff_time_ajax.php",
                data:{day:day,endShiftTime:endShiftTime,currTime:currTime,type:type},
                 beforeSend: function(){
                loading();
                },
                success: function(result){
                    unloading();
                    //alert(result);
                    if(result < 2){
                      toasterrormsg("Not Eligible For This Shift It will be routed as deviation");
                      $("#leaveSubmit").attr("disabled",true);
                    }
                    else{
               $("#leaveSubmit").attr("disabled",false);
                   }
                  }
            });
            }else{
              $("#leaveSubmit").attr("disabled",false);
            }
           
            
            
        }else if(catid == 4 && ot_type == "1"){
          diff=( (new Date(day + endShiftTime) - new Date(day + currTime) )/1000/60/60) ;
             //alert(diff);
             
            if(diff > 2){
              toasterrormsg("Not Eligible For This Shift It will be routed as Standard");
              $("#leaveSubmit").attr("disabled",true);
            }
        }else{
          $("#leaveSubmit").attr("disabled",false);
        }

    }


}


function getCurrentTime(){
    var dt = new Date();
    return dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    //alert(dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds());
}


function getother(value){
	if(value == 9){
		$("#div_other").css('display','block');	
	}else{
		$("#div_other").css('display','none');
	}
 }



function gettable_val(empCode){ 

 	var type="getApprId";
 	var code= empCode;

    $.ajax({
        type: "POST",
        url: "ajax/calculate_diff_time_ajax.php",
        data: {type:type,code:empCode},
        cache:false,
        success: function (result) {
          //alert(result);
          $("#levellist").val(result);
            levelfunc(empCode);

        }
    });
}


function levelfunc(empCode){

    var level_list = $("#levellist").val();
    var l = level_list.split(';');
    var emp_code=empCode;
  
    $.ajax({
        type: "POST",
        url: "ajax/calculate_diff_time_ajax.php?type=data2",
        data: {dataval:l[2], emp_code:emp_code},
        cache:false,
        success: function (result) {
            
            $("#showlevel").html(result);
            emp_code=result;
            getFinancialYear();
        }
    });
}

function deviationApprover(empCode){
  var type="getDeviApprId";
  var code= empCode;
   var autoapp = $("#advancewfmethod").val();
   //alert(autoapp);
    if(autoapp != 'automatic'){
      $("#approverDiv").css('display','block');
    }
    else{
      $("#approverDiv").css('display','none');
    }

    $.ajax({
        type: "POST",
        url: "ajax/calculate_diff_time_ajax.php",
        data: {type:type,code:empCode},
        cache:false,
        success: function (result) {
          //alert(result);
          $("#levellist").val(result);
            deviationLevelFunc(empCode);

        }
    });
}


function deviationLevelFunc(empCode){

    var level_list = $("#levellist").val();
    var l = level_list.split(';');
    var emp_code=empCode;

  
    $.ajax({
        type: "POST",
        url: "ajax/calculate_diff_time_ajax.php?type=deviData2",
        data: {dataval:l[2], emp_code:emp_code},
        cache:false,
        success: function (result) {
            
            
            $("#showlevel").html(result);
            emp_code=result;
            getFinancialYear();
        }
    });
}



function now()
{
  var d = new Date();
  var month = d.getMonth()+1;
  var day = d.getDate();

  var output = (month<10 ? '0' : '') + month + '/' 
              + (day<10 ? '0' : '') + day + "/"
              + d.getFullYear();
    //alert(output);
  return output;
}



function quartLimit(len){
 
  var data =[];
  for (var i =0; i<len;i++) { 
    data.push($("#quar"+i).html());
  }  
  $("#selectedquartlimit").val(data.toString());
  getselectedShift(len);
}

function allCheck(checkid,code) { 
    
    var type = "AllCheck";

    if($("#"+checkid).is(":checked")){   
        $.ajax({
            type: "POST",
            url: "ajax/calculate_diff_time_ajax.php",
            data: {mngrCode :  code, type: type},
            success: function (result) {
               if(result){
                // var str=result;
                // str = str.slice(0, -1);
                   $("#selectedcheckbox").val(result);
                   //convert string into array
                   var strVale = result;
                   arr = strVale.split(',');
                  
                    for(i=0; i < arr.length; i++)
                    {
                        $("#mulCheck"+i).attr("checked", true);
                    } 
                    quartLimit(arr.length);   
               }

            }
        });
    }
    
    else if($("#"+checkid).is(":not(:checked)")){ 
        $.ajax({
                type: "POST",
                url: "ajax/calculate_diff_time_ajax.php",
                data: {mngrCode :  code, type: type},
                success: function (result1) {
                   if(result1){
                       var strVale1 = result1;
                       arr1 = strVale1.split(',');
                      
                        for(j=0; j < arr1.length; j++)
                        {
                            $("#mulCheck"+j).attr("checked", false);
                        }    
                        $("#selectedcheckbox").val("0");
                        quartLimit(arr.length); 
                   }

                }
        });
    
    }
}


function mulCheck(mulcheckid) {
        var vals = [];
        var quarLim =[]; 
        var shiftName =[]; 
        
        if($("#mulCheck"+mulcheckid).is(":checked")){

          $('.checkboxes:checked').each(function(){
              vals.push($(this).val());
              //alert($(this).attr('qlimit'));
              quarLim.push($(this).attr('qlimit'));
              shiftName.push($(this).attr('sName'));

            });
            
            $("#selectedcheckbox").val(vals);
            $("#selectedquartlimit").val(quarLim);
            $("#selectedshfid").val(shiftName);
           
        }
        else if($("#mulCheck"+mulcheckid).is(":not(:checked)")){
            $("#Allcheck").val("0");
            $("#uniform-Allcheck  > span").removeClass ( 'checked' );

            $('.checkboxes:checked').each(function(i) {
                vals.push($(this).val());
                quarLim.push($(this).attr('qlimit'));
                shiftName.push($(this).attr('sName'));
            });

              $("#selectedcheckbox").val(vals);
              $("#selectedquartlimit").val(quarLim);
              $("#selectedshfid").val(shiftName);
            //quartLimit(vals.length); 
             
        }
}



function getselectedShift(len){
  var data =[];
  for (var i =0; i<len;i++) { 
    data.push($("#shfName"+i).html());
  }  
  $("#selectedshfid").val(data.toString());

}

function allPayCycle(checkid,mngrCode){

    var type="allPayCycle";

    if($("#"+checkid).is(":checked")){   
        $.ajax({
                type: "POST",
                url: "ajax/calculate_diff_time_ajax.php",
                data: {mngrCode :  mngrCode, type: type},
                dataType: 'json',
                success: function (result) {
                   if(result){ 
                    for(i=0; i< result.length;i++ ){

                        $("#pay"+i).val(result[i]);
                         endShift(i);
                    }
                   }else{
                        toasterrormsg("No Data Availabe");
                   }

                }
        });
    }
    
    else if($("#"+checkid).is(":not(:checked)")){ 

         $.ajax({
                type: "POST",
                url: "ajax/calculate_diff_time_ajax.php",
                data: {mngrCode :  mngrCode, type: type},
                dataType: 'json',
                success: function (result) {
                   if(result){ 
                    for(i=0; i< result.length;i++ ){

                        $("#pay"+i).val(0);
                         endShift(i);
                    }
                   }else{
                        toasterrormsg("No Data Availabe");
                   }

                }
        });
    }
}


function payCycle(empCode,id){
     
     var type="payCycle";
     //var pay=$("#pay"+id).val();

      if($("#mulCheck"+id).is(":checked")){
         $.ajax({
                    type: "POST",
                    url: "ajax/calculate_diff_time_ajax.php",
                    data: {empCode :  empCode, type: type},
                    success: function (result) { 
                       if(result){
                           
                            $("#pay"+id).val($.trim(result));
                             endShift(id);
                       }

                    }
            });
        }else if($("#mulCheck"+id).is(":not(:checked)")){
             $("#pay"+id).val(0);
              endShift(id);
        }



}


/*function endShift(id){

      if($("#from").val() !=""){
          var day1= $("#from").val();
      }else{
          var day1= $("#pastdate").val();
      }
      //alert(day1);
      //'1/1/1900 ';
      var startTime= $("#Time1").val();
      var endshift= $("#et"+id).html();

      var date = day1.split('/');
      var day=date[0];

     
      var monthIndex = date[1];
      var year = date[2];
      var ind=monthIndex.split('');
      if(monthIndex == 01){
        monthIndex="January";
      }else if(monthIndex == 02){
        monthIndex="February";
      }else if(monthIndex == 03){
        monthIndex="March";
      }else if(monthIndex == 04){
        monthIndex="April";
      }else if(monthIndex == 05){
        monthIndex="May";
      }else if(monthIndex == 06){
        monthIndex="June";
      }else if(monthIndex == 07){
        monthIndex="July";
      }else if(monthIndex == 08){
        monthIndex="August";
      }else if(monthIndex == 09){
        monthIndex="September";
      }else if(monthIndex == 10){
        monthIndex="October";
      }else if(monthIndex == 11){
        monthIndex="November";
      }else if(monthIndex == 12){
        monthIndex="December";
      }
     

      var strdate=startTime.split(':');
      if(strdate[0] == "0"){
        day1=parseInt(day)+parseInt(1); 
        var fdate1=day1 + ' ' + monthIndex + ' ' + year + ' ';
      }else{
        day1=day; 
        var fdate1=day1 + ' ' + monthIndex + ' ' + year + ' ';
      }
      
        var cat=$("#category").val();
        if($("#mulCheck"+id).is(":checked")){ 
          
         var fdate=day + ' ' + monthIndex + ' ' + year + ' ';
         //alert(fdate);
          //stTime=Converttimeformat();
              start = new Date(fdate1 + startTime); //eg "09:20 "
              stt = start.getTime();
              end = new Date(fdate + endshift); //eg "10:00 "
              endt = end.getTime();
            
            console.log(start + ' start');
            console.log(end + ' end');

            if(start < end) {

                toasterrormsg("Start Time should be greater Than End Shift");
                $("#tr"+id).css('background-color','red');
                 $('#leaveSubmit').attr("disabled",true);
                
            } 
            if(cat == 1 || cat == 2 || cat == 3 || cat == 4){
                 
              diff=( (new Date(fdate1 + startTime) - new Date(fdate + endshift) )/1000/60/60) ;
                        
                 if(diff > validateTime){
                    toasterrormsg("Start Time Of OverTime Should Not Be Greater 2 Hours");
                    $("#tr"+id).css('background-color','red');
                    $("#error").val("1");
                    $('#leaveSubmit').attr("disabled",true);
                 } // for diffrence between endshift and  OT time 
            }
            
        }else if($("#mulCheck"+id).is(":not(:checked)")){
            $("#tr"+id).css('background-color','green');
            $("#error").val("0");
            $('#leaveSubmit').attr("disabled",false);
        }

}*/


function endShift(id){

      var day1;
      var ot_type=$('input[name=ot_type]:checked', '#form').val();
      var empStatus=$("#empStatus"+id).html();
      if(ot_type == 1){
           day1= $("#pastdate").val();
      }else{
           day1= $("#from").val();
      }
      //alert(day1);
      //'1/1/1900 ';
      var startTime= $("#Time1").val();
      var endshift= $("#et"+id).html();

      var date = day1.split('/');
      var day=date[0];

     
      var monthIndex = date[1];
     
      var year = date[2];
      var ind=monthIndex.split('');
      if(monthIndex == 01){
        monthIndex="January";
      }else if(monthIndex == 02){
        monthIndex="February";
      }else if(monthIndex == 03){
        monthIndex="March";
      }else if(monthIndex == 04){
        monthIndex="April";
      }else if(monthIndex == 05){
        monthIndex="May";
      }else if(monthIndex == 06){
        monthIndex="June";
      }else if(monthIndex == 07){
        monthIndex="July";
      }else if(monthIndex == 08){
        monthIndex="August";
      }else if(monthIndex == 09){
        monthIndex="September";
      }else if(monthIndex == 10){
        monthIndex="October";
      }else if(monthIndex == 11){
        monthIndex="November";
      }else if(monthIndex == 12){
        monthIndex="December";
      }
     

      var strdate=startTime.split(':');
      if(strdate[0] == "0"){
        day1=parseInt(day)+parseInt(1); 
        var fdate1=day1 + ' ' + monthIndex + ' ' + year + ' ';
      }else{
        day1=day; 
        var fdate1=day1 + ' ' + monthIndex + ' ' + year + ' ';
      }
      
        var cat=$("#category").val();
        if($("#mulCheck"+id).is(":checked")){ 
          
         var fdate=day + ' ' + monthIndex + ' ' + year + ' ';
         //alert(fdate);
          //stTime=Converttimeformat();
              start = new Date(fdate1 + startTime); //eg "09:20 "
              stt = start.getTime();
              end = new Date(fdate + endshift); //eg "10:00 "
              endt = end.getTime();
            
            console.log(start + ' start');
            console.log(end + ' end');

            if(start < end) {

                toasterrormsg("Start Time should be greater Than End Shift");
                $("#tr"+id).css('background-color','red');
                 $('#leaveSubmit').attr("disabled",true);
                
            } 
             if(empStatus == "Working" || empStatus == "Leave" || empStatus == "MarkPastAttendance" || empStatus == "Out On Duty"){
              if(cat == 1 || cat == 2 || cat == 3 ){
                   
                diff=( (new Date(fdate1 + startTime) - new Date(fdate + endshift) )/1000/60/60) ;
                          
                   if(diff > validateTime){
                      toasterrormsg("OT shall be continuous, OT start time not more then 2 hrs of shift end time.");
                      $("#tr"+id).css('background-color','red');
                      $("#error").val("1");
                      $('#leaveSubmit').attr("disabled",true);
                   } // for diffrence between endshift and  OT time 
              }
            }
            
        }else if($("#mulCheck"+id).is(":not(:checked)")){
            if($("#tr"+id).attr('bgcolor') == "red"){
               $("#tr"+id).css('background-color','red');
             }else{
                $("#tr"+id).css('background-color','green');
             }
           
            $("#error").val("0");
            $('#leaveSubmit').attr("disabled",false);
        }

}


function submitOverTime(mngrCode){
   
     var appDate=$("#applyDate").val();
     var category =$("#category").val();
     var shift=$("#selectedshfid").val();
     var otdate=$("#from").val();
     var pastotdate=$("#pastdate").val();
     var startTime=$("#Time1").val();
     var endTime=$("#Time2").val();
     var totalHour=$("#totalHour").val();
     var purpose=$("#purpose").val();
     var newFunReq=$("#newRequest").val();
     var other=$("#other").val();
     var approver=$("#approverId").val();
     var selectEmp=$("#selectedcheckbox").val();
     var endShift=$("#endShift").val();
     var finYear=$("#finYear").val();
     var QuartLimt=$("#selectedquartlimit").val();
     var ottype=$('input[name=ot_type]:checked').val();
     var numOfRow=$("#numRow").val();
     var autoapprover=$("#autoapprover").val();
     var advancewfmethod=$("#advancewfmethod").val();
     var wfmethod=$("#wfmethod").val();


     if(ottype == 1){
         otdate = pastotdate;
         wfmethod = advancewfmethod;
     }

    
    
    /*--------------Start Find Number of days----------------*/
      var d = new Date();
      var month = d.getMonth()+1;
      var day = d.getDate();

      var output = ((''+day).length<2 ? '0' : '') + day +'/'+
      ((''+month).length<2 ? '0' : '') + month + '/' +
      d.getFullYear();

      var f=output.split('/');
      var s=otdate.split('/');
    
      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = new Date(f[2],f[1],f[0]);
      var secondDate = new Date(s[2],s[1],s[0]);

      var numberOfDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
      numberOfDays=parseInt(numberOfDays)+ parseInt(1);
      //alert(numberOfDays); 
    /*--------------End Find Number of days----------------*/

    /*------------Start First Validation----------------*/
    
        if(category == "NO"){
             toasterrormsg("Select OverTime Category From Drop Down");
             $("#category").css('border-color','red');
             return false;
        }else{
            $("#category").css('border-color','');
        }

        if(shift == " " || shift == null){
            toasterrormsg("Select OverTime Shift From Drop Down");
            $("#shift").css('border-color','red');
            return false;
        }else{
            $("#shift").css('border-color','');
        }

        /*------OT Date Validation-----*/
            if(ottype == 2){
               if(otdate == " "){
                   
                    toasterrormsg("Select OverTime Date");
                    $("#from").css('border-color','red');
                    return false;
                  
                }
                else{
                    $("#from").css('border-color','');
                }


               if(category == 1){ 
                    if(numberOfDays <= 2){
                        toasterrormsg("Weekly Off Overtime can be apply Before 2 days, Please select deviation from Overtime type");
                       // $("#from").css('border-color','red');
                        return false;
                    }else{
                       // $("#from").css('border-color','');
                    }   
               }else if(category == 2){
          
                    if(numberOfDays <= 15){
                        toasterrormsg("Holiday Overtime can be apply Before 15 days, Please select deviation from Overtime type");
                        return false;
                    }
               }else if(category == 3){
                    if(numberOfDays <= 1){
                        toasterrormsg("Emergancy Can not be apply Before 1 days So It will be routed as a deviation");
                        return false;
                    }
               }
            }
            else if(ottype == 1){
            
              if(pastotdate == " "){
                  toasterrormsg("Select OverTime Date");
                  $("#pastdate").css('border-color','red');
                  return false;
              }else{
                $("#pastdate").css('border-color','');
                  
              }

             /*if(category == 1){ 
                  if(numberOfDays > 2){
                      toasterrormsg("Weekly Off Can not be apply More Than 2 days So It  will be routed as a Standard");
                     // $("#from").css('border-color','red');
                      return false;
                  }else{
                     // $("#from").css('border-color','');
                  }   
               }else if(category == 2){
          
                    if(numberOfDays > 15){
                        toasterrormsg("Holiday Can not be apply More Than 15 days So It will be routed as a Standard");
                        return false;
                    }
               }else if(category == 3){
                    if(numberOfDays > 1){
                        toasterrormsg("Emergancy Can not be apply More Than 1 days So It will be routed as a Standard");
                        return false;
                    }
             }*/
             
            }  
        /*------End OT Date Validation-----*/

          if(totalHour.match('0:0')){
              toasterrormsg("Please Select Ot Start Time And OT End Time");
                  return false;
          }
         
          if(selectEmp == 0){
              toasterrormsg("Select Employee For overTime From The Table");
                  return false;
          }

          if(purpose == ""){
              toasterrormsg("Select Pupose Of OverTime");
              $("#purpose").css('border-color','red');
              return false;
          }else{
              $("#purpose").css('border-color','');
          }

          if(purpose == 2){
              if(newFunReq == ""){
                toasterrormsg("Select OverTime Reason From OT Request Reason ");
                $("#newRequest").css('border-color','red');
                return false;
              }else if(newFunReq == 9){
                if(other == ""){
                   $("#other").css('border-color','red');
                    return false;
                }else{
                  $("#other").css('border-color','');
                }
              }else{
                $("#newRequest").css('border-color','');
              }
          }

    /*--------End First Validation-------------*/
    /*--------Start Second Validation-------------*/
        
       var toh=totalHour.split(':');                   
       var qu=QuartLimt.split(',');
       var checkedList=selectEmp.split(',');

       for(var i=0; i< checkedList.length; i++){
            var qu1=qu[i].split(':');
            var QT = (qu1[0]*60) + qu1[1];//converting into minutes
            var TH = (toh[0]*60) + toh[1];//converting into minutes
            if(parseInt(QT) < parseInt(TH)){
                var r = confirm(checkedList[i]+' has No Enough Balances For OverTime.You want to Continue?');
                if (r == true) { 
                  
                } else {
                  toasterrormsg("Deselect Employee "+checkedList[i]);
                  return false;
                }
            }else{
                //alert('else case ');
               
            }
       }

        

    /*--------End Second Validation-------------*/

    /*--------Start Insertion Process---------*/
      var type="insertOT";

      $.ajax({
          type: "POST",
          url: "ajax/calculate_diff_time_ajax.php",
          data: {mngrCode :  mngrCode, type: type,appDate:appDate,category:category,shift:shift,otdate:
              otdate,startTime:startTime,endTime:endTime,totalHour:totalHour,purpose:purpose,newFunReq:newFunReq,
              other:other,approver:approver,selectEmp:selectEmp,finYear:finYear,QuartLimt:QuartLimt,ottype:ottype,
              wfmethod:wfmethod,autoapprover:autoapprover},
          // dataType: 'json',
          success: function (result) { //alert(result); 
              if(result == 5){
                toastmsg("Succesfully Request Generated");
                setTimeout(function(){
                    location.reload();
                 },3000);
              }else if(result == 6){
                toasterrormsg("Error In Request Generation");
              }else{
                  toasterrormsg("This Employee"+result+"Already Apllied OverTime.");
              }
              
          }
      });
  /*--------End Insertion Process---------*/
}



